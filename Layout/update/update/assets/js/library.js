

(function($) {
    var size;
  
    //fixed HEADER WHEN SCROLL PAGE
    function fixedMenu() {
        var sc = $(window).scrollTop();

        if ($(window).width() >= 992) {
            if (sc > 165) {
                $('#header-sroll').addClass('fixed');
                $(".content-main").css('margin-top','52px');
            }else {
                $('#header-sroll').removeClass('fixed');
                $(".content-main").css('margin-top','0px');

            }
        }
        else{
            if (sc > 31) {
                $('.hder_mid').addClass('fixed');
                $(".content-main").css('margin-top','80px');
            }else {
                $('.hder_mid').removeClass('fixed');
                $(".content-main").css('margin-top','0px');

            }
        }

        
    }

    // VERIFY WINDOW SIZE
    function windowSize() {
        size = $(document).width();
        if (size >= 991) {
            $('body').removeClass('open-menu');
            $('.hamburger-menu .bar').removeClass('animate');
        }
    };

     // ESC BUTTON ACTION
    $(document).keyup(function(e) {
        if (e.keyCode == 27) {
            $('.bar').removeClass('animate');
            $('body').removeClass('open-menu');
            $('header .desk-menu .menu-container .menu .menu-item-has-children a ul').each(function( index ) {
                $(this).removeClass('open-sub');
            });
        }
    });

     // ANIMATE HAMBURGER MENU
    $('.hamburger-menu').on('click', function() {
        $('.hamburger-menu .bar').toggleClass('animate');
        $(".box_s_h_mb").removeClass('show_h');
        if($('body').hasClass('open-menu')){
            $('body').removeClass('open-menu');
        }else{
            $('body').toggleClass('open-menu');
        }
    });

     // btn-icon-menu-mobie-USER
    $(".box-btn-mb").on('click',function(){
        if ($(window).width() < 992) {
            $(".show-list").toggle(200);
            $(".box-btn-mb .fa").toggleClass("fa-times");
            $(".box-btn-mb .fa").toggleClass("fa-caret-down");
        }    
    });

    // back
    $('header .desk-menu .menu-container .menu .menu-item-has-children ul').each(function(index) {
        $(this).append('<li class="back"><a href="#">Back</a></li>');
    });

    // RESPONSIVE MENU NAVIGATION
    $('header .desk-menu .menu-container .menu .menu-item-has-children > a').on('click', function(e) {
        e.preventDefault();
        if(size <= 991){
            $(this).next('ul').addClass('open-sub');
        }
    });

    // CLICK FUNCTION BACK MENU RESPONSIVE
    $('header .desk-menu .menu-container .menu .menu-item-has-children ul .back').on('click', function(e) {
        e.preventDefault();
        $(this).parent('ul').removeClass('open-sub');
    });

    $('body .over-menu').on('click', function() {
        $('body').removeClass('open-menu');
        $('.bar').removeClass('animate');
        $(".box_s_h_mb").removeClass('show_h');
        $('body').removeClass('bgr_x');

    });

    $(document).ready(function(){
        windowSize();
    });

    $(window).scroll(function(){
        fixedMenu();
    });

    $(window).resize(function(){
        windowSize();
    });



 
   




    // Back to top
    $(".back-to-top a").click(function (n) {
        n.preventDefault();
        $("html, body").animate({
            scrollTop: 0
        }, 500)
    });
    $(window).scroll(function () {
        $(document).scrollTop() > 1e3 ? $(".back-to-top").addClass("display") : $(".back-to-top").removeClass("display")
    });

})(jQuery);


// slide_index
$('.slide_index').owlCarousel({
    loop:true,
    items:1,
    smartSpeed:450,
    paginationSpeed : 400,
    nav:true,
    responsive:{
        0:{
           
            nav:true,
        },
        992:{
           
            nav:true,
        },
    }

    
});





// list_news
$('.owl_news_id').owlCarousel({
    loop:true,
    items:1,
    responsiveClass:true,
    
})
// list video news id
$('.list_videos_id').owlCarousel({
    loop:true,
    items:3,
    margin:10,
    dots:false,
    nav:true,
    responsiveClass:true,
    responsive:{
        768:{
            items:3,
        },
        0:{
            items:2,
        }
    }
    
})

// list news same same
$('.list_news_same').owlCarousel({
    loop:true,
    items:3,
    margin:10,
    dots:true,
    nav:false,
    responsiveClass:true,
    responsive:{
        768:{
            items:3,
        },
        0:{
            items:2,
        }
    }
    
})

//list_nghien cứu
$('.list_nghiencuu').owlCarousel({
    center: true,
    items:3,
    loop:true,
    margin:10,
    nav:true,
    responsive:{
        767:{
            items:3,
            nav:true,
        },
        0:{
            center: false,
            items:2,
            nav:false,
        }
    }
});




