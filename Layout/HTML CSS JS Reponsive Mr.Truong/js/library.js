//mmMenu-----------------
var $menu = $("#mainMenu").clone();
$menu.attr("id", "my-mobile-menu");
$menu.mmenu({
    extensions: ["theme-white"]
});


// ontop
$(function () { 
    $(".back-to-top a").click(function(n) {
        n.preventDefault();
        $("html, body").animate({
            scrollTop: 0
        }, 500)
    });
    $(window).scroll(function() {
        $(document).scrollTop() > 1e3 ? $(".back-to-top").addClass("display") : $(".back-to-top").removeClass("display")
    }); 
})
jQuery(document).ready(function($) {   
   var slideTop = $('#akr_home');
    slideTop.owlCarousel({
        autoPlay: 3000,
        navigation: false,
        pagination: false,
        slideSpeed: 500,
        paginationSpeed: 500,
        singleItem: true ,
        pagination : true,
    });
    $('#akr_home_prev').click(function () {
        slideTop.trigger('owl.next');
    })
    $('#akr_home_next').click(function () {
        slideTop.trigger('owl.prev');
    }) 

    $(".slide-kh").owlCarousel({
        slideSpeed : 200,
        items : 4,
        itemsCustom : false,
        itemsDesktop : [1199, 4],
        itemsDesktopSmall : [979, 3],
        itemsTablet : [768, 2],
        itemsTabletSmall : false,
        itemsMobile : [479, 1],
        autoPlay: true,
        stopOnHover: true,
        autoHeight: true,
        responsive: true,
        navigation: true,
        pagination : false,
        navigationText: ["",""], 
    }); 
})  