﻿using Reddevil.Lib.Global;
using Reddevil.Lib.Models;
using Reddevil.Lib.MVC;

namespace Reddevil.Lib.CPControllers
{
    public class FormUserMarketingController : CPController
    {
        public FormUserMarketingController()
        {
            //khoi tao Service
            DataService = ModSellerService.Instance;
            DataEntity = new ModSellerEntity();

            CheckPermissions = true;
        }

        public void ActionIndex(FormUserMarketingModel model)
        {
            //sap xep tu dong
            var orderBy = AutoSort(model.Sort);

            //tao danh sach
            var dbQuery = ModSellerService.Instance.CreateQuery()
                                    .Where(model.MarketingID > 0, o => o.MarketingID == model.MarketingID)
                                    .Take(model.PageSize)
                                    .OrderBy(orderBy)
                                    .Skip(model.PageIndex * model.PageSize);

            ViewBag.Data = dbQuery.ToList_Cache();
            model.TotalRecord = dbQuery.TotalRecord;
            ViewBag.Model = model;

        }

        public void ActionAdd(FormUserMarketingModel model)
        {
        }

        public void ActionSave(FormUserMarketingModel model)
        {
            //if (ValidSave(model))
            //    SaveRedirect();
        }

        public void ActionApply(FormUserMarketingModel model)
        {
            //if (ValidSave(model))
            //    ApplyRedirect(model.RecordID, _item.ID);
        }

        public void ActionSaveNew(FormUserMarketingModel model)
        {
            //if (ValidSave(model))
            //    SaveNewRedirect(model.RecordID, _item.ID);
        }

        public override void ActionDelete(int[] arrID)
        {

            //xoa Promotion
            DataService.Delete("[ID] IN (" + CoreMr.Reddevil.Global.Array.ToString(arrID) + ")");

            //thong bao
            CPViewPage.SetMessage("Đã xóa thành công.");
            CPViewPage.RefreshPage();
        }

        #region private func

       // private ModAffilateUserEntity _item;
        //private bool ValidSave(FormUserMarketingModel model)
        //{
            
        //}


        #endregion private func
    }

    public class FormUserMarketingModel : DefaultModel
    {
        private int _langID = 1;
        public int LangID
        {
            get { return _langID; }
            set { _langID = value; }
        }

        public int MarketingID { get; set; }
     
    }

}