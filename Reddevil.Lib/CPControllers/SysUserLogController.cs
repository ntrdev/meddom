﻿using Reddevil.Lib.Models;
using Reddevil.Lib.MVC;
using System;

namespace Reddevil.Lib.CPControllers
{
    [CPModuleInfo(Name = "Nhật ký truy cập",
     Description = "Quản lý - Nhật ký truy cập",
     Code = "SysUserLog",
     Access = 1,
     Order = 9,
     ShowInMenu = true,
     CssClass = "icon-16-article", Partitioning = 3)]
    public class SysUserLogController : CPController
    {
        public SysUserLogController()
        {
            //khoi tao Service
            DataService = CPUserLogService.Instance;
            //CheckPermissions = false;
        }

        public void ActionIndex(SysUserLogModel model)
        {
            //sap xep tu dong
            var orderBy = AutoSort(model.Sort);

            //tao danh sach


            var dbQuery = CPUserLogService.Instance.CreateQuery()
                                  .Where(model.StartDate > DateTime.MinValue, o => o.Created >= model.StartDate)
                                    .Where(model.EndDate > DateTime.MinValue, o => o.Created <= model.EndDate)
                                .Take(model.PageSize)
                                .OrderBy(orderBy)
                                .Skip(model.PageIndex * model.PageSize);

            ViewBag.Data = dbQuery.ToList();
            model.TotalRecord = dbQuery.TotalRecord;
            ViewBag.Model = model;
        }
    }

    public class SysUserLogModel : DefaultModel
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}