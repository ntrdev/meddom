﻿using System;
using Reddevil.Lib.Global;
using Reddevil.Lib.Models;
using Reddevil.Lib.MVC;
using System.Web.Script.Serialization;
using System.Collections.Generic;

namespace Reddevil.Lib.CPControllers
{
    [CPModuleInfo(Name = "Quản lý Shop",
        Description = "Quản lý - Shop",
        Code = "ModShop",
        Access = 31,
        Order = 2,
        ShowInMenu = false,
        CssClass = "icon-16-article", Partitioning = 2)]
    public class ModShopController : CPController
    {
        public ModShopController()
        {
            //khoi tao Service
            DataService = ModShopService.Instance;
            DataEntity = new ModShopEntity();

            CheckPermissions = true;
        }

        public void ActionIndex(ModShopModel model)
        {
            //sap xep tu dong
            var orderBy = AutoSort(model.Sort);

            //tao danh sach
            var dbQuery = ModShopService.Instance.CreateQuery()
                                    .Where(!string.IsNullOrEmpty(model.SearchText), o => (o.Name.Contains(model.SearchText) || o.Url.Contains(model.SearchText)))
                                    .Take(model.PageSize)
                                    .OrderBy(orderBy)
                                    .Skip(model.PageIndex * model.PageSize);

            ViewBag.Data = dbQuery.ToList_Cache();
            model.TotalRecord = dbQuery.TotalRecord;
            ViewBag.Model = model;

            //send to client
            //CoreMr.Reddevil.SignalR.HubData<ModShopEntity>.SendData(ViewBag.Data);
        }

        public void ActionAdd(ModShopModel model)
        {
            if (model.RecordID > 0)
            {
                _item = ModShopService.Instance.GetByID(model.RecordID);

                //khoi tao gia tri mac dinh khi update
                // if (_item.Updated <= DateTime.MinValue) _item.Updated = DateTime.Now;
            }
            else
            {
                //khoi tao gia tri mac dinh khi insert
                //_item = new ModShopEntity
                //{
                //    CityID = model.C,
                //    BrandID = model.BrandID,
                //    Published = DateTime.Now,
                //    Updated = DateTime.Now,
                //    Order = GetMaxOrder(),
                //    Activity = CPViewPage.UserPermissions.Approve
                //};

                //var json = Global.Cookies.GetValue(DataService.ToString(), true);
                //if (!string.IsNullOrEmpty(json))
                //    _item = new JavaScriptSerializer().Deserialize<ModShopEntity>(json);
            }

            ViewBag.Data = _item;
            ViewBag.Model = model;
        }

        public void ActionSave(ModShopModel model)
        {
            if (ValidSave(model))
                SaveRedirect();
        }

        public void ActionApply(ModShopModel model)
        {
            if (ValidSave(model))
                ApplyRedirect(model.RecordID, _item.ID);
        }

        public void ActionSaveNew(ModShopModel model)
        {
            if (ValidSave(model))
                SaveNewRedirect(model.RecordID, _item.ID);
        }


        public void ActionSetNv(int[] selectp, int[] empmanger)
        {

            if (CheckPermissions && !CPViewPage.UserPermissions.Full)
            {
                //thong bao
                CPViewPage.Message.MessageType = Message.MessageTypeEnum.Error;
                CPViewPage.Message.ListMessage.Add("Bạn không có quyền.");
                return;
            }
            try
            {
                if (selectp.Length > 0 && empmanger.Length > 0)
                {
                    string _nameAdmin = string.Empty;
                    foreach (var item in empmanger)
                    {
                        var _userAdmin = CPUserService.Instance.GetByID(item);
                        _nameAdmin = _userAdmin != null ? (string.IsNullOrEmpty(_userAdmin.Name) ? _userAdmin.LoginName : _userAdmin.Name) : "";
                        for (int i = 0; selectp.Length > 0 && i < selectp.Length; i++)
                        {
                            var shop = ModShopService.Instance.GetByID_Cache(selectp[i]);
                            if (shop == null) continue;
                            shop.AdminID = item;
                            ModShopService.Instance.Save(shop, o => new { o.AdminID });
                        }

                    }
                    //thong bao
                    CPViewPage.SetMessage("Đã set nv quản lý " + _nameAdmin + " , cho " + selectp.Length + " shop");
                    CPViewPage.RefreshPage();
                }
                else
                {
                    CPViewPage.Message.MessageType = Message.MessageTypeEnum.Error;
                    CPViewPage.Message.ListMessage.Add("Chưa chọn phần quyền cho nv hoặc chưa chọn shop để set quyền.");

                }
            }
            catch (Exception ex)
            {
                Error.Write(ex);
                CPViewPage.Message.ListMessage.Add(ex.Message);

            }

        }

        public override void ActionDelete(int[] arrID)
        {
            //xoa cleanurl
            // ModCleanURLService.Instance.Delete("[Value] IN (" + CoreMr.Reddevil.Global.Array.ToString(arrID) + ") AND [Type]='Shop'");

            //xoa Shop
            DataService.Delete("[ID] IN (" + CoreMr.Reddevil.Global.Array.ToString(arrID) + ")");

            //thong bao
            CPViewPage.SetMessage("Đã xóa thành công.");
            CPViewPage.RefreshPage();
        }

        #region private func

        private ModShopEntity _item;
        private bool ValidSave(ModShopModel model)
        {
            TryUpdateModel(_item);

            //chong hack
            _item.ID = model.RecordID;

            ViewBag.Data = _item;
            ViewBag.Model = model;

            CPViewPage.Message.MessageType = Message.MessageTypeEnum.Error;

            //kiem tra quyen han
            if ((model.RecordID < 1 && !CPViewPage.UserPermissions.Add) || (model.RecordID > 0 && !CPViewPage.UserPermissions.Edit))
                CPViewPage.Message.ListMessage.Add("Quyền hạn chế.");

            //kiem tra ten
            if (_item.Name.Trim() == string.Empty)
                CPViewPage.Message.ListMessage.Add("Nhập tiêu đề.");

            //if (ModCleanURLService.Instance.CheckCode(_item.Code, "Shop", _item.ID, model.LangID))
            //    CPViewPage.Message.ListMessage.Add("Mã đã tồn tại. Vui lòng chọn mã khác.");

            //kiem tra chuyen muc
            //if (_item.MenuID < 1)
            //    CPViewPage.Message.ListMessage.Add("Chọn chuyên mục.");

            //kiem tra chuyen muc
            //if (_item.BrandID < 1)
            //    CPViewPage.Message.ListMessage.Add("Chọn thương hiệu.");

            if (CPViewPage.Message.ListMessage.Count != 0) return false;

            //if (string.IsNullOrEmpty(_item.Code)) _item.Code = Data.GetCode(_item.Name);

            //cap nhat state
            //  _item.State = GetState(model.ArrState);

            try
            {
                if (_item.Activity)
                {
                    var _user = _item.GetUser();
                    _user.IsShop = true;
                    _user.ShopID = _item.ID;
                    ModUserService.Instance.Save(_user, o => new { o.IsShop, o.ShopID });
                    if (_user != null)
                    {
                        if (_user.Email != model.Email)
                        {
                            if (!ModUserService.Instance.CheckEmail(model.Email))
                            {
                                _user.Email = model.Email;
                                ModUserService.Instance.Save(_user, o => new { o.Email });
                            }
                        }
                        ModNotificationService.Instance.Save(new ModNotificationEntity
                        {
                            Activity = false,
                            ToUserID = _user.ID,
                            Content = "Gian hàng của bạn đã được Admin xét duyệt. Bạn có thể bắt đầu bán hàng tại " + CoreMr.Reddevil.Web.HttpRequest.Domain,
                            Type = "Notification",
                            Name = "Thông báo xác nhận gian hàng.",
                            Created = DateTime.Now
                        });
                        //send to client
                        HubData hubData = new HubData();
                        _ = hubData.ConfirmOrder(_user.LoginName, "Gian hàng của bạn đã được Admin xét duyệt. Bạn có thể bắt đầu bán hàng tại " + CoreMr.Reddevil.Web.HttpRequest.Domain);
                    }
                }
                else
                {
                    var _user = _item.GetUser();
                    if (_user != null)
                    {
                        if (_user.Email != model.Email)
                        {
                            if (!ModUserService.Instance.CheckEmail(model.Email))
                            {
                                _user.Email = model.Email;
                                ModUserService.Instance.Save(_user, o => new { o.Email });
                            }
                        }

                        ModNotificationService.Instance.Save(new ModNotificationEntity
                        {
                            Activity = true,
                            ToUserID = _user.ID,
                            Content = "Gian hàng của bạn đã bị Admin khóa. Để biết thêm chi tiết liên hệ. " + WebResource.GetValue("Web_Email"),
                            Type = "Notification",
                            Name = "Thông báo khóa gian hàng.",
                            Created = DateTime.Now
                        });
                        //send to client
                        HubData hubData = new HubData();
                        _ = hubData.ConfirmOrder(_user.LoginName, "Gian hàng của bạn đã bị Admin khóa. Để biết thêm chi tiết liên hệ. " + WebResource.GetValue("Web_Email"));
                    }
                }
                //save
                ModShopService.Instance.Save(_item);

                //update url



            }
            catch (Exception ex)
            {
                Error.Write(ex);
                CPViewPage.Message.ListMessage.Add(ex.Message);
                return false;
            }

            return true;
        }



        #endregion private func
    }

    public class ModShopModel : DefaultModel
    {
        private int _langID = 1;
        public int LangID
        {
            get { return _langID; }
            set { _langID = value; }
        }

        public string SearchText { get; set; }
        public string Email { get; set; }

        public int MenuID { get; set; }
        public int BrandID { get; set; }

        public int State { get; set; }
        public List<int> ArrState { get; set; }
    }
}