﻿using Reddevil.Lib.MVC;

namespace Reddevil.Lib.CPControllers
{
    [CPModuleInfo(Name = "File tải lên",
        Description = "Quản lý - File tải lên",
        Code = "ModFile",
        Access = 15,
        Order = 9999,
        ShowInMenu = true,
        CssClass = "icon-16-component", Partitioning =2)]
    public class ModFileController : CPController
    {
        public void ActionIndex()
        {
        }
    }
}