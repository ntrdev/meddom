﻿using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using Reddevil.Lib.Global;
using Reddevil.Lib.Models;
using Reddevil.Lib.MVC;

namespace Reddevil.Lib.CPControllers
{
    [CPModuleInfo(Name = "Nhà khoa học",
        Description = "Quản lý - Nhà khoa học",
        Code = "ModScientist",
        Access = 31,
        Order = 4,
        ShowInMenu = true,
        CssClass = "icon-16-article", Partitioning = 1)]
    public class ModScientistController : CPController
    {
        public ModScientistController()
        {
            //khoi tao Service
            DataService = ModScientistService.Instance;
            DataEntity = new ModScientistEntity();
            CheckPermissions = true;
        }

        public void ActionIndex(ModScientistModel model)
        {
            //sap xep tu dong
            var orderBy = AutoSort(model.Sort);

            //tao danh sach
            var dbQuery = ModScientistService.Instance.CreateQuery()
                                .Where(!string.IsNullOrEmpty(model.SearchText), o => (o.Name.Contains(model.SearchText) || o.Url.Contains(model.SearchText)))
                                .WhereIn(o => o.MenuID, WebMenuService.Instance.GetChildIDForCP("Scientist", model.MenuID, model.LangID))
                                .Take(model.PageSize)
                                .OrderBy(orderBy)
                                .Skip(model.PageIndex * model.PageSize);

            ViewBag.Data = dbQuery.ToList();
            model.TotalRecord = dbQuery.TotalRecord;
            ViewBag.Model = model;
        }

        public void ActionAdd(ModScientistModel model)
        {
            if (model.RecordID > 0)
            {
                _item = ModScientistService.Instance.GetByID(model.RecordID);

                //khoi tao gia tri mac dinh khi update
                if (_item.Created <= DateTime.MinValue) _item.Created = DateTime.Now;
            }
            else
            {
                _item = new ModScientistEntity
                {
                    MenuID = model.MenuID,
                    Created = DateTime.Now,
                    Order = GetMaxOrder(),
                    Activity = CPViewPage.UserPermissions.Approve,
                    AdminID = CPLogin.CurrentUser.ID,
                    Gender = true
                };

                //khoi tao gia tri mac dinh khi insert
                var json = Cookies.GetValue(DataService.ToString(), true);
                if (!string.IsNullOrEmpty(json))
                    _item = new JavaScriptSerializer().Deserialize<ModScientistEntity>(json);
            }

            ViewBag.Data = _item;
            ViewBag.Model = model;
        }

        public void ActionSave(ModScientistModel model)
        {
            if (ValidSave(model))
                SaveRedirect();
        }

        public void ActionApply(ModScientistModel model)
        {
            if (ValidSave(model))
                ApplyRedirect(model.RecordID, _item.ID);
        }

        public void ActionSaveNew(ModScientistModel model)
        {
            if (ValidSave(model))
                SaveNewRedirect(model.RecordID, _item.ID);
        }

        public override void ActionDelete(int[] arrID)
        {
            //xoa cleanurl
            ModCleanURLService.Instance.Delete("[Value] IN (" + CoreMr.Reddevil.Global.Array.ToString(arrID) + ") AND [Type]='Scientist'");

            //xoa Scientist
            DataService.Delete("[ID] IN (" + CoreMr.Reddevil.Global.Array.ToString(arrID) + ")");

            //thong bao
            CPViewPage.SetMessage("Đã xóa thành công.");
            CPViewPage.RefreshPage();
        }

        #region private func

        private ModScientistEntity _item;

        private bool ValidSave(ModScientistModel model)
        {
            TryUpdateModel(_item);

            //chong hack
            _item.ID = model.RecordID;

            ViewBag.Data = _item;
            ViewBag.Model = model;

            CPViewPage.Message.MessageType = Message.MessageTypeEnum.Error;

            //kiem tra quyen han
            if ((model.RecordID < 1 && !CPViewPage.UserPermissions.Add) || (model.RecordID > 0 && !CPViewPage.UserPermissions.Edit))
                CPViewPage.Message.ListMessage.Add("Quyền hạn chế.");

            //kiem tra ten
            if (_item.Name.Trim() == string.Empty)
                CPViewPage.Message.ListMessage.Add("Nhập tên nhà khoa học.");

            if (ModCleanURLService.Instance.CheckUrl(_item.Url, "Scientist", _item.ID, model.LangID))
                CPViewPage.Message.ListMessage.Add("Mã đã tồn tại. Vui lòng chọn mã khác.");

            //kiem tra chuyen muc
            if (_item.MenuID < 1)
                CPViewPage.Message.ListMessage.Add("Chọn danh mục khoa học.");

            if (CPViewPage.Message.ListMessage.Count != 0) return false;

            if (string.IsNullOrEmpty(_item.Url)) _item.Url = Data.GetCode(_item.Name) + "-" + Global.Random.GetRandom(3, true);

            //cap nhat state

            try
            {

                //save
                ModScientistService.Instance.Save(_item);

                //update url
                ModCleanURLService.Instance.InsertOrUpdate(_item.Url, "Scientist", _item.ID, _item.MenuID, model.LangID);
            }
            catch (Exception ex)
            {
                Error.Write(ex);
                CPViewPage.Message.ListMessage.Add(ex.Message);
                return false;
            }

            return true;
        }

        private static int GetMaxOrder()
        {
            return ModScientistService.Instance.CreateQuery()
                    .Max(o => o.Order)
                    .ToValue().ToInt(0) + 1;
        }

        #endregion private func
    }

    public class ModScientistModel : DefaultModel
    {
        public int LangID { get; set; } = 1;

        public int MenuID { get; set; }
        public int State { get; set; }
        public string SearchText { get; set; }

        public List<int> ArrState { get; set; }
    }
}