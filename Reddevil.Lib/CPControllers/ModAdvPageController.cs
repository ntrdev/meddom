﻿using System;
using Reddevil.Lib.Global;
using Reddevil.Lib.Models;
using Reddevil.Lib.MVC;

namespace Reddevil.Lib.CPControllers
{
    [CPModuleInfo(Name = "Quảng cáo For Page",
        Description = "Quản lý - Quảng cáo For Page",
        Code = "ModAdvPage",
        Access = 31,
        Order = 10,
        ShowInMenu = true,
        CssClass = "icon-16-media", Partitioning = 2)]
    public class ModAdvPageController : CPController
    {
        public ModAdvPageController()
        {
            //khoi tao Service
            DataService = ModAdvPageService.Instance;
            CheckPermissions = true;
        }

        public void ActionIndex(ModAdvPageModel model)
        {
            //sap xep tu dong
            string orderBy = AutoSort(model.Sort, "[Order]");

            //tao danh sach
            var dbQuery = ModAdvPageService.Instance.CreateQuery()
                                    .WhereIn(model.MenuID > 0, o => o.MenuID, WebMenuService.Instance.GetChildIDForCP("Product", model.MenuID, model.LangID))
                                    .Where(!string.IsNullOrEmpty(model.SearchText), o => o.Name.Contains(model.SearchText))
                                    .Take(model.PageSize)
                                    .OrderBy(orderBy)
                                    .Skip(model.PageIndex * model.PageSize);

            ViewBag.Data = dbQuery.ToList();
            model.TotalRecord = dbQuery.TotalRecord;
            ViewBag.Model = model;
        }

        public void ActionAdd(ModAdvPageModel model)
        {
            if (model.RecordID > 0)
            {
                _item = ModAdvPageService.Instance.GetByID(model.RecordID);

                //khoi tao gia tri mac dinh khi update
            }
            else
            {
                _item = new ModAdvPageEntity
                {
                    MenuID = model.MenuID,
                    Activity = CPViewPage.UserPermissions.Approve,
                    Order = GetMaxOrder()
                };

                //khoi tao gia tri mac dinh khi insert
            }

            ViewBag.Data = _item;
            ViewBag.Model = model;
        }

        public void ActionSave(ModAdvPageModel model)
        {
            if (ValidSave(model))
                SaveRedirect();
        }

        public void ActionApply(ModAdvPageModel model)
        {
            if (ValidSave(model))
                ApplyRedirect(model.RecordID, _item.ID);
        }

        public void ActionSaveNew(ModAdvPageModel model)
        {
            if (ValidSave(model))
                SaveNewRedirect(model.RecordID, _item.ID);
        }

        #region private func

        private ModAdvPageEntity _item;

        private bool ValidSave(ModAdvPageModel model)
        {
            TryUpdateModel(_item);

            //chong hack
            _item.ID = model.RecordID;

            ViewBag.Data = _item;
            ViewBag.Model = model;

            CPViewPage.Message.MessageType = Message.MessageTypeEnum.Error;

            //kiem tra quyen han
            if ((model.RecordID < 1 && !CPViewPage.UserPermissions.Add) || (model.RecordID > 0 && !CPViewPage.UserPermissions.Edit))
                CPViewPage.Message.ListMessage.Add("Quyền hạn chế.");

            //kiem tra chuyen muc
            if (_item.MenuID < 1)
                CPViewPage.Message.ListMessage.Add("Chọn chuyên mục.");

            if (CPViewPage.Message.ListMessage.Count != 0) return false;

            try
            {
                //save
                ModAdvPageService.Instance.Save(_item);
            }
            catch (Exception ex)
            {
                Error.Write(ex);
                CPViewPage.Message.ListMessage.Add(ex.Message);
                return false;
            }

            return true;
        }

        private static int GetMaxOrder()
        {
            return ModAdvPageService.Instance.CreateQuery()
                    .Max(o => o.Order)
                    .ToValue().ToInt(0) + 1;
        }

        #endregion private func
    }

    public class ModAdvPageModel : DefaultModel
    {
        private int _LangID = 1;

        public int LangID
        {
            get => _LangID;
            set => _LangID = value;
        }

        public int MenuID { get; set; }
        public string SearchText { get; set; }
    }
}