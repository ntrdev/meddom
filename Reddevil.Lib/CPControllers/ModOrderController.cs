﻿using Reddevil.Lib.Global;
using Reddevil.Lib.Models;
using Reddevil.Lib.MVC;
using System;

namespace Reddevil.Lib.CPControllers
{
    [CPModuleInfo(Name = "Đơn hàng",
        Description = "Quản lý  - Đơn hàng",
        Code = "ModOrder",
        Access = 31,
        Order = 2,
        ShowInMenu = true,
        CssClass = "icon-16-article", Partitioning = 1)]
    public class ModOrderController : CPController
    {
        public ModOrderController()
        {
            //khoi tao Service
            DataService = ModOrderService.Instance;
            CheckPermissions = true;
        }

        public void ActionIndex(ModOrderModel model)
        {
            //sap xep tu dong
            var orderBy = AutoSort(model.Sort);

            //tao danh sach
            int year = model.Date.Year;
            model.Month = model.Date.Month;
            var dbQuery = ModOrderService.Instance.CreateQuery()
                                    .Where(!string.IsNullOrEmpty(model.SearchText), o => (o.Name.Contains(model.SearchText) || o.Code.Contains(model.SearchText) || o.Email.Contains(model.SearchText) || o.Phone.Contains(model.SearchText) || o.Address.Contains(model.SearchText)))
                                    .Where(model.StatusID > 0, o => o.StatusID == model.StatusID)
                                    .Where(model.Date > DateTime.MinValue, o => o.Month == model.Month && o.Year == year)
                                    .Take(model.PageSize)
                                    .OrderBy(orderBy)
                                    .Skip(model.PageIndex * model.PageSize);

            ViewBag.Data = dbQuery.ToList();
            model.TotalRecord = dbQuery.TotalRecord;
            ViewBag.Model = model;
            var dbOrder = ModOrderService.Instance.CreateQuery().ToList_Cache();

            if (dbOrder != null)
            {
                long _total = 0; int sucess = 0;
                foreach (var item in dbOrder)
                {
                    _total += item.Total;
                    sucess = item.Success ? sucess + 1 : sucess + 0;
                }
                ViewBag.Total = _total;
                ViewBag.AllOrder = dbOrder.Count;
                ViewBag.Sucess = sucess;
            }

        }

        public void ActionAdd(ModOrderModel model)
        {
            if (model.RecordID > 0)
            {
                _item = ModOrderService.Instance.GetByID(model.RecordID);

                //khoi tao gia tri mac dinh khi update
            }
            else
            {
                _item = new ModOrderEntity();

                //khoi tao gia tri mac dinh khi insert
            }

            ViewBag.Data = _item;
            ViewBag.Model = model;
        }

        public void ActionSave(ModOrderModel model)
        {
            if (ValidSave(model))
                SaveRedirect();
        }

        public void ActionApply(ModOrderModel model)
        {
            if (ValidSave(model))
                ApplyRedirect(model.RecordID, _item.ID);
        }
        public override void ActionDelete(int[] arrID)
        {
           
            //xoa đơn hàng
            DataService.Delete("[ID] IN (" + CoreMr.Reddevil.Global.Array.ToString(arrID) + ")");

            //thong bao
            CPViewPage.SetMessage("Đã xóa thành công.");
            CPViewPage.RefreshPage();
        }

        #region private func

        private ModOrderEntity _item;

        private bool ValidSave(ModOrderModel model)
        {
            TryUpdateModel(_item);

            ViewBag.Data = _item;
            ViewBag.Model = model;

            CPViewPage.Message.MessageType = Message.MessageTypeEnum.Error;

            //kiem tra quyen han
            if ((model.RecordID < 1 && !CPViewPage.UserPermissions.Add) || (model.RecordID > 0 && !CPViewPage.UserPermissions.Edit))
                CPViewPage.Message.ListMessage.Add("Quyền hạn chế.");

            if (CPViewPage.Message.ListMessage.Count != 0) return false;


          
            //save
            ModOrderService.Instance.Save(_item);

            return true;
        }

        #endregion private func
    }

    public class ModOrderModel : DefaultModel
    {
        private int _langID = 1;
        public int LangID
        {
            get { return _langID; }
            set { _langID = value; }
        }

        public int StatusID { get; set; }
        public int Month { get; set; }
        public DateTime Date { get; set; }
        public string SearchText { get; set; }
    }
}