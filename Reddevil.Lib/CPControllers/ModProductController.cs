﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using Reddevil.Lib.Global;
using Reddevil.Lib.Models;
using Reddevil.Lib.MVC;

namespace Reddevil.Lib.CPControllers
{
    [CPModuleInfo(Name = "Sản phẩm",
        Description = "Quản lý - Sản phẩm",
        Code = "ModProduct",
        Access = 31,
        Order = 1,
        ShowInMenu = true,
        CssClass = "icon-16-article", Partitioning = 1)]
    public class ModProductController : CPController
    {
        public ModProductController()
        {
            //khoi tao Service
            DataService = ModProductService.Instance;
            DataEntity = new ModProductEntity();

            CheckPermissions = true;
        }

        public void ActionIndex(ModProductModel model)
        {
            //sap xep tu dong
            var orderBy = AutoSort(model.Sort);

            //tao danh sach
            var dbQuery = ModProductService.Instance.CreateQuery()
                                    .Where(!string.IsNullOrEmpty(model.SearchText), o => (o.Name.Contains(model.SearchText) || o.Url.Contains(model.SearchText)) || o.Sku.Contains(model.SearchText))
                                    .Where(model.State > 0, o => (o.State & model.State) == model.State)
                                    .WhereIn(model.MenuID > 0, o => o.MenuID, WebMenuService.Instance.GetChildIDForCP("Product", model.MenuID, model.LangID))
                                    .Take(model.PageSize)
                                    .OrderBy(orderBy)
                                    .Skip(model.PageIndex * model.PageSize);

            ViewBag.Data = dbQuery.ToList_Cache();
            model.TotalRecord = dbQuery.TotalRecord;
            ViewBag.Model = model;

            //send to client
            //CoreMr.Reddevil.SignalR.HubData<ModProductEntity>.SendData(ViewBag.Data);
        }

        public void ActionAdd(ModProductModel model)
        {
            if (model.RecordID > 0)
            {
                _item = ModProductService.Instance.GetByID(model.RecordID);

                //khoi tao gia tri mac dinh khi update
                if (_item.Updated <= DateTime.MinValue) _item.Updated = DateTime.Now;
            }
            else
            {
                //khoi tao gia tri mac dinh khi insert
                _item = new ModProductEntity
                {
                    MenuID = model.MenuID,
                    Published = DateTime.Now,
                    Updated = DateTime.Now,
                    Order = GetMaxOrder(),
                    Activity = CPViewPage.UserPermissions.Approve
                };

                var json = Cookies.GetValue(DataService.ToString(), true);
                if (!string.IsNullOrEmpty(json))
                    _item = new JavaScriptSerializer().Deserialize<ModProductEntity>(json);
            }

            ViewBag.Data = _item;
            ViewBag.Model = model;
        }

        public void ActionSave(ModProductModel model)
        {
            if (ValidSave(model))
                SaveRedirect();
        }

        public void ActionApply(ModProductModel model)
        {
            if (ValidSave(model))
                ApplyRedirect(model.RecordID, _item.ID);
        }

        public void ActionSaveNew(ModProductModel model)
        {
            if (ValidSave(model))
                SaveNewRedirect(model.RecordID, _item.ID);
        }


        public override void ActionDelete(int[] arrID)
        {
            //xoa cleanurl
            ModCleanURLService.Instance.Delete("[Value] IN (" + CoreMr.Reddevil.Global.Array.ToString(arrID) + ") AND [Type]='Product'");

            //xoa Product
            DataService.Delete("[ID] IN (" + CoreMr.Reddevil.Global.Array.ToString(arrID) + ")");

            //thong bao
            CPViewPage.SetMessage("Đã xóa thành công.");
            CPViewPage.RefreshPage();
        }


        //public void ActionExport(ModProductModel model)
        //{
        //    //tao danh sach
        //    var listItem = ModProductService.Instance.CreateQuery()
        //                            .Where(!string.IsNullOrEmpty(model.SearchText), o => (o.Name.Contains(model.SearchText) || o.Url.Contains(model.SearchText)))
        //                            .Where(model.State > 0, o => (o.State & model.State) == model.State)
        //                            .WhereIn(o => o.MenuID, WebMenuService.Instance.GetChildIDForCP("Product", model.MenuID, model.LangID))

        //                            .ToList_Cache();

        //    //title	description	link	image_link	condition	availability	price	sale_price	sale_price_effective_date	brand
        //    var listExcel = new List<List<object>>();
        //    for (int i = 0; listItem != null && i < listItem.Count; i++)
        //    {
        //        var listRow = new List<object>
        //        {
        //            listItem[i].ID,
        //            listItem[i].Name,
        //            listItem[i].Content,
        //            CoreMr.Reddevil.Web.HttpRequest.Domain + "/" + listItem[i].Url + CoreMr.Reddevil.Web.Setting.Sys_PageExt,
        //            CoreMr.Reddevil.Web.HttpRequest.Domain + listItem[i].Url.Replace("~/", "/"),
        //            "new",
        //            "in stock",
        //            listItem[i].Price,
        //            "",
        //            "",
        //            listItem[i].GetMenu().Name
        //        };

        //        listExcel.Add(listRow);
        //    }

        //    string exportFile = CPViewPage.Server.MapPath("~/Data/upload/files/EXPORT/catalog_" + string.Format("{0:ddMMyyyy}", DateTime.Now) + "_" + DateTime.Now.Ticks + ".xls");

        //    Excel.Export(listExcel, 1, CPViewPage.Server.MapPath("~/CP/Template/catalog.xls"), exportFile);

        //    CPViewPage.Response.Clear();
        //    CPViewPage.Response.ContentType = "application/excel";
        //    CPViewPage.Response.AppendHeader("Content-Disposition", "attachment; filename=" + System.IO.Path.GetFileName(exportFile));
        //    CPViewPage.Response.WriteFile(exportFile);
        //    CPViewPage.Response.End();
        //}

        #region private func

        private ModProductEntity _item;
        //private List<ModPropertyEntity> listPro;
        private bool ValidSave(ModProductModel model)
        {
            TryUpdateModel(_item);

            //chong hack
            _item.ID = model.RecordID;

            ViewBag.Data = _item;
            ViewBag.Model = model;

            CPViewPage.Message.MessageType = Message.MessageTypeEnum.Error;

            //kiem tra quyen han
            if ((model.RecordID < 1 && !CPViewPage.UserPermissions.Add) || (model.RecordID > 0 && !CPViewPage.UserPermissions.Edit))
                CPViewPage.Message.ListMessage.Add("Quyền hạn chế.");

            //kiem tra ten
            if (_item.Name.Trim() == string.Empty)
                CPViewPage.Message.ListMessage.Add("Nhập tiêu đề.");

            if (_item.Price < 1)
                CPViewPage.Message.ListMessage.Add("Giá bán phải lớn hơn 0.");

            if (string.IsNullOrEmpty(_item.IDRnd))
                _item.IDRnd = Global.Random.GetRandom(6, false);

            if (string.IsNullOrEmpty(_item.Url)) _item.Url = Data.GetCode(_item.Name);

            if (ModCleanURLService.Instance.CheckUrl(_item.Url, "Product", _item.ID, model.LangID))
                CPViewPage.Message.ListMessage.Add("Mã đã tồn tại. Vui lòng chọn mã khác.");

            //kiem tra chuyen muc
            if (_item.MenuID < 1)
                CPViewPage.Message.ListMessage.Add("Chọn chuyên mục.");

            #region cập nhạt thuộc tính


            //var _AllKey = CPViewPage.PageViewState.AllKeys;
            //listPro = new List<ModPropertyEntity>();
            //foreach (var key in _AllKey)
            //{
            //    if (!key.StartsWith("Property_")) continue;

            //    var arrProperty = key.Split('_');
            //    if (arrProperty.Length < 2) continue;

            //    int propertyID = 0, propertyValueID = 0;

            //    switch (arrProperty.Length)
            //    {
            //        case 2:
            //            propertyID = CoreMr.Reddevil.Global.Convert.ToInt(key.Replace("Property_", ""));
            //            propertyValueID = CoreMr.Reddevil.Global.Convert.ToInt(CPViewPage.PageViewState[key]);
            //            break;

            //        case 3:
            //            propertyID = CoreMr.Reddevil.Global.Convert.ToInt(arrProperty[1]);
            //            propertyValueID = CoreMr.Reddevil.Global.Convert.ToInt(CPViewPage.PageViewState[key]);
            //            break;
            //    }

            //    if (propertyID < 1 || propertyValueID < 1) continue;


            //    listPro.Add(new ModPropertyEntity
            //    {
            //        ID = 0,
            //        ProductID = _item.ID,
            //        MenuID = _item.MenuID,
            //        PropertyID = propertyID,
            //        PropertyValueID = propertyValueID
            //    });
            //}
            #endregion

            if (CPViewPage.Message.ListMessage.Count != 0)
            {  //cap nhat thuoc tinh
                //if (listPro != null && listPro.Count > 0)
                //{
                //    string _jsonPro = JsonConvert.SerializeObject(listPro);
                //    Cookies.SetValue("property", _jsonPro, true);
                //}
                return false;
            }

            //cap nhat state
            _item.State = GetState(model.ArrState);

            try
            {

                _item.AdminEdit = CPLogin.CurrentUser.LoginName;

                //if (model.RecordID < 1) _item.Sku = string.IsNullOrEmpty(_item.Sku) ? "GSP-" + Global.Random.GetRandom(3, false) : _item.Sku.Replace(" ", "") + "-" + Global.Random.GetRandom(3, false);
                //_item.Sku = Regex.Replace(_item.Sku, " ", "");
                //save
                ModProductService.Instance.Save(_item);
                //update url
                ModCleanURLService.Instance.InsertOrUpdate(_item.Url, "Product", _item.ID, _item.MenuID, model.LangID);

                ////cap nhat thuoc tinh
                //ModPropertyService.Instance.Delete(o => o.ProductID == _item.ID);

                //for (int i = 0; listPro.Count > 0 && i < listPro.Count; i++)
                //{
                //    listPro[i].ProductID = _item.ID;
                //    ModPropertyService.Instance.Save(listPro[i]);
                //}
                //Cookies.Remove("property");

            }
            catch (Exception ex)
            {
                Error.Write(ex);
                CPViewPage.Message.ListMessage.Add(ex.Message);
                return false;
            }

            return true;
        }

        private static int GetMaxOrder()
        {
            return ModProductService.Instance.CreateQuery()
                    .Max(o => o.Order)
                    .ToValue().ToInt(0) + 1;
        }

        #endregion private func
    }

    public class ModProductModel : DefaultModel
    {
        public int LangID { get; set; } = 1;
        public string SearchText { get; set; }
        public int MenuID { get; set; }
        public int BrandID { get; set; }

        public int State { get; set; }
        public int[] ArrState { get; set; }
    }

    //public class ExcelEntity
    //{
    //    public string id { get; set; }
    //    public string title { get; set; }
    //    public string description { get; set; }
    //    public string link { get; set; }
    //    public string image_link { get; set; }
    //    public string condition { get; set; }
    //    public string availability { get; set; }
    //    public string price { get; set; }
    //    public string sale_price { get; set; }
    //    public string sale_price_effective_date { get; set; }
    //    public string brand { get; set; }
    //}
}