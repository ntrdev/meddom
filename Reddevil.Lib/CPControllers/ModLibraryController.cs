﻿using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using Reddevil.Lib.Global;
using Reddevil.Lib.Models;
using Reddevil.Lib.MVC;

namespace Reddevil.Lib.CPControllers
{
    [CPModuleInfo(Name = "Thư viện số",
        Description = "Quản lý - Thư viện số",
        Code = "ModLibrary",
        Access = 31,
        Order = 4,
        ShowInMenu = true,
        CssClass = "icon-16-article", Partitioning = 1)]
    public class ModLibraryController : CPController
    {
        public ModLibraryController()
        {
            //khoi tao Service
            DataService = ModLibraryService.Instance;
            DataEntity = new ModLibraryEntity();
            CheckPermissions = true;
        }

        public void ActionIndex(ModLibraryModel model)
        {
            //sap xep tu dong
            var orderBy = AutoSort(model.Sort);

            //tao danh sach
            var dbQuery = ModLibraryService.Instance.CreateQuery()
                                .Where(model.Value > 0, o => o.ID != model.Value)
                                .Where(!string.IsNullOrEmpty(model.SearchText), o => (o.Name.Contains(model.SearchText) || o.Url.Contains(model.SearchText)))
                                .WhereIn(o => o.CategoryID, WebMenuService.Instance.GetChildIDForCP("Library", model.MenuID, model.LangID))
                                .Take(model.PageSize)
                                .OrderBy(orderBy)
                                .Skip(model.PageIndex * model.PageSize);

            ViewBag.Data = dbQuery.ToList();
            model.TotalRecord = dbQuery.TotalRecord;
            ViewBag.Model = model;
        }

        public void ActionAdd(ModLibraryModel model)
        {
            if (model.RecordID > 0)
            {
                _item = ModLibraryService.Instance.GetByID(model.RecordID);
            }
            else
            {
                _item = new ModLibraryEntity
                {
                    CategoryID = model.MenuID,
                    Order = GetMaxOrder(),
                    Activity = false,
                    CPUser = CPLogin.CurrentUser.LoginName,
                };

                //khoi tao gia tri mac dinh khi insert
                var json = Cookies.GetValue(DataService.ToString(), true);
                if (!string.IsNullOrEmpty(json))
                    _item = new JavaScriptSerializer().Deserialize<ModLibraryEntity>(json);
            }

            ViewBag.Data = _item;
            ViewBag.Model = model;
        }

        public void ActionSave(ModLibraryModel model)
        {
            if (ValidSave(model))
                SaveRedirect();
        }
        public void ActionApply(ModLibraryModel model)
        {
            if (ValidSave(model))
                SaveRedirect();
        }

        public void ActionSaveNew(ModLibraryModel model)
        {
            if (ValidSave(model))
                SaveNewRedirect(model.RecordID, _item.ID);
        }

        public override void ActionDelete(int[] arrID)
        {
            if (CheckPermissions && !CPViewPage.UserPermissions.Delete)
            {
                //thong bao
                CPViewPage.Message.MessageType = Message.MessageTypeEnum.Error;
                CPViewPage.Message.ListMessage.Add("Bạn không có quyền xóa.");
                return;
            }
            else if (CheckPermissions && CPViewPage.UserPermissions.Full && CPViewPage.CurrentUser.IsAdministrator)
            {

                //xoa cleanurl
                ModCleanURLService.Instance.Delete("[Value] IN (" + CoreMr.Reddevil.Global.Array.ToString(arrID) + ") AND [Type]='Library'");

                //xoa Library
                DataService.Delete("[ID] IN (" + CoreMr.Reddevil.Global.Array.ToString(arrID) + ")");

                CPUserLogService.Instance.Save(new CPUserLogEntity
                {
                    Module = "Tin Tức",
                    Note = "Đã xóa",
                    UserID = CPLogin.UserID,
                    Created = DateTime.Now,
                    IP = CoreMr.Reddevil.Web.HttpRequest.IP,
                    TypeAction = "Đã xóa bài viết"
                });

                //thong bao
                CPViewPage.SetMessage("Đã xóa thành công.");

            }
            CPViewPage.RefreshPage();
        }

        #region private func

        private ModLibraryEntity _item;

        private bool ValidSave(ModLibraryModel model)
        {

            if (!CPViewPage.UserPermissions.Edit && !CPViewPage.UserPermissions.Add)
            {
                CPViewPage.Message.MessageType = Message.MessageTypeEnum.Error;
                CPViewPage.Message.ListMessage.Add("Bạn không có quyền sửa.");
                return false;
            }

            TryUpdateModel(_item);

            //chong hack
            _item.ID = model.RecordID;

            ViewBag.Data = _item;
            ViewBag.Model = model;

            CPViewPage.Message.MessageType = Message.MessageTypeEnum.Error;

            //kiem tra quyen han
            if ((model.RecordID < 1 && !CPViewPage.UserPermissions.Add) || (model.RecordID > 0 && !CPViewPage.UserPermissions.Edit))
                CPViewPage.Message.ListMessage.Add("Quyền hạn chế.");

            //kiem tra ten
            if (_item.Name.Trim() == string.Empty)
                CPViewPage.Message.ListMessage.Add("Nhập tiêu đề.");

            if (ModCleanURLService.Instance.CheckUrl(_item.Url, "Library", _item.ID, model.LangID))
                CPViewPage.Message.ListMessage.Add("Mã đã tồn tại. Vui lòng chọn mã khác.");

            //kiem tra chuyen muc
            if (_item.CategoryID < 1)
                CPViewPage.Message.ListMessage.Add("Chọn chuyên mục.");

            if (string.IsNullOrEmpty(_item.Image)) CPViewPage.Message.ListMessage.Add("Bạn chưa chọn ảnh đại diện cho bài viết.");
            if (string.IsNullOrEmpty(_item.File)) CPViewPage.Message.ListMessage.Add("Bạn chưa chọn file tài liệu bài viết.");

            if (CPViewPage.Message.ListMessage.Count != 0) return false;

            if (string.IsNullOrEmpty(_item.Url)) _item.Url = Data.GetCode(_item.Name) + "-" + Data.GetFirstChar(_item.Name);
          
            _item.State = GetState(model.ArrState);

            try
            {

                ModLibraryService.Instance.Save(_item);

                //update url
                ModCleanURLService.Instance.InsertOrUpdate(_item.Url, "Library", _item.ID, _item.CategoryID, model.LangID);


                CPUserLogService.Instance.Save(new CPUserLogEntity
                {
                    Module = "Thư viện số",
                    ObjectID = _item.ID,
                    Note = model.RecordID > 0 ? "Đã cập nhật Thư viện số" : "Thêm mới thư viện số",
                    UserID = CPLogin.UserID,
                    Created = DateTime.Now,
                    IP = CoreMr.Reddevil.Web.HttpRequest.IP,
                    TypeAction = model.RecordID > 0 ? "Đã cập nhật thư viện số " + _item.Name : "Thêm mới thư viện số",
                });


            }
            catch (Exception ex)
            {
                Error.Write(ex);
                CPViewPage.Message.ListMessage.Add(ex.Message);
                return false;
            }

            return true;
        }

        private static int GetMaxOrder()
        {
            return ModLibraryService.Instance.CreateQuery()
                    .Max(o => o.Order)
                    .ToValue().ToInt(0) + 1;
        }

        #endregion private func
    }

    public class ModLibraryModel : DefaultModel
    {
        public int LangID { get; set; } = 1;
        public int MenuID { get; set; }
        public int State { get; set; }
        public int Value { get; set; }
        public string SearchText { get; set; }
        public List<int> ArrState { get; set; }
    }
}