﻿using VSW.Lib.Models;
using VSW.Lib.MVC;

namespace VSW.Lib.CPControllers
{
    [CPModuleInfo(Name = "Thống kê truy cập",
        Description = "Quản lý - Thống kê truy cập",
        Code = "ModStatistic",
        Access = 1,
        Order = 4,
        ShowInMenu = true,
        CssClass = "icon-16-article")]
    public class ModStatisticController : CPController
    {
        public ModStatisticController()
        {
            //khoi tao Service
            DataService = ModStatisticService.Instance;
            CheckPermissions = true;
        }

        public void ActionIndex(ModStatisticModel model)
        {
            //sap xep tu dong
            var orderBy = AutoSort(model.Sort);

            //tao danh sach
            var dbQuery = ModStatisticService.Instance.CreateQuery()
                                .Take(model.PageSize)
                                .OrderByDesc(o => o.View)
                                .Skip(model.PageIndex * model.PageSize);

            ViewBag.Data = dbQuery.ToList();
            model.TotalRecord = dbQuery.TotalRecord;
            ViewBag.Model = model;
        }
    }

    public class ModStatisticModel : DefaultModel
    {
    }
}