﻿using System;
using Reddevil.Lib.Global;
using Reddevil.Lib.Models;
using Reddevil.Lib.MVC;

namespace Reddevil.Lib.CPControllers
{
    [CPModuleInfo(Name = "Thành viên",
        Description = "Quản lý - Thành viên",
        Code = "ModUser",
        Access = 29,
        Order = 8,
        ShowInMenu = false,
        CssClass = "icon-16-component")]
    public class ModUserController : CPController
    {
        public ModUserController()
        {
            //khoi tao Service
            DataService = ModUserService.Instance;
            //CheckPermissions = false;
        }

        public void ActionIndex(ModUserModel model)
        {
            //sap xep tu dong
            string orderBy = AutoSort(model.Sort);


            //tao danh sach
            var dbQuery = ModUserService.Instance.CreateQuery()
                                .Where(!string.IsNullOrEmpty(model.SearchText), o => o.LoginName.Contains(model.SearchText))
                                .Take(model.PageSize)
                                .OrderBy(orderBy)
                                .Skip(model.PageIndex * model.PageSize);

            ViewBag.Data = dbQuery.ToList();
            model.TotalRecord = dbQuery.TotalRecord;
            ViewBag.Model = model;
        }

        public void ActionAdd(ModUserModel model)
        {
            if (model.RecordID > 0)
            {
                _item = ModUserService.Instance.GetByID(model.RecordID);

                //khoi tao gia tri mac dinh khi update
            }
            else
            {
                _item = new ModUserEntity();

                //khoi tao gia tri mac dinh khi insert
            }

            ViewBag.Data = _item;
            ViewBag.Model = model;
        }

        public void ActionSave(ModUserModel model)
        {
            if (ValidSave(model))
                SaveRedirect();
        }

        public void ActionApply(ModUserModel model)
        {
            if (ValidSave(model))
                ApplyRedirect(model.RecordID, _item.ID);
        }

        public void ActionSaveNew(ModUserModel model)
        {
            if (ValidSave(model))
                SaveNewRedirect(model.RecordID, _item.ID);
        }

        #region private func

        private ModUserEntity _item = null;
        private bool ValidSave(ModUserModel model)
        {
            TryUpdateModel(_item);

            ViewBag.Data = _item;
            ViewBag.Model = model;

            CPViewPage.Message.MessageType = Message.MessageTypeEnum.Error;

            //kiem tra ten dang nhap
            if (!Utils.IsEmailAddress(_item.Email))
                CPViewPage.Message.ListMessage.Add("Nhập email đăng nhập.");
            else if (model.RecordID < 1 && ModUserService.Instance.GetByEmail(_item.Email.Trim()) != null)
                CPViewPage.Message.ListMessage.Add("Email đã tồn tại. Chọn email khác.");


            if (CPViewPage.Message.ListMessage.Count == 0)
            {
                //if (model.Password2 != string.Empty)
                //    _item.Password = Security.Md5(model.Password2);

                try
                {
                    //save
                    ModUserService.Instance.Save(_item);
                }
                catch (Exception ex)
                {
                    Global.Error.Write(ex);
                    CPViewPage.Message.ListMessage.Add(ex.Message);
                    return false;
                }

                return true;
            }

            return false;
        }

        #endregion
    }

    public class ModUserModel : DefaultModel
    {
        private int _LangID = 1;
        public int LangID
        {
            get { return _LangID; }
            set { _LangID = value; }
        }

        public string SearchText { get; set; }
        public string Password2 { get; set; }
    }
}
