﻿using Reddevil.Lib.Models;
using Reddevil.Lib.MVC;
using System;

namespace Reddevil.Lib.CPControllers
{
    [CPModuleInfo(Name = "Phản hồi",
        Description = "Quản lý - Phản hồi",
        Code = "ModFeedback",
        Access = 9,
        Order = 11,
        ShowInMenu = true,
        CssClass = "icon-16-massmail", Partitioning = 1)]
    public class ModFeedbackController : CPController
    {
        public ModFeedbackController()
        {
            //khoi tao Service
            DataService = ModFeedbackService.Instance;
            CheckPermissions = true;
        }

        public void ActionIndex(ModFeedbackModel model)
        {
            //sap xep tu dong
            string orderBy = AutoSort(model.Sort);

            //tao danh sach
            var dbQuery = ModFeedbackService.Instance.CreateQuery()
                                    .Where(model.StartDate > DateTime.MinValue, o => o.Created >= model.StartDate)
                                    .Where(model.EndDate > DateTime.MinValue, o => o.Created <= model.EndDate)
                                    .Take(model.PageSize)
                                    .OrderBy(orderBy)
                                    .Skip(model.PageIndex * model.PageSize);

            ViewBag.Data = dbQuery.ToList();
            model.TotalRecord = dbQuery.TotalRecord;
            ViewBag.Model = model;
        }

        public void ActionAdd(ModFeedbackModel model)
        {
            _item = model.RecordID > 0 ? ModFeedbackService.Instance.GetByID(model.RecordID) : new ModFeedbackEntity();

            ViewBag.Data = _item;
            ViewBag.Model = model;
        }

        #region private func

        private ModFeedbackEntity _item = null;

        #endregion private func
    }

    public class ModFeedbackModel : DefaultModel
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}