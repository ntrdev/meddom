﻿using System;
using Reddevil.Lib.Global;
using Reddevil.Lib.Models;
using Reddevil.Lib.MVC;

namespace Reddevil.Lib.CPControllers
{
    [CPModuleInfo(Name = "Thông tin liên hệ",
      Description = "Quản lý - Thông tin liên hệ",
      Code = "ModResource",
      Access = 31,
      Order = 4,
      ShowInMenu = true,
      CssClass = "icon-16-article", Partitioning = 3)]
    public class ModResourceController : CPController
    {

        public ModResourceController()
        {
            //khoi tao Service
            DataService = WebResourceService.Instance;
            CheckPermissions = true;
        }

        public void ActionIndex(ModResourceModel model)
        {
            //sap xep tu dong
            var orderBy = AutoSort(model.Sort, "[Code]");

            //tao danh sach
            var dbQuery = WebResourceService.Instance.CreateQuery()
                                .Where(o => o.LangID == model.LangID)
                                .Where(!string.IsNullOrEmpty(model.SearchText), o => o.Code.Contains(model.SearchText))
                                .Take(model.PageSize)
                                .OrderBy(orderBy)
                                .Skip(model.PageIndex * model.PageSize);

            ViewBag.Data = dbQuery.ToList();
            model.TotalRecord = dbQuery.TotalRecord;
            ViewBag.Model = model;
        }

        public void ActionUpload(ModResourceModel model)
        {
            CPViewPage.Script("Redirect", "REDDEVILRedirect('Import')");
        }

        public void ActionImport(ModResourceModel model)
        {
            ViewBag.Model = model;
        }

        public void ActionAdd(ModResourceModel model)
        {
            _item = model.RecordID > 0 ? WebResourceService.Instance.GetByID(model.RecordID) : new WebResourceEntity { LangID = model.LangID };

            ViewBag.Data = _item;
            ViewBag.Model = model;
        }

        public void ActionSave(ModResourceModel model)
        {
            if (ValidSave(model))
                SaveRedirect();
        }

        public void ActionApply(ModResourceModel model)
        {
            if (ValidSave(model))
                ApplyRedirect(model.RecordID, _item.ID);
        }

        public void ActionSaveNew(ModResourceModel model)
        {
            if (ValidSave(model))
                SaveNewRedirect(model.RecordID, _item.ID);
        }

        public override void ActionCancel()
        {
            CPViewPage.Response.Redirect(CPViewPage.Request.RawUrl.Replace("Add.aspx", "Index.aspx")
                .Replace("Import.aspx", "Index.aspx"));
        }

        #region private func

        private WebResourceEntity _item;

        private bool ValidSave(ModResourceModel model)
        {
            if (!string.IsNullOrEmpty(model.Resource))
            {
                var ArrItem = model.Resource.Split('\n');
                foreach (var t in ArrItem)
                {
                    if (string.IsNullOrEmpty(t.Trim()) || t.StartsWith("//"))
                        continue;

                    var index = t.IndexOf('=');
                    if (index < 0)
                        continue;

                    var key = t.Substring(0, index).Trim();
                    var value = t.Substring(index + 1).Trim();

                    _item = WebResourceService.Instance.CreateQuery()
                                                .Where(o => o.LangID == model.LangID && o.Code == key)
                                                .ToSingle();

                    if (_item == null)
                        _item = new WebResourceEntity { ID = 0, LangID = model.LangID, Code = key };

                    _item.Value = value;

                    WebResourceService.Instance.Save(_item);
                }

                return true;
            }
            else
            {
                TryUpdateModel(_item);

                ViewBag.Data = _item;
                ViewBag.Model = model;

                CPViewPage.Message.MessageType = Message.MessageTypeEnum.Error;

                //kiem tra ma
                if (_item.Code.Trim() == string.Empty)
                    CPViewPage.Message.ListMessage.Add("Nhập mã.");

                //kiem tra ton tai
                if (model.RecordID < 1 && WebResourceService.Instance.CP_HasExists(_item.Code, _item.LangID))
                    CPViewPage.Message.ListMessage.Add("Mã đã tồn tại.");

                if (CPViewPage.Message.ListMessage.Count != 0) return false;

                try
                {
                    //save
                    WebResourceService.Instance.Save(_item);
                }
                catch (Exception ex)
                {
                    Error.Write(ex);
                    CPViewPage.Message.ListMessage.Add(ex.Message);
                    return false;
                }

                return true;
            }
        }
        
        #endregion private func
    }

    public class ModResourceModel : DefaultModel
    {
        private int _langID = 1;

        public int LangID
        {
            get => _langID;
            set => _langID = value;
        }

        public string SearchText { get; set; }
        public string Resource { get; set; }
    }
}