﻿using System;
using System.Web.Script.Serialization;
using VSW.Lib.Global;
using VSW.Lib.Models;
using VSW.Lib.MVC;

namespace VSW.Lib.CPControllers
{
    [CPModuleInfo(Name = "Remarketing",
        Description = "Quản lý - Remarketing",
        Code = "ModRemarketing",
        Access = 31,
        Order = 4,
        ShowInMenu = true,
        CssClass = "icon-16-article")]
    public class ModRemarketingController : CPController
    {
        public ModRemarketingController()
        {
            //khoi tao Service
            DataService = ModRemarketingService.Instance;
            DataEntity = new ModRemarketingEntity();
            CheckPermissions = true;
        }

        public void ActionIndex(ModRemarketingModel model)
        {
            //sap xep tu dong
            var orderBy = AutoSort(model.Sort);

            //tao danh sach
            var dbQuery = ModRemarketingService.Instance.CreateQuery()
                                .Where(!string.IsNullOrEmpty(model.SearchText), o => (o.Name.Contains(model.SearchText) || o.Code.Contains(model.SearchText)))
                                .Take(model.PageSize)
                                .OrderBy(orderBy)
                                .Skip(model.PageIndex * model.PageSize);

            ViewBag.Data = dbQuery.ToList();
            model.TotalRecord = dbQuery.TotalRecord;
            ViewBag.Model = model;
        }

        public void ActionAdd(ModRemarketingModel model)
        {
            if (model.RecordID > 0)
            {
                _item = ModRemarketingService.Instance.GetByID(model.RecordID);
            }
            else
            {
                _item = new ModRemarketingEntity
                {
                    Published = DateTime.Now,
                    Activity = CPViewPage.UserPermissions.Approve
                };

                //khoi tao gia tri mac dinh khi insert
                var json = Cookies.GetValue(DataService.ToString(), true);
                if (!string.IsNullOrEmpty(json))
                    _item = new JavaScriptSerializer().Deserialize<ModRemarketingEntity>(json);
            }

            ViewBag.Data = _item;
            ViewBag.Model = model;
        }

        public void ActionSave(ModRemarketingModel model)
        {
            if (ValidSave(model))
                SaveRedirect();
        }

        public void ActionApply(ModRemarketingModel model)
        {
            if (ValidSave(model))
                ApplyRedirect(model.RecordID, _item.ID);
        }

        public void ActionSaveNew(ModRemarketingModel model)
        {
            if (ValidSave(model))
                SaveNewRedirect(model.RecordID, _item.ID);
        }

        #region private func

        private ModRemarketingEntity _item;

        private bool ValidSave(ModRemarketingModel model)
        {
            TryUpdateModel(_item);

            //chong hack
            _item.ID = model.RecordID;

            ViewBag.Data = _item;
            ViewBag.Model = model;

            CPViewPage.Message.MessageType = Message.MessageTypeEnum.Error;

            //kiem tra quyen han
            if ((model.RecordID < 1 && !CPViewPage.UserPermissions.Add) || (model.RecordID > 0 && !CPViewPage.UserPermissions.Edit))
                CPViewPage.Message.ListMessage.Add("Quyền hạn chế.");

            //kiem tra ten
            if (_item.Name.Trim() == string.Empty)
                CPViewPage.Message.ListMessage.Add("Nhập tiêu đề.");

            if (_item.Code.Trim() == string.Empty)
                CPViewPage.Message.ListMessage.Add("Chọn loại mã.");

            if (_item.Content.Trim() == string.Empty)
                CPViewPage.Message.ListMessage.Add("Nhập nội dung mã.");

            if (CPViewPage.Message.ListMessage.Count != 0) return false;

            if (string.IsNullOrEmpty(_item.Code)) _item.Code = Data.GetCode(_item.Name);

            try
            {
                //save
                ModRemarketingService.Instance.Save(_item);
            }
            catch (Exception ex)
            {
                Error.Write(ex);
                CPViewPage.Message.ListMessage.Add(ex.Message);
                return false;
            }

            return true;
        }

        #endregion private func
    }

    public class ModRemarketingModel : DefaultModel
    {
        public int LangID { get; set; } = 1;

        public string SearchText { get; set; }
    }
}