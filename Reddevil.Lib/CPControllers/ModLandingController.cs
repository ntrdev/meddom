﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using VSW.Lib.Global;
using VSW.Lib.Models;
using VSW.Lib.MVC;

namespace VSW.Lib.CPControllers
{
    [CPModuleInfo(Name = "Landing Page",
    Description = "Quản lý  - Landing Page",
    Code = "ModLanding",
    Access = 31,
    Order = 6,
    ShowInMenu = true,
    CssClass = "icon-16-article")]
    public class ModLandingController : CPController
    {
        public ModLandingController()
        {
            //khoi tao Service
            DataService = ModLandingService.Instance;
            CheckPermissions = true;
        }

        public void ActionIndex(ModLandingModel model)
        {
            // sap xep tu dong
            var orderBy = AutoSort(model.Sort);

            // tao danh sach
            var dbQuery = ModLandingService.Instance.CreateQuery()
                                .Where(!string.IsNullOrEmpty(model.SearchText), o => o.Name.Contains(model.SearchText))
                                .Where(model.State > 0, o => (o.State & model.State) == model.State)
                                .Take(model.PageSize)
                                .OrderBy(orderBy)
                                .Skip(model.PageIndex * model.PageSize);

            ViewBag.Data = dbQuery.ToList();
            model.TotalRecord = dbQuery.TotalRecord;
            ViewBag.Model = model;
        }

        public void ActionAdd(ModLandingModel model)
        {
            if (model.RecordID > 0)
            {
                _item = ModLandingService.Instance.GetByID(model.RecordID);

                // khoi tao gia tri mac dinh khi update
            }
            else
            {
                _item = new ModLandingEntity
                {
                    MenuID = model.MenuID,
                    Published = DateTime.Now,
                    Activity = CPViewPage.UserPermissions.Approve,
                    Order = GetMaxOrder()
                };

                // khoi tao gia tri mac dinh khi insert
            }

            ViewBag.Data = _item;
            ViewBag.Model = model;
        }

        public void ActionSave(ModLandingModel model)
        {
            if (ValidSave(model))
                SaveRedirect();
        }

        public void ActionApply(ModLandingModel model)
        {
            if (ValidSave(model))
                ApplyRedirect(model.RecordID, _item.ID);
        }

        public void ActionSaveNew(ModLandingModel model)
        {
            if (ValidSave(model))
                SaveNewRedirect(model.RecordID, _item.ID);
        }

        #region private func

        private ModLandingEntity _item = null;

        private bool ValidSave(ModLandingModel model)
        {
            TryUpdateModel(_item);

            //chong hack
            _item.ID = model.RecordID;

            ViewBag.Data = _item;
            ViewBag.Model = model;

            CPViewPage.Message.MessageType = Message.MessageTypeEnum.Error;

            //kiem tra quyen han
            if ((model.RecordID < 1 && !CPViewPage.UserPermissions.Add) || (model.RecordID > 0 && !CPViewPage.UserPermissions.Edit))
                CPViewPage.Message.ListMessage.Add("Quyền hạn chế.");

            //kiem tra ten
            if (_item.Name.Trim() == string.Empty)
                CPViewPage.Message.ListMessage.Add("Nhập tên landing.");

            if (CPViewPage.Message.ListMessage.Count != 0) return false;

            //neu khong nhap code -> tu sinh
            if (_item.Code.Trim() == string.Empty)
                _item.Code = Data.GetCode(_item.Name).ToLower();

            //cap nhat state
            _item.State = GetState(model.ArrState);

            try
            {
                //save
                ModLandingService.Instance.Save(_item);

                //update url
                ModCleanURLService.Instance.InsertOrUpdate(_item.Code, "Landing", _item.ID, _item.MenuID, model.LangID);
            }
            catch (Exception ex)
            {
                Error.Write(ex);
                CPViewPage.Message.ListMessage.Add(ex.Message);
                return false;
            }

            return true;
        }

        private static int GetMaxOrder()
        {
            return ModLandingService.Instance.CreateQuery()
                    .Max(o => o.Order)
                    .ToValue().ToInt(0) + 1;
        }

        private static string FetchImg(ModLandingEntity item, string html)
        {
            const string regex = @"<img[^>]*?src\s*=\s*[""']?([^'"" >]+?)[ '""][^>]*?>";
            foreach (Match m in Regex.Matches(html, regex, RegexOptions.IgnoreCase | RegexOptions.Singleline))
            {
                var value = m.Groups[1].Value;
                if (!value.StartsWith("/Data/upload/landing/") && !value.StartsWith("http"))
                    html = html.Replace(value, "/Data/upload/landing/landing-" + item.ID + "/" + value);
            }

            return html;
        }

        private static string FetchCss(ModLandingEntity item, string html)
        {
            const string regex = @"<link[^>]*?href\s*=\s*[""']?([^'"" >]+?)[ '""][^>]*?>";
            foreach (Match m in Regex.Matches(html, regex, RegexOptions.IgnoreCase | RegexOptions.Singleline))
            {
                var value = m.Groups[1].Value;
                if (!value.StartsWith("/Data/upload/landing/") && !value.StartsWith("http"))
                    html = html.Replace(value, "/Data/upload/landing/landing-" + item.ID + "/" + value);
            }

            return html;
        }

        private static string FetchJs(ModLandingEntity item, string html)
        {
            const string regex = @"<script[^>]*?src\s*=\s*[""']?([^'"" >]+?)[ '""][^>]*?>";
            foreach (Match m in Regex.Matches(html, regex, RegexOptions.IgnoreCase | RegexOptions.Singleline))
            {
                var value = m.Groups[1].Value;
                if (!value.StartsWith("/Data/upload/landing/") && !value.StartsWith("http"))
                    html = html.Replace(value, "/Data/upload/landing/landing-" + item.ID + "/" + value);
            }

            return html;
        }

        #endregion private func
    }

    public class ModLandingModel : DefaultModel
    {
        public int LangID { get; set; } = 1;
        public int MenuID { get; set; }
        public int State { get; set; }
        public string SearchText { get; set; }

        public List<int> ArrState { get; set; }
    }
}