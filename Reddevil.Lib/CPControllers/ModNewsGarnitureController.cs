﻿using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using CoreMr.Reddevil.Web;
using Reddevil.Lib.Global;
using Reddevil.Lib.Models;
using Reddevil.Lib.MVC;

namespace Reddevil.Lib.CPControllers
{
    [CPModuleInfo(Name = "Trưng bày",
        Description = "Quản lý - Tin Trưng bày",
        Code = "ModNewsGarniture",
        Access = 31,
        Order = 4,
        ShowInMenu = true,
        CssClass = "icon-16-article", Partitioning = 1)]
    public class ModNewsGarnitureController : CPController
    {
        public ModNewsGarnitureController()
        {
            //khoi tao Service
            DataService = ModNewsGarnitureService.Instance;
            DataEntity = new ModNewsGarnitureEntity();
            CheckPermissions = true;
        }

        public void ActionIndex(ModNewsGarnitureModel model)
        {
            //sap xep tu dong
            var orderBy = AutoSort(model.Sort);

            //tao danh sach
            var dbQuery = ModNewsGarnitureService.Instance.CreateQuery()
                                // .Where(o => o.Status == false)
                                .Where(!string.IsNullOrEmpty(model.SearchText), o => (o.Name.Contains(model.SearchText) || o.Url.Contains(model.SearchText)))
                                .Where(model.State > 0, o => (o.State & model.State) == model.State)
                                .Where(model.Status > 0, o => o.Condition == model.Status)
                                .WhereIn(o => o.MenuID, WebMenuService.Instance.GetChildIDForCP("NewsGarniture", model.MenuID, model.LangID))
                                .Take(model.PageSize)
                                .OrderBy(orderBy)
                                .Skip(model.PageIndex * model.PageSize);

            ViewBag.Data = dbQuery.ToList();
            model.TotalRecord = dbQuery.TotalRecord;
            ViewBag.Model = model;
        }

        public void ActionAdd(ModNewsGarnitureModel model)
        {
            if (model.RecordID > 0)
            {
                _item = ModNewsGarnitureService.Instance.GetByID(model.RecordID);

                FakeContent = !string.IsNullOrEmpty(_item.Summary) ? _item.Summary + " " + _item.Content : _item.Content;

                FakeContent = Data.ConvertISO8859EntityNameToUnicode(FakeContent);
                FakeContent = Data.StripCommentsTag(FakeContent);
                FakeContent = Data.StripScriptTags(FakeContent);
                FakeContent = Data.StripStyleTags(FakeContent);
                FakeContent = Data.StripTagsHTML("a|noscript|span|font|embed|p|em|strong|u|h2|li|img|st1|i|sup|hr", FakeContent);
                FakeContent = Data.StripAttributesTags("class|id|rel|onload|onclick|data-field|data-font|data-show-faces|data-width|data-send|em", FakeContent);
                FakeContent = Data.StripWhiteSpace(FakeContent);
                FakeContent = FakeContent.Replace("<meta charset=\"utf-8\" />", "");
                //khoi tao gia tri mac dinh khi update
                // if (_item.Updated <= DateTime.MinValue) _item.Updated = DateTime.Now;
            }
            else
            {
                _item = new ModNewsGarnitureEntity
                {
                    MenuID = model.MenuID,
                    Published = DateTime.Now,
                    Updated = DateTime.Now,
                    Order = GetMaxOrder(),
                    Activity = false,
                    AdminEdit = CPLogin.CurrentUser.LoginName,
                    Condition = 1
                };

                //khoi tao gia tri mac dinh khi insert
                var json = Cookies.GetValue(DataService.ToString(), true);
                if (!string.IsNullOrEmpty(json))
                    _item = new JavaScriptSerializer().Deserialize<ModNewsGarnitureEntity>(json);
            }

            ViewBag.Data = _item;
            ViewBag.Model = model;
        }

        public void ActionSave(ModNewsGarnitureModel model)
        {
            if (ValidSave(model))
                SaveRedirect();
        }

        public void ActionApply(ModNewsGarnitureModel model)
        {
            if (model.ChangeID < 1)
            {
                if (ValidSave(model))
                    ApplyRedirect(model.RecordID, _item.ID);
            }
            else
            {
                if (ValidSave(model))
                    SaveRedirect();
            }
        }

        public void ActionSaveNew(ModNewsGarnitureModel model)
        {
            if (ValidSave(model))
                SaveNewRedirect(model.RecordID, _item.ID);
        }

        public override void ActionDelete(int[] arrID)
        {
            if (CheckPermissions && !CPViewPage.UserPermissions.Delete)
            {
                //thong bao
                CPViewPage.Message.MessageType = Message.MessageTypeEnum.Error;
                CPViewPage.Message.ListMessage.Add("Bạn không có quyền xóa.");
                return;
            }
            else if (arrID.Length > 1 && CheckPermissions && CPViewPage.UserPermissions.Delete)
            {

                for (int i = 0; i < arrID.Length; i++)
                {
                    var _News = ModNewsGarnitureService.Instance.GetByID(arrID[i]);
                    if (_News.Condition == 2 && (!CPViewPage.CurrentUser.IsAdministrator || !CPViewPage.UserPermissions.Full))
                    {
                        CPViewPage.Message.ListMessage.Add("Tin " + _News.Name + " đang chờ duyệt không thể xóa.");
                    }
                    else if (_News.AdminEdit != CPViewPage.CurrentUser.LoginName && !CPViewPage.CurrentUser.IsAdministrator && !CPViewPage.UserPermissions.Full)
                    {
                        CPViewPage.Message.ListMessage.Add("Tin " + _News.Name + " không phải của bạn, bạn không có quyền xóa.");
                    }
                    else
                    {
                        ModCleanURLService.Instance.Delete("[Value]=" + _News.ID + " AND [Type]='NewsGarniture'");
                        ModNewsGarnitureService.Instance.Delete(_News.ID);
                    }

                }
                if (CPViewPage.Message.ListMessage.Count > 0)
                {
                    CPViewPage.Message.MessageType = Message.MessageTypeEnum.Error; return;
                }
            }
            else if (CheckPermissions && CPViewPage.UserPermissions.Full && CPViewPage.CurrentUser.IsAdministrator)
            {

                //xoa cleanurl
                ModCleanURLService.Instance.Delete("[Value] IN (" + CoreMr.Reddevil.Global.Array.ToString(arrID) + ") AND [Type]='NewsGarniture'");

                //xoa News
                DataService.Delete("[ID] IN (" + CoreMr.Reddevil.Global.Array.ToString(arrID) + ")");

                CPUserLogService.Instance.Save(new CPUserLogEntity
                {
                    Module = "Tin trưng bày",
                    Note = "Đã xóa",
                    UserID = CPLogin.UserID,
                    Created = DateTime.Now,
                    IP = CoreMr.Reddevil.Web.HttpRequest.IP,
                    TypeAction = "Đã xóa bài viết"
                });


                //thong bao
                CPViewPage.SetMessage("Đã xóa thành công.");

            }
            CPViewPage.RefreshPage();
        }



        #region private func

        private ModNewsGarnitureEntity _item;
        private string FakeContent { get; set; }
        private bool ValidSave(ModNewsGarnitureModel model)
        {
            if (!CPViewPage.CurrentUser.IsAdministrator || !CPViewPage.UserPermissions.Full)
            {
                if (CPViewPage.UserPermissions.Edit && CPViewPage.UserPermissions.Add && _item.AdminEdit != CPViewPage.CurrentUser.LoginName)
                {
                    CPViewPage.Message.MessageType = Message.MessageTypeEnum.Error;
                    CPViewPage.Message.ListMessage.Add("Tin này không phải của bạn, bạn không có quyền sửa.");
                    return false;
                }
                else
                {
                    if (_item.Activity && _item.Condition == 0)
                    {
                        CPViewPage.Message.MessageType = Message.MessageTypeEnum.Error;
                        CPViewPage.Message.ListMessage.Add("Tin đã được duyệt, không có quyền sửa.");
                        return false;
                    }
                    else if (_item.Condition == 2)
                    {
                        CPViewPage.Message.MessageType = Message.MessageTypeEnum.Error;
                        CPViewPage.Message.ListMessage.Add("Tin đang chờ duyệt, không có quyền sửa.");
                        return false;
                    }
                }
            }

            TryUpdateModel(_item);

            //chong hack
            _item.ID = model.RecordID;

            ViewBag.Data = _item;
            ViewBag.Model = model;

            CPViewPage.Message.MessageType = Message.MessageTypeEnum.Error;

            //kiem tra quyen han
            if ((model.RecordID < 1 && !CPViewPage.UserPermissions.Add) || (model.RecordID > 0 && !CPViewPage.UserPermissions.Edit))
                CPViewPage.Message.ListMessage.Add("Quyền hạn chế.");

            //kiem tra ten
            if (_item.Name.Trim() == string.Empty)
                CPViewPage.Message.ListMessage.Add("Nhập tiêu đề.");

            if (ModCleanURLService.Instance.CheckUrl(_item.Url, "NewsGarniture", _item.ID, model.LangID))
                CPViewPage.Message.ListMessage.Add("Mã đã tồn tại. Vui lòng chọn mã khác.");
            if (string.IsNullOrEmpty(_item.File)) CPViewPage.Message.ListMessage.Add("Bạn chưa chọn ảnh đại diện cho bài viết.");

            //kiem tra chuyen muc
            if (_item.MenuID < 1)
                CPViewPage.Message.ListMessage.Add("Chọn chuyên mục.");

            if (CPViewPage.Message.ListMessage.Count != 0) return false;

            if (string.IsNullOrEmpty(_item.Url)) _item.Url = Data.GetCode(_item.Name) + "-" + Data.GetFirstChar(_item.Name);

            //cap nhat state
            _item.State = GetState(model.ArrState);

            try
            {
                if (_item.Condition == 3)
                    _item.Activity = false;

                if (_item.Activity)
                {
                    _item.Condition = 0; _item.AdminApprove = CPLogin.CurrentUser.LoginName;
                } //trạng thái đã duyệt
                else
                {
                    _item.Updated = DateTime.Now;
                }

                if (model.ChangeID < 1)
                {

                    if (string.IsNullOrEmpty(_item.QrCode))
                        _item.QrCode = Image.GenQrCode("http://meddom.org/" + _item.Url + "/", Data.GetFirstChar(_item.Name) + "-" + _item.MenuID);
                    //save
                    ModNewsGarnitureService.Instance.Save(_item);

                    string text = !string.IsNullOrEmpty(_item.Summary) ? _item.Summary + " " + _item.Content : _item.Content;

                    text = Data.ConvertISO8859EntityNameToUnicode(text);
                    text = Data.StripCommentsTag(text);
                    text = Data.StripScriptTags(text);
                    text = Data.StripStyleTags(text);
                    text = Data.StripTagsHTML("a|noscript|span|font|embed|p|em|strong|u|h2|li|img|st1|i|sup|hr", text);
                    text = Data.StripAttributesTags("class|id|rel|onload|onclick|data-field|data-font|data-show-faces|data-width|data-send|em", text);
                    text = Data.StripWhiteSpace(text);
                    text = text.Replace("<meta charset=\"utf-8\" />", "");

                    if (!string.Equals(text, FakeContent))
                    {
                        text = Data.ReplaceEnquote(text);
                        TextToSpeech.SaveFileAudio(text, _item.ID);
                    }


                    //update url
                    ModCleanURLService.Instance.InsertOrUpdate(_item.Url, "NewsGarniture", _item.ID, _item.MenuID, model.LangID);

                    Cache.Clear("Mod_SearchAll_" + DateTime.Now.Month);
                    CPUserLogService.Instance.Save(new CPUserLogEntity
                    {
                        Module = "Tin trưng bày",
                        ObjectID = _item.ID,
                        Note = model.RecordID > 0 ? "Đã cập nhật tin " + _item.Name : "Thêm mới tin trưng bày" + _item.Name,
                        UserID = CPLogin.UserID,
                        Created = DateTime.Now,
                        IP = CoreMr.Reddevil.Web.HttpRequest.IP,
                        TypeAction = model.RecordID > 0 ? "Đã cập nhật bài viết" + _item.Name : "Thêm mới tin trưng bày",
                    });

                }
                else
                {
                    var getCate = WebMenuService.Instance.GetByID_Cache(model.ChangeID);
                    if (getCate != null)
                    {
                        switch (getCate.Type)
                        {
                            case "NewsEducation"://chuyển sang tin giáo dục
                                if (_item.ID > 0)
                                {
                                    var _newsGar = new ModNewsEducationEntity();
                                    _newsGar.Activity = _item.Activity;
                                    _newsGar.AdminApprove = _item.AdminApprove;
                                    _newsGar.AdminEdit = _item.AdminEdit;
                                    _newsGar.Condition = _item.Condition;
                                    _newsGar.Content = _item.Content;
                                    _newsGar.Cost = _item.Cost;
                                    _newsGar.Custom = _item.Custom;
                                    _newsGar.File = _item.File;
                                    _newsGar.MenuID = getCate.ID;
                                    _newsGar.Name = _item.Name;
                                    _newsGar.Order = ModNewsEducationService.Instance.CreateQuery()
                                                                    .Max(o => o.Order)
                                                                    .ToValue().ToInt(0) + 1;
                                    _newsGar.Published = _item.Published;
                                    _newsGar.PageDescription = _item.PageDescription;
                                    _newsGar.PageHeading = _item.PageHeading;
                                    _newsGar.PageKeywords = _item.PageKeywords;
                                    _newsGar.PageTitle = _item.PageTitle;
                                    _newsGar.State = _item.State;
                                    _newsGar.Status = _item.Status;
                                    _newsGar.Summary = _item.Summary;
                                    _newsGar.Updated = _item.Updated;
                                    _newsGar.Url = Data.GetCode(_newsGar.Name) + "-" + Data.GetFirstChar(_item.Name);
                                    _newsGar.View = _item.View;
                                    ModNewsEducationService.Instance.Save(_newsGar);

                                    //update url
                                    ModCleanURLService.Instance.InsertOrUpdate(_newsGar.Url, getCate.Type, _newsGar.ID, _newsGar.MenuID, model.LangID);

                                    ModCleanURLService.Instance.Delete("[Value]=" + _item.ID + " AND [Type]='NewsGarniture'");
                                    ModNewsGarnitureService.Instance.Delete(_item.ID);

                                }
                                else
                                {
                                    var _newsGar = new ModNewsEducationEntity();
                                    _newsGar.Activity = _item.Activity;
                                    _newsGar.AdminApprove = _item.AdminApprove;
                                    _newsGar.AdminEdit = _item.AdminEdit;
                                    _newsGar.Condition = _item.Condition;
                                    _newsGar.Content = _item.Content;
                                    _newsGar.Cost = _item.Cost;
                                    _newsGar.Custom = _item.Custom;
                                    _newsGar.File = _item.File;
                                    _newsGar.MenuID = getCate.ID;
                                    _newsGar.Name = _item.Name;
                                    _newsGar.Order = ModNewsEducationService.Instance.CreateQuery()
                                                                    .Max(o => o.Order)
                                                                    .ToValue().ToInt(0) + 1;
                                    _newsGar.Published = _item.Published;
                                    _newsGar.PageDescription = _item.PageDescription;
                                    _newsGar.PageHeading = _item.PageHeading;
                                    _newsGar.PageKeywords = _item.PageKeywords;
                                    _newsGar.PageTitle = _item.PageTitle;
                                    _newsGar.State = _item.State;
                                    _newsGar.Status = _item.Status;
                                    _newsGar.Summary = _item.Summary;
                                    _newsGar.Updated = _item.Updated;
                                    _newsGar.Url = Data.GetCode(_newsGar.Name) + "-" + Data.GetFirstChar(_item.Name);
                                    _newsGar.View = _item.View;
                                    ModNewsEducationService.Instance.Save(_newsGar);

                                    //update url
                                    ModCleanURLService.Instance.InsertOrUpdate(_newsGar.Url, getCate.Type, _newsGar.ID, _newsGar.MenuID, model.LangID);
                                }
                                break;
                            case "News"://chuyển sang tin tức
                                if (_item.ID > 0)
                                {
                                    var _news = new ModNewsEntity();
                                    _news.Activity = _item.Activity;
                                    _news.AdminApprove = _item.AdminApprove;
                                    _news.AdminEdit = _item.AdminEdit;
                                    _news.Condition = _item.Condition;
                                    _news.Content = _item.Content;
                                    _news.Cost = _item.Cost;
                                    _news.Custom = _item.Custom;
                                    _news.File = _item.File;
                                    _news.MenuID = getCate.ID;
                                    _news.Name = _item.Name;
                                    _news.Order = ModNewsService.Instance.CreateQuery()
                                                                    .Max(o => o.Order)
                                                                    .ToValue().ToInt(0) + 1;
                                    _news.Published = _item.Published;
                                    _news.PageDescription = _item.PageDescription;
                                    _news.PageHeading = _item.PageHeading;
                                    _news.PageKeywords = _item.PageKeywords;
                                    _news.PageTitle = _item.PageTitle;
                                    _news.State = _item.State;
                                    _news.Status = _item.Status;
                                    _news.Summary = _item.Summary;
                                    _news.Updated = _item.Updated;
                                    _news.Url = Data.GetCode(_news.Name) + "-" + Data.GetFirstChar(_item.Name);
                                    _news.View = _item.View;
                                    ModNewsService.Instance.Save(_news);

                                    //update url
                                    ModCleanURLService.Instance.InsertOrUpdate(_news.Url, getCate.Type, _news.ID, _news.MenuID, model.LangID);

                                    ModCleanURLService.Instance.Delete("[Value]=" + _item.ID + " AND [Type]='NewsGarniture'");
                                    ModNewsGarnitureService.Instance.Delete(_item.ID);

                                }
                                else
                                {
                                    var _news = new ModNewsEntity();
                                    _news.Activity = _item.Activity;
                                    _news.AdminApprove = _item.AdminApprove;
                                    _news.AdminEdit = _item.AdminEdit;
                                    _news.Condition = _item.Condition;
                                    _news.Content = _item.Content;
                                    _news.Cost = _item.Cost;
                                    _news.Custom = _item.Custom;
                                    _news.File = _item.File;
                                    _news.MenuID = getCate.ID;
                                    _news.Name = _item.Name;
                                    _news.Order = ModNewsService.Instance.CreateQuery()
                                                                    .Max(o => o.Order)
                                                                    .ToValue().ToInt(0) + 1;
                                    _news.Published = _item.Published;
                                    _news.PageDescription = _item.PageDescription;
                                    _news.PageHeading = _item.PageHeading;
                                    _news.PageKeywords = _item.PageKeywords;
                                    _news.PageTitle = _item.PageTitle;
                                    _news.State = _item.State;
                                    _news.Status = _item.Status;
                                    _news.Summary = _item.Summary;
                                    _news.Updated = _item.Updated;
                                    _news.Url = Data.GetCode(_news.Name) + "-" + Data.GetFirstChar(_item.Name);
                                    _news.View = _item.View;
                                    ModNewsService.Instance.Save(_news);

                                    //update url
                                    ModCleanURLService.Instance.InsertOrUpdate(_news.Url, getCate.Type, _news.ID, _news.MenuID, model.LangID);
                                }
                                break;
                            case "NewsResearch"://chuyển sang tin nghiên cứu
                                if (_item.ID > 0)
                                {
                                    var _newsRe = new ModNewsResearchEntity();
                                    _newsRe.Activity = _item.Activity;
                                    _newsRe.AdminApprove = _item.AdminApprove;
                                    _newsRe.AdminEdit = _item.AdminEdit;
                                    _newsRe.Condition = _item.Condition;
                                    _newsRe.Content = _item.Content;
                                    _newsRe.Cost = _item.Cost;
                                    _newsRe.Custom = _item.Custom;
                                    _newsRe.File = _item.File;
                                    _newsRe.MenuID = getCate.ID;
                                    _newsRe.Name = _item.Name;
                                    _newsRe.Order = ModNewsResearchService.Instance.CreateQuery()
                                                                    .Max(o => o.Order)
                                                                    .ToValue().ToInt(0) + 1;
                                    _newsRe.Published = _item.Published;
                                    _newsRe.PageDescription = _item.PageDescription;
                                    _newsRe.PageHeading = _item.PageHeading;
                                    _newsRe.PageKeywords = _item.PageKeywords;
                                    _newsRe.PageTitle = _item.PageTitle;
                                    _newsRe.State = _item.State;
                                    _newsRe.Summary = _item.Summary;
                                    _newsRe.Updated = _item.Updated;
                                    _newsRe.Url = Data.GetCode(_newsRe.Name) + "-" + Data.GetFirstChar(_item.Name);
                                    _newsRe.View = _item.View;
                                    ModNewsResearchService.Instance.Save(_newsRe);

                                    //update url
                                    ModCleanURLService.Instance.InsertOrUpdate(_newsRe.Url, getCate.Type, _newsRe.ID, _newsRe.MenuID, model.LangID);
                                    ModCleanURLService.Instance.Delete("[Value]=" + _item.ID + " AND [Type]='NewsGarniture'");
                                    ModNewsGarnitureService.Instance.Delete(_item.ID);

                                }
                                else
                                {
                                    var _newsRe = new ModNewsResearchEntity();
                                    _newsRe.Activity = _item.Activity;
                                    _newsRe.AdminApprove = _item.AdminApprove;
                                    _newsRe.AdminEdit = _item.AdminEdit;
                                    _newsRe.Condition = _item.Condition;
                                    _newsRe.Content = _item.Content;
                                    _newsRe.Cost = _item.Cost;
                                    _newsRe.Custom = _item.Custom;
                                    _newsRe.File = _item.File;
                                    _newsRe.MenuID = getCate.ID;
                                    _newsRe.Name = _item.Name;
                                    _newsRe.Order = ModNewsResearchService.Instance.CreateQuery()
                                                                    .Max(o => o.Order)
                                                                    .ToValue().ToInt(0) + 1;
                                    _newsRe.Published = _item.Published;
                                    _newsRe.PageDescription = _item.PageDescription;
                                    _newsRe.PageHeading = _item.PageHeading;
                                    _newsRe.PageKeywords = _item.PageKeywords;
                                    _newsRe.PageTitle = _item.PageTitle;
                                    _newsRe.State = _item.State;
                                    _newsRe.Summary = _item.Summary;
                                    _newsRe.Updated = _item.Updated;
                                    _newsRe.Url = Data.GetCode(_newsRe.Name) + "-" + Data.GetFirstChar(_item.Name);
                                    _newsRe.View = _item.View;
                                    ModNewsResearchService.Instance.Save(_newsRe);

                                    //update url
                                    ModCleanURLService.Instance.InsertOrUpdate(_newsRe.Url, getCate.Type, _newsRe.ID, _newsRe.MenuID, model.LangID);
                                }
                                break;
                            case "NewsVideo"://chuyển sang tin video
                                if (_item.ID > 0)
                                {
                                    var _newsVideo = new ModNewsVideoEntity();
                                    _newsVideo.Activity = _item.Activity;
                                    _newsVideo.AdminApprove = _item.AdminApprove;
                                    _newsVideo.AdminEdit = _item.AdminEdit;
                                    _newsVideo.Condition = _item.Condition;
                                    _newsVideo.Content = _item.Content;
                                    _newsVideo.Cost = _item.Cost;
                                    _newsVideo.Custom = _item.Custom;
                                    _newsVideo.File = _item.File;
                                    _newsVideo.MenuID = getCate.ID;
                                    _newsVideo.Name = _item.Name;
                                    _newsVideo.Order = ModNewsResearchService.Instance.CreateQuery()
                                                                    .Max(o => o.Order)
                                                                    .ToValue().ToInt(0) + 1;
                                    _newsVideo.Published = _item.Published;
                                    _newsVideo.PageDescription = _item.PageDescription;
                                    _newsVideo.PageHeading = _item.PageHeading;
                                    _newsVideo.PageKeywords = _item.PageKeywords;
                                    _newsVideo.PageTitle = _item.PageTitle;
                                    _newsVideo.State = _item.State;
                                    _newsVideo.Summary = _item.Summary;
                                    _newsVideo.Updated = _item.Updated;
                                    _newsVideo.Url = Data.GetCode(_newsVideo.Name) + "-" + Data.GetFirstChar(_item.Name);
                                    _newsVideo.View = _item.View;
                                    ModNewsVideoService.Instance.Save(_newsVideo);

                                    //update url
                                    ModCleanURLService.Instance.InsertOrUpdate(_newsVideo.Url, getCate.Type, _newsVideo.ID, _newsVideo.MenuID, model.LangID);
                                    ModCleanURLService.Instance.Delete("[Value]=" + _item.ID + " AND [Type]='NewsGarniture'");
                                    ModNewsGarnitureService.Instance.Delete(_item.ID);

                                }
                                else
                                {
                                    var _newsVideo = new ModNewsVideoEntity();
                                    _newsVideo.Activity = _item.Activity;
                                    _newsVideo.AdminApprove = _item.AdminApprove;
                                    _newsVideo.AdminEdit = _item.AdminEdit;
                                    _newsVideo.Condition = _item.Condition;
                                    _newsVideo.Content = _item.Content;
                                    _newsVideo.Cost = _item.Cost;
                                    _newsVideo.Custom = _item.Custom;
                                    _newsVideo.File = _item.File;
                                    _newsVideo.MenuID = getCate.ID;
                                    _newsVideo.Name = _item.Name;
                                    _newsVideo.Order = ModNewsVideoService.Instance.CreateQuery()
                                                                    .Max(o => o.Order)
                                                                    .ToValue().ToInt(0) + 1;
                                    _newsVideo.Published = _item.Published;
                                    _newsVideo.PageDescription = _item.PageDescription;
                                    _newsVideo.PageHeading = _item.PageHeading;
                                    _newsVideo.PageKeywords = _item.PageKeywords;
                                    _newsVideo.PageTitle = _item.PageTitle;
                                    _newsVideo.State = _item.State;
                                    _newsVideo.Summary = _item.Summary;
                                    _newsVideo.Updated = _item.Updated;
                                    _newsVideo.Url = Data.GetCode(_newsVideo.Name) + "-" + Data.GetFirstChar(_item.Name);
                                    _newsVideo.View = _item.View;
                                    ModNewsVideoService.Instance.Save(_newsVideo);

                                    //update url
                                    ModCleanURLService.Instance.InsertOrUpdate(_newsVideo.Url, getCate.Type, _newsVideo.ID, _newsVideo.MenuID, model.LangID);
                                }
                                break;
                            case "NewsMedia"://chuyển sang tin Media
                                if (_item.ID > 0)
                                {
                                    var _newsMedia = new ModNewsMediaEntity();
                                    _newsMedia.Activity = _item.Activity;
                                    _newsMedia.AdminApprove = _item.AdminApprove;
                                    _newsMedia.AdminEdit = _item.AdminEdit;
                                    _newsMedia.Condition = _item.Condition;
                                    _newsMedia.Content = _item.Content;
                                    _newsMedia.Cost = _item.Cost;
                                    _newsMedia.Custom = _item.Custom;
                                    _newsMedia.File = _item.File;
                                    _newsMedia.MenuID = getCate.ID;
                                    _newsMedia.Name = _item.Name;
                                    _newsMedia.Order = ModNewsMediaService.Instance.CreateQuery()
                                                                    .Max(o => o.Order)
                                                                    .ToValue().ToInt(0) + 1;
                                    _newsMedia.Published = _item.Published;
                                    _newsMedia.PageDescription = _item.PageDescription;
                                    _newsMedia.PageHeading = _item.PageHeading;
                                    _newsMedia.PageKeywords = _item.PageKeywords;
                                    _newsMedia.PageTitle = _item.PageTitle;
                                    _newsMedia.State = _item.State;
                                    _newsMedia.Summary = _item.Summary;
                                    _newsMedia.Updated = _item.Updated;
                                    _newsMedia.Url = Data.GetCode(_newsMedia.Name) + "-" + Data.GetFirstChar(_item.Name);
                                    _newsMedia.View = _item.View;
                                    ModNewsMediaService.Instance.Save(_newsMedia);

                                    //update url
                                    ModCleanURLService.Instance.InsertOrUpdate(_newsMedia.Url, getCate.Type, _newsMedia.ID, _newsMedia.MenuID, model.LangID);
                                    ModCleanURLService.Instance.Delete("[Value]=" + _item.ID + " AND [Type]='NewsGarniture'");
                                    ModNewsGarnitureService.Instance.Delete(_item.ID);

                                }
                                else
                                {

                                    var _newsMedia = new ModNewsMediaEntity();
                                    _newsMedia.Activity = _item.Activity;
                                    _newsMedia.AdminApprove = _item.AdminApprove;
                                    _newsMedia.AdminEdit = _item.AdminEdit;
                                    _newsMedia.Condition = _item.Condition;
                                    _newsMedia.Content = _item.Content;
                                    _newsMedia.Cost = _item.Cost;
                                    _newsMedia.Custom = _item.Custom;
                                    _newsMedia.File = _item.File;
                                    _newsMedia.MenuID = getCate.ID;
                                    _newsMedia.Name = _item.Name;
                                    _newsMedia.Order = ModNewsMediaService.Instance.CreateQuery()
                                                                    .Max(o => o.Order)
                                                                    .ToValue().ToInt(0) + 1;
                                    _newsMedia.Published = _item.Published;
                                    _newsMedia.PageDescription = _item.PageDescription;
                                    _newsMedia.PageHeading = _item.PageHeading;
                                    _newsMedia.PageKeywords = _item.PageKeywords;
                                    _newsMedia.PageTitle = _item.PageTitle;
                                    _newsMedia.State = _item.State;
                                    _newsMedia.Summary = _item.Summary;
                                    _newsMedia.Updated = _item.Updated;
                                    _newsMedia.Url = Data.GetCode(_newsMedia.Name) + "-" + Data.GetFirstChar(_item.Name);
                                    _newsMedia.View = _item.View;
                                    ModNewsMediaService.Instance.Save(_newsMedia);

                                    //update url
                                    ModCleanURLService.Instance.InsertOrUpdate(_newsMedia.Url, getCate.Type, _newsMedia.ID, _newsMedia.MenuID, model.LangID);
                                }
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Error.Write(ex);
                CPViewPage.Message.ListMessage.Add(ex.Message);
                return false;
            }

            return true;
        }

        private static int GetMaxOrder()
        {
            return ModNewsGarnitureService.Instance.CreateQuery()
                    .Max(o => o.Order)
                    .ToValue().ToInt(0) + 1;
        }

        #endregion private func
    }

    public class ModNewsGarnitureModel : DefaultModel
    {
        public int LangID { get; set; } = 1;

        public int MenuID { get; set; }
        public int State { get; set; }
        public string SearchText { get; set; }
        public int Status { get; set; }
        public List<int> ArrState { get; set; }
        public int ChangeID { get; set; }
    }
}