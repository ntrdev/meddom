﻿using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using CoreMr.Reddevil.Web;
using Reddevil.Lib.Global;
using Reddevil.Lib.Models;
using Reddevil.Lib.MVC;

namespace Reddevil.Lib.CPControllers
{
    [CPModuleInfo(Name = "Tổng quan",
        Description = "Quản lý - Tổng quan",
        Code = "ModNewsInfo",
        Access = 31,
        Order = 1,
        ShowInMenu = true,
        CssClass = "icon-16-article", Partitioning = 1)]
    public class ModNewsInfoController : CPController
    {
        public ModNewsInfoController()
        {
            //khoi tao Service
            DataService = ModNewsService.Instance;
            DataEntity = new ModNewsEntity();
            CheckPermissions = true;
        }

        public void ActionIndex(ModNewsInfoModel model)
        {
            //sap xep tu dong
            var orderBy = AutoSort(model.Sort);

            //tao danh sach
            var dbQuery = ModNewsService.Instance.CreateQuery()
                                .Where(o => o.Status == true)
                                .Where(model.MenuID > 0, o => o.MenuID == model.MenuID)
                                .Where(!string.IsNullOrEmpty(model.SearchText), o => (o.Name.Contains(model.SearchText) || o.Url.Contains(model.SearchText)))
                                .Where(model.State > 0, o => (o.State & model.State) == model.State)
                                .Where(model.Status > 0, o => o.Condition == model.Status)
                                .Take(model.PageSize)
                                .OrderBy(orderBy)
                                .Skip(model.PageIndex * model.PageSize);

            ViewBag.Data = dbQuery.ToList();
            model.TotalRecord = dbQuery.TotalRecord;
            ViewBag.Model = model;
        }

        public void ActionAdd(ModNewsInfoModel model)
        {
            if (model.RecordID > 0)
            {
                _item = ModNewsService.Instance.GetByID(model.RecordID);

                //khoi tao gia tri mac dinh khi update
             //   if (_item.Updated <= DateTime.MinValue) _item.Updated = DateTime.Now;
            }
            else
            {
                _item = new ModNewsEntity
                {
                    MenuID = model.MenuID,
                    Published = DateTime.Now,
                    Updated = DateTime.Now,
                    Order = GetMaxOrder(),
                    Activity = false,
                    AdminEdit = CPLogin.CurrentUser.LoginName,
                    Condition = 1,
                    Status = true
                };

                //khoi tao gia tri mac dinh khi insert
                var json = Cookies.GetValue(DataService.ToString(), true);
                if (!string.IsNullOrEmpty(json))
                    _item = new JavaScriptSerializer().Deserialize<ModNewsEntity>(json);
            }

            ViewBag.Data = _item;
            ViewBag.Model = model;
        }

        public void ActionSave(ModNewsInfoModel model)
        {
            if (ValidSave(model))
                SaveRedirect();
        }

        public void ActionApply(ModNewsInfoModel model)
        {
            if (ValidSave(model))
                ApplyRedirect(model.RecordID, _item.ID);
        }

        public void ActionSaveNew(ModNewsInfoModel model)
        {
            if (ValidSave(model))
                SaveNewRedirect(model.RecordID, _item.ID);
        }

        public override void ActionDelete(int[] arrID)
        {
            if (CheckPermissions && !CPViewPage.UserPermissions.Delete)
            {
                //thong bao
                CPViewPage.Message.MessageType = Message.MessageTypeEnum.Error;
                CPViewPage.Message.ListMessage.Add("Bạn không có quyền xóa.");
                return;
            }
            else if (arrID.Length > 0 && CheckPermissions && CPViewPage.UserPermissions.Delete)
            {

                for (int i = 0; i < arrID.Length; i++)
                {
                    var _News = ModNewsService.Instance.GetByID_Cache(arrID[i]);
                    if (_News.Condition == 2 && (!CPViewPage.CurrentUser.IsAdministrator || !CPViewPage.UserPermissions.Full))
                    {
                        CPViewPage.Message.ListMessage.Add("Tin " + _News.Name + " đang chờ duyệt không thể xóa.");
                    }
                    else if (_News.AdminEdit != CPViewPage.CurrentUser.LoginName && !CPViewPage.CurrentUser.IsAdministrator && !CPViewPage.UserPermissions.Full)
                    {
                        CPViewPage.Message.ListMessage.Add("Tin " + _News.Name + " không phải của bạn, bạn không có quyền xóa.");
                    }
                    else
                    {
                        ModCleanURLService.Instance.Delete("[Value]=" + _News.ID + " AND [Type]='News'");
                        ModNewsService.Instance.Delete(_News.ID);
                    }

                }
                if (CPViewPage.Message.ListMessage.Count > 0)
                {
                    CPViewPage.Message.MessageType = Message.MessageTypeEnum.Error; return;
                }
            }
            else if (CheckPermissions && CPViewPage.UserPermissions.Full && CPViewPage.CurrentUser.IsAdministrator)
            {

                //xoa cleanurl
                ModCleanURLService.Instance.Delete("[Value] IN (" + CoreMr.Reddevil.Global.Array.ToString(arrID) + ") AND [Type]='News'");

                //xoa News
                DataService.Delete("[ID] IN (" + CoreMr.Reddevil.Global.Array.ToString(arrID) + ")");

                CPUserLogService.Instance.Save(new CPUserLogEntity
                {
                    Module = "Tin Tức",
                    Note = "Đã xóa",
                    UserID = CPLogin.UserID,
                    Created = DateTime.Now,
                    IP = CoreMr.Reddevil.Web.HttpRequest.IP,
                    TypeAction = "Đã xóa bài viết"
                });

                //thong bao
                CPViewPage.SetMessage("Đã xóa thành công.");

            }
            CPViewPage.RefreshPage();
        }

        #region private func

        private ModNewsEntity _item;

        private bool ValidSave(ModNewsInfoModel model)
        {
            if (!CPViewPage.CurrentUser.IsAdministrator || !CPViewPage.UserPermissions.Edit)
            {
                if (CPViewPage.UserPermissions.Edit && CPViewPage.UserPermissions.Add && _item.AdminEdit != CPViewPage.CurrentUser.LoginName)
                {
                    CPViewPage.Message.MessageType = Message.MessageTypeEnum.Error;
                    CPViewPage.Message.ListMessage.Add("Tin này không phải của bạn, bạn không có quyền sửa.");
                    return false;
                }
                else
                {
                    if (_item.Activity && _item.Condition == 0)
                    {
                        CPViewPage.Message.MessageType = Message.MessageTypeEnum.Error;
                        CPViewPage.Message.ListMessage.Add("Tin đã được duyệt, không có quyền sửa.");
                        return false;
                    }
                    else if (_item.Condition == 2)
                    {
                        CPViewPage.Message.MessageType = Message.MessageTypeEnum.Error;
                        CPViewPage.Message.ListMessage.Add("Tin đang chờ duyệt, không có quyền sửa.");
                        return false;
                    }
                }
            }
            TryUpdateModel(_item);

            //chong hack
            _item.ID = model.RecordID;

            ViewBag.Data = _item;
            ViewBag.Model = model;

            CPViewPage.Message.MessageType = Message.MessageTypeEnum.Error;

            //kiem tra quyen han
            if ((model.RecordID < 1 && !CPViewPage.UserPermissions.Add) || (model.RecordID > 0 && !CPViewPage.UserPermissions.Edit))
                CPViewPage.Message.ListMessage.Add("Quyền hạn chế.");

            //kiem tra ten
            if (_item.Name.Trim() == string.Empty)
                CPViewPage.Message.ListMessage.Add("Nhập tiêu đề.");

            if (ModCleanURLService.Instance.CheckUrl(_item.Url, "News", _item.ID, model.LangID))
                CPViewPage.Message.ListMessage.Add("Mã đã tồn tại. Vui lòng chọn mã khác.");

            //kiem tra chuyen muc
            if (_item.MenuID < 1)
                CPViewPage.Message.ListMessage.Add("Chọn chuyên mục.");

            if (_item.Condition < 1 && !_item.Activity)
                CPViewPage.Message.ListMessage.Add("Chọn trạng thái.");

            //if (string.IsNullOrEmpty(_item.File)) CPViewPage.Message.ListMessage.Add("Bạn chưa chọn ảnh đại diện cho bài viết.");

            if (CPViewPage.Message.ListMessage.Count != 0) return false;

            if (string.IsNullOrEmpty(_item.Url)) _item.Url = Data.GetCode(_item.Name) + "-" + Data.GetFirstChar(_item.Name);

            //cap nhat state
            _item.State = GetState(model.ArrState);

            try
            {
                if (_item.Condition == 3)
                    _item.Activity = false;

                if (_item.Activity)
                {
                    _item.Condition = 0; _item.AdminApprove = CPLogin.CurrentUser.LoginName;
                } //trạng thái đã duyệt
                else
                {
                    _item.Updated = DateTime.Now;
                }
                //save
                ModNewsService.Instance.Save(_item);
                Cache.Clear("Mod_SearchAll_" + DateTime.Now.Month);
                //update url
                ModCleanURLService.Instance.InsertOrUpdate(_item.Url, "News", _item.ID, _item.MenuID, model.LangID);
                CPUserLogService.Instance.Save(new CPUserLogEntity
                {
                    Module = "Tin tổng quan",
                    ObjectID = _item.ID,
                    Note = model.RecordID > 0 ? "Đã cập nhật tin giới thiệu" : "Thêm mới tin",
                    UserID = CPLogin.UserID,
                    Created = DateTime.Now,
                    IP = CoreMr.Reddevil.Web.HttpRequest.IP,
                    TypeAction = model.RecordID > 0 ? "Đã cập nhật bài viết giới thiệu" + _item.Name : "Thêm mới tin",
                });
            }
            catch (Exception ex)
            {
                Error.Write(ex);
                CPViewPage.Message.ListMessage.Add(ex.Message);
                return false;
            }

            return true;
        }

        private static int GetMaxOrder()
        {
            return ModNewsService.Instance.CreateQuery()
                    .Max(o => o.Order)
                    .ToValue().ToInt(0) + 1;
        }

        #endregion private func
    }

    public class ModNewsInfoModel : DefaultModel
    {
        public int LangID { get; set; } = 1;
        public int Status { get; set; }
        public int MenuID { get; set; }
        public int State { get; set; }
        public string SearchText { get; set; }

        public List<int> ArrState { get; set; }
    }
}