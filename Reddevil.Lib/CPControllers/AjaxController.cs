﻿using System;
using System.Collections.Generic;
using CoreMr.Reddevil.Global;
using CoreMr.Reddevil.MVC;
using Newtonsoft.Json;
using Reddevil.Lib.Global;
using Reddevil.Lib.Global.ListItem;
using Reddevil.Lib.Models;
using Reddevil.Lib.MVC;
using Error = Reddevil.Lib.Global.Error;
using Setting = CoreMr.Reddevil.Web.Setting;

namespace Reddevil.Lib.CPControllers
{
    public class AjaxController : CPController
    {

        public void ActionIndex()
        {

        }

        public void ActionDeleteCache()
        {
            var json = new Json();

            try
            {
                Utils.Clear_Cache();
                json.Instance.Message = "Xóa cache thành công.";
            }
            catch
            {
                json.Instance.Message = "Có lỗi xảy ra. Vui lòng thử lại.";

            }

            json.Create();
        }

        public void ActionGetChild(GetChildModel model)
        {
            var json = new Json();

            var listItem = WebMenuService.Instance.GetByParentID_Cache(model.ParentID);

            json.Instance.Html = @"<option value=""0"" selected=""selected"">- chọn -</option>";
            for (var i = 0; listItem != null && i < listItem.Count; i++)
            {
                json.Instance.Html += @"<option value=""" + listItem[i].ID + @""" " + (listItem[i].ID == model.SelectedID ? @"selected=""selected""" : @"") + @">" + listItem[i].Name + @"</option>";
            }

            ViewBag.Model = model;

            json.Create();
        }

        public void ActionSiteGetPage(SiteGetPageModel model)
        {


            var json = new Json();

            var listPage = List.GetList(SysPageService.Instance, model.LangID);

            for (var i = 0; listPage != null && i < listPage.Count; i++)
            {
                json.Instance.Params += "<option " + (model.PageID.ToString() == listPage[i].Value ? "selected" : "") + " value='" + listPage[i].Value + "'>&nbsp; " + listPage[i].Name + "</option>";
            }

            json.Create();
        }

        public void ActionTemplateGetCustom(int templateID)
        {
            var json = new Json();

            json.Instance.Html = SysTemplateService.Instance.GetByID(templateID).Custom;

            json.Create();
        }

        public void ActionPageGetCustom(int pageID)
        {
            var json = new Json();

            json.Instance.Html = SysPageService.Instance.GetByID(pageID).Custom;

            json.Create();
        }

        public void ActionPageGetControl(PageGetControlModel model)
        {
            var json = new Json();
            try
            {
                if (!string.IsNullOrEmpty(model.ModuleCode))
                {
                    SysPageEntity currentPage = null;
                    var currentModule = SysModuleService.Instance.CoreMr_Reddevil_GetByCode(model.ModuleCode);

                    if (model.PageID > 0)
                        currentPage = SysPageService.Instance.GetByID(model.PageID);

                    if (currentModule != null)
                    {
                        var currentObject = new Class(currentModule.ModuleType);

                        var filePath = (System.IO.File.Exists(CPViewPage.Server.MapPath("~/Views/Design/" + currentModule.Code + ".ascx")) ?
                            "~/Views/Design/" + currentModule.Code + ".ascx" : "~/" + Setting.Sys_AdminDir + "/Design/EditModule.ascx");

                        var sHtml = CoreMr.Reddevil.Web.Utils.GetHtmlControl(CPViewPage, filePath,
                                            "CurrentObject", currentObject,
                                            "CurrentPage", currentPage,
                                            "CurrentModule", currentModule,
                                            "LangID", model.LangID);

                        if (currentObject.ExistsField("MenuID"))
                        {
                            var fieldInfo = currentObject.GetFieldInfo("MenuID");
                            var attributes = fieldInfo.GetCustomAttributes(typeof(PropertyInfo), true);
                            if (attributes.GetLength(0) > 0)
                            {
                                var propertyInfo = (PropertyInfo)attributes[0];
                                var listMenu = List.GetListByText(propertyInfo.Value.ToString());

                                var menuType = List.FindByName(listMenu, "Type").Value;

                                listMenu = List.GetList(WebMenuService.Instance, model.LangID, menuType);
                                listMenu.Insert(0, new Item(string.Empty, string.Empty));

                                for (var j = 1; j < listMenu.Count; j++)
                                {
                                    if (string.IsNullOrEmpty(listMenu[j].Name)) continue;

                                    json.Instance.Params += "<option " + (currentPage != null && currentPage.MenuID.ToString() == listMenu[j].Value ? "selected" : "") + " value='" + listMenu[j].Value + "'>&nbsp; " + listMenu[j].Name + "</option>";
                                }
                            }
                        }


                        json.Instance.Html = sHtml.Replace("{CPPath}", CPViewPage.CPPath);
                    }
                }
            }
            catch (Exception ex)
            {
                Error.Write(ex);
            }

            json.Create();
        }



        #region File Product

        public void ActionAddFile(FileModel model)
        {
            var json = new Json();

            if (string.IsNullOrEmpty(model.Name))
            {
                json.Instance.Params += "Nhập tên Đối tượng cần thêm ảnh trước.";
                ViewBag.Model = model;

                json.Create();
            }

            if (model.MenuID < 1)
            {
                json.Instance.Params += "Chọn chuyên mục Đối tượng cần thêm ảnh trước.";
                ViewBag.Model = model;

                json.Create();
            }

            if (model.ProductID < 1)
            {


                dynamic _obj = null;
                switch (model.Dyanamic)
                {

                    case "ModNews":
                        _obj = new ModNewsEntity()
                        {
                            ID = 0,
                            MenuID = model.MenuID,
                            Name = model.Name,
                            Url = Data.GetCode(model.Name),
                            Published = DateTime.Now,
                            Updated = DateTime.Now,
                            Activity = true,
                            AdminEdit = CPLogin.IsLogin() ? CPLogin.CurrentUser.LoginName : ""
                        };
                        ModNewsService.Instance.Save(_obj);
                        model.ProductID = _obj.ID;
                        break;
                    case "ModNewsMedia":
                        _obj = new ModNewsMediaEntity()
                        {
                            ID = 0,
                            MenuID = model.MenuID,
                            Name = model.Name,
                            Url = Data.GetCode(model.Name),
                            Published = DateTime.Now,
                            Updated = DateTime.Now,
                            Activity = true,
                            AdminEdit = CPLogin.IsLogin() ? CPLogin.CurrentUser.LoginName : ""
                        };
                        ModNewsMediaService.Instance.Save(_obj);
                        model.ProductID = _obj.ID;
                        break;
                    case "ModNewsEducation":
                        _obj = new ModNewsEducationEntity()
                        {
                            ID = 0,
                            MenuID = model.MenuID,
                            Name = model.Name,
                            Url = Data.GetCode(model.Name),
                            Published = DateTime.Now,
                            Updated = DateTime.Now,
                            Activity = true,
                            AdminEdit = CPLogin.IsLogin() ? CPLogin.CurrentUser.LoginName : ""
                        };
                        ModNewsEducationService.Instance.Save(_obj);
                        model.ProductID = _obj.ID;
                        break;
                    case "ModNewsResearch":
                        _obj = new ModNewsResearchEntity()
                        {
                            ID = 0,
                            MenuID = model.MenuID,
                            Name = model.Name,
                            Url = Data.GetCode(model.Name),
                            Published = DateTime.Now,
                            Updated = DateTime.Now,
                            Activity = true,
                            AdminEdit = CPLogin.IsLogin() ? CPLogin.CurrentUser.LoginName : ""
                        };
                        ModNewsResearchService.Instance.Save(_obj);
                        model.ProductID = _obj.ID;
                        break;
                    case "ModNewsGarniture":
                        _obj = new ModNewsGarnitureEntity()
                        {
                            ID = 0,
                            MenuID = model.MenuID,
                            Name = model.Name,
                            Url = Data.GetCode(model.Name),
                            Published = DateTime.Now,
                            Updated = DateTime.Now,
                            Activity = true,
                            AdminEdit = CPLogin.IsLogin() ? CPLogin.CurrentUser.LoginName : ""
                        };
                        ModNewsGarnitureService.Instance.Save(_obj);
                        model.ProductID = _obj.ID;
                        break;
                    case "ModNewsVideo":
                        _obj = new ModNewsVideoEntity()
                        {
                            ID = 0,
                            MenuID = model.MenuID,
                            Name = model.Name,
                            Url = Data.GetCode(model.Name),
                            Published = DateTime.Now,
                            Updated = DateTime.Now,
                            Activity = true,
                            AdminEdit = CPLogin.IsLogin() ? CPLogin.CurrentUser.LoginName : ""
                        };
                        ModNewsVideoService.Instance.Save(_obj);
                        model.ProductID = _obj.ID;
                        break;
                    case "ModPS":
                        _obj = new ModPSEntity()
                        {
                            ID = 0,
                            CategoryID = model.MenuID,
                            Name = model.Name,
                            Url = Data.GetCode(model.Name),
                            Activity = true,
                            Image = model.Image,
                            CPUser = CPLogin.IsLogin() ? CPLogin.CurrentUser.LoginName : ""
                        };
                        ModPSService.Instance.Save(_obj);
                        model.ProductID = _obj.ID;
                        break;
                }

                json.Instance.Js += model.ProductID;

            }

            if (ModDynamicFileService.Instance.Exists(model.ProductID, model.File, model.TypeID))
            {
                json.Instance.Params += "Ảnh đã tồn tại.";
                ViewBag.Model = model;

                json.Create();

                return;
            }

            ModDynamicFileService.Instance.Save(new ModDynamicFileEntity()
            {
                ID = 0,
                ObjectID = model.ProductID,
                File = model.File,
                Order = GetMaxDynamicFileOrder(model.ProductID, model.TypeID),
                Activity = true,
                Type = model.TypeID
            });

            var listItem = ModDynamicFileService.Instance.CreateQuery()
                                            .Where(o => o.Activity == true && o.ObjectID == model.ProductID && o.Type == model.TypeID)
                                            .OrderByAsc(o => o.Order)
                                            .ToList_Cache();

            for (var i = 0; listItem != null && i < listItem.Count; i++)
            {
                json.Instance.Html += @"<li>
                                            <img src=""" + Utils.DefaultImage(listItem[i].File) + @""" />

                                            <a href=""javascript:void(0)"" onclick=""deleteFile('" + Utils.DefaultImage(listItem[i].File) + @"','" + listItem[i].Type + @"')"" data-toggle=""tooltip"" data-placement=""bottom"" data-original-title=""Xóa""><i class=""fa fa-ban""></i></a>
                                            <a href=""javascript:void(0)"" onclick=""upFile('" + Utils.DefaultImage(listItem[i].File) + @"','" + listItem[i].Type + @"')"" data-toggle=""tooltip"" data-placement=""bottom"" data-original-title=""Chuyển lên trên""><i class=""fa fa-arrow-up""></i></a>
                                            <a href=""javascript:void(0)"" onclick=""downFile('" + Utils.DefaultImage(listItem[i].File) + @"','" + listItem[i].Type + @"')"" data-toggle=""tooltip"" data-placement=""bottom"" data-original-title=""Chuyển xuống dưới""><i class=""fa fa-arrow-down""></i></a>
                                        </li>";
            }

            ViewBag.Model = model;

            json.Create();
        }

        public void ActionDeleteFile(FileModel model)
        {
            var json = new Json();

            var item = ModDynamicFileService.Instance.CreateQuery()
                                        .Where(o => o.ObjectID == model.ProductID && o.File == model.File && o.Type == model.TypeID)
                                        .ToSingle();

            if (item != null)
                ModDynamicFileService.Instance.Delete(item);

            var listItem = ModDynamicFileService.Instance.CreateQuery()
                                            .Where(o => o.Activity == true && o.ObjectID == model.ProductID && o.Type == model.TypeID)
                                            .OrderByAsc(o => o.Order)
                                            .ToList_Cache();

            for (var i = 0; listItem != null && i < listItem.Count; i++)
            {
                json.Instance.Html += @"<li>
                                            <img src=""" + Utils.DefaultImage(listItem[i].File) + @""" />

                                            <a href=""javascript:void(0)"" onclick=""deleteFile('" + Utils.DefaultImage(listItem[i].File) + @"','" + listItem[i].Type + @"')"" data-toggle=""tooltip"" data-placement=""bottom"" data-original-title=""Xóa""><i class=""fa fa-ban""></i></a>
                                            <a href=""javascript:void(0)"" onclick=""upFile('" + Utils.DefaultImage(listItem[i].File) + @"','" + listItem[i].Type + @"')"" data-toggle=""tooltip"" data-placement=""bottom"" data-original-title=""Chuyển lên trên""><i class=""fa fa-arrow-up""></i></a>
                                            <a href=""javascript:void(0)"" onclick=""downFile('" + Utils.DefaultImage(listItem[i].File) + @"','" + listItem[i].Type + @"')"" data-toggle=""tooltip"" data-placement=""bottom"" data-original-title=""Chuyển xuống dưới""><i class=""fa fa-arrow-down""></i></a>
                                        </li>";
            }

            ViewBag.Model = model;

            json.Create();
        }

        public void ActionUpFile(FileModel model)
        {
            var json = new Json();

            if (model.ProductID < 1)
            {
                json.Instance.Params += "Đối tượng cần thêm ảnh không tồn tại.";
                ViewBag.Model = model;

                json.Create();
            }

            var item = ModDynamicFileService.Instance.CreateQuery()
                                        .Where(o => o.ObjectID == model.ProductID && o.File == model.File && o.Type == model.TypeID)
                                        .ToSingle();
            if (item == null)
            {
                json.Instance.Params += "Ảnh không tồn tại.";
                ViewBag.Model = model;

                json.Create();
            }

            var listItem = ModDynamicFileService.Instance.CreateQuery()
                                            .Where(o => o.Activity == true && o.ObjectID == model.ProductID && o.Type == model.TypeID)
                                            .OrderByAsc(o => o.Order)
                                            .ToList_Cache();

            var index = listItem.FindIndex(o => o.ID == item.ID);
            var indexPrev = index == 0 ? listItem.Count - 1 : (index - 1);

            var itemPrev = listItem[indexPrev];

            var order = item.Order;

            item.Order = itemPrev.Order;
            ModDynamicFileService.Instance.Save(item, o => o.Order);

            itemPrev.Order = order;
            ModDynamicFileService.Instance.Save(itemPrev, o => o.Order);

            listItem = ModDynamicFileService.Instance.CreateQuery()
                                            .Where(o => o.Activity == true && o.ObjectID == model.ProductID && o.Type == model.TypeID)
                                            .OrderByAsc(o => o.Order)
                                            .ToList_Cache();

            for (var i = 0; listItem != null && i < listItem.Count; i++)
            {
                json.Instance.Html += @"<li>
                                            <img src=""" + Utils.DefaultImage(listItem[i].File) + @""" />

                                            <a href=""javascript:void(0)"" onclick=""deleteFile('" + Utils.DefaultImage(listItem[i].File) + @"','" + listItem[i].Type + @"')"" data-toggle=""tooltip"" data-placement=""bottom"" data-original-title=""Xóa""><i class=""fa fa-ban""></i></a>
                                            <a href=""javascript:void(0)"" onclick=""upFile('" + Utils.DefaultImage(listItem[i].File) + @"','" + listItem[i].Type + @"')"" data-toggle=""tooltip"" data-placement=""bottom"" data-original-title=""Chuyển lên trên""><i class=""fa fa-arrow-up""></i></a>
                                            <a href=""javascript:void(0)"" onclick=""downFile('" + Utils.DefaultImage(listItem[i].File) + @"','" + listItem[i].Type + @"')"" data-toggle=""tooltip"" data-placement=""bottom"" data-original-title=""Chuyển xuống dưới""><i class=""fa fa-arrow-down""></i></a>
                                        </li>";
            }

            ViewBag.Model = model;

            json.Create();
        }

        public void ActionDownFile(FileModel model)
        {
            var json = new Json();

            if (model.ProductID < 1)
            {
                json.Instance.Params += "Đối tượng cần thêm ảnh không tồn tại.";
                ViewBag.Model = model;

                json.Create();
            }

            var item = ModDynamicFileService.Instance.CreateQuery()
                                        .Where(o => o.ObjectID == model.ProductID && o.File == model.File && o.Type == model.TypeID)
                                        .ToSingle();
            if (item == null)
            {
                json.Instance.Params += "Ảnh không tồn tại.";
                ViewBag.Model = model;

                json.Create();
            }

            var listItem = ModDynamicFileService.Instance.CreateQuery()
                                            .Where(o => o.Activity == true && o.ObjectID == model.ProductID && o.Type == model.TypeID)
                                            .OrderByAsc(o => o.Order)
                                            .ToList_Cache();

            var index = listItem.FindIndex(o => o.ID == item.ID);
            var indexNext = index == listItem.Count - 1 ? 0 : (index + 1);

            var itemNext = listItem[indexNext];

            var order = item.Order;

            item.Order = itemNext.Order;
            ModDynamicFileService.Instance.Save(item, o => o.Order);

            itemNext.Order = order;
            ModDynamicFileService.Instance.Save(itemNext, o => o.Order);

            listItem = ModDynamicFileService.Instance.CreateQuery()
                                            .Where(o => o.Activity == true && o.ObjectID == model.ProductID && o.Type == model.TypeID)
                                            .OrderByAsc(o => o.Order)
                                            .ToList_Cache();

            for (var i = 0; listItem != null && i < listItem.Count; i++)
            {
                json.Instance.Html += @"<li>
                                            <img src=""" + listItem[i].File + @""" />

                                            <a href=""javascript:void(0)"" onclick=""deleteFile('" + listItem[i].File + @"','" + listItem[i].Type + @"')"" data-toggle=""tooltip"" data-placement=""bottom"" data-original-title=""Xóa""><i class=""fa fa-ban""></i></a>
                                            <a href=""javascript:void(0)"" onclick=""upFile('" + listItem[i].File + @"','" + listItem[i].Type + @"')"" data-toggle=""tooltip"" data-placement=""bottom"" data-original-title=""Chuyển lên trên""><i class=""fa fa-arrow-up""></i></a>
                                            <a href=""javascript:void(0)"" onclick=""downFile('" + listItem[i].File + @"','" + listItem[i].Type + @"')"" data-toggle=""tooltip"" data-placement=""bottom"" data-original-title=""Chuyển xuống dưới""><i class=""fa fa-arrow-down""></i></a>
                                        </li>";
            }

            ViewBag.Model = model;

            json.Create();
        }

        #endregion File Product

        #region add tin liên quan

        public void ActionAddNews(NewsModel model)
        {
            var json = new Json();

            if (string.IsNullOrEmpty(model.Name))
            {
                json.Instance.Params += "Nhập tên Đối tượng cần thêm tin trước.";
                ViewBag.Model = model;

                json.Create();
            }

            if (model.MenuID < 1)
            {
                json.Instance.Params += "Chọn chuyên mục Đối tượng cần thêm tin trước.";
                ViewBag.Model = model;

                json.Create();
            }

            if (model.ParentID < 1)
            {


                dynamic _obj = new ModNewsEntity()
                {
                    ID = 0,
                    MenuID = model.MenuID,
                    Name = model.Name,
                    Url = Data.GetCode(model.Name),
                    Published = DateTime.Now,
                    Updated = DateTime.Now,
                    Activity = true
                };
                ModNewsService.Instance.Save(_obj);
                model.ParentID = _obj.ID;

                json.Instance.Js += model.ParentID;

            }

            if (ModNewsMutilService.Instance.Exists(model.ParentID, model.ID, model.Type))
            {
                json.Instance.Params += "Đã thêm tin này rồi.";
                ViewBag.Model = model;

                json.Create();

                return;
            }

            ModNewsMutilService.Instance.Save(new ModNewsMutilEntity()
            {

                NewsID = model.ID,
                ParentID = model.ParentID,
                TypeID = model.Type
            });

            var listItem = ModNewsMutilService.Instance.CreateQuery()
                                            .Where(o => o.ParentID == model.ParentID && o.TypeID == model.Type)
                                            .OrderByAsc(o => o.NewsID)
                                            .ToList_Cache();

            for (var i = 0; listItem != null && i < listItem.Count; i++)
            {
                dynamic _news = null;
                if (listItem[i].TypeID == 1)
                    _news = ModNewsService.Instance.CreateQuery().Select(o => new { o.File, o.Url, o.Name }).Where(o => o.ID == listItem[i].NewsID).ToSingle_Cache();
                else if (listItem[i].TypeID == 2)
                    _news = ModNewsEducationService.Instance.CreateQuery().Select(o => new { o.File, o.Url, o.Name }).Where(o => o.ID == listItem[i].NewsID).ToSingle_Cache();
                else if (listItem[i].TypeID == 3)
                    _news = ModNewsGarnitureService.Instance.CreateQuery().Select(o => new { o.File, o.Url, o.Name }).Where(o => o.ID == listItem[i].NewsID).ToSingle_Cache();
                else if (listItem[i].TypeID == 4)
                    _news = ModNewsResearchService.Instance.CreateQuery().Select(o => new { o.File, o.Url, o.Name }).Where(o => o.ID == listItem[i].NewsID).ToSingle_Cache();

                if (_news == null) continue;
                json.Instance.Html += @"<li>
                                            <a href=""" + CoreMr.Reddevil.Web.HttpRequest.Domain + _news.Url + @""" style=""width: 85%;"">" + _news.Name + @"</a>
                                            <a href=""javascript:void(0)"" onclick=""deleteNews('" + listItem[i].NewsID + @"','" + listItem[i].ParentID + @"','" + listItem[i].TypeID + @"')"" data-toggle=""tooltip"" data-placement=""bottom"" data-original-title=""Xóa""><i class=""fa fa-ban""></i></a>
                                           
                                        </li>";
            }

            ViewBag.Model = model;

            json.Create();
        }

        public void ActionDeleteNews(NewsModel model)
        {
            var json = new Json();

            ModNewsMutilService.Instance.Delete("[NewsID]=" + model.ID + " and [ParentID]=" + model.ParentID + " and [TypeID]=" + model.Type);

            var listItem = ModNewsMutilService.Instance.CreateQuery()
                                              .Where(o => o.ParentID == model.ParentID && o.TypeID == model.Type)
                                              .OrderByAsc(o => o.NewsID)
                                              .ToList_Cache();

            for (var i = 0; listItem != null && i < listItem.Count; i++)
            {
                dynamic _news = null;
                if (listItem[i].TypeID == 1)
                    _news = ModNewsService.Instance.CreateQuery().Select(o => new { o.File, o.Url, o.Name }).Where(o => o.ID == listItem[i].NewsID).ToSingle_Cache();
                else if (listItem[i].TypeID == 2)
                    _news = ModNewsEducationService.Instance.CreateQuery().Select(o => new { o.File, o.Url, o.Name }).Where(o => o.ID == listItem[i].NewsID).ToSingle_Cache();
                else if (listItem[i].TypeID == 3)
                    _news = ModNewsGarnitureService.Instance.CreateQuery().Select(o => new { o.File, o.Url, o.Name }).Where(o => o.ID == listItem[i].NewsID).ToSingle_Cache();
                else if (listItem[i].TypeID == 4)
                    _news = ModNewsResearchService.Instance.CreateQuery().Select(o => new { o.File, o.Url, o.Name }).Where(o => o.ID == listItem[i].NewsID).ToSingle_Cache();

                if (_news == null) continue;
                json.Instance.Html += @"<li>
                                            <a href=""" + CoreMr.Reddevil.Web.HttpRequest.Domain + _news.Url + @""" style=""width: 85%;"">" + _news.Name + @"</a>
                                            <a href=""javascript:void(0)"" onclick=""deleteNews('" + listItem[i].NewsID + @"','" + listItem[i].ParentID + @"','" + listItem[i].TypeID + @"')"" data-toggle=""tooltip"" data-placement=""bottom"" data-original-title=""Xóa""><i class=""fa fa-ban""></i></a>
                                        </li>";
            }

            ViewBag.Model = model;

            json.Create();
        }
        #endregion


        #region private


        private static int GetMaxDynamicFileOrder(int productID, int typeid)
        {
            return ModDynamicFileService.Instance.CreateQuery()
                    .Where(o => o.ObjectID == productID && o.Type == typeid)
                    .Max(o => o.Order)
                    .ToValue().ToInt(0) + 1;
        }



        #endregion private



        #region private func

        private void EndResponse()
        {
            var s = @"<Xml>
  <Item>
    <Html><![CDATA[" + ajaxModel.Html + @"]]></Html>
    <Params><![CDATA[" + ajaxModel.Params + @"]]></Params>
    <JS><![CDATA[" + ajaxModel.JS + @"]]></JS>
  </Item>
</Xml>";
            CPViewPage.Response.ContentType = "text/xml";
            CPViewPage.Response.Write(s);
            CPViewPage.Response.End();
        }

        private AjaxModel ajaxModel = new AjaxModel() { Params = string.Empty, Html = string.Empty, JS = string.Empty };

        #endregion private func
    }

    public class GetChildModel
    {
        public int ParentID { get; set; }
        public int SelectedID { get; set; }
    }

    public class PageGetControlModel
    {
        public int LangID { get; set; }
        public int PageID { get; set; }
        public string ModuleCode { get; set; }
    }

    public class SiteGetPageModel
    {
        public int LangID { get; set; }
        public int PageID { get; set; }
    }

    public class GetPropertiesModel
    {
        public int MenuID { get; set; }
        public int LangID { get; set; }
        public int ProductID { get; set; }
    }

    #region File
    public class NewsModel
    {
        public int ID { get; set; }
        public int ParentID { get; set; }
        public string Name { get; set; }
        public int MenuID { get; set; }
        public int Type { get; set; }
    }

    public class FileModel
    {
        public string Name { get; set; }
        public int MenuID { get; set; }
        public int TypeID { get; set; }
        public int ProductID { get; set; }
        public string File { get; set; }
        public string Image { get; set; }
        public string Dyanamic { get; set; }
    }

    #endregion File

    public class AjaxModel
    {
        public string Html { get; set; }
        public string Params { get; set; }
        public string JS { get; set; }
    }




}