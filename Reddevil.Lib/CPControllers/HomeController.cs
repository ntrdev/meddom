﻿using Reddevil.Lib.Global;
using Reddevil.Lib.MVC;

namespace Reddevil.Lib.CPControllers
{
    public class HomeController : CPController
    {
        public void ActionIndex()
        {
        }

        public void ActionLogout()
        {
            CPViewPage.SetLog("Thoát khỏi hệ thống.");

            CPLogin.Logout();

            CPViewPage.CPRedirect("Login.aspx");
        }
    }
}