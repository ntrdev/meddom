﻿using System;
using System.Web.Script.Serialization;
using Reddevil.Lib.Global;
using Reddevil.Lib.Models;
using Reddevil.Lib.MVC;

namespace Reddevil.Lib.CPControllers
{
    [CPModuleInfo(Name = "Mã giảm giá",
        Description = "Quản lý - Mã giảm giá",
        Code = "ModGiffCode",
        Access = 31,
        Order = 1,
        ShowInMenu = false,
        CssClass = "icon-16-article")]
    public class ModGiffCodeController : CPController
    {
        public ModGiffCodeController()
        {
            //khoi tao Service
            DataService = ModGiffCodeService.Instance;
            DataEntity = new ModGiffCodeEntity();

            CheckPermissions = true;
        }

        public void ActionIndex(ModGiffCodeModel model)
        {
            //sap xep tu dong
            var orderBy = AutoSort(model.Sort);

            var _Promotion = !string.IsNullOrEmpty(model.SearchText) ? Data.GetCode(model.SearchText) : string.Empty;
            //tao danh sach
            var dbQuery = ModGiffCodeService.Instance.CreateQuery()
                                    .Where(!string.IsNullOrEmpty(model.SearchText), o => (o.Name.Contains(model.SearchText) || o.Code.Contains(_Promotion) || o.CodeGenerate.Contains(_Promotion)))
                                    .Take(model.PageSize)
                                    .OrderBy(orderBy)
                                    .Skip(model.PageIndex * model.PageSize);

            ViewBag.Data = dbQuery.ToList_Cache();
            model.TotalRecord = dbQuery.TotalRecord;
            ViewBag.Model = model;

        }

        public void ActionAdd(ModGiffCodeModel model)
        {
            if (model.RecordID > 0)
            {
                _item = ModGiffCodeService.Instance.GetByID(model.RecordID);

            }
            else
            {
                //khoi tao gia tri mac dinh khi insert
                _item = new ModGiffCodeEntity
                {
                    StartDate = DateTime.Now,
                    Activity = CPViewPage.UserPermissions.Approve,
                    IsAdmin = true,
                    ShopID = 0,
                    CodeGenerate = Reddevil.Lib.Global.Random.GetRandom(8, true),
                    Created = DateTime.Now,
                    Updated = DateTime.Now,
                    Order = GetMaxGiffCodeOrder()
                };

                var json = Global.Cookies.GetValue(DataService.ToString(), true);
                if (!string.IsNullOrEmpty(json))
                    _item = new JavaScriptSerializer().Deserialize<ModGiffCodeEntity>(json);
            }

            ViewBag.Data = _item;
            ViewBag.Model = model;
        }

        public void ActionSave(ModGiffCodeModel model)
        {
            if (ValidSave(model))
                SaveRedirect();
        }

        public void ActionApply(ModGiffCodeModel model)
        {
            if (ValidSave(model))
                ApplyRedirect(model.RecordID, _item.ID);
        }

        public void ActionSaveNew(ModGiffCodeModel model)
        {
            if (ValidSave(model))
                SaveNewRedirect(model.RecordID, _item.ID);
        }

        public override void ActionDelete(int[] arrID)
        {
            ModCleanURLService.Instance.Delete("[Value] IN (" + CoreMr.Reddevil.Global.Array.ToString(arrID) + ") AND [Type]='MGiffCode'");
            //xoa Promotion
            DataService.Delete("[ID] IN (" + CoreMr.Reddevil.Global.Array.ToString(arrID) + ")");

            //thong bao
            CPViewPage.SetMessage("Đã xóa thành công.");
            CPViewPage.RefreshPage();
        }

        #region private func

        private ModGiffCodeEntity _item;
        private bool ValidSave(ModGiffCodeModel model)
        {
            TryUpdateModel(_item);

            //chong hack
            _item.ID = model.RecordID;

            ViewBag.Data = _item;
            ViewBag.Model = model;

            CPViewPage.Message.MessageType = Message.MessageTypeEnum.Error;

            //kiem tra quyen han
            if ((model.RecordID < 1 && !CPViewPage.UserPermissions.Add) || (model.RecordID > 0 && !CPViewPage.UserPermissions.Edit))
                CPViewPage.Message.ListMessage.Add("Quyền hạn chế.");

            //kiem tra ten
            if (_item.Name.Trim() == string.Empty)
                CPViewPage.Message.ListMessage.Add("Nhập tiêu đề.");


            if (CPViewPage.Message.ListMessage.Count != 0) return false;

            if (string.IsNullOrEmpty(_item.Code)) _item.Code = Data.GetCode(_item.Name) + "-" + Reddevil.Lib.Global.Random.GetRandom(3, false);


            try
            {
                if (string.IsNullOrEmpty(_item.CodeGenerate)) _item.CodeGenerate = Reddevil.Lib.Global.Random.GetRandom(8, true);
                _item.IsAdmin = true;

                //save
                ModGiffCodeService.Instance.Save(_item);
                //update url
                ModCleanURLService.Instance.InsertOrUpdate(_item.Code, "MGiffCode", _item.ID, _item.MenuID, model.LangID);

            }
            catch (Exception ex)
            {
                Error.Write(ex);
                CPViewPage.Message.ListMessage.Add(ex.Message);
                return false;
            }

            return true;
        }
        private static int GetMaxGiffCodeOrder()
        {
            return ModGiffCodeService.Instance.CreateQuery()
                    .Max(o => o.Order)
                    .ToValue().ToInt(0) + 1;
        }

        #endregion private func
    }

    public class ModGiffCodeModel : DefaultModel
    {
        private int _langID = 1;
        public int LangID
        {
            get { return _langID; }
            set { _langID = value; }
        }

        public string SearchText { get; set; }

    }
}