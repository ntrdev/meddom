﻿using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using Reddevil.Lib.Global;
using Reddevil.Lib.Models;
using Reddevil.Lib.MVC;

namespace Reddevil.Lib.CPControllers
{
    [CPModuleInfo(Name = "Affilate marketing",
        Description = "Quản lý - Affilate marketing",
        Code = "ModMarketing",
        Access = 31,
        Order = 4,
        ShowInMenu = false,
        CssClass = "icon-16-article")]
    public class ModMarketingController : CPController
    {
        public ModMarketingController()
        {
            //khoi tao Service
            DataService = ModMarketingService.Instance;
            DataEntity = new ModMarketingEntity();
            CheckPermissions = true;
        }

        public void ActionIndex(ModMarketingModel model)
        {
            //sap xep tu dong
            var orderBy = AutoSort(model.Sort);

            //tao danh sach
            var dbQuery = ModMarketingService.Instance.CreateQuery()
                                .Where(!string.IsNullOrEmpty(model.SearchText), o => (o.Name.Contains(model.SearchText) || o.Code.Contains(model.SearchText)))
                                //.Where(model.State > 0, o => (o.State & model.State) == model.State)
                                //.WhereIn(o => o.MenuID, WebMenuService.Instance.GetChildIDForCP("MarketingNews", model.MenuID, model.LangID))
                                .Where(model.StartDate > DateTime.MinValue, o => o.StartDate >= model.StartDate)
                                .Where(model.EndDate > DateTime.MinValue, o => o.EndDate <= model.EndDate)
                                .Take(model.PageSize)
                                .OrderBy(orderBy)
                                .Skip(model.PageIndex * model.PageSize);

            ViewBag.Data = dbQuery.ToList();
            model.TotalRecord = dbQuery.TotalRecord;
            ViewBag.Model = model;

        }

        public void ActionAdd(ModMarketingModel model)
        {
            if (model.RecordID > 0)
            {
                _item = ModMarketingService.Instance.GetByID(model.RecordID);

                //khoi tao gia tri mac dinh khi update
                if (_item.Updated <= DateTime.MinValue) _item.Updated = DateTime.Now;
            }
            else
            {
                _item = new ModMarketingEntity
                {
                    MenuID = model.MenuID,
                    Published = DateTime.Now,
                    Updated = DateTime.Now,
                    Order = GetMaxOrder(),
                    Activity = CPViewPage.UserPermissions.Approve
                };

                //khoi tao gia tri mac dinh khi insert
                var json = Global.Cookies.GetValue(DataService.ToString(), true);
                if (!string.IsNullOrEmpty(json))
                    _item = new JavaScriptSerializer().Deserialize<ModMarketingEntity>(json);
            }

            ViewBag.Data = _item;
            ViewBag.Model = model;
        }

        public void ActionSave(ModMarketingModel model)
        {
            if (ValidSave(model))
                SaveRedirect();
        }

        public void ActionApply(ModMarketingModel model)
        {
            if (ValidSave(model))
                ApplyRedirect(model.RecordID, _item.ID);
        }

        public void ActionSaveNew(ModMarketingModel model)
        {
            if (ValidSave(model))
                SaveNewRedirect(model.RecordID, _item.ID);
        }

        public override void ActionDelete(int[] arrID)
        {
            //xoa cleanurl
            ModCleanURLService.Instance.Delete("[Value] IN (" + CoreMr.Reddevil.Global.Array.ToString(arrID) + ") AND [Type]='Marketing'");

            //xoa MarketingNews
            DataService.Delete("[ID] IN (" + CoreMr.Reddevil.Global.Array.ToString(arrID) + ")");

            //thong bao
            CPViewPage.SetMessage("Đã xóa thành công.");
            CPViewPage.RefreshPage();
        }

        #region private func

        private ModMarketingEntity _item;

        private bool ValidSave(ModMarketingModel model)
        {
            TryUpdateModel(_item);

            //chong hack
            _item.ID = model.RecordID;

            ViewBag.Data = _item;
            ViewBag.Model = model;

            CPViewPage.Message.MessageType = Message.MessageTypeEnum.Error;

            //kiem tra quyen han
            if ((model.RecordID < 1 && !CPViewPage.UserPermissions.Add) || (model.RecordID > 0 && !CPViewPage.UserPermissions.Edit))
                CPViewPage.Message.ListMessage.Add("Quyền hạn chế.");

            //kiem tra ten
            if (_item.Name.Trim() == string.Empty)
                CPViewPage.Message.ListMessage.Add("Nhập tiêu đề.");

            if (ModCleanURLService.Instance.CheckUrl(_item.Code, "Marketing", _item.ID, model.LangID))
                CPViewPage.Message.ListMessage.Add("Mã đã tồn tại. Vui lòng chọn mã khác.");

            ////kiem tra chuyen muc
            //if (_item.MenuID < 1)
            //    CPViewPage.Message.ListMessage.Add("Chọn chuyên mục.");

            if (CPViewPage.Message.ListMessage.Count != 0) return false;

            if (string.IsNullOrEmpty(_item.Code)) _item.Code = Data.GetCode(_item.Name) + "-" + Reddevil.Lib.Global.Random.GetRandom(6, false);



            try
            {

              
                //save
                ModMarketingService.Instance.Save(_item);
                //if (model.UserID == null) ModAffilateUserService.Instance.Delete("[MaketingID] IN (" + _item.ID + ")");
                //if (model != null && model.UserID != null)
                //{
                //    ModAffilateUserService.Instance.Delete("[MaketingID] IN (" + _item.ID + ")");

                //    foreach (var item in model.UserID)
                //    {

                //        ModAffilateUserService.Instance.Save(new ModAffilateUserEntity
                //        {
                //            UserID = item,
                //            MarketingID = _item.ID,
                //            Created = DateTime.Now
                //        });


                //    }

                //}

                //update url
                ModCleanURLService.Instance.InsertOrUpdate(_item.Code, "Marketing", _item.ID, _item.MenuID, model.LangID);
            }
            catch (Exception ex)
            {
                Error.Write(ex);
                CPViewPage.Message.ListMessage.Add(ex.Message);
                return false;
            }

            return true;
        }

        private static int GetMaxOrder()
        {
            return ModMarketingService.Instance.CreateQuery()
                    .Max(o => o.Order)
                    .ToValue().ToInt(0) + 1;
        }

        #endregion private func
    }

    public class ModMarketingModel : DefaultModel
    {
        private int _langID = 1;
        public int LangID
        {
            get { return _langID; }
            set { _langID = value; }
        }
        public List<int> UserID { get; set; }
        public int MenuID { get; set; }
        public int State { get; set; }

        public DateTime EndDate { get; set; }

        public DateTime StartDate { get; set; }
        public string SearchText { get; set; }

    }
}