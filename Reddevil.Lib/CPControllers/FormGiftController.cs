﻿namespace VSW.Lib.CPControllers
{
    public class FormGiftController : ModGiftController
    {
        public override void ActionCopy(int id)
        {
        }

        public override void ActionPublish(int[] arrID)
        {
        }

        public override void ActionUnPublish(int[] arrID)
        {
        }

        public override void ActionDelete(int[] arrID)
        {
        }

        public override void ActionSaveOrder(int[] arrID)
        {
        }

        public override void ActionPublishGX(int[] arrID)
        {
        }

        public new void ActionSave(ModGiftModel model)
        {
        }

        public new void ActionApply(ModGiftModel model)
        {
        }
    }
}