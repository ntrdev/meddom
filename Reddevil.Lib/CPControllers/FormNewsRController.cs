﻿namespace Reddevil.Lib.CPControllers
{
    public class FormNewsRController : ModNewsResearchController
    {
        public override void ActionCopy(int id)
        {
        }

        public override void ActionPublish(int[] arrID)
        {
        }

        public override void ActionUnPublish(int[] arrID)
        {
        }

        public override void ActionDelete(int[] arrID)
        {
        }

        public override void ActionSaveOrder(int[] arrID)
        {
        }

        public override void ActionPublishGX(int[] arrID)
        {
        }

        public new void ActionSave(ModNewsModel model)
        {
        }

        public new void ActionApply(ModNewsModel model)
        {
        }
    }
}