﻿using System;
using Reddevil.Lib.Global;
using Reddevil.Lib.Models;
using Reddevil.Lib.MVC;
using System.Web.Script.Serialization;
using System.Collections.Generic;

namespace Reddevil.Lib.CPControllers
{
    [CPModuleInfo(Name = "Nhận báo giá",
        Description = "Quản lý - Nhận báo giá",
        Code = "ModSubscribe",
        Access = 31,
        Order = 9900,
        ShowInMenu = true,
        CssClass = "icon-16-massmail")]
    public class ModSubscribeController : CPController
    {
        public ModSubscribeController()
        {
            //khoi tao Service
            DataService = ModSubscribeService.Instance;
            CheckPermissions = true;
        }

        public void ActionIndex(ModSubscribeModel model)
        {
            // sap xep tu dong
            string orderBy = AutoSort(model.Sort);

            // tao danh sach
            var dbQuery = ModSubscribeService.Instance.CreateQuery()
                                .Where(!string.IsNullOrEmpty(model.SearchText), o => o.Name.Contains(model.SearchText) || o.Email.Contains(model.SearchText) || o.Phone.Contains(model.SearchText))
                                .Take(model.PageSize)
                                .OrderBy(orderBy)
                                .Skip(model.PageIndex * model.PageSize);

            ViewBag.Data = dbQuery.ToList();
            model.TotalRecord = dbQuery.TotalRecord;
            ViewBag.Model = model;
        }
        public void ActionAdd(ModSubscribeModel model)
        {
            if (model.RecordID > 0)
            {
                _item = ModSubscribeService.Instance.GetByID(model.RecordID);

                //khoi tao gia tri mac dinh khi update
                // if (_item.Updated <= DateTime.MinValue) _item.Updated = DateTime.Now;
            }
            else
            {
                //khoi tao gia tri mac dinh khi insert
                //_item = new ModShopEntity
                //{
                //    CityID = model.C,
                //    BrandID = model.BrandID,
                //    Published = DateTime.Now,
                //    Updated = DateTime.Now,
                //    Order = GetMaxOrder(),
                //    Activity = CPViewPage.UserPermissions.Approve
                //};

                //var json = Global.Cookies.GetValue(DataService.ToString(), true);
                //if (!string.IsNullOrEmpty(json))
                //    _item = new JavaScriptSerializer().Deserialize<ModShopEntity>(json);
            }

            ViewBag.Data = _item;
            ViewBag.Model = model;
        }

        public void ActionSave(ModSubscribeModel model)
        {
            if (ValidSave(model))
                SaveRedirect();
        }

        public void ActionApply(ModSubscribeModel model)
        {
            if (ValidSave(model))
                ApplyRedirect(model.RecordID, _item.ID);
        }

        #region private func

        private ModSubscribeEntity _item;
        private bool ValidSave(ModSubscribeModel model)
        {
            TryUpdateModel(_item);

            //chong hack
            _item.ID = model.RecordID;

            ViewBag.Data = _item;
            ViewBag.Model = model;

            CPViewPage.Message.MessageType = Message.MessageTypeEnum.Error;

            //kiem tra quyen han
            if ((model.RecordID < 1 && !CPViewPage.UserPermissions.Add) || (model.RecordID > 0 && !CPViewPage.UserPermissions.Edit))
                CPViewPage.Message.ListMessage.Add("Quyền hạn chế.");

            //kiem tra ten
            //if (_item.Name.Trim() == string.Empty)
            //    CPViewPage.Message.ListMessage.Add("Nhập tiêu đề.");

            //if (ModCleanURLService.Instance.CheckCode(_item.Code, "Shop", _item.ID, model.LangID))
            //    CPViewPage.Message.ListMessage.Add("Mã đã tồn tại. Vui lòng chọn mã khác.");

            //kiem tra chuyen muc
            //if (_item.MenuID < 1)
            //    CPViewPage.Message.ListMessage.Add("Chọn chuyên mục.");

            //kiem tra chuyen muc
            //if (_item.BrandID < 1)
            //    CPViewPage.Message.ListMessage.Add("Chọn thương hiệu.");

            if (CPViewPage.Message.ListMessage.Count != 0) return false;

            //if (string.IsNullOrEmpty(_item.Code)) _item.Code = Data.GetCode(_item.Name);

            //cap nhat state
            //  _item.State = GetState(model.ArrState);

            try
            {

                ModSubscribeService.Instance.Save(_item);

                //update url


            }
            catch (Exception ex)
            {
                Error.Write(ex);
                CPViewPage.Message.ListMessage.Add(ex.Message);
                return false;
            }

            return true;
        }



        #endregion private func
    }

    public class ModSubscribeModel : DefaultModel
    {
        public string SearchText { get; set; }
    }
}
