﻿using System;
using System.Collections.Generic;
using VSW.Lib.Global;
using VSW.Lib.Models;
using VSW.Lib.MVC;

namespace VSW.Lib.CPControllers
{
    [CPModuleInfo(Name = "Tracking",
        Description = "Quản lý - Tracking",
        Code = "ModTracking",
        Access = 31,
        Order = 4,
        ShowInMenu = true,
        CssClass = "icon-16-article")]
    public class ModTrackingController : CPController
    {
        public ModTrackingController()
        {
            //khoi tao Service
            DataService = ModTrackingService.Instance;
            CheckPermissions = true;
        }

        public void ActionIndex(ModTrackingModel model)
        {
            //sap xep tu dong
            var orderBy = AutoSort(model.Sort);

            //tao danh sach
            var dbQuery = ModTrackingService.Instance.CreateQuery()
                                .Where(!string.IsNullOrEmpty(model.SearchText), o => (o.Name.Contains(model.SearchText) || o.Code.Contains(model.SearchText)))
                                .Take(model.PageSize)
                                .OrderBy(orderBy)
                                .Skip(model.PageIndex * model.PageSize);

            ViewBag.Data = dbQuery.ToList();
            model.TotalRecord = dbQuery.TotalRecord;
            ViewBag.Model = model;
        }

        public void ActionAdd(ModTrackingModel model)
        {
            if (model.RecordID > 0)
            {
                _item = ModTrackingService.Instance.GetByID(model.RecordID);

                //khoi tao gia tri mac dinh khi update
            }
            else
            {
                _item = new ModTrackingEntity
                {
                    Activity = CPViewPage.UserPermissions.Approve
                };
            }

            ViewBag.Data = _item;
            ViewBag.Model = model;
        }

        public void ActionSave(ModTrackingModel model)
        {
            if (ValidSave(model))
                SaveRedirect();
        }

        public void ActionApply(ModTrackingModel model)
        {
            if (ValidSave(model))
                ApplyRedirect(model.RecordID, _item.ID);
        }

        public void ActionSaveNew(ModTrackingModel model)
        {
            if (ValidSave(model))
                SaveNewRedirect(model.RecordID, _item.ID);
        }

        #region private func

        private ModTrackingEntity _item;

        private bool ValidSave(ModTrackingModel model)
        {
            TryUpdateModel(_item);

            //chong hack
            _item.ID = model.RecordID;

            ViewBag.Data = _item;
            ViewBag.Model = model;

            CPViewPage.Message.MessageType = Message.MessageTypeEnum.Error;

            //kiem tra quyen han
            if ((model.RecordID < 1 && !CPViewPage.UserPermissions.Add) || (model.RecordID > 0 && !CPViewPage.UserPermissions.Edit))
                CPViewPage.Message.ListMessage.Add("Quyền hạn chế.");

            //kiem tra ten
            if (_item.Name.Trim() == string.Empty)
                CPViewPage.Message.ListMessage.Add("Nhập tiêu đề.");

            if (_item.Code.Trim() == string.Empty)
                CPViewPage.Message.ListMessage.Add("Nhập từ khóa URL.");

            if (_item.Content.Trim() == string.Empty)
                CPViewPage.Message.ListMessage.Add("Nhập mã tracking.");

            if (CPViewPage.Message.ListMessage.Count != 0) return false;

            //cap nhat state
            _item.State = GetState(model.ArrState);

            try
            {
                //save
                ModTrackingService.Instance.Save(_item);
            }
            catch (Exception ex)
            {
                Error.Write(ex);
                CPViewPage.Message.ListMessage.Add(ex.Message);
                return false;
            }

            return true;
        }

        #endregion private func
    }

    public class ModTrackingModel : DefaultModel
    {
        public string SearchText { get; set; }

        public int State { get; set; }
        public List<int> ArrState { get; set; }
    }
}