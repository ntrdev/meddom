﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using CoreMr.Reddevil.Models;

namespace Reddevil.Lib.Models
{
    public class ModNewsMediaEntity : EntityBase
    {
        #region Autogen by RDV

        [DataInfo]
        public override int ID { get; set; }

        [DataInfo]
        public int MenuID { get; set; }

        [DataInfo]
        public int State { get; set; }

        [DataInfo]
        public override string Name { get; set; }

        [DataInfo]
        public string Url { get; set; }

        [DataInfo]
        public string UrlMedia { get; set; }

        [DataInfo]
        public string AdminEdit { get; set; }

        [DataInfo]
        public string AdminApprove { get; set; }

        [DataInfo]
        public string File { get; set; }

        [DataInfo]
        public string Summary { get; set; }

        [DataInfo]
        public string Content { get; set; }

        [DataInfo]
        public long View { get; set; }

        [DataInfo]
        public string Custom { get; set; }

        [DataInfo]
        public string PageTitle { get; set; }

        [DataInfo]
        public string PageHeading { get; set; }

        [DataInfo]
        public string PageDescription { get; set; }

        [DataInfo]
        public string PageKeywords { get; set; }

        [DataInfo]
        public DateTime Published { get; set; }

        [DataInfo]
        public DateTime Updated { get; set; }

        [DataInfo]
        public int Order { get; set; }

        [DataInfo]
        public bool Activity { get; set; }

        [DataInfo]
        public bool Status { get; set; }

        [DataInfo]
        public int Condition { get; set; }//giai đoạn


        [DataInfo]
        public bool Cost { get; set; }
        #endregion Autogen by RDV

        private List<ModDynamicFileEntity> _oGetFile;
        public List<ModDynamicFileEntity> GetFile
        {
            get
            {
                if (_oGetFile == null && this.ID > 0)
                    _oGetFile = ModDynamicFileService.Instance.CreateQuery()
                                                    .Where(o => o.ObjectID == ID && o.Type == 3)//Media
                                                    .ToList_Cache();
                return _oGetFile;
            }
        }

      
        public string Time
        {
            get
            {
                var ts = DateTime.Now - Published;

                if (ts.TotalDays >= 365) return ts.Days / 365 + " năm trước.";
                if (ts.TotalDays >= 30) return ts.Days / 30 + " tháng trước.";
                if (ts.TotalDays >= 7) return ts.Days / 7 + " tuần trước.";
                if (ts.TotalDays >= 1) return ts.Days + " ngày trước.";
                if (ts.TotalHours >= 1) return ts.Hours + " giờ trước.";
                if (ts.TotalMinutes >= 1) return ts.Minutes + " phút trước.";
                return ts.Seconds + " giây trước.";
            }
        }

        public void UpView()
        {
            View++;
            ModNewsMediaService.Instance.Save(this, o => o.View);
        }

        private WebMenuEntity _oMenu;

        public WebMenuEntity GetMenu()
        {
            if (_oMenu == null && MenuID > 0)
                _oMenu = WebMenuService.Instance.GetByID_Cache(MenuID);

            return _oMenu ?? (_oMenu = new WebMenuEntity());
        }

        private string _YoutubeID;
        public string YoutubeID
        {
            get
            {
                if (string.IsNullOrEmpty(_YoutubeID) && !string.IsNullOrEmpty(UrlMedia))
                {
                    string[] _ArrCode = Regex.Split(UrlMedia, "v=");
                    if (_ArrCode.Length > 1) _YoutubeID = _ArrCode[1];
                }

                return _YoutubeID;
            }
        }

        private string _Thumbnail;
        public string Thumbnail
        {
            get
            {
                if (string.IsNullOrEmpty(_Thumbnail) && !string.IsNullOrEmpty(UrlMedia))
                    _Thumbnail = "http://img.youtube.com/vi/" + YoutubeID + "/0.jpg";

                return _Thumbnail;
            }
        }

        private string _Duration;
        public string Duration
        {
            get
            {
                string _Url = @"https://www.googleapis.com/youtube/v3/Medias?id=" + YoutubeID + @"&key=AIzaSyDYwPzLevXauI-kTSVXTLroLyHEONuF9Rw&part=snippet,contentDetails";
                string _Json = Reddevil.Lib.Global.Utils.GetResponseJson(_Url);

                dynamic _Data = Newtonsoft.Json.JsonConvert.DeserializeObject(_Json);
                dynamic items = _Data.items;

                if (items.Count == 0) return "0";

                dynamic contentDetails = items[0].contentDetails;
                dynamic duration = contentDetails.duration;

                var _TotalSecond = System.Xml.XmlConvert.ToTimeSpan(duration.Value).TotalSeconds;

                _Duration = Global.Utils.ConvertTime(CoreMr.Reddevil.Global.Convert.ToInt(_TotalSecond));

                return _Duration;
            }
        }
    }

    public class ModNewsMediaService : ServiceBase<ModNewsMediaEntity>
    {
        #region Autogen by RDV

        public ModNewsMediaService() : base("[Mod_NewsMedia]")
        {
        }

        private static ModNewsMediaService _instance;
        public static ModNewsMediaService Instance => _instance ?? (_instance = new ModNewsMediaService());

        #endregion Autogen by RDV

        public ModNewsMediaEntity GetByID(int id)
        {
            return CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle();
        }

        public bool Exists(string query)
        {
            return CreateQuery()
                           .Where(query)
                           .Count()
                           .ToValue()
                           .ToBool();
        }
    }
}