﻿using System;
using System.Collections.Generic;
using CoreMr.Reddevil.Models;

namespace Reddevil.Lib.Models
{
    public class ModAINewsEntity : EntityBase
    {
        #region Autogen by RDV

        [DataInfo]
        public long Id { get; set; }

        [DataInfo]
        public string Title { get; set; }

        [DataInfo]
        public string Image1 { get; set; }

        [DataInfo]
        public string Subject { get; set; }

        [DataInfo]
        public string Contents { get; set; }


        [DataInfo]
        public DateTime UpdatedDate { get; set; }

        [DataInfo]
        public DateTime CreatedDate { get; set; }



        [DataInfo]
        public int ViewCount { get; set; }

        #endregion Autogen by RDV
    }

    public class ModAINewsService : ServiceBase<ModAINewsEntity>
    {
        #region Autogen by RDV

        public ModAINewsService()
            : base("[AINews]")
        {
            DBConfigKey = "ConnectToCpd1";
        }

        private static ModAINewsService _Instance = null;
        public static ModAINewsService Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new ModAINewsService();

                return _Instance;
            }
        }

        #endregion

        public ModAINewsEntity GetByID(int id)
        {
            return base.CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle();
        }

    }

    public class ModSearchAllEntity : EntityBase
    {
        #region Autogen by RDV

      
        [DataInfo]
        public string title { get; set; }

        [DataInfo]
        public string url { get; set; }

        [DataInfo]
        public string category { get; set; }

       
        #endregion Autogen by RDV
    }

    public class ModSearchAllService : ServiceBase<ModSearchAllEntity>
    {
        #region Autogen by RDV

        public ModSearchAllService() : base("[Mod_News]")
        {
           
        }

        private static ModSearchAllService _Instance = null;
        public static ModSearchAllService Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new ModSearchAllService();

                return _Instance;
            }
        }

        #endregion

    }
}