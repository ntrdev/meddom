﻿using System;
using System.Collections.Generic;
using CoreMr.Reddevil.Models;

namespace Reddevil.Lib.Models
{
    public class ModNewsGarnitureEntity : EntityBase
    {
        #region Autogen by RDV

        [DataInfo]
        public override int ID { get; set; }

        [DataInfo]
        public int MenuID { get; set; }

        [DataInfo]
        public int State { get; set; }

        [DataInfo]
        public override string Name { get; set; }

        [DataInfo]
        public string Url { get; set; }

        [DataInfo]
        public string UrlAudio { get; set; }

        [DataInfo]
        public string AdminEdit { get; set; }

        [DataInfo]
        public string AdminApprove { get; set; }

        [DataInfo]
        public string File { get; set; }

        [DataInfo]
        public string QrCode { get; set; }

        [DataInfo]
        public string Summary { get; set; }

        [DataInfo]
        public string Content { get; set; }

        [DataInfo]
        public long View { get; set; }

        [DataInfo]
        public string Custom { get; set; }

        [DataInfo]
        public string PageTitle { get; set; }

        [DataInfo]
        public string PageHeading { get; set; }

        [DataInfo]
        public string PageDescription { get; set; }

        [DataInfo]
        public string PageKeywords { get; set; }

        [DataInfo]
        public DateTime Published { get; set; }

        [DataInfo]
        public DateTime Updated { get; set; }

        [DataInfo]
        public int Order { get; set; }

        [DataInfo]
        public bool Activity { get; set; }
        [DataInfo]
        public bool Status { get; set; }
        [DataInfo]
        public int Condition { get; set; }//giai đoạn

        [DataInfo]
        public bool Cost { get; set; }
        #endregion Autogen by RDV

        private List<ModDynamicFileEntity> _oGetFile;
        public List<ModDynamicFileEntity> GetFile
        {
            get
            {
                if (_oGetFile == null && this.ID > 0)
                    _oGetFile = ModDynamicFileService.Instance.CreateQuery()
                                                    .Select(o => new { o.File, o.ID })
                                                    .Where(o => o.ObjectID == ID && o.Type == 4)//file trưng bày
                                                    .ToList_Cache();
                return _oGetFile;
            }
        }
        public string Time
        {
            get
            {
                var ts = DateTime.Now - Published;

                if (ts.TotalDays >= 365) return ts.Days / 365 + " năm trước.";
                if (ts.TotalDays >= 30) return ts.Days / 30 + " tháng trước.";
                if (ts.TotalDays >= 7) return ts.Days / 7 + " tuần trước.";
                if (ts.TotalDays >= 1) return ts.Days + " ngày trước.";
                if (ts.TotalHours >= 1) return ts.Hours + " giờ trước.";
                if (ts.TotalMinutes >= 1) return ts.Minutes + " phút trước.";
                return ts.Seconds + " giây trước.";
            }
        }

        public void UpView()
        {
            View++;
            ModNewsGarnitureService.Instance.Save(this, o => o.View);
        }

        private WebMenuEntity _oMenu;

        public WebMenuEntity GetMenu()
        {
            if (_oMenu == null && MenuID > 0)
                _oMenu = WebMenuService.Instance.GetByID_Cache(MenuID);

            return _oMenu ?? (_oMenu = new WebMenuEntity());
        }
        private List<ModNewsMutilEntity> _oGetNews;
        public List<ModNewsMutilEntity> GetNews
        {
            get
            {
                if (_oGetNews == null && this.ID > 0)
                    _oGetNews = ModNewsMutilService.Instance.CreateQuery()
                                                    .Where(o => o.ParentID == ID && o.TypeID == 3)
                                                    .ToList_Cache();
                return _oGetNews;
            }
        }
    }

    public class ModNewsGarnitureService : ServiceBase<ModNewsGarnitureEntity>
    {
        #region Autogen by RDV

        public ModNewsGarnitureService() : base("[Mod_NewsGarniture]")
        {
        }

        private static ModNewsGarnitureService _instance;
        public static ModNewsGarnitureService Instance => _instance ?? (_instance = new ModNewsGarnitureService());

        #endregion Autogen by RDV

        public ModNewsGarnitureEntity GetByID(int id)
        {
            return CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle();
        }

        public bool Exists(string query)
        {
            return CreateQuery()
                           .Where(query)
                           .Count()
                           .ToValue()
                           .ToBool();
        }
    }
}