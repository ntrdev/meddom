﻿using System;
using System.Collections.Generic;
using CoreMr.Reddevil.Models;

namespace Reddevil.Lib.Models
{
    public class ModScientistEntity : EntityBase
    {
        #region Autogen by RDV

        //[DataInfo]
        //public override int ID { get; set; }

        [DataInfo]
        public int Id { get; set; }

        [DataInfo]
        public int Status { get; set; }//trạng thái

        [DataInfo]
        public string FirstName { get; set; }//ID người tạo

        [DataInfo]
        public string FullName { get; set; }//tên nhà khoa học

        [DataInfo]
        public string AliasName { get; set; }

        [DataInfo]
        public string Images { get; set; }//ảnh đại diện

        [DataInfo]
        public string Contents { get; set; }//Mô tả ngắn về nhà khoa học

        [DataInfo]
        public DateTime Birthday { get; set; }//ngày sinh

        [DataInfo]
        public string Degree { get; set; }//học hàm

        [DataInfo]
        public string ProfessionalKnowledge { get; set; }//kiến thức chuyên môn

        [DataInfo]
        public string ResearchTopic { get; set; }//chủ đề nghiên cứu

        [DataInfo]
        public string Article { get; set; }//các bài viêt

        [DataInfo]
        public string PublishedBook { get; set; }//Đề tài nghiên cứu

        [DataInfo]
        public string PhDThesis { get; set; }// luận án tiến sĩ

        [DataInfo]
        public string MAThesis { get; set; }// luận văn thạc sĩ

        [DataInfo]
        public string GraduationPaper { get; set; }//khóa luận

        [DataInfo]
        public string Ext2 { get; set; }//Nét cơ bản về NKH

        [DataInfo]
        public string TimeLine { get; set; }//quá trình hoạt động

        [DataInfo]
        public string WorkingHistory { get; set; }//Quá trình làm việc

        [DataInfo]
        public string StudyHistory { get; set; }//quá trình học tập

        [DataInfo]
        public string Award { get; set; }//Giải thưởng

        [DataInfo]
        public string YearOfDead { get; set; }//năm mất

        [DataInfo]
        public string Tel { get; set; }//sđt

        [DataInfo]
        public string Address { get; set; }//địa chỉ

        [DataInfo]
        public string NativeVillage { get; set; }//quê quán

        [DataInfo]
        public string Gender { get; set; }//giới tính

        [DataInfo]
        public DateTime DateCreated { get; set; }//ngày tạo

        [DataInfo]
        public string LanguageID { get; set; }//languageid

        #endregion Autogen by RDV



        //private ModTuKhoaDongEntity _oDanhMuc;

        //public ModTuKhoaDongEntity GetDanhMuc()
        //{
        //    if (_oDanhMuc == null && NganhID > 0)
        //        _oDanhMuc = ModTuKhoaDongService.Instance.GetByID_Cache(NganhID);

        //    return _oDanhMuc ?? (_oDanhMuc = new ModTuKhoaDongEntity());
        //}
        private List<ModScientistAlbumEntity> _oAlbum;

        public List<ModScientistAlbumEntity> GetAlbum
        {
            get
            {
                if (_oAlbum == null && Id > 0)
                    _oAlbum = ModScientistAlbumService.Instance.CreateQuery().Where(o => o.DoctorId == Id).ToList_Cache();

                return _oAlbum;
            }
        }

        private string _oAward = string.Empty;
        public string GetAward
        {
            get
            {
                if (string.IsNullOrEmpty(_oAward) && !string.IsNullOrEmpty(Award))
                {
                    var _split = CoreMr.Reddevil.Global.Array.ToString(Award, ';');
                    for (int i = 0; i < _split.Length; i++)
                    {
                        _oAward += "<b>" + _split[i] + "</b></br>";
                    }
                }

                return _oAward;
            }

        }


        //private ModTuKhoaDongEntity _oSomeOne;

        //public ModTuKhoaDongEntity GetDMSomeone(int id)
        //{
        //    if (id > 0)
        //        _oSomeOne = ModTuKhoaDongService.Instance.GetByID_Cache(id);

        //    return _oSomeOne ?? (_oSomeOne = new ModTuKhoaDongEntity());
        //}
        //private ModTuKhoaPhanCapEntity _oSomeOnePC;

        //public ModTuKhoaPhanCapEntity GetDMPCSomeone(int id)
        //{
        //    if (_oSomeOnePC == null && id > 0)
        //        _oSomeOnePC = ModTuKhoaPhanCapService.Instance.GetByID_Cache(id);

        //    return _oSomeOnePC ?? (_oSomeOnePC = new ModTuKhoaPhanCapEntity());
        //}
        private string _oSpecialized = string.Empty;

        public string GetSpecialized
        {
            get
            {
                if (string.IsNullOrEmpty(_oSpecialized) && Id > 0)
                {
                    var listChuyenNganh = ModChuyenNganhService.Instance.CreateQuery()
                                                .SelectJoin(true, o => new { o.Name, o.Id })
                                                .Join.LeftJoin(true, ModChuyenNganhInScientistService.Instance.CreateQuery().Where(x => x.DoctorId == this.Id), o => o.Id, x => x.MajorId)
                                                .ToList_Cache();

                    for (int i = 0; listChuyenNganh != null && i < listChuyenNganh.Count; i++)
                    {
                        if (i == 0) _oSpecialized = listChuyenNganh[i].Name;
                        else _oSpecialized = _oSpecialized + ", " + listChuyenNganh[i].Name;
                    }
                }

                return _oSpecialized;
            }
        }
    }

    public class ModScientistService : ServiceBase<ModScientistEntity>
    {
        #region Autogen by RDV

        public ModScientistService() : base("[TNDoctorInformation]")
        {
            DBConfigKey = "ConnectToCpd1";
        }

        private static ModScientistService _instance;
        public static ModScientistService Instance => _instance ?? (_instance = new ModScientistService());

        #endregion Autogen by RDV


    }
}