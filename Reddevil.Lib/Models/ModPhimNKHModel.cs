﻿using System.Collections.Generic;
using CoreMr.Reddevil.Models;

namespace Reddevil.Lib.Models
{
    public class ModTuLieuPhimEntity : EntityBase
    {
        #region Autogen by RDV
        

        [DataInfo]
        public int TuLieuPhimID { get; set; }

        [DataInfo]
        public int NKHID { get; set; }

        [DataInfo]
        public string TieuDe { get; set; }

        [DataInfo]
        public string TacGiaNguoiQuay { get; set; }

        [DataInfo]
        public string  NoiSXDCQuay { get; set; }

        [DataInfo]
        public int NamSanXuat { get; set; }

        #endregion Autogen by RDV
    }

    public class ModTuLieuPhimService : ServiceBase<ModTuLieuPhimEntity>
    {
        #region Autogen by RDV

        public ModTuLieuPhimService()
            : base("[cpd_TuLieuPhim]")
        {
            DBConfigKey = "ConnectToCpd";
        }

        private static ModTuLieuPhimService _Instance = null;
        public static ModTuLieuPhimService Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new ModTuLieuPhimService();

                return _Instance;
            }
        }

        #endregion

        public ModTuLieuPhimEntity GetByID(int id)
        {
            return base.CreateQuery()
               .Where(o => o.TuLieuPhimID == id)
               .ToSingle();
        }

    }
}