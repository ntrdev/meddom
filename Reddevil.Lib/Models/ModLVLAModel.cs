﻿using System.Collections.Generic;
using CoreMr.Reddevil.Models;

namespace Reddevil.Lib.Models
{
    public class ModLVLAEntity : EntityBase
    {
        #region Autogen by RDV
        

        [DataInfo]
        public int LVLAID { get; set; }

        [DataInfo]
        public int NKHID { get; set; }

        [DataInfo]
        public string TenDeTai { get; set; }

        [DataInfo]
        public int NgonNguID { get; set; }

        [DataInfo]
        public int ChuyenNganhKHID { get; set; }

        [DataInfo]
        public string NguoiHuongDan { get; set; }

        [DataInfo]
        public int NamBaoVe { get; set; }

        [DataInfo]
        public string NoiBaoVe { get; set; }

        [DataInfo]
        public string LoaiHinh { get; set; }

        #endregion Autogen by RDV
    }

    public class ModLVLAService : ServiceBase<ModLVLAEntity>
    {
        #region Autogen by RDV

        public ModLVLAService()
            : base("[cpd_LVLA]")
        {
            DBConfigKey = "ConnectToCpd";
        }

        private static ModLVLAService _Instance = null;
        public static ModLVLAService Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new ModLVLAService();

                return _Instance;
            }
        }

        #endregion

        public ModLVLAEntity GetByID(int id)
        {
            return base.CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle();
        }

    }
}