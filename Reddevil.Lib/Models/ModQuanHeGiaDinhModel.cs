﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using CoreMr.Reddevil.Models;

namespace Reddevil.Lib.Models
{
    public class ModQuanHeGiaDinhEntity : EntityBase
    {
        #region Autogen by RDV

        [DataInfo]
        public int QuanHeGiaDinhID { get; set; }

        [DataInfo]
        public string HoTen { get; set; }

        [DataInfo]
        public string NamSinh { get; set; }

        [DataInfo]
        public string NgheNghiep { get; set; }

        [DataInfo]
        public bool DaMat { get; set; }

        [DataInfo]
        public string LoaiQuanHe { get; set; }

        [DataInfo]
        public int NKHID { get; set; }

        #endregion Autogen by RDV


    }

    public class ModQuanHeGiaDinhService : ServiceBase<ModQuanHeGiaDinhEntity>
    {
        #region Autogen by RDV

        public ModQuanHeGiaDinhService()
            : base("[cpd_QuanHeGiaDinh]")
        {
            DBConfigKey = "ConnectToCpd";
        }

        private static ModQuanHeGiaDinhService _instance;
        public static ModQuanHeGiaDinhService Instance
        {
            get { return _instance ?? (_instance = new ModQuanHeGiaDinhService()); }
        }

        #endregion Autogen by RDV

        public ModQuanHeGiaDinhEntity GetByID(int id)
        {
            return CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle();
        }

        public ModQuanHeGiaDinhEntity GetByID_Cache(int id)
        {
            return CreateQuery()
               .Where(o => o.QuanHeGiaDinhID == id)
               .ToSingle_Cache();
        }

    }
}