﻿using System.Collections.Generic;
using CoreMr.Reddevil.Models;

namespace Reddevil.Lib.Models
{
    public class ModBaiBaoNghienCuuEntity : EntityBase
    {
        #region Autogen by RDV
        

        [DataInfo]
        public int BaiBaoNghienCuuID { get; set; }

        [DataInfo]
        public int NKHID { get; set; }

        [DataInfo]
        public string TieuDeBaiViet { get; set; }

        [DataInfo]
        public string TacGia { get; set; }

        [DataInfo]
        public string TenTapChi { get; set; }

        [DataInfo]
        public string TapSo { get; set; }

        [DataInfo]
        public int NamXuatBan { get; set; }

        #endregion Autogen by RDV
    }

    public class ModBaiBaoNghienCuuService : ServiceBase<ModBaiBaoNghienCuuEntity>
    {
        #region Autogen by RDV

        public ModBaiBaoNghienCuuService()
            : base("[cpd_BaiBaoNghienCuu]")
        {
            DBConfigKey = "ConnectToCpd";
        }

        private static ModBaiBaoNghienCuuService _Instance = null;
        public static ModBaiBaoNghienCuuService Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new ModBaiBaoNghienCuuService();

                return _Instance;
            }
        }

        #endregion

        public ModBaiBaoNghienCuuEntity GetByID(int id)
        {
            return base.CreateQuery()
               .Where(o => o.BaiBaoNghienCuuID == id)
               .ToSingle();
        }

    }
}