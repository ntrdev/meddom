﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using CoreMr.Reddevil.Models;

namespace Reddevil.Lib.Models
{
    public class ModChuyenNganhInScientistEntity : EntityBase
    {
        #region Autogen by RDV

        [DataInfo]
        public int Id { get; set; }

        [DataInfo]
        public int DoctorId { get; set; }

        [DataInfo]
        public int MajorId { get; set; }

        #endregion Autogen by RDV


    }

    public class ModChuyenNganhInScientistService : ServiceBase<ModChuyenNganhInScientistEntity>
    {
        #region Autogen by RDV

        public ModChuyenNganhInScientistService()
            : base("[TNDoctorInMajor]")
        {
            DBConfigKey = "ConnectToCpd1";
        }

        private static ModChuyenNganhInScientistService _instance;
        public static ModChuyenNganhInScientistService Instance
        {
            get { return _instance ?? (_instance = new ModChuyenNganhInScientistService()); }
        }

        #endregion Autogen by RDV

        public ModChuyenNganhInScientistEntity GetByID(int id)
        {
            return CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle();
        }

        public ModChuyenNganhInScientistEntity GetByID_Cache(int id)
        {
            return CreateQuery()
               .Where(o => o.Id == id)
               .ToSingle_Cache();
        }
       
    }
}