﻿using System;
using System.Collections.Generic;
using CoreMr.Reddevil.Models;

namespace Reddevil.Lib.Models
{
    public class ModNhaKhoaHocEntity : EntityBase
    {
        #region Autogen by RDV
        /// <summary>
        /// ID nhà khoa học
        /// </summary>
        [DataInfo]
        public int NKHID { get; set; }

        [DataInfo]
        public int DoctorID { get; set; }

        /// <summary>
        /// Tên nhà khoa học
        /// </summary>
        [DataInfo]
        public string TenNKH { get; set; }

        /// <summary>
        /// Họ tên nhà khoa học
        /// </summary>
        [DataInfo]
        public string HoTen { get; set; }

        /// <summary>
        /// Bí danh nhà khoa học
        /// </summary>
        [DataInfo]
        public string BiDanh { get; set; }

        /// <summary>
        /// Ngày sinh
        /// </summary>
        [DataInfo]
        public int NgaySinh { get; set; }

        /// <summary>
        /// Tháng sinh
        /// </summary>
        [DataInfo]
        public int ThangSinh { get; set; }

        /// <summary>
        /// Năm sinh
        /// </summary>
        [DataInfo]
        public int NamSinh { get; set; }

        /// <summary>
        /// ID của danh mục Dân tộc (ModDanToc)
        /// </summary>
        [DataInfo]
        public int DanTocID { get; set; }

        /// <summary>
        /// Chuyên ngành
        /// </summary>
        [DataInfo]
        public int NganhID { get; set; }

        /// <summary>
        /// Giới tính
        /// </summary>
        [DataInfo]
        public int GioiTinh { get; set; }

        /// <summary>
        /// Địa chỉ
        /// </summary>
        [DataInfo]
        public string DiaChi { get; set; }

        /// <summary>
        /// Id của địa chỉ
        /// Tỉnh thành
        /// Quận huyện gì đó :))
        /// </summary>
        [DataInfo]
        public int DiaChiDiaDanhID { get; set; }

        /// <summary>
        /// Sđt di động
        /// </summary>
        [DataInfo]
        public string DTDiDong { get; set; }

        /// <summary>
        /// Sđt cố định
        /// </summary>
        [DataInfo]
        public string DTCoDinh { get; set; }

        /// <summary>
        /// sđt cơ quan
        /// </summary>
        [DataInfo]
        public string DTCoQuan { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        [DataInfo]
        public string Email { get; set; }

        /// <summary>
        /// Danh hiệu, giải thưởng đã đạt được
        /// </summary>
        [DataInfo]
        public string DanhHieuGiaiThuong { get; set; }

        /// <summary>
        /// Nét tiêu biểu
        /// </summary>
        [DataInfo]
        public string NetTieuBieu { get; set; }//Quá trình làm việc

        /// <summary>
        /// Còn sống hay đã mất
        /// </summary>
        [DataInfo]
        public bool DaMat { get; set; }

        /// <summary>
        /// Ngày mất
        /// </summary>
        [DataInfo]
        public int NgayMat { get; set; }

        /// <summary>
        /// tháng mất
        /// </summary>
        [DataInfo]
        public int ThangMat { get; set; }

        /// <summary>
        /// Năm mất
        /// </summary>
        [DataInfo]
        public int NamMat { get; set; }

        /// <summary>
        /// Thời gian bắt đầu nghiên cứu
        /// </summary>
        [DataInfo]
        public DateTime ThoiGianBatDauNghienCuu { get; set; }

        /// <summary>
        /// Nơi sinh
        /// </summary>
        [DataInfo]
        public string NoiSinh { get; set; }//sđt

        /// <summary>
        /// Nơi sinh ID
        /// </summary>
        [DataInfo]
        public int NoiSinhDiaDanhID { get; set; }

        /// <summary>
        /// Quê quán
        /// </summary>
        [DataInfo]
        public string QueQuan { get; set; }

        /// <summary>
        /// Quê quán ID
        /// </summary>
        [DataInfo]
        public int QueQuanDiaDanhID { get; set; }

        /// <summary>
        /// Học hàm ID
        /// </summary>
        [DataInfo]
        public int HocHamID { get; set; }
        
        /// <summary>
        /// năm đạt được học hàm
        /// </summary>
        [DataInfo]
        public int NamDatHocHam { get; set; }

        /// <summary>
        /// Học vị ID
        /// </summary>
        [DataInfo]
        public int HocViID { get; set; }

        /// <summary>
        /// Năm đạt học vị
        /// </summary>
        [DataInfo]
        public int NamDatHocVi { get; set; }

        /// <summary>
        /// Đơn vị công tác ID
        /// </summary>
        [DataInfo]
        public int DonViCongTacID { get; set; }

        /// <summary>
        /// Hiển thị trên web hay không
        /// </summary>
        [DataInfo]
        public bool QLTrenWeb { get; set; }

        /// <summary>
        /// ID của người nghiên cứu về NKH
        /// </summary>
        [DataInfo]
        public int NguoiNghienCuuID { get; set; }

        /// <summary>
        /// trạng thái nghiên cứu
        /// Sâu - rộng
        /// </summary>
        [DataInfo]
        public string TrangThaiNC { get; set; }

        /// <summary>
        /// duyệt 
        /// </summary>
        [DataInfo]
        public bool Active { get; set; }

       
        #endregion Autogen by RDV
    }

    public class ModNhaKhoaHocService : ServiceBase<ModNhaKhoaHocEntity>
    {
        #region Autogen by RDV

        public ModNhaKhoaHocService() : base("[cpd_NKH]", "NKHID")
        {
            DBConfigKey = "ConnectToCpd";
        }

        private static ModNhaKhoaHocService _instance;
        public static ModNhaKhoaHocService Instance => _instance ?? (_instance = new ModNhaKhoaHocService());

        #endregion Autogen by RDV


    }
}