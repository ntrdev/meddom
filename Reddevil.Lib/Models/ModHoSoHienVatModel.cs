﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using CoreMr.Reddevil.Models;

namespace Reddevil.Lib.Models
{
    public class ModHoSoHienVatEntity : EntityBase
    {
        #region Autogen by RDV

        [DataInfo]
        public int HoSoHienVatID { get; set; }

        [DataInfo]
        public string SoKiemKe { get; set; }

        [DataInfo]
        public string SoHoSo { get; set; }

        [DataInfo]
        public string TenPhoThong { get; set; }

        [DataInfo]
        public string MuaHienTang { get; set; }

        [DataInfo]
        public DateTime NgaySuuTam { get; set; }

        [DataInfo]
        public string DiaChiNoiSuuTam { get; set; }

        [DataInfo]
        public int NKHID { get; set; }

        #endregion Autogen by RDV


    }

    public class ModHoSoHienVatService : ServiceBase<ModHoSoHienVatEntity>
    {
        #region Autogen by RDV

        public ModHoSoHienVatService()
            : base("[cpd_HoSoHienVat]")
        {
            DBConfigKey = "ConnectToCpd";
        }

        private static ModHoSoHienVatService _instance;
        public static ModHoSoHienVatService Instance
        {
            get { return _instance ?? (_instance = new ModHoSoHienVatService()); }
        }

        #endregion Autogen by RDV

        public ModHoSoHienVatEntity GetByID(int id)
        {
            return CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle();
        }

        public ModHoSoHienVatEntity GetByID_Cache(int id)
        {
            return CreateQuery()
               .Where(o => o.HoSoHienVatID == id)
               .ToSingle_Cache();
        }

    }
}