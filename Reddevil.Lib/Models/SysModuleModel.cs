﻿using CoreMr.Reddevil.Interface;
using Reddevil.Lib.Web;

namespace Reddevil.Lib.Models
{
    public class SysModuleService : IModuleServiceInterface
    {
        private static SysModuleService _instance;

        public static SysModuleService Instance => _instance ?? (_instance = new SysModuleService());

        public IModuleInterface CoreMr_Reddevil_GetByCode(string code)
        {
            return Application.Modules.Find(o => o.Code == code);
        }
    }
}