﻿using System;
using System.Collections.Generic;
using CoreMr.Reddevil.Models;

namespace Reddevil.Lib.Models
{
    public class ModScientistArticleEntity : EntityBase
    {
        #region Autogen by RDV


        [DataInfo]
        public int Id { get; set; }

        [DataInfo]
        public int DoctorId { get; set; }

        [DataInfo]
        public string Title { get; set; }

        [DataInfo]
        public string Sumary { get; set; }

        [DataInfo]
        public string Contents { get; set; }

        [DataInfo]
        public string ImagePath { get; set; }

        [DataInfo]
        public string ImageTitle { get; set; }

        [DataInfo]
        public DateTime CreatedDate { get; set; }
        
        #endregion Autogen by RDV
    }

    public class ModScientistArticleService : ServiceBase<ModScientistArticleEntity>
    {
        #region Autogen by RDV

        public ModScientistArticleService()
            : base("[TNDoctorArticle]","Id")
        {
            DBConfigKey = "ConnectToCpd1";
        }

        private static ModScientistArticleService _Instance = null;
        public static ModScientistArticleService Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new ModScientistArticleService();

                return _Instance;
            }
        }



        #endregion


        public List<ModScientistArticleEntity> GetByID_Cache(int id)
        {
            return CreateQuery()
               .Where(o => o.DoctorId == id)
               .ToList_Cache();
        }
    }
}