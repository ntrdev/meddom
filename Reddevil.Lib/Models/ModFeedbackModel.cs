﻿using System;

using CoreMr.Reddevil.Models;

namespace Reddevil.Lib.Models
{
    public class ModFeedbackEntity : EntityBase
    {
        #region Autogen by RDV

        [DataInfo]
        public override int ID { get; set; }

        [DataInfo]
        public override string Name { get; set; }

        [DataInfo]
        public string Email { get; set; }

        [DataInfo]
        public string Phone { get; set; }

        [DataInfo]
        public string Address { get; set; }

        [DataInfo]
        public string Title { get; set; }

        [DataInfo]
        public string Content { get; set; }

        [DataInfo]
        public string IP { get; set; }

        [DataInfo]
        public DateTime Created { get; set; }

        #endregion Autogen by RDV

        public string Time
        {
            get
            {
                if ((DateTime.Now - Created).TotalDays >= 365) return (DateTime.Now - Created).Days / 365 + " năm trước.";
                if ((DateTime.Now - Created).TotalDays >= 30) return (DateTime.Now - Created).Days / 30 + " tháng trước.";
                if ((DateTime.Now - Created).TotalDays >= 7) return (DateTime.Now - Created).Days / 7 + " tuần trước.";
                if ((DateTime.Now - Created).TotalDays >= 1) return (DateTime.Now - Created).Days + " ngày trước.";
                if ((DateTime.Now - Created).TotalHours >= 1) return (DateTime.Now - Created).Hours + " giờ trước.";
                if ((DateTime.Now - Created).TotalMinutes >= 1) return (DateTime.Now - Created).Minutes + " phút trước.";
                return (DateTime.Now - Created).Seconds + " giây trước.";
            }
        }
    }

    public class ModFeedbackService : ServiceBase<ModFeedbackEntity>
    {
        #region Autogen by RDV

        public ModFeedbackService() : base("[Mod_Feedback]")
        {
        }

        private static ModFeedbackService _instance;
        public static ModFeedbackService Instance => _instance ?? (_instance = new ModFeedbackService());

        #endregion Autogen by RDV

        public ModFeedbackEntity GetByID(int id)
        {
            return CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle();
        }
    }
}