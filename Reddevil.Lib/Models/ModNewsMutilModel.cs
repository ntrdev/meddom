﻿using System.Collections.Generic;
using CoreMr.Reddevil.Models;

namespace Reddevil.Lib.Models
{
    public class ModNewsMutilEntity : EntityBase
    {
        #region Autogen by RDV


        [DataInfo]
        public int NewsID { get; set; }

        [DataInfo]
        public int ParentID { get; set; }

        [DataInfo]
        public int TypeID { get; set; }


        #endregion Autogen by RDV

    }

    public class ModNewsMutilService : ServiceBase<ModNewsMutilEntity>
    {
        #region Autogen by RDV

        public ModNewsMutilService() : base("[Mod_NewsMutil]")
        {
        }

        private static ModNewsMutilService _instance;
        public static ModNewsMutilService Instance => _instance ?? (_instance = new ModNewsMutilService());

        #endregion Autogen by RDV

        public ModNewsMutilEntity GetByID(int id)
        {
            return CreateQuery()
                .Where(o => o.ID == id)
                .ToSingle();
        }

        public ModNewsMutilEntity GetByID_Cache(int id)
        {
            return CreateQuery()
                .Where(o => o.ID == id)
                .ToSingle_Cache();
        }

        public bool Exists(int parent, int newsId, int type)
        {
            return CreateQuery()
                .Where(o => o.ParentID == parent && o.NewsID == newsId && o.TypeID == type)
                .Count()
                .ToValue_Cache()
                .ToBool();
        }

        public List<ModNewsMutilEntity> GetAll_Cache(int parent, int newsId)
        {
            return CreateQuery()
                .Where(o => o.ParentID == parent && o.NewsID == newsId)
                .ToList_Cache();
        }


    }
}