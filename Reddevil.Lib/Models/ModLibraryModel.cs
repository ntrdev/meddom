﻿using System;
using System.Collections.Generic;
using CoreMr.Reddevil.Models;

namespace Reddevil.Lib.Models
{
    public class ModLibraryEntity : EntityBase
    {
        #region Autogen by RDV

        [DataInfo]
        public override int ID { get; set; }

        [DataInfo]
        public override string Name { get; set; }

        [DataInfo]
        public int CategoryID { get; set; }

        [DataInfo]
        public string Url { get; set; }

        [DataInfo]
        public string File { get; set; }

        [DataInfo]
        public int Order { get; set; }

        [DataInfo]
        public int State { get; set; }

        [DataInfo]
        public int Views { get; set; }

        [DataInfo]
        public int Download { get; set; }

        [DataInfo]
        public string Description { get; set; }

        [DataInfo]
        public string Image { get; set; }

        [DataInfo]
        public string CPUser { get; set; }

        [DataInfo]
        public bool Activity { get; set; }

        [DataInfo]
        public DateTime DateCreated { get; set; } = DateTime.Now;
        #endregion Autogen by RDV

        public void UpView()
        {
            Views++;
            ModLibraryService.Instance.Save(this, o => o.Views);
        }
        public void UpDownload ()
        {
            Download++;
            ModLibraryService.Instance.Save(this, o => o.Download);
        }

        private WebMenuEntity _oMenu;
        public WebMenuEntity GetMenu()
        {
            if (_oMenu == null && CategoryID > 0)
                _oMenu = WebMenuService.Instance.GetByID_Cache(CategoryID);

            return _oMenu ?? (_oMenu = new WebMenuEntity());
        }

    }

    public class ModLibraryService : ServiceBase<ModLibraryEntity>
    {
        #region Autogen by RDV

        public ModLibraryService() : base("[Mod_Library]"){ }

        private static ModLibraryService _instance;
        public static ModLibraryService Instance => _instance ?? (_instance = new ModLibraryService());

        #endregion Autogen by RDV

        public ModLibraryEntity GetByID(int id)
        {
            return CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle();
        }

    }
}