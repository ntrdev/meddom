﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using CoreMr.Reddevil.Models;

namespace Reddevil.Lib.Models
{
    public class ModTuKhoaPhanCapEntity : EntityBase
    {
        #region Autogen by RDV

        [DataInfo]
        public int TuKhoaPhanCapID { get; set; }

        [DataInfo]
        public string TuKhoaPhanCapName { get; set; }

        [DataInfo]
        public string DienGiai { get; set; }

        [DataInfo]
        public  string LoaiTuKhoa { get; set; }

        

        #endregion Autogen by RDV

        
    }

    public class ModTuKhoaPhanCapService : ServiceBase<ModTuKhoaPhanCapEntity>
    {
        #region Autogen by RDV

        public ModTuKhoaPhanCapService()
            : base("[cpd_DM_TuKhoaPhanCap]")
        {
            DBConfigKey = "ConnectToCpd";
        }

        private static ModTuKhoaPhanCapService _instance;
        public static ModTuKhoaPhanCapService Instance
        {
            get { return _instance ?? (_instance = new ModTuKhoaPhanCapService()); }
        }

        #endregion Autogen by RDV

        public ModTuKhoaPhanCapEntity GetByID(int id)
        {
            return CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle();
        }

        public ModTuKhoaPhanCapEntity GetByID_Cache(int id)
        {
            return CreateQuery()
               .Where(o => o.TuKhoaPhanCapID == id)
               .ToSingle_Cache();
        }
       
    }
}