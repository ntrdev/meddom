﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using CoreMr.Reddevil.Models;

namespace Reddevil.Lib.Models
{
    public class ModTuKhoaDongEntity : EntityBase
    {
        #region Autogen by RDV

        [DataInfo]
        public int TuKhoaDongID { get; set; }

        [DataInfo]
        public string TuKhoaDongName { get; set; }

        [DataInfo]
        public string DienGiai { get; set; }

        [DataInfo]
        public  string LoaiTuKhoa { get; set; }

        

        #endregion Autogen by RDV

        
    }

    public class ModTuKhoaDongService : ServiceBase<ModTuKhoaDongEntity>
    {
        #region Autogen by RDV

        public ModTuKhoaDongService()
            : base("[cpd_DM_TuKhoaDong]")
        {
            DBConfigKey = "ConnectToCpd";
        }

        private static ModTuKhoaDongService _instance;
        public static ModTuKhoaDongService Instance
        {
            get { return _instance ?? (_instance = new ModTuKhoaDongService()); }
        }

        #endregion Autogen by RDV

        public ModTuKhoaDongEntity GetByID(int id)
        {
            return CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle();
        }

        public ModTuKhoaDongEntity GetByID_Cache(int id)
        {
            return CreateQuery()
               .Where(o => o.TuKhoaDongID == id)
               .ToSingle_Cache();
        }
       
    }
}