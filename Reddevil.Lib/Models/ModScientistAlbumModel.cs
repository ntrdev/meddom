﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using CoreMr.Reddevil.Models;

namespace Reddevil.Lib.Models
{
    /// <summary>
    /// ảnh về nhà khoa học
    /// </summary>
    public class ModScientistAlbumEntity : EntityBase
    {
        #region Autogen by RDV

        [DataInfo]
        public int Id { get; set; }

        [DataInfo]
        public int DoctorId { get; set; }

        [DataInfo]
        public override string Name { get; set; }

        [DataInfo]
        public string Sumary { get; set; }

        [DataInfo]
        public string Description { get; set; }

        [DataInfo]
        public string PathOrigin { get; set; }

        #endregion Autogen by RDV


    }

    public class ModScientistAlbumService : ServiceBase<ModScientistAlbumEntity>
    {
        #region Autogen by RDV

        public ModScientistAlbumService()
            : base("[TNDoctorPhoto]")
        {
            DBConfigKey = "ConnectToCpd1";
        }

        private static ModScientistAlbumService _instance;
        public static ModScientistAlbumService Instance
        {
            get { return _instance ?? (_instance = new ModScientistAlbumService()); }
        }

        #endregion Autogen by RDV

        public ModScientistAlbumEntity GetByID(int id)
        {
            return CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle();
        }

        public ModScientistAlbumEntity GetByID_Cache(int id)
        {
            return CreateQuery()
               .Where(o => o.Id == id)
               .ToSingle_Cache();
        }
        public ModScientistAlbumEntity GetByParentID_Cache(int id)
        {
            return CreateQuery()
               .Where(o => o.DoctorId == id)
               .ToSingle_Cache();
        }

    }
}