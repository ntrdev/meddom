﻿using System.Collections.Generic;
using CoreMr.Reddevil.Models;

namespace Reddevil.Lib.Models
{
    public class ModDanhMucSachBaiVietEntity : EntityBase
    {
        #region Autogen by RDV
        

        [DataInfo]
        public int DanhMucSachBaiVietID { get; set; }

        [DataInfo]
        public int NKHID { get; set; }

        [DataInfo]
        public string TieuDeSach { get; set; }

        [DataInfo]
        public string TomTatNoiDung { get; set; }

        [DataInfo]
        public string TacGia { get; set; }

        #endregion Autogen by RDV
    }

    public class ModDanhMucSachBaiVietService : ServiceBase<ModDanhMucSachBaiVietEntity>
    {
        #region Autogen by RDV

        public ModDanhMucSachBaiVietService()
            : base("[cpd_DanhMucSachBaiViet]")
        {
            DBConfigKey = "ConnectToCpd";
        }

        private static ModDanhMucSachBaiVietService _Instance = null;
        public static ModDanhMucSachBaiVietService Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new ModDanhMucSachBaiVietService();

                return _Instance;
            }
        }

        #endregion

        public ModDanhMucSachBaiVietEntity GetByID(int id)
        {
            return base.CreateQuery()
               .Where(o => o.DanhMucSachBaiVietID == id)
               .ToSingle();
        }

    }
}