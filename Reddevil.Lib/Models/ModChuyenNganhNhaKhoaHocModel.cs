﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using CoreMr.Reddevil.Models;
using CoreMr.Reddevil.Web;

namespace Reddevil.Lib.Models
{
    public class ModChuyenNganhNhaKhoaHocEntity : EntityBase
    {
        #region Autogen by RDV

        [DataInfo]
        public int TuKhoaPhanCapID { get; set; }

        [DataInfo]
        public int Parrent { get; set; }

        [DataInfo]
        public string TuKhoaPhanCapName { get; set; }

        [DataInfo]
        public string LoaiTuKhoa { get; set; }

        [DataInfo]
        public bool Active { get; set; }

        #endregion Autogen by RDV


    }

    public class ModChuyenNganhNhaKhoaHocService : ServiceBase<ModChuyenNganhNhaKhoaHocEntity>
    {
        #region Autogen by RDV

        public ModChuyenNganhNhaKhoaHocService()
            : base("[cpd_DM_TuKhoaPhanCap]", "TuKhoaPhanCapID")
        {
            DBConfigKey = "ConnectToCpd";
        }

        private static ModChuyenNganhNhaKhoaHocService _instance;
        public static ModChuyenNganhNhaKhoaHocService Instance
        {
            get { return _instance ?? (_instance = new ModChuyenNganhNhaKhoaHocService()); }
        }

        #endregion Autogen by RDV

        public ModChuyenNganhNhaKhoaHocEntity GetByID(int id)
        {
            return CreateQuery()
               .Where(o => o.TuKhoaPhanCapID == id)
               .ToSingle();
        }

        public ModChuyenNganhNhaKhoaHocEntity GetByID_Cache(int id)
        {
            return CreateQuery()
               .Where(o => o.TuKhoaPhanCapID == id)
               .ToSingle_Cache();
        }
        public ModChuyenNganhNhaKhoaHocEntity GetByParentID_Cache(int id)
        {
            return CreateQuery()
               .Where(o => o.Parrent == id)
               .ToSingle_Cache();
        }

        public List<ModChuyenNganhNhaKhoaHocEntity> GetListByParentID_Cache(int id)
        {
            return CreateQuery()
               .Where(o => o.Parrent == id)
               .ToList_Cache();
        }


        public string GetChildIDForWeb_Cache(string type, int menuId)
        {
            var keyCache = string.Concat("[Web_Menu]", "GetChildIDForWeb_Cache." + type + "." + menuId);

            string cacheValue;
            var obj = Cache.GetValue(keyCache);
            if (obj != null) cacheValue = obj.ToString();
            else
            {
                var list = new List<int>();

                var listWebMenu = CreateQuery()
                                    .Where(o => o.Active == true)
                                    .Where(type != string.Empty, o => o.LoaiTuKhoa == type)
                                    .Select(o => new { o.ID, o.Parrent })
                                    .ToList_Cache();

                GetChildIDForWeb_Cache(ref list, listWebMenu, menuId);

                cacheValue = CoreMr.Reddevil.Global.Array.ToString(list.ToArray());

                CoreMr.Reddevil.Web.Cache.SetValue(keyCache, cacheValue);
            }

            return cacheValue;
        }

        private static void GetChildIDForWeb_Cache(ref List<int> list, List<ModChuyenNganhNhaKhoaHocEntity> listWebMenu, int menuId)
        {
            list.Add(menuId);

            if (listWebMenu == null)
                return;

            var listMenu = listWebMenu.FindAll(o => o.Parrent == menuId);

            foreach (var t in listMenu)
            {
                GetChildIDForWeb_Cache(ref list, listWebMenu, t.ID);
            }
        }
    }
}
