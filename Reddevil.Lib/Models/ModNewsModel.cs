﻿using System;
using System.Collections.Generic;
using CoreMr.Reddevil.Models;

namespace Reddevil.Lib.Models
{
    public class ModNewsEntity : EntityBase
    {
        #region Autogen by RDV

        [DataInfo]
        public override int ID { get; set; }

        [DataInfo]
        public int MenuID { get; set; }

        [DataInfo]
        public int State { get; set; }

        [DataInfo]
        public override string Name { get; set; }

        [DataInfo]
        public string Url { get; set; }

        [DataInfo]
        public string AdminEdit { get; set; }

        [DataInfo]
        public string AdminApprove { get; set; }

        [DataInfo]
        public string File { get; set; }

        [DataInfo]
        public string Summary { get; set; }

        [DataInfo]
        public string Content { get; set; }

        [DataInfo]
        public long View { get; set; }

        [DataInfo]
        public string Custom { get; set; }

        [DataInfo]
        public string PageTitle { get; set; }

        [DataInfo]
        public string PageHeading { get; set; }

        [DataInfo]
        public string PageDescription { get; set; }

        [DataInfo]
        public string PageKeywords { get; set; }

        [DataInfo]
        public DateTime Published { get; set; }

        [DataInfo]
        public DateTime Updated { get; set; }

        [DataInfo]
        public int Order { get; set; }

        [DataInfo]
        public bool Activity { get; set; }

        [DataInfo]
        public bool Status { get; set; }

        [DataInfo]
        public bool Cost { get; set; }

        [DataInfo]
        public int Condition { get; set; }//giai đoạn


        #endregion Autogen by RDV

        private List<ModDynamicFileEntity> _oGetFile;
        public List<ModDynamicFileEntity> GetFile
        {
            get
            {
                if (_oGetFile == null && this.ID > 0)
                    _oGetFile = ModDynamicFileService.Instance.CreateQuery()

                                                    .Where(o => o.ObjectID == ID && o.Type == 2)
                                                    .ToList_Cache();
                return _oGetFile;
            }
        }

        private List<ModNewsMutilEntity> _oGetNews;
        public List<ModNewsMutilEntity> GetNews
        {
            get
            {
                if (_oGetNews == null && this.ID > 0)
                    _oGetNews = ModNewsMutilService.Instance.CreateQuery()
                                                    .Where(o => o.ParentID == ID && o.TypeID == 1)
                                                    .ToList_Cache();
                return _oGetNews;
            }
        }
        public string Time
        {
            get
            {
                var ts = DateTime.Now - Published;

                if (ts.TotalDays >= 365) return ts.Days / 365 + " năm trước.";
                if (ts.TotalDays >= 30) return ts.Days / 30 + " tháng trước.";
                if (ts.TotalDays >= 7) return ts.Days / 7 + " tuần trước.";
                if (ts.TotalDays >= 1) return ts.Days + " ngày trước.";
                if (ts.TotalHours >= 1) return ts.Hours + " giờ trước.";
                if (ts.TotalMinutes >= 1) return ts.Minutes + " phút trước.";
                return ts.Seconds + " giây trước.";
            }
        }

        public void UpView()
        {
            View++;
            ModNewsService.Instance.Save(this, o => o.View);
        }

        private WebMenuEntity _oMenu;

        public WebMenuEntity GetMenu()
        {
            if (_oMenu == null && MenuID > 0)
                _oMenu = WebMenuService.Instance.GetByID_Cache(MenuID);

            return _oMenu ?? (_oMenu = new WebMenuEntity());
        }


    }

    public class ModNewsService : ServiceBase<ModNewsEntity>
    {
        #region Autogen by RDV

        public ModNewsService() : base("[Mod_News]")
        {
        }

        private static ModNewsService _instance;
        public static ModNewsService Instance => _instance ?? (_instance = new ModNewsService());

        #endregion Autogen by RDV

        public ModNewsEntity GetByID(int id)
        {
            return CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle();
        }

        public ModNewsEntity GetByID_Cache(int id)
        {
            return CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle_Cache();
        }

        public bool Exists(string query)
        {
            return CreateQuery()
                           .Where(query)
                           .Count()
                           .ToValue()
                           .ToBool();
        }
    }
}