﻿using System;
using System.Collections.Generic;
using CoreMr.Reddevil.Models;

namespace Reddevil.Lib.Models
{
    public class ModUserEntity : EntityBase
    {
        #region Autogen by RDV

        [DataInfo]
        public override int ID { get; set; }

       
        #region thông tin tài khoản
        /// <summary>
        /// Tên đăng nhập
        /// </summary>
        [DataInfo]
        public string LoginName { get; set; }

        /// <summary>
        /// Email xác thực tài khoản
        /// </summary>
        [DataInfo]
        public string Email { get; set; }

        /// <summary>
        /// Mật khẩu
        /// </summary>
        [DataInfo]
        public string Password { get; set; }

        /// <summary>
        /// Mật khẩu để reset
        /// </summary>
        [DataInfo]
        public string TempPassword { get; set; }

        /// <summary>
        /// Mật khẩu
        /// </summary>
        [DataInfo]
        public string VrCode { get; set; }

        #endregion

        #region thông tin cá nhân

        /// <summary>
        /// Họ tên đầy đủ
        /// </summary>
        [DataInfo]
        public override string Name { get; set; }

        /// <summary>
        /// Ảnh đại diện
        /// </summary>
        [DataInfo]
        public string Avatar { get; set; }


        /// <summary>
        /// SĐT
        /// </summary>
        [DataInfo]
        public string Phone { get; set; }

        /// <summary>
        /// Giới tính
        /// </summary>
        [DataInfo]
        public bool Gender { get; set; }

        /// <summary>
        /// Nội dung ghi chú
        /// </summary>
        [DataInfo]
        public string Content { get; set; }

        #endregion

        /// <summary>
        /// Ngày tạo
        /// </summary>
        [DataInfo]
        public DateTime Created { get; set; }

        /// <summary>
        /// Thời gian cập nhật
        /// </summary>
        [DataInfo]
        public DateTime Updated { get; set; }

        /// <summary>
        /// Khóa hoặc cho phép
        /// </summary>
        [DataInfo]
        public bool Activity { get; set; }

        #endregion Autogen by RDV

        #region get someone

        
        #endregion
    }

    public class ModUserService : ServiceBase<ModUserEntity>
    {
        #region Autogen by RDV

        private ModUserService() : base("[Mod_User]")
        {
        }

        private static ModUserService _instance;
        public static ModUserService Instance => _instance ?? (_instance = new ModUserService());

        #endregion Autogen by RDV

        public ModUserEntity GetByID(int id)
        {
            return CreateQuery()
                   .Where(o => o.ID == id)
                   .ToSingle();
        }

        public ModUserEntity GetByID_Cache(int id)
        {
            return CreateQuery()
                   .Where(o => o.ID == id)
                   .ToSingle_Cache();
        }

        public ModUserEntity GetByCode_Cache(string code)
        {
            return CreateQuery()
                   .Where(o => o.LoginName == code)
                   .ToSingle_Cache();
        }


        public ModUserEntity GetByLoginName(string loginName)
        {
            return base.CreateQuery()
                    .Where(o => o.LoginName == loginName)
                    .ToSingle();
        }


        public ModUserEntity GetByPhone(string phone)
        {
            return base.CreateQuery()
                    .Where(o => o.Phone == phone)
                    .ToSingle();
        }

        public bool CheckPhone(string phone)
        {
            return base.CreateQuery()
                   .Where(o => o.Phone == phone)
                   .Count()
                   .ToValue()
                   .ToBool();
        }


        public int GetCount()
        {
            return CreateQuery()
                .Select(o => o.ID)
                .Count()
                .ToValue_Cache()
                .ToInt(0);
        }

        public ModUserEntity GetByEmail(string email)
        {
            return CreateQuery()
                    .Where(o => o.Email == email)
                    .ToSingle();
        }

        public bool CheckEmail(string email)
        {
            return CreateQuery()
                   .Where(o => o.Email == email)
                   .Count()
                   .ToValue()
                   .ToBool();
        }

        public ModUserEntity GetByCode(string code)
        {
            return CreateQuery()
                    .Where(o => o.LoginName == code)
                    .ToSingle();
        }

        public bool CheckLoginName(string loginName)
        {
            return CreateQuery()
                   .Where(o => o.LoginName == loginName)
                   .Count()
                   .ToValue()
                   .ToBool();
        }

        public ModUserEntity GetForLogin(int id)
        {
            if (id < 1) return null;

            return CreateQuery()
                      .Where(o => o.ID == id)
                      .ToSingle();
        }

        public ModUserEntity GetForLogin(string email, string md5_passwd)
        {
            if (Global.Utils.IsEmailAddress(email))
            {
                var User = CreateQuery()
                                    .Where(o => o.Email == email && (o.Password == md5_passwd || o.TempPassword == md5_passwd))
                                    .ToSingle();

                if (User != null && !string.IsNullOrEmpty(User.TempPassword))
                {
                    if (User.TempPassword == md5_passwd) User.Password = md5_passwd;
                    User.TempPassword = string.Empty;
                    Save(User);
                }

                return User;
            }
            else if (Global.Utils.IsLoginName(email))
            {
                var User = CreateQuery()
                                    .Where(o => o.LoginName == email && (o.Password == md5_passwd || o.TempPassword == md5_passwd))
                                    .ToSingle();

                if (User != null && !string.IsNullOrEmpty(User.TempPassword))
                {
                    if (User.TempPassword == md5_passwd) User.Password = md5_passwd;
                    User.TempPassword = string.Empty;
                    Save(User);
                }

                return User;
            }
            else
            {
                var User = CreateQuery()
                                   .Where(o => o.Phone == email && (o.Password == md5_passwd || o.TempPassword == md5_passwd))
                                   .ToSingle();

                if (User != null && !string.IsNullOrEmpty(User.TempPassword))
                {
                    if (User.TempPassword == md5_passwd) User.Password = md5_passwd;
                    User.TempPassword = string.Empty;
                    Save(User);
                }

                return User;
            }
        }
    }
}