﻿using System.Collections.Generic;
using CoreMr.Reddevil.Models;

namespace Reddevil.Lib.Models
{
    /// <summary>
    /// Bảng lưu ảnh liên quan tới các nhà khoa học
    /// </summary>
    public class ModCpdImageEntity : EntityBase
    {
        #region Autogen by RDV

        /// <summary>
        /// ID của nhà khoa học - album ảnh - tin liên quan...vv
        /// </summary>
        [DataInfo]
        public int ObjID { get; set; }

        /// <summary>
        /// Đường dẫn ảnh
        /// </summary>
        [DataInfo]
        public string PathImage { get; set; }

        /// <summary>
        /// Loại dữ liệu
        /// Avatar - ảnh đại diện của nhà khoa học
        /// ...chưa nghĩ ra
        /// </summary>
        [DataInfo]
        public string TypeObj { get; set; }

        #endregion Autogen by RDV

    }

    public class ModCpdImageService : ServiceBase<ModCpdImageEntity>
    {
        #region Autogen by RDV

        public ModCpdImageService() : base("[cpd_Image]")
        {
            DBConfigKey = "ConnectToCpd";
        }

        private static ModCpdImageService _instance;
        public static ModCpdImageService Instance => _instance ?? (_instance = new ModCpdImageService());

        #endregion Autogen by RDV

        public ModCpdImageEntity GetByID(int id)
        {
            return CreateQuery()
                .Where(o => o.ID == id)
                .ToSingle();
        }

        public ModCpdImageEntity GetByID_Cache(int id)
        {
            return CreateQuery()
                .Where(o => o.ID == id)
                .ToSingle_Cache();
        }

        public bool Exists(int productID, string file, string _type)
        {
            return CreateQuery()
                .Where(o => o.ObjID == productID && o.PathImage == file && o.TypeObj == _type)
                .Count()
                .ToValue_Cache()
                .ToBool();
        }

        public List<ModCpdImageEntity> GetAll_Cache(int productID, string _type)
        {
            return CreateQuery()
                .Where(o => o.ObjID == productID && o.TypeObj == _type)
                .ToList_Cache();
        }

        public void InsertOrUpdate(int objectID, string[] arrFile, string _type)
        {
            if (arrFile == null || arrFile.Length == 0)
            {
                Delete(o => o.ObjID == objectID && o.TypeObj == _type);
                return;
            }

            var listInDb = CreateQuery()
                                .Where(o => o.ObjID == objectID && o.TypeObj == _type)
                                .ToList_Cache();

            for (var i = 0; listInDb != null && i < listInDb.Count; i++)
            {
                if (System.Array.IndexOf(arrFile, listInDb[i].PathImage) < 0)
                    Delete("[ObjID]=" + listInDb[i].ObjID);
            }

            foreach (var file in arrFile)
            {
                if (string.IsNullOrEmpty(file)) continue;

                var file1 = file;
                var item = CreateQuery().Where(o => o.ObjID == objectID && o.PathImage == file1 && o.TypeObj == _type).ToSingle_Cache();
                if (item != null) continue;

                item = new ModCpdImageEntity()
                {
                    ObjID = objectID,
                    PathImage = file,
                    TypeObj = _type
                };

                Save(item);
            }
        }
    }
}