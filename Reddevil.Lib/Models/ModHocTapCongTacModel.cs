﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using CoreMr.Reddevil.Models;

namespace Reddevil.Lib.Models
{
    public class ModHocTapCongTacEntity : EntityBase
    {
        #region Autogen by RDV

        [DataInfo]
        public int HocTapCongTacID { get; set; }

        [DataInfo]
        public string ThoiGian { get; set; }

        [DataInfo]
        public int TuNam { get; set; }

        [DataInfo]
        public int DenNam { get; set; }

        [DataInfo]
        public string HoatDong { get; set; }

        [DataInfo]
        public int NKHID { get; set; }

        #endregion Autogen by RDV


    }

    public class ModHocTapCongTacService : ServiceBase<ModHocTapCongTacEntity>
    {
        #region Autogen by RDV

        public ModHocTapCongTacService()
            : base("[cpd_HocTapCongTac]")
        {
            DBConfigKey = "ConnectToCpd";
        }

        private static ModHocTapCongTacService _instance;
        public static ModHocTapCongTacService Instance
        {
            get { return _instance ?? (_instance = new ModHocTapCongTacService()); }
        }

        #endregion Autogen by RDV

        public ModHocTapCongTacEntity GetByID(int id)
        {
            return CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle();
        }

        public ModHocTapCongTacEntity GetByID_Cache(int id)
        {
            return CreateQuery()
               .Where(o => o.HocTapCongTacID == id)
               .ToSingle_Cache();
        }

    }
}