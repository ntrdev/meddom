﻿using System.Collections.Generic;
using CoreMr.Reddevil.Models;

namespace Reddevil.Lib.Models
{
    public class ModAINewsByCategoryEntity : EntityBase
    {
        #region Autogen by RDV

        [DataInfo]
        public int NewsId { get; set; }


        [DataInfo]
        public int CategoryId { get; set; }

        #endregion Autogen by RDV
    }

    public class ModAINewsByCategoryService : ServiceBase<ModAINewsByCategoryEntity>
    {
        #region Autogen by RDV

        public ModAINewsByCategoryService()
            : base("[AINewsByCategory]")
        {
            DBConfigKey = "ConnectToCpd1";
        }

        private static ModAINewsByCategoryService _Instance = null;
        public static ModAINewsByCategoryService Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new ModAINewsByCategoryService();

                return _Instance;
            }
        }

        #endregion

        public ModAINewsByCategoryEntity GetByID(int id)
        {
            return base.CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle();
        }

    }
}