﻿using CoreMr.Reddevil.Models;

namespace Reddevil.Lib.Models
{
    public class WebSettingEntity : EntityBase
    {
        #region Autogen by RDV

        [DataInfo]
        public override int ID { get; set; }

        [DataInfo]
        public override string Name { get; set; }

        [DataInfo]
        public string Code { get; set; }

        [DataInfo]
        public long Value { get; set; }

        #endregion Autogen by RDV
    }

    public class WebSettingService : ServiceBase<WebSettingEntity>
    {
        #region Autogen by RDV

        public WebSettingService()
            : base("[Web_Setting]")
        {
        }

        private static WebSettingService _instance;

        public static WebSettingService Instance => _instance ?? (_instance = new WebSettingService());

        #endregion Autogen by RDV
    }
}