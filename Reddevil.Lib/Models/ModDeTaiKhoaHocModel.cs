﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using CoreMr.Reddevil.Models;

namespace Reddevil.Lib.Models
{
    public class ModDeTaiKhoaHocEntity : EntityBase
    {
        #region Autogen by RDV

        [DataInfo]
        public int DeTaiKhoaHocID { get; set; }

        [DataInfo]
        public string TenDeTai { get; set; }

        [DataInfo]
        public string NguoiThucHien { get; set; }

        [DataInfo]
        public string DuongDanFile { get; set; }

        [DataInfo]
        public string NamThucHienTu { get; set; }

        [DataInfo]
        public string NamThucHienDen { get; set; }

        [DataInfo]
        public string MaSo { get; set; }

        [DataInfo]
        public int NKHID { get; set; }

        #endregion Autogen by RDV


    }

    public class ModDeTaiKhoaHocService : ServiceBase<ModDeTaiKhoaHocEntity>
    {
        #region Autogen by RDV

        public ModDeTaiKhoaHocService()
            : base("[cpd_DeTaiKhoaHoc]")
        {
            DBConfigKey = "ConnectToCpd";
        }

        private static ModDeTaiKhoaHocService _instance;
        public static ModDeTaiKhoaHocService Instance
        {
            get { return _instance ?? (_instance = new ModDeTaiKhoaHocService()); }
        }

        #endregion Autogen by RDV

        public ModDeTaiKhoaHocEntity GetByID(int id)
        {
            return CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle();
        }

        public ModDeTaiKhoaHocEntity GetByID_Cache(int id)
        {
            return CreateQuery()
               .Where(o => o.DeTaiKhoaHocID == id)
               .ToSingle_Cache();
        }

    }
}