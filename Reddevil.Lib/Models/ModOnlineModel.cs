﻿using System.Collections.Generic;
using CoreMr.Reddevil.Models;

namespace Reddevil.Lib.Models
{
    public class ModOnlineEntity : EntityBase
    {
        #region Autogen by RDV
        

        [DataInfo]
        public int UserID { get; set; }

        //[DataInfo]
        //public int PageID { get; set; }

        [DataInfo]
        public long TimeValue { get; set; }

        [DataInfo]
        public string SessionID { get; set; }

        [DataInfo]
        public string ConnectionID { get; set; }

        [DataInfo]
        public string ModuleCode { get; set; }

        [DataInfo]
        public string IP { get; set; }

        [DataInfo]
        public bool IsLogin { get; set; }

        #endregion Autogen by RDV
    }

    public class ModOnlineService : ServiceBase<ModOnlineEntity>
    {
        #region Autogen by RDV

        public ModOnlineService()
            : base("[Mod_Online]")
        {

        }

        private static ModOnlineService _Instance = null;
        public static ModOnlineService Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new ModOnlineService();

                return _Instance;
            }
        }

        #endregion

        public ModOnlineEntity GetByID(int id)
        {
            return base.CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle();
        }


        public ModOnlineEntity GetByUserID_IsLogin(int userID)
        {
            return base.CreateQuery()
               .Where(o => o.UserID == userID && o.IsLogin == true)
               //.OrderByDesc(o => new { o.ID })
               .ToSingle();
        }

        //public ModOnlineEntity GetByUserID_Browser(int userID, string browserUser)
        //{
        //    return base.CreateQuery()
        //       .Where(o => o.WebUserID == userID && o.Browser == browserUser)
        //       .OrderByDesc(o => new { o.ID })
        //       .ToSingle();
        //}

        public ModOnlineEntity GetByConnectionID_IsLogin(string connectionID)
        {
            return base.CreateQuery()
               .Where(o => o.ConnectionID == connectionID && o.IsLogin == true)
               .ToSingle();
        }

        public List<ModOnlineEntity> GetList_IsLogin()
        {
            return base.CreateQuery()
               .Where(o => o.IsLogin == true)
               .ToList();
        }
        public bool CheckIsOnline(int userID)
        {

            return base.CreateQuery()
                         .Where(o => o.UserID == userID && o.IsLogin == true)
                         .Count()
                         .ToValue()
                         .ToBool();
        }
    }
}