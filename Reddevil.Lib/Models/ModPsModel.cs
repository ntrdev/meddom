﻿using System;
using System.Collections.Generic;
using CoreMr.Reddevil.Models;

namespace Reddevil.Lib.Models
{
    /// <summary>
    /// Lưu bút
    /// </summary>
    public class ModPSEntity : EntityBase
    {
        #region Autogen by RDV

        [DataInfo]
        public override int ID { get; set; }

        [DataInfo]
        public override string Name { get; set; }

        [DataInfo]
        public int CategoryID { get; set; }

        [DataInfo]
        public string Url { get; set; }
      
        [DataInfo]
        public int Order { get; set; }

        [DataInfo]
        public int Views { get; set; }

        [DataInfo]
        public string Description { get; set; }

        [DataInfo]
        public string Image { get; set; }

        [DataInfo]
        public string CPUser { get; set; }

        [DataInfo]
        public bool Activity { get; set; }

        [DataInfo]
        public DateTime DateCreated { get; set; } = DateTime.Now;

        [DataInfo]
        public int State { get; set; }
        #endregion Autogen by RDV

        public void UpView()
        {
            Views++;
            ModPSService.Instance.Save(this, o => o.Views);
        }
    
        private WebMenuEntity _oMenu;
        public WebMenuEntity GetMenu()
        {
            if (_oMenu == null && CategoryID > 0)
                _oMenu = WebMenuService.Instance.GetByID_Cache(CategoryID);

            return _oMenu ?? (_oMenu = new WebMenuEntity());
        }
        private List<ModDynamicFileEntity> _oGetFile;
        public List<ModDynamicFileEntity> GetFile
        {
            get
            {
                if (_oGetFile == null && this.ID > 0)
                    _oGetFile = ModDynamicFileService.Instance.CreateQuery()
                                                    .Where(o => o.ObjectID == ID && o.Type == 6)
                                                    .ToList_Cache();
                return _oGetFile;
            }
        }
    }

    public class ModPSService : ServiceBase<ModPSEntity>
    {
        #region Autogen by RDV

        public ModPSService() : base("[Mod_PS]"){ }

        private static ModPSService _instance;
        public static ModPSService Instance => _instance ?? (_instance = new ModPSService());

        #endregion Autogen by RDV

        public ModPSEntity GetByID(int id)
        {
            return CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle();
        }

    }
}