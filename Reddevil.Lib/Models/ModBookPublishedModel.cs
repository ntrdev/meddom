﻿using System.Collections.Generic;
using CoreMr.Reddevil.Models;

namespace Reddevil.Lib.Models
{
    public class ModBookPublishedEntity : EntityBase
    {
        #region Autogen by RDV
        

        [DataInfo]
        public int SachDaXuatBanID { get; set; }

        [DataInfo]
        public int NKHID { get; set; }

        [DataInfo]
        public string TieuDeSach { get; set; }

        [DataInfo]
        public string TacGia { get; set; }

        [DataInfo]
        public int NXBID { get; set; }

        [DataInfo]
        public int NamXuatBan { get; set; }

        #endregion Autogen by RDV
    }

    public class ModBookPublishedService : ServiceBase<ModBookPublishedEntity>
    {
        #region Autogen by RDV

        public ModBookPublishedService()
            : base("[cpd_SachDaXuatBan]")
        {
            DBConfigKey = "ConnectToCpd";
        }

        private static ModBookPublishedService _Instance = null;
        public static ModBookPublishedService Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new ModBookPublishedService();

                return _Instance;
            }
        }

        #endregion

        public ModBookPublishedEntity GetByID(int id)
        {
            return base.CreateQuery()
               .Where(o => o.SachDaXuatBanID == id)
               .ToSingle();
        }

    }
}