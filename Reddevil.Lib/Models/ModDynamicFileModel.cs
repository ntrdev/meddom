﻿using System.Collections.Generic;
using CoreMr.Reddevil.Models;

namespace Reddevil.Lib.Models
{
    public class ModDynamicFileEntity : EntityBase
    {
        #region Autogen by RDV

        [DataInfo]
        public override int ID { get; set; }

        [DataInfo]
        public int ObjectID { get; set; }

        [DataInfo]
        public string File { get; set; }

        [DataInfo]
        public int Type { get; set; }

        [DataInfo]
        public bool Default { get; set; }

        [DataInfo]
        public int Order { get; set; }

        [DataInfo]
        public bool Activity { get; set; }

        #endregion Autogen by RDV

    }

    public class ModDynamicFileService : ServiceBase<ModDynamicFileEntity>
    {
        #region Autogen by RDV

        public ModDynamicFileService() : base("[Mod_DynamicFile]")
        {
        }

        private static ModDynamicFileService _instance;
        public static ModDynamicFileService Instance => _instance ?? (_instance = new ModDynamicFileService());

        #endregion Autogen by RDV

        public ModDynamicFileEntity GetByID(int id)
        {
            return CreateQuery()
                .Where(o => o.ID == id)
                .ToSingle();
        }

        public ModDynamicFileEntity GetByID_Cache(int id)
        {
            return CreateQuery()
                .Where(o => o.ID == id)
                .ToSingle_Cache();
        }

        public bool Exists(int productID, string file, int _type)
        {
            return CreateQuery()
                .Where(o => o.ObjectID == productID && o.File == file && o.Type == _type)
                .Count()
                .ToValue_Cache()
                .ToBool();
        }

        public List<ModDynamicFileEntity> GetAll_Cache(int productID, int _type)
        {
            return CreateQuery()
                .Where(o => o.ObjectID == productID && o.Type == _type)
                .OrderByAsc(o => o.Order)
                .ToList_Cache();
        }

        public void InsertOrUpdate(int objectID, string[] arrFile, int _type)
        {
            if (arrFile == null || arrFile.Length == 0)
            {
                Delete(o => o.ObjectID == objectID && o.Type == _type);
                return;
            }

            var listInDb = CreateQuery()
                                .Where(o => o.ObjectID == objectID && o.Type == _type)
                                .ToList_Cache();

            for (var i = 0; listInDb != null && i < listInDb.Count; i++)
            {
                if (System.Array.IndexOf(arrFile, listInDb[i].File) < 0)
                    Delete(listInDb[i]);
            }

            foreach (var file in arrFile)
            {
                if (string.IsNullOrEmpty(file)) continue;

                var file1 = file;
                var item = CreateQuery().Where(o => o.ObjectID == objectID && o.File == file1 && o.Type == _type).ToSingle_Cache();
                if (item != null) continue;

                item = new ModDynamicFileEntity()
                {
                    ObjectID = objectID,
                    File = file,
                    Default = false
                };

                Save(item);
            }
        }
    }
}