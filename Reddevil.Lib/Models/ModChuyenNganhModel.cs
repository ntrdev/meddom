﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using CoreMr.Reddevil.Models;

namespace Reddevil.Lib.Models
{
    public class ModChuyenNganhEntity : EntityBase
    {
        #region Autogen by RDV

        [DataInfo]
        public int Id { get; set; }

        [DataInfo]
        public int ParentId { get; set; }

        [DataInfo]
        public override string Name { get; set; }

        [DataInfo]
        public  string Url { get; set; }

        [DataInfo]
        public int LevelNo { get; set; }

        #endregion Autogen by RDV


    }

    public class ModChuyenNganhService : ServiceBase<ModChuyenNganhEntity>
    {
        #region Autogen by RDV

        public ModChuyenNganhService()
            : base("[AIMenus]")
        {
            DBConfigKey = "ConnectToCpd1";
        }

        private static ModChuyenNganhService _instance;
        public static ModChuyenNganhService Instance
        {
            get { return _instance ?? (_instance = new ModChuyenNganhService()); }
        }

        #endregion Autogen by RDV

        public ModChuyenNganhEntity GetByID(int id)
        {
            return CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle();
        }

        public ModChuyenNganhEntity GetByID_Cache(int id)
        {
            return CreateQuery()
               .Where(o => o.Id == id)
               .ToSingle_Cache();
        }
        public ModChuyenNganhEntity GetByParentID_Cache(int id)
        {
            return CreateQuery()
               .Where(o => o.ParentId == id)
               .ToSingle_Cache();
        }

        public List<ModChuyenNganhEntity> GetListByParentID_Cache(int id)
        {
            return CreateQuery()
               .Where(o => o.ParentId == id)
               .ToList_Cache();
        }
    }
}