﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using CoreMr.Reddevil.Models;

namespace Reddevil.Lib.Models
{
    public class ModChuyenDeNghienCuuEntity : EntityBase
    {
        #region Autogen by RDV

        [DataInfo]
        public int ChuyenDeNghienCuuID { get; set; }

        [DataInfo]
        public int NganhID { get; set; }

        [DataInfo]
        public int NKHID { get; set; }

        #endregion Autogen by RDV


    }

    public class ModChuyenDeNghienCuuService : ServiceBase<ModChuyenDeNghienCuuEntity>
    {
        #region Autogen by RDV

        public ModChuyenDeNghienCuuService()
            : base("[cpd_ChuyenDeNghienCuu]")
        {
            DBConfigKey = "ConnectToCpd";
        }

        private static ModChuyenDeNghienCuuService _instance;
        public static ModChuyenDeNghienCuuService Instance
        {
            get { return _instance ?? (_instance = new ModChuyenDeNghienCuuService()); }
        }

        #endregion Autogen by RDV

        public ModChuyenDeNghienCuuEntity GetByID(int id)
        {
            return CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle();
        }

        public ModChuyenDeNghienCuuEntity GetByID_Cache(int id)
        {
            return CreateQuery()
               .Where(o => o.ChuyenDeNghienCuuID == id)
               .ToSingle_Cache();
        }

    }
}