﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using CoreMr.Reddevil.Models;

namespace Reddevil.Lib.Models
{
    public class ModTaiLieuCpdEntity : EntityBase
    {
        #region Autogen by RDV

        [DataInfo]
        public int TaiLieuCpdID { get; set; }

        [DataInfo]
        public string TenPhoThong { get; set; }

        [DataInfo]
        public string SoDangKy { get; set; }

        [DataInfo]
        public string TomTatNoiDung { get; set; }

        [DataInfo]
        public string TacGia { get; set; }

        [DataInfo]
        public string LoaiHinhKho { get; set; }

        [DataInfo]
        public int NKHID { get; set; }

        #endregion Autogen by RDV


    }

    public class ModTaiLieuCpdService : ServiceBase<ModTaiLieuCpdEntity>
    {
        #region Autogen by RDV

        public ModTaiLieuCpdService()
            : base("[cpd_TaiLieuCpd]")
        {
            DBConfigKey = "ConnectToCpd";
        }

        private static ModTaiLieuCpdService _instance;
        public static ModTaiLieuCpdService Instance
        {
            get { return _instance ?? (_instance = new ModTaiLieuCpdService()); }
        }

        #endregion Autogen by RDV

        public ModTaiLieuCpdEntity GetByID(int id)
        {
            return CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle();
        }

        public ModTaiLieuCpdEntity GetByID_Cache(int id)
        {
            return CreateQuery()
               .Where(o => o.TaiLieuCpdID == id)
               .ToSingle_Cache();
        }

    }
}