﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using CoreMr.Reddevil.Models;

namespace Reddevil.Lib.Models
{
    public class ModScientistValueEntity : EntityBase
    {
        #region Autogen by RDV

        [DataInfo]
        public int Id { get; set; }

        [DataInfo]
        public int DoctorId { get; set; }

        [DataInfo]
        public override string Name { get; set; }

        [DataInfo]
        public string Value { get; set; }

        #endregion Autogen by RDV


    }

    public class ModScientistValueService : ServiceBase<ModScientistValueEntity>
    {
        #region Autogen by RDV

        public ModScientistValueService()
            : base("[TNDoctorValue]")
        {
            DBConfigKey = "ConnectToCpd1";
        }

        private static ModScientistValueService _instance;
        public static ModScientistValueService Instance
        {
            get { return _instance ?? (_instance = new ModScientistValueService()); }
        }

        #endregion Autogen by RDV

        public ModScientistValueEntity GetByID(int id)
        {
            return CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle();
        }

        public ModScientistValueEntity GetByID_Cache(int id)
        {
            return CreateQuery()
               .Where(o => o.Id == id)
               .ToSingle_Cache();
        }
        public ModScientistValueEntity GetByParentID_Cache(int id)
        {
            return CreateQuery()
               .Where(o => o.DoctorId == id)
               .ToSingle_Cache();
        }

    }
}