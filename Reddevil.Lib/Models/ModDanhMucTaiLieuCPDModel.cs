﻿using System.Collections.Generic;
using CoreMr.Reddevil.Models;

namespace Reddevil.Lib.Models
{
    public class ModDanhMucTaiLieuCPDEntity : EntityBase
    {
        #region Autogen by RDV
        

        [DataInfo]
        public int DanhMucTaiLieuCPDID { get; set; }

        [DataInfo]
        public int NKHID { get; set; }

        [DataInfo]
        public string TenPhoThong { get; set; }

        [DataInfo]
        public string SoDangKy { get; set; }

        [DataInfo]
        public string TomTatNoiDung { get; set; }

        [DataInfo]
        public string TacGia { get; set; }

        [DataInfo]
        public string LoaiHinhKho { get; set; }


        #endregion Autogen by RDV
    }

    public class ModDanhMucTaiLieuCPDService : ServiceBase<ModDanhMucTaiLieuCPDEntity>
    {
        #region Autogen by RDV

        public ModDanhMucTaiLieuCPDService()
            : base("[cpd_TaiLieuCpd]")
        {
            DBConfigKey = "ConnectToCpd";
        }

        private static ModDanhMucTaiLieuCPDService _Instance = null;
        public static ModDanhMucTaiLieuCPDService Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new ModDanhMucTaiLieuCPDService();

                return _Instance;
            }
        }

        #endregion

        public ModDanhMucTaiLieuCPDEntity GetByID(int id)
        {
            return base.CreateQuery()
               .Where(o => o.DanhMucTaiLieuCPDID == id)
               .ToSingle();
        }

    }
}