﻿using System;
using System.Collections.Generic;
using CoreMr.Reddevil.Models;

namespace Reddevil.Lib.Models
{
    public class ModALLNEWSEntity : EntityBase
    {
        #region Autogen by RDV
        

        [DataInfo]
        public long Id { get; set; }

        [DataInfo]
        public string Title { get; set; }

        [DataInfo]
        public string Sumary { get; set; }

        [DataInfo]
        public string Contents { get; set; }

        [DataInfo]
        public string ImagePath { get; set; }

        [DataInfo]
        public DateTime CreatedDate { get; set; }

        #endregion Autogen by RDV
    }

    public class ModALLNEWSService : ServiceBase<ModALLNEWSEntity>
    {
        #region Autogen by RDV

        public ModALLNEWSService()
            : base("[TNItemStory]")
        {
            DBConfigKey = "ConnectToCpd1";
        }

        private static ModALLNEWSService _Instance = null;
        public static ModALLNEWSService Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new ModALLNEWSService();

                return _Instance;
            }
        }

        #endregion

        public ModALLNEWSEntity GetByID(int id)
        {
            return base.CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle();
        }

    }
}