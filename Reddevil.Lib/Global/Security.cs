﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Reddevil.Lib.Global
{
    public static class Security
    {
        public static string Md5(string s)
        {
            var bytes = new UnicodeEncoding().GetBytes(s);
            var hasBytes = ((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(bytes);
            return BitConverter.ToString(hasBytes);
        }

        static string sDefaultWHEEL1 = "ABCDEFGHIJKLMNOPQRSTVUWXYZabcdefghijklmnopqrstuvwxyz1357986420-+*/;.,?><=|[] ";
        static string sDefaultWHEEL2 = "BNMQRYUASD-+*/;.,?><=|[]XCIWEHJKTLZVOPFG0246897531qwert yuiopasdfghjklzxcvbnm";
        /// <summary>
        /// Mã hóa chuỗi ký tự
        /// </summary>
        /// <param name="sINPUT"> Chuỗi ký tự đầu vào</param>
        /// <returns> Trả ra chuỗi mã hóa</returns>
        public static string Encrypt(string sINPUT)
        {
            if (sINPUT == null) return "";
            string sWHEEL1 = (string)sDefaultWHEEL1.Clone();
            string sWHEEL2 = (string)sDefaultWHEEL2.Clone();
            int k, i;
            string sRESULT = "";
            char C;

            for (i = 0; i < sINPUT.Length; i++)
            {
                C = sINPUT[i];
                k = sWHEEL1.IndexOf(C);
                if (k == -1) sRESULT += C.ToString();
                else sRESULT += sWHEEL2[k].ToString();
                sWHEEL1 = LeftShift(sWHEEL1);
                sWHEEL2 = RightShift(sWHEEL2);
            }

            return sRESULT;
        }
        /// <summary>
        /// Giải mã chuỗi ký tự
        /// </summary>
        /// <param name="sINPUT"> Chuỗi ký tự đầu vào</param>
        /// <returns> Chuỗi ký tự được giải mã</returns>
        public static string Decrypt(string sINPUT)
        {
            if (sINPUT == null) return "";
            string sWHEEL1 = (string)sDefaultWHEEL1.Clone();
            string sWHEEL2 = (string)sDefaultWHEEL2.Clone();
            int k, i;
            string sRESULT = "";
            char C;

            for (i = 0; i < sINPUT.Length; i++)
            {
                C = sINPUT[i];
                k = sWHEEL2.IndexOf(C);
                if (k == -1) sRESULT += C.ToString();
                else sRESULT += sWHEEL1[k].ToString();
                sWHEEL1 = LeftShift(sWHEEL1);
                sWHEEL2 = RightShift(sWHEEL2);
            }
            return sRESULT;
        }

        private static string LeftShift(string S)
        {
            int len = S.Length;
            string s = "";
            if (len > 0) s = S.Substring(1, len - 1) + S.Substring(0, 1);
            return s;
        }

        private static string RightShift(string S)
        {
            int len = S.Length;
            string s = "";
            if (len > 0) s = S.Substring(len - 1, 1) + S.Substring(0, len - 1);
            return s;
        }
    }
}