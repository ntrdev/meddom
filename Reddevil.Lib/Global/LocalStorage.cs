﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Web;

namespace VSW.Lib.Global
{
    public class LocalStorage
    {
        public static Dictionary<string, string> GetData()
        {
            Dictionary<string, string> dicItem = new Dictionary<string,string>();

            using (var conn = new SQLiteConnection("Data Source=https_giaodichtrungquoc.com_0.localstorage;Version=3;", true))
            using (var cmd = conn.CreateCommand())
            {
                conn.Open();
                cmd.CommandText = "SELECT key, value FROM ItemTable";
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        string s1 = reader.GetString(reader.GetOrdinal("key"));
                        string s2 = reader.GetString(reader.GetOrdinal("value"));

                        dicItem.Add(reader.GetString(reader.GetOrdinal("key")), reader.GetString(reader.GetOrdinal("value")));

                        //Console.WriteLine(
                        //    "key: {0}, value: {1}",
                        //    reader.GetString(reader.GetOrdinal("key")),
                        //    reader.GetString(reader.GetOrdinal("value"))
                        //);
                    }
                }
            }

            return dicItem;
        }
    }
}
