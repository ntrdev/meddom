using System;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Reddevil.Lib.Models;

namespace Reddevil.Lib.Global
{
    public class TextToSpeech
    {

        public static void SaveFileAudio(string content, int idNews)
        {
            Content content1 = new Content();
            content1.ID = idNews;
            content1.Text = content;
            content1.UrlPost = Setting.TTS_PostUrl;
            content1.Token = Setting.TTS_Key;
            content1.UrlGet = Setting.TTS_GetAudio;
            Thread t = new Thread(ThreadTextToSpeech);
            t.Start(content1);

        }

        private static void ThreadTextToSpeech(object content)
        {

            string urlAudio = "";
            var _content = (Content)content;
            Task t = Task.Run(async () =>
            {
                try
                {
                    var httpWebRequest = (HttpWebRequest)WebRequest.Create(_content.UrlPost);
                    httpWebRequest.Accept = "application/json";
                    httpWebRequest.ContentType = "application/json";
                    httpWebRequest.Method = "POST";
                    httpWebRequest.Headers.Add("api-key", _content.Token);

                    using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                    {
                        string jsonToken = JsonConvert.SerializeObject(new
                        {
                            text = _content.Text
                        });

                        streamWriter.Write(jsonToken);
                        streamWriter.Flush();
                        streamWriter.Close();
                    }
                    var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                    var streamReader = new StreamReader(httpResponse.GetResponseStream());

                    var result = await streamReader.ReadToEndAsync();
                    var obj = JsonConvert.DeserializeObject<TextToSpeechResult>(result.ToString());

                    if (obj != null)
                    {

                        var result34 = GetAudio(_content.Token, obj.id, _content.UrlGet);
                        if (result34 != null)
                        {
                            urlAudio = result34.audio_url;

                            WebClient myWebClient = new WebClient();
                            var path = CoreMr.Reddevil.Global.Application.BaseDirectory + "Data/Audio/" + DateTime.Now.Year + "/" + DateTime.Now.Month + "/";
                            if (!System.IO.Directory.Exists(path))
                                System.IO.Directory.CreateDirectory(path);

                            string mnsc = (DateTime.Now.ToString("dd") + DateTime.Now.Millisecond).ToString();
                            path = path + "/" + mnsc + _content.ID.ToString() + ".mp3";
                            // Download the Web resource and save it into the current filesystem folder.
                            myWebClient.DownloadFile(urlAudio, path);
                            string url = "/Data/Audio/" + DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + mnsc + _content.ID.ToString() + ".mp3";
                            urlAudio = url;
                            Error.Write("SUCCESS SAVE FILE TO PATH: " + urlAudio);
                        }

                        Save(_content.ID, urlAudio);

                    }
                }
                catch (Exception ex)
                {
                    Error.Write(ex);
                }
            });

        }


        private static void Save(int idNews, string urlAudio)
        {
            try
            {
                var newsGarniture = ModNewsGarnitureService.Instance.GetByID(idNews);
                if (newsGarniture != null)
                {
                    newsGarniture.UrlAudio = urlAudio;
                    ModNewsGarnitureService.Instance.Save(newsGarniture);
                }
            }
            catch (Exception ex)
            {
                Error.Write(ex);
                throw;
            }

        }

        private static AudioResutlt GetAudio(string token, string id, string geturl)
        {
            bool success = false;
            AudioResutlt convert = null; int run = 0;
            while (!success && run < 11)
            {
                try
                {
                    string _UrlGetAutio = geturl + id;
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(_UrlGetAutio);
                    request.Method = "GET";
                    request.Headers.Add("api-key", token);
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        success = true;
                        var streamReader = new StreamReader(response.GetResponseStream());
                        var result = streamReader.ReadToEnd();
                        convert = JsonConvert.DeserializeObject<AudioResutlt>(result.ToString());

                        response.Close();
                        streamReader.Close();
                        if (string.IsNullOrEmpty(convert.audio_url) || convert.audio_url == "null")
                        {
                            success = false;
                            throw new Exception("Audio_url: NULL");
                        }
                    }
                }
                catch (Exception ex)
                {
                    run++;
                    Error.Write("CURENT THREAD: " + run + "\r Seconds: " + (run * 30) + "s\r Id: " + id + "\r ERROR: " + ex.Message);
                    Thread.Sleep(30000);
                }
            }
            return convert;
        }
        private class Content
        {
            public int ID { get; set; }
            public string Text { get; set; }
            public string UrlPost { get; set; }
            public string Token { get; set; }
            public string UrlGet { get; set; }

        }
        private class AudioResutlt
        {
            public DateTime create_time { get; set; }
            public DateTime update_time { get; set; }
            public string id { get; set; }
            public int code { get; set; }
            public string message { get; set; }
            public string text_id { get; set; }
            public string audio_url { get; set; }
        }

        private class TextToSpeechResult
        {
            public DateTime create_time { get; set; }
            public DateTime update_time { get; set; }
            public string id { get; set; }
            public string name { get; set; }
            public string user_id { get; set; }
            public string user_webhook_url { get; set; }
            public string audiosource_id { get; set; }
            public int status { get; set; }
            public int retry_attempt { get; set; }
            public string audio_url { get; set; }

        }

    }

}