﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Net;
using System.Web;
using ZXing;
using ZXing.QrCode;
using ZXing.QrCode.Internal;

namespace Reddevil.Lib.Global
{
    public static class Image
    {


        public static void SaveFromUrl(string url, string target)
        {
            try
            {
                var client = new WebClient();
                client.DownloadFile(new Uri(url), target);
            }
            catch (Exception e)
            {
                Error.Write(e.Message);
            }
        }

        public static string GenQrCode(string url, string fileName)
        {
            try
            {

                QrCodeEncodingOptions options = new QrCodeEncodingOptions();
                options = new QrCodeEncodingOptions
                {
                    DisableECI = true,
                    CharacterSet = "UTF-8",
                    Width = 1000,
                    Height = 1000,
                    PureBarcode = false
                };
                options.Hints.Add(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
                var qr = new BarcodeWriter();
                qr.Options = options;
                qr.Format = BarcodeFormat.QR_CODE;
                var result = new Bitmap(qr.Write(url));
                var logo = new Bitmap(HttpContext.Current.Server.MapPath("/interface/pc/images/logo.jpg"));
                var g = Graphics.FromImage(result);
                g.DrawImage(logo, new Point((result.Width - logo.Width) / 2, (result.Height - logo.Height) / 2));

                Bitmap QRCodeImage = result;

                Directory.Create(HttpContext.Current.Server.MapPath("~/Data/Imgm/QrCode/"));
                string target = "~/Data/Imgm/QrCode/" + fileName + ".png";
                var targetPạth = HttpContext.Current.Server.MapPath(target);

                QRCodeImage.Save(targetPạth);
                url = target;
            }

            catch (Exception Ex)
            {
                Error.Write("Encoding exception.\r\n" + Ex.Message);
            }
            return url;
        }



        public static void CompressionImageFile(int settingQuantity, string file, string target)
        {
            var oldBmp = new Bitmap(file);

            //quality image
            var quantity = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, settingQuantity);
            var jpegCodec = GetEncoder("image/jpeg");
            var encoder = new EncoderParameters(1) { Param = { [0] = quantity } };
            oldBmp.Save(target, jpegCodec, encoder);
            oldBmp.Dispose();
        }

        public static void CompressionImageFile(System.Drawing.Image img, int settingQuantity, string target)
        {

            //quality image
            var quantity = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, settingQuantity);
            var jpegCodec = GetEncoder("image/jpeg");
            var encoder = new EncoderParameters(1) { Param = { [0] = quantity } };
            img.Save(target, jpegCodec, encoder);
            img.Dispose();
        }

        public static void ResizeImageFile(int type, string file, string target, string extension, int width, int height, params object[] parameter)
        {
            var oldBmp = new Bitmap(file);

            var oldWidth = oldBmp.Width;
            var oldHeight = oldBmp.Height;

            int newWidth = 0, newHeight = 0, left = 0, top = 0;
            decimal ratio;

            switch (type)
            {
                case 1:
                    newWidth = width == 0 ? oldWidth : width;
                    newHeight = height == 0 ? oldHeight : height;
                    break;

                case 2:
                    newWidth = width == 0 ? (int)(oldWidth * (height / (double)oldHeight)) : width;
                    newHeight = height == 0 ? (int)(oldHeight * (width / (double)oldWidth)) : height;
                    break;

                case 3:
                    newWidth = width == 0 ? oldWidth : (int)(oldWidth * ((double)width / 100));
                    newHeight = height == 0 ? oldHeight : (int)(oldHeight * ((double)height / 100));
                    break;

                case 4:
                    if (oldWidth > oldHeight)
                    {
                        ratio = (decimal)width / oldWidth;
                        newWidth = width;
                        newHeight = (int)(oldHeight * ratio);
                    }
                    else
                    {
                        ratio = (decimal)height / oldHeight;
                        newHeight = height;
                        newWidth = (int)(oldWidth * ratio);
                    }
                    break;

                case 5:
                    if (oldWidth > oldHeight)
                    {
                        ratio = (decimal)width / height;

                        newWidth = (int)(Math.Round(oldHeight * ratio));
                        if (newWidth < oldWidth)
                        {
                            newHeight = oldHeight;
                            left = (oldWidth - newWidth) / 2;
                        }
                        else
                        {
                            newHeight = (int)Math.Round(oldHeight * ((double)oldWidth / newWidth));
                            newWidth = oldWidth;
                            top = (oldHeight - newHeight) / 2;
                        }
                    }
                    else
                    {
                        ratio = (decimal)height / width;

                        newHeight = (int)(Math.Round(oldWidth * ratio));
                        if (newHeight < oldHeight)
                        {
                            newWidth = oldWidth;
                            top = (oldHeight - newHeight) / 2;
                        }
                        else
                        {
                            newWidth = (int)Math.Round(oldWidth * ((double)oldHeight / newHeight));
                            newHeight = oldHeight;
                            left = (oldHeight - newWidth) / 2;
                        }
                    }
                    break;
            }

            var newBmp = new Bitmap(newWidth, newHeight);
            var graphic = Graphics.FromImage(newBmp);

            if (extension == ".png" || extension == ".gif" || extension == ".svg")
            {
                graphic.SmoothingMode = SmoothingMode.AntiAlias;
                graphic.InterpolationMode = InterpolationMode.HighQualityBicubic;

                //graphic.FillRectangle(Brushes.Transparent, 0, 0, newWidth, newHeight);
                graphic.DrawImage(oldBmp, left, top, newWidth, newHeight);

                //watermark
                AddWaterMark(graphic, newWidth, newHeight, parameter);

                newBmp.Save(target, System.Drawing.Image.FromFile(file).RawFormat);
            }
            else
            {
                graphic.SmoothingMode = SmoothingMode.AntiAlias;
                graphic.PixelOffsetMode = PixelOffsetMode.HighQuality;
                graphic.CompositingQuality = CompositingQuality.HighQuality;
                graphic.InterpolationMode = InterpolationMode.HighQualityBicubic;

                graphic.FillRectangle(Brushes.White, 0, 0, newWidth, newHeight);
                graphic.DrawImage(oldBmp, left, top, newWidth, newHeight);

                //watermark
                AddWaterMark(graphic, newWidth, newHeight, parameter);

                //quality image
                var quantity = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 50L);
                var jpegCodec = GetEncoder("image/jpeg");
                var encoder = new EncoderParameters(1) { Param = { [0] = quantity } };

                newBmp.Save(target, jpegCodec, encoder);
            }

            graphic.Dispose();
            oldBmp.Dispose();
            newBmp.Dispose();
        }

        public static void CroppedImageFile(string file, string target, string extension, int width, int height, params object[] parameter)
        {
            ResizeImageFile(5, file, target, extension, width, height, parameter);
        }

        #region private

        private static void AddWaterMark(Graphics graphic, int newWidth, int newHeight, params object[] parameter)
        {
            //watermark
            if (parameter == null) return;

            var watermark = System.Drawing.Image.FromFile(HttpContext.Current.Server.MapPath(HttpUtility.UrlDecode(parameter[0].ToString())));
            {
                var wtmWidth = newWidth - watermark.Width - parameter.Length > 1 ? CoreMr.Reddevil.Global.Convert.ToInt(parameter[1], 0) : 0;
                var wtmHeight = newHeight - watermark.Height - parameter.Length > 2 ? CoreMr.Reddevil.Global.Convert.ToInt(parameter[2], 0) : 0;

                graphic.DrawImage(watermark, wtmWidth, wtmHeight, watermark.Width, watermark.Height);
            }
        }

        private static ImageCodecInfo GetEncoder(string mime)
        {
            return Encoders[mime.ToLower()];
        }

        private static Dictionary<string, ImageCodecInfo> _oEncoders;

        private static Dictionary<string, ImageCodecInfo> Encoders
        {
            get
            {
                if (_oEncoders == null)
                    _oEncoders = new Dictionary<string, ImageCodecInfo>();

                if (_oEncoders.Count != 0) return _oEncoders;

                foreach (var encoder in ImageCodecInfo.GetImageEncoders())
                    _oEncoders.Add(encoder.MimeType.ToLower(), encoder);

                return _oEncoders;
            }
        }



        public static System.Drawing.Image FixedSize(System.Drawing.Image img, int maxWidth, int maxHeight)
        {
            if (img.Height < maxHeight && img.Width < maxWidth) return img;
            using (img)
            {
                Double xRatio = (double)img.Width / maxWidth;
                Double yRatio = (double)img.Height / maxHeight;
                Double ratio = Math.Max(xRatio, yRatio);
                int nnx = (int)Math.Floor(img.Width / ratio);
                int nny = (int)Math.Floor(img.Height / ratio);
                Bitmap cpy = new Bitmap(nnx, nny, PixelFormat.Format32bppArgb);
                using (Graphics gr = Graphics.FromImage(cpy))
                {
                    gr.Clear(Color.Transparent);

                    // This is said to give best quality when resizing images
                    gr.InterpolationMode = InterpolationMode.HighQualityBicubic;

                    gr.DrawImage(img,
                        new Rectangle(0, 0, nnx, nny),
                        new Rectangle(0, 0, img.Width, img.Height),
                        GraphicsUnit.Pixel);
                }
                return cpy;
            }

        }

        #endregion private
    }
}