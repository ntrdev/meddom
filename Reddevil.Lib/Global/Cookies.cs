﻿using System;
using System.Collections.Generic;
using System.Web;

namespace Reddevil.Lib.Global
{
    public static class Cookies
    {
        private static string SiteID => CoreMr.Reddevil.Web.Setting.Sys_SiteID + "_";

        public static bool Exist(string Key)
        {
            Key = SiteID + Key;

            return HttpContext.Current.Request.Cookies[Key] != null;
        }

        public static void SetValue(string key, string value, int minutes, bool secure)
        {
            key = SiteID + key;

            var IP = HttpContext.Current.Request.UserHostAddress;

            if (IP != "127.0.0.1" && CoreMr.Reddevil.Web.Setting.Mod_DomainCookies != string.Empty)
                HttpContext.Current.Response.Cookies[key].Domain = CoreMr.Reddevil.Web.Setting.Mod_DomainCookies;

            if (secure)
                HttpContext.Current.Response.Cookies[key].Value = CoreMr.Reddevil.Global.Security.Encrypt(IP + "_MrReddevil_" + key + value);
            else
                HttpContext.Current.Response.Cookies[key].Value = value;

            if (minutes > 0)
                HttpContext.Current.Response.Cookies[key].Expires = System.DateTime.Now.AddMinutes(minutes);
        }

        public static void SetValue(string key, string value, int minutes)
        {
            SetValue(key, value, minutes, true);
        }

        public static void SetValue(string key, string value, bool secure)
        {
            SetValue(key, value, 0, secure);
        }

        public static void SetValue(string key, string value)
        {
            SetValue(key, value, 0, false);
        }

        public static string GetValue(string key, bool secure)
        {
            if (!Exist(key))
                return string.Empty;

            key = SiteID + key;

            if (!secure) return HttpContext.Current.Request.Cookies[key].Value;

            var IP = HttpContext.Current.Request.UserHostAddress;

            var decrypt = CoreMr.Reddevil.Global.Security.Decrypt(HttpContext.Current.Request.Cookies[key].Value).Replace(IP + "_MrReddevil_" + key, string.Empty);

            if (decrypt.IndexOf("_MrReddevil_" + key, StringComparison.Ordinal) <= -1) return decrypt;

            Remove(key);

            return string.Empty;
        }

        public static string GetValue(string key)
        {
            return GetValue(key, false);
        }

        public static List<CartShop> GetAllCookie(string key)
        {
            List<CartShop> cookie = new List<CartShop>();
            string[] allCookie = HttpContext.Current.Request.Cookies.AllKeys;
            foreach (var item in allCookie)
            {
                if (item.StartsWith(SiteID + key))
                {
                    string[] arr = item.Split('_');
                    string IDshop = arr[4];
                    cookie.Add(new CartShop(IDshop));

                }
            }
            return cookie;
        }

        public static void Remove(string key)
        {
            if (!Exist(key)) return;

            key = SiteID + key;

            //ip
            var IP = HttpContext.Current.Request.UserHostAddress;

            //local
            if (IP != "127.0.0.1" && CoreMr.Reddevil.Web.Setting.Mod_DomainCookies != string.Empty)
                HttpContext.Current.Response.Cookies[key].Domain = CoreMr.Reddevil.Web.Setting.Mod_DomainCookies;

            //remove
            HttpContext.Current.Response.Cookies[key].Value = string.Empty;
            HttpContext.Current.Response.Cookies[key].Expires = DateTime.Now.AddDays(-1);
        }
    }
}