﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using CoreMr.Reddevil.Global;
using CoreMr.Reddevil.OptimalImage;
using CoreMr.Reddevil.Web;
using Reddevil.Lib.Global.ListItem;
using Reddevil.Lib.Models;
using Reddevil.Lib.MVC;
using HttpRequest = CoreMr.Reddevil.Web.HttpRequest;

namespace Reddevil.Lib.Global
{
    public class Utils : CoreMr.Reddevil.Web.Utils
    {
        //public static string GetTracking(int device)
        //{
        //    string tracking = string.Empty;

        //    string rawUrl = HttpRequest.RawUrl;
        //    var listItem = ModTrackingService.Instance.CreateQuery()
        //                                .Where(o => o.Activity == true)
        //                                .Where(o => (o.State & device) == device)
        //                                .ToList_Cache();

        //    for (int i = 0; listItem != null && i < listItem.Count; i++)
        //    {
        //        if (rawUrl.Contains(listItem[i].Url))
        //            tracking += listItem[i].Content;
        //    }

        //    return tracking;
        //}
        public static string GetResponseJson(string url)
        {
            Uri uri = new Uri(url);
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(uri);
            request.Method = WebRequestMethods.Http.Get;
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            string output = reader.ReadToEnd();
            response.Close();

            return output;
        }

        public static string ConvertTime(int totalseconds)
        {
            int time = totalseconds, h = 0, m = 0;
            while (time > 60)
            {
                if (time > 3600)
                {
                    h = time / 3600;
                    time = time % 3600;
                }
                else if (time > 60)
                {
                    m = time / 60;
                    time = time % 60;
                }
            }

            return h + ":" + m + ":" + time;
        }

        public static void GooglePing(string sitemap)
        {
            sitemap = "/ping?sitemap=" + sitemap;

            //GOOGLE
            try
            {
                var request = WebRequest.Create("http://www.google.com/webmasters/tools" + sitemap);
                request.GetResponse();
            }
            catch (Exception ex)
            {
                throw new Exception("Ping sitemap to google had error - " + ex.Message);
            }
        }


        public static string GetHtmlForSeo(string content)
        {
            if (string.IsNullOrEmpty(content))
                return string.Empty;

            content = HttpUtility.HtmlDecode(content);

            var listAutoLinks = ModAutoLinksService.Instance.CreateQuery()
                                    .Where(o => o.Activity == true)
                                    .OrderByDesc(o => o.Order)
                                    .ToList_Cache();

            for (var i = 0; listAutoLinks != null && i < listAutoLinks.Count; i++)
            {
                var value = listAutoLinks[i].Name.Trim();

                if (content.IndexOf(value, StringComparison.OrdinalIgnoreCase) < 0)
                    continue;

                var quantity = listAutoLinks[i].Quantity;
                var replaceValue = @"<a href=""" + listAutoLinks[i].Link + @""" class=""text-primary"" title=""" + listAutoLinks[i].Title + @""" target=""_blank"">" + value + @"</a>";

                var reg = new Regex(value, RegexOptions.IgnoreCase);

                content = quantity > 0 ? reg.Replace(content, replaceValue, quantity) : reg.Replace(content, replaceValue);

                break;
            }

            return content;
        }

        public static string DayOfWeekVn(DateTime dt)
        {
            var arrDayOfWeek = "Chủ nhật,Thứ hai,Thứ ba,Thứ tư,Thứ năm,Thứ sáu,Thứ bảy".Split(',');

            return arrDayOfWeek[(int)dt.DayOfWeek];
        }

        public static string GetNameOfConfig(string configKey, int value)
        {
            var list = List.GetListByConfigkey(configKey);

            var item = list.Find(o => o.Value == value.ToString());

            return item == null ? string.Empty : item.Name;
        }

        public static string ShowDdlPage(int selectId, string swhere)
        {
            var list = List.GetList(SysPageService.Instance);


            var s = string.Empty;

            for (var i = 0; list != null && i < list.Count; i++)
            {
                s += "<option " + (list[i].Value == selectId.ToString() ? "selected" : string.Empty) + " value=\"" + list[i].Value + "\">&nbsp; " + list[i].Name + "</option>";
            }
            return s;
        }

        #region charater

        public static string GetFirstLetterOfString(string s)
        {
            if (string.IsNullOrEmpty(s)) return string.Empty;

            while (s.Contains("  "))
            {
                s = s.Replace("  ", " ");
            }

            var result = string.Empty;

            for (var i = 1; i < s.Length; i++)
                if (s[i - 1] == ' ') result += s[i];

            return result;
        }

        public static string GetFirstChar(string title)
        {
            return !string.IsNullOrEmpty(title) ? title[0].ToString() : string.Empty;
        }

        public static string GetCharWithoutFirst(string title)
        {
            return !string.IsNullOrEmpty(title) ? title.Substring(1, title.Length - 1) : string.Empty;
        }

        #endregion charater

        #region validate

        public static bool IsEmailAddress(string email)
        {
            return Regex.IsMatch(email.Trim(), @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
        }

        public static bool IsLoginName(string s)
        {
            if (s.Length < 6 || s.Length > 12) return false;

            if (!char.IsLetter(s[0])) return false;

            return (!Regex.IsMatch(s, "[^a-z0-9_]", RegexOptions.IgnoreCase));
        }
        public static bool IsPhoneNumber(string number)
        {
            number = number.Replace("-", "").Replace(".", "");
            return Regex.Match(number, @"^(0)+([0-9]{9})$").Success;
        }
        public static bool CheckCarrier(string numberPhone)
        {
            string[] carriers_phone = { "070", "079", "077", "076", "078", "089", "090", "093", "083", "084", "085", "081", "082", "088", "091", "094", "032", "033", "034", "035", "036", "037", "038", "039", "086", "096", "097", "098", "056", "058", "092", "059", "099", "087", "095" };
            bool isPhone = false;
            numberPhone = numberPhone.Replace("-", "").Replace(".", "");
            foreach (var item in carriers_phone)
            {
                if (numberPhone.StartsWith(item))
                {
                    isPhone = true;
                    break;
                }
            }
            return isPhone;
        }


        public static string GetRandString()
        {
            var rand = new System.Random();
            var iRan = rand.Next();
            var sKey = Security.Md5(iRan.ToString());
            return sKey.Substring(0, 2) + iRan.ToString()[0] + sKey.Substring(3, 2);
        }

        #endregion validate

        #region data

        public static string ShowDdlMenuByParent(int parentId, int selectId)
        {
            var html = string.Empty;

            var keyCache = Cache.CreateKey("Web_Menu", "ShowDdlMenuByParent." + parentId + "." + selectId);
            var obj = Cache.GetValue(keyCache);
            if (obj != null) html = obj.ToString();
            else
            {
                if (parentId < 1)
                    return html;

                var list = WebMenuService.Instance.CreateQuery()
                                              .Where(o => o.ParentID == parentId)
                                              .OrderByAsc(o => o.Order)
                                              .ToList_Cache();

                for (var i = 0; list != null && i < list.Count; i++)
                {
                    html += "<option " + (list[i].ID == selectId ? "selected" : string.Empty) + " value=\"" + list[i].ID + "\">" + list[i].Name + "</option>";
                }

                Cache.SetValue(keyCache, html);
            }

            return html;
        }

        public static string ShowDdlMenuByType2(string type, int langId, int selectId)
        {
            var html = string.Empty;

            var keyCache = Cache.CreateKey("Web_Menu", "ShowDdlMenuByType2." + type + "." + langId + "." + selectId);
            var obj = Cache.GetValue(keyCache);
            if (obj != null) html = obj.ToString();
            else
            {
                var list = WebMenuService.Instance.CreateQuery()
                                            .Where(o => o.ParentID == 0 && o.LangID == langId && o.Type == type)
                                            .OrderByAsc(o => o.Order)
                                            .ToList_Cache();

                if (list == null) return html;

                var parentId = list[0].ID;

                list = WebMenuService.Instance.CreateQuery()
                                .Where(o => o.ParentID == parentId)
                                .OrderByAsc(o => o.Order)
                                .ToList_Cache();

                for (var i = 0; list != null && i < list.Count; i++)
                {
                    html += "<option " + (list[i].ID == selectId ? "selected" : string.Empty) + " value=\"" + list[i].ID + "\">" + list[i].Name + "</option>";
                }

                Cache.SetValue(keyCache, html);
            }

            return html;
        }
        //public static string ShowDdlMenuByTukhoa(string type, int langId, int selectId)
        //{
        //    var html = string.Empty;

        //    var keyCache = Cache.CreateKey("Web_Menu", "ShowDdlMenuByTukhoa." + type + "." + langId + "." + selectId);
        //    var obj = Cache.GetValue(keyCache);
        //    if (obj != null) html = obj.ToString();
        //    else
        //    {
        //        var list = ModTuKhoaDongService.Instance.CreateQuery()
        //                                    .Where(o => o.LoaiTuKhoa == type)
        //                                    .ToList_Cache();

        //        if (list == null) return html;


        //        for (var i = 0; list != null && i < list.Count; i++)
        //        {
        //            html += "<option " + (list[i].TuKhoaDongID == selectId ? "selected" : string.Empty) + " value=\"" + list[i].TuKhoaDongID + "\">" + list[i].TuKhoaDongName + "</option>";
        //        }

        //        Cache.SetValue(keyCache, html);
        //    }

        //    return html;
        //}

        public static string ShowDdlChuyenNganh(string type, int parent, int selectId)
        {
            var html = string.Empty;
            string keyCache = Cache.CreateKey("Web_ChuyenNganh", "ShowDdlChuyenNganhByType." + type + "." + parent);
            var obj = Cache.GetValue(keyCache);
            if (obj != null) html = obj.ToString();
            else
            {
                var list = ModChuyenNganhNhaKhoaHocService.Instance.CreateQuery()
                                            .Select(o => new { o.TuKhoaPhanCapID, o.Parrent, o.TuKhoaPhanCapName })
                                            .Where(o => o.Parrent == parent && o.Active == true && o.LoaiTuKhoa == type)
                                            .OrderByAsc(o => o.TuKhoaPhanCapName)
                                            .ToList_Cache();

                if (list == null) return html;


                for (var i = 1; list != null && i < list.Count; i++)
                {
                    html += $@" <li>
                                    <label>
                                        <input type=""checkbox"" class=""chuyen-nganh"" name=""provinces"" value=""{list[i].TuKhoaPhanCapID}"">
                                        <span>{list[i].TuKhoaPhanCapName}</span>
                                    </label>
                                </li>";
                }

                Cache.SetValue(keyCache, html);
            }

            return html;
        }


        public static string ShowDdlMenuByType(string type, int langId, int selectId)
        {
            var html = string.Empty;

            var keyCache = Cache.CreateKey("Web_Menu", "ShowDdlMenuByType." + type + "." + langId + "." + selectId);
            var obj = Cache.GetValue(keyCache);
            if (obj != null) html = obj.ToString();
            else
            {
                var list = List.GetList(WebMenuService.Instance, langId, type);
                for (var i = 0; list != null && i < list.Count; i++)
                {
                    html += "<option " + (list[i].Value == selectId.ToString() ? "selected" : string.Empty) + " value=\"" + list[i].Value + "\">&nbsp; " + list[i].Name + "</option>";
                }

                Cache.SetValue(keyCache, html);
            }

            return html;
        }

        public static string ShowDdlMenuByType4(string type, int langId, int selectId)
        {
            var html = string.Empty;

            var keyCache = Cache.CreateKey("Web_Menu", "ShowDdlMenuByType4." + type + "." + langId + "." + selectId);
            var obj = Cache.GetValue(keyCache);
            if (obj != null) html = obj.ToString();
            else
            {
                var list = List.GetList(WebMenuService.Instance, langId, type);
                for (var i = 1; list != null && i < list.Count; i++)
                {
                    html += "<option " + (list[i].Value == selectId.ToString() ? "selected" : string.Empty) + " value=\"" + list[i].Value + "\">&nbsp; " + list[i].Name + "</option>";
                }

                Cache.SetValue(keyCache, html);
            }

            return html;
        }

        public static string ShowDdlAllMenuNews(int langId, int selectId)
        {
            var html = string.Empty;
            var keyCache = Cache.CreateKey("Web_Menu", "AllChangeMenuNews." + langId + "." + selectId);
            var obj = Cache.GetValue(keyCache);
            if (obj != null) html = obj.ToString();
            else
            {
                var list = List.GetList2(WebMenuService.Instance, langId, "Type Like 'News%'");
                for (var i = 0; list != null && i < list.Count; i++)
                {
                    html += "<option " + (list[i].Value == selectId.ToString() ? "selected" : string.Empty) + " value=\"" + list[i].Value + "\">&nbsp; " + list[i].Name + "</option>";
                }

                Cache.SetValue(keyCache, html);
            }

            return html;
        }

        public static string ShowDdlByConfigkey(string configKey, int selectId)
        {
            var html = string.Empty;

            string tableName = string.Empty;
            if (configKey.Contains("."))
                tableName = "Mod_" + configKey.Split('.')[1].Replace("State", "");

            var keyCache = Cache.CreateKey(tableName, "ShowDdlByConfigkey." + configKey + "." + selectId);
            var obj = Cache.GetValue(keyCache);
            if (obj != null) html = obj.ToString();
            else
            {
                var list = List.GetListByConfigkey(configKey);
                for (var i = 0; list != null && i < list.Count; i++)
                {
                    html += "<option " + (list[i].Value == selectId.ToString() ? "selected" : string.Empty) + " value=\"" + list[i].Value + "\">" + list[i].Name + "</option>";
                }

                Cache.SetValue(keyCache, html);
            }

            return html;
        }

        public static string ShowRadioByConfigkey(string configKey, string name, int flag)
        {
            var html = string.Empty;

            string tableName = string.Empty;
            if (configKey.Contains("."))
                tableName = "Mod_" + configKey.Split('.')[1].Replace("State", "");

            var keyCache = Cache.CreateKey(tableName, "ShowCheckBoxByConfigkey." + configKey + "." + name + "." + flag);
            var obj = Cache.GetValue(keyCache);
            if (obj != null) html = obj.ToString();
            else
            {
                var list = List.GetListByConfigkey(configKey);
                for (var i = 0; list != null && i < list.Count; i++)
                {
                    html += "<input name=\"" + name + "\"" + ((flag & CoreMr.Reddevil.Global.Convert.ToInt(list[i].Value)) == CoreMr.Reddevil.Global.Convert.ToInt(list[i].Value) ? "checked=\"checked\"" : string.Empty) + " value=\"" + list[i].Value + "\" type=\"radio\" />" + list[i].Name + " &nbsp;";
                }

                Cache.SetValue(keyCache, html);
            }

            return html;
        }

        public static string ShowDdlMenuByTypeCity(string type, int langId, int selectId)
        {
            var html = string.Empty;
            var keyCache = Cache.CreateKey("Web_Menu", "ShowDdlMenuByTypeCity." + type + "." + langId + "." + selectId);
            var obj = Cache.GetValue(keyCache);
            if (obj != null) html = obj.ToString();
            else
            {
                var list = WebMenuService.Instance.CreateQuery()
                                            .Where(o => o.ParentID == 0 && o.LangID == langId && o.Type == type)
                                            .OrderByAsc(o => o.Order)
                                            .ToList_Cache();

                if (list == null) return html;

                var parentId = list[0].ID;

                list = WebMenuService.Instance.CreateQuery()
                                .Where(o => o.ParentID == parentId)
                                .OrderByAsc(o => o.Order)
                                .ToList_Cache();

                for (var i = 0; list != null && i < list.Count; i++)
                {
                    html += "<option " + (list[i].ID == selectId ? "selected" : string.Empty) + " value=\"" + list[i].ID + "\">" + list[i].Name + "</option>";

                }
            }

            return html;
        }

        public static string ShowCheckBoxByConfigkey(string configKey, string name, int flag)
        {
            var html = string.Empty;

            string tableName = string.Empty;
            if (configKey.Contains("."))
                tableName = "Mod_" + configKey.Split('.')[1].Replace("State", "");

            var keyCache = Cache.CreateKey(tableName, "ShowCheckBoxByConfigkey." + configKey + "." + name + "." + flag);
            var obj = Cache.GetValue(keyCache);
            if (obj != null) html = obj.ToString();
            else
            {
                var list = List.GetListByConfigkey(configKey);
                for (var i = 0; list != null && i < list.Count; i++)
                {
                    html += @"<div class=""md-checkbox border-checkbox-group border-checkbox-group-success""   data-toggle=""tooltip"" data-placement=""bottom"" data-original-title=""Click để chọn hoặc bỏ chọn"" >
                                  <input type=""checkbox"" id=""" + name + i + @""" name=""" + name + @""" value=""" + list[i].Value + @""" " + ((flag & CoreMr.Reddevil.Global.Convert.ToInt(list[i].Value)) == CoreMr.Reddevil.Global.Convert.ToInt(list[i].Value) ? "checked=\"checked\"" : string.Empty) + @" onclick=""isChecked(this.checked)"" class=""md-check border-checkbox check-select"" />
                                        <label class=""border-checkbox-label"" for=""" + name + i + @"""> " + list[i].Name + @" </label>
                           </div>";
                }

                Cache.SetValue(keyCache, html);
            }

            return html;
        }

        public static string ShowNameByConfigkey(string configKey, int flag)
        {
            var html = string.Empty;

            string tableName = string.Empty;
            if (configKey.Contains("."))
                tableName = "Mod_" + configKey.Split('.')[1].Replace("State", "");

            var keyCache = Cache.CreateKey(tableName, "ShowNameByConfigkey." + configKey + "." + flag);
            var obj = Cache.GetValue(keyCache);
            if (obj != null) html = obj.ToString();
            else
            {
                var list = List.GetListByConfigkey(configKey);
                for (var i = 0; list != null && i < list.Count; i++)
                {
                    if ((flag & CoreMr.Reddevil.Global.Convert.ToInt(list[i].Value)) != CoreMr.Reddevil.Global.Convert.ToInt(list[i].Value)) continue;

                    html += list[i].Name + "<br />";
                }

                Cache.SetValue(keyCache, html);
            }

            return html;
        }

        public static string GetNameByConfigkey(string configKey, int value)
        {
            var list = List.GetListByConfigkey(configKey);

            var item = list.Find(o => o.Value == value.ToString());

            return item == null ? string.Empty : item.Name;
        }

        public static List<WebMenuEntity> GetListMenuByType(string type, int langId)
        {
            var keyCache = Cache.CreateKey("Web_Menu", "GetListMenuByType." + type + "." + langId);
            var list = Cache.GetValue(keyCache) as List<WebMenuEntity>;
            if (list != null) return list;
            else
            {
                list = WebMenuService.Instance.CreateQuery()
                                            .Where(o => o.ParentID == 0 && o.LangID == langId && o.Type == type)
                                            .OrderByAsc(o => o.Order)
                                            .ToList_Cache();

                if (list == null) return null;

                var parentId = list[0].ID;

                list = WebMenuService.Instance.CreateQuery()
                                .Where(o => o.ParentID == parentId)
                                .OrderByAsc(o => o.Order)
                                .ToList_Cache();

                Cache.SetValue(keyCache, list);
            }

            return list;
        }

        public static string GetListMenuByType(string type, int langId, int selectId)
        {
            var html = string.Empty;
            var keyCache = Cache.CreateKey("Web_Menu", "GetListMenuByType." + type + "." + langId + "." + selectId);
            var obj = Cache.GetValue(keyCache);
            if (obj != null) html = obj.ToString();
            else
            {
                var list = WebMenuService.Instance.CreateQuery()
                                              .Where(o => o.ParentID == 0 && o.LangID == langId && o.Type == type)
                                              .OrderByAsc(o => o.Order)
                                              .ToList_Cache();

                if (list == null) return null;

                var parentId = list[0].ID;

                list = WebMenuService.Instance.CreateQuery()
                                .Where(o => o.ParentID == parentId)
                                .OrderByAsc(o => o.Order)
                                .ToList_Cache();


                for (var i = 0; list != null && i < list.Count; i++)
                {
                    html += "<option " + (list[i].ID == selectId ? "selected" : string.Empty) + " value=\"" + list[i].ID + "\">" + list[i].Name + "</option>";

                }
                Cache.SetValue(keyCache, html);
            }

            return html;
        }

        #endregion data

        #region breadcrumb

        public static string GetMapPage(SysPageEntity page)
        {
            var viewPage = CoreMr.Reddevil.Web.Application.CurrentViewPage as ViewPage;

            var html = @"  <li class=""breadcrumb-item"" itemprop=""itemListElement"" itemscope itemtype=""http://schema.org/ListItem"">
                                <a href=""/"" itemprop=""item"" ><span itemprop=""name"">Trang chủ</span></a>
                                 <meta itemprop=""position"" content=""1"" />
                            </li>";

            return html + GetMapPage(viewPage, page);
        }

        private static string GetMapPage(ViewPage viewPage, SysPageEntity page)
        {
            if (page == null || page.Root) return string.Empty;
            if (viewPage.IsPageActived(page))
            {
                var level = CoreMr.Reddevil.Global.Array.ToInts(page.Levels, '-');
                var ine = System.Array.IndexOf(level, page.ID);
                var html = @"   <li class=""breadcrumb-item"" itemprop=""itemListElement"" itemscope itemtype=""http://schema.org/ListItem"">
                                <a itemprop=""item"" href=""" + (viewPage.ViewBag.SecondHand == 1 ? viewPage.GetURL(page.Url + "-secondhand") : viewPage.GetPageURL(page)) + @"""><span itemprop=""name""><i class=""icon-angle-right""></i>" + page.Name + @"</span></a>
                                <meta itemprop=""position"" content=""" + (ine + 1) + @""" />
                            </li>";

                var parent = SysPageService.Instance.GetByID_Cache(page.ParentID);

                if (parent == null || parent.Root || parent.Url == "-") return html;

                return GetMapPage(viewPage, parent) + html;
            }
            else
            {
                var level = CoreMr.Reddevil.Global.Array.ToInts(page.Levels, '-');
                var ine = System.Array.IndexOf(level, page.ID);
                var html = @"   <li class=""breadcrumb-item"" itemprop=""itemListElement"" itemscope itemtype=""http://schema.org/ListItem"">
                                <a itemprop=""item"" href=""" + (viewPage.ViewBag.SecondHand == 1 ? viewPage.GetURL(page.Url + "-secondhand") : viewPage.GetPageURL(page)) + @"""><span itemprop=""name""><i class=""icon-angle-right""></i>" + page.Name + @"</span></a>
                                <meta itemprop=""position"" content=""" + (ine + 1) + @""" />
                            </li>";

                var parent = SysPageService.Instance.GetByID_Cache(page.ParentID);

                if (parent == null || parent.Root || parent.Url == "-") return html;

                return GetMapPage(viewPage, parent) + html;
            }
        }
        #endregion breadcrumb

        #region visitor

        //public static void UpdateStatistic()
        //{
        //    if (HttpRequest.RawUrl == "https://bepvuson.vn/Default.aspx")
        //        return;

        //    string link = HttpRequest.RawUrl;
        //    if (link.IndexOf("?") > -1)
        //        link = link.Split('?')[0];

        //    var statistic = ModStatisticService.Instance.GetByLink(link);
        //    if (statistic == null)
        //    {
        //        statistic = new ModStatisticEntity
        //        {
        //            ID = 0,
        //            Link = link
        //        };

        //        ModStatisticService.Instance.Save(statistic);
        //    }

        //    //detail
        //    string city = HttpRequest.City;
        //    var statisticDetail = ModStatisticDetailService.Instance.GetByCity(statistic.ID, city);
        //    if (statisticDetail == null || string.IsNullOrEmpty(city))
        //    {
        //        ModStatisticDetailService.Instance.Save(new ModStatisticDetailEntity()
        //        {
        //            ID = 0,
        //            StatisticID = statistic.ID,
        //            City = HttpRequest.City,
        //            Country = HttpRequest.Country,
        //            CountryCode = HttpRequest.CountryCode,
        //            View = 1
        //        });
        //    }
        //    else
        //    {
        //        statisticDetail.View++;
        //        ModStatisticDetailService.Instance.Save(statisticDetail, o => o.View);
        //    }

        //    statistic.View++;
        //    ModStatisticService.Instance.Save(statistic, o => o.View);
        //}

        public static long Online
        {
            get
            {
                var listOnline = GetOnline();

                if (listOnline == null) return 1;

                return listOnline.Count;
            }
        }

        public static long Visit
        {
            get
            {
                return GetVisit(Range.Total);
            }
        }

        public class Range
        {
            public static string Total = "Total";
            public static string Today = "Today";
            public static string Yesterday = "Yesterday";
            public static string ThisWeek = "ThisWeek";
            public static string LastWeek = "LastWeek";
            public static string ThisMonth = "ThisMonth";
            public static string LastMonth = "LastMonth";
            public static string ThisYear = "ThisYear";
            public static string LastYear = "LastYear";
        }

        public static List<ModOnlineEntity> GetOnline()
        {
            return ModOnlineService.Instance.CreateQuery()
                                    .Where(o => o.TimeValue > DateTime.Now.AddMinutes(-5).Ticks)
                                    .ToList_Cache();
        }

        public static List<ModOnlineEntity> GetOnline(bool isLogin)
        {
            var listOnline = GetOnline();

            if (listOnline == null) return null;

            return listOnline.FindAll(o => o.UserID > 0);
        }

        public static long GetVisit(string code)
        {
            return WebSettingService.Instance.CreateQuery()
                            .Select(o => o.Value)
                            .Where(o => o.Code == code)
                            .ToValue_Cache()
                            .ToLong();
        }

        private static WebSettingEntity _oInitRecord;

        private static WebSettingEntity InitRecord(string code)
        {
            if (_oInitRecord == null)
            {
                var record = WebSettingService.Instance.CreateQuery()
                                        .Where(o => o.Code == code)
                                        .ToSingle_Cache();

                if (record == null)
                {
                    record = new WebSettingEntity
                    {
                        ID = 0,
                        Name = code,
                        Code = code,
                        Value = 0
                    };

                    WebSettingService.Instance.Save(record);
                }

                _oInitRecord = record;
            }

            return _oInitRecord;
        }

        private static void Update()
        {
            var dt = DateTime.Now;

            var today = InitRecord(Range.Today);
            var thisWeek = InitRecord(Range.ThisWeek);
            var thisMonth = InitRecord(Range.ThisMonth);
            var thisYear = InitRecord(Range.ThisYear);

            if (dt != dt.Date)
            {
                today.Value++;
                WebSettingService.Instance.Save(today, o => o.Value);

                thisWeek.Value++;
                WebSettingService.Instance.Save(thisWeek, o => o.Value);

                thisMonth.Value++;
                WebSettingService.Instance.Save(thisMonth, o => o.Value);

                thisYear.Value++;
                WebSettingService.Instance.Save(thisYear, o => o.Value);
            }
            else
            {
                var yesterday = InitRecord(Range.Yesterday);

                yesterday.Value = today.Value + 1;
                WebSettingService.Instance.Save(yesterday, o => o.Value);

                today.Value = 1;
                WebSettingService.Instance.Save(today, o => o.Value);

                //ngay cuoi tuan
                if (dt.DayOfWeek == DayOfWeek.Sunday)
                {
                    var lastWeek = InitRecord(Range.LastWeek);

                    lastWeek.Value = thisWeek.Value + 1;
                    WebSettingService.Instance.Save(lastWeek, o => o.Value);

                    thisWeek.Value = 1;
                    WebSettingService.Instance.Save(thisWeek, o => o.Value);
                }

                //ngay cuoi thang
                if (dt.Day == DateTime.DaysInMonth(dt.Year, dt.Month))
                {
                    var lastMonth = InitRecord(Range.LastMonth);

                    lastMonth.Value = thisMonth.Value + 1;
                    WebSettingService.Instance.Save(lastMonth, o => o.Value);

                    thisMonth.Value = 1;
                    WebSettingService.Instance.Save(thisMonth, o => o.Value);

                    //ngay cuoi nam
                    if (dt.Month == 12)
                    {
                        var lastYear = InitRecord(Range.LastYear);

                        lastYear.Value = thisYear.Value + 1;
                        WebSettingService.Instance.Save(lastYear, o => o.Value);

                        thisYear.Value = 1;
                        WebSettingService.Instance.Save(thisYear, o => o.Value);
                    }
                }
            }
        }

        public static void UpdateOnline()
        {
            if (!Config.GetValue("Mod.Visit").ToBool() || Cookies.GetValue("Mod.Online") != null) return;

            Update();

            Cookies.SetValue("Mod.Online", "1", 5, true);

            if (!Config.GetValue("Mod.Online").ToBool()) return;

            ModOnlineService.Instance.Delete(o => o.TimeValue < DateTime.Now.AddMinutes(-5).Ticks);
            ModOnlineService.Instance.Save(new ModOnlineEntity
            {
                SessionID = HttpContext.Current.Session.SessionID,
                TimeValue = DateTime.Now.Ticks,
                IP = HttpRequest.IP
                //UserID = WebLogin.IsLogin() ? WebLogin.UserID : 0
            });
        }
        //real-time
        public static void UpdateOnlineHUB(string connectionID, int userID)
        {

            if (!Config.GetValue("Mod.Visit").ToBool() || !string.IsNullOrEmpty(Cookies.GetValue("Mod.Online")))
            {
                var _checkCurrent = ModOnlineService.Instance.GetByUserID_IsLogin(userID);
                if (_checkCurrent != null)
                {
                    _checkCurrent.ConnectionID = connectionID;
                    _checkCurrent.TimeValue = DateTime.Now.Ticks;
                    ModOnlineService.Instance.Save(_checkCurrent, o => new { o.ConnectionID, o.TimeValue });
                    Cookies.SetValue("Mod.Online", "1", 10, true);
                    return;
                };

            }

            Update();

            Cookies.SetValue("Mod.Online", "1", 10, true);

            if (!Config.GetValue("Mod.Online").ToBool()) return;

            ModOnlineService.Instance.Delete(o => o.TimeValue < DateTime.Now.AddMinutes(-10).Ticks);

            ModOnlineService.Instance.Save(new ModOnlineEntity
            {
                TimeValue = DateTime.Now.Ticks,
                IP = CoreMr.Reddevil.Web.HttpRequest.IP,
                IsLogin = true,
                UserID = userID,
                ConnectionID = connectionID
            });
        }


        #endregion visitor

        #region number to word

        private static string Read(string number)
        {
            var result = "";
            switch (number)
            {
                case "0":
                    result = "không";
                    break;

                case "1":
                    result = "một";
                    break;

                case "2":
                    result = "hai";
                    break;

                case "3":
                    result = "ba";
                    break;

                case "4":
                    result = "bốn";
                    break;

                case "5":
                    result = "năm";
                    break;

                case "6":
                    result = "sáu";
                    break;

                case "7":
                    result = "bảy";
                    break;

                case "8":
                    result = "tám";
                    break;

                case "9":
                    result = "chín";
                    break;
            }
            return result;
        }

        private static string Unit(string number)
        {
            var result = "";

            if (number.Equals("1"))
                result = "";
            if (number.Equals("2"))
                result = "nghìn";
            if (number.Equals("3"))
                result = "triệu";
            if (number.Equals("4"))
                result = "tỷ";
            if (number.Equals("5"))
                result = "nghìn tỷ";
            if (number.Equals("6"))
                result = "triệu tỷ";
            if (number.Equals("7"))
                result = "tỷ tỷ";

            return result;
        }

        private static string Split(string possition)
        {
            var result = "";

            if (possition.Equals("000")) return result;

            if (possition.Length != 3) return result;

            var first = possition.Trim().Substring(0, 1).Trim();
            var middle = possition.Trim().Substring(1, 1).Trim();
            var last = possition.Trim().Substring(2, 1).Trim();

            if (first.Equals("0") && middle.Equals("0"))
                result = " không trăm lẻ " + Read(last.Trim()) + " ";

            if (!first.Equals("0") && middle.Equals("0") && last.Equals("0"))
                result = Read(first.Trim()).Trim() + " trăm ";

            if (!first.Equals("0") && middle.Equals("0") && !last.Equals("0"))
                result = Read(first.Trim()).Trim() + " trăm lẻ " + Read(last.Trim()).Trim() + " ";

            if (first.Equals("0") && CoreMr.Reddevil.Global.Convert.ToInt(middle) > 1 && CoreMr.Reddevil.Global.Convert.ToInt(last) > 0 && !last.Equals("5"))
                result = " không trăm " + Read(middle.Trim()).Trim() + " mươi " + Read(last.Trim()).Trim() + " ";

            if (first.Equals("0") && CoreMr.Reddevil.Global.Convert.ToInt(middle) > 1 && last.Equals("0"))
                result = " không trăm " + Read(middle.Trim()).Trim() + " mươi ";

            if (first.Equals("0") && CoreMr.Reddevil.Global.Convert.ToInt(middle) > 1 && last.Equals("5"))
                result = " không trăm " + Read(middle.Trim()).Trim() + " mươi lăm ";

            if (first.Equals("0") && middle.Equals("1") && CoreMr.Reddevil.Global.Convert.ToInt(last) > 0 && !last.Equals("5"))
                result = " không trăm mười " + Read(last.Trim()).Trim() + " ";

            if (first.Equals("0") && middle.Equals("1") && last.Equals("0"))
                result = " không trăm mười ";

            if (first.Equals("0") && middle.Equals("1") && last.Equals("5"))
                result = " không trăm mười lăm ";

            if (CoreMr.Reddevil.Global.Convert.ToInt(first) > 0 && CoreMr.Reddevil.Global.Convert.ToInt(middle) > 1 && CoreMr.Reddevil.Global.Convert.ToInt(last) > 0 && !last.Equals("5"))
                result = Read(first.Trim()).Trim() + " trăm " + Read(middle.Trim()).Trim() + " mươi " + Read(last.Trim()).Trim() + " ";

            if (CoreMr.Reddevil.Global.Convert.ToInt(first) > 0 && CoreMr.Reddevil.Global.Convert.ToInt(middle) > 1 && last.Equals("0"))
                result = Read(first.Trim()).Trim() + " trăm " + Read(middle.Trim()).Trim() + " mươi ";

            if (CoreMr.Reddevil.Global.Convert.ToInt(first) > 0 && CoreMr.Reddevil.Global.Convert.ToInt(middle) > 1 && last.Equals("5"))
                result = Read(first.Trim()).Trim() + " trăm " + Read(middle.Trim()).Trim() + " mươi lăm ";

            if (CoreMr.Reddevil.Global.Convert.ToInt(first) > 0 && middle.Equals("1") && CoreMr.Reddevil.Global.Convert.ToInt(last) > 0 && !last.Equals("5"))
                result = Read(first.Trim()).Trim() + " trăm mười " + Read(last.Trim()).Trim() + " ";

            if (CoreMr.Reddevil.Global.Convert.ToInt(first) > 0 && middle.Equals("1") && last.Equals("0"))
                result = Read(first.Trim()).Trim() + " trăm mười ";

            if (CoreMr.Reddevil.Global.Convert.ToInt(first) > 0 && middle.Equals("1") && last.Equals("5"))
                result = Read(first.Trim()).Trim() + " trăm mười lăm ";

            return result;
        }

        public static string NumberToWord(string number)
        {
            if (string.IsNullOrEmpty(number)) return "Không đồng.";

            string result = string.Empty, firstPart = string.Empty, lastPart = string.Empty;

            var quotient = CoreMr.Reddevil.Global.Convert.ToInt(number.Length / 3);
            var remainder = number.Length % 3;

            switch (remainder)
            {
                case 0:
                    firstPart = "000";
                    break;

                case 1:
                    firstPart = "00" + number.Trim().Substring(0, 1);
                    break;

                case 2:
                    firstPart = "0" + number.Trim().Substring(0, 2);
                    break;
            }

            if (number.Length > 2)
                lastPart = CoreMr.Reddevil.Global.Convert.ToString(number.Trim().Substring(remainder, number.Length - remainder)).Trim();

            var im = quotient + 1;
            if (remainder > 0)
                result = Split(firstPart).Trim() + " " + Unit(im.ToString().Trim());

            var i = quotient;
            var j = quotient;
            var k = 1;

            while (i > 0)
            {
                var possition = lastPart.Trim().Substring(0, 3).Trim();
                result = result.Trim() + " " + Split(possition.Trim()).Trim();
                quotient = j + 1 - k;

                if (!possition.Equals("000"))
                    result = result.Trim() + " " + Unit(quotient.ToString().Trim()).Trim();

                lastPart = lastPart.Trim().Substring(3, lastPart.Trim().Length - 3);

                i--;
                k++;
            }

            if (result.Trim().Length <= 0) return result.Trim();

            if (result.Trim().Substring(0, 1).Equals("k"))
                result = result.Trim().Substring(10, result.Trim().Length - 10).Trim();

            if (result.Trim().Substring(0, 1).Equals("l"))
                result = result.Trim().Substring(2, result.Trim().Length - 2).Trim();

            result = result.Trim().Substring(0, 1).Trim().ToUpper() +
                     result.Trim().Substring(1, result.Trim().Length - 1).Trim() + " đồng.";

            return result.Trim();
        }

        public static string NumberToWordV2(string number)
        {
            string[] dv = { "", "mươi", "trăm", "nghìn", "triệu", "tỉ" };
            string[] cs = { "không", "một", "hai", "ba", "bốn", "năm", "sáu", "bảy", "tám", "chín" };
            string doc;
            int i, j, k, n, len, found, ddv, rd;

            len = number.Length;
            number += "ss";
            doc = "";
            found = 0;
            ddv = 0;
            rd = 0;

            i = 0;
            while (i < len)
            {
                //So chu so o hang dang duyet
                n = (len - i + 2) % 3 + 1;

                //Kiem tra so 0
                found = 0;
                for (j = 0; j < n; j++)
                {
                    if (number[i + j] != '0')
                    {
                        found = 1;
                        break;
                    }
                }

                //Duyet n chu so
                if (found == 1)
                {
                    rd = 1;
                    for (j = 0; j < n; j++)
                    {
                        ddv = 1;
                        switch (number[i + j])
                        {
                            case '0':
                                if (n - j == 3) doc += cs[0] + " ";
                                if (n - j == 2)
                                {
                                    if (number[i + j + 1] != '0') doc += "lẻ ";
                                    ddv = 0;
                                }
                                break;

                            case '1':
                                if (n - j == 3) doc += cs[1] + " ";
                                if (n - j == 2)
                                {
                                    doc += "mười ";
                                    ddv = 0;
                                }
                                if (n - j == 1)
                                {
                                    if (i + j == 0) k = 0;
                                    else k = i + j - 1;

                                    if (number[k] != '1' && number[k] != '0')
                                        doc += "mốt ";
                                    else
                                        doc += cs[1] + " ";
                                }
                                break;

                            case '5':
                                if (i + j == len - 1)
                                    doc += "lăm ";
                                else
                                    doc += cs[5] + " ";
                                break;

                            default:
                                doc += cs[(int)number[i + j] - 48] + " ";
                                break;
                        }

                        //Doc don vi nho
                        if (ddv == 1)
                        {
                            doc += dv[n - j - 1] + " ";
                        }
                    }
                }

                //Doc don vi lon
                if (len - i - n > 0)
                {
                    if ((len - i - n) % 9 == 0)
                    {
                        if (rd == 1)
                            for (k = 0; k < (len - i - n) / 9; k++)
                                doc += "tỉ ";
                        rd = 0;
                    }
                    else
                        if (found != 0) doc += dv[((len - i - n + 1) % 9) / 3 + 2] + " ";
                }

                i += n;
            }

            if (len == 1)
                if (number[0] == '0' || number[0] == '5') return cs[(int)number[0] - 48];

            return doc;
        }

        public static string NumberToWordV3(string input)
        {
            var result = "";

            var number = CoreMr.Reddevil.Global.Convert.ToLong(input);
            if (number < 1) return "0 đồng";

            var billion = number / (long)Math.Pow(10, 9);
            var million = (number - billion * (long)Math.Pow(10, 9)) / (long)Math.Pow(10, 6);
            var thousand = (number - billion * (long)Math.Pow(10, 9) - million * (long)Math.Pow(10, 6)) / (long)Math.Pow(10, 3);

            if (billion > 0) result += billion + " tỷ ";
            if (million > 0) result += million + " triệu ";
            if (thousand > 0) result += thousand + " nghìn ";

            return result.Trim();
        }

        #endregion number to word

        #region media


        public static string DefaultImage(string parth)
        {
            if (string.IsNullOrEmpty(parth)) return string.Empty;
            else return parth.Replace("~/", "/");
        }


        public static string OptimalImage(string file)
        {
            if (string.IsNullOrEmpty(file))
            {
                if (Device.Mobile || Device.Tablet) return "";
                else return "";
            }
            else
            {

                if (file.StartsWith("http", StringComparison.OrdinalIgnoreCase)) return file;

                var keyCache = string.Concat("[Mod_Adv]", ".GetImgOptimal." + file);
                var obj = Cache.GetValue(keyCache);
                if (obj != null) return obj.ToString();
                else
                {
                    file = HttpUtility.UrlDecode(file);
                    var filePath = HttpContext.Current.Server.MapPath(file);
                    if (!System.IO.File.Exists(filePath))
                    {
                        if (Device.Mobile || Device.Tablet) return "";
                        else return "";

                    }
                    var target = Path.GetDirectoryName(file.ToLower().Replace("~/data/upload/images", "").Replace("/data/upload/images", ""));
                    var extension = Path.GetExtension(file);
                    Directory.Create(HttpContext.Current.Server.MapPath("~/Data/Imgm/" + target));
                    target = "~/Data/Imgm/" + target + "/" + File.FormatFileName(Path.GetFileNameWithoutExtension(file)) + "-m60" + extension;
                    var targetPạth = HttpContext.Current.Server.MapPath(target);
                    if (System.IO.File.Exists(targetPạth))
                    {
                        Cache.SetValue(keyCache, HttpRequest.ApplicationPath + target.Replace("~/", "/").Replace("\\", "/").Replace("//", "/").Replace("//", "/").Replace("~/", "/"));
                        return HttpRequest.ApplicationPath + target.Replace("~/", "/").Replace("\\", "/").Replace("//", "/").Replace("//", "/").Replace("~/", "/");
                    }
                    if (extension == ".webp")
                    {
                        GetResizeFile(file, 4, 630, 315);
                    }
                    else if (extension == ".png")
                    {


                        try
                        {
                            new Task(() =>
                            {
                                try
                                {
                                    var quantizer = new OptimalQuantizer();
                                    using (var bitmap = new Bitmap(filePath))
                                    {
                                        using (var quantized = quantizer.OptimalPng(bitmap))
                                        {
                                            quantized.Save(targetPạth, ImageFormat.Png);
                                        }
                                    }
                                }
                                catch
                                {
                                    Image.CompressionImageFile(60, filePath, targetPạth);
                                }

                            }).Start();
                        }
                        catch
                        {
                            Image.CompressionImageFile(60, filePath, targetPạth);
                        }
                    }
                    else
                    {
                        Image.CompressionImageFile(60, filePath, targetPạth);
                    }
                    Cache.SetValue(keyCache, HttpRequest.ApplicationPath + target.Replace("~/", "/").Replace("\\", "/").Replace("//", "/").Replace("//", "/").Replace("~/", "/"));
                    return HttpRequest.ApplicationPath + target.Replace("~/", "/").Replace("\\", "/").Replace("//", "/").Replace("//", "/").Replace("~/", "/");

                }

            }
        }


        public static string GetCodeAdv(ModAdvEntity adv)
        {
            string html;
            var keyCache = string.Concat("[Mod_Adv]", ".GetCodeAdv." + adv.ID);
            var obj = Cache.GetValue(keyCache);

            if (obj != null) html = obj.ToString();
            else
            {
                html = !string.IsNullOrEmpty(adv.AdvCode) ? adv.AdvCode : string.Empty;

                if (!string.IsNullOrEmpty(adv.File))
                {
                    if (!string.IsNullOrEmpty(adv.URL) && adv.File.EndsWith(".swf", StringComparison.OrdinalIgnoreCase))
                        return GetMedia(adv.File, adv.Width, adv.Height, string.Empty, adv.Name, string.Empty, false);

                    if (string.IsNullOrEmpty(adv.URL)) adv.URL = "/";

                    html = "<a href=\"" + adv.URL + "\" target=\"" + adv.Target + "\" rel=\"nofollow\">" + GetMedia(adv.File, adv.Width, adv.Height, string.Empty, adv.Name, string.Empty, true) + "</a>";
                }

                Cache.SetValue(keyCache, html);
            }

            return html;
        }

        public static string GetResizeFile(string file, int type, int width, int height)
        {
            if (string.IsNullOrEmpty(file))
                return "";

            if (file.StartsWith("http", StringComparison.OrdinalIgnoreCase))
                return file;

            var applicationPath = HttpRequest.ApplicationPath;

            file = HttpUtility.UrlDecode(file);
            var filePath = HttpContext.Current.Server.MapPath(file);

            var target = Path.GetDirectoryName(file.ToLower().Replace("~/data/upload/", "").Replace("/data/upload/", ""));
            Directory.Create(HttpContext.Current.Server.MapPath("~/Data/ResizeImage/" + target));

            var extension = Path.GetExtension(file);
            target = "~/Data/ResizeImage/" + target + "/" + File.FormatFileName(Path.GetFileNameWithoutExtension(file)) + "x" + width + "x" + height + "x" + type + extension;
            var targetPạth = HttpContext.Current.Server.MapPath(target);

            if (System.IO.File.Exists(targetPạth))
                return applicationPath + target.Replace("~/", "/").Replace("\\", "/").Replace("//", "/").Replace("//", "/");

            if (!System.IO.File.Exists(filePath))
                return "";

            try
            {
                Image.ResizeImageFile(type, filePath, targetPạth, extension, width, height, null);
            }
            catch (Exception ex)
            {
                Error.Write(ex);

                // ignored
            }

            if (System.IO.File.Exists(targetPạth))
                return applicationPath + target.Replace("~/", "/").Replace("\\", "/").Replace("//", "/").Replace("//", "/");

            return applicationPath + target.Replace("~/", "/").Replace("\\", "/").Replace("//", "/").Replace("//", "/");
        }

        public static string GetResizeFile(string file, int type, int width, int height, string cssClass, string alt)
        {
            var html = string.Empty;
            var keyCache = string.Concat("[Mod_Adv]", ".GetResizeFile." + file + "." + type + "." + width + "." + height + "." + cssClass + "." + alt);
            var obj = Cache.GetValue(keyCache);
            if (obj != null) html = obj.ToString();
            else
            {
                if (!string.IsNullOrEmpty(cssClass))
                    html += " class=\"" + cssClass + "\" ";

                if (type != 4 && type != 5)
                {
                    if (width > 0)
                        html += " width=\"" + width + "\" ";

                    if (height > 0)
                        html += " height=\"" + height + "\" ";
                }

                if (!string.IsNullOrEmpty(alt))
                    html += " alt=\"" + alt + "\" ";

                html = @"<img src=""" + GetResizeFile(file, type, width, height) + @""" " + html + @" />";

                Cache.SetValue(keyCache, html);
            }

            return html;
        }

        public static string GetResizeFile(string file, int type, int width, int height, string alt)
        {
            return GetResizeFile(file, type, width, height, string.Empty, alt);
        }

        public static string GetCropFile(string file, int width, int height)
        {
            return GetResizeFile(file, 5, width, height);
        }

        public static string GetMedia(int typeResize, string file, int width, int height, string cssClass, string alt, string addInTag, bool compression)
        {
            var html = string.Empty;

            var keyCache = string.Concat("[Mod_Adv]", ".GetMedia." + typeResize + "." + file + "." + width + "." + height + "." + cssClass + "." + alt + "." + addInTag + "." + compression);
            var obj = Cache.GetValue(keyCache);
            if (obj != null) html = obj.ToString();
            else
            {
                if (string.IsNullOrEmpty(file))
                    return string.Empty;

                var extension = Path.GetExtension(file).ToLower();
                if (extension == ".swf")
                {
                    file = file.Replace("~/", "/");

                    if (!file.StartsWith("http", StringComparison.OrdinalIgnoreCase))
                        file = HttpRequest.ApplicationPath + HttpContext.Current.Server.UrlPathEncode(file);

                    html = @"   <object width=""" + (width > 0 ? width.ToString() : "100%") + @""" height=""" + (height > 0 ? height.ToString() : "100%") + @" border=""0"" codebase=""http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0"" classid=""clsid:d27cdb6e-ae6d-11cf-96b8-444553540000"">
                                    <param value=""" + file + @""" name=""movie"">
                                    <param value=""always"" name=""AllowScriptAccess"">
                                    <param value=""high"" name=""quality"">
                                    <param name=""scale"" value=""exactfit"">
                                    <param value=""transparent"" name=""wmode"">
                                    <embed width=""" + (width > 0 ? width.ToString() : "100%") + @""" height=""" + (height > 0 ? height.ToString() : "100%") + @" type=""application/x-shockwave-flash"" scale=""exactfit"" pluginspage=""http://www.macromedia.com/go/getflashplayer"" allowscriptaccess=""always"" wmode=""transparent"" quality=""high"" src=""" + file + @""">
                                </object>";
                }
                else
                {
                    if (!string.IsNullOrEmpty(cssClass))
                        html += " class=\"" + cssClass + "\" ";

                    if (!string.IsNullOrEmpty(alt))
                        html += " alt=\"" + alt + "\" ";

                    if (!compression)
                    {
                        if (!file.StartsWith("http", StringComparison.OrdinalIgnoreCase))
                            file = HttpRequest.ApplicationPath + HttpContext.Current.Server.UrlPathEncode(file);

                        if (width > 0)
                            html += " width=\"" + width + "\" ";

                        if (height > 0)
                            html += " height=\"" + height + "\" ";
                    }
                    else
                    {
                        file = GetResizeFile(file, typeResize, width, height);

                        if (typeResize != 4)
                        {
                            if (width > 0)
                                html += " width=\"" + width + "\" ";

                            if (height > 0)
                                html += " height=\"" + height + "\" ";
                        }
                    }

                    if (!file.StartsWith("/") && !file.StartsWith("http", StringComparison.OrdinalIgnoreCase))
                        file = "/" + file;

                    html = @"<img src=""" + file + @""" " + html + addInTag + @" />";
                }

                Cache.SetValue(keyCache, html);
            }

            return html;
        }

        public static string GetMedia(string file, int width, int height, string cssClass, string alt, string addInTag, bool compression)
        {
            return GetMedia(2, file, width, height, cssClass, alt, addInTag, compression);
        }

        public static string GetMedia(string file, int width, int height, string cssClass, string alt, string addInTag)
        {
            return GetMedia(2, file, width, height, cssClass, alt, addInTag, true);
        }

        public static string GetMedia(string file, int width, int height, string addInTag)
        {
            return GetMedia(2, file, width, height, string.Empty, string.Empty, addInTag, true);
        }

        public static string GetMedia(string file, int width, int height)
        {
            return GetMedia(2, file, width, height, string.Empty, string.Empty, string.Empty, true);
        }

        public static string GetMedia(string file, int width)
        {
            return GetMedia(2, file, width, 0, string.Empty, string.Empty, string.Empty, true);
        }

        public static string GetMedia(string file)
        {
            return GetMedia(2, file, 0, 0, string.Empty, string.Empty, string.Empty, true);
        }

        #endregion media

        #region fomart 
        public static string FormatPhoneNumber(string phone)
        {
            if (string.IsNullOrEmpty(phone) || phone.Length != 10) return phone;
            else return Regex.Replace(phone, @"(\d{4})(\d{3})(\d{3})", "$1.$2.$3");
        }
        //hàm covert kiểu decimal thành kiểu float 123,456,789.00
        public static string FormatFloat(decimal money)
        {
            return string.Format(CultureInfo.InvariantCulture, "{0:#,##0.##}", money);
        }
        //hàm covert kiểu int thành kiểu float 123,456,789.00
        public static string FormatFloat(int money)
        {
            return string.Format(CultureInfo.InvariantCulture, "{0:#,##0.##}", money);
        }
        //hàm covert kiểu double thành kiểu float 123,456,789.00
        public static string FormatFloat(double money)
        {
            return string.Format(CultureInfo.InvariantCulture, "{0:#,##0.##}", money);
        }
        //hàm covert kiểu decimal thành kiểu tiền tệ 123,456,789
        public static string FormatMoney(decimal money)
        {
            return string.Format(CultureInfo.InvariantCulture, "{0:#,##0}", money);
        }
        //hàm covert kiểu int thành kiểu tiền tệ 123,456,789
        public static string FormatMoney(int money)
        {
            return string.Format(CultureInfo.InvariantCulture, "{0:#,##0}", money);
        }
        //hàm covert kiểu double thành kiểu tiền tệ 123,456,789
        public static string FormatMoney(double money)
        {
            return string.Format(CultureInfo.InvariantCulture, "{0:#,##0}", money);
        }
        //hàm covert kiểu double thành kiểu tiền tệ 123,456,789
        public static string FormatMoney(long money)
        {
            return string.Format(CultureInfo.InvariantCulture, "{0:#,##0}", money);
        }
        //hàm cồng chuỗi 
        public static string GetText(string[] arrString)
        {
            string @string = "";

            for (int i = 0; arrString != null && i < arrString.Length; i++)
                if (!string.IsNullOrEmpty(arrString[i])) @string += (!string.IsNullOrEmpty(@string) ? "," : "") + arrString[i];

            return @string;
        }
        //hàm lấy dữ liệu theo tag xml add vào mảng dữ liệu trả vê
        //public static DataTable Convert_xml_string_to_array(string file_xml, string tag)
        //{
        //    DataTable list = new DataTable();
        //    XmlDataDocument xmldoc = new XmlDataDocument();
        //    XmlNodeList xmlnode;
        //    xmldoc.Load(file_xml);
        //    xmlnode = xmldoc.GetElementsByTagName(tag);
        //    bool flag = false;
        //    for (int i = 0; i <= xmlnode.Count - 1; i++)
        //    {
        //        DataRow dr = list.NewRow();
        //        foreach (System.Xml.XmlNode child in xmlnode[i].ChildNodes)
        //        {
        //            flag = false;
        //            if (list.Columns.Count == 0)
        //            {
        //                flag = true;
        //            }
        //            else
        //            {
        //                for (int k = 0; k < list.Columns.Count; k++)
        //                {
        //                    if (list.Columns[k].ColumnName.ToLower() == child.Name.ToLower())
        //                    {
        //                        flag = false;
        //                        break;
        //                    }
        //                    if (k == list.Columns.Count - 1)
        //                    {
        //                        flag = true;
        //                    }
        //                }
        //            }

        //            if (flag)
        //            {
        //                list.Columns.Add(child.Name);
        //            }
        //            dr[child.Name] = child.InnerText.Trim();
        //        }
        //        list.Rows.Add(dr);
        //    }

        //    return list;
        //}
        //public static string get_value_tag_xml(string file_xml, string tag)
        //{
        //    XmlDataDocument xmldoc = new XmlDataDocument();
        //    XmlNodeList xmlnode;
        //    string value = "";
        //    try
        //    {
        //        xmldoc.Load(file_xml);
        //        xmlnode = xmldoc.GetElementsByTagName(tag);

        //        for (int i = 0; i <= xmlnode.Count - 1; i++)
        //        {
        //            value = xmlnode[i].InnerText;
        //            break;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        Global.Error.Write(e.Message);
        //    }
        //    return value;
        //}
        //hàm covert kiểu DateTime thành kiểu hiển thị ngày dd-MM-yyyy
        public static string FormatDate(DateTime datetime)
        {
            if (datetime <= DateTime.MinValue)
            {
                return "";
            }
            return string.Format("{0:dddd, dd/MM/yyyy}", datetime);
        }
        public static string FormatDate2(DateTime datetime)
        {
            if (datetime <= DateTime.MinValue)
            {
                return "";
            }
            return string.Format("{0:dd/MM/yyyy}", datetime);
        }


        public static string FomartTime(DateTime datetime)
        {
            if (datetime <= DateTime.MinValue)
            {
                return "";
            }
            return string.Format("{0:HH:mm}", datetime);
        }


        //hàm covert kiểu DateTime thành kiểu hiển thị ngày dd-MM-yyyy
        public static string FormatDateTime(DateTime datetime)
        {
            CultureInfo vn = new CultureInfo("vi");
            if (datetime == DateTime.MinValue)
            {
                return "";
            }
            return string.Format(vn, "{0:HH:mm - dddd, dd/MM/yyyy}", datetime);
        }


        #endregion
    }
}