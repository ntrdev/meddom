using System;
using CoreMr.Reddevil.Data;

namespace Reddevil.Lib.Global
{
    public static class Error
    {
        private static int Year => DateTime.Now.Year;

        private static int Month => DateTime.Now.Month;

        private static string PathCurrent
        {
            get
            {
                var path = CoreMr.Reddevil.Global.Application.BaseDirectory + "/Logs";

                //bo qua loi
                try
                {
                    Directory.Create(path + "/" + Year);
                }
                catch
                {
                    //ignored
                }

                return path;
            }
        }

        public static void Write(string func, string var, Exception exception)
        {
            Write(func + " : " + var + "\r\n", exception);
        }

        public static void Write(string func, Exception exception)
        {
            Write(func + " : \r\n", exception);
        }

        public static void Write(Exception exception)
        {
            var message = "Message : " + exception.Message + "\r\n";

            //neu la loi SQL
            if (exception is SQLException ex)
            {
                var sqlException = ex;
                message += "SQLText : " + sqlException.CommandText + "\r\n";
                message += "SQLParameters : " + sqlException.Parameters + "\r\n";
            }

            message += "Source : " + exception.Source
                + "\r\nInner : " + exception.InnerException
                + "\r\nTargetSite : " + exception.TargetSite
                + "\r\nStackTrace : " + exception.StackTrace;

            Write(message);
        }

        public static void Write(string message)
        {
            var s = "Time : " + $"{DateTime.Now:dd/MM/yyyy hh:mm:ss}" + "\r\n";
            //s += "IP : " + CoreMr.Reddevil.Web.HttpRequest.IP + "\r\n";
            //s += "URL : " + CoreMr.Reddevil.Web.HttpRequest.Domain + CoreMr.Reddevil.Web.HttpRequest.RawUrl + "\r\n";
            s += message + "\r\n----------------------------------------------";

            //bo qua loi
            try
            {
                File.WriteText(PathCurrent + "/" + Year + "/" + Month + ".err", s);
            }
            catch
            {
                //ignored
            }
        }

        public static void WriteAsync(string message)
        {
            var s = "Time : " + $"{DateTime.Now:dd/MM/yyyy hh:mm:ss}" + "\n";
            //s += "IP : " + CoreMr.Reddevil.Web.HttpRequest.IP + "\r\n";
            //s += "URL : " + CoreMr.Reddevil.Web.HttpRequest.Domain + CoreMr.Reddevil.Web.HttpRequest.RawUrl + "\r\n";
            s += message + "\r\n----------------------------------------------";

            //bo qua loi
            try
            {
                File.WriteText(PathCurrent + "/" + Year + "/" + Month + ".err", s);
            }
            catch
            {
                //ignored
            }
        }
    }
}