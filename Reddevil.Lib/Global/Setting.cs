﻿using CoreMr.Reddevil.Global;

namespace Reddevil.Lib.Global
{
    public class Setting : CoreMr.Reddevil.Web.Setting
    {

        //facebook
        public static string FacebookClientId = Config.GetValue("Facebook.ClientID").ToString();

        public static string FacebookClientSecret = Config.GetValue("Facebook.ClientSecret").ToString();
        public static string FacebookRedirectUri = CoreMr.Reddevil.Web.HttpRequest.Domain + "/login-fb/Facebook";  //Config.GetValue("Facebook.RedirectUri").ToString();

        //google
        public static string GoogleClientId = Config.GetValue("Google.ClientID").ToString();

        public static string GoogleClientSecret = Config.GetValue("Google.ClientSecret").ToString();
        public static string GoogleRedirectUri = CoreMr.Reddevil.Web.HttpRequest.Domain + "/login-gg/Google";//Config.GetValue("Google.RedirectUri").ToString();

        public static string GoogleAPI = CoreMr.Reddevil.Web.HttpRequest.IsLocal ? Config.GetValue("Google.LocalAPI").ToString() : Config.GetValue("Google.ServerAPI").ToString();

        public static string Zalo_ClientID = CoreMr.Reddevil.Global.Config.GetValue("Zalo.ClientID").ToString();
        public static string Zalo_ClientSecret = CoreMr.Reddevil.Global.Config.GetValue("Zalo.ClientSecret").ToString();
        public static string Zalo_RedirectUri = CoreMr.Reddevil.Global.Config.GetValue("Zalo.RedirectUri").ToString();

        public static string TTS_PostUrl = CoreMr.Reddevil.Global.Config.GetValue("Api.TTS").ToString();
        public static string TTS_GetAudio = CoreMr.Reddevil.Global.Config.GetValue("Api.TTS.GetAudio").ToString();
        public static string TTS_Key = CoreMr.Reddevil.Global.Config.GetValue("Api.TTS.Key").ToString();
    }
}