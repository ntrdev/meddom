﻿using CoreMr.Reddevil.Global;
using Reddevil.Lib.Models;

namespace Reddevil.Lib.Global
{
    public class WebLogin
    {
        public static ModUserEntity CurrentUser
        {
            get
            {
                var webPage = CoreMr.Reddevil.Web.Application.CurrentViewPage;

                var obj = webPage?.PageViewState["User.CurrentUser"];

                if (obj != null)
                    return (ModUserEntity)obj;

                ModUserEntity User = null;

                if (UserID > 0)
                    User = ModUserService.Instance.GetForLogin(UserID);

                if (User == null)
                    Logout();

                if (webPage != null)
                    webPage.PageViewState["User.CurrentUser"] = User;

                return User;
            }
        }

        public static string Name => CurrentUser.Name;

        public static int UserID => Convert.ToInt(Cookies.GetValue("Logged", true));

        public static bool IsLogin()
        {
            return CurrentUser != null;
        }

        public static void Logout()
        {
            Cookies.Remove("Logged");
        }

        public static void SetLogin(int UserId, bool savepass)
        {
            Cookies.SetValue("Logged", UserId.ToString(), savepass ? 0 : 120, true);
        }
    }
}