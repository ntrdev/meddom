using System.Web;

namespace Reddevil.Lib.Global
{
    public static class Template
    {
        public static string GetHtml(string template, params object[] Params)
        {
            var sHtml = File.ReadText(HttpContext.Current.Server.MapPath("~/Views/Design/" + template + ".html"));

            for (var i = 0; i < Params.Length - 1; i = i + 2)
            {
                sHtml = sHtml.Replace("{" + Params[i] + "}", Params[i + 1].ToString());
            }

            return sHtml;
        }
    }
}