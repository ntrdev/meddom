﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Aspose.Cells;
using Reddevil.Lib.Models;
using System.IO;

namespace Reddevil.Lib.Global
{
    public class Excel
    {
        public static void Export(List<List<object>> list, int start_row, string sourceFile, string exportFile)
        {
            if (list == null) return;

            Workbook workbook = new Workbook();
            workbook.Open(sourceFile);
            Cells cells = workbook.Worksheets[0].Cells;

            for (int i = 0; i < list.Count; i++)
            {
                for (int j = 0; j < list[i].Count; j++)
                {
                    cells[start_row + i, j].PutValue(list[i][j]);
                }
            }

            workbook.Save(exportFile);
        }

        //public static int Import(string file, int menuID)
        //{
        //    if (!file.EndsWith(".xls") && !file.EndsWith(".xlsx")) return 0;

        //    var fstream = new FileStream(file, FileMode.Open);
        //    var workbook = new Workbook();
        //    workbook.Open(fstream);
        //    var worksheet = workbook.Worksheets[0];

        //    if (worksheet == null || worksheet.Cells.MaxRow < 1) return 0;

        //    int success = 0;

        //    for (int i = 1; i <= worksheet.Cells.MaxRow; i++)
        //    {
        //        string name = worksheet.Cells[i, 0].StringValue.Trim();
        //        string model = worksheet.Cells[i, 1].StringValue.Trim();
        //        int price = CoreMr.Reddevil.Global.Convert.ToInt(worksheet.Cells[i, 2].StringValue.Trim().Replace(".", "").Replace(",", ""));
        //        string summary = worksheet.Cells[i, 3].StringValue.Trim();
        //        string content = worksheet.Cells[i, 4].StringValue.Trim();

        //        string code = Data.GetCode(name);

        //        try
        //        {
        //            var item = new ModProductEntity()
        //            {
        //                ID = 0,
        //                MenuID = menuID,
        //                Name = name,
        //                Url = code,
        //                Sku = model,
        //                Price = price,
        //                Content = content,
        //                Order = GetMaxOrder(menuID),
        //                Published = DateTime.Now,
        //                Updated = DateTime.Now,
        //                Activity = true
        //            };

        //            ModProductService.Instance.Save(item);

        //            //update url
        //            ModCleanURLService.Instance.InsertOrUpdate(item.Url, "Product", item.ID, item.MenuID, item.GetMenu().LangID);

        //            success++;
        //        }
        //        catch
        //        {
        //            continue;
        //        }
        //    }

        //    fstream.Close();
        //    fstream.Dispose();

        //    return success;
        //}
        private static int GetMaxOrderVideo()
        {

            return ModNewsVideoService.Instance.CreateQuery()
                    .Max(o => o.Order)
                    .ToValue().ToInt(0) + 1;
        }
        private static int GetMaxOrderNewsR()
        {

            return ModNewsResearchService.Instance.CreateQuery()
                    .Max(o => o.Order)
                    .ToValue().ToInt(0) + 1;
        }
        public static void ImportNews(string file, int langID)
        {
            if (!file.EndsWith(".xls") && !file.EndsWith(".xlsx")) return;

            FileStream fstream = new FileStream(file, FileMode.Open);
            Workbook workbook = new Workbook();
            workbook.Open(fstream);
            Worksheet worksheet = workbook.Worksheets[0];

            if (worksheet == null || worksheet.Cells.MaxRow < 1) return;

            for (int i = 1; i <= worksheet.Cells.MaxRow; i++)
            {

                int _id = worksheet.Cells[i, 0].IntValue;
                string _name = worksheet.Cells[i, 1].StringValue.Trim();
                var _itemNews = ModNewsService.Instance.CreateQuery().Where(o => o.ID == _id).ToSingle();
                if (_itemNews == null)
                {
                    string code = Data.GetCode(_name);
                    _itemNews = ModNewsService.Instance.CreateQuery().Where(o => o.Name == _name || o.Url == code).ToSingle();
                }
                string _page = worksheet.Cells[i, 3].StringValue.Trim();
                string _med = worksheet.Cells[i, 4].StringValue.Trim();
                DateTime _medupdate = worksheet.Cells[i, 2].DateTimeValue;
                string[] _pages = _page.Split('-');
                if (_pages.Length > 1 && _itemNews != null)
                {
                    int _idP = CoreMr.Reddevil.Global.Convert.ToInt(_pages[1]);
                    var _page2 = SysPageService.Instance.GetByID_Cache(_idP);

                    switch (_idP)
                    {
                        case 72:// tin video

                            var _video = new ModNewsVideoEntity();
                            _video.Activity = true;
                            _video.AdminApprove = _itemNews.AdminApprove;
                            _video.AdminEdit = _itemNews.AdminEdit;
                            _video.Condition = _itemNews.Condition;
                            _video.Content = _itemNews.Content;
                            _video.Summary = _itemNews.Summary;
                            _video.File = _itemNews.File;
                            _video.Cost = !string.IsNullOrEmpty(_med) && _med == "x" ? false : true;
                            _video.MenuID = _page2.MenuID;
                            _video.Name = _itemNews.Name;
                            _video.Order = GetMaxOrderVideo();
                            _video.Published = _medupdate;
                            _video.Updated = _medupdate;
                            _video.Url = _itemNews.Url;
                            _video.View = _itemNews.View;

                            ModNewsVideoService.Instance.Save(_video);

                            ModCleanURLService.Instance.Delete("[Value]=" + _itemNews.ID + " AND [Type]='News'");
                            ModCleanURLService.Instance.InsertOrUpdate(_video.Url, "NewsVideo", _video.ID, _video.MenuID, 1);
                            ModNewsService.Instance.Delete(_itemNews.ID);
                            break;
                        case 104://chia sẻ thông tin
                            _itemNews.MenuID = 104;
                            ModNewsService.Instance.Save(_itemNews, o => new { o.MenuID });
                            ModCleanURLService.Instance.InsertOrUpdate(_itemNews.Url, "News", _itemNews.ID, _itemNews.MenuID, 1);
                            break;

                        case 81://kể chuyện hiện vật
                            var _newsRes = new ModNewsResearchEntity();
                            _newsRes.Name = _itemNews.Name;
                            _newsRes.MenuID = _page2.MenuID;
                            _newsRes.Activity = true;
                            _newsRes.AdminApprove = _itemNews.AdminApprove;
                            _newsRes.AdminEdit = _itemNews.AdminEdit;
                            _newsRes.Condition = _itemNews.Condition;
                            _newsRes.Summary = _itemNews.Summary;
                            _newsRes.Content = _itemNews.Content;
                            _newsRes.Cost = !string.IsNullOrEmpty(_med) && _med == "x" ? false : true;
                            _newsRes.File = _itemNews.File;
                            _newsRes.Order = GetMaxOrderNewsR();
                            _newsRes.Published = _medupdate;
                            _newsRes.Updated = _medupdate;
                            _newsRes.Url = _itemNews.Url;
                            _newsRes.View = _itemNews.View;


                            ModNewsResearchService.Instance.Save(_newsRes);
                            ModCleanURLService.Instance.Delete("[Value]=" + _itemNews.ID + " AND [Type]='News'");
                            ModCleanURLService.Instance.InsertOrUpdate(_newsRes.Url, "NewsResearch", _newsRes.ID, _newsRes.MenuID, 1);
                            ModNewsService.Instance.Delete(_itemNews.ID);
                            break;
                        case 80://ký ức nhà kh
                            _newsRes = new ModNewsResearchEntity();
                            _newsRes.Name = _itemNews.Name;
                            _newsRes.MenuID = _page2.MenuID;
                            _newsRes.Activity = true;
                            _newsRes.AdminApprove = _itemNews.AdminApprove;
                            _newsRes.AdminEdit = _itemNews.AdminEdit;
                            _newsRes.Summary = _itemNews.Summary;
                            _newsRes.Condition = _itemNews.Condition;
                            _newsRes.Content = _itemNews.Content;
                            _newsRes.Cost = !string.IsNullOrEmpty(_med) && _med == "x" ? false : true;
                            _newsRes.File = _itemNews.File;
                            _newsRes.Order = GetMaxOrderNewsR();
                            _newsRes.Published = _medupdate;
                            _newsRes.Updated = _medupdate;
                            _newsRes.Url = _itemNews.Url;
                            _newsRes.View = _itemNews.View;


                            ModNewsResearchService.Instance.Save(_newsRes);
                            ModCleanURLService.Instance.Delete("[Value]=" + _itemNews.ID + " AND [Type]='News'");
                            ModCleanURLService.Instance.InsertOrUpdate(_newsRes.Url, "NewsResearch", _newsRes.ID, _newsRes.MenuID, 1);
                            ModNewsService.Instance.Delete(_itemNews.ID);

                            break;


                        case 105://tin tức khác
                            _itemNews.MenuID = _page2.MenuID;
                            ModNewsService.Instance.Save(_itemNews, o => new { o.MenuID });
                            ModCleanURLService.Instance.InsertOrUpdate(_itemNews.Url, "News", _itemNews.ID, _itemNews.MenuID, 1);
                            break;
                        case 106://tin medg
                            _itemNews.MenuID = _page2.MenuID;
                            ModNewsService.Instance.Save(_itemNews, o => new { o.MenuID });
                            ModCleanURLService.Instance.InsertOrUpdate(_itemNews.Url, "News", _itemNews.ID, _itemNews.MenuID, 1);
                            break;
                        case 71://tin meddom
                            _itemNews.MenuID = _page2.MenuID;
                            ModNewsService.Instance.Save(_itemNews, o => new { o.MenuID });
                            ModCleanURLService.Instance.InsertOrUpdate(_itemNews.Url, "News", _itemNews.ID, _itemNews.MenuID, 1);
                            break;
                    }
                }


            }

            fstream.Close();
            fstream.Dispose();
        }


        public static void ImportCity(string file, int langID)
        {
            if (!file.EndsWith(".xls") && !file.EndsWith(".xlsx")) return;

            FileStream fstream = new FileStream(file, FileMode.Open);
            Workbook workbook = new Workbook();
            workbook.Open(fstream);
            Worksheet worksheet = workbook.Worksheets[0];

            if (worksheet == null || worksheet.Cells.MaxRow < 1) return;

            for (int i = 1; i <= worksheet.Cells.MaxRow; i++)
            {
                string _Name = worksheet.Cells[i, 0].StringValue.Trim();
                string _Code = Data.GetCode(_Name);

                var _item = WebMenuService.Instance.CreateQuery()
                                                .Where(o => o.Activity == true && o.Type == "City" && o.Url == _Code)
                                                .ToSingle_Cache();
                if (_item == null)
                {
                    _item = new WebMenuEntity();

                    _item.Name = _Name;
                    _item.Url = _Code;
                    _item.Type = "City";
                    _item.CityCode = worksheet.Cells[i, 1].StringValue.Trim();

                    _item.ParentID = 1;
                    _item.LangID = langID;

                    _item.Order = GetMaxOrder(langID, 1);
                    _item.Activity = true;

                    WebMenuService.Instance.Save(_item);

                    _item.Levels = 1 + "-" + _item.ID;
                    WebMenuService.Instance.Save(_item, o => new { o.Levels });
                }
            }

            fstream.Close();
            fstream.Dispose();
        }

        public static void ImportDistrict(string file, int langID)
        {
            if (!file.EndsWith(".xls") && !file.EndsWith(".xlsx")) return;

            FileStream fstream = new FileStream(file, FileMode.Open);
            Workbook workbook = new Workbook();
            workbook.Open(fstream);
            Worksheet worksheet = workbook.Worksheets[0];

            if (worksheet == null || worksheet.Cells.MaxRow < 1) return;

            for (int i = 1; i <= worksheet.Cells.MaxRow; i++)
            {
                string _Name = worksheet.Cells[i, 2].StringValue.Trim();
                string _Code = Data.GetCode(_Name);
                string _CityCode = worksheet.Cells[i, 1].StringValue.Trim();

                var _item = WebMenuService.Instance.CreateQuery()
                                    .Where(o => o.Activity == true && o.Type == "City" && o.Url == _Code)
                                    .ToSingle_Cache();

                var parent = WebMenuService.Instance.CreateQuery()
                                    .Where(o => o.Activity == true && o.Type == "City" && o.CityCode == _CityCode)
                                    .ToSingle_Cache();

                if (_item == null)
                {
                    _item = new WebMenuEntity();

                    _item.Name = _Name;
                    _item.Url = _Code;
                    _item.Type = "City";

                    _item.ParentID = parent != null ? parent.ID : 5;
                    _item.LangID = langID;

                    _item.Order = GetMaxOrder(langID, _item.ParentID);
                    _item.Activity = true;

                    WebMenuService.Instance.Save(_item);

                    _item.Levels = parent.Levels + "-" + _item.ID;
                    WebMenuService.Instance.Save(_item, o => new { o.Levels });
                }
            }

            fstream.Close();
            fstream.Dispose();
        }

        public static void ImportWard(string file, int langID)
        {
            if (!file.EndsWith(".xls") && !file.EndsWith(".xlsx")) return;

            FileStream fstream = new FileStream(file, FileMode.Open);
            Workbook workbook = new Workbook();
            workbook.Open(fstream);
            Worksheet worksheet = workbook.Worksheets[0];

            if (worksheet == null || worksheet.Cells.MaxRow < 1) return;

            for (int i = 1; i <= worksheet.Cells.MaxRow; i++)
            {
                string _Name = worksheet.Cells[i, 2].StringValue.Trim();
                string _Code = Data.GetCode(_Name);
                string _CityCode = worksheet.Cells[i, 1].StringValue.Trim();

                string _NameItem = worksheet.Cells[i, 4].StringValue.Trim();
                string _CodeItem = Data.GetCode(_NameItem);

                var parent = WebMenuService.Instance.CreateQuery()
                                  .Where(o => o.Activity == true && o.Type == "City" && o.CityCode == _CityCode)
                                  .ToSingle_Cache();

                var _item = WebMenuService.Instance.CreateQuery()
                                    .Where(o => o.Activity == true && o.Type == "City" && o.ParentID == parent.ID && o.Url == _Code)
                                    .ToSingle_Cache();

                if (_item != null)
                {
                    var _itemName = WebMenuService.Instance.CreateQuery()
                                       .Where(o => o.Activity == true && o.Type == "City" && o.Url == _CodeItem && o.ParentID == _item.ID)
                                       .ToSingle_Cache();





                    if (_itemName == null)
                    {
                        _itemName = new WebMenuEntity();

                        _itemName.Name = _NameItem;
                        _itemName.Url = _CodeItem;
                        _itemName.Type = "City";

                        _itemName.ParentID = _item != null ? _item.ID : 1;
                        _itemName.LangID = langID;

                        _itemName.Order = GetMaxOrder(langID, _itemName.ParentID);
                        _itemName.Activity = true;

                        WebMenuService.Instance.Save(_itemName);

                        _itemName.Levels = _item.Levels + "-" + _itemName.ID;
                        WebMenuService.Instance.Save(_itemName, o => new { o.Levels });
                    }
                }
            }

            fstream.Close();
            fstream.Dispose();
        }

        private static int GetMaxOrder(int langID, int parentID)
        {
            return WebMenuService.Instance.CreateQuery()
                            .Where(o => o.LangID == langID && o.ParentID == parentID)
                            .Max(o => o.Order)
                            .ToValue().ToInt(0) + 1;
        }

        //private static int GetMaxOrder(int menuID)
        //{
        //    return ModProductService.Instance.CreateQuery()
        //                    .Where(o => o.MenuID == menuID)
        //                    .Max(o => o.Order)
        //                    .ToValue().ToInt(0) + 1;
        //}
    }
}
