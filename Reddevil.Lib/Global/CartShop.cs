﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Reddevil.Lib.Global
{
    public class CartShopItem
    {

        public int ProductID { get; set; }
        public int IDShop { get; set; }
        public int Quantity { get; set; }
        public int Price { get; set; }
        public int PriceShipping { get; set; }
        public string GiffCode { get; set; }
        public int PriceSaleOff { get; set; }
        public int Seller { get; set; }
        public int Affilate { get; set; }
        public bool Negotiate { get; set; }

        public override bool Equals(object obj)
        {
            if (!(obj is CartShopItem)) return base.Equals(obj);

            var temp = (CartShopItem)obj;

            return ProductID.Equals(temp.ProductID) && IDShop.Equals(temp.IDShop);
        }

        public override int GetHashCode()
        {
            return (ProductID + "-" + IDShop).GetHashCode();
        }
    }

    public class CartShop
    {
        private readonly List<CartShopItem> _listItem = new List<CartShopItem>();
        private readonly string _cookieKey = "MrReddevil_CartShop_";

        public ReadOnlyCollection<CartShopItem> Items => _listItem.AsReadOnly();

        public int Count => _listItem.Count;

        public CartShop()
            : this(string.Empty)
        {
        }

        public CartShop(string serviceName)
        {
            _cookieKey += serviceName;

            if (ObjectCookies<List<CartShopItem>>.Exists(_cookieKey))
                _listItem = ObjectCookies<List<CartShopItem>>.GetValue(_cookieKey);

            if (_listItem == null)
                _listItem = new List<CartShopItem>();
        }


        public bool Exists(CartShopItem item)
        {
            return _listItem.Contains(item);
        }

        public void Add(CartShopItem item)
        {
            Remove(item);

            _listItem.Add(item);
        }

        public CartShopItem Find(CartShopItem item)
        {
            return _listItem.Find(o => o.Equals(item));
        }

        public void Remove(CartShopItem item)
        {
            if (Exists(item))
                _listItem.Remove(item);
        }

        public void RemoveAll()
        {
            _listItem.Clear();
        }

        public void Save()
        {
            if (_listItem.Count > 0)
                ObjectCookies<List<CartShopItem>>.SetValue(_cookieKey, _listItem);
            else
                ObjectCookies<List<CartShopItem>>.Remove(_cookieKey);
        }
    }
}