﻿using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using Reddevil.Lib.Global;
using Array = CoreMr.Reddevil.Global.Array;

namespace Reddevil.Lib.MVC
{
    public class CPController : CoreMr.Reddevil.MVC.Controller
    {
        public CPViewPage CPViewPage => ViewPageBase as CPViewPage;
        public CPViewControl CPViewControl => ViewControl as CPViewControl;

        protected dynamic DataService { get; set; }
        protected dynamic DataEntity { get; set; }
        protected bool CheckPermissions { get; set; }

        protected string AutoSort(string sort)
        {
            return AutoSort(sort, "[ID] DESC");
        }

        protected string AutoSort(string sort, string orderDefault)
        {
            if (string.IsNullOrEmpty(sort))
                return orderDefault;

            var sortType = sort.Split('-')[0]
                                  .Replace("'", string.Empty)
                                  .Replace("-", string.Empty)
                                  .Replace(";", string.Empty);

            var sortDesc = string.Equals("desc", sort.Split('-')[1].ToLower(), StringComparison.OrdinalIgnoreCase);

            return "[" + sortType + "] " + (sortDesc ? "DESC" : "ASC");
        }

        protected int GetState(int[] arrState)
        {
            var state = 0;

            for (var i = 0; arrState != null && i < arrState.Length; i++)
                if (arrState[i] > 0) state ^= arrState[i];

            return state;
        }

        protected int GetState(List<int> arrState)
        {
            var state = 0;

            for (var i = 0; arrState != null && i < arrState.Count; i++)
                if (arrState[i] > 0) state ^= arrState[i];

            return state;
        }

        protected void SaveRedirect()
        {
            CPViewPage.SetMessage("Thông tin đã cập nhật.");
            CPViewPage.Response.Redirect(CPViewPage.Request.RawUrl.Replace("Add.aspx", "Index.aspx"));
        }

        protected void ApplyRedirect(int recordID, int itemID)
        {
            CPViewPage.SetMessage("Thông tin đã cập nhật.");

            if (recordID > 0)
                CPViewPage.RefreshPage();
            else
                CPViewPage.Response.Redirect(CPViewPage.Request.RawUrl + "/RecordID/" + itemID);
        }

        protected void SaveNewRedirect(int recordID, int itemID)
        {
            CPViewPage.SetMessage("Thông tin đã cập nhật.");

            CPViewPage.Response.Redirect(recordID > 0
                ? CPViewPage.Request.RawUrl.Replace("/RecordID/" + itemID, string.Empty)
                : CPViewPage.Request.RawUrl);
        }

        public virtual void ActionCancel()
        {
            CPViewPage.Response.Redirect(CPViewPage.Request.RawUrl.Replace("Add.aspx", "Index.aspx"));
        }

        public virtual void ActionConfig()
        {
            CoreMr.Reddevil.Web.Cache.Clear(DataService);

            //thong bao
            CPViewPage.SetMessage("Xóa cache thành công.");
            CPViewPage.RefreshPage();
        }

        public virtual void ActionCopy(int id)
        {
            if (CheckPermissions && !CPViewPage.UserPermissions.Approve)
            {
                //thong bao
                CPViewPage.Message.MessageType = Message.MessageTypeEnum.Error;
                CPViewPage.Message.ListMessage.Add("Quyền hạn chế.");
                return;
            }

            var item = DataService.GetByID(id);

            string _typeof = item.GetType().ToString();

            if (string.Equals(_typeof.ToLower(), "Reddevil.Lib.Models.ModProductEntity", StringComparison.OrdinalIgnoreCase))
            {
                item.ID = 0;
                item.IDRnd = Reddevil.Lib.Global.Random.GetRandom(6, false);
                item.Url = Data.GetCode(item.Name) + "-" + item.IDRnd;
                item.Sku = "SPX-" + Reddevil.Lib.Global.Random.GetRandom(4, false);

                item.ShopID = 0;
                item.AdminEdit = "Set Copy";

                DataService.Save(item);

                Models.ModCleanURLService.Instance.InsertOrUpdate(item.Url, "Product", item.ID, item.MenuID, 1);

            }
            else
            {
                item.ID = 0;
                item.Name += " - (Copy)";
                DataService.Save(item);
            }
            //thong bao
            CPViewPage.SetMessage("Sao chép thành công.");
            CPViewPage.RefreshPage();
        }

        public void ActionAutoSave(int recordID)
        {
            if (CheckPermissions && !CPViewPage.UserPermissions.Approve)
            {
                //thong bao
                CPViewPage.Message.MessageType = Message.MessageTypeEnum.Error;
                CPViewPage.Message.ListMessage.Add("Quyền hạn chế.");
                return;
            }

            dynamic item;

            if (recordID > 0)
            {
                item = DataService.GetByID(recordID);
                if (item != null)
                {
                    TryUpdateModel(item);

                    DataService.Save(item);
                }
            }
            else
            {
                item = DataEntity;
                TryUpdateModel(item);

                var json = new JavaScriptSerializer().Serialize(item);
                //var json = JsonConvert.SerializeObject(item);
                Cookies.SetValue(DataService.ToString(), json, true);
            }

            //thong bao
            CPViewPage.SetMessage("Tự động lưu nội dung.");
            CPViewPage.RefreshPage();
        }

        public virtual void ActionPublish(int[] arrID)
        {
            if (CPViewPage.CurrentUser.IsAdministrator && CPViewPage.UserPermissions.Approve)
            {
                var _User = CPLogin.CurrentUser.LoginName;
                DataService.Update("[ID] IN (" + Array.ToString(arrID) + ")", "@AdminApprove", _User);
                DataService.Update("[ID] IN (" + Array.ToString(arrID) + ")", "@Activity", 1);
                DataService.Update("[ID] IN (" + Array.ToString(arrID) + ")", "@Condition", 0);
                CPViewPage.SetMessage("Đã duyệt thành công.");
                CPViewPage.RefreshPage();

            }
            else
            {
                //thong bao
                CPViewPage.Message.MessageType = Message.MessageTypeEnum.Error;
                CPViewPage.Message.ListMessage.Add("Quyền hạn chế.");
                return;
            }
        }

        public virtual void ActionUnPublish(int[] arrID)
        {

            if (CPViewPage.CurrentUser.IsAdministrator && CPViewPage.UserPermissions.Approve)
            {

              
                DataService.Update("[ID] IN (" + Array.ToString(arrID) + ")", "@Activity", 0);
                DataService.Update("[ID] IN (" + Array.ToString(arrID) + ")", "@Condition", 3);
                CPViewPage.SetMessage("Đã bỏ duyệt thành công.");
                CPViewPage.RefreshPage();

            }
            else
            {
                //thong bao
                CPViewPage.Message.MessageType = Message.MessageTypeEnum.Error;
                CPViewPage.Message.ListMessage.Add("Quyền hạn chế.");

            }

        }

        public virtual void ActionDelete(int[] arrID)
        {
            if (CheckPermissions && !CPViewPage.UserPermissions.Delete)
            {
                //thong bao
                CPViewPage.Message.MessageType = Message.MessageTypeEnum.Error;
                CPViewPage.Message.ListMessage.Add("Bạn không có quyền xóa.");
                return;
            }

            string _typeof = DataService.GetType().ToString();

            DataService.Delete("[ID] IN (" + Array.ToString(arrID) + ")");
            if (string.Equals(_typeof.ToLower(), "Reddevil.Lib.Models.ModProductEntity", StringComparison.OrdinalIgnoreCase))
            {
                Models.ModCleanURLService.Instance.Delete("[Value] IN (" + Array.ToString(arrID) + ") AND [Type]='Product'");
            }
            else if (string.Equals(_typeof.ToLower(), "Reddevil.Lib.Models.ModProductEntity", StringComparison.OrdinalIgnoreCase))
            {
                Models.ModCleanURLService.Instance.Delete("[Value] IN (" + Array.ToString(arrID) + ") AND [Type]='News'");
            }

            //thong bao
            CPViewPage.SetMessage("Đã xóa thành công.");
            CPViewPage.RefreshPage();
        }

        public virtual void ActionSaveOrder(int[] arrID)
        {
            if (CheckPermissions && !CPViewPage.UserPermissions.Approve)
            {
                //thong bao
                CPViewPage.Message.MessageType = Message.MessageTypeEnum.Error;
                CPViewPage.Message.ListMessage.Add("Quyền hạn chế.");
                return;
            }

            for (var i = 0; i < arrID.Length - 1; i = i + 2)
            {
                DataService.Update("[ID]=" + arrID[i], "@Order", arrID[i + 1]);
            }

            //thong bao
            CPViewPage.SetMessage("Đã sắp xếp thành công.");
            CPViewPage.RefreshPage();
        }




        public virtual void ActionPublishGX(int[] arrID)
        {
            if (CheckPermissions && !CPViewPage.UserPermissions.Approve)
            {
                //thong bao
                CPViewPage.Message.MessageType = Message.MessageTypeEnum.Error;
                CPViewPage.Message.ListMessage.Add("Quyền hạn chế.");
                return;
            }


            DataService.Update("[ID]=" + arrID[0], "@Activity", arrID[1]);

            //thong bao
            CPViewPage.SetMessage(arrID[1] == 0 ? "Đã chuyển sang chờ duyệt thành công." : "Đã duyệt thành công.");
            CPViewPage.RefreshPage();
        }
        public virtual void ActionHide(int[] arrID)
        {
            if (CPViewPage.CurrentUser.IsAdministrator && CPViewPage.UserPermissions.Full)
            {

                DataService.Update("[ID]=" + arrID[0], "@ShowMenuTop", arrID[1]);

                //thong bao
                CPViewPage.SetMessage(arrID[1] == 0 ? "Đã ẩn thành công." : "Đã hiển thị thành công.");
                CPViewPage.RefreshPage();


            }
            else
            {
                //thong bao
                CPViewPage.Message.MessageType = Message.MessageTypeEnum.Error;
                CPViewPage.Message.ListMessage.Add("Quyền hạn chế.");
                return;
            }


        }


    }

    public class DefaultModel
    {
        private int _pageIndex;

        public int PageIndex
        {
            get => _pageIndex;
            set => _pageIndex = value - 1;
        }

        public int PageSize { get; set; } = 20;

        public int TotalRecord { get; set; }

        public string Sort { get; set; }

        public int RecordID { get; set; }
    }
}