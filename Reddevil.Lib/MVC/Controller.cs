﻿namespace Reddevil.Lib.MVC
{
    public class Controller : CoreMr.Reddevil.MVC.Controller
    {
        public ViewPage ViewPage => ViewPageBase as ViewPage;
        public new ViewControl ViewControl => base.ViewControl as ViewControl;
    }
}