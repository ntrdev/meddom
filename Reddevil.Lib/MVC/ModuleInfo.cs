﻿using System;
using CoreMr.Reddevil.Interface;

namespace Reddevil.Lib.MVC
{
    [AttributeUsage(AttributeTargets.Class)]
    public class ModuleInfo : Attribute, IModuleInterface
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public bool IsControl { get; set; }
        public int Order { get; set; }
        
        public bool Crawl { get; set; }
        public Type ModuleType { get; set; }
    }
}