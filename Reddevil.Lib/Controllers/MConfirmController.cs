﻿using Reddevil.Lib.Global;
using Reddevil.Lib.Models;
using Reddevil.Lib.MVC;

namespace Reddevil.Lib.Controllers
{
    [ModuleInfo(Name = "MO_Default: Df - Xác nhận tài khoản", Code = "MConfirm", Order = 2, Crawl = false)]
    public class MConfirmController : Controller
    {
        public void ActionIndex(MConfirmModel model)
        {
            if (model == null || string.IsNullOrEmpty(model.uid) || string.IsNullOrEmpty(model.code)) ViewPage.Response.Redirect("/");

            string code = Data.FormatRemoveSql(model.code);

            int uid = CoreMr.Reddevil.Global.Convert.ToInt(Security.Decrypt(model.uid));


            var item = ModUserService.Instance.GetByID(uid);

            if (item != null)
            {
                item.VrCode = string.Empty;
                item.Activity = true;

                ModUserService.Instance.Save(item, o => new { o.VrCode, o.Activity });

                WebLogin.SetLogin(item.ID, true);

                ViewPage.AlertThenRedirect("Thông báo !", "Quý vị đã kích hoạt tài khoản thành công.", "/");
            }
            else
            {
                ViewPage.AlertThenRedirect("Thông báo !", "Tài khoản không tồn tại hoặc sai mã kích hoạt.", "/");
            }
        }
    }

    public class MConfirmModel
    {
        public string Email { get; set; }
        public string uid { get; set; }
        public string code { get; set; }
    }
}