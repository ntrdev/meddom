﻿using Reddevil.Lib.Global;
using Reddevil.Lib.Models;
using Reddevil.Lib.MVC;

namespace Reddevil.Lib.Controllers
{
    [ModuleInfo(Name = "MO_Default: Df - Nhà khoa học", Code = "MScientist", Order = 10, Crawl = true)]
    public class MScientistController : Controller
    {


        [CoreMr.Reddevil.MVC.PropertyInfo("Số lượng hiển thị")]
        public int PageSize = 10;

        public void ActionIndex(MScientistModel model)
        {
            if (ViewPage.ViewBag.Item != null)
            {
                RedirectToAction("Detail");
                return;
            }


            var dbQuery = ModScientistService.Instance.StoreProcedure("meddom_org_get_nkh")
                                                      .AddParam(!string.IsNullOrEmpty(model.cate), "@pCate", model.cate)
                                                      .AddParam(!string.IsNullOrEmpty(model.news), "@pKeyword", model.news)
                                                      .AddParam(!string.IsNullOrEmpty(model.nkh), "@pEducation", model.nkh)
                                                      .AddParam("@RowsOfPage", PageSize)
                                                      .AddParam("@PageNumber", model.page);


            ViewBag.Data = dbQuery.ToList_Cache();
            model.TotalRecord = dbQuery.TotalRecord;
            model.PageSize = PageSize;
            ViewBag.Model = model;


            //SEO
            ViewPage.CurrentPage.PageTitle = ViewPage.CurrentPage.Name;
            ViewPage.CurrentPage.PageURL = ViewPage.CurrentURL;
            ViewPage.CurrentPage.PageFile = CoreMr.Reddevil.Web.HttpRequest.Domain + Global.Utils.GetResizeFile(ViewPage.CurrentPage.File, 4, 630, 315);
        }

        public void ActionDetail(string endCode)
        {

            if (ViewPage.ViewBag.Item != null)
            {

                ViewBag.Data = ViewPage.ViewBag.Item as ModScientistEntity;

                ViewPage.CurrentPage.PageTitle = ViewPage.ViewBag.Item.Degree + " " + ViewPage.ViewBag.Item.FullName + " | " + CoreMr.Reddevil.Web.HttpRequest.Host;
                ViewPage.CurrentPage.PageKeywords = ViewPage.CurrentPage.PageTitle;
                ViewPage.CurrentPage.PageURL = ViewPage.ViewScientistURL + Data.GetCode(ViewPage.ViewBag.Item.Degree + " " + ViewPage.ViewBag.Item.FullName) + "-" + ViewPage.ViewBag.Item.Id;
                ViewPage.CurrentPage.PageFile = Global.Utils.GetResizeFile("http://img.meddom.org/0" + ViewBag.Data.Images, 4, 600, 315);

            }
            else
            {
                ViewPage.Error404();
            }

        }
    }

    public class MScientistModel
    {
        private int _page;
        public int page
        {
            get { return _page; }
            set { _page = value - 1; }
        }

        public int TotalRecord { get; set; }
        public int PageSize { get; set; }
        public string cate { get; set; }
        public string nkh { get; set; }
        public string news { get; set; }
        public int choseid { get; set; }
    }
}