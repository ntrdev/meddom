﻿using System;
using System.Drawing;
using System.Net.Mail;
using System.Net.Mime;
using System.Text.RegularExpressions;
using CoreMr.Reddevil.Web;
using Reddevil.Lib.Global;
using Reddevil.Lib.Models;
using Reddevil.Lib.MVC;

namespace Reddevil.Lib.Controllers
{
    [ModuleInfo(Name = "MO_Default: Df - Đăng ký thành viên", Code = "MRegister", Order = 12, Crawl = false)]
    public class MRegisterController : Controller
    {
        public void ActionIndex(ModUserEntity item, MRegisterModel model)
        {
            item.Gender = true;
            ViewBag.Data = item;
            ViewBag.Model = model;
            ViewBag.Job = WebMenuService.Instance.CreateQuery()
                                    .Select(o => new { o.Name, o.ID })
                                    .Where(o => o.Activity == true && o.Type == "Job" && o.ParentID > 0)
                                    .OrderByAsc(o => o.Name)
                                    .ToList_Cache();
            //SEO
            ViewPage.CurrentPage.PageURL = ViewPage.CurrentURL;
            ViewPage.CurrentPage.PageFile = HttpRequest.Domain + Global.Utils.GetResizeFile(ViewPage.CurrentPage.File, 4, 630, 315);
        }

        public void ActionAddPOST(ModUserEntity item, MRegisterModel model)
        {

            if (item.LoginName.Trim() == string.Empty)
                ViewPage.Message.ListMessage.Add("Nhập: Tên tài khoản.");
            else if (!Global.Utils.IsLoginName(item.LoginName.Trim()))
                ViewPage.Message.ListMessage.Add("Nhập: Tên tài khoản không bao gồm các ký tự đặc biệt.");
            else if (ModUserService.Instance.GetByLoginName(item.LoginName.Trim()) != null)
                ViewPage.Message.ListMessage.Add("Tên tài khoản đã tồn tại.");

            if (item.Password.Trim() == string.Empty)
                ViewPage.Message.ListMessage.Add("Nhập: Mật khẩu.");
            else if (item.Password.Trim().Length < 6 || item.Password.Trim().Length > 12)
                ViewPage.Message.ListMessage.Add("Mật khẩu phải từ 6-12 ký tự.");
            else if (item.Password.Trim() != model.Password2)
                ViewPage.Message.ListMessage.Add("Mật khẩu không đồng nhất.");

            if (item.Name.Trim() == string.Empty)
                ViewPage.Message.ListMessage.Add("Nhập: Họ và tên.");


            if (ModUserService.Instance.GetByEmail(item.Email.Trim()) != null)
                ViewPage.Message.ListMessage.Add("Email đã tồn tại. Hãy chọn email khác.");

            if (item.Phone.Trim() == string.Empty)
                ViewPage.Message.ListMessage.Add("Nhập: Số điện thoại.");

            else if (!Global.Utils.IsPhoneNumber(item.Phone))
                ViewPage.Message.ListMessage.Add("Nhập: Đúng định dạng số điện thoại.");

            else if (ModUserService.Instance.GetByPhone(item.Phone.Replace(".", "")) != null)
                ViewPage.Message.ListMessage.Add("Số điện thoại đã tồn tại.");


            string sVY = CoreMr.Reddevil.Global.Security.Decrypt(ViewPage.Session["CaptchaREDDEVIL"].ToString()).Replace("CaptchaREDDEVIL.Secure." + ViewPage.Request.UserHostAddress + "." + string.Format("yyyy.MM.dd.hh", DateTime.Now) + ".", string.Empty);
            string sValidCode = model.ValidCode.Trim();

            if (sVY == string.Empty || (sVY.ToLower() != sValidCode.ToLower()))
                ViewPage.Message.ListMessage.Add("Nhập mã an toàn.");

            if (ViewPage.Request.Files.Count > 0)
            {
                var _File = ViewPage.Request.Files[0];
                if (_File == null)
                    ViewPage.Message.ListMessage.Add("Bạn chưa tải ảnh đại diện.");

                string _FileName = _File.FileName;
                if (!string.IsNullOrEmpty(_FileName))
                {
                    if (_File.ContentLength > 5 * 1024 * 1024)
                        ViewPage.Message.ListMessage.Add("Dung lượng File ảnh không được lớn hơn 5Mb.");

                    try
                    {
                        new Bitmap(_File.InputStream);

                        string _Path = ViewPage.Server.MapPath("~/Data/upload/images/user/avartar/");

                        if (!System.IO.Directory.Exists(_Path))
                            System.IO.Directory.CreateDirectory(_Path);

                        string _DBFile = "~/data/upload/images/user/avartar/" + Data.GetCode(System.IO.Path.GetFileNameWithoutExtension(_FileName)) + System.IO.Path.GetExtension(_FileName);
                        string _PhysFile = ViewPage.Server.MapPath(_DBFile);

                        _File.SaveAs(_PhysFile);

                        item.Avatar = _DBFile.Replace("~/", "/");
                    }
                    catch
                    {
                        ViewPage.Message.ListMessage.Add("File ảnh đại diện tải lên có lỗi. Vui lòng chọn file ảnh khác.");
                    }
                }
            }
            //hien thi thong bao loi
            if (ViewPage.Message.ListMessage.Count > 0)
            {
                string message = string.Empty;
                for (int i = 0; i < ViewPage.Message.ListMessage.Count; i++)
                    message += ViewPage.Message.ListMessage[i] + "<br />";

                ViewPage.Alert(message);
            }
            else
            {



                string pass = item.Password;

                item.ID = 0;
                item.Activity = false;
                item.Password = Security.Md5(item.Password.Trim());
                item.Created = DateTime.Now;
                item.VrCode = Guid.NewGuid().ToString();
                item.Phone = item.Phone.Replace(".", "");

                ModUserService.Instance.Save(item);


                if (item.ID > 0 && !string.IsNullOrEmpty(item.Email))
                {
                    string sURL = CoreMr.Reddevil.Web.HttpRequest.Domain + "/confirm?code=" + item.VrCode + "&uid=" + Security.Encrypt(item.ID.ToString());

                    string sHtml = Template.GetHtml("EmailActive",
                      "URL", sURL,
                      "Email", item.LoginName,
                      "Pass", pass
                      );

                    var _AvHtml = AlternateView.CreateAlternateViewFromString(sHtml, null, MediaTypeNames.Text.Html);

                   
                    var domain = ViewPage.Request.Url.Host;
                    Mail.SendMailAsync(item.Email, "noreply@gmail.com", domain, domain + "- Kích hoạt tài khoản - Đăng ký ngày:" + string.Format("{0:dd/MM/yyyy HH:mm}", DateTime.Now), _AvHtml, null);

                }
                item = new ModUserEntity();
                ViewPage.AlertThenRedirect("Quý vị đã đăng ký thành công.<br/> Vui lòng đợi kiểm tra hộp thư email để xác nhận tài khoản.", "/");


                ViewBag.Data = item;
                ViewBag.Model = model;
            }
        }


    }
    public class MRegisterModel
    {
        public string ValidCode { get; set; }
        public string Password2 { get; set; }

        public int User2 { get; set; }

        public int User1 { get; set; }
        public int uid { get; set; }
    }
}
