﻿using System;
using Reddevil.Lib.Global;
using Reddevil.Lib.Models;
using Reddevil.Lib.MVC;

namespace Reddevil.Lib.Controllers
{
    [ModuleInfo(Name = "MO_Default: Df - Liên hệ", Code = "MFeedback", Order = 3, Crawl = false)]
    public class MFeedbackController : Controller
    {
        public void ActionIndex(ModFeedbackEntity item, MFeedbackModel model)
        {
            ViewBag.Data = item;
            ViewBag.Model = model;

            //SEO
            ViewPage.CurrentPage.PageURL = ViewPage.CurrentURL;
            ViewPage.CurrentPage.PageFile = CoreMr.Reddevil.Web.HttpRequest.Domain + Global.Utils.GetResizeFile(ViewPage.CurrentPage.File, 4, 600, 315);
        }

        public void ActionAddPOST(ModFeedbackEntity item, MFeedbackModel model)
        {
            if (item.Name.Trim() == string.Empty)
                ViewPage.Message.ListMessage.Add("Nhập: Họ và tên.");

            if (item.Email.Trim() == string.Empty)
                ViewPage.Message.ListMessage.Add("Nhập: Địa chỉ email.");
            else if (!Utils.IsEmailAddress(item.Email.Trim()))
                ViewPage.Message.ListMessage.Add("Nhập: Đúng định dạng email.");

            if (item.Phone.Trim() == string.Empty)
                ViewPage.Message.ListMessage.Add("Nhập: Só điện thoại.");
            else if (Utils.IsPhoneNumber(item.Phone.Trim()))
                ViewPage.Message.ListMessage.Add("Nhập: Đúng định dạng số điện thoại.");

            if (item.Content.Trim() == string.Empty)
                ViewPage.Message.ListMessage.Add("Nhập: Nội dung liên hệ.");


            string sVY = CoreMr.Reddevil.Global.Security.Decrypt(ViewPage.Session["CaptchaREDDEVIL"].ToString()).Replace("CaptchaREDDEVIL.Secure." + ViewPage.Request.UserHostAddress + "." + string.Format("yyyy.MM.dd.hh", DateTime.Now) + ".", string.Empty);
            string sValidCode = model.ValidCode.Trim();

            if (sVY == string.Empty || (sVY.ToLower() != sValidCode.ToLower()))
                ViewPage.Message.ListMessage.Add("Nhập mã an toàn.");

            //hien thi thong bao loi
            if (ViewPage.Message.ListMessage.Count > 0)
            {
                string message = string.Empty;
                for (int i = 0; i < ViewPage.Message.ListMessage.Count; i++)
                    message += ViewPage.Message.ListMessage[i] + "<br />";

                ViewPage.Alert(message);
            }
            else
            {
                item.ID = 0;
                item.IP = CoreMr.Reddevil.Web.HttpRequest.IP;
                item.Created = DateTime.Now.Date;

                ModFeedbackService.Instance.Save(item);

                //gui mail

                #region send mail

                var listEmail = item.Email.Trim() + "," + WebResource.GetValue("Hotro_Email");

                string sHtml = Reddevil.Lib.Global.Template.GetHtml("EmailForot", "Name", item.Name);

                Global.Mail.SendMail(
                    listEmail,
                    "noreply@gmail.com",
                    CoreMr.Reddevil.Web.HttpRequest.Domain + "/",
                    "Thông tin phản hồi từ Quý khách " + item.Name,
                    sHtml
                );

                #endregion send mail

                //xoa trang
                item = new ModFeedbackEntity();
                ViewPage.Alert("Cảm ơn bạn đã gửi phản hồi tới chúng tôi.<br /> Chúng tôi sẽ phản hồi lại trong thời gian sớm nhất.");
            }

            ViewBag.Data = item;
            ViewBag.Model = model;
        }
    }

    public class MFeedbackModel
    {
        public string ValidCode { get; set; }
    }
}