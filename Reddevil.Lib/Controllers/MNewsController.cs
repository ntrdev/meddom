﻿using Reddevil.Lib.Global;
using Reddevil.Lib.Models;
using Reddevil.Lib.MVC;

namespace Reddevil.Lib.Controllers
{
    [ModuleInfo(Name = "MO_Default: Df - Tin tức", Code = "MNews", Order = 10, Crawl = true)]
    public class MNewsController : Controller
    {
        [CoreMr.Reddevil.MVC.PropertyInfo("Danh mục", "Type|News")]
        public int MenuID;

        [CoreMr.Reddevil.MVC.PropertyInfo("Dạng hiển thị", "Danh sách|Index,Bộ sưu tập|HoatDong")]
        public string Layout = "Index";

        [CoreMr.Reddevil.MVC.PropertyInfo("Số lượng hiển thị")]
        public int PageSize = 20;

        public void ActionIndex(MNewsModel model)
        {
            // Utils.UpdateStatistic();

            if (ViewPage.CurrentPage.MenuID > 0)
                MenuID = ViewPage.CurrentPage.MenuID;

            string _Keyword = !string.IsNullOrEmpty(model.news) ? Data.GetCode(model.news) : "";
            RenderView(Layout);
            PageSize = model.PageSize > 0 ? model.PageSize : PageSize;


            var dbQuery = ModNewsService.Instance.CreateQuery()
                                    .Where(o => o.Status == false)
                                    .Where(o => o.Activity == true)
                                    .Where(!string.IsNullOrEmpty(model.news), o => o.Url.Contains(_Keyword) || o.Name.Contains(model.news))
                                    .WhereIn(MenuID > 0, o => o.MenuID, WebMenuService.Instance.GetChildIDForWeb_Cache("News", MenuID, ViewPage.CurrentLang.ID))
                                    .Skip(PageSize * model.page)
                                    .Take(PageSize);

            string sort = CoreMr.Reddevil.Web.HttpQueryString.GetValue("sort").ToString();

            switch (sort.ToLower())
            {
                case "new_asc":
                    dbQuery.OrderByDesc(o => o.Order);
                    break;
                case "view_desc":
                    dbQuery.OrderByDesc(o => o.View);
                    break;
                case "":
                    dbQuery.OrderByDesc(o => new { o.ID, o.Order });
                    break;
            }

            ViewBag.Data = dbQuery.ToList_Cache();
            model.TotalRecord = dbQuery.TotalRecord;
            model.PageSize = PageSize;
            ViewBag.Model = model;

            //SEO
            ViewPage.CurrentPage.PageURL = ViewPage.CurrentURL;
            ViewPage.CurrentPage.PageFile = CoreMr.Reddevil.Web.HttpRequest.Domain + Global.Utils.GetResizeFile(ViewPage.CurrentPage.File, 4, 630, 315);
        }
        private ModNewsEntity item;
        public void ActionDetail(string endCode)
        {
            if (ViewPage.CurrentPage.MenuID > 0)
                MenuID = ViewPage.CurrentPage.MenuID;

            if (Cookies.Exist("CP.UserID"))
            {
                item = ModNewsService.Instance.CreateQuery()
                               .Where(o => o.Url == endCode)
                               .ToSingle_Cache();
            }
            else
            {
                item = ModNewsService.Instance.CreateQuery()
                              .Where(o => o.Activity == true && o.Url == endCode)
                              .ToSingle_Cache();
            }

            if (item != null)
            {
                // Utils.UpdateStatistic();

                //up view
                item.UpView();

                ViewBag.Other = ModNewsService.Instance.CreateQuery()
                                            .Where(o => o.Activity == true && o.ID != item.ID)
                                            .WhereIn(MenuID > 0, o => o.MenuID, WebMenuService.Instance.GetChildIDForWeb_Cache("News", MenuID, ViewPage.CurrentLang.ID))
                                            .OrderByDesc(o => new { o.Published, o.ID })
                                            .Take(10)
                                            .ToList_Cache();

                ViewPage.ViewBag.Data = ViewBag.Data = item;

                ViewBag.Lienquan = ModNewsService.Instance.CreateQuery()
                                                    .Join.LeftJoin(true, ModNewsMutilService.Instance.CreateQuery().Where(x => x.ParentID == item.ID && x.TypeID == 1), o => o.ID, x => x.NewsID)
                                                    //.Join.LeftJoin(ModNewsMutilService.Instance.CreateQuery(), "ID", "NewsID")
                                                    //.Where(" Mod_NewsMutil.ParentID=" + item.ID + " AND Mod_NewsMutil.TypeID=1")
                                                    .Take(9).ToList_Cache();

                //SEO
                ViewPage.CurrentPage.PageTitle = (!string.IsNullOrEmpty(item.PageTitle) ? item.PageTitle : item.Name) + " | " + CoreMr.Reddevil.Web.HttpRequest.Host;
                ViewPage.CurrentPage.PageDescription = string.IsNullOrEmpty(item.PageDescription) ? (!string.IsNullOrEmpty(item.PageTitle) ? item.PageTitle : item.Name) : item.PageDescription;
                ViewPage.CurrentPage.PageKeywords = string.IsNullOrEmpty(item.PageKeywords) ? (!string.IsNullOrEmpty(item.PageTitle) ? item.PageTitle : item.Name) : item.PageKeywords;
                ViewPage.CurrentPage.PageURL = ViewPage.GetURL(item.MenuID, item.Url);
                ViewPage.CurrentPage.PageFile = CoreMr.Reddevil.Web.HttpRequest.Domain + Global.Utils.GetResizeFile(item.File, 4, 600, 315);

            }
            else
            {
                ViewPage.Error404();
            }
        }
    }

    public class MNewsModel
    {
        private int _page;
        public int page
        {
            get { return _page; }
            set { _page = value - 1; }
        }

        public int TotalRecord { get; set; }
        public int PageSize { get; set; }

        public string news { get; set; }
    }
}