﻿using Reddevil.Lib.Models;
using Reddevil.Lib.MVC;

namespace Reddevil.Lib.Controllers
{
    [ModuleInfo(Name = "ĐK : Menu", Code = "CMenu", IsControl = true, Order = 2)]
    public class CMenuController : Controller
    {
        [CoreMr.Reddevil.MVC.PropertyInfo("Default[PageID-true|PageSize-false],Product[PageID-true|PageSize-true],ProductMobile[PageID-true|PageSize-true]")]
        public string LayoutDefine;

        [CoreMr.Reddevil.MVC.PropertyInfo("Trang(Menu)")]
        public int PageID;

        [CoreMr.Reddevil.MVC.PropertyInfo("Số lượng")]
        public int PageSize = 9;

        public override void OnLoad()
        {

            var page = SysPageService.Instance.GetByID_Cache(PageID);
            if (page != null)
            {
                ViewBag.Data = SysPageService.Instance.GetByParent_Cache(page.ID);
                ViewBag.Page = page;
            }
            else
            {
                ViewBag.Data = SysPageService.Instance.CreateQuery()
                                        .Where(o => o.LangID == ViewPage.CurrentLang.ID && o.Activity == true && o.ShowMenuTop == true)
                                        .OrderByAsc(o => new { o.Order, o.ID })
                                        .ToList_Cache();
            }

            ViewBag.PageSize = PageSize;

        }
    }
}