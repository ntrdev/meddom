﻿using System;
using Reddevil.Lib.Global;
using Reddevil.Lib.Models;
using Reddevil.Lib.MVC;

namespace Reddevil.Lib.Controllers
{
    [ModuleInfo(Name = "ĐK : Static", Code = "CStatic", IsControl = true, Order = 4)]
    public class CStaticController : Controller
    {
        [CoreMr.Reddevil.MVC.PropertyInfo("Trang")]
        public int PageID = 3442;
        public override void OnLoad()
        {
            ViewBag.Page = SysPageService.Instance.GetByID_Cache(PageID);
        }

        
      
        public void ActionChangeState()
        {
            if (ViewPage.MobileDevice)
                Global.Cookies.SetValue("CustomDevice", "desktop", true);
            else
                Global.Cookies.SetValue("CustomDevice", "mobile", true);

            ViewPage.RefreshPage();
        }

        public void ActionLoginPOST(CStaticModel model)
        {
            ViewBag.Model = model;

            var _User = ModUserService.Instance.GetForLogin(model.LoginName, Security.Md5(model.Password));

            if (_User == null)
            {
                ViewPage.Alert("Sai tài khoản hoặc mật khẩu.");
                return;
            }

            WebLogin.SetLogin(_User.ID, true);

            ViewPage.RefreshPage();
        }

        public void ActionLogoutPOST()
        {

            WebLogin.Logout();

            ViewPage.RefreshPage();
        }

    }
    public class CStaticModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string ValidCode { get; set; }
        public string Password2 { get; set; }
        public string LoginName { get; set; }
        public int uid { get; set; }
        public string returnpath { get; set; }
    }
}