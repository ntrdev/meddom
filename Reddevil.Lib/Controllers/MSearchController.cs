﻿using System;

using Reddevil.Lib.MVC;
using Reddevil.Lib.Models;
using Reddevil.Lib.Global;

namespace Reddevil.Lib.Controllers
{
    [ModuleInfo(Name = "MO_Default: Df - Tìm kiếm", Code = "MSearch", Order = 14, Crawl = false)]
    public class MSearchController : Controller
    {
        [CoreMr.Reddevil.MVC.PropertyInfo("Số lượng")]
        public int PageSize = 20;

        [CoreMr.Reddevil.MVC.PropertyInfo("Chuyên mục", "Type|Product")]
        public int MenuID = 1;

        public void ActionIndex(MSearchModel model)
        {
            if (string.IsNullOrEmpty(model.keyword)) { ViewPage.AlertThenRedirect("Bạn phải nhập tên cần tìm.", "/"); return; }
            string _Keyword = !string.IsNullOrEmpty(model.keyword) ? Data.GetCode(model.keyword) : string.Empty;



            string where = "", whereCode = "";
            string[] ArrKeyword = model.keyword.Split(' ');
            string[] CodeArr = _Keyword.Split('-');
            for (int i = 0; ArrKeyword != null && i < ArrKeyword.Length && i < CodeArr.Length; i++)
            {
                if (ArrKeyword[i].Contains("'"))
                {
                    where += (!string.IsNullOrEmpty(where) ? " AND " : "") + "[Name] LIKE '%" + CodeArr[i] + "%'";
                    whereCode += (!string.IsNullOrEmpty(whereCode) ? " AND " : "") + "[Url] LIKE '%" + CodeArr[i] + "%'";

                }
                else
                {
                    where += (!string.IsNullOrEmpty(where) ? " OR " : "") + "[Name] COLLATE SQL_Latin1_General_CP1_CI_AS LIKE '%" + ArrKeyword[i] + "%'";
                    whereCode += (!string.IsNullOrEmpty(whereCode) ? " AND " : "") + "[Url] LIKE '%" + CodeArr[i] + "%'";
                }
            }

            var dbQuery = ModNewsService.Instance.CreateQuery()
                                    .Where(o => o.Activity == true)
                                    .Where(whereCode)
                                    .OrderByDesc(o => new { o.Name, o.ID })
                                    .Skip(PageSize * model.page)
                                    .Take(PageSize);

            //var dbQuery2 = ModNewsEducationService.Instance.CreateQuery()
            //                        .Where(o => o.Activity == true)
            //                        .Where(whereCode)
            //                        .OrderByDesc(o => new { o.Name, o.ID })
            //                        .Skip(PageSize * model.page)
            //                        .Take(PageSize);

            //var dbQuery3 = ModNewsGarnitureService.Instance.CreateQuery()
            //                     .Where(o => o.Activity == true)
            //                     .Where(whereCode)
            //                     .OrderByDesc(o => new { o.Name, o.ID })
            //                     .Skip(PageSize * model.page)
            //                     .Take(PageSize);

            //var dbQuery4 = ModNewsResearchService.Instance.CreateQuery()
            //                     .Where(o => o.Activity == true)
            //                     .Where(whereCode)
            //                     .OrderByDesc(o => new { o.Name, o.ID })
            //                     .Skip(PageSize * model.page)
            //                     .Take(PageSize);

            if (model.type == null || string.IsNullOrEmpty(model.type))
            {

              //  string atr = CoreMr.Reddevil.Web.HttpQueryString.GetValue("atr").ToString();
                string sort = CoreMr.Reddevil.Web.HttpQueryString.GetValue("sort").ToString();

                switch (sort.ToLower())
                {
                    case "new":
                        dbQuery.OrderByDesc(o => o.Published);
                        break;
                    case "view":
                        dbQuery.OrderByDesc(o => o.View);
                        break;
                    case "":
                        dbQuery.OrderByDesc(o => new { o.View, o.Order });
                        break;
                }


                //int[] arrID = CoreMr.Reddevil.Global.Array.ToInts(atr, '-');

                //for (int i = 0; !string.IsNullOrEmpty(atr) && i < arrID.Length; i++)
                //{
                //    var pid = arrID[i];

                //    if (pid < 1) continue;
                //    dbQuery.WhereIn(o => o.ID, ModPropertyService.Instance.CreateQuery().Select(o => o.ProductID).Distinct().WhereIn(o => o.MenuID, WebMenuService.Instance.GetChildIDForWeb_Cache("Product", MenuID, ViewPage.CurrentLang.ID)).Where(o => o.PropertyValueID == pid));
                //}

                var listProd = dbQuery.ToList_Cache();
                if (listProd == null)
                {
                    var listProd2 = ModNewsService.Instance.CreateQuery()
                                     .Where(o => o.Activity == true)
                                     .Where(!string.IsNullOrEmpty(model.keyword), o => o.Url.Contains(model.keyword) || o.Name.Contains(model.keyword))
                                     .OrderByDesc(o => new { o.Name, o.ID })
                                     .Skip(PageSize * model.page)
                                     .Take(PageSize).ToList_Cache();
                    listProd = listProd2;
                }

               

                model.TotalRecord = dbQuery.TotalRecord < 1 && listProd != null ? listProd.Count : dbQuery.TotalRecord;



                if (listProd != null) { RenderView("Index"); ViewBag.Data = listProd; }

                model.PageSize = PageSize;

                ViewBag.Model = model;
                ViewBag.ModelShop = model;
            }
          


            //SEO
            
            ViewPage.CurrentPage.PageURL = ViewPage.CurrentURL;
            ViewPage.CurrentPage.PageFile = CoreMr.Reddevil.Web.HttpRequest.Domain + Global.Utils.GetResizeFile(ViewPage.CurrentPage.File, 4, 630, 315);
        }
    }

    public class MSearchModel
    {
        private int _page;
        public int page
        {
            get { return _page; }
            set { _page = value - 1; }
        }

        public int TotalRecord { get; set; }
        public int TotalRecordShop { get; set; }
        public int PageSize { get; set; }
        public int State { get; set; }

        public string keyword { get; set; }
        public string type { get; set; }
    }
}
