﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using CoreMr.Reddevil.Global;
using CoreMr.Reddevil.Web;
using Reddevil.Lib.Global;
using Reddevil.Lib.Models;
using Reddevil.Lib.MVC;
using System.Threading.Tasks;
using System.Web;
using System.IO;

namespace Reddevil.Lib.Controllers
{
    [ModuleInfo(Name = "MO_Default: Df - MAjax", Code = "MAjax", Order = 10, Crawl = false)]

    public class MAjaxController : Controller
    {

        public void ActionIndex() { }
        public void ActionSecurity(SecurityModel model)
        {


            Captcha.CaseSensitive = false;
            var ci = new Captcha(175, 35);

            ViewPage.Response.Clear();
            ViewPage.Response.ContentType = "image/jpeg";

            ci.Image.Save(ViewPage.Response.OutputStream, ImageFormat.Jpeg);
            ci.Dispose();

            ViewBag.Model = model;

            ViewPage.Response.End();
        }

        #region get Someone
        public void ActionAnonymous(AnonymousModel model)
        {
            var json = new Json();

            if (model.typefunc == "4")
            {
                json.Instance.Html = string.Empty;
                var randomList = ModNewsService.Instance.CreateQuery()
                                                .Select(o => new { o.Name, o.Url, o.File })
                                                .Where(o => o.Activity == true && (o.State & 4) == 4)
                                                .WhereIn(model.cate > 0, o => o.MenuID, WebMenuService.Instance.GetChildIDForWeb_Cache("News", model.cate, 1))
                                                .ToList_Cache();
                if (randomList != null)
                {
                    randomList.Shuffle();
                    for (int i = 0; i < (randomList.Count > 10 ? 10 : randomList.Count); i++)
                    {

                        if (i == 0)
                        {
                            json.Instance.Html += @"<li class=""big"">
                                                        <figure class=""hm-reponsive"">
                                                            <img src=""" + Global.Utils.DefaultImage(randomList[i].File) + @""" title=""" + randomList[i].Name + @""">
                                                        </figure>
                                                        <div class=""blogsummary"">" + randomList[i].Name + @"</div>
                                                        <a href=""" + ViewPage.GetURL(0, randomList[i].Url) + @""" title=""" + randomList[i].Name + @""" class=""alink-all""></a>
                                                    </li>";
                        }
                        else
                        {
                            json.Instance.Html += @"<li>
                                                        <a href=""" + ViewPage.GetURL(0, randomList[i].Url) + @""" title=""" + randomList[i].Name + @""">" + randomList[i].Name + @"</a>
                                                    </li>";
                        }


                    }
                }

            }
            if (model.typefunc == "1")
            {
                json.Instance.Html = string.Empty;
                var randomList = ModNewsService.Instance.CreateQuery()
                                                .Select(o => new { o.Name, o.Url, o.File, o.View })
                                                .Where(o => o.Activity == true)
                                                //.WhereIn(model.cate > 0, o => o.MenuID, WebMenuService.Instance.GetChildIDForWeb_Cache("News", model.cate, 1))
                                                .Take(100)
                                                .OrderByDesc(o => o.View)
                                                .ToList_Cache();

                if (randomList != null)
                {
                    randomList.Shuffle();

                    for (int i = 0; i < (randomList.Count > 15 ? 15 : randomList.Count); i++)
                    {

                        if (i == 0)
                        {
                            json.Instance.Html += @"<li class=""big"">
                                                        <figure class=""hm-reponsive"">
                                                            <img src=""" + Global.Utils.DefaultImage(randomList[i].File) + @""" title=""" + randomList[i].Name + @""">
                                                        </figure>
                                                        <div class=""blogsummary"">" + randomList[i].Name + @"</div>
                                                        <a href=""" + ViewPage.GetURL(0, randomList[i].Url) + @""" title=""" + randomList[i].Name + @""" class=""alink-all""></a>
                                                    </li>";
                        }
                        else
                        {
                            json.Instance.Html += @"<li>
                                                        <a href=""" + ViewPage.GetURL(0, randomList[i].Url) + @""" title=""" + randomList[i].Name + @""">" + randomList[i].Name + @"</a>
                                                    </li>";
                        }


                    }
                }
            }


            json.Create();
        }



        public void ActionGetChild(GetChildModel model)
        {
            var json = new Json();

            var listItem = WebMenuService.Instance.CreateQuery()
                                    .Select(o => new { o.ID, o.Name })
                                    .Where(o => o.Activity == true && o.Type == "City" && o.ParentID == model.ParentID)
                                    .OrderByAsc(o => new { o.Order, o.ID })
                                    .ToList_Cache();

            json.Instance.Html += @"<option value=""0"" selected=""selected"">- Chọn quận / huyện -</option>";
            for (int i = 0; listItem != null && i < listItem.Count; i++)
            {
                json.Instance.Html += @"<option value=""" + listItem[i].ID + @""" " + (listItem[i].ID == model.SelectedID ? @"selected=""selected""" : @"") + @">" + listItem[i].Name + @"</option>";
            }
            json.Create();

            ViewBag.Model = model;

        }

        public void ActionGetData(GetSearchModel model)
        {
            var json = new Json();
            dynamic obj = null;
            // luận văn
            if (model.name == 1)
            {
                switch (model.Type)
                {
                    case "Ext":// Thông tin chung
                        obj = ModScientistService.Instance.CreateQuery().Select(o => o.Ext2).Where(o => o.Id == model.NKHID).ToSingle_Cache();
                        if (obj != null && !string.IsNullOrEmpty(obj.Ext2))
                            json.Instance.Html = Global.Utils.GetHtmlForSeo(obj.Ext2);
                        else
                            json.Instance.Html = "Không có bản ghi nào";
                        break;
                    case "TimeLine"://niên biểu cuộc đời
                        obj = ModScientistValueService.Instance.CreateQuery().Select(o => o.Value).Where(o => o.DoctorId == model.NKHID && o.Name == "NBCD").ToSingle_Cache();
                        if (obj != null && !string.IsNullOrEmpty(obj.Value))
                            json.Instance.Html = Global.Utils.GetHtmlForSeo(obj.Value);
                        else
                            json.Instance.Html = "Không có bản ghi nào";
                        break;
                    case "WorkingHistory"://quá trình làm việc
                        obj = ModScientistValueService.Instance.CreateQuery().Select(o => o.Value).Where(o => o.DoctorId == model.NKHID && o.Name == "GDDT").ToSingle_Cache();
                        if (obj != null && !string.IsNullOrEmpty(obj.Value))
                            json.Instance.Html = Global.Utils.GetHtmlForSeo(obj.Value);
                        else
                            json.Instance.Html = "Không có bản ghi nào";
                        break;
                    case "GraduationPaper":
                        obj = ModScientistService.Instance.CreateQuery().Select(o => o.GraduationPaper).Where(o => o.Id == model.NKHID).ToSingle_Cache();
                        if (obj != null && !string.IsNullOrEmpty(obj.GraduationPaper))
                            json.Instance.Html = Global.Utils.GetHtmlForSeo(obj.GraduationPaper);
                        else
                            json.Instance.Html = "Không có bản ghi nào";
                        break;
                    case "MAThesis":
                        obj = ModScientistService.Instance.CreateQuery().Select(o => o.MAThesis).Where(o => o.Id == model.NKHID).ToSingle_Cache();
                        if (obj != null && !string.IsNullOrEmpty(obj.MAThesis))
                            json.Instance.Html = Global.Utils.GetHtmlForSeo(obj.MAThesis);
                        else
                            json.Instance.Html = "Không có bản ghi nào";
                        break;
                    case "LATS":
                        obj = ModScientistValueService.Instance.CreateQuery().Select(o => o.Value).Where(o => o.DoctorId == model.NKHID && o.Name == "LATS").ToSingle_Cache();
                        if (obj != null && !string.IsNullOrEmpty(obj.Value))
                            json.Instance.Html = Global.Utils.GetHtmlForSeo(obj.Value);
                        else
                            json.Instance.Html = "Không có bản ghi nào";
                        break;
                }
            }
            else if (model.name == 2)
            {
                switch (model.Type)
                {
                    case "PublishedBook":// sách đã xuất bản
                        obj = ModScientistValueService.Instance.CreateQuery().Select(o => o.Value).Where(o => o.DoctorId == model.NKHID && o.Name == "SDXB").ToSingle_Cache();
                        if (obj != null && !string.IsNullOrEmpty(obj.Value))
                            json.Instance.Html = Global.Utils.GetHtmlForSeo(obj.Value);
                        else
                            json.Instance.Html = "Không có bản ghi nào";
                        break;
                    case "CBB":// các bài báo 
                        obj = ModScientistValueService.Instance.CreateQuery().Select(o => o.Value).Where(o => o.DoctorId == model.NKHID && o.Name == "CBB").ToList_Cache();
                        if (obj != null)
                        {
                            for (int i = 0; i < obj.Count; i++)
                                json.Instance.Html += Global.Utils.GetHtmlForSeo(obj[i].Value);
                        }
                        else
                            json.Instance.Html = "Không có bản ghi nào";
                        break;
                    case "CDTNC":// chủ đề nghiên cứu
                        obj = ModScientistValueService.Instance.CreateQuery().Select(o => o.Value).Where(o => o.DoctorId == model.NKHID && o.Name == "CDTNC").ToSingle_Cache();
                        if (obj != null && !string.IsNullOrEmpty(obj.Value))
                            json.Instance.Html = Global.Utils.GetHtmlForSeo(obj.Value);
                        else
                            json.Instance.Html = "Không có bản ghi nào";
                        break;
                }
            }
            else if (model.name == 3)
            {
                switch (model.Type)
                {
                    case "CBV":// bài viết
                        obj = ModScientistArticleService.Instance.CreateQuery().Select(o => new { o.DoctorId, o.Title, o.ImagePath, o.Sumary, o.CreatedDate, o.Id }).Where(o => o.DoctorId == model.NKHID).OrderByDesc(o => o.CreatedDate).ToList_Cache();
                        if (obj != null)
                        {
                            json.Instance.Html = @"<ul class=""list_news"">";
                            for (int i = 0; i < obj.Count; i++)
                            {
                                json.Instance.Html += @" <li class=""item_news"">
                                                            <div class=""box_left_img"">
                                                                <div class=""box_img"">
                                                                    <a href=""javascript:void(0)""  class=""clickall"" onclick=""viewacticle('" + obj[i].Id + @"')""></a>
                                                                    <img src=""" + Global.Utils.DefaultImage("http://img.meddom.org/0" + obj[i].ImagePath) + @""" height=""92"" width=""77"" alt="""">
                                                                    <div class=""time_news"">
                                                                        <span>" + Global.Utils.FormatDate2(obj[i].CreatedDate) + @"</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class=""box_text_r"">
                                                                <h4><a href=""javascript:void(0)"" onclick=""viewacticle('" + obj[i].Id + @"')"" class=""view-detail-article"" >" + obj[i].Title + @"</a></h4>
                                                                <p class=""desc"">" + obj[i].Sumary + @"</p>
                                                            </div>
                                                        </li>";
                            }
                            json.Instance.Html += "</ul>";
                        }
                        else
                            json.Instance.Html = "Không có bản ghi nào";
                        break;
                    case "DMSCPD"://danh mục sách và bài viết
                        obj = ModScientistValueService.Instance.CreateQuery().Select(o => o.Value).Where(o => o.DoctorId == model.NKHID && o.Name == "DMSCPD").ToSingle_Cache();
                        if (obj != null && !string.IsNullOrEmpty(obj.Value))
                            json.Instance.Html = Global.Utils.GetHtmlForSeo(obj.Value);
                        else
                            json.Instance.Html = "Không có bản ghi nào";
                        break;
                    case "DMTL"://danh mục sách và bài viết tại cpd
                        obj = ModScientistValueService.Instance.CreateQuery().Select(o => o.Value).Where(o => o.DoctorId == model.NKHID && o.Name == "DMTL").ToSingle_Cache();
                        if (obj != null && !string.IsNullOrEmpty(obj.Value))
                            json.Instance.Html = Global.Utils.GetHtmlForSeo(obj.Value);
                        else
                            json.Instance.Html = "Không có bản ghi nào";
                        break;
                    case "CTTK":// Các thư tịch khác
                        obj = ModScientistValueService.Instance.CreateQuery().Select(o => o.Value).Where(o => o.DoctorId == model.NKHID && o.Name == "CTTK").ToSingle_Cache();
                        if (obj != null && !string.IsNullOrEmpty(obj.Value))
                            json.Instance.Html = Global.Utils.GetHtmlForSeo(obj.Value);
                        else
                            json.Instance.Html = "Không có bản ghi nào";
                        break;
                }
            }
            else if (model.name == 4)
            {
                switch (model.Type)
                {
                    case "family":// album ảnh
                        var Album = ModScientistAlbumService.Instance.CreateQuery().Where(o => o.DoctorId == model.NKHID).ToList_Cache();
                        if (Album != null)
                        {
                            json.Instance.Html = @" <div role=""tabpanel"" class=""tab-pane fade in active show"" id=""img_nhakhoahoc"">
                                                        <div class=""itembox_news"">
                                                            <ul class=""list_item_o list_3_item"">";
                            for (int i = 0; i < Album.Count; i++)
                            {
                                json.Instance.Html += @"<li class=""item_news_o"">
                                                            <div class=""box_img"">
                                                                <a href=""" + Global.Utils.DefaultImage("http://img.meddom.org/0" + Album[i].PathOrigin) + @""" data-fancybox=""preview"" class=""fancybox get-img"" data-img=""" + Global.Utils.DefaultImage("http://img.meddom.org/0" + Album[i].PathOrigin) + @""" data-bigimg=""" + Global.Utils.DefaultImage("http://img.meddom.org/0" + Album[i].PathOrigin) + @""" data-album=""" + Album[i].Id + @""">
                                                                    <img src=""" + Global.Utils.DefaultImage("http://img.meddom.org/0" + Album[i].PathOrigin) + @""" alt=""" + Album[i].Description + @""" title=""" + Album[i].Description + @"""></a>
                                                            </div>
                                                            <div class=""box_info"">
                                                                <h4><a href=""javascript:void(0)"" class=""get-img"">" + Album[i].Name + @"</a></h4>
                                                            </div>
                                                        </li>";
                            }
                            json.Instance.Html += @"
                                                            </ul>
                                                        </div>
                                                    </div>";
                        }
                        else json.Instance.Html = "Không có bản ghi nào";
                        break;
                    case "study"://báo viết về nkh
                        //obj = ModHocTapCongTacService.Instance.CreateQuery().Where(o => o.NKHID == model.NKHID).ToList_Cache();
                        //if (obj != null)
                        //{
                        //    json.Instance.Html = @"<ul class=""box_text_detail_hoso"">";
                        //    for (int i = 0; i < obj.Count; i++)
                        //    {
                        //        json.Instance.Html += @"<li class=""flex3""><span>Thời gian: <b>" + obj[i].ThoiGian + @"</b></span><br/><span>Hoạt động: <b>" + obj[i].HoatDong + @"</b></span><br/></li>";
                        //    }
                        //    json.Instance.Html += "</ul>";

                        //}else 
                        json.Instance.Html = "Không có bản ghi nào";
                        break;
                    case "hoatdong"://hoạt động
                                    //obj = ModHocTapCongTacService.Instance.CreateQuery().Where(o => o.NKHID == model.NKHID).ToList_Cache();
                                    //if (obj != null)
                                    //{
                                    //    json.Instance.Html = @"<ul class=""box_text_detail_hoso"">";
                                    //    for (int i = 0; i < obj.Count; i++)
                                    //    {
                                    //        json.Instance.Html += @"<li class=""flex3""><span>Thời gian: <b>" + obj[i].ThoiGian + @"</b></span><br/><span>Hoạt động: <b>" + obj[i].HoatDong + @"</b></span><br/></li>";
                                    //    }
                                    //    json.Instance.Html += "</ul>";

                        //}
                        //else 
                        json.Instance.Html = "Không có bản ghi nào";
                        break;
                    case "hienvat"://hiện vật
                        //obj = ModHoSoHienVatService.Instance.CreateQuery().Where(o => o.NKHID == model.NKHID).ToList_Cache();
                        //if (obj != null)
                        //{
                        //    json.Instance.Html = @"<ul class=""box_text_detail_hoso"">";
                        //    for (int i = 0; i < obj.Count; i++)
                        //    {
                        //        json.Instance.Html += @"<li class=""flex3""><span>Tên phổ thông: <b>" + obj[i].TenPhoThong + @"</b></span><br/><span>Sổ đăng ký: <b>" + obj[i].SoHoSo + @"</b></span><br/><span>Ngày sưu tầm: <b>" + Global.Utils.FormatDate2(obj[i].NgaySuuTam) + @"</b></span><br/><span>Địa chỉ sưu tầm: <b>" + obj[i].DiaChiNoiSuuTam + @"</b></span></li>";
                        //    }
                        //    json.Instance.Html += "</ul>";

                        //}
                        //else 
                        json.Instance.Html = "Không có bản ghi nào";
                        break;
                }
            }
            else if (model.name == 5)
            {
                switch (model.Type)
                {
                    case "Video":// sách đã xuất bản
                                 //obj = ModTuLieuPhimService.Instance.CreateQuery().Where(o => o.NKHID == model.NKHID).ToList_Cache();
                                 //if (obj != null)
                                 //{
                                 //    json.Instance.Html = @"<ul class=""box_text_detail_hoso"">";
                                 //    for (int i = 0; i < obj.Count; i++)
                                 //    {
                                 //        json.Instance.Html += @"<li class=""flex3""><span>Tiêu đề: <b>" + obj[i].TieuDe + @"</b></span><br/><span>Người quay, tác giả: <b>" + obj[i].TacGiaNguoiQuay + @"</b></span><br/><span>Nơi quay: <b>" + obj[i].NoiSXDCQuay + @"</b></span><br/><span>Năm sản xuất: <b>" + obj[i].NamSanXuat + @"</b></span></li>";
                                 //    }
                                 //    json.Instance.Html += "</ul>";
                                 //}
                                 //else
                        json.Instance.Html = "Không có bản ghi nào";
                        break;

                }
            }
            else if (model.name == 6)
            {
                switch (model.Type)
                {
                    case "TLPV":// sách đã xuất bản
                                //obj = ModTuLieuPhimService.Instance.CreateQuery().Where(o => o.NKHID == model.NKHID).ToList_Cache();
                                //if (obj != null)
                                //{
                                //    json.Instance.Html = @"<ul class=""box_text_detail_hoso"">";
                                //    for (int i = 0; i < obj.Count; i++)
                                //    {
                                //        json.Instance.Html += @"<li class=""flex3""><span>Tiêu đề: <b>" + obj[i].TieuDe + @"</b></span><br/><span>Người quay, tác giả: <b>" + obj[i].TacGiaNguoiQuay + @"</b></span><br/><span>Nơi quay: <b>" + obj[i].NoiSXDCQuay + @"</b></span><br/><span>Năm sản xuất: <b>" + obj[i].NamSanXuat + @"</b></span></li>";
                                //    }
                                //    json.Instance.Html += "</ul>";
                                //}
                                //else 
                        json.Instance.Html = "Không có bản ghi nào";
                        break;

                }
            }
            json.Create();

        }

        public void ActionViewsActicle(GetSearchModel model)
        {
            var json = new Json();

            var _Article = ModScientistArticleService.Instance.CreateQuery().Where(o => o.Id == model.NKHID).ToSingle_Cache();
            if (_Article != null)
            {
                json.Instance.Html = @"<div class=""box_title_post"">
                                        <h4>" + _Article.Title + @"</h4>
                                        <div class=""time_news"">
                                            <i class=""fa fa-clock-o"" aria-hidden=""true""></i>
                                            <span>" + Global.Utils.FormatDateTime(_Article.CreatedDate) + @"</span>
                                        </div>
                                    </div>";
                json.Instance.Html += Global.Utils.GetHtmlForSeo(_Article.Contents);
            }

            json.Create();

            ViewBag.Model = model;

        }
        //public void ActionGetData(GetSearchModel model)
        //{
        //    var json = new Json();
        //    dynamic obj = null;
        //    // luận văn
        //    if (model.name == 1)
        //    {
        //        switch (model.Type.ToLower())
        //        {
        //            case "luận án":// luận văn
        //                obj = ModLVLAService.Instance.CreateQuery().Where(o => o.NKHID == model.NKHID && o.LoaiHinh.Contains(model.Type)).ToList_Cache();
        //                if (obj != null)
        //                {
        //                    json.Instance.Html = @"<ul class=""box_text_detail_hoso"">";
        //                    for (int i = 0; i < obj.Count; i++)
        //                    {
        //                        json.Instance.Html += @"<li class=""flex3""><span>Đề tài: <b>" + obj[i].TenDeTai + @"</b></span><br/><span>Người hướng dẫn: <b>" + obj[i].NguoiHuongDan + @"</b></span><br/><span>Nơi bảo vệ: <b>" + obj[i].NoiBaoVe + @"</b></span><br/><span>Năm bảo vệ: <b>" + obj[i].NamBaoVe + @"</b></span></li>";
        //                    }
        //                    json.Instance.Html += "</ul>";
        //                }
        //                else json.Instance.Html = "Không có bản ghi nào";
        //                break;
        //            case "khóa luận":
        //                obj = ModLVLAService.Instance.CreateQuery().Where(o => o.NKHID == model.NKHID && o.LoaiHinh.Contains(model.Type)).ToList_Cache();
        //                if (obj != null)
        //                {
        //                    json.Instance.Html = @"<ul class=""box_text_detail_hoso"">";
        //                    for (int i = 0; i < obj.Count; i++)
        //                    {
        //                        json.Instance.Html += @"<li class=""flex3""><span>Đề tài: <b>" + obj[i].TenDeTai + @"</b></span><br/><span>Người hướng dẫn: <b>" + obj[i].NguoiHuongDan + @"</b></span><br/><span>Nơi bảo vệ: <b>" + obj[i].NoiBaoVe + @"</b></span><br/><span>Năm bảo vệ: <b>" + obj[i].NamBaoVe + @"</b></span></li>";
        //                    }
        //                    json.Instance.Html += "</ul>";

        //                }
        //                else json.Instance.Html = "Không có bản ghi nào";
        //                break;
        //            case "luận án tiến sĩ":
        //                obj = ModLVLAService.Instance.CreateQuery().Where(o => o.NKHID == model.NKHID && o.LoaiHinh.Contains(model.Type)).ToList_Cache();
        //                if (obj != null)
        //                {
        //                    json.Instance.Html = @"<ul class=""box_text_detail_hoso"">";
        //                    for (int i = 0; i < obj.Count; i++)
        //                    {
        //                        json.Instance.Html += @"<li class=""flex3""><span>Đề tài: <b>" + obj[i].TenDeTai + @"</b></span><br/><span>Người hướng dẫn: <b>" + obj[i].NguoiHuongDan + @"</b></span><br/><span>Nơi bảo vệ: <b>" + obj[i].NoiBaoVe + @"</b></span><br/><span>Năm bảo vệ: <b>" + obj[i].NamBaoVe + @"</b></span></li>";
        //                    }
        //                    json.Instance.Html += "</ul>";

        //                }
        //                else json.Instance.Html = "Không có bản ghi nào";
        //                break;
        //            case "luận văn":
        //                obj = ModLVLAService.Instance.CreateQuery().Where(o => o.NKHID == model.NKHID && o.LoaiHinh.Contains(model.Type)).ToList_Cache();
        //                if (obj != null)
        //                {
        //                    json.Instance.Html = @"<ul class=""box_text_detail_hoso"">";
        //                    for (int i = 0; i < obj.Count; i++)
        //                    {
        //                        json.Instance.Html += @"<li class=""flex3""><span>Đề tài: <b>" + obj[i].TenDeTai + @"</b></span><br/><span>Người hướng dẫn: <b>" + obj[i].NguoiHuongDan + @"</b></span><br/><span>Nơi bảo vệ: <b>" + obj[i].NoiBaoVe + @"</b></span><br/><span>Năm bảo vệ: <b>" + obj[i].NamBaoVe + @"</b></span></li>";
        //                    }
        //                    json.Instance.Html += "</ul>";

        //                }
        //                else json.Instance.Html = "Không có bản ghi nào";
        //                break;
        //        }
        //    }
        //    else if (model.name == 2)
        //    {
        //        switch (model.Type.ToLower())
        //        {
        //            case "sách":// sách đã xuất bản
        //                obj = ModBookPublishedService.Instance.CreateQuery().Where(o => o.NKHID == model.NKHID).ToList_Cache();
        //                if (obj != null)
        //                {
        //                    json.Instance.Html = @"<ul class=""box_text_detail_hoso"">";
        //                    for (int i = 0; i < obj.Count; i++)
        //                    {
        //                        json.Instance.Html += @"<li class=""flex3""><span>Tiêu đề sách: <b>" + obj[i].TieuDeSach + @"</b></span><br/><span>Tác giả: <b>" + obj[i].TacGia + @"</b></span><br/><span>Năm Xuất Bản: <b>" + obj[i].NamXuatBan + @"</b></span></li>";
        //                    }
        //                    json.Instance.Html += "</ul>";
        //                }
        //                else json.Instance.Html = "Không có bản ghi nào";
        //                break;
        //            case "báo"://báo viết về nkh
        //                obj = ModBaiBaoNghienCuuService.Instance.CreateQuery().Where(o => o.NKHID == model.NKHID).ToList_Cache();
        //                if (obj != null)
        //                {
        //                    json.Instance.Html = @"<ul class=""box_text_detail_hoso"">";
        //                    for (int i = 0; i < obj.Count; i++)
        //                    {
        //                        json.Instance.Html += @"<li class=""flex3""><span>Tiêu đề: <b>" + obj[i].TieuDeBaiViet + @"</b></span><br/><span>Tác giả: <b>" + obj[i].TacGia + @"</b></span><br/><span>Tên tạp chí: <b>" + obj[i].TenTapChi + @"</b></span><br/><span>Năm Xuất Bản: <b>" + obj[i].NamXuatBan + @"</b></span></li>";
        //                    }
        //                    json.Instance.Html += "</ul>";

        //                }
        //                else json.Instance.Html = "Không có bản ghi nào";
        //                break;
        //            case "đề tài"://đề tài khoa học
        //                obj = ModDeTaiKhoaHocService.Instance.CreateQuery().Where(o => o.NKHID == model.NKHID).ToList_Cache();
        //                if (obj != null)
        //                {
        //                    json.Instance.Html = @"<ul class=""box_text_detail_hoso"">";
        //                    for (int i = 0; i < obj.Count; i++)
        //                    {
        //                        json.Instance.Html += @"<li class=""flex3""><span>Đề tài: <b>" + obj[i].TenDeTai + @"</b></span><br/><span>Người thực hiện: <b>" + obj[i].NguoiThucHien + @"</b></span><br/><span>Mã số: <b>" + obj[i].MaSo + @"</b></span><br/><span>Năm Thực Hiện Từ: <b>" + obj[i].NamThucHienTu + @"</b></span><br/><span>Năm Thực Hiện Đến: <b>" + obj[i].NamThucHienDen + @"</b></span></li>";
        //                    }
        //                    json.Instance.Html += "</ul>";

        //                }
        //                else json.Instance.Html = "Không có bản ghi nào";
        //                break;
        //        }
        //    }
        //    else if (model.name == 3)
        //    {
        //        switch (model.Type.ToLower())
        //        {
        //            case "bài viết":// sách đã xuất bản
        //                obj = ModBaiBaoNghienCuuService.Instance.CreateQuery().Where(o => o.NKHID == model.NKHID).ToList_Cache();
        //                if (obj != null)
        //                {
        //                    json.Instance.Html = @"<ul class=""box_text_detail_hoso"">";
        //                    for (int i = 0; i < obj.Count; i++)
        //                    {
        //                        json.Instance.Html += @"<li class=""flex3""><span>Tiêu đề sách: <b>" + obj[i].TieuDeSach + @"</b></span><br/><span>Tác giả: <b>" + obj[i].TacGia + @"</b></span><br/><span>Năm Xuất Bản: <b>" + obj[i].NamXuatBan + @"</b></span></li>";
        //                    }
        //                    json.Instance.Html += "</ul>";
        //                }
        //                else json.Instance.Html = "Không có bản ghi nào";
        //                break;
        //            case "danh mục"://báo viết về nkh
        //                obj = ModDanhMucSachBaiVietService.Instance.CreateQuery().Where(o => o.NKHID == model.NKHID).ToList_Cache();
        //                if (obj != null)
        //                {
        //                    json.Instance.Html = @"<ul class=""box_text_detail_hoso"">";
        //                    for (int i = 0; i < obj.Count; i++)
        //                    {
        //                        json.Instance.Html += @"<li class=""flex3""><span>Tiêu đề: <b>" + obj[i].TieuDeSach + @"</b></span><br/><span>Tóm tắt nội dung: <b>" + obj[i].TomTatNoiDung + @"</b></span><br/><span>Tác giả: <b>" + obj[i].TacGia + @"</b></span></li>";
        //                    }
        //                    json.Instance.Html += "</ul>";

        //                }
        //                else json.Instance.Html = "Không có bản ghi nào";
        //                break;
        //            case "cpd"://đề tài khoa học
        //                obj = ModDanhMucTaiLieuCPDService.Instance.CreateQuery().Where(o => o.NKHID == model.NKHID).ToList_Cache();
        //                if (obj != null)
        //                {
        //                    json.Instance.Html = @"<ul class=""box_text_detail_hoso"">";
        //                    for (int i = 0; i < obj.Count; i++)
        //                    {
        //                        json.Instance.Html += @"<li class=""flex3""><span>Tên phổ thông: <b>" + obj[i].TenPhoThong + @"</b></span><br/><span>Sổ đăng ký: <b>" + obj[i].SoDangKy + @"</b></span><br/><span>Tóm tắt Nội dung: <b>" + obj[i].TomTatNoiDung + @"</b></span><br/><span>Tác giả: <b>" + obj[i].TacGia + @"</b></span></li>";
        //                    }
        //                    json.Instance.Html += "</ul>";

        //                }
        //                else json.Instance.Html = "Không có bản ghi nào";
        //                break;
        //        }
        //    }
        //    else if (model.name == 4)
        //    {
        //        switch (model.Type.ToLower())
        //        {
        //            case "gia đình":// sách đã xuất bản
        //                obj = ModQuanHeGiaDinhService.Instance.CreateQuery().Where(o => o.NKHID == model.NKHID).ToList_Cache();
        //                if (obj != null)
        //                {
        //                    json.Instance.Html = @"<ul class=""box_text_detail_hoso"">";
        //                    for (int i = 0; i < obj.Count; i++)
        //                    {
        //                        json.Instance.Html += @"<li class=""flex3""><span>Họ tên: <b>" + obj[i].HoTen + @"</b></span><br/><span>Năm sinh: <b>" + obj[i].NamSinh + @"</b></span><br/><span>Nghề nghiệp: <b>" + obj[i].NgheNghiep + @"</b></span><br/><span>Loại quan hệ: <b>" + obj[i].LoaiQuanHe + @"</b></span></li>";
        //                    }
        //                    json.Instance.Html += "</ul>";
        //                }
        //                else json.Instance.Html = "Không có bản ghi nào";
        //                break;
        //            case "đi học"://báo viết về nkh
        //                //obj = ModHocTapCongTacService.Instance.CreateQuery().Where(o => o.NKHID == model.NKHID).ToList_Cache();
        //                //if (obj != null)
        //                //{
        //                //    json.Instance.Html = @"<ul class=""box_text_detail_hoso"">";
        //                //    for (int i = 0; i < obj.Count; i++)
        //                //    {
        //                //        json.Instance.Html += @"<li class=""flex3""><span>Thời gian: <b>" + obj[i].ThoiGian + @"</b></span><br/><span>Hoạt động: <b>" + obj[i].HoatDong + @"</b></span><br/></li>";
        //                //    }
        //                //    json.Instance.Html += "</ul>";

        //                //}else 
        //                json.Instance.Html = "Không có bản ghi nào";
        //                break;
        //            case "hoạt động"://hoạt động
        //                obj = ModHocTapCongTacService.Instance.CreateQuery().Where(o => o.NKHID == model.NKHID).ToList_Cache();
        //                if (obj != null)
        //                {
        //                    json.Instance.Html = @"<ul class=""box_text_detail_hoso"">";
        //                    for (int i = 0; i < obj.Count; i++)
        //                    {
        //                        json.Instance.Html += @"<li class=""flex3""><span>Thời gian: <b>" + obj[i].ThoiGian + @"</b></span><br/><span>Hoạt động: <b>" + obj[i].HoatDong + @"</b></span><br/></li>";
        //                    }
        //                    json.Instance.Html += "</ul>";

        //                }
        //                else json.Instance.Html = "Không có bản ghi nào";
        //                break;
        //            case "hiện vật"://hiện vật
        //                obj = ModHoSoHienVatService.Instance.CreateQuery().Where(o => o.NKHID == model.NKHID).ToList_Cache();
        //                if (obj != null)
        //                {
        //                    json.Instance.Html = @"<ul class=""box_text_detail_hoso"">";
        //                    for (int i = 0; i < obj.Count; i++)
        //                    {
        //                        json.Instance.Html += @"<li class=""flex3""><span>Tên phổ thông: <b>" + obj[i].TenPhoThong + @"</b></span><br/><span>Sổ đăng ký: <b>" + obj[i].SoHoSo + @"</b></span><br/><span>Ngày sưu tầm: <b>" + Global.Utils.FormatDate2(obj[i].NgaySuuTam) + @"</b></span><br/><span>Địa chỉ sưu tầm: <b>" + obj[i].DiaChiNoiSuuTam + @"</b></span></li>";
        //                    }
        //                    json.Instance.Html += "</ul>";

        //                }
        //                else json.Instance.Html = "Không có bản ghi nào";
        //                break;
        //        }
        //    }
        //    else if (model.name == 5)
        //    {
        //        switch (model.Type.ToLower())
        //        {
        //            case "phim":// sách đã xuất bản
        //                obj = ModTuLieuPhimService.Instance.CreateQuery().Where(o => o.NKHID == model.NKHID).ToList_Cache();
        //                if (obj != null)
        //                {
        //                    json.Instance.Html = @"<ul class=""box_text_detail_hoso"">";
        //                    for (int i = 0; i < obj.Count; i++)
        //                    {
        //                        json.Instance.Html += @"<li class=""flex3""><span>Tiêu đề: <b>" + obj[i].TieuDe + @"</b></span><br/><span>Người quay, tác giả: <b>" + obj[i].TacGiaNguoiQuay + @"</b></span><br/><span>Nơi quay: <b>" + obj[i].NoiSXDCQuay + @"</b></span><br/><span>Năm sản xuất: <b>" + obj[i].NamSanXuat + @"</b></span></li>";
        //                    }
        //                    json.Instance.Html += "</ul>";
        //                }
        //                else json.Instance.Html = "Không có bản ghi nào";
        //                break;

        //        }
        //    }
        //    json.Create();

        //}



        #endregion

        #region up download
        public void ActionPlusDownload(string url)
        {
            var response = System.Web.HttpContext.Current.Response;
            if (!string.IsNullOrEmpty(url))
            {
                new Task(() =>
                {
                    var item = ModLibraryService.Instance.CreateQuery().Where(o => o.Url == url).ToSingle();
                    if (item != null) item.UpDownload();

                }).Start();
            }
            response.Clear();
            response.ContentType = "application/json; charset=utf-8";
            response.Write("");
            response.End();


        }

        #endregion

        #region search All 
        public void ActionSearch()
        {
            var response = HttpContext.Current;
            var keyCacheDistribute = "Mod_SearchAll_" + DateTime.Now.Month;
            var objdist = Cache.GetValue(keyCacheDistribute);
            if (objdist != null)
            {
                var json = JsonConvert.SerializeObject(new ResultModel
                {
                    Error = 0,
                    Message = "",
                    DataInfo = objdist
                }, Formatting.Indented);

                response.Response.Clear();
                response.Response.ContentType = "application/json; charset=utf-8";
                response.Response.Write(json);
                response.Response.End();
            }
            else
            {
                var listService = ModSearchAllService.Instance.StoreProcedure("Search_AllPage").ToList_Cache();
                if (listService != null) Cache.SetValue(keyCacheDistribute, listService, 1600);


                var json = JsonConvert.SerializeObject(new ResultModel
                {
                    Error = 0,
                    Message = "",
                    DataInfo = listService
                }, Formatting.Indented);

                response.Response.Clear();
                response.Response.ContentType = "application/json; charset=utf-8";
                response.Response.Write(json);

                response.Response.End();
            }


        }
        #endregion

        
    }


    public class UpdateInfo
    {
        public string tenNKH { get; set; }
        public string fileName { get; set; }
        public string imageBase64 { get; set; }
        public string mamauBP { get; set; }
        public int nkhID { get; set; }
        public string userID { get; set; }
        public string typeObj { get; set; }

    }
    public class SecurityModel
    {
        public string Code { get; set; }
    }
    public class ResultModel
    {
        public int Error { get; set; }
        public string Message { get; set; }
        public object DataInfo { get; set; }
    }

    public class GetChildModel
    {
        public int ParentID { get; set; }
        public int SelectedID { get; set; }
    }

    public class GetSearchModel
    {
        public int NKHID { get; set; }
        public string Type { get; set; }
        public int name { get; set; }
    }
    public class AnonymousModel
    {
        public bool device { get; set; }
        public int cate { get; set; }
        public int pageid { get; set; }
        public int pagedex { get; set; }
        public string typefunc { get; set; }
        public bool prod { get; set; }
    }

}
