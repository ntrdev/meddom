﻿using Reddevil.Lib.Global;
using Reddevil.Lib.Models;
using Reddevil.Lib.MVC;

namespace Reddevil.Lib.Controllers
{
    [ModuleInfo(Name = "MO_Default: Df - Tin Giáo Dục Di Sản", Code = "MNewsEducation", Order = 10, Crawl = true)]
    public class MNewsEducationController : Controller
    {
        [CoreMr.Reddevil.MVC.PropertyInfo("Danh mục", "Type|NewsEducation")]
        public int MenuID;

        [CoreMr.Reddevil.MVC.PropertyInfo("Dạng hiển thị", "Danh sách|Index,Bộ sưu tập|HoatDong,CT|Chuongtrinh")]
        public string Layout = "Index";

        [CoreMr.Reddevil.MVC.PropertyInfo("Số lượng hiển thị")]
        public int PageSize = 20;

        public void ActionIndex(MNewsEducationModel model)
        {
            // Utils.UpdateStatistic();

            if (ViewPage.CurrentPage.MenuID > 0)
                MenuID = ViewPage.CurrentPage.MenuID;

            string _Keyword = !string.IsNullOrEmpty(model.news) ? Data.GetCode(model.news) : "";
            PageSize = model.PageSize > 0 ? model.PageSize : PageSize;
            var dbQuery = ModNewsEducationService.Instance.CreateQuery()
                                    .Where(o => o.Activity == true)
                                    .Where(!string.IsNullOrEmpty(model.news), o => o.Url.Contains(_Keyword) || o.Name.Contains(model.news))
                                    .WhereIn(MenuID > 0, o => o.MenuID, WebMenuService.Instance.GetChildIDForWeb_Cache("NewsEducation", MenuID, ViewPage.CurrentLang.ID))
                                    .Skip(PageSize * model.page)
                                    .Take(PageSize)
                                    .OrderByDesc(o => new { o.ID, o.Order });

            ViewBag.Data = dbQuery.ToList_Cache();
            model.TotalRecord = dbQuery.TotalRecord;
            model.PageSize = PageSize;
            ViewBag.Model = model;
            if (Layout == "HoatDong")
            {
                ViewBag.ListPage = SysPageService.Instance.GetByParent_Cache(ViewPage.CurrentPage.ID);
            }
            RenderView(Layout);
            //SEO
            ViewPage.CurrentPage.PageURL = ViewPage.CurrentURL;
            ViewPage.CurrentPage.PageFile = CoreMr.Reddevil.Web.HttpRequest.Domain + Global.Utils.GetResizeFile(ViewPage.CurrentPage.File, 4, 630, 315);
        }
        private ModNewsEducationEntity item;
        public void ActionDetail(string endCode)
        {
            if (ViewPage.CurrentPage.MenuID > 0)
                MenuID = ViewPage.CurrentPage.MenuID;

            if (Cookies.Exist("CP.UserID"))
            {
                item = ModNewsEducationService.Instance.CreateQuery()
                                         .Where(o => o.Url == endCode)
                                         .ToSingle_Cache();
            }
            else
            {
                item = ModNewsEducationService.Instance.CreateQuery()
                                   .Where(o => o.Activity == true && o.Url == endCode)
                                   .ToSingle_Cache();
            }
            if (item != null)
            {
                // Utils.UpdateStatistic();

                //up view
                item.UpView();

                ViewBag.Other = ModNewsEducationService.Instance.CreateQuery()
                                            .Where(o => o.Activity == true && o.ID != item.ID)
                                            .WhereIn(MenuID > 0, o => o.MenuID, WebMenuService.Instance.GetChildIDForWeb_Cache("NewsEducation", MenuID, ViewPage.CurrentLang.ID))
                                            .OrderByDesc(o => new { o.Order, o.ID })
                                            .Take(9)
                                            .ToList_Cache();

                ViewPage.ViewBag.Data = ViewBag.Data = item;
                ViewBag.Lienquan = ModNewsEducationService.Instance.CreateQuery()
                                                 .Join.LeftJoin(true, ModNewsMutilService.Instance.CreateQuery().Where(x => x.ParentID == item.ID && x.TypeID == 2), o => o.ID, x => x.NewsID)
                                                //.Join.LeftJoin(ModNewsMutilService.Instance.CreateQuery(), "ID", "NewsID")
                                                //.Where(" Mod_NewsMutil.ParentID=" + item.ID+ " AND Mod_NewsMutil.TypeID=2")
                                                .Take(9)
                                                .ToList_Cache();

                //SEO
                ViewPage.CurrentPage.PageTitle = (!string.IsNullOrEmpty(item.PageTitle) ? item.PageTitle : item.Name) + " | " + CoreMr.Reddevil.Web.HttpRequest.Host;
                ViewPage.CurrentPage.PageDescription = string.IsNullOrEmpty(item.PageDescription) ? (!string.IsNullOrEmpty(item.PageTitle) ? item.PageTitle : item.Name) : item.PageDescription;
                ViewPage.CurrentPage.PageKeywords = string.IsNullOrEmpty(item.PageKeywords) ? (!string.IsNullOrEmpty(item.PageTitle) ? item.PageTitle : item.Name) : item.PageKeywords;
                ViewPage.CurrentPage.PageURL = ViewPage.GetURL(item.MenuID, item.Url);
                ViewPage.CurrentPage.PageFile = CoreMr.Reddevil.Web.HttpRequest.Domain + Global.Utils.GetResizeFile(item.File, 4, 600, 315);

            }
            else
            {
                ViewPage.Error404();
            }
        }
    }

    public class MNewsEducationModel
    {
        private int _page;
        public int page
        {
            get { return _page; }
            set { _page = value - 1; }
        }

        public int TotalRecord { get; set; }
        public int PageSize { get; set; }
        public string news { get; set; }
    }
}