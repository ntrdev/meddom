﻿using Reddevil.Lib.Global;
using Reddevil.Lib.MVC;

namespace Reddevil.Lib.Controllers
{
    [ModuleInfo(Name = "ĐK : Text", Code = "CText", IsControl = true, Order = 99)]
    public class CTextController : Controller
    {
        [CoreMr.Reddevil.MVC.PropertyInfo("Văn bản / Html")]
        public string Text;

        [CoreMr.Reddevil.MVC.PropertyInfo("Tiêu đề")]
        public string Title = string.Empty;

        public override void OnLoad()
        {
            ViewBag.Text = !string.IsNullOrEmpty(Text) ? Data.Base64Decode(Text) : string.Empty;

            ViewBag.Title = Title;
        }
    }
}