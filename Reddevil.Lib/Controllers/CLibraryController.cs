﻿using Reddevil.Lib.MVC;
using Reddevil.Lib.Models;
using Reddevil.Lib.Global;

namespace Reddevil.Lib.Controllers
{
    [ModuleInfo(Name = "ĐK : Thư viện số", Code = "CLibrary", IsControl = true, Order = 2)]
    public class CLibraryController : Controller
    {

        //[CoreMr.Reddevil.MVC.PropertyInfo("Chuyên mục", "Type|Library")]
        //public int MenuID;

        //[CoreMr.Reddevil.MVC.PropertyInfo("Trang")]
        //public int PageID;

        //[CoreMr.Reddevil.MVC.PropertyInfo("Số lượng")]
        //public int PageSize = 5;


        public override void OnLoad()
        {
            ViewBag.Data = ModLibraryService.Instance.StoreProcedure("Meddom_GetTin3DanhMuc")
                                             .ToList();
        }
    }
}