﻿using Reddevil.Lib.Global;
using Reddevil.Lib.Models;
using Reddevil.Lib.MVC;
using System;
using System.Collections.Generic;

namespace Reddevil.Lib.Controllers
{
    [ModuleInfo(Name = "MO: Sitemap", Code = "MSitemap", Order = 50, Crawl = false)]
    public class MSitemapController : Controller
    {
        private List<string> listSitemap = null;

        public void ActionIndex()
        {
            listSitemap = new List<string>();

            listSitemap.Add("sitemap");
            listSitemap.Add("sitemap-misc");
            listSitemap.Add("sitemap-category");
            listSitemap.Add("sitemap-product");
            listSitemap.Add("sitemap-misc");
            listSitemap.Add("sitemap-news");

            string endCode = ViewPage.CurrentVQS.EndCode.ToLower();

            if (endCode.Equals("sitemap-misc", StringComparison.Ordinal))
            {
                RedirectToAction("Misc");
                return;
            }
            else if (endCode.Equals("sitemap-category", StringComparison.Ordinal))
            {
                RedirectToAction("Category");
                return;
            }
            //else if (endCode.StartsWith("sitemap-product", StringComparison.Ordinal))
            //{
            //    RedirectToAction("Product");
            //    return;
            //}
            else if (endCode.StartsWith("sitemap-news", StringComparison.Ordinal))
            {
                RedirectToAction("News");
                return;
            }

            CoreMr.Reddevil.Global.Sitemap._siteMapList = null;

            CoreMr.Reddevil.Global.Sitemap.AddLocation(CoreMr.Reddevil.Web.HttpRequest.Domain + "/sitemap-misc.xml", DateTime.Now);
            CoreMr.Reddevil.Global.Sitemap.AddLocation(CoreMr.Reddevil.Web.HttpRequest.Domain + "/sitemap-category.xml", DateTime.Now);

            ////product
            //var listProduct = ModProductService.Instance.CreateQuery()
            //                                .Select(o => o.Updated)
            //                                .Where(o => o.Activity == true)
            //                                .OrderByDesc(o => o.Updated)
            //                                .ToList_Cache();

            //Dictionary<string, DateTime> dicSmProduct = new Dictionary<string, DateTime>();
            //for (int i = 0; listProduct != null && i < listProduct.Count; i++)
            //{
            //    var smName = "sitemap-product-" + $"{listProduct[i].Updated:yyyy-MM}";
            //    if (!dicSmProduct.ContainsKey(smName)) dicSmProduct[smName] = listProduct[i].Updated;
            //}

            //foreach (var o in dicSmProduct)
            //{
            //    CoreMr.Reddevil.Global.Sitemap.AddLocation(CoreMr.Reddevil.Web.HttpRequest.Domain + "/" + o.Key + ".xml", o.Value);

            //    listSitemap.Add(o.Key);
            //}

            //news
            var listNews = ModNewsService.Instance.CreateQuery()
                                            .Select(o => o.Updated)
                                            .Where(o => o.Activity == true)
                                            .OrderByDesc(o => o.Updated)
                                            .ToList_Cache();

            Dictionary<string, DateTime> dicSmNews = new Dictionary<string, DateTime>();
            for (int i = 0; listNews != null && i < listNews.Count; i++)
            {
                if (listNews[i].Updated <= DateTime.MinValue) continue;
                var smName = "sitemap-news-" + $"{listNews[i].Updated:yyyy-MM}";
                if (!dicSmNews.ContainsKey(smName)) dicSmNews[smName] = listNews[i].Updated;
            }

            foreach (var o in dicSmNews)
            {
                CoreMr.Reddevil.Global.Sitemap.AddLocation(CoreMr.Reddevil.Web.HttpRequest.Domain + "/" + o.Key + ".xml", o.Value);

                listSitemap.Add(o.Key);
            }

            try
            {
                if (!listSitemap.Exists(o => o.Equals(endCode)))
                {
                    ViewPage.Error404();
                }

                string sitemap = CoreMr.Reddevil.Global.Sitemap.BuiltRootXml();

                ViewPage.Response.ContentType = "text/xml";
                ViewPage.Response.Write(sitemap);
            }
            catch (Exception ex)
            {
                Error.Write(ex.Message + " - Có lỗi xảy ra khi tạo sitemap.");

                ViewPage.Response.Write("Có lỗi xảy ra khi tạo sitemap.");
            }

            ViewPage.Response.End();
        }

        public void ActionMisc()
        {
            RenderView("Index");

            CoreMr.Reddevil.Global.Sitemap._siteMapList = null;

            var defaultPage = SysPageService.Instance.GetByID_Cache(ViewPage.CurrentSite.PageID);
            if (defaultPage != null)
                CoreMr.Reddevil.Global.Sitemap.AddLocation(ViewPage.GetPageURL(defaultPage).Replace(defaultPage.Url + Setting.Sys_PageExt, ""), DateTime.Now, "1.0", CoreMr.Reddevil.Global.ChangeFrequency.Daily);

            CoreMr.Reddevil.Global.Sitemap.AddLocation(CoreMr.Reddevil.Web.HttpRequest.Domain + "/sitemap.xml", DateTime.Now, "0.5", CoreMr.Reddevil.Global.ChangeFrequency.Monthly);

            try
            {
                string sitemap = CoreMr.Reddevil.Global.Sitemap.BuiltXml();

                ViewPage.Response.ContentType = "text/xml";
                ViewPage.Response.Write(sitemap);
            }
            catch (Exception ex)
            {
                Error.Write(ex.Message + " - Có lỗi xảy ra khi tạo sitemap.");

                ViewPage.Response.Write("Có lỗi xảy ra khi tạo sitemap.");
            }

            ViewPage.Response.End();
        }

        public void ActionCategory()
        {
            RenderView("Index");

            CoreMr.Reddevil.Global.Sitemap._siteMapList = null;

            var listPage = SysPageService.Instance.CreateQuery()
                                    .Where(o => o.Activity == true && (o.ModuleCode == "MNews" || o.ModuleCode == "MProduct") && o.LangID == 1)
                                    .OrderByAsc(o => o.Created)
                                    .ToList_Cache();

            for (var i = 0; listPage != null && i < listPage.Count; i++)
            {
                var cleanURL = ModCleanURLService.Instance.CreateQuery()
                                                .Where(o => o.Type == "Page" && o.Url == listPage[i].Url && o.LangID == 1)
                                                .ToSingle_Cache();

                if (cleanURL == null || cleanURL.MenuID < 1 || cleanURL.ID < 1) continue;

                var url = ViewPage.GetPageURL(listPage[i]);
                var lastmod = listPage[i].Updated <= DateTime.MinValue ? (listPage[i].Created <= DateTime.MinValue ? DateTime.Now : listPage[i].Created) : listPage[i].Updated;

                CoreMr.Reddevil.Global.Sitemap.AddLocation(url, lastmod, "0.8", CoreMr.Reddevil.Global.ChangeFrequency.Monthly);
            }

            try
            {
                string sitemap = CoreMr.Reddevil.Global.Sitemap.BuiltXml();

                ViewPage.Response.ContentType = "text/xml";
                ViewPage.Response.Write(sitemap);
            }
            catch (Exception ex)
            {
                Error.Write(ex);
                ViewPage.Error404();

            }

            ViewPage.Response.End();
        }

        //public void ActionProduct()
        //{
        //    RenderView("Index");

        //    string endCode = ViewPage.CurrentVQS.EndCode.ToLower();
        //    endCode = endCode.Replace("sitemap-product-", "");

        //    var minDate = CoreMr.Reddevil.Global.Convert.ToDateTime(endCode + "-1");
        //    var maxDate = CoreMr.Reddevil.Global.Convert.ToDateTime(endCode + "-31");
        //    if (maxDate <= DateTime.MinValue) maxDate = CoreMr.Reddevil.Global.Convert.ToDateTime(endCode + "-30");
        //    if (maxDate <= DateTime.MinValue) maxDate = CoreMr.Reddevil.Global.Convert.ToDateTime(endCode + "-29");
        //    if (maxDate <= DateTime.MinValue) maxDate = CoreMr.Reddevil.Global.Convert.ToDateTime(endCode + "-28");

        //    if (minDate <= DateTime.MinValue || maxDate <= DateTime.MinValue) ViewPage.Error404();

        //    CoreMr.Reddevil.Global.Sitemap._siteMapList = null;

        //    //product
        //    var listProduct = ModProductService.Instance.CreateQuery()
        //                                    .Select(o => new { o.Url, o.Published, o.Updated })
        //                                    .Where(o => o.Activity == true)
        //                                    .Where(o => o.Updated >= minDate && o.Updated <= maxDate)
        //                                    .OrderByDesc(o => o.Updated)
        //                                    .ToList_Cache();

        //    for (var i = 0; listProduct != null && i < listProduct.Count; i++)
        //    {
        //        var cleanURL = ModCleanURLService.Instance.CreateQuery()
        //                                        .Where(o => o.Type == "Product" && o.Url == listProduct[i].Url && o.LangID == 1)
        //                                        .ToSingle_Cache();

        //        if (cleanURL == null || cleanURL.MenuID < 1 || cleanURL.ID < 1) continue;

        //        var url = ViewPage.GetURL(listProduct[i].Url);
        //        var lastmod = listProduct[i].Updated <= DateTime.MinValue ? listProduct[i].Published : listProduct[i].Updated;

        //        CoreMr.Reddevil.Global.Sitemap.AddLocation(url, lastmod, "0.6", CoreMr.Reddevil.Global.ChangeFrequency.Daily);
        //    }

        //    try
        //    {
        //        string sitemap = CoreMr.Reddevil.Global.Sitemap._siteMapList != null ? CoreMr.Reddevil.Global.Sitemap.BuiltXml() : string.Empty;

        //        ViewPage.Response.ContentType = "text/xml";
        //        ViewPage.Response.Write(sitemap);

        //        Ping(CoreMr.Reddevil.Web.HttpRequest.Domain + "/" + endCode + ".xml");
        //    }
        //    catch (Exception ex)
        //    {
        //        Error.Write(ex);
        //        ViewPage.Error404();

        //    }

        //    ViewPage.Response.End();
        //}

        public void ActionNews()
        {
            RenderView("Index");

            string endCode = ViewPage.CurrentVQS.EndCode.ToLower();
            endCode = endCode.Replace("sitemap-news-", "");

            var minDate = CoreMr.Reddevil.Global.Convert.ToDateTime(endCode + "-1");
            var maxDate = CoreMr.Reddevil.Global.Convert.ToDateTime(endCode + "-31");
            if (maxDate <= DateTime.MinValue) maxDate = CoreMr.Reddevil.Global.Convert.ToDateTime(endCode + "-30");
            if (maxDate <= DateTime.MinValue) maxDate = CoreMr.Reddevil.Global.Convert.ToDateTime(endCode + "-29");
            if (maxDate <= DateTime.MinValue) maxDate = CoreMr.Reddevil.Global.Convert.ToDateTime(endCode + "-28");

            if (minDate <= DateTime.MinValue || maxDate <= DateTime.MinValue) ViewPage.Error404();

            CoreMr.Reddevil.Global.Sitemap._siteMapList = null;

            //product
            var listNews = ModNewsService.Instance.CreateQuery()
                                    .Select(o => new { o.Url, o.Published, o.Updated })
                                    .Where(o => o.Activity == true)
                                    .Where(o => o.Updated >= minDate && o.Updated <= maxDate)
                                    .OrderByDesc(o => o.Updated)
                                    .ToList_Cache();

            for (var i = 0; listNews != null && i < listNews.Count; i++)
            {
                var cleanURL = ModCleanURLService.Instance.CreateQuery()
                                                .Where(o => o.Type == "News" && o.Url == listNews[i].Url && o.LangID == 1)
                                                .ToSingle_Cache();

                if (cleanURL == null || cleanURL.MenuID < 1 || cleanURL.ID < 1) continue;

                var url = ViewPage.GetURL(listNews[i].Url);
                var lastmod = listNews[i].Updated <= DateTime.MinValue ? listNews[i].Published : listNews[i].Updated;

                CoreMr.Reddevil.Global.Sitemap.AddLocation(url, lastmod, "0.6", CoreMr.Reddevil.Global.ChangeFrequency.Daily);
            }

            try
            {
                string sitemap = CoreMr.Reddevil.Global.Sitemap.BuiltXml();

                ViewPage.Response.ContentType = "text/xml";
                ViewPage.Response.Write(sitemap);

                Ping(CoreMr.Reddevil.Web.HttpRequest.Domain + "/" + endCode + ".xml");
            }
            catch (Exception ex)
            {
                Error.Write(ex);
                ViewPage.Error404();

            }

            ViewPage.Response.End();
        }

        private static void Ping(string url)
        {
            //GOOGLE
            try
            {
                var request = System.Net.WebRequest.Create(CoreMr.Reddevil.Web.HttpRequest.Scheme + "://www.google.com/webmasters/tools/ping?sitemap=" + url);
                request.GetResponse();
            }
            catch (Exception ex)
            {
                Error.Write(ex);

            }
        }
    }
}