﻿using Reddevil.Lib.MVC;
using Reddevil.Lib.Models;
using Reddevil.Lib.Global;

namespace Reddevil.Lib.Controllers
{
    [ModuleInfo(Name = "ĐK : Tin trưng bày", Code = "CNewsGarniture", IsControl = true, Order = 2)]
    public class CNewsGarnitureController : Controller
    {

        [CoreMr.Reddevil.MVC.PropertyInfo("Chuyên mục", "Type|NewsGarniture")]
        public int MenuID;

        [CoreMr.Reddevil.MVC.PropertyInfo("Trang")]
        public int PageID;

        [CoreMr.Reddevil.MVC.PropertyInfo("Số lượng")]
        public int PageSize = 5;

       
        public override void OnLoad()
        {
            //ViewBag.ListView = ModNewsGarnitureService.Instance.CreateQuery()
            //                         .Where(o => o.Activity == true )
            //                         .WhereIn(MenuID > 0, o => o.MenuID, WebMenuService.Instance.GetChildIDForWeb_Cache("NewsGarniture", MenuID, ViewPage.CurrentLang.ID))
            //                         .OrderByDesc(o => new { o.Updated, o.Published })
            //                         .Take(PageSize)
            //                         .ToList_Cache();
            ViewBag.Page = SysPageService.Instance.GetByID_Cache(PageID);
            ViewBag.ListPage = SysPageService.Instance.GetByParent_Cache(PageID);
        }
    }
}