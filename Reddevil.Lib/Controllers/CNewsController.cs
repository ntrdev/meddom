﻿using Reddevil.Lib.MVC;
using Reddevil.Lib.Models;
using Reddevil.Lib.Global;

namespace Reddevil.Lib.Controllers
{
    [ModuleInfo(Name = "ĐK : Tin tức", Code = "CNews", IsControl = true, Order = 2)]
    public class CNewsController : Controller
    {
        //[CoreMr.Reddevil.MVC.PropertyInfo("Default[MenuID-true|PageID-true|PageID2-false|PageSize-true],Top[MenuID-true|PageID-true|PageID2-true|PageSize-true]")]
        //public string LayoutDefine;

        [CoreMr.Reddevil.MVC.PropertyInfo("Chuyên mục", "Type|News")]
        public int MenuID;

        [CoreMr.Reddevil.MVC.PropertyInfo("Trang")]
        public int PageID;

        [CoreMr.Reddevil.MVC.PropertyInfo("Số lượng")]
        public int PageSize = 5;

        [CoreMr.Reddevil.MVC.PropertyInfo("Vị trí", "ConfigKey|Mod.NewsState")]
        public int State;


        [CoreMr.Reddevil.MVC.PropertyInfo("Văn bản / Html")]
        public string ServiceText;

        [CoreMr.Reddevil.MVC.PropertyInfo("Tiêu đề")]
        public string Title = string.Empty;


        public override void OnLoad()
        {
         
            //ViewBag.ListRecent = ModNewsService.Instance.CreateQuery()
            //                        .Where(o => o.Activity == true)
            //                        .Where(State > 0, o => (o.State & State) == State)
            //                        .WhereIn(MenuID > 0, o => o.MenuID, WebMenuService.Instance.GetChildIDForWeb_Cache("News", MenuID, ViewPage.CurrentLang.ID))
            //                        .OrderByDesc(o => new { o.Order })
            //                        .Take(PageSize)
            //                        .ToList_Cache();
            //ViewBag.Text = !string.IsNullOrEmpty(ServiceText) ? Data.Base64Decode(ServiceText) : string.Empty;
            //ViewBag.Title = Title;
            if (ViewLayout == "Home")
            {
                ViewBag.Page = SysPageService.Instance.GetByID_Cache(PageID);
                ViewBag.Video = ModNewsVideoService.Instance.CreateQuery()
                                    .Where(o => o.Activity == true)
                                    .Where(State > 0, o => (o.State & State) == State)
                                    .OrderByDesc(o => new { o.Updated, o.Published })
                                    .Take(PageSize)
                                    .ToList_Cache();

                ViewBag.Data = ModNewsService.Instance.CreateQuery()
                                .Where(o => o.Activity == true && o.Status == false)
                                .Where(State > 0, o => (o.State & State) == State)
                                .WhereIn(MenuID > 0, o => o.MenuID, WebMenuService.Instance.GetChildIDForWeb_Cache("News", MenuID, ViewPage.CurrentLang.ID))
                                .OrderByDesc(o => new { o.Updated, o.Published })
                                .Take(PageSize)
                                .ToList_Cache();
            }
            else
            {
                ViewBag.ListView = ModNewsService.Instance.CreateQuery()
                                  .Where(o => o.Activity == true && o.Status == false)
                                  .Where(State > 0, o => (o.State & State) == State)
                                  .WhereIn(MenuID > 0, o => o.MenuID, WebMenuService.Instance.GetChildIDForWeb_Cache("News", MenuID, ViewPage.CurrentLang.ID))
                                  .OrderByDesc(o => new { o.Updated, o.Published })
                                  .Take(PageSize)
                                  .ToList_Cache();

                ViewBag.Category = MenuID;
                ViewBag.State = State;
                ViewBag.Title = Title;
            }
        }
    }
}