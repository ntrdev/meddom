﻿using Reddevil.Lib.Global;
using Reddevil.Lib.Models;
using Reddevil.Lib.MVC;
using Controller = Reddevil.Lib.MVC.Controller;

namespace Reddevil.Lib.Controllers
{
    [ModuleInfo(Name = "MO_Default: Df - Bài viết", Code = "MContent", Order = 1, Crawl = false)]
    public class MContentController : Controller
    {
        public void ActionIndex()
        {
            var _News = ModNewsService.Instance.CreateQuery()
                                                .Select(o => new{o.ID, o.Name, o.Content, o.PageTitle, o.PageDescription, o.PageKeywords })
                                                .Where(o => o.Status == true && o.MenuID == ViewPage.CurrentPage.ID)
                                                .ToSingle_Cache();
            if (_News != null)
            {
                ViewPage.CurrentPage.Content = _News.Content;
                ViewPage.CurrentPage.File = _News.File;
                ViewPage.CurrentPage.PageTitle = _News.Name;
                ViewPage.CurrentPage.PageKeywords = _News.PageKeywords;
                ViewPage.CurrentPage.PageDescription = _News.Summary;
            }
            //SEO
            ViewPage.CurrentPage.PageURL = ViewPage.CurrentURL;
            ViewPage.CurrentPage.PageFile = CoreMr.Reddevil.Web.HttpRequest.Domain + Global.Utils.GetResizeFile(ViewPage.CurrentPage.File, 4, 630, 315);
        }
    }
}