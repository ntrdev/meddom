﻿using Reddevil.Lib.MVC;
using Reddevil.Lib.Models;

namespace Reddevil.Lib.Controllers
{
    [ModuleInfo(Name = "ĐK : DS Nhà khoa học", Code = "CScientist", IsControl = true, Order = 2)]
    public class CScientistController : Controller
    {

        [CoreMr.Reddevil.MVC.PropertyInfo("Trang")]
        public int PageID;

        [CoreMr.Reddevil.MVC.PropertyInfo("Trang2")]
        public int PageID2;

        [CoreMr.Reddevil.MVC.PropertyInfo("Số lượng")]
        public int PageSize = 5;

        [CoreMr.Reddevil.MVC.PropertyInfo("Vị trí", "ConfigKey|Mod.NewsState")]
        public int State;

        public override void OnLoad()
        {
            var dBQuery = ModScientistService.Instance.CreateQuery();
            var listItem = dBQuery.Clone();

            ViewBag.Count = dBQuery.Select(o => o.Id).Where(o=>o.Status == 9).Count().ToValue_Cache().ToInt(0);
            ViewBag.Data = listItem.Where(o => o.Images != null && o.Status == 9).Take(PageSize).OrderByDesc(o => o.Id).ToList_Cache();

            ViewBag.Page = SysPageService.Instance.GetByID_Cache(PageID);
            ViewBag.Page2 = SysPageService.Instance.GetByID_Cache(PageID2);

            ViewBag.Research  = ModNewsResearchService.Instance.CreateQuery()
                                     .Where(o => o.Activity == true)
                                     .OrderByDesc(o => new { o.Updated, o.Order })
                                     .Take(3)
                                     .ToList_Cache();

        }
    }
}