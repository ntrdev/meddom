﻿using Reddevil.Lib.Models;
using Reddevil.Lib.MVC;
using System;
using System.Collections.Generic;

namespace Reddevil.Lib.Controllers
{
    [ModuleInfo(Name = "ĐK : Quảng cáo / Liên kết", Code = "CAdv", IsControl = true, Order = 1)]
    public class CAdvController : Controller
    {
        [CoreMr.Reddevil.MVC.PropertyInfo("Default[MenuID-true|MultiRecord-true],SlideSub[MenuID-false|MultiRecord-false]")]
        public string LayoutDefine;

        [CoreMr.Reddevil.MVC.PropertyInfo("Chuyên mục", "Type|Adv")]
        public int MenuID;

        [CoreMr.Reddevil.MVC.PropertyInfo("Dữ liệu")]
        public bool MultiRecord = true;

        public override void OnLoad()
        {
            if (!MultiRecord)
                ViewBag.Data = ModAdvService.Instance.CreateQuery()
                                        .Where(o => o.Activity == true && o.MenuID == MenuID)
                                        .OrderByAsc(o => new { o.Order, o.ID })
                                        .Take(1)
                                        .ToSingle_Cache();
            else if (ViewLayout == "Banner")
            {
                ViewBag.Adv = MenuID;
            }
            else
            {
                ViewBag.Data = ModAdvService.Instance.CreateQuery()
                                            .Where(o => o.Activity == true && o.MenuID == MenuID)
                                            .OrderByAsc(o => new { o.Order, o.ID })
                                            .ToList_Cache();
            }
        }
    }
}