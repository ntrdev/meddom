﻿using Reddevil.Lib.Global;
using Reddevil.Lib.Models;
using Reddevil.Lib.MVC;

namespace Reddevil.Lib.Controllers
{
    [ModuleInfo(Name = "MO_Default: Df - Tin video", Code = "MNewsVideo", Order = 10, Crawl = true)]
    public class MNewsVideoController : Controller
    {
        [CoreMr.Reddevil.MVC.PropertyInfo("Danh mục", "Type|NewsVideo")]
        public int MenuID;

        [CoreMr.Reddevil.MVC.PropertyInfo("Dạng hiển thị", "Danh sách|Index,Bộ sưu tập|HoatDong")]
        public string Layout = "Index";

        [CoreMr.Reddevil.MVC.PropertyInfo("Số lượng hiển thị")]
        public int PageSize = 20;

        public void ActionIndex(MNewsVideoModel model)
        {
            // Utils.UpdateStatistic();

            if (ViewPage.CurrentPage.MenuID > 0)
                MenuID = ViewPage.CurrentPage.MenuID;

            string _Keyword = !string.IsNullOrEmpty(model.news) ? Data.GetCode(model.news) : "";
            RenderView(Layout);

            PageSize = model.PageSize > 0 ? model.PageSize : PageSize;
            var dbQuery = ModNewsVideoService.Instance.CreateQuery()
                                    .Where(o => o.Activity == true && o.MenuID != 11748)
                                    .Where(!string.IsNullOrEmpty(_Keyword), o => o.Url.Contains(_Keyword))
                                    .WhereIn(MenuID > 0, o => o.MenuID, WebMenuService.Instance.GetChildIDForWeb_Cache("NewsVideo", MenuID, ViewPage.CurrentLang.ID))
                                    .Skip(PageSize * model.page)
                                    .Take(PageSize)
                                    .OrderByDesc(o => new { o.Order, o.ID });

            ViewBag.Data = dbQuery.ToList_Cache();
            model.TotalRecord = dbQuery.TotalRecord;
            model.PageSize = PageSize;
            ViewBag.Model = model;

            //SEO
            ViewPage.CurrentPage.PageURL = ViewPage.CurrentURL;
            ViewPage.CurrentPage.PageFile = CoreMr.Reddevil.Web.HttpRequest.Domain + Global.Utils.GetResizeFile(ViewPage.CurrentPage.File, 4, 630, 315);
        }
        private ModNewsVideoEntity item;
        public void ActionDetail(string endCode)
        {
            if (ViewPage.CurrentPage.MenuID > 0)
                MenuID = ViewPage.CurrentPage.MenuID;
            if (Cookies.Exist("CP.UserID"))
            {
                item = ModNewsVideoService.Instance.CreateQuery()
                               .Where(o => o.Url == endCode)
                               .ToSingle_Cache();
            }
            else
            {
                item = ModNewsVideoService.Instance.CreateQuery()
                              .Where(o => o.Activity == true && o.Url == endCode)
                              .ToSingle_Cache();
            }
            if (item != null)
            {
                // Utils.UpdateStatistic();

                //up view
                item.UpView();

                ViewBag.Other = ModNewsVideoService.Instance.CreateQuery()
                                            .Where(o => o.Activity == true && o.ID != item.ID)
                                            .WhereIn(MenuID > 0, o => o.MenuID, WebMenuService.Instance.GetChildIDForWeb_Cache("NewsVideo", MenuID, ViewPage.CurrentLang.ID))
                                            .OrderByDesc(o => new { o.Published, o.ID })
                                            .Take(PageSize)
                                            .ToList_Cache();

                ViewPage.ViewBag.Data = ViewBag.Data = item;

                //SEO
                ViewPage.CurrentPage.PageTitle = (!string.IsNullOrEmpty(item.PageTitle) ? item.PageTitle : item.Name) + " | " + CoreMr.Reddevil.Web.HttpRequest.Host;
                ViewPage.CurrentPage.PageDescription = string.IsNullOrEmpty(item.PageDescription) ? (!string.IsNullOrEmpty(item.PageTitle) ? item.PageTitle : item.Name) : item.PageDescription;
                ViewPage.CurrentPage.PageKeywords = string.IsNullOrEmpty(item.PageKeywords) ? (!string.IsNullOrEmpty(item.PageTitle) ? item.PageTitle : item.Name) : item.PageKeywords;
                ViewPage.CurrentPage.PageURL = ViewPage.GetURL(item.MenuID, item.Url);
                ViewPage.CurrentPage.PageFile = CoreMr.Reddevil.Web.HttpRequest.Domain + Global.Utils.GetResizeFile(item.File, 4, 600, 315);
            }
            else
            {
                ViewPage.Error404();
            }
        }
    }

    public class MNewsVideoModel
    {
        private int _page;
        public int page
        {
            get { return _page; }
            set { _page = value - 1; }
        }

        public int TotalRecord { get; set; }
        public int PageSize { get; set; }

        public string news { get; set; }
    }
}