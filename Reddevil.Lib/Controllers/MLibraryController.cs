﻿using Reddevil.Lib.Global;
using Reddevil.Lib.Models;
using Reddevil.Lib.MVC;

namespace Reddevil.Lib.Controllers
{
    [ModuleInfo(Name = "MO_Default: Df - Thư viện số", Code = "MLibrary", Order = 10, Crawl = true)]
    public class MLibraryController : Controller
    {
        [CoreMr.Reddevil.MVC.PropertyInfo("Danh mục", "Type|Library")]
        public int MenuID;

        [CoreMr.Reddevil.MVC.PropertyInfo("Dạng hiển thị", "Danh sách|Index,Bộ sưu tập|HoatDong")]
        public string Layout = "Index";

        [CoreMr.Reddevil.MVC.PropertyInfo("Số lượng hiển thị")]
        public int PageSize = 20;

        public void ActionIndex(MLibraryModel model)
        {
            // Utils.UpdateStatistic();

            if (ViewPage.CurrentPage.MenuID > 0)
                MenuID = ViewPage.CurrentPage.MenuID;

            string _Keyword = !string.IsNullOrEmpty(model.news) ? Data.GetCode(model.news) : "";
            RenderView(Layout);
            PageSize = model.PageSize > 0 ? model.PageSize : PageSize;


            var dbQuery = ModLibraryService.Instance.CreateQuery()
                                    .Where(o => o.Activity == true)
                                    .Where(!string.IsNullOrEmpty(model.news), o => o.Url.Contains(_Keyword) || o.Name.Contains(model.news))
                                    .WhereIn(MenuID > 0, o => o.CategoryID, WebMenuService.Instance.GetChildIDForWeb_Cache("Library", MenuID, ViewPage.CurrentLang.ID))
                                    .Skip(PageSize * model.page)
                                    .Take(PageSize);

            string sort = CoreMr.Reddevil.Web.HttpQueryString.GetValue("sort").ToString();

            switch (sort.ToLower())
            {
                case "new_asc":
                    dbQuery.OrderByDesc(o => o.Order);
                    break;
                case "view_desc":
                    dbQuery.OrderByDesc(o => o.Views);
                    break;
                case "down_desc":
                    dbQuery.OrderByDesc(o => o.Download);
                    break;
                case "":
                    dbQuery.OrderByDesc(o => new { o.ID, o.Order });
                    break;
            }
            ViewBag.Views = ModLibraryService.Instance.CreateQuery()
                                    .Where(o => o.Activity == true)
                                    .OrderByDesc(o => o.ID)
                                    .Take(4).ToList_Cache();
            ViewBag.Data = dbQuery.ToList_Cache();
            model.TotalRecord = dbQuery.TotalRecord;
            model.PageSize = PageSize;
            ViewBag.Model = model;

            //SEO
            ViewPage.CurrentPage.PageURL = ViewPage.CurrentURL;
            ViewPage.CurrentPage.PageFile = CoreMr.Reddevil.Web.HttpRequest.Domain + Global.Utils.GetResizeFile(ViewPage.CurrentPage.File, 4, 600, 315);
        }
        private ModLibraryEntity item;
        public void ActionDetail(string endCode)
        {
            if (ViewPage.CurrentPage.MenuID > 0)
                MenuID = ViewPage.CurrentPage.MenuID;

            if (Cookies.Exist("CP.UserID"))
            {

                item = ModLibraryService.Instance.CreateQuery()
                                 .Where(o => o.Activity == true && o.Url == endCode)
                                 .ToSingle_Cache();
            }
            else
            {
                item = ModLibraryService.Instance.CreateQuery()
                               .Where(o => o.Url == endCode)
                               .ToSingle_Cache();
            }

            if (item != null)
            {
                //up view
                item.UpView();

                ViewBag.Other = ModLibraryService.Instance.CreateQuery()
                                            .Where(o => o.Activity == true && o.ID != item.ID)
                                            .WhereIn(MenuID > 0, o => o.CategoryID, WebMenuService.Instance.GetChildIDForWeb_Cache("Library", MenuID, ViewPage.CurrentLang.ID))
                                            .OrderByDesc(o => new { o.DateCreated, o.ID })
                                            .Take(10)
                                            .ToList_Cache();

                ViewPage.ViewBag.Data = ViewBag.Data = item;


                //SEO
                ViewPage.CurrentPage.PageTitle = item.Name + " | " + CoreMr.Reddevil.Web.HttpRequest.Host;
                ViewPage.CurrentPage.PageDescription = item.Description;
                ViewPage.CurrentPage.PageKeywords = item.Name;
                ViewPage.CurrentPage.PageURL = ViewPage.GetURL(item.CategoryID, item.Url);
                ViewPage.CurrentPage.PageFile = CoreMr.Reddevil.Web.HttpRequest.Domain + Global.Utils.GetResizeFile(item.Image, 4, 600, 315);

            }
            else
            {
                ViewPage.Error404();
            }
        }
    }

    public class MLibraryModel
    {
        private int _page;
        public int page
        {
            get { return _page; }
            set { _page = value - 1; }
        }

        public int TotalRecord { get; set; }
        public int PageSize { get; set; }

        public string news { get; set; }
    }
}