﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using CoreMr.Reddevil.Global;
using CoreMr.Reddevil.Web;
using Reddevil.Lib.Global;
using Reddevil.Lib.Models;
using Reddevil.Lib.MVC;
using System.Threading.Tasks;
using System.Web;
using System.IO;
using System.Text.RegularExpressions;

namespace Reddevil.Lib.Controllers
{
    [ModuleInfo(Name = "MO_Default:MApi", Code = "MApi", Order = 10, Crawl = false)]

    public class MApiController : Controller
    {
        public void ActionIndex() { }

        #region 
        public void ActionAvatarNKH(UpdateInfo updateInfo)
        {
            var response = HttpContext.Current;
            string fileImage = "", json = "";
            try
            {

                byte[] imageBytes = System.Convert.FromBase64String(updateInfo.imageBase64);
                using (var ms = new MemoryStream(imageBytes, 0, imageBytes.Length))
                {
                    string path = "~/data/upload/images/nhakhoahoc/" + updateInfo.nkhID;

                    if (!System.IO.Directory.Exists(path))
                        System.IO.Directory.CreateDirectory(ViewPage.Server.MapPath(path));


                    System.Drawing.Image image = System.Drawing.Image.FromStream(ms);
                    string _extentionImg = "";
                    if (image.RawFormat.Equals(ImageFormat.Jpeg))
                        _extentionImg = ".jpg";
                    else if (image.RawFormat.Equals(ImageFormat.Png))
                        _extentionImg = ".png";
                    else if (image.RawFormat.Equals(ImageFormat.Gif))
                        _extentionImg = ".gif";
                    else if (image.RawFormat.Equals(ImageFormat.Bmp))
                        _extentionImg = ".bmp";


                    string _DBFile = path + "/" + updateInfo.nkhID + "-" + DateTime.Now.Ticks + System.IO.Path.GetExtension(_extentionImg);
                    string _PhysFile = ViewPage.Server.MapPath(_DBFile);

                    image.Save(_PhysFile, image.RawFormat);

                    fileImage = _DBFile.Replace("~/", "/");
                    ms.Dispose();

                }
                ModCpdImageService.Instance.InsertOrUpdate(updateInfo.nkhID, new string[] { fileImage }, updateInfo.typeObj);


                json = JsonConvert.SerializeObject(new ResultModel
                {
                    Error = 0,
                    Message = "Insert Successfull",
                    DataInfo = fileImage
                }, Formatting.Indented);

            }
            catch (Exception rx)
            {
                Error.Write(rx);
                json = JsonConvert.SerializeObject(new ResultModel
                {
                    Error = 500,
                    Message = "Có lỗi xảy ra",
                    DataInfo = fileImage
                }, Formatting.Indented);
            }

            response.Response.Clear();
            response.Response.ContentType = "application/json; charset=utf-8";
            response.Response.Write(json);

            response.Response.End();
        }
        #endregion

        #region account user validate

        public void ActionGetAccount(GetAccountModel model)
        {
            var json = new Json();
            if (!string.IsNullOrEmpty(model.LoginName))
            {
                if (model.Type == "LoginName")
                {
                    var _user = ModUserService.Instance.GetByLoginName(model.LoginName);

                    if (!Reddevil.Lib.Global.Utils.IsLoginName(model.LoginName))
                    {
                        json.Instance.Html = "Tên tài khoản sai định dạng (tên đăng nhập không bao gồm các ký tự đặc biệt (* @ # $ % & .) vd: gshop_vt)";
                    }
                    else if (_user != null)
                    {
                        json.Instance.Html = "Tên đăng nhập đã có người sử dụng, vui lòng dùng tên khác.";
                    }

                }
                else if (model.Type == "email")
                {
                    var _user = ModUserService.Instance.GetByEmail(model.LoginName);

                    if (!Reddevil.Lib.Global.Utils.IsEmailAddress(model.LoginName))
                    {
                        json.Instance.Html = "Sai định dạng email.";
                    }
                    else if (_user != null)
                    {
                        json.Instance.Html = "Email đã tồn tại.";
                    }
                }
                else if (model.Type == "phone")
                {

                    var _user = ModUserService.Instance.CheckPhone(model.LoginName.Replace(".", ""));
                    if (!Global.Utils.IsPhoneNumber(model.LoginName) || model.LoginName.Length != 12)
                    {
                        json.Instance.Html = "Sai định dạng số điện thoại.";
                    }
                    else if (!Global.Utils.CheckCarrier(model.LoginName))
                    {
                        json.Instance.Html = "Hệ thống chỉ chấp nhận số điện thoại từ 3 nhà mạng Viettel, Mobifone, Vinaphone.";
                    }
                    else if (!_user)
                    {
                        json.Instance.Html = "";
                    }
                    else json.Instance.Html = "Số điện thoại đã tồn tại.";
                }

                else if (model.Type == "Pass")
                {
                    var hasNumber = new Regex(@"[0-9]+");
                    var hasUpperChar = new Regex(@"[A-Z]+");
                    var hasMinimum8Chars = new Regex(@".{8,}");

                    var isValidated = hasNumber.IsMatch(model.LoginName) && hasUpperChar.IsMatch(model.LoginName) && hasMinimum8Chars.IsMatch(model.LoginName);
                    if (!isValidated) json.Instance.Html = "Thêm ký tự viết hoa và ký tự là số, độ dài hơn 8 ký tự";

                }

            }

            ViewBag.Model = model;

            json.Create();
        }




        #endregion
    }

    #region account user
    public class GetAccountModel
    {
        public string LoginName { get; set; }
        public string Type { get; set; }

        public string Phone { get; set; }
    }


    #endregion

}
