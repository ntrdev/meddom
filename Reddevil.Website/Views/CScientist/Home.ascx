﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.ViewControl" %>

<% 
    var _page = ViewBag.Page as SysPageEntity;
    var _page2 = ViewBag.Page2 as SysPageEntity ?? new SysPageEntity();
    var listScientist = ViewBag.Data as List<ModScientistEntity>;
    var listPage = SysPageService.Instance.GetByParent_Cache(_page2.ID);
    var listItem = ViewBag.Research as List<ModNewsResearchEntity>;
%>
<section class="sec_3">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="box_headding">
                    <h2><a href="<%=ViewPage.GetURL("danh-sach-nha-khoa-hoc") %>">Nhà khoa học</a> <span>(<%= ViewBag.Count %> hồ sơ)</span> </h2>
                </div>
                <div class="box_data_nhakhoahoc">
                    <ul class="list_data">
                        <%for (int i = 0; listScientist != null && i < listScientist.Count; i++)
                            {%>
                        <li class="item_data">
                            <div class="box_img">
                                <a href="<%=ViewPage.ViewScientistURL %><%=Data.GetCode(listScientist[i].Degree+" "+listScientist[i].FullName) +"-"+listScientist[i].Id %>">
                                    <img src="<%=Utils.OptimalImage("http://img.meddom.org/0"+listScientist[i].Images) %>" alt="<%=listScientist[i].FullName %>"></a>
                            </div>
                            <div class="info">
                                <a href="<%=ViewPage.ViewScientistURL %><%=Data.GetCode(listScientist[i].Degree+" "+listScientist[i].FullName) +"-"+listScientist[i].Id %>">
                                    <h4><%=Data.GetFirstChar(listScientist[i].Degree).ToUpper()+"." + Data.GetFirstChar(listScientist[i].ProfessionalKnowledge).ToUpper()%> <%=listScientist[i].FullName %></h4>
                                  
                                    <p class="nganhnghe">
                                        <i class="fa fa-stethoscope"></i>&nbsp;Chuyên ngành <%=listScientist[i].GetSpecialized %>
                                    </p>
                                    <p>
                                        <i class="fa fa-calendar"></i>&nbsp;<%=Utils.FormatDate2(listScientist[i].Birthday) %>
                                    </p>
                                </a>
                            </div>
                        </li>
                        <%} %>
                    </ul>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="box_headding">
                    <h2><a href="<%=ViewPage.GetPageURL(_page2) %>"><%=_page2.Name %></a></h2>
                    <ul class="menu_hd">
                        <%for (int i = 0; listPage != null && i < (listPage.Count > 2 ? 2 : listPage.Count); i++)
                            {%>
                        <li><a href="<%=ViewPage.GetURL(listPage[i].MenuID,listPage[i].Url) %>"><%=listPage[i].Name %></a></li>
                        <%} %>
                    </ul>
                </div>
                <div class="box_body_content">
                    <ul class="list_news">
                        <%for (int i = 0; listItem != null && i < listItem.Count; i++)
                            { %>
                        <li class="item_news">
                            <div class="col-lg-6 col-6">
                                <div class="box_img">
                                    <a href="<%=ViewPage.GetURL(listItem[i].MenuID, listItem[i].Url) %>" class="clickall" title="<%=listItem[i].Name %>"></a>
                                    <img src="<%=Utils.OptimalImage(listItem[i].File) %>" height="92" width="77" alt="<%=listItem[i].Name %>">
                                    <div class="time_news">
                                        <span><%=Utils.FormatDateTime(listItem[i].Updated) %></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-6">
                                <h4 class="title_news"><a href="<%=ViewPage.GetURL(listItem[i].MenuID, listItem[i].Url) %>" title="<%=listItem[i].Name %>"><%=listItem[i].Name %></a></h4>
                                <p class="descreption"><%=listItem[i].Summary %></p>
                            </div>
                        </li>
                        <%} %>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</section>
