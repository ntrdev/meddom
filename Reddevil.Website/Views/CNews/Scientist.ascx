﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.ViewControl" %>

<%
    var _page = ViewBag.Page as SysPageEntity;
    if (_page == null) return;
    var listPage = ViewBag.ListPage as List<SysPageEntity>;
    var listNews = ViewBag.News as List<ModNewsEntity>;

%>

<section class="sec_3">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="box_headding">
                    <h2><a href="">Dữ liệu nhà khoa học</a> <span>(1058 hồ sơ)</span> </h2>
                    <ul class="menu_hd">
                        <li><a href="" class="icon_down"><i class="fa fa-download" aria-hidden="true"></i></a></li>
                    </ul>
                </div>
                <div class="box_data_nhakhoahoc">
                    <ul class="list_data">
                        <li class="item_data">
                            <div class="box_img">
                                <img src="assets/images/img/image 50 (1).png" alt="">
                            </div>
                            <div class="info">
                                <h4>PGS. TS Phan An</h4>
                                <p class="nganhnghe">
                                    Ngành y học
                                </p>
                            </div>
                        </li>
                        <li class="item_data">
                            <div class="box_img">
                                <img src="assets/images/img/image 51 (1).png" alt="">
                            </div>
                            <div class="info">
                                <h4>PGS. TS Phan An</h4>
                                <p class="nganhnghe">
                                    Ngành y học
                                </p>
                            </div>
                        </li>
                        <li class="item_data">
                            <div class="box_img">
                                <img src="assets/images/img/image 51 (1).png" alt="">
                            </div>
                            <div class="info">
                                <h4>PGS. TS Phan An</h4>
                                <p class="nganhnghe">
                                    Ngành y học
                                </p>
                            </div>
                        </li>


                        <li class="item_data">
                            <div class="box_img">
                                <img src="assets/images/img/image 52 (1).png" alt="">
                            </div>
                            <div class="info">
                                <h4>PGS. TS Phan An</h4>
                                <p class="nganhnghe">
                                    Ngành y học
                                </p>
                            </div>
                        </li>
                        <li class="item_data">
                            <div class="box_img">
                                <img src="assets/images/img/image 50 (1).png" alt="">
                            </div>
                            <div class="info">
                                <h4>PGS. TS Phan An</h4>
                                <p class="nganhnghe">
                                    Ngành y học
                                </p>
                            </div>
                        </li>
                        <li class="item_data">
                            <div class="box_img">
                                <img src="assets/images/img/image 50 (1).png" alt="">
                            </div>
                            <div class="info">
                                <h4>PGS. TS Phan An</h4>
                                <p class="nganhnghe">
                                    Ngành y học
                                </p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="box_headding">
                    <h2><a href="<%=ViewPage.GetPageURL(_page) %>"><%=_page.Name %></a></h2>
                </div>
                <div class="owl_news_id owl-carousel owl-theme">
                    <%for (int i = 0; listPage != null && i < listPage.Count; i++)
                        {
                            var listItem = ModNewsService.Instance.CreateQuery()
                                                    .Select(o => new { o.ID, o.Name, o.MenuID, o.Url, o.Published, o.Summary, o.File })
                                                    .Where(o => o.Activity == true)
                                                    .WhereIn(listPage[i].MenuID > 0, o => o.MenuID, WebMenuService.Instance.GetChildIDForWeb_Cache("News", listPage[i].MenuID, ViewPage.CurrentLang.ID))
                                                    .OrderByDesc(o => o.Order).Take(4).ToList_Cache();
                    %>
                    <div class="item">
                        <ul class="list_new_idk">
                            <%for (int j = 0; listItem != null && j < listItem.Count; j++)
                                { %>
                            <li class="box_item_news">
                                <div class="box_img">
                                    <a href="<%=ViewPage.GetURL(listItem[j].MenuID, listItem[j].Url) %>" class="clickall"></a>
                                    <img src="<%=Utils.GetResizeFile(listItem[j].File, 4, 500,350) %>" alt="<%=listItem[j].Name %>">
                                </div>
                                <div class="box_content">
                                    <h4><a href="<%=ViewPage.GetURL(listItem[j].MenuID, listItem[j].Url) %>"><%=listItem[j].Name %></a></h4>
                                    <p class="desc"><%=listItem[j].Summary %></p>
                                    <p class="date_news"><i class="fa fa-calendar" aria-hidden="true"></i><%=Utils.FormatDateTime(listItem[j].Published) %></p>
                                </div>
                            </li>
                            <%} %>
                        </ul>
                    </div>
                    <%} %>
                </div>
            </div>
        </div>
    </div>
</section>
