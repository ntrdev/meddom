﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.ViewControl" %>
<% 
    var listView = ViewBag.ListView as List<ModNewsEntity>;
    if (listView == null) return;
%>

<section class="box_slide_index">
    <div class="slide_index owl-carousel owl-theme slide_index-home">
        <%for (int i = 0; i < listView.Count; i++)
            { %>
        <div class="item">
            <div class="box_img">
                <a href="<%=ViewPage.GetURL(listView[i].MenuID, listView[i].Url) %>" title="<%=listView[i].Name %>">
                    <img src="<%=Utils.OptimalImage(listView[i].File) %>" alt="<%=listView[i].Name %>"></a>
            </div>
            <div class="mota_slide">
                <div class="noidung">
                    <img src="/interface/pc/images/phay.png">Một góc trưng bày Bảo tàng Công an nhân dân giai đoạn đổi mới, hội nhập và phát triển đất nước (1986-2015)
                </div>
                <a class="xemthem" href="<%=ViewPage.GetURL(listView[i].MenuID, listView[i].Url) %>" tabindex="-1">Xem chi tiết</a>
            </div>
        </div>
        <%} %>
    </div>
</section>
