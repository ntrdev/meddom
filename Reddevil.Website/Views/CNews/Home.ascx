﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.ViewControl" %>

<%
    var _page = ViewBag.Page as SysPageEntity;
    var listVideo = ViewBag.Video as List<ModNewsVideoEntity>;
    var listItem = ViewBag.Data as List<ModNewsEntity>;
    if (listItem == null || _page == null) return;
%>

<section class="sec_1">
    <div class="container">
        <div class="col-12 col-lg-12">
            <div class="row">
                <div class="col-12 col-lg-9">
                    <div class="box_headding">
                        <h2><a href="<%=ViewPage.GetURL(_page.MenuID, _page.Url) %>" title="<%=_page.Name %>"><%=_page.Name %></a></h2>
                    </div>
                    <div class="col-12 col-lg-12">
                        <div class="row">
                            <div class="col-12 col-lg-8">
                                <div class="featured-thumbnail">
                                    <p class="hm-reponsive">
                                        <a href="<%=ViewPage.GetURL(listItem[0].MenuID, listItem[0].Url) %>" title="<%=listItem[0].Name %>">
                                            <img src="<%=Utils.OptimalImage(listItem[0].File) %>" class="img-responsive " alt="<%=listItem[0].Name %>">
                                        </a>
                                    </p>
                                    <div class="bgtrans">
                                        <a href="<%=ViewPage.GetURL(listItem[0].MenuID, listItem[0].Url) %>" title="<%=listItem[0].Name %>">
                                            <h3><%=listItem[0].Name %></h3>
                                        </a>
                                        <div class="userdetail">
                                            <span><%=Utils.FormatDateTime(listItem[0].Updated) %></span> <span><i class="fa fa-eye"></i>&nbsp;<%=listItem[0].View %></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-lg-4">
                                <%if (listItem.Count > 2)
                                    {  %>
                                <div class="featured-thumbnail-right">
                                    <div class="thumbnail">
                                        <a href="<%=ViewPage.GetURL(listItem[1].MenuID, listItem[1].Url) %>" title="<%=listItem[1].Name %>">
                                            <img src="<%=Utils.OptimalImage(listItem[1].File) %>" class="img-responsive img-hover" alt="<%=listItem[1].Name %>">
                                        </a>
                                    </div>
                                    <h4 class="title_news"><a href="<%=ViewPage.GetURL(listItem[1].MenuID, listItem[1].Url) %>" title="<%=listItem[1].Name %>"><%=listItem[1].Name %></a></h4>
                                    <div class="date_news">
                                        <p class=""><i class="fa fa-calendar" aria-hidden="true"></i><%=Utils.FormatDate2(listItem[1].Updated) %></p>
                                        <p style="margin-left: 20px;"><i class="fa fa-eye"></i>&nbsp;<%=listItem[1].View %></p>
                                    </div>
                                </div>
                                <div class="featured-thumbnail-bottom">
                                    <div class="box_img">
                                        <a href="<%=ViewPage.GetURL(listItem[2].MenuID, listItem[2].Url) %>" title="<%=listItem[2].Name %>">
                                            <img src="<%=Utils.OptimalImage(listItem[2].File) %>" class="img-responsive img-hover" alt="<%=listItem[2].Name %>">
                                        </a>
                                    </div>
                                    <div class="info">
                                        <a href="<%=ViewPage.GetURL(listItem[2].MenuID, listItem[2].Url) %>" title="<%=listItem[2].Name %>">
                                            <h4 class="title_news"><%=listItem[2].Name %></h4>
                                            <div class="date_news">
                                                <p class=""><i class="fa fa-calendar" aria-hidden="true"></i><%=Utils.FormatDate2(listItem[2].Updated) %></p>
                                                <p style="margin-left: 20px;"><i class="fa fa-eye"></i>&nbsp;<%=listItem[2].View %></p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <%} %>
                            </div>
                        </div>
                    </div>
                </div>
                <%if (listVideo != null && listVideo.Count > 1)
                    { %>
                <div class="col-12 col-lg-3">
                    <div class="box_headding">
                        <h2><a href="/tin-video/" title="Video">Video</a></h2>
                    </div>
                    <ul class="list_video">
                        <li class="item_video">
                            <iframe style="width: 100%;" src="https://www.youtube.com/embed/<%=listVideo[0].UrlVideo.Split('=')[1] %>" title="<%=listVideo[0].Name %>" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            <h4><a href="<%=ViewPage.GetURL(listVideo[0].MenuID, listVideo[0].Url) %>" title="<%=listVideo[0].Name %>"><%=listVideo[0].Name %></a></h4>
                        </li>
                        <li class="item_video">
                            <iframe style="width: 100%;" src="https://www.youtube.com/embed/<%=listVideo[1].UrlVideo.Split('=')[1] %>" title="<%=listVideo[1].Name %>" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            <h4><a href="<%=ViewPage.GetURL(listVideo[1].MenuID, listVideo[1].Url) %>" title="<%=listVideo[1].Name %>"><%=listVideo[1].Name %></a></h4>
                        </li>
                    </ul>
                </div>
                <%} %>
            </div>
        </div>
    </div>
</section>