﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.ViewControl" %>

<%
    var page = ViewBag.Page as SysPageEntity;
    if (page == null) return;
    
    var listItem = ViewBag.Data as List<ModNewsEntity>;
    var listPage = SysPageService.Instance.GetByParent_Cache(page.ID);
    var _webUser = WebLogin.CurrentUser;
%>

<div class="box-pro all">
    <div class="hero">
        <h3>CÔNG TRÌNH TIÊU BIỂU</h3>
        <p>Chúng tôi tự hào đã thực hiện</p>
    </div>
    <div class="portfolio">
        <ul class="filter-home clear">
            <li class="active"><a href="<%=ViewPage.GetPageURL(page) %>">Xem tất cả</a></li>
            <%for (var i = 0; listPage != null && i < listPage.Count; i++){%>
            <li><a href="<%=ViewPage.GetPageURL(listPage[i]) %>"><%=listPage[i].Name %></a></li>
            <%} %>
        </ul>
        <ul class="isotope clear">
            <%for (var i = 0; listItem != null && i < listItem.Count; i++)
              {
                  string url = ViewPage.GetURL(listItem[i].MenuID, listItem[i].Code);
            %>
            <li class="isotope-item">
                <img src="<%=Utils.GetResizeFile(listItem[i].File, 4, 300, 300)%>" alt="<%=listItem[i].Name%>" />
                <div class="hover">
                    <p>
                        <a href="<%=url%>" <%if (_webUser != null) { %> onclick="flow_user('News','<%=listItem[i].ID %>','<%=_webUser.ID %>','0')"<%}%>  class="btn" target="_blank"><i class="fa fa-hand-o-right"></i>Xem<br />công trình</a>
                    </p>
                    <h2 class="title-ct"><%=listItem[i].Name%></h2>
                </div>
            </li>
            <%} %>
        </ul>
    </div>
</div>