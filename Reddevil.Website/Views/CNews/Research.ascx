﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.ViewControl" %>

<%
    var _page = ViewBag.Page as SysPageEntity;
    if (_page == null) return;
    var listPage = ViewBag.ListPage as List<SysPageEntity>;
    var listNews = ViewBag.News as List<ModNewsEntity>;

%>
<section class="sec_2">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="box_headding">
                    <h2><a href="<%=ViewPage.GetPageURL(_page) %>"><%=_page.Name %></a></h2>
                    <ul class="menu_hd">
                        <%for (int i = 0; listPage != null && i < listPage.Count; i++)
                            { %>
                        <li><a href="<%=ViewPage.GetPageURL(listPage[i]) %>"><%=listPage[i].Name %> </a></li>
                        <%} %>
                    </ul>
                </div>
                <div class="list_nghiencuu owl-carousel owl-theme">
                    <%for (int i = 0; listNews != null && i < listNews.Count; i++)
                        { %>
                    <div class="item">
                        <div class="box_img">
                            <a href="<%=ViewPage.GetURL(listNews[i].MenuID, listNews[i].Url) %>" class="clickall"></a>
                            <img src="<%=Utils.OptimalImage(listNews[i].File) %>" alt="<%=listNews[i].Name %>">
                        </div>
                        <div class="box_content">
                            <h4><a href="<%=ViewPage.GetURL(listNews[i].MenuID, listNews[i].Url) %>"><%=listNews[i].Name %></a></h4>
                            <p class="desc"><%=listNews[i].Summary %></p>
                            <p class="date_news">
                                <i class="fa fa-calendar" aria-hidden="true"></i>
                                <%=Utils.FormatDateTime(listNews[i].Published) %>
                            </p>
                        </div>
                    </div>
                    <%} %>
                </div>
            </div>
        </div>

    </div>
</section>
