﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.ViewControl" %>

<%

    var listVideo = ViewBag.ListView as List<ModNewsEntity>;
    if (listVideo == null) return;
%>
<link rel="stylesheet" href="/interface/pc/css/jquery.jConveyorTicker.min.css">
<div class="box_conveyor">
    <div class="jctkr-label">
        <strong>Nổi bật</strong>
    </div>
    <div class="js-conveyor-1">
        <ul>
            <%for (int i = 0; i < listVideo.Count; i++)
                { %>
            <li>
                <a href="<%=ViewPage.GetURL(listVideo[i].MenuID, listVideo[i].Url) %>"><span><%=listVideo[i].Name %></span></a>
            </li>
            <%} %>
        </ul>
    </div>

</div>
