﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.ViewControl" %>

<div class="sidebar-box">
    <div class="sidebar-title">
        <a href="javascript:void(0)"><%=ViewBag.Title %></a>
    </div>
    <ul class="list-new-side d-flex-wrap <%=ViewBag.State %>_<%=ViewBag.Category %>" data-value-load="<%=ViewBag.Category %>" data-func-load="<%=ViewBag.State %>">
        <li class="placeload">
            <div class="image-placeholder loads"></div>
            <div class="placeholder-footer">
                <div class="footer-block">
                    <div class="content-shape loads"></div>
                    <div class="content-shape loads"></div>
                    <div class="content-shape loads"></div>
                </div>
            </div>
        </li>
        <li class="placeload">

            <div class="placeholder-footer">
                <div class="footer-block">
                    <div class="content-shape loads"></div>
                    <div class="content-shape loads"></div>
                </div>
            </div>
        </li>
        <li class="placeload">

            <div class="placeholder-footer">
                <div class="footer-block">
                    <div class="content-shape loads"></div>
                    <div class="content-shape loads"></div>
                </div>
            </div>
        </li>
        <li class="placeload">

            <div class="placeholder-footer">
                <div class="footer-block">
                    <div class="content-shape loads"></div>
                    <div class="content-shape loads"></div>
                </div>
            </div>
        </li>
        <li class="placeload">

            <div class="placeholder-footer">
                <div class="footer-block">
                    <div class="content-shape loads"></div>
                    <div class="content-shape loads"></div>
                </div>
            </div>
        </li>
        <li class="placeload">

            <div class="placeholder-footer">
                <div class="footer-block">
                    <div class="content-shape loads"></div>
                    <div class="content-shape loads"></div>
                </div>
            </div>
        </li>
        <li class="placeload">

            <div class="placeholder-footer">
                <div class="footer-block">
                    <div class="content-shape loads"></div>
                    <div class="content-shape loads"></div>
                </div>
            </div>
        </li>
        <li class="placeload">

            <div class="placeholder-footer">
                <div class="footer-block">
                    <div class="content-shape loads"></div>
                    <div class="content-shape loads"></div>
                </div>
            </div>
        </li>
        <li class="placeload">

            <div class="placeholder-footer">
                <div class="footer-block">
                    <div class="content-shape loads"></div>
                    <div class="content-shape loads"></div>
                </div>
            </div>
        </li>

        <li class="placeload">

            <div class="placeholder-footer">
                <div class="footer-block">
                    <div class="content-shape loads"></div>
                    <div class="content-shape loads"></div>
                </div>
            </div>
        </li>
        <li class="placeload">

            <div class="placeholder-footer">
                <div class="footer-block">
                    <div class="content-shape loads"></div>
                    <div class="content-shape loads"></div>
                </div>
            </div>
        </li>
        <li class="placeload">

            <div class="placeholder-footer">
                <div class="footer-block">
                    <div class="content-shape loads"></div>
                    <div class="content-shape loads"></div>
                </div>
            </div>
        </li>
    </ul>
</div>

<style type="text/css">
    @keyframes fadein {
        from {
            opacity: 0
        }

        to {
            opacity: 1
        }
    }

    @-moz-keyframes fadein {
        from {
            opacity: 0
        }

        to {
            opacity: 1
        }
    }

    @-webkit-keyframes fadein {
        from {
            opacity: 0
        }

        to {
            opacity: 1
        }
    }

    @-o-keyframes fadein {
        from {
            opacity: 0
        }

        to {
            opacity: 1
        }
    }

    @-webkit-keyframes placeload {
        0% {
            background-position: -468px 0;
        }

        100% {
            background-position: 468px 0;
        }
    }

    @keyframes placeload {
        0% {
            background-position: -468px 0;
        }

        100% {
            background-position: 468px 0;
        }
    }

    .placeload {
        background: #fff;
        max-width: 500px;
        width: 20%;
        padding: 10px;
        border-radius: 3px;
        position: relative;
        float: left
    }


        .placeload .image-placeholder {
            width: 100%;
            height: 165px;
        }

        .placeload .placeholder-footer {
            position: relative;
            margin-top: 20px;
            width: 100%;
            display: flex;
            justify-content: space-between;
            align-items: center;
        }

            .placeload .placeholder-footer .footer-block {
                width: 100%;
                height: 100%;
                min-height: 20px;
            }

                .placeload .placeholder-footer .footer-block .content-shape {
                    height: 12px;
                    margin-bottom: 6px;
                }

                    .placeload .placeholder-footer .footer-block .content-shape:first-child {
                        width: 68%;
                    }

                    .placeload .placeholder-footer .footer-block .content-shape:nth-child(2) {
                        width: 45%;
                    }

                    .placeload .placeholder-footer .footer-block .content-shape:nth-child(3) {
                        width: 22%;
                    }

    .loads {
        -webkit-animation-duration: 1s;
        animation-duration: 1s;
        -webkit-animation-fill-mode: forwards;
        animation-fill-mode: forwards;
        -webkit-animation-iteration-count: infinite;
        animation-iteration-count: infinite;
        -webkit-animation-name: placeload;
        animation-name: placeload;
        -webkit-animation-timing-function: linear;
        animation-timing-function: linear;
        background: #f6f7f8;
        background: #eee;
        background: -webkit-gradient(linear, left top, right top, color-stop(8%, #eee), color-stop(18%, #ddd), color-stop(33%, #eee));
        background: -webkit-linear-gradient(left, #eee 8%, #ddd 18%, #eee 33%);
        background: linear-gradient(to right, #eee 8%, #ddd 18%, #eee 33%);
        -webkit-background-size: 800px 104px;
        background-size: 1200px 104px;
        position: relative;
    }
</style>
