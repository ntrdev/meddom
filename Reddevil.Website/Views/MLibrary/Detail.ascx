﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.ViewControl" %>

<% 
    var item = ViewBag.Data as ModLibraryEntity;
    var listItem = ViewBag.Other as List<ModLibraryEntity>;
%>

<div class="box_body_content">
    <div class="box_title_post">
        <h4><%=item.Name %></h4>
        <div class="time_news">
            <i class="fa fa-clock-o" aria-hidden="true"></i>
            <span><%=Utils.FormatDateTime(item.DateCreated) %></span>
        </div>
    </div>


    <%=Utils.GetHtmlForSeo(item.Description) %>
    <div class="entry-content single-page">
        <%if (!string.IsNullOrEmpty(item.File) && item.File.EndsWith(".docx"))
            {%>

        <div id="">
            <div id="">
                <iframe src='https://view.officeapps.live.com/op/embed.aspx?src=<%="http://meddom.org"+item.File.Replace("%20"," ") %>' width='100%' height='800px' frameborder='0'>This is an embedded <a target='_blank' href='http://office.com'>Microsoft Office</a> document, powered by <a target='_blank' href='http://office.com/webapps'>Office Online</a>.</iframe>
            </div>
        </div>
        <%} %>
    </div>
    <div class="entry-content single-page">
        <%if (!string.IsNullOrEmpty(item.File) && item.File.EndsWith(".pdf"))
            {%>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdf.js/2.0.943/pdf.min.js"> </script>

        <style>
            #canvas_container {
                width: auto;
                height: au;
                overflow: auto;
            }

            #canvas_container {
                background: #333;
                text-align: center;
                border: 1px solid rgb(0, 148, 255);
            }

            .content-heading__secondary-categories-tag {
                display: inline-block;
                margin-top: 5px;
                margin-right: 4px;
                padding: 0 4px;
                color: #9b9b9b;
                border: 2px solid #e4e4e4;
                border-radius: 6px;
                white-space: nowrap;
            }
        </style>
        <div id="my_pdf_viewer">
            <div id="navigation_controls" style="width: 100%; text-align: center">
                <a class="content-heading__secondary-categories-tag" data-toggle="tooltip" data-placement="bottom" title="Quay lại" id="go_previous"><i class="fa fa-backward"></i></a>
                <a class="content-heading__secondary-categories-tag" data-toggle="tooltip" data-placement="bottom" title="Zoom +" id="zoom_in"><i class="fa fa-search-plus"></i></a>
                <input id="current_page" hidden value="1" type="number" />
                <a class="content-heading__secondary-categories-tag" data-toggle="tooltip" data-placement="bottom" title="Zoom -" id="zoom_out"><i class="fa fa-search-minus"></i></a>
                <a class="content-heading__secondary-categories-tag " data-toggle="tooltip" data-placement="bottom" title="Trang tiếp theo" id="go_next"><i class="fa fa-forward"></i></a>
                <a class="content-heading__secondary-categories-tag" data-toggle="tooltip" data-placement="bottom" title="In tài liệu" id="print"><i class="fa fa-print"></i></a>
            </div>
            <div id="canvas_container">
                <canvas id="pdf_renderer"></canvas>
            </div>

        </div>

        <script>
            var myState = {
                pdf: null,
                currentPage: 1,
                zoom: 1
            }

            pdfjsLib.getDocument('<%= item.File%>').then((pdf) => {
                myState.pdf = pdf;
                render();
            });

            function render() {
                myState.pdf.getPage(myState.currentPage).then((page) => {
                    var canvas = document.getElementById("pdf_renderer");
                    var ctx = canvas.getContext('2d');

                    var viewport = page.getViewport(myState.zoom);

                    canvas.width = viewport.width;
                    canvas.height = viewport.height;

                    page.render({
                        canvasContext: ctx,
                        viewport: viewport
                    });
                });
            }

            document.getElementById('go_previous')
                .addEventListener('click', (e) => {
                    if (myState.pdf == null || myState.currentPage == 1)
                        return;

                    myState.currentPage -= 1;
                    document.getElementById("current_page").value = myState.currentPage;
                    render();
                });

            document.getElementById('go_next')
                .addEventListener('click', (e) => {
                    if (myState.pdf == null || myState.currentPage > myState.pdf._pdfInfo.numPages)
                        return;

                    myState.currentPage += 1;
                    document.getElementById("current_page").value = myState.currentPage;
                    render();
                });

            document.getElementById('current_page')
                .addEventListener('keypress', (e) => {
                    if (myState.pdf == null) return;
                    // Get key code
                    var code = (e.keyCode ? e.keyCode : e.which);
                    // If key code matches that of the Enter key
                    if (code == 13) {
                        var desiredPage = document.getElementById('current_page').valueAsNumber;

                        if (desiredPage >= 1 && desiredPage <= myState.pdf._pdfInfo.numPages) {
                            myState.currentPage = desiredPage;
                            document.getElementById("current_page").value = desiredPage;
                            render();
                        }
                    }
                });

            document.getElementById('zoom_in')
                .addEventListener('click', (e) => {
                    if (myState.pdf == null) return;
                    myState.zoom += 0.3;

                    render();
                });

            document.getElementById('zoom_out')
                .addEventListener('click', (e) => {
                    if (myState.pdf == null) return;
                    myState.zoom -= 0.3;
                    render();
                });
            document.getElementById('print')
                .addEventListener('click', (e) => {
                    if (myState.pdf == null) return;

                    window.open('<%= item.File.Replace("~/", "/")%>', '_blank').print();


                });
        </script>
        <%} %>
    </div>

    <div class="box_like_share">
        <div class="fb-like" data-href="<%=ViewPage.GetURL(item.CategoryID, item.Url) %>" data-width="" data-layout="button_count" data-action="like" data-size="large" data-share="true"></div>
    </div>

    <% if (listItem != null)
        { %>
    <div class="box_list_news_same">
        <h2>Tài liệu liên quan</h2>
        <ul class="list_news_same owl-carousel owl-theme">
            <%for (int i = 0; listItem != null && i < listItem.Count; i++)
                { %>
            <li class="item_news_same">
                <div class="box_img">
                    <a href="<%=ViewPage.GetURL(0, listItem[i].Url) %>" class="clickall"></a>
                    <img src="<%=Utils.DefaultImage(listItem[i].File) %>" height="auto" width="100%" alt="">
                    <div class="time_news">
                        <span><%=Utils.FomartTime(listItem[i].DateCreated) %></span>- <span><%=Utils.FormatDate(listItem[i].DateCreated) %></span>
                    </div>
                </div>
                <div class="box_info">
                    <h4><a href="<%=ViewPage.GetURL(0, listItem[i].Url) %>"><%=listItem[i].Name %></a></h4>
                </div>
            </li>
            <%} %>
        </ul>
    </div>
    <%} %>
</div>
