﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.ViewControl" %>

<%
    var listItem = ViewBag.Data as List<ModLibraryEntity>;
    var listView = ViewBag.Views as List<ModLibraryEntity>;
    var model = ViewBag.Model as MLibraryModel;

%>

<link rel="stylesheet" type="text/css" href="/interface/pc/css/main_phase2.css">

<div class="mid_content ">
    <div class="hl_content">
        <div class="sidebar-title">
            <div class="title">
                <span>Tài liệu nổi bật</span>
            </div>
        </div>
        <div class="featured-thumbnails">
            <div class="container">
                <div class="row">
                    <%for (int i = 0; listView != null && i < listView.Count; i++)
                        {%>
                    <div class="col-sm-3 col-6">
                        <div class="featured-thumbnail">
                            <div class="thumbnail hm-reponsive">
                                <a href="<%=ViewPage.GetURL(listView[i].CategoryID, listView[i].Url) %>" title="<%=listView[i].Name %>">
                                    <img src="<%=Utils.OptimalImage(listView[i].Image) %>" title="<%=listView[i].Name %>" class="img-responsive img-hover">
                                </a>
                            </div>
                            <div class="title_hl_content"><a style="color: #278abd;" class="cl_hdoc" title="<%=listView[i].Name %>" href="<%=ViewPage.GetURL(listView[i].CategoryID, listView[i].Url) %>"><%=listView[i].Name %></a></div>
                        </div>
                    </div>
                    <%} %>
                </div>
            </div>
        </div>
    </div>

    <div class="type_content ">
        <div class="content_box_common">
            <div class="col-12 col-lg-8 ">
                <div class="tab">
                    <a class="tablinks" onclick="filter_product('<%=ViewPage.CurrentURL %>?sort=new_asc','.list_news','');"><i class="fa fa-filter"></i>&nbsp;Mới nhất</a>
                    <a class="tablinks" onclick="filter_product('<%=ViewPage.CurrentURL %>?sort=view_desc','.list_news','');"><i class="fa fa-filter"></i>&nbsp;Xem nhiều nhất</a>
                    <a class="tablinks" onclick="filter_product('<%=ViewPage.CurrentURL %>?sort=down_desc','.list_news','');"><i class="fa fa-filter"></i>&nbsp;Download nhiều</a>
                </div>
            </div>
            <div class="col-12 col-lg-4 box_filter_data">
                <div class="box_search_khoahoc">
                    <input type="text" value="" placeholder="Tìm kiếm" onchange="filter_product('<%=ViewPage.CurrentURL %>?sort=down_desc','.list_news',this.value);" id="key">
                    <button><i class="fa fa-search" aria-hidden="true"></i></button>
                </div>
            </div>
        </div>
        <div class="list-news-other">
            <ul class="list_news all mt10">
                <%for (int i = 0; listItem != null && i < listItem.Count; i++)
                    {%>
                <li class="item clear">
                    <div class="blog-item">
                        <div class="blog-item-img">
                            <div class="hm-reponsive">
                                <a href="<%=ViewPage.GetURL(listItem[i].CategoryID, listItem[i].Url) %>" title="<%=listItem[i].Name %>">
                                    <img alt="<%=listItem[i].Name %>" src="<%=Utils.OptimalImage(listItem[i].Image) %>"></a>
                            </div>
                        </div>
                        <div class="blog-item-title">
                            <h3 class="title-blog">
                                <a href="<%=ViewPage.GetURL(listItem[i].CategoryID, listItem[i].Url) %>" title="<%=listItem[i].Name %>"><%=listItem[i].Name %></a>
                            </h3>
                            <p class="blog-item-created">
                                <a href="<%=ViewPage.GetURL(listItem[i].CategoryID, listItem[i].Url) %>" title="<%=listItem[i].Name %>"><%=listItem[i].CPUser %></a> | <%=Utils.FormatDateTime(listItem[i].DateCreated) %>
                            </p>
                            <p class="description">
                                <%=listItem[i].Description %>
                            </p>
                            <span class="sprite icoview"><i class="fa fa-eye" aria-hidden="true"></i>&nbsp;</span><span class="txt_note fleft"><%=listItem[i].Views %></span>&ensp;
                                                    <span class="sprite icodown"><i class="fa fa-download" aria-hidden="true"></i>&nbsp;</span><span class="txt_note fleft "><%=listItem[i].Download %></span>&ensp;
                                                    <button class="btn_orange_sm btndown" onclick="down_load('<%=listItem[i].Url%>','<%=listItem[i].File %>','<%=WebLogin.IsLogin() %>')"><i class="fa fa-download"></i>&nbsp;Download</button>
                        </div>
                    </div>
                </li>
                <%} %>
            </ul>
            <ul class="pagination">
                <%= GetPagination("",model.page, model.PageSize, model.TotalRecord)%>
            </ul>
        </div>

    </div>
</div>
 <div class="loading" style="display: none;"><i class="icon">Loading</i></div>
