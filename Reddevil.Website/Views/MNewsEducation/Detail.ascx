﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.ViewControl" %>

<% 
    var item = ViewBag.Data as ModNewsEducationEntity;
    var listItem = ViewBag.Other as List<ModNewsEducationEntity>;
    var listLQ = ViewBag.Lienquan as List<ModNewsEducationEntity>;

%>

<div class="box_body_content">
    <div class="box_title_post">
        <h4><%=item.Name %></h4>
        <div class="time_news">
            <i class="fa fa-clock-o" aria-hidden="true"></i>
            <span><%=Utils.FormatDateTime(item.Updated) %></span>
        </div>
    </div>
    <%if (!string.IsNullOrEmpty(item.Summary)){ %> <p class="desc"><%=!item.Cost ? "<img src=\"/interface/pc/images/favicon.png\" alt=\"tin từ MEDDOM\" style=\" width:30px;height:30px;\"/>" : "" %> <b><%=item.Summary %></b></p><%} %>
    <%=Utils.GetHtmlForSeo(item.Content) %>
</div>

<div class="box_like_share">
    <div class="fb-like" data-href="<%=ViewPage.GetURL(item.MenuID, item.Url) %>" data-width="" data-layout="button_count" data-action="like" data-size="large" data-share="true"></div>
</div>
<div class="box_list_news_same">
    <h2>Tin liên quan</h2>
    <ul class="list_news_same owl-carousel owl-theme">
        <%for (int i = 0; listLQ != null && i < listLQ.Count; i++)
            { %>
        <li class="item_news_same">
            <div class="box_img">
                <a href="<%=ViewPage.GetURL(0,listLQ[i].Url) %>" class="clickall"></a>
                <img src="<%=Utils.OptimalImage(listLQ[i].File) %>" height="auto" width="100%" alt="">
                <div class="time_news">
                    <span><%=Utils.FomartTime(listLQ[i].Updated) %></span>- <span><%=Utils.FormatDate(listLQ[i].Updated) %></span>
                </div>
            </div>
            <div class="box_info">
                <h4><a href="<%=ViewPage.GetURL(0,listLQ[i].Url) %>"><%=listLQ[i].Name %></a></h4>
            </div>
        </li>
        <%} %>
    </ul>
</div>
 <%if (listItem != null)
        { %>
<div class="box_list_news_same">
    <h2>Tin khác</h2>
    <ul class="list_news_same owl-carousel owl-theme">
        <%for (int i = 0; listItem != null && i < listItem.Count; i++)
            { %>
        <li class="item_news_same">
            <div class="box_img">
                <a href="<%=ViewPage.GetURL(0,listItem[i].Url) %>" class="clickall"></a>
                <img src="<%=Utils.OptimalImage(listItem[i].File) %>" height="auto" width="100%" alt="">
                <div class="time_news">
                    <span><%=Utils.FomartTime(listItem[i].Updated) %></span>- <span><%=Utils.FormatDate(listItem[i].Updated) %></span>
                </div>
            </div>
            <div class="box_info">
                <h4><a href="<%=ViewPage.GetURL(0,listItem[i].Url) %>"><%=listItem[i].Name %></a></h4>
            </div>
        </li>
        <%} %>
    </ul>
</div>
  <%} %>