﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.ViewControl" %>

<%
    var listItem = ViewBag.Data as List<ModNewsEducationEntity>;
    var model = ViewBag.Model as MNewsEducationModel;
    if (listItem == null) return;

%>

<div class="box_body_content">

    <div class="itembox_news">

        <ul class="list_item_o">
            <%for (int i = 0; i < listItem.Count; i++)
                {%>
            <li class="item_news_o">
                <div class="box_img">
                    <a href="<%=ViewPage.GetURL(0, listItem[i].Url) %>" class="clickall"></a>
                    <img src="<%=Utils.OptimalImage(listItem[i].File) %>" alt="">
                </div>
                <div class="box_info">
                    <h4><a href="<%=ViewPage.GetURL(0, listItem[i].Url) %>"><%=listItem[i].Name %></a></h4>
                    <div class="date_time">
                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                        <span><%=Utils.FormatDateTime(listItem[i].Updated) %>
                        </span>
                    </div>
                </div>
            </li>
            <%} %>
        </ul>

        <ul class="pagination">
               <%= GetPagination("",model.page, model.PageSize, model.TotalRecord)%>
        </ul>
    </div>



</div>
