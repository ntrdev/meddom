﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.ViewControl" %>

<%
    var listItem = ViewBag.Data as List<ModNewsEducationEntity>;
    var model = ViewBag.Model as MNewsEducationModel;
    var listPage = ViewBag.ListPage as List<SysPageEntity>;
    if (listPage == null || listPage.Count < 1) listPage = SysPageService.Instance.GetByParent_Cache(ViewPage.CurrentPage.ParentID);
%>
<div class="box_headding_news">
    <div class="box_link_tab_news">

        <ul class="nav nav-tabs" role="tablist">
            <%for (int i = 0; listPage != null && i < listPage.Count; i++)
                {%>
            <li class="nav-item">
                <a class="nav-link " href="<%=ViewPage.GetPageURL(listPage[i]) %>"><%=listPage[i].Name %></a>
            </li>
            <%}%>
        </ul>
    </div>
</div>
<div class="box_body_content">

    <ul class="list_news">
        <%for (int i = 0; listItem != null && i < listItem.Count; i++)
            {%>
        <li class="item_news">
            <div class="box_left_img">
                <div class="box_img">
                    <a href="<%=ViewPage.GetURL(0, listItem[i].Url) %>" class="clickall"></a>
                    <img src="<%=Utils.OptimalImage(listItem[i].File) %>" height="92" width="77" alt="">
                    <div class="time_news">
                        <span><%=Utils.FomartTime(listItem[i].Updated) %> </span>- <span><%=Utils.FormatDate(listItem[i].Updated) %></span>
                    </div>
                </div>
            </div>
            <div class="box_text_r">
                <h4><a href="<%=ViewPage.GetURL(0, listItem[i].Url) %>"><%=listItem[i].Name %></a></h4>
                <p class="desc"><%=listItem[i].Summary %></p>
            </div>
        </li>
        <%} %>
    </ul>
    <ul class="pagination">
           <%= GetPagination("",model.page, model.PageSize, model.TotalRecord)%>
    </ul>
</div>

