﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.ViewControl" %>

<%
    var listItem = ViewBag.Data as List<ModNewsEntity>;
    var model = ViewBag.Model as MNewsModel;

    var listPage = SysPageService.Instance.GetByParent_Cache(ViewPage.CurrentPage.ID);
    if (listPage.Count < 1)
        listPage = SysPageService.Instance.GetByParent_Cache(ViewPage.CurrentPage.ParentID);
%>

<section class="pageNew">
    <div class="container">
        <div class="topicNews">
            <%for (var i = 0; listPage != null && i < listPage.Count; i++){%>
            <a href="<%=ViewPage.GetPageURL(listPage[i]) %>" title="<%=listPage[i].Name %>"><%=listPage[i].Name %></a>
            <%} %>
        </div>
    </div>

    <div class="container chuyenmucrv ctgrNew mb20 ">
        <ul class="listNewCtgr all">
            <%for (var i = 0; listItem != null && i < listItem.Count; i++){
                    string url = ViewPage.GetURL(listItem[i].Code);
            %>
            <li>
                <div class="item">
                    <div class="thumbimg">
                        <div class="hm-responsive">
                            <a href="<%=url %>" title="<%=listItem[i].Name %>">
                                <img src="<%=Utils.GetResizeFile(listItem[i].File, 4, 200, 200) %>" alt="<%=listItem[i].Name %>" />
                            </a>
                        </div>
                    </div>
                    <h3 class="namenew">
                        <a href="<%=url %>" title="<%=listItem[i].Name %>"><%=listItem[i].Name %></a>
                    </h3>
                </div>
            </li>
            <%} %>
        </ul>

        <div class="pagination">
            <a href="javascript:void(0)" data-url="<%=ViewPage.CurrentURL %>" rel="nofollow">Xem thêm</a>
            <div class="loading"><i class="icon">Loading</i></div>
        </div>
    </div>
    <div class="clearfix"></div>
</section>