﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.ViewControl" %>

<%
    var item = ViewBag.Data as ModUserEntity;
    var model = ViewBag.Model as MRegisterModel;
%>

<section class="main-cate  mb20">
    <div class="container">

        <div class="box-dndk">
            <ul class="tab-dkdn justify-content-end ">
                <li role="presentation" class="active">
                    <a href="#tab-div2" aria-controls="tab" role="tab" data-toggle="tab">Tạo tài khoản</a>
                </li>
            </ul>
            <div class="tab-content-dkdn">
                <form class="login-form" method="post" enctype="multipart/form-data">
                    <div role="tabpanel" class="tab-pane active" id="tab-div2">
                        <div class="d-flex-wrap">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-5 info-dkdn" style="padding: 20px">
                                <div class="tile-dndk">Đăng ký</div>
                                <p>
                                    Đăng ký để truy cập thông tin, thực hiện và dùng được nhiều tính năng khác
                                                trên website.
                                </p>

                                <div class="d-flex-wrap justify-content-center mt90" style="text-align: center; margin-top: 10px">
                                    <div class="list_upload_image">
                                        <div class="fileinput fileinput-new list_upload_image" data-provides="fileinput">
                                            <div class="fileinput-new ">
                                                <div class="item-upload">
                                                    <div class="avatar-uploader_avatar-image avatar-uploader_avatar" style="height: 150px; width: 150px">
                                                        <input type="file" class="avatar-uploader_file-input file_view" name="upload-logo" onchange="previewFile('logo')" accept="image/*">
                                                        <span data-v-7f82623c="" onclick="deleteFile('logo')" class="shopee-image-manager__icon shopee-image-manager__icon--delete">
                                                            <i data-v-7f82623c="" class="shopee-icon">
                                                                <svg viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
                                                                    <g fill-rule="nonzero">
                                                                        <path d="M14.516 3.016h-4v-1a.998.998 0 0 0-.703-.955.99.99 0 0 0-.297-.045h-3a.998.998 0 0 0-.955.703.99.99 0 0 0-.045.297v1h-4a.5.5 0 1 0 0 1h1v10a.998.998 0 0 0 .703.955.99.99 0 0 0 .297.045h9a.998.998 0 0 0 .955-.703.99.99 0 0 0 .045-.297v-10h1a.5.5 0 1 0 0-1zm-8-1h3v1h-3v-1zm6 12h-9v-10h9v10z"></path>
                                                                        <path d="M5.516 12.016a.5.5 0 0 0 .5-.5v-4a.5.5 0 1 0-1 0v4a.5.5 0 0 0 .5.5zM8.016 12.016a.5.5 0 0 0 .5-.5v-5a.5.5 0 1 0-1 0v5a.5.5 0 0 0 .5.5zM10.516 12.016a.5.5 0 0 0 .5-.5v-4a.5.5 0 1 0-1 0v4a.5.5 0 0 0 .5.5z"></path>
                                                                    </g>
                                                                </svg>
                                                            </i>
                                                        </span>
                                                        <img src="" id="logo" style="width: 250px; height: 250px" alt="Đại diện ">
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-12 col-lg-7">
                                <ul class="form-list all" style="padding: 20px">
                                    <li class="clear">
                                        <div class="table_left">
                                            <label>Tên tài khoản<span class="required">*</span></label>
                                        </div>
                                        <div class="table_right">
                                            <input class="form-control loginname_changer" onchange="check_acc(this.value,'LoginName');" type="text" placeholder="Tên truy cập (là tên tài khoản để truy cập vd: tentaikhoan_xxx)" required="required" value="<%=item.LoginName %>" name="LoginName" />
                                            <p id="messenger" class="error"></p>
                                        </div>
                                    </li>
                                    <li class="clear">
                                        <div class="table_left">
                                            <label>Mật Khẩu<span class="required">*</span></label>
                                        </div>
                                        <div class="table_right">
                                            <input class="form-control" name="Password" value="<%=item.Password %>" id="Password" required onchange="check_acc(this.value,'Pass');" type="password" placeholder="mật khẩu bao gồm ký tự viết Hoa và ký tự là số, độ dài hơn 8 ký tự">
                                            <p id="Pass" class="error"></p>
                                        </div>
                                    </li>
                                    <li class="clear">
                                        <div class="table_left">
                                            <label>Nhập lại Mật Khẩu<span class="required">*</span></label>
                                        </div>
                                        <div class="table_right">
                                            <input class="form-control" placeholder="Xác nhận lại mật khẩu" value="<%=model.Password2 %>" name="Password2" id="Password1" onchange="check_pass(this.value,$('#Password').val()); return false;" required type="password">
                                            <p id="Password2" class="error"></p>
                                        </div>
                                    </li>


                                    <li class="active ct" style="border-bottom: 1px solid #ddd; margin: 20px 0; font-size: 16px; text-align: center; font-weight: 600; color: #4870a5;" id="title">Thông tin cá nhân</li>
                                     <li class="clear" id="gender">
                                            <div class="table_left">
                                                <label>
                                                    Giới tính <span
                                                        class="required">*</span></label>
                                            </div>
                                            <div class="table_right ">
                                                <div class="radio-list">
                                                    <label class="radioPure radio-inline">
                                                        <input id="radio5" type="radio" name="Gender" value="1" checked=""><span class="outer"><span class="inner"></span></span><i>Nam</i></label>
                                                    <label class="radioPure radio-inline">
                                                        <input id="radio5" type="radio" name="Gender" value="0"><span class="outer"><span class="inner"></span></span><i>Nữ</i></label>
                                                </div>
                                            </div>
                                        </li>
                                    <li class="clear">
                                        <div class="table_left">
                                            <label>Họ và tên <span class="required">*</span></label>
                                        </div>
                                        <div class="table_right">
                                            <input class="form-control name_full" type="text" placeholder="Nhập họ và tên" required="required" value="<%=item.Name %>" name="Name" />
                                            <p id="name" class="error"></p>
                                        </div>
                                    </li>
                                    <li class="clear">
                                        <div class="table_left">
                                            <label>Số điện thoại <span class="required">*</span></label>
                                        </div>
                                        <div class="table_right">
                                            <input class="form-control phone" type="text" placeholder="Số điện thoại đang sử dụng" data-id="1" name="Phone" value="<%=item.Phone %>" required />
                                            <p id="phone" class="error"></p>
                                        </div>
                                    </li>
                                    <li class="clear">
                                        <div class="table_left">
                                            <label>Email <span class="required">*</span></label>
                                        </div>
                                        <div class="table_right">
                                            <input class="form-control " type="text" autocomplete="off" placeholder="Địa chỉ Email" onchange="check_acc(this.value,'email'); return false;" name="Email" value="<%=item.Email %>" required />
                                            <p id="email" class="error"></p>
                                        </div>
                                    </li>
                                  
                                    <li class="clear">
                                        <div class="table_left">
                                            <label>Mã bảo mật <span class="required">*</span></label>
                                        </div>
                                        <div class="table_right">
                                            <div class="col-xs-6">
                                                <p>
                                                    <img src="/ajax/Security<%=Setting.Sys_PageExt %>" class="vAlignMiddle" id="imgValidCode" alt="security code" />
                                                    <a href="javascript:void(0)" onclick="change_captcha()" title="Tạo mã khác" rel="nofollow">
                                                        <img src="/interface/pc/images/icon-refreh.png" alt="refresh security code" />
                                                    </a>
                                                </p>
                                            </div>
                                            <div class="col-xs-6">
                                                <input type="text" class="form-control" name="ValidCode" id="ValidCode" required="required" autocomplete="off" value="" size="40" placeholder="Nhập mã bảo mật">
                                            </div>

                                        </div>
                                    </li>

                                    <li class="clear">
                                        <div class="table_left"></div>
                                        <div class="table_right">
                                            <button type="submit" class="btn btn-bluestyle" style="background: #0960af; color: #fff" name="_reddevil_action[AddPOST]">Tạo tài khoản</button>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</section>
<script type="text/javascript">
    function change_type1(va) {
        if (va == 1) {
            $('#congty').slideDown();
            $('#canhan2').slideUp();
            $('#title').text('Thông tin người đại diện công ty');

        } else {
            $('#title').text('Thông tin tài khoản');
            $('#congty').slideUp();
            $('#canhan2').slideDown();
        }
    }
</script>
