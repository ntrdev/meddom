﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>
<div class="box-pro all">
    <div class="title_01 title_02">
        <h2><a href="javascript:void(0)"><%=ViewBag.Title %></a></h2>
    </div>
    <div class="bg-described clear">
        <%=ViewBag.Text %>
    </div>
</div>
