﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.ViewControl" %>

<% 
    var _page = ViewBag.Page as SysPageEntity ?? new SysPageEntity();
    var listPage = ViewBag.ListPage as List<SysPageEntity>;
%>


<section class="sec_2">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="box_headding">
                    <h2><a href="<%=ViewPage.GetURL(0, _page.Url) %>"><%=_page.Name %></a></h2>
                </div>
                <div class="featured-thumbnails">
                    <div class="container">
                        <div class="row">
                            <%for (int i = 0; listPage != null && i < (listPage.Count > 3 ? 3 : listPage.Count); i++)
                                {
                                    var news = ModNewsGarnitureService.Instance.CreateQuery()
                                                                            .Where(o => o.Activity == true && (o.State & 1) == 1)
                                                                            .WhereIn(o => o.MenuID, WebMenuService.Instance.GetChildIDForWeb_Cache("NewsGarniture", listPage[i].MenuID, ViewPage.CurrentLang.ID))
                                                                            .OrderByDesc(o => o.ID)
                                                                            .ToSingle_Cache();
                                    if (news == null) continue;
                                    %>
                            <div class="col-sm-<%=i==0?"6":"3" %> col-xs-12">
                                <div class="featured-thumbnail">
                                    <div class="thumbnail">
                                        <a href="<%=ViewPage.GetURL(news.MenuID, news.Url) %>" title="<%=news.Name %>">
                                            <img src="<%=Utils.OptimalImage(news.File) %>" alt="<%=news.Name %>" class="img-responsive img-hover">
                                        </a>
                                    </div>
                                    <h5><a href="<%=ViewPage.GetPageURL(listPage[i]) %>" title="<%=listPage[i].Name %>"><%=listPage[i].Name %></a></h5>
                                </div>
                            </div>
                            <%} %>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
