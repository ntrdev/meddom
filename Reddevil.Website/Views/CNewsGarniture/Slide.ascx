﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<% var listItem = ViewBag.Data as List<ModAdvEntity>;%>

<div class="container slidetop">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div id="slide_main" class="owl-carousel">
                <%for (int i = 0; listItem != null && i < listItem.Count; i++){%>
                <div class="item">
                    <%= Utils.GetCodeAdv(listItem[i])%>
                </div>
                <%} %>
            </div>
        </div>
    </div>
</div>