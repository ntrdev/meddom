﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<% var listItem = ViewBag.Data as List<ModAdvEntity>;%>

<div class="back-to-top"><a href="javascript:void(0)" rel="nofollow"><span class="fa fa-angle-up fa-2x"></span></a></div>
<div class="float-ck">
    <div id="hide_float_left"><a href="javascript:hide_float_left()">Tắt quảng cáo [X]</a></div>
    <div id="float_content_left">
        <%for (int i = 0; listItem != null && i < listItem.Count; i++){%>
        <%= Utils.GetCodeAdv(listItem[i])%>
        <%} %>
    </div>
</div>

<div class="copyright">
    <strong>Cityplaza © 2017 | Một sản phẩm của Phidias International<br />Phiên bản mới</strong>
</div>
