﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.ViewControl" %>

<%
    var listItem = ViewBag.Data as List<ModNewsEntity>;
    var model = ViewBag.Model as MNewsModel;
    if (listItem == null) return;

%>
<link type="text/css" href="<%=Static.Tag("/interface/utils/fancybox/jquery.fancybox.css") %>" rel="stylesheet" />
<div class="box_body_content">

    <ul class="list_3imgs row">
        <%for (int i = 0; i < listItem.Count; i++)
            {
                var listFile = listItem[i].GetFile;
        %>
        <li class="col-md-6 ">
            <div class=" box_3img owl-carousel owl-theme">
                <div class="item">
                    <div class="box_img">
                        <a href="<%=Utils.DefaultImage(listItem[i].File) %>" class="fancybox" data-fancybox="preview">
                            <img src="<%=Utils.DefaultImage(listItem[i].File) %>" alt="<%=listItem[i].Name %>" data-bigimg="<%=Utils.DefaultImage(listItem[i].File) %>">
                        </a>

                    </div>
                </div>
                <%for (int j = 0; listFile != null && j < listFile.Count; j++)
                    {%>
                <div class="item">
                    <div class="box_img">
                        <a href="<%=Utils.DefaultImage(listFile[j].File) %>" class="fancybox" data-fancybox="preview">
                            <img src="<%=Utils.DefaultImage(listFile[j].File) %>" alt="<%=listItem[i].Name %>" data-bigimg="<%=Utils.DefaultImage(listFile[j].File) %>">
                        </a>
                    </div>
                </div>
                <%} %>
            </div>


            <h4><%=listItem[i].Name %></h4>
        </li>
        <%} %>
    </ul>

    <ul class="pagination">
        <%= GetPagination(model.page, model.PageSize, model.TotalRecord)%>
    </ul>

</div>
<script type="text/javascript" src="<%=Static.Tag("/interface/utils/fancybox/jquery.fancybox.js") %>"></script>
<script type="text/javascript">


    $(document).ready(function () {
        $(".fancybox").fancybox({
            animationEffect: "zoom-in-out",
            transitionEffect: "tube"
        });
    }
</script>
