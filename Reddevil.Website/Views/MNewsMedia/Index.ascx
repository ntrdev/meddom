﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.ViewControl" %>

<%
    var listItem = ViewBag.Data as List<ModNewsMediaEntity>;
    var listKhoahoc = ViewBag.KhoaHoc as List<ModNewsVideoEntity>;
    var listVideo = ViewBag.Video as List<ModNewsVideoEntity>;
    var model = ViewBag.Model as MNewsMediaModel;


%>

<link type="text/css" href="<%=Static.Tag("/interface/utils/fancybox/jquery.fancybox.css") %>" rel="stylesheet" />


<div class="box_body_content">

    <div class="box_tab_media">
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" href="#hoso-khoahoc" role="tab" data-toggle="tab">Phim về nhà khoa học</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#img_nhakhoahoc" role="tab" data-toggle="tab">Thư viện ảnh</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#kyucc" role="tab" data-toggle="tab">Thư viện Video</a>
            </li>
        </ul>
    </div>

    <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade in active show" id="hoso-khoahoc">
            <ul class="list_videos_news">
                <%for (int i = 0; listKhoahoc != null && i < listKhoahoc.Count; i++)
                    {%>
                <li class="item_video">
                    <div class="box_img">

                        <img src="<%=listKhoahoc[i].Thumbnail %>" alt="">
                        <div class="icon_play">
                            <a href="<%=ViewPage.GetURL(listKhoahoc[i].MenuID, listKhoahoc[i].Url) %>"><i class="fa fa-play-circle" aria-hidden="true"></i></a>
                        </div>
                    </div>
                    <div class="box_info">
                        <h4><a href="<%=ViewPage.GetURL(listKhoahoc[i].MenuID, listKhoahoc[i].Url) %>"><%=listKhoahoc[i].Name %></a></h4>
                    </div>
                </li>
                <%} %>
            </ul>
            <ul class="pagination">
                <%= GetPagination("",model.page, model.PageSize, model.TotalRecord)%>
            </ul>
        </div>

        <div role="tabpanel" class="tab-pane fade" id="img_nhakhoahoc">

            <div class="itembox_news">
                <ul class="list_item_o list_3_item">
                    <%for (int i = 0; listItem != null && i < listItem.Count; i++)
                        { %>
                    <li class="item_news_o">
                        <div class="box_img">
                            <a href="<%=Utils.OptimalImage(listItem[i].File) %>" data-fancybox="preview" class="fancybox" data-img="<%=Utils.OptimalImage(listItem[i].File) %>" data-bigimg="<%=Utils.OptimalImage(listItem[i].File) %>">
                                <img src="<%=Utils.OptimalImage(listItem[i].File) %>" alt="<%=listItem[i].Name %>"></a>
                        </div>
                        <div class="box_info">
                            <h4><a href="<%=Utils.OptimalImage(listItem[i].File) %>" data-fancybox="preview" class="fancybox"><%=listItem[i].Name %></a></h4>
                        </div>
                    </li>
                    <%} %>
                </ul>

                <ul class="pagination">
                    <%= GetPagination("",model.page, model.PageSize, model.TotalRecord)%>
                </ul>
            </div>

        </div>
        <div role="tabpanel" class="tab-pane fade " id="kyucc">

            <div class="itembox_news">
                <ul class="list_videos_news">
                    <%for (int i = 0; listVideo != null && i < listVideo.Count; i++)
                        {%>
                    <li class="item_video">
                        <div class="box_img">

                            <img src="<%=listVideo[i].Thumbnail %>" alt="">
                            <div class="icon_play">
                                <a href="<%=ViewPage.GetURL(listVideo[i].MenuID, listVideo[i].Url) %>"><i class="fa fa-play-circle" aria-hidden="true"></i></a>
                            </div>
                        </div>
                        <div class="box_info">
                            <h4><a href="<%=ViewPage.GetURL(listVideo[i].MenuID, listVideo[i].Url) %>"><%=listVideo[i].Name %></a></h4>
                        </div>
                    </li>
                    <%} %>
                </ul>

                <ul class="pagination">
                    <%= GetPagination("",model.page, model.PageSize, model.TotalRecord)%>
                </ul>
            </div>

        </div>

    </div>
</div>
<script type="text/javascript" src="<%=Static.Tag("/interface/utils/fancybox/jquery.fancybox.js") %>"></script>
<script type="text/javascript">
    $(".fancybox").fancybox({
        animationEffect: "zoom-in-out",
        transitionEffect: "tube"
    });

</script>
