﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.ViewControl" %>

<% 
    var item = ViewBag.Data as ModNewsVideoEntity;
    var listItem = ViewBag.Other as List<ModNewsVideoEntity>;

%>
<script type="text/javascript" src="<%=Static.Tag("/interface/utils/jwplayer/jwplayer.js") %>"></script>
<script type="text/javascript">jwplayer.key = "bmYhK/6mW299S1kjNJFsA7IAuEIajluMDteXkg==";</script>
<script type="text/javascript">
    $(window).on('load', function () {
        play_video('video_main', '<%=item.UrlVideo%>', '<%=item.Thumbnail%>', '100%', ($('#video_main').width() * 1 / 2), true);
    });

    function play_video(pos, file, image, width, height, auto) {
        jwplayer(pos).setup({
            file: file,
            image: image,
            abouttext: 'IMEXsoft® - 01032019',
            width: width,
            height: height,
            stretching: "exactfit",
            autostart: auto,
            logo: {
                file: '',
                link: '',
            }
        });
    }
</script>

<div class="box_body_content">
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade in active show" id="news_video">

            <div class="box_video_big">
                <div id="video_main" class="video_main"></div>
                <h4><%=item.Name %></h4>
                <%if (!string.IsNullOrEmpty(item.Summary)){ %> <p class="desc"><%=!item.Cost ? "<img src=\"/interface/pc/images/favicon.png\" alt=\"tin từ MEDDOM\" style=\" width:30px;height:30px;\"/>" : "" %> <b><%=item.Summary %></b></p><%} %>
            </div>
            <ul class="list_videos_news">
                <%for (int i = 0; i < listItem.Count; i++)
                    {%>
                <li class="item_video">
                    <div class="box_img" onclick="play_video('video_main','<%=listItem[i].UrlVideo %>','<%=listItem[i].Thumbnail %>','100%', ($('#video_main').width() *3 / 2), 'true')">

                        <img src="<%=listItem[i].Thumbnail %>" alt="">
                        <div class="icon_play">
                            <i class="fa fa-play-circle" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="box_info">
                        <h4><a href="<%=ViewPage.GetURL(listItem[i].MenuID, listItem[i].Url) %>"><%=listItem[i].Name %></a></h4>
                    </div>
                </li>
                <%} %>
            </ul>
        </div>
    </div>
</div>
