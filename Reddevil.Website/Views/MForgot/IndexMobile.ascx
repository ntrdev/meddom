﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.ViewControl" %>

<%
    var model = ViewBag.Model as MForgotModel;
%>

<div class="boxFormPayment all">
    <div class="container center">
        <form method="post" name="forgot_form" class="forgot-form">
            <div class="form-group">
                <label>Email hoặc tên đăng nhập</label>
                <input type="email" class="form-control" name="Email" required="required" value="<%=model.Email %>" placeholder="Nhập địa chỉ email hoặc tên đăng nhập" maxlength="50" />
            </div>
            <button type="submit" class="btn btn-primary" name="_reddevil_action[AddPOST]" style="margin-bottom:10px;">Nhận mật khẩu</button>
        </form>
    </div>
</div>
