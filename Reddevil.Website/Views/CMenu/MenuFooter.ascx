﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.ViewControl" %>

<% 
    var listItem = ViewBag.Data as List<SysPageEntity>;
    if (listItem == null) return;
%>

<div class="col-lg-3 col-md-6">
    <div class="box_item_ft">
        <h2 class="title_ft">Liên kết</h2>
        <ul class="list_link_ft">
            <%for (int i = 0; i < listItem.Count; i++)
                {
            %>
            <li><a href="<%=ViewPage.GetPageURL(listItem[i]) %>" title="<%=listItem[i].Name %>"><%=listItem[i].Name %></a></li>
            <%} %>
        </ul>
    </div>
</div>
