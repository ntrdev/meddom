﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.ViewControl" %>

<% 
    var listItem = ViewBag.Data as List<SysPageEntity>;
    if (listItem == null) return;
%>
<div id="header-sroll">
    <div class="desk-menu">

        <nav class="container container-menutop">
            <div class="menu-container ">
                <div class="menu-head">

                    <div class="manage-account">
                    </div>
                </div>
                <div class="menu-header-container">
                    <ul id="cd-primary-nav" class="menu">
                        <li class="menu-item">
                            <a href="/">
                                <img src="/interface/pc/images/icon/iconhome.png" alt=""></a>
                        </li>
                        <%foreach (var item in listItem)
                            {
                                var listChild = SysPageService.Instance.GetByParent_Cache(item.ID);
                        %>

                        <%if (listChild.Count > 0)
                            { %>
                        <li class="menu-item menu-item-has-children">
                            <a href="<%=item.Url.StartsWith("http")?item.Url: ViewPage.GetPageURL(item) %>"><%=item.Name %></a>

                            <ul class="sub-menu">
                                <%for (int i = 0; i < listChild.Count; i++)
                                    {
                                        var getChild = SysPageService.Instance.GetByParent_Cache(listChild[i].ID);
                                %>
                                <li class="menu-item <%=getChild.Count>0?"menu-item-has-children":"" %>">
                                    <a href="<%=listChild[i].Url.StartsWith("http")?listChild[i].Url: ViewPage.GetPageURL(listChild[i]) %>"><%=listChild[i].Name %></a>
                                    <%if (getChild.Count > 0)
                                        {  %>
                                    <ul class="sub-menu">
                                        <%for (int j = 0; j < getChild.Count; j++)
                                            { %>
                                        <li class="menu-item">
                                            <a href="<%=ViewPage.GetPageURL(getChild[j]) %>" title="<%=getChild[j].Name %>"><%=getChild[j].Name %></a>
                                        </li>
                                        <%} %>
                                    </ul>
                                    <%} %>
                                </li>
                                <%} %>
                            </ul>
                        </li>
                        <%}
                            else
                            {
                        %>
                        <li class="menu-item">
                            <a href="<%=item.Url.StartsWith("http")?item.Url: ViewPage.GetPageURL(item) %>"><i class="<%=item.Faicon %>"></i><%=item.Name %></a>
                        </li>
                        <%} %>
                        <%} %>
                    </ul>
                </div>
                {RS:Contact_MenuMobile}
            </div>
        </nav>
    </div>
</div>
