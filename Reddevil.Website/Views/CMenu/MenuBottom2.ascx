﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.ViewControl" %>

<% 
    var listItem = ViewBag.Data as List<SysPageEntity>;
%>

<%--<nav class="wh-menu">
    <div class="container">
        <div id="mainMenu">
            <ul class="navmenu">
                <%for (int i = 0; listItem != null && i < (listItem.Count > 9 ? 9 : listItem.Count); i++){
                      var listChildItem = SysPageService.Instance.GetByParent_Cache(listItem[i].ID);
                %>
                <li <%if (ViewPage.IsPageActived(listItem[i])) {%>class="active" <%} %>>
                    <a href="javascript:void(0)" title="<%=listItem[i].Name %>" rel="nofollow"><%=listItem[i].Name %></a>

                    <%if (listChildItem != null){%>
                    <ul class="submenu">
                        <li>
                            <span class="grand_title">Danh mục</span>
                            <ul class="grandmenu">
                                <%for (int j = 0; j < listChildItem.Count; j++){%>
                                <li><a href="<%=ViewPage.GetPageURL(listChildItem[j])%>" title="<%=listChildItem[j].Name%>"><%=listChildItem[j].Name%></a></li>
                                <%} %>
                            </ul>
                        </li>
                    </ul>
                    <%} %>

                </li>
                <%} %>
            </ul>
        </div>
    </div>
</nav>--%>

<ul class="mf-listmn">
    <li class="col_10_2">
        <div class="heading"><a href="">THÔNG TIN CHUNG</a> </div>
        <div class="bg-ft">
            <ul class="list-mn">
                <li><a href="">Các câu hỏi thường gặp </a></li>
                <li><a href="">Chính sách đổi trả </a></li>
                <li><a href="">Phương thức vận chuyển </a></li>
                <li><a href="">Hướng dẫn đặt hàng </a></li>
            </ul>
        </div>
    </li>
    <li class="col_10_2">
        <div class="heading"><a href="">TÀI KHOẢN CỦA BẠN</a> </div>
        <div class="bg-ft">
            <ul class="list-mn">
                <li><a href="">Xem trạng thái đơn hàng </a></li>
                <li><a href="">Quên mật khẩu </a></li>
                <li><a href="">Thông tin tài khoản </a></li>
            </ul>
        </div>
    </li>
    <li class="col_10_2">
        <div class="heading"><a href="">CHÍNH SÁCH CHUNG</a> </div>
        <div class="bg-ft">
            <ul class="list-mn">
                <li><a href="">Chính sách bảo mật </a></li>
                <li><a href="">Điều khoản sử dụng </a></li>
                <li><a href="">Chính sách bán hàng </a></li>
            </ul>
        </div>
    </li>
    <li class="col_10_2">
        <div class="heading"><a href="">VỀ CHÚNG TÔI</a> </div>
        <div class="bg-ft">
            <ul class="list-mn">
                <li><a href="">Giới thiệu chung </a></li>
                <li><a href="">Tuyển dụng nhân sự </a></li>
                <li><a href="">Sứ mệnh &amp; Tầm nhìn </a></li>
            </ul>
        </div>
    </li>
    <li class="col_10_2">
        <div class="heading"><a href="">Kết nối với chúng tôi</a> </div>
        <div class="bg-ft">
            <div class="social all mt10">
                <a class="btn btn-social-icon btn-facebook"><i class="fa fa-facebook"></i></a>
                <a class="btn btn-social-icon btn-google-plus"><i class="fa fa-google-plus"></i></a>
                <a class="btn btn-social-icon btn-twitter"><i class="fa fa-twitter"></i></a>
                <a class="btn btn-social-icon btn-youtube"><i class="fa fa-youtube"></i></a>
                <a class="btn btn-social-icon btn-pinterest"><i class="fa fa-pinterest"></i></a>
                <div class="clear"></div>
            </div>
        </div>
    </li>
</ul>
