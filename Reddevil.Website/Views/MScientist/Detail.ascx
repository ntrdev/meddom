﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.ViewControl" %>

<% 
    var item = ViewBag.Data as ModScientistEntity;
    if (item == null) return;
    var listAlbum = item.GetAlbum;
%>
<div class="box_body_content">

    <div class="box_all_detail_hoso">
        <div class="box_info_o  col-md-12">
            <div class="box_img">
                <img src="<%=Utils.OptimalImage("http://img.meddom.org/0"+item.Images) %>" alt="<%=item.FullName %>">
            </div>

            <ul class="box_text_detail_hoso">
                <li class="flex3">
                    <span>Họ tên: <b><%=item.FullName %></b></span>
                    <span>Năm sinh: <b><%=Utils.FormatDate2(item.Birthday) %></b></span>
                    <span>Giới tính: <b><%=item.Gender %></b></span>
                </li>
                <li>
                    <span>Quê quán: <b><%=item.NativeVillage %></b></span>
                </li>
                <li class="flex3">
                    <span>Chuyên ngành: <b><%=item.GetSpecialized %></b></span>
                    <span>Học hàm: <b><%=item.Degree %></b></span>
                    <span>Học vị: <b><%=item.ProfessionalKnowledge %></b></span>
                </li>
                <li>
                    <span>Danh hiệu giải thưởng, huân huy chương: <%=item.GetAward %></span>
                </li>
            </ul>

        </div>
        <div class="box_left_links ">

            <div class="box_select2">
                <select class="list_links_l" id="list_links_l">
                    <option value="">Chọn</option>
                    <optgroup label="Lý lịch khoa học" id="1">
                        <option value="TimeLine">Niên biểu cuộc đời</option>
                        <option value="WorkingHistory">Quá trình giảng dạy và đào tạo</option>
                    </optgroup>
                    <optgroup label="Luận văn, luận án" id="1">
                        <option value="GraduationPaper">Khóa luận tốt nghiệp đại học</option>
                        <option value="MAThesis">Luận văn thạc sĩ</option>
                        <option value="LATS">Luận án tiến sĩ/ tiến sĩ khoa học</option>
                    </optgroup>
                    <optgroup label="Các công trình nghiên cứu khoa học" id="2">
                        <option value="PublishedBook">Sách đã xuất bản</option>
                        <option value="CBB">Các bài báo nghiên cứu</option>
                        <option value="CDTNC">Các đề tài khoa học</option>
                    </optgroup>
                    <optgroup label="Các bài viết về nhà khoa học" id="3">
                        <option value="CBV">Các bài viết về nhà khoa học</option>
                        <option value="DMSCPD">Danh mục sách và bài viết</option>
                        <option value="DMTL">Danh mục tài liệu tại MEDDOM</option>
                        <option value="CTTK">Các thư tịch khác</option>
                    </optgroup>
                    <optgroup label="Ảnh tư liệu" id="4">
                        <option value="family">Gia đình và thời thơ ấu</option>
                        <option value="study">Thời đi học</option>
                        <option value="hoatdong">Hoạt động khoa học và xã hội</option>
                        <option value="hienvat">Các hiện vật</option>
                    </optgroup>
                    <optgroup label="Tư liệu phim" id="5">
                        <option value="Video">Video nhà khoa học</option>
                    </optgroup>
                    <optgroup label="Tư liệu phỏng vấn">
                        <option value="PVNKH">Phỏng vấn nhà khoa học</option>
                    </optgroup>
                </select>
            </div>
            <div class="itembox_links">
                <h3>Lý lịch khoa học</h3>
                <ul>
                    <li class="active"><a href="javascript:void(0)" class="get-data" data-name="1" data-value="Ext" data-id="<%=item.Id %>">Thông tin chung</a></li>
                    <li><a href="javascript:void(0)" class="get-data" data-name="1" data-value="TimeLine" data-id="<%=item.Id %>">Niên biểu cuộc đời</a></li>
                    <li><a href="javascript:void(0)" class="get-data" data-name="1" data-value="WorkingHistory" data-id="<%=item.Id %>">Quá trình giảng dạy và đào tạo</a></li>
                </ul>
            </div>
            <div class="itembox_links">
                <h3>Luận văn, luận án</h3>
                <ul>
                    <li><a href="javascript:void(0)" class="get-data" data-name="1" data-value="GraduationPaper" data-id="<%=item.Id %>">Khóa luận tốt nghiệp đại học</a></li>
                    <li><a href="javascript:void(0)" class="get-data" data-name="1" data-value="MAThesis" data-id="<%=item.Id %>">Luận văn thạc sĩ</a></li>
                    <li><a href="javascript:void(0)" class="get-data" data-name="1" data-value="LATS" data-id="<%=item.Id %>">Luận án tiến sĩ/ tiến sĩ khoa học</a></li>
                </ul>
            </div>
            <div class="itembox_links">
                <h3>Các công trình nghiên cứu khoa học</h3>
                <ul>
                    <li><a href="javascript:void(0)" class="get-data" data-name="2" data-value="PublishedBook" data-id="<%=item.Id %>">Sách đã xuất bản</a></li>
                    <li><a href="javascript:void(0)" class="get-data" data-name="2" data-value="CBB" data-id="<%=item.Id %>">Các bài báo nghiên cứu</a></li>
                    <li><a href="javascript:void(0)" class="get-data" data-name="2" data-value="CDTNC" data-id="<%=item.Id %>">Các đề tài khoa học</a></li>
                </ul>
            </div>
            <div class="itembox_links">
                <h3>Các bài viết về nhà khoa học</h3>
                <ul>
                    <li><a href="javascript:void(0)" class="get-data" data-name="3" data-value="CBV" data-id="<%=item.Id %>">Các bài viết về nhà khoa học</a></li>
                    <li><a href="javascript:void(0)" class="get-data" data-name="3" data-value="DMSCPD" data-id="<%=item.Id %>">Danh mục sách và bài viết</a></li>
                    <li><a href="javascript:void(0)" class="get-data" data-name="3" data-value="DMTL" data-id="<%=item.Id %>">Danh mục tài liệu tại MEDDOM</a></li>
                    <li><a href="javascript:void(0)" class="get-data" data-name="3" data-value="CTTK" data-id="<%=item.Id %>">Các thư tịch khác</a></li>
                </ul>
            </div>
            <div class="itembox_links">
                <h3>Ảnh tư liệu</h3>
                <ul>
                    <li><a href="javascript:void(0)" class="get-data" data-name="4" data-value="family" data-id="<%=item.Id %>">Gia đình và thời thơ ấu</a></li>
                    <li><a href="javascript:void(0)" class="get-data" data-name="4" data-value="study" data-id="<%=item.Id %>">Thời đi học</a></li>
                    <li><a href="javascript:void(0)" class="get-data" data-name="4" data-value="hoatdong" data-id="<%=item.Id %>">Hoạt động khoa học và xã hội</a></li>
                    <li><a href="javascript:void(0)" class="get-data" data-name="4" data-value="hienvat" data-id="<%=item.Id %>">Các hiện vật</a></li>
                </ul>
            </div>
            <div class="itembox_links">
                <h3>Tư liệu phim</h3>
                <ul>
                    <li><a href="javascript:void(0)" class="get-data" data-name="5" data-value="Video" data-id="<%=item.Id %>">Video nhà khoa học</a></li>
                </ul>
            </div>
            <div class="itembox_links">
                <h3>Tư liệu phỏng vấn</h3>
                <ul>
                    <li><a href="javascript:void(0)" class="get-data" data-name="6" data-value="TLPV" data-id="<%=item.Id %>">Phỏng vấn nhà khoa học</a></li>
                </ul>
            </div>
        </div>
        <div class="box_right_content_hoso" id="load_noidung">
            <%=Utils.GetHtmlForSeo(item.Ext2) %>
        </div>
    </div>
    <div class="loading" style="display: none;"><i class="icon">Loading</i></div>
</div>
<input hidden="hidden" type="hidden" value="<%=item.Id %>" id="nkhid" />
<link type="text/css" href="<%=Static.Tag("/interface/utils/fancybox/jquery.fancybox.css") %>" rel="stylesheet" />
<script type="text/javascript" src="<%=Static.Tag("/interface/utils/fancybox/jquery.fancybox.js") %>"></script>
<script type="text/javascript">
    $(".fancybox").fancybox({
        animationEffect: "zoom-in-out",
        transitionEffect: "tube"
    });

</script>