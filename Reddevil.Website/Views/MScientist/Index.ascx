﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.ViewControl" %>

<%
    var listItem = ViewBag.Data as List<ModScientistEntity>;
    var model = ViewBag.Model as MScientistModel;
    string _chuyenNganh = string.Empty;
    if (model.choseid > 0)
    {
        var chuyenNganh = ModChuyenNganhService.Instance.CreateQuery().Select(o => o.Name).Where(o => o.Id == model.choseid).ToSingle_Cache() ?? new ModChuyenNganhEntity();
        _chuyenNganh = "<p class=\"nganh\">Chuyên ngành: " + chuyenNganh.Name + "</p>";
    }
%>

<link type="text/css" href="<%=Static.Tag("/interface/pc/css/nhakhoahoc.css") %>" rel="stylesheet" />
<div class="box_body_content" id="list">
    <div id="page-title">
        <div class="background"></div>
        <div class="mask">
            <div class="position">
                <h1>Danh sách nhà khoa học
                </h1>
            </div>
        </div>
    </div>
    <div class="content">
        <div id="filter-cta">
            <a class="button secondary weak activator" href="#filter-summary">
                <i class="fa fa-filter fa-fw" aria-hidden="true"></i><span class="active">Ẩn bộ lọc</span><span class="inactive">Hiện bộ lọc</span>
            </a>
        </div>
        <div id="filter-summary">
            <a href="javascript:$('.filter-overlay').show()" class="has-icon icon-specialities" title="Lọc danh sách nhà khoa học theo Chuyên ngành">Chuyên ngành</a>
            <a href="javascript:$('.filter-overlay').show()" class="has-icon icon-titles" title="Lọc danh sách nhà khoa học theo Học hàm - Học vị">Học hàm - Học vị</a>
            <div class="search" style="float: right">
                <input type="text" class="form-control key-search" onchange="filter_scientist(this.value);" placeholder="Tìm trên danh sách này...">
            </div>
        </div>
        <ul class="list-scientist">
            <%for (int i = 0; listItem != null && i < listItem.Count; i++)
                {  %>
            <li>
                <div class="media">
                    <a href="<%=ViewPage.ViewScientistURL %><%=Data.GetCode(listItem[i].Degree + " " + listItem[i].FullName) + "-" + listItem[i].Id %>" title="<%=(!string.IsNullOrEmpty(listItem[i].Degree) ? Data.GetFirstChar(listItem[i].Degree).ToUpper() + "." : "") + Data.GetFirstChar(listItem[i].ProfessionalKnowledge).ToUpper()%> <%=listItem[i].FullName %>" class="hero-image" style="background-image: url('<%=Utils.OptimalImage("http://img.meddom.org/0"+listItem[i].Images) %>'); background-repeat: no-repeat; background-size: cover;"></a>
                </div>
                <div class="body">
                    <div class="info">
                        <h2>
                            <a href="<%=ViewPage.ViewScientistURL %><%=Data.GetCode(listItem[i].Degree + " " + listItem[i].FullName) + "-" + listItem[i].Id %>" title="<%=(!string.IsNullOrEmpty(listItem[i].Degree) ? Data.GetFirstChar(listItem[i].Degree).ToUpper() + "." : "") + Data.GetFirstChar(listItem[i].ProfessionalKnowledge).ToUpper()%> <%=listItem[i].FullName %>"><%=(!string.IsNullOrEmpty(listItem[i].Degree) ? Data.GetFirstChar(listItem[i].Degree).ToUpper() + "." : "") + Data.GetFirstChar(listItem[i].ProfessionalKnowledge).ToUpper()%> <%=listItem[i].FullName %></a>
                        </h2>
                        <dl class="brief">
                            <dt>
                                <i class="fa fa-graduation-cap"></i>
                            </dt>
                            <dd>
                                <%if (!string.IsNullOrEmpty(listItem[i].Degree))
                                    {  %>
                                <a href="javascript:void(0)" title="Học vị <%=listItem[i].Degree %>"><%=listItem[i].Degree %></a>
                                <%} %>
                                <%if (!string.IsNullOrEmpty(listItem[i].ProfessionalKnowledge))
                                    {  %>
                                <a href="javascript:void(0)" title="Học hàm <%=listItem[i].ProfessionalKnowledge %>">, <%=listItem[i].ProfessionalKnowledge %></a>
                                <%} %>
                            </dd>
                            <dt>
                                <i class="fa fa-stethoscope"></i>
                            </dt>
                            <dd>
                                <a href="javascript:void(0)" title="Chuyên ngành <%=listItem[i].GetSpecialized %>"><%=listItem[i].GetSpecialized %></a><br />
                            </dd>
                            <dt>
                                <i class="fa fa-calendar"></i>
                            </dt>
                            <dd>
                                <a href="javascript:void(0)" title="Ngày sinh <%=Utils.FormatDate2(listItem[i].Birthday) %>"><%=Utils.FormatDate2(listItem[i].Birthday) %></a>
                            </dd>
                        </dl>
                        <div class="desc">
                            Danh hiệu, giải thưởng, huân huy chương mà <%=listItem[i].Degree + ", " + listItem[i].ProfessionalKnowledge + " " + listItem[i].FullName %> dành được:
                       <%=Data.CutString(listItem[i].Award, 60) %>
                            <a class="readmore" href="<%=ViewPage.ViewScientistURL %><%=Data.GetCode(listItem[i].Degree + " " + listItem[i].FullName) + "-" + listItem[i].Id %>">Xem thêm <i class="fa fa-angle-double-right"></i></a>
                        </div>

                    </div>

                </div>
            </li>
            <%} %>
        </ul>
    </div>
    <ul class="pagination mt-2">
        <%= GetPagination("",model.page, model.PageSize, model.TotalRecord)%>
    </ul>
</div>
<div class="overlay filter-overlay" style="display: none">
    <div class="overlay-content">
        <div class="inner">
            <div>
                <ul class="tab-content-triggers">

                    <li>
                        <a href="#tab-provinces" class="has-icon icon-specialities active" title="Lọc danh sách nhà khoa học theo Chuyên ngành ">Chuyên ngành</a>
                    </li>
                    <li>
                        <a href="#tab-titles" class="has-icon icon-titles" title="Lọc danh sách nhà khoa học theo Học hàm - học vị">Học hàm - Học vị</a>
                    </li>

                </ul>
                <div class="tab-content-container">
                    <div id="tab-provinces" class="tab-content filter-content active">
                        <div class="inner">
                            <ul>
                                <%=Utils.ShowDdlChuyenNganh("ChuyenNganhKhoaHoc",0,model.choseid) %>
                            </ul>

                        </div>
                        <div class="search">
                            <input type="text" placeholder="Tìm trên danh sách này..." onchange="filter_scientist(this.value);">
                        </div>
                    </div>
                    <div id="tab-titles" class="tab-content filter-content">
                        <div class="inner">
                            <ul>
                                <li>
                                    <label>
                                        <input type="checkbox" name="titles" class="hoc-ham" value="giáo sư">
                                        <span>Giáo sư</span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input type="checkbox" name="titles" class="hoc-ham" value="phó giáo sư">
                                        <span>Phó Giáo sư</span>
                                    </label>
                                </li>

                                <li>
                                    <label>
                                        <input type="checkbox" name="degrees" class="hoc-ham" value="thạc sĩ">
                                        <span>Thạc sĩ</span>
                                    </label>
                                </li>

                                <li>
                                    <label>
                                        <input type="checkbox" name="degrees" class="hoc-ham" value="tiến sĩ">
                                        <span>Tiến sĩ</span>
                                    </label>
                                </li>
                            </ul>
                        </div>

                    </div>

                    <div id="tab-degrees" class="tab-content filter-content  ">
                        <div class="inner">
                            <ul>
                            </ul>

                        </div>

                    </div>

                </div>
                <div class="button-row">
                    <button type="button" onclick="filter_scientist($('.key-search').val()); "><i class="fa fa-filter fa-fw" aria-hidden="true"></i>Lọc danh sách</button>
                </div>
            </div>
        </div>
        <a href="javascript:$('.filter-overlay').hide()" id="closeOverlay" class="close">
            <i18n>Đóng</i18n></a>
    </div>
</div>
<div class="loading" style="display: none;"><i class="icon">Loading</i></div>
<script type="text/javascript">
    function filter_scientist(keyword) {
        var sUrl = '';
        var mapCheckbox = $(".tab-content-container").find('.chuyen-nganh:checked').map(function () {
            return this.value;
        }).get();

        if (mapCheckbox.length > 0) sUrl += (sUrl == '' ? '?' : '&') + 'cate=' + mapCheckbox.join(',');
        mapCheckbox = $(".filter-content").find('.hoc-ham:checked').map(function () {
            return this.value;
        }).get();

        if (mapCheckbox.length > 0) sUrl += (sUrl == '' ? '?' : '&') + 'nkh=' + mapCheckbox.join(',');
        filter_product('<%=ViewPage.CurrentURL%>' + sUrl, '.list-scientist', keyword);

        $('.filter-overlay').hide();
    }

    $('.tab-content-triggers a').on('click', function () {
        var id = $(this).attr('href');
        $('.tab-content').removeClass('active');
        $('.tab-content-triggers a').removeClass('active');
        $(this).addClass('active');
        $(id).addClass('active');
    });

</script>
