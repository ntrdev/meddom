﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.ViewControl" %>

<% 
    var item = ViewBag.Data as ModFeedbackEntity;
    var model = ViewBag.Model as MFeedbackModel;
%>


<div class="container">
    <div class="google-map">
        {RS:Map_IF}
    </div>
    <div class="row">
        <div class="col-md-8">
            <h4>Gửi phản hồi tới chúng tôi</h4>
            <form method="post" role="form" class="contactForm">
                <div class="row">
                    <div class="col-md-6 form-group field">
                        <input type="text" class="form-control" name="Name" id="name" value="<%=item.Name %>" placeholder="Nhập họ và tên đầy đủ" maxlength="100" />
                        <div class="validation"></div>
                    </div>
                    <div class="col-md-6 form-group">
                        <input type="text" class="form-control phone" name="Phone" id="Phone" value="<%=item.Phone %>" placeholder="Nhập số điện thoại " maxlength="100" />
                        <div class="validation"></div>
                    </div>
                    <div class="col-md-6 form-group">
                        <input type="email" class="form-control" name="Email" id="email" value="<%=item.Email %>" placeholder="Nhập địa chỉ email" maxlength="100" />
                        <div class="validation"></div>
                    </div>
                    <div class="col-md-6 form-group">
                        <input type="text" class="form-control" name="Address" id="Address" value="<%=item.Address %>" placeholder="Nhập địa chỉ" maxlength="100" />
                        <div class="validation"></div>
                    </div>

                    <div class="col-md-12 form-group">
                        <textarea name="Content" class="form-control description" rows="5" data-rule="required"
                            placeholder="Nhập chi tiết yêu cầu"><%=item.Content %></textarea>
                        <div class="validation"></div>
                        <div class="form-group" style="width: 80%">
                            <input type="text" class="form-control" name="ValidCode" id="ValidCode" style="width: 50%;float: left;" required="required" autocomplete="off" value="" size="40" placeholder="Mã bảo mật">
                            <img src="/ajax/Security.html" class="vAlignMiddle" id="imgValidCode" style="width: 35%;float: left;max-height: 35px;border-radius: 20px;" alt="security code">
                            <a href="javascript:void(0)" onclick="change_captcha()" title="Tạo mã khác" rel="nofollow" style="float: right;margin-right: 54px;">
                                <img src="/interface/pc/images/icon-refreh.png" alt="refresh security code">
                            </a>
                        </div>
                        <div class="text-center">
                            <button class="btn btn-theme btn-medium margintop10" style="float: right" type="submit" name="_reddevil_action[AddPOST]">Gửi yêu cầu</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-4">
            <div class="clearfix"></div>
            <aside class="right-sidebar">

                <div class="widget">
                    <h5 class="widgetheading">Thông tin liên hệ<span></span></h5>
                    <ul class="contact-info">
                        {RS:Contact_Feedback}
                    </ul>

                </div>
            </aside>
        </div>
    </div>
</div>
