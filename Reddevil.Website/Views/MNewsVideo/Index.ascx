﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.ViewControl" %>

<%
    var listItem = ViewBag.Data as List<ModNewsVideoEntity>;
    var model = ViewBag.Model as MNewsVideoModel;
    if (listItem == null) return;

%>
<div class="box_body_content">

    <div class="tab-content">
        <div class="box_filter_data">
            <div class="box_search_khoahoc" style="position:relative">
                <input type="text" value="<%=model.news %>" placeholder="Tìm kiếm" onchange="filter_scientist()" id="key">
                <button><i class="fa fa-search" aria-hidden="true"></i></button>
            </div>
        </div>
        <div id="video_main" class="video_main" style=""></div>
        <div role="tabpanel" class="tab-pane fade in active show" id="news_video">
            <div class="box_video_big">
                <h4 class="title_video"><%=listItem[0].Name %></h4>
            </div>
            <ul class="list_videos_news">
                <%for (int i = 1; i < listItem.Count; i++)
                    {%>
                <li class="item_video">
                    <div class="box_img" onclick="play_video('video_main','<%=listItem[i].UrlVideo %>','<%=listItem[i].Thumbnail %>','100%', ($('#video_main').width() * 2 / 3), 'true','<%=listItem[i].Name %>')">

                        <img src="<%=listItem[i].Thumbnail %>" alt="">
                        <div class="icon_play">
                            <i class="fa fa-play-circle" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="box_info">
                        <h4><a href="<%=ViewPage.GetURL(listItem[i].MenuID, listItem[i].Url) %>"><%=listItem[i].Name %></a></h4>
                    </div>
                </li>
                <%} %>
            </ul>
            <ul class="pagination">
                <%= GetPagination("",model.page, model.PageSize, model.TotalRecord)%>
            </ul>
        </div>
    </div>
</div>
<script type="text/javascript" src="/interface/utils/jwplayer/jwplayer.js"></script>
<script type="text/javascript">jwplayer.key = "bmYhK/6mW299S1kjNJFsA7IAuEIajluMDteXkg==";</script>
<script type="text/javascript">
    $(window).on('load', function () {
        play_video('video_main', '<%=listItem[0].UrlVideo%>', '<%=listItem[0].Thumbnail%>', '100%', ($('#video_main').width() * 2 / 3), true,'<%=listItem[0].Name %>');

    });
    function play_video(pos, file, image, width, height, auto, name) {
        jwplayer(pos).setup({
            file: file,
            image: image,
            abouttext: 'IMEXsoft® - 01032019',
            width: width,
            height: height,
            stretching: "exactfit",
            autostart: auto,
            logo: {
                file: '',
                link: '',
            }
        });
        $('.title_video').html(name);
    }
    function filter_scientist() {
        var keyword = $('#key').val();
        var sUrl = '';
        if (keyword != '') sUrl += (sUrl == '' ? '?' : '&') + 'news=' + keyword;
        if (sUrl != '') location.href = '<%=ViewPage.CurrentURL%>' + sUrl;
    }
</script>
