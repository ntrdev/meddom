﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.ViewControl"%>

<% 
    var item = ViewBag.Data as ModNewsEntity;
    var listItem = ViewBag.Other as List<ModNewsEntity>;
%>

<section class="pageNew">
    <div class="container">
        <article class="thumbnail-news-view">
            <h1><%=item.Name %></h1>
            <div class="block_timer_share">
                <div class="block_timer pull-left"><i class="fa fa-clock-o"></i><%=string.Format("{0:dd/MM/yyyy HH:mm}", item.Updated) %></div>
                <div class="block_share pull-right">
                    <div class="fb-like" data-href="<%=ViewPage.GetURL(item.Code) %>" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div>
                </div>
            </div>
            <div class="post_content"><%=Utils.GetHtmlForSeo(item.Content) %></div>

            <%if(listItem != null) {%>
            <div class="other-news-detail">
                <h2><span>Tin liên quan</span></h2>
                <ul>
                    <%for (var i = 0; i < listItem.Count; i++){%>
                    <li><span><a href="<%=ViewPage.GetURL(listItem[i].Code) %>" title="<%=listItem[i].Name %>"><%=listItem[i].Name %></a></span></li>
                    <%} %>
                </ul>
            </div>
            <%} %>
        </article>
    </div>
</section>