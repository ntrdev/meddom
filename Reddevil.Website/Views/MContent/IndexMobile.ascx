﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.ViewControl" %>

<section class="pageNew">
    <div class="container">
        <article class="thumbnail-news-view">
            <h1><%=ViewPage.CurrentPage.Name %></h1>
            <div class="block_timer_share">
                <div class="block_timer pull-left"><i class="fa fa-clock-o"></i><%=string.Format("{0:dd/MM/yyyy HH:mm}", ViewPage.CurrentPage.Updated) %></div>
                <div class="block_share pull-right">
                    <div class="fb-like" data-href="<%=ViewPage.CurrentURL %>" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div>
                </div>
            </div>
            <div class="post_content"><%=Utils.GetHtmlForSeo(ViewPage.CurrentPage.Content) %></div>
        </article>
    </div>
</section>