﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.ViewControl" %>


<div class="box_body_content">
    <div class="box_title_post">
        <h4><%=ViewPage.CurrentPage.Name %></h4>
        <div class="time_news">
            <i class="fa fa-clock-o" aria-hidden="true"></i>
            <span><%=Utils.FormatDateTime(ViewPage.CurrentPage.Updated) %></span>
        </div>
    </div>
    <p class="desc"><b><%=ViewPage.CurrentPage.Summary %></b></p>
    <%=Utils.GetHtmlForSeo(ViewPage.CurrentPage.Content) %>
</div>

<div class="box_like_share">
    <div class="fb-like" data-href="<%=ViewPage.GetPageURL(ViewPage.CurrentPage) %>" data-width="" data-layout="button_count" data-action="like" data-size="large" data-share="true"></div>
</div>
