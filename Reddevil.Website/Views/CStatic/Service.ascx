﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.ViewControl" %>
<div class="container">
    <div class="row">

        <div class="span3">
            <div class="service-box aligncenter flyLeft">
                <div class="icon">
                    <i class="icon-circled icon-bgprimary icon-code icon-4x"></i>
                </div>
                <h5>Tùy biến <span class="colored">Quản trị</span></h5>
                <p>Chúng tôi cung cấp dịch vụ tùy biến hệ quản trị website theo yêu cầu của khách hàng.</p>
            </div>
        </div>
        <div class="span3">
            <div class="service-box aligncenter flyIn">
                <div class="icon">
                    <i class="icon-circled icon-bgsuccess icon-cloud icon-4x"></i>
                </div>
                <h5>Dịch vụ <span class="colored">cho thuê Hosting</span></h5>
                <p>Cung cấp, cho thuê dịch vụ, nền tảng để website hoạt động.</p>
            </div>
        </div>
        <div class="span3">
            <div class="service-box aligncenter flyIn">
                <div class="icon">
                    <i class="icon-circled icon-bgdanger icon-mobile-phone icon-4x"></i>
                </div>
                <h5>Lập trình <span class="colored">Di động</span></h5>
                <p>Lập trình ứng dung IOS/Android theo các công nghệ di động mới nhất từng thời điểm.</p>

            </div>
        </div>
        <div class="span3">
            <div class="service-box aligncenter flyRight">
                <div class="icon">
                    <i class="icon-circled icon-bgwarning icon-envelope-alt icon-4x"></i>
                </div>
                <h5>Dịch vụ <span class="colored">SEO, Maketing...</span></h5>
                <p>Dịch vụ SEO giúp cho website của bạn nhanh chóng lên top tìm kiếm của ông lớn Google...</p>

            </div>
        </div>
    </div>
</div>
