﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.ViewControl" %>


<% var model = new CStaticModel(); %>
<div class="account x-hover" id="head-account-box">

    <div class="account-link">
        <%if (!WebLogin.IsLogin())
            { %>
        <i class="fa fa-user fa-2x"></i>
        <div class="account-link-pad">
            <span class="account-login">Đăng nhập</span><i class="i-view-more"></i>
            <span class="account-login-more">Tài khoản</span>
        </div>
        <%}
            else
            {
                var currentUser = WebLogin.CurrentUser;
        %>
        <%if (!string.IsNullOrEmpty(currentUser.Avatar))
            { %>
        <div class="avatar-user">
            <img class="avatar-img" src="<%=Utils.GetResizeFile(currentUser.Avatar, 2, 30, 30) %>">
        </div>
        <%}
            else
            { %><i class="fa fa-user fa-2x"></i>
        <%} %>
        <div class="account-link-pad">
            <span class="account-login username"><%=currentUser.Name%></span><i class="i-view-more"></i>
            <span class="account-login-more">Tài khoản</span>
        </div>
        <%} %>
    </div>

    <div class="account-box acc-login hover-lg-top">
        <%if (!WebLogin.IsLogin())
            { %>
        <p>Đăng nhập</p>
        <form method="post" name="login_form" autocomplete="off" role="form">
            <input type="text" class="account-input" id="hd-lg-email" name="LoginName" placeholder="Tên đăng nhập [hoặc] Số điện thoại, Email" value="<%=model.LoginName %>">
            <input type="password" class="account-input" id="hd-lg-password" name="Password" placeholder="Mật khẩu" value="">
            <input type="hidden" name="act" value="act-login">
            <div class="login-submit">
                <input type="submit" id="btnHdLoginSubmit" class="btn-login" value="Đăng nhập" name="_reddevil_action[CStatic-LoginPOST-SearchBox]" title="Đăng nhập">
            </div>
            <div class="d-flex account-forget-pass">
                <a class="login-lnk forget" id="hd-lnk-forget" href="<%=ViewPage.ForgotUrl %>">Quên mật khẩu?</a>
                <a class="login-lnk regis signin" id="hd-lnk-regis" href="<%=ViewPage.RegisterUrl %>" title="Đăng ký">Đăng ký</a>
            </div>
        </form>
        <%}
            else
            {
                var currentUser = WebLogin.CurrentUser;
        %>
        <form method="post" name="logout_form" autocomplete="off" role="form">
            <ul class="acc-info">
                <li><button class="btn btn-login" id="logout"  name="_reddevil_action[CStatic-LogoutPOST-SearchBox]" ><i class="fa fa-sign-out"></i>&nbsp;Thoát</button></li>
            </ul>
        </form>
        <%} %>
    </div>
</div>

<div class="ui search">
    <div class="ui icon input">
        <input class="prompt" type="text" placeholder="Tìm kiếm...">
        <i class="fa fa-search search icon "></i>
    </div>
    <div class="results"></div>
</div>

<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.5/jquery-ui.min.js"></script>
<link rel="stylesheet" href="/interface/utils/sucgess/search.min.css">
<script src="/interface/utils/sucgess/search.js"></script>
<script>
    $(document).ready(function () {
        search();
    });
</script>
