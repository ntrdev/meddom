﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<div class="in">
    <div class="search_box">
        <form method="post" onsubmit="doSearch2(); return false;" action="{ActionForm}" name="search_form2">
            <input type="text" name="keywords" id="keyword2" placeholder="{RS:Web_SearchText}" />
            <button type="submit" class="ico_07" onclick="doSearch2(); return false;" value="Tìm kiếm"></button>
        </form>
    </div>
    <a href="/" class="homeBtn"><span class="ico_05"></span></a><a href="#top" class="gotop"><span class="ico_06"></span>
        <p>TOP </p>
    </a>
</div>
<div class="region">
    <div class="webfooter">{RS:Web_Footer}</div>
    <div class="online-gov">
        <a href="javascript:void(0)" target="_blank">
            <img src="http://online.gov.vn/Images/dathongbao.png" alt="cityplaza.vn" />
        </a>
    </div>
</div>

<script type="text/javascript">
    function doSearch2() {
        var keyword = document.getElementById('keyword2');

        var sURL = '';

        if (keyword.value != '{RS:Web_SearchText}') {
            if (keyword.value.length < 2) {
                alertZebra('Từ khóa phải nhiều hơn 1 ký tự.', 'Thông báo');
                return;
            }
            else sURL += (sURL == '' ? '?' : '&') + 'keyword=' + keyword.value;
        }

        window.location.href = '<%=ViewPage.GetURL("tim-kiem")%>' + sURL;
    }
</script>