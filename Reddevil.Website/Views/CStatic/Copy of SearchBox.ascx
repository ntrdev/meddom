﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<% 
    var listItem = ViewBag.Data as List<WebMenuEntity>;
    var _Cart = new Cart();
%>

<div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
    <div class="search-input-select all">
        <form method="post" onsubmit="doSearch(); return false;" name="search_form">
            <div class="selectBox all">
                <div class="selected">
                    <span class="textdm" id="search_text">Tất cả danh mục</span>
                    <div class="selectOptions">
                        <%for (int i = 0; listItem != null && i < listItem.Count; i++){%>
                        <span data-id="<%=listItem[i].ID %>" data-code="<%=listItem[i].Code %>"><%=listItem[i].Name %></span>
                        <%} %>
                    </div>
                </div>
                <input type="search" class="search-txt" name="keyword" id="keyword" onkeypress="get_search($('#MenuID').val(), this.value)" placeholder="{RS:Web_SearchText}" />
                <button type="submit" class="search-btn" onclick="doSearch(); return false;"><i class="fa fa-search"></i></button>

                <input type="hidden" id="MenuID" value="0" />
                <input type="hidden" id="MenuCode" value="" />

                <ul id="search-suggestion" class="results"></ul>
            </div>
        </form>
    </div>
</div>
<div class="cart-phone col-xs-12 col-sm-12 col-md-2 col-lg-2 pd0 hidden-sm hidden-xs">
    <a href="/gio-hang.html" title="" class="bt-cart">
        <span>Giỏ hàng của bạn<b>(<%=string.Format("{0:#,##0}", _Cart.Items.Count) %>)</b></span>
        <i>Giao hàng toàn quốc</i>
    </a>
</div>

<script type="text/javascript">
    function doSearch() {
        var sURL = '';

        if ($('#keyword').val().length < 2) {
            zebra_alert('Thông báo !', 'Từ khóa phải nhiều hơn 1 ký tự.');
            return;
        }
        else sURL += (sURL == '' ? '?' : '&') + 'keyword=' + keyword.value;

        if ($('#MenuCode').val() == '') {
            zebra_alert('Thông báo !', 'Bạn chưa chọn chuyên mục.');
            return;
        }

        location.href = encodeURI('/' + $('#MenuCode').val().toLowerCase() + '.html' + sURL);
    }
</script>