﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>
<% 
    var _Cart = new Cart();
    var model = ViewBag.Model as MLoginModel;
    var item = ViewBag.User as ModWebUserEntity;
%>
<aside class="sidebar_right">
    <ul class="siderright">
        <li>
            <a href="<%=ViewPage.ViewCartURL %>?returnpath=<%=ViewPage.ReturnPath %>" class="cart">
                <i class="iconltd-asidecart"></i>
                <span id="cbCount" class="number_notice"><%=string.Format("{0:#,##0}", _Cart.Items.Count) %></span>
            </a>
        </li>
        <li>
            <a href="">
                <i class="iconltd-asidecheck"></i>
                Tra cứu đơn hàng
            </a>
        </li>
        <li>
            <a href="">
                <i class="iconltd-notice"></i>
                Thông báo
            </a>
        </li>
        <li>
            <a href="">
                <i class="iconltd-asidesupport"></i>
                Tư vấn
            </a>
        </li>
        <li>
            <a href="" data-toggle="modal" data-target="#popup-holiday-01">
                <i class="iconltd-asidehelp"></i>
                Hỗ trợ & Khiếu nại
            </a>
        </li>
        <li><a title="Lên đầu trang" id="topcontrol" rel="nofollow">Lên đầu <i class="iconltd-gotop"></i></a></li>
    </ul>
</aside>

<%-- Form liên hệ --%>
<div class="modal fade " id="popup-holiday-01" role="dialog">
    <div class="modal-dialog">
        <button type="button" class="close" data-dismiss="modal"></button>
        <div class="modal-content body-popup">
            <div class="header-pop"><strong>TIẾP NHẬN HỔ TRỢ VÀ KHIẾU NẠI KHÁCH HÀNG</strong> </div>
            <div class="modal-body post-modal">
                <p><strong class="required">(*)</strong>Thông tin bắt buộc</p>
                <div class="tbl_support">
                    <div class="rows_support">
                        <div class="data_input">
                            <input type="text" name="" required="required" class="form-control">
                            <label for="">Nhập họ tên của bạn </label>
                        </div>
                    </div>
                    <div class="rows_support">
                        <div class="data_input">
                            <input type="text" name="" required="required" class="form-control">
                            <label for="">Nhập số điện thoại của bạn </label>
                        </div>
                    </div>
                    <div class="rows_support">
                        <div class="data_input">
                            <input type="text" name="" required="required" class="form-control" placeholder="Nhập địa chỉ email của bạn">
                        </div>
                    </div>
                    <div class="rows_support">
                        <div class="data_input">
                            <textarea name="content_support" rows="8" required="required" class="form-control"></textarea>
                            <label for="">Nội dung góp ý </label>
                        </div>
                    </div>
                    <p class="text_thanks"><i class="blue">Cám ơn sự góp ý chân thành của bạn !</i></p>
                    <div class="sent_commend">
                        <button type="submit" class="btn btn_sentcomment">GỬI THÔNG TIN</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
