﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var _Page = ViewBag.Page as SysPageEntity;
    if (_Page == null) return;

    var listItem = ViewBag.Data as List<WebMenuEntity>;
%>

<section class="home-box all mt20">
    <div class="title-lage02 all">
        <i class="fa fa-star"></i>
        <h2 class="txt"><a href="<%=ViewPage.GetPageURL(_Page) %>" title="Chọn mua theo thương hiệu" rel="nofollow">Chọn mua theo thương hiệu</a></h2>
    </div>
    <div class="bg-trademark all">
        <ul class="list-trademark all">
            <%for (int i = 0; listItem != null && i < listItem.Count; i++){%>
            <li class="col-xs-6 col-sm-4 col-md-2 col-lg-2">
                <div class="brand-logo">
                    <a href="<%=ViewPage.GetURL(listItem[i].Code) %>" rel="nofollow">
                        <img src="<%=listItem[i].File.Replace("~/", "/") %>" title="<%=listItem[i].Name %>" alt="<%=listItem[i].Name %>" />
                    </a>
                </div>
            </li>
            <%} %>
        </ul>
        <p class="text-center all"><a href="<%=ViewPage.GetPageURL(_Page) %>" class="btn btn-seemore" title="Chọn mua theo thương hiệu" rel="nofollow">Xem thêm</a></p>
    </div>
</section>