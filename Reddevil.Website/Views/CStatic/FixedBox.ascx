﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<ul class="right-controls" style="display: none;">

    <li><a href="javascript:void(0)" id="closetab"><i class="sprite-icon_tenvao main-nav-icon2" title="Đóng"></i></a></li>
    <li>
        <a href=""><i class="main-nav-icon sprite-icon_payment"></i><span><span>Thanh toán</span> <span>linh hoạt</span> </span>
        </a>
    </li>
    <li>
        <a href=""><i class="main-nav-icon sprite-icon_delivery"></i><span><span>Giao hàng</span> <span>tận nơi</span> </span>
        </a>
    </li>
    <li>
        <a href="">
            <i class="sprite-icon_set_up main-nav-icon"></i>
            <span><span>Lắp đặt</span> <span>tận tình</span> </span>
        </a>
    </li>
    <li>
        <a href="">
            <i class="sprite-icon_change main-nav-icon"></i>
            <span><span>Đổi trả</span> <span>dễ dàng</span> </span>
        </a>
    </li>
    <li>
        <a href="">
            <i class="sprite-icon_after_market main-nav-icon"></i>
            <span><span>Hậu mãi</span> <span>chu đáo</span> </span>
        </a>
    </li>
    <li><a href="javascript:void(0)" onclick="$('html, body').animate({ scrollTop: 0 }, 'slow')"><i class="sprite-icon-top-up main-nav-icon" title="Về đầu trang"></i></a></li>
</ul>
<ul class="right-controls2">
    <li><a href="javascript:void(0)" id="showtab"><i class="sprite-icon_tenra main-nav-icon2" title="Đóng"></i></a></li>
</ul>