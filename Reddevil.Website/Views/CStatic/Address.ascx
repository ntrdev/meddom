﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<h3 class="heading">Văn phòng <%=ViewPage.Request.Url.Host %></h3>
<div class="bg-ft">
    {RS:Web_Address}
    <ul>
        <li><b>Hà Nội (Văn phòng)</b>: Tầng 4 - 49 Hàng Gà – Q. Hoàn Kiếm – Hà Nội</li>
        <li><b>Hà Nội (Showroom)</b>: Tầng 3 - 49 Hàng Gà, Hoàn Kiếm</li>
        <li><b>TP. Hồ Chí Minh</b>: Kho hàng tại Quận 1 và Q. Tân Bình</li>
        <li>Điện thoại: 04-39231755 - 39231766 - 0943382868</li>
        <li>Email: info@cityplaza.vn</li>
        <li>Số giấy chứng nhận đăng ký kinh doanh: 01C8022695.</li>
        <li>Mã số thuế: 0106704817</li>
    </ul>
</div>