﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.ViewControl" %>

<% 
    var item = ViewPage.ViewBag.Data;
    var itemScientist = ViewPage.ViewBag.Item as ModScientistEntity;
%>



<div class="box_links_top">
    <ol class="breadcrumb">
        <%=Utils.GetMapPage(ViewPage.CurrentPage) %>
        <%if (item != null)
            {%>
        <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="<%=ViewPage.GetURL(0, item.Url) %>" title="<%=item.Name %>" itemprop="item"><span itemprop="name"><%=item.Name %></span></a>
            <meta itemprop="position" content="4" />
        </li>
        <%} %>
        <%if (itemScientist != null)
            {%>
        <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="javascript:void(0)" title="<%=itemScientist.FullName %>" itemprop="item"><span itemprop="name"><%=itemScientist.Degree+" "+ itemScientist.FullName %></span></a>
            <meta itemprop="position" content="4" />
        </li>
        <%} %>
    </ol>
</div>
