﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.ViewControl" %>
<%
    var _CurrentKiot = ViewPage.CurrentKiot;
    if (_CurrentKiot == null) return;
%>

<input type="hidden" id="KiotName" value="" />

<div class="box-danhmuc-sibar">
    <h3 class="widget-title">Tài khoản  doanh nghiệp/ người bán</h3>
    <div class="box-img-brand-sibar img-upload">
        <img src="<%=Utils.GetResizeFile(_CurrentKiot.File,4,500,500) %>" alt="<%=_CurrentKiot.Name %>" />
    </div>
    <div class="info-store">
        <p><i class="fa fa-home" aria-hidden="true"></i>&nbsp;<b style="color: #166ec3;"><%=_CurrentKiot.Name %></b></p>
        <p><i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;<%=_CurrentKiot.Email %></p>
        <p><i class="fa fa-phone" aria-hidden="true"></i>&nbsp;<%=_CurrentKiot.Phone %></p>
        <p><i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp; Sản phẩm đã đăng bán: <%=_CurrentKiot.getCountProduct() %></p>
        <p><i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;<%=_CurrentKiot.Address%></p>
        <p>
            Đáng giá hiện tại:
                                        <span class="starRating">
                                            <input id="rating-input_<%=_CurrentKiot.ID %>" type="number" class="stars" value="<%=_CurrentKiot.Star %>" />
                                            <input type="hidden" id="Star" value="" />
                                        </span>

        </p>
        
    </div>
    <div class="clear"></div>
</div>

<div class="box-danhmuc-sibar mb30">
    <h3 class="widget-title">Danh mục</h3>
    <ul class="ul-ctgr-sibar ul-quanly pb10">
        <li <%if (ViewPage.CurrentVQS.EndCode == "messengermng")
            {%>class="active"
            <%} %>><a href="<%=ViewPage.KiotCPURL%>messengermng"><i class="fa fa-commenting"></i>&nbsp;Quản lý Tin nhắn</a></li>
        <li <%if (ViewPage.CurrentVQS.EndCode == "NewsMng")
            {%>class="active"
            <%} %>><a href="<%=ViewPage.NewsMngUrl%>"><i class="fa fa-archive"></i>&nbsp;Quản lý Tin rao vặt</a></li>

        <li <%if (ViewPage.CurrentVQS.EndCode == "NewsPost")
            {%>class="active"
            <%} %>><a href="<%=ViewPage.NewsPostUrl%>"><i class="fa fa-upload"></i>&nbsp;Đăng Tin rao vặt</a></li>

        <li <%if (ViewPage.CurrentModule.Code == "MOrderKiotCP")
            {%>class="active"
            <%} %>><a href="<%=ViewPage.OrderKiotCPURL %>"><i class="fa fa-server"></i>&nbsp;Đơn hàng</a></li>

        <li <%if (ViewPage.CurrentVQS.EndCode == "ProductMng")
            {%>class="active"
            <%} %>><a href="<%=ViewPage.ProductMngUrl%>"><i class="fa fa-th-list"></i>&nbsp;Quản lý sản phẩm</a></li>

        <li <%if (ViewPage.CurrentVQS.EndCode == "ProductPost")
            {%>class="active"
            <%} %>><a href="<%=ViewPage.ProductPostUrl%>"><i class="fa fa-upload"></i>&nbsp;Đăng Sản phẩm</a></li>

        <li <%if (ViewPage.CurrentVQS.Count == 1 && ViewPage.CurrentVQS.EndCode.Contains("danh-gia"))
            {%>class="active"
            <%} %>><a href="<%=ViewPage.MnRattingURL%>"><i class="fa fa-star"></i>&nbsp;Quản lí đánh giá</a></li>
        <li <%if (ViewPage.CurrentVQS.EndCode == "NotificationMng")
            {%>class="active"
            <%} %>><a href="<%=ViewPage.KiotCPURL%>/NotificationMng"><i class="fa fa-bell"></i>&nbsp;Quản lí thông báo</a></li>

        <li <%if (ViewPage.CurrentVQS.Count == 1 && !ViewPage.CurrentVQS.EndCode.Contains("danh-gia"))
            {%>class="active"
            <%} %>><a href="<%=ViewPage.KiotCPURL%>"><i class="fa fa-info-circle"></i>&nbsp;Thông tin tài khoản</a></li>

        <li><a href="<%=ViewPage.LogoutURL%>"><i class="fa fa-sign-out"></i>&nbsp;Thoát</a></li>
    </ul>
</div>
