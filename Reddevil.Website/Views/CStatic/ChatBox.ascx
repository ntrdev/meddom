﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.ViewControl" %>


<%
    var _webuser = WebLogin.CurrentUser;
    if (_webuser == null) return;

%>
<script type="text/javascript" src="<%=Static.Tag("/interface/pc/js/jquery.signalR-2.4.1.min.js") %>"></script>
<script type="text/javascript" src="/signalr/hubs"></script>

<script type="text/javascript" async src="<%=Static.Tag("/interface/pc/js/chat-hub.js") %>"></script>

<script type="text/javascript"  src="<%=Static.Tag("/interface/pc/js/toastr.min.js") %>"></script>
<script type="text/javascript"  src="<%=Static.Tag("/interface/pc/js/ui-toastr.min.js") %>"></script>
<input type="hidden" id="fileUser" value="<%=Utils.OptimalImage(_webuser.Avatar) %>" />

<div id="sound"></div>
<input type="hidden" id="fromID" value="<%=_webuser!=null?_webuser.ID.ToString():"" %>" />
<div id="btn-chat" onclick="open_global();" style="display: none">
    <div class="shop-chat-container">
        <i class="fa fa-comments"></i>
        <span>Chát ngay</span>
    </div>
</div>
<div id="shop-chat-bg">
    <div class="shop-chat-bg-view">
        <div class="buddy-list">
            <div class="shop-chat-tabs">
                <ul class="tabs-shop">
                    <li role="presentation" class="active">
                        <a href="#tab-div1" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true">Tất cả</a>
                    </li>
                </ul>
                <div class="tab-content-list all">
                    <div class="tab-content-list all">
                        <div role="tabpanel" class="tab-pane clear tab-list-user active" id="tab-div1">
                            <ul class="member_list" id="list_userChat">
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="conversation-window">
            <div class="chat-header">
                <div onclick="closeglobal()" id="close"><i class="fa fa-times"></i></div>
                <div class="chat-header-text">
                    <span role="button" aria-expanded="true" id="name_shop"></span>

                </div>
            </div>
            <div class="chat-window" id="list_chatContent">
            </div>
            <audio id="audio" src="/Data/upload/images/FbNotifi.mp3"></audio>
            <div class="chat-window-panel">
                <input type="hidden" value="0" id="setToIDUser" />
                <input type="hidden" value="" id="loginNameToUser" />
                <input type="text" placeholder="Nhập tin nhắn ..." maxlength="3000" autocomplete="off" id="txtMessage" class="form-control text-mess" />
                <div class="fr abbtn">
                    <button class="btn-sent-shop" id="btnSendMsg"><i class="fa fa-paper-plane"></i></button>
                    <button class="btn-sent-shop" data-toggle="modal" data-target="#popup-sticker"><i class="fa fa-smile-o" aria-hidden="true"></i></button>
                </div>
            </div>
        </div>
    </div>
</div>





<div class="modal fade popup-cart fAvantGarde in" id="popup-sticker" role="dialog">
    <div class="modal-dialog">
        <button type="button" class="close" data-dismiss="modal"></button>
        <div class="modal-content body-popup-cart">
            <div class="header-pop"><strong>Sticker</strong> </div>
            <div class="sticker-box full">
                <div class="ongech">
                    {RS:z_icon_emoij_quobee}
                </div>
                <div class="echxanh">
                    <%--{RS:z_icon_emoij_cat}--%>
                </div>
                <div class="smile">
                </div>
            </div>
        </div>
    </div>
</div>

<link type="text/css" href="<%=Static.Tag("/interface/pc/css/toastr.min.css") %>" rel="stylesheet" />
<link type="text/css" href="<%=Static.Tag("/interface/pc/css/chathub.css") %>" rel="stylesheet" />
<script type="text/javascript">
    $(".sticker").on("click", function () {
        var zzz = $(this).attr('src');
        var string_xxx = '<img src="' + zzz + '"/>';
        $('#txtMessage').val($('#txtMessage').val() + '<br/>' + string_xxx);
        $('#btnSendMsg').click();
        $('.close').click();

    });

    
</script>
