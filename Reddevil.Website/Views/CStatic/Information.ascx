﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.ViewControl" %>
<div class="container">
    <div class="row">
        <div class="span12">
            <div class="row">
                <div class="span4">
                    <div class="box flyLeft">
                        <div class="icon">
                            <i class="ico icon-circled icon-bgdark icon-star icon-3x"></i>
                        </div>
                        <div class="text">
                            <h4>Chuyên nghiệp <strong>Chất lượng</strong></h4>
                            <p>Tư vấn và triển khai theo nhu cầu và yêu cầu, Cấu trúc web chuẩn SEO, tối ưu trải nghiệm người dùng, tốc độ website...</p>
                            <a href="#">Xem thêm</a>
                        </div>
                    </div>
                </div>

                <div class="span4">
                    <div class="box flyIn">
                        <div class="icon">
                            <i class="ico icon-circled icon-bglight icon-dropbox icon-3x"></i>
                        </div>
                        <div class="text">
                            <h4>Bảo mật <strong>dễ nâng cấp</strong></h4>
                            <p>Bảo mật với dữ liệu khách hàng, dễ dàng nâng cấp, mở rộng thêm chức năng của website...</p>
                            <a href="#">Xem thêm</a>
                        </div>
                    </div>
                </div>
                <div class="span4">
                    <div class="box flyRight">
                        <div class="icon">
                            <i class="ico icon-circled icon-bgdark icon-laptop active icon-3x"></i>
                        </div>
                        <div class="text">
                            <h4>Công nghệ <strong>Responsive</strong></h4>
                            <p>Với công nghệ luôn cập nhật, tối ưu tốc độ load, tương thích với nhiều nền tảng...</p>
                            <a href="#">Xem thêm</a>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="span4">
                    <div class="box flyLeft">
                        <div class="icon">
                            <i class="ico icon-circled icon-bgprimary icon-code icon-3x"></i>
                        </div>
                        <div class="text">
                            <h4>Mã hóa <strong>Code</strong></h4>
                            <p>Đóng gói, mã hóa mã nguồn của app/website để tăng cường bảo mật cho hệ thống.</p>
                            <a href="#">Xem thêm</a>
                        </div>
                    </div>
                </div>

                <div class="span4">
                    <div class="box flyIn">
                        <div class="icon">
                            <i class="ico icon-circled icon-bgdanger icon-cut icon-3x"></i>
                        </div>
                        <div class="text">
                            <h4>Thiết kế <strong>theo yêu cầu</strong></h4>
                            <p>Thiết kế giao diện theo nhu cầu của khách hàng</p>
                            <a href="#">Xem thêm</a>
                        </div>
                    </div>
                </div>
                <div class="span4">
                    <div class="box flyRight">
                        <div class="icon">
                            <i class="ico icon-circled icon-bgsuccess icon-user icon-3x"></i>
                        </div>
                        <div class="text">
                            <h4>Hỗ trợ <strong>24/7</strong></h4>
                            <p>Hỗ trợ khách hàng, tất cả các ngày trong tuần, cả thứ 7 và chủ nhật.</p>
                            <a href="#">Xem thêm</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
