﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<% 
    var cart = new Cart();
%>

<div id="header" style="z-index: 1">
    <div class="header_l"><a class="ico_10" href="cat_all.php">Danh mục </a></div>
    <div id="search_box2">
        <div class="search_box">
            <form method="post" action="search.php" name="searchForm" id="searchForm_id">
                <input placeholder="Tìm kiếm" name="keywords" type="text" id="keywordBox" />
                <button class="ico_07" type="submit" onclick="return check('keywordBox')"></button>
            </form>
        </div>
    </div>
    <div class="header_r header_search"><a class="switchBtn switchBtn-album" href="javascript:void(0);" onclick="changeCl(this)" style="opacity: 1;">Hiển thị</a> </div>
</div>










<div class="header-right">
    <div class="block_search_web fl">
        <form onsubmit="doSearch(); return false;">
            <div class="keyword fl">
                <input type="text" class="keyword" name="keyword" id="keyword" onblur="onTextBlur('keyword','{RS:Web_SearchText}');" onfocus="onTextFocus('keyword','{RS:Web_SearchText}');" value="{RS:Web_SearchText}" />
            </div>
            <div class="button">
                <input type="submit" onclick="doSearch(); return false;" value="">
            </div>
        </form>
    </div>

    <div class="cart"><%=cart.Items.Count %></div>
    <div class="cart-txt"><a rel="nofollow" href="<%=ViewPage.GetURL("gio-hang") %>?returnpath=<%=ViewPage.ReturnPath %>">Giỏ hàng</a></div>

    <div class="login">
        <%if (!WebLogin.IsLogin()){%>
        <a rel="nofollow" href="<%=ViewPage.GetURL("dang-ky")%>">Đăng ký</a>
        <a rel="nofollow" href="<%=ViewPage.GetURL("dang-nhap")%>">Đăng nhập</a>
        <%}else{ %>
        <a rel="nofollow" href="<%=ViewPage.GetURL("thanh-vien/ChangeInfo")%>"><b><%=WebLogin.CurrentUser.Email %></b></a>
        <a href="<%=ViewPage.GetURL("thanh-vien/ChangePassword")%>">Đổi mật khẩu </a>
        <a rel="nofollow" href="<%=ViewPage.GetURL("thanh-vien/Logout")%>">Thoát</a>
        <%} %>
    </div>

    <div class="clear"></div>

    <p class="date_time">Bây giờ là <%=string.Format("{0:f}", DateTime.Now) %></p>
</div>

<script type="text/javascript">
    function doSearch() {
        var keyword = document.getElementById('keyword');

        var sURL = '';

        if (keyword.value != '{RS:Web_SearchText}') {
            if (keyword.value.length > 0 && keyword.value.length < 2) {
                alertZebra('Từ khóa phải nhiều hơn 1 ký tự.', 'Thông báo');
                return;
            }
            else sURL += (sURL == '' ? '?' : '&') + 'keyword=' + keyword.value;
        }

        window.location.href = '<%=ViewPage.GetURL("tim-kiem")%>' + sURL;
    }
</script>