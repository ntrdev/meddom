﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%var item = ViewBag.Data as ModSubscribeEntity ?? new ModSubscribeEntity(); %>

<div class="newsletter">
    <p>Đăng ký nhận tin khuyến mãi hàng tuần</p>
    <form method="post" name="subscribe_form" action="{ActionForm}">
        <div class="input-group">
            <input type="email" class="newsletter-form" required="required" name="Email" value="<%=item.Email %>" title="Nhập địa chỉ email" placeholder="Nhập địa chỉ email" maxlength="50" />
            <span class="newsletter-button">
                <button type="submit" name="_vsw_action[CStatic-AddPOST-Subscribe]"><i class="fa fa-envelope"></i></button>
            </span>
        </div>
    </form>
</div>
<div class="footer-share clear">
    <p>Chia sẻ</p>
    <a href="#" target="_blank" rel="nofollow"><i class="fa fa-facebook"></i></a>
    <a href="#" target="_blank" rel="nofollow"><i class="fa fa-youtube"></i></a>
    <a href="#" target="_blank" rel="nofollow"><i class="fa fa-google-plus"></i></a>
    <a href="#" target="_blank" rel="nofollow"><i class="fa fa-instagram"></i></a>
</div>