﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.ViewControl" %>



<section id="our-services" class="sec use-bg">
    <div class="container white">
        <h3 class="sec-tit text-center">Tại sao bạn nên chọn Webtinhte</h3>
        <ul class="feats use-cols">
            <li class="span6 it col">
                <h4 class="tit">SỬ DỤNG CÔNG NGHỆ VÀ NGÔN NGỮ MẠNH MẼ</h4>
                <div class="desc">
                    <p>Đối với dự án khó như thiết kế web dạy học trực tuyến thì việc lựa chọn ngôn ngữ lập trình cần được cân nhắc, đối với Webtinhte chúng tôi lựa chon .NET và .Net Core để thực hiện dự án này. Vì đây là ngôn ngữ mạnh mẽ với tính bảo mật cao, được sử dụng rộng rãi trong các dự án web-application.</p>
                </div>
            </li>
            <li class="span6 it col">
                <h4 class="tit">GIAO DIỆN ĐƯỢC THIẾT KẾ RIÊNG VÀ ĐỘC QUYỀN</h4>
                <div class="desc">
                    <p>Chúng tôi biết được mỗi trung tâm là một style khác nhau vì vậy website cũng vậy chúng tôi không thể sử dụng chung một giao diện cho nhiều website khác nhau, Tại WEBTINHTE mỗi dự án đều được chúng tôi thiết kế giao diện riêng và độc quyền của riêng khách hàng, giúp họ có được một trang web với style duy nhất</p>
                </div>
            </li>
            <li class="span6 it col">
                <h4 class="tit">HIỂN THỊ TỐT TRÊN MỌI THIẾT BỊ DI ĐỘNG</h4>
                <div class="desc">
                    <p>Xu hướng khách hàng sử dụng di động ngày càng nhiều và website học trực tuyến luôn được khách hàng tận dụng để học trong thời gian di chuyển vì vậy đây là tính năng cần được tối ưu ngay từ đầu. Chúng tôi hiểu được điều đó nên giao diện website của bạn sẽ được hiển thị tốt trên mọi thiết bị di động như điện thoại và máy tính bảng.</p>
                </div>
            </li>
            <li class="span6 it col">
                <h4 class="tit">CÔNG NGHỆ BẢO MẬT ĐA TẦNG</h4>
                <div class="desc">
                    <p>học trực tuyến cần được trang bị những phương thức bảo mật hiện đại và chuyên nghiệp, Chúng tôi là một chuyên gia trong lĩnh vực bảo mật website. Ngay từ đầu chúng tôi đã tính toán để website bạn bảo mật nhất có thể thông qua các hệ thống và cách viết Code.</p>
                </div>
            </li>
        </ul>
    </div>
</section>