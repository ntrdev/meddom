﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.ViewControl" %>

<%
    var listItem = ViewBag.Data as List<ModNewsEntity>;
    var model = ViewBag.Model as MNewsModel;
    if (listItem == null) return;

%>
<div class="box_headding_news">
    <div class="box_link_tab_news">
        <%if (ViewPage.CurrentPage.Url == "tin-hoat-dong" || ViewPage.CurrentPage.Url == "tin-tuc")
            { %>
        <ul class="nav ">
            <li class="nav-item">
                <a class="nav-link" href="<%=ViewPage.GetURL("tin-video") %>">Tin video</a>
            </li>
            <li class="nav-item">
                <a class="nav-link active" href="#news_o" role="tab" data-toggle="tab">Tin hoạt động</a>
            </li>
        </ul>
        <%} %>
    </div>
    <div class="box_filter_news">
        <select id="sort" class="filter" data-url="<%=ViewPage.CurrentURL %>">
            <option value="new_asc" <%=ViewPage.SortMode=="new_asc" || ViewPage.SortMode==""?"selected":"" %>>Mới nhất</option>
            <option value="view_desc" <%=ViewPage.SortMode=="view_desc"?"selected":"" %>>Đọc nhiều</option>
        </select>
        <select id="page" class="filter" data-url="<%=ViewPage.CurrentURL %>">
            <option value="10" <%=model.PageSize==10?"selected":"" %>>Số tin hiển thị 10</option>
            <option value="20" <%=model.PageSize==20?"selected":"" %>>Số tin hiển thị 20</option>
            <option value="30" <%=model.PageSize==30?"selected":"" %>>Số tin hiển thị 30</option>
        </select>
    </div>
</div>
<div class="box_body_content">

    <ul class="list_news">
        <%for (int i = 0; i < listItem.Count; i++)
            {%>
        <li class="item_news">
            <div class="box_left_img">
                <div class="box_img">
                    <a href="<%=ViewPage.GetURL(0, listItem[i].Url) %>" class="clickall"></a>
                    <img src="<%=Utils.OptimalImage(listItem[i].File) %>" height="92" width="77" alt="">
                    <div class="time_news">
                        <span><%=Utils.FomartTime(listItem[i].Updated) %> </span>- <span><%=Utils.FormatDate2(listItem[i].Updated) %></span>
                    </div>
                </div>
            </div>
            <div class="box_text_r">
                <h4><a href="<%=ViewPage.GetURL(0, listItem[i].Url) %>"><%=listItem[i].Name %></a></h4>
                <p class="desc"><%=listItem[i].Summary %></p>
            </div>
        </li>
        <%} %>
    </ul>
    <ul class="pagination">
        <%= GetPagination("",model.page, model.PageSize, model.TotalRecord)%>
    </ul>
</div>
 <div class="loading" style="display: none;"><i class="icon">Loading</i></div>