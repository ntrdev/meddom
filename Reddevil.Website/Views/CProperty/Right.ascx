﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.ViewControl" %>

<script runat="server">
    public string GetAddAtr(List<WebPropertyEntity> listItem, int propertyID)
    {
        if (ArrAtr == null)
            return ViewPage.CurrentURL + "?atr=" + propertyID;

        var listAtr = new List<int>();
        listAtr.AddRange(ArrAtr);

        foreach (var item in listItem)
        {
            if (Array.IndexOf(ArrAtr, item.ID) > -1)
                listAtr.Remove(item.ID);
        }

        if (!listAtr.Contains(propertyID))
            listAtr.Add(propertyID);

        return ViewPage.CurrentURL + "?atr=" + string.Join("-", listAtr.ToArray());
    }

    public string GetRemoveAtr(int propertyID)
    {
        var listAtr = new List<int>();
        listAtr.AddRange(ArrAtr);

        if (listAtr.Contains(propertyID))
            listAtr.Remove(propertyID);

        if (listAtr.Count == 0)
            return ViewPage.CurrentURL;

        return ViewPage.CurrentURL + "?atr=" + string.Join("-", listAtr.ToArray());
    }

    int[] ArrAtr = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        ArrAtr = ViewBag.ArrAtr;
    }
</script>

<% 
    if (ViewPage.CurrentPage.ModuleCode != "MProduct" || ViewPage.ViewBag.Data != null) return;
    var listChildPage = ViewBag.Page as List<SysPageEntity>;
    if (listChildPage != null && listChildPage.Count == 0 && !string.Equals(ViewPage.CurrentVQS.GetString(0).ToLower(), "flash-sale", StringComparison.OrdinalIgnoreCase))
    {
        listChildPage = SysPageService.Instance.GetByParent_Cache(ViewPage.CurrentPage.ParentID);
    }

    var listItem = ViewBag.Data as Dictionary<WebPropertyEntity, List<WebPropertyEntity>>;
    var listBrand = ViewBag.Brand as List<ModBrandEntity>;

%>


<div class="col-md-3 col-left-find">
    <aside class="col-right-find" style="padding-right: 10px">
        <form method="get" accept-charset="utf-8" class="pca-pl-r">
            <%if (listChildPage != null && listChildPage.Count > 0)
                { %>
            <div class="box-filter all">


                <div class="sidebox-filter all">
                    <div class="title"><a href="<%=ViewPage.GetPageURL(ViewPage.CurrentPage)%>"><span><%=ViewPage.CurrentPage.Name %></span></a></div>

                    <ul class="list-menu">
                        <%foreach (var item in listChildPage)
                            {
                                var listGrandPage = SysPageService.Instance.GetByParent_Cache(item.ID); %>
                        <li class="col-xs-12">
                            <div class="item-cha">
                                <a href="<%=ViewPage.GetPageURL(item)%>"><%=item.Name %></a>
                                <div data-id="<%=item.ID %>" class="<%=listGrandPage!=null&&listGrandPage.Count > 0?"heading":"" %> fix"></div>
                            </div>
                            <%if (listGrandPage != null && listGrandPage.Count > 0)
                                { %>
                            <div class="bg-ft" data-id="<%=item.ID %>">
                                <ul class="list-mn">
                                    <%foreach (var itemGrandPage in listGrandPage)
                                        {
                                    %>
                                    <li><a href="<%=ViewPage.GetPageURL(itemGrandPage)%>"><%=itemGrandPage.Name %></a></li>
                                    <%} %>
                                </ul>

                            </div>
                            <%} %>
                        </li>
                        <%} %>
                    </ul>
                </div>

            </div>
            <%} %>
            <div class="box-filter all">
                <div class="sidebox-filter all">
                    <div class="title"><a href="javascript:void(0)"><span><i class="fa fa-filter"></i>&nbsp;Bộ lọc tìm kiếm</span></a></div>
                </div>
            </div>
            <%if (listBrand != null)
                { %>
            <div class="box-filter all">
                <strong class="title">Thương hiệu</strong>
                <ul class="row-checkbox mt20 all item-show-d-muc">
                    <%foreach (var item in listBrand)
                        {%>
                    <li data-url="<%=ViewPage.CurrentURL %>" class="hihi">
                        <div class="checkbox checkbox-danger">
                            <input id="<%=item.Code %><%=item.ID %>" type="checkbox" data-url="<%=ViewPage.CurrentURL %>" value="<%=item.Name %>">
                            <label for="<%=item.Code %><%=item.ID %>">
                                <%=item.Name%>
                            </label>
                        </div>
                    </li>
                    <%} %>
                </ul>
            </div>
            <%} %>
            <%if (listItem != null)
                { %>

            <%foreach (var item in listItem.Keys)
                {
                    var listChildItem = listItem[item];
            %>

            <div class="box-filter all">
                <strong class="title"><%=item.Name %></strong>
                <ul class="row-checkbox mt20 all item-show-d-muc loc-theo-danh-muc">
                    <%for (var i = 0; listChildItem != null && i < listChildItem.Count; i++)
                        {%>
                    <li data-url="<%=ViewPage.CurrentURL %>">
                        <div class="checkbox checkbox-danger">
                            <input id="<%=item.Code %><%=listChildItem[i].ID %>" type="checkbox" data-url="<%=ViewPage.CurrentURL %>" <%if (listChildItem[i].Selected)
                                {%>checked="checked"
                                <%} %> value="<%=listChildItem[i].ID %>">
                            <label for="<%=item.Code %><%=listChildItem[i].ID %>">
                                <%=listChildItem[i].Name%>
                            </label>
                        </div>
                    </li>
                    <%} %>
                </ul>
            </div>
            <%} %>
            <%} %>
        </form>
    </aside>
</div>
