﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.ViewControl" %>

<% 
    var listPage = ViewBag.Data as List<ModLibraryEntity>;
%>

<section class="sec_4">
    <div class="container">
        <div class="row">
            <%for (int i = 0; listPage != null && i < listPage.Count; i++)
                { %>
            <div class="col-12 col-lg-4">
                <div class="box_headding" style="text-align: center; display: block;">
                    <h2><a href="<%=ViewPage.GetURL(0,listPage[i].Url) %>" title="<%=listPage[i].Name %>"><%=listPage[i].Name %></a></h2>
                </div>
                <div class="featured-thumbnail">
                    <div class="thumbnail">
                        <a href="<%=ViewPage.GetURL(0,listPage[i].Url) %>" title="<%=listPage[i].Name %>">
                            <img src="<%=Utils.OptimalImage(listPage[i].Image) %>" alt="<%=listPage[i].Name %>" class="img-responsive img-hover">
                        </a>
                    </div>
                </div>
            </div>
            <%} %>
        </div>
    </div>
</section>
