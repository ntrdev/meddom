﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.ViewControl" %>

<%
    var listItem = ViewBag.Data as List<ModNewsGarnitureEntity>;
    var model = ViewBag.Model as MNewsGarnitureModel;
    if (listItem == null) return;
    var listPage = ViewBag.Page as List<SysPageEntity>;
%>

<div class="container">
    <div class="row">
        <div class="col-12 col-lg-8">
            <h1 class="page-header"><%=ViewPage.CurrentPage.Name %></h1>
        </div>
        <div class="col-12 col-lg-4">
            <div class="box_headding_news" style="padding-bottom: 16px; margin: 40px 0 20px; border-bottom: 1px solid #eee;">

                <div class="box_filter_news">
                    <select id="sort" class="filter" data-url="<%=ViewPage.CurrentURL %>">
                        <option value="new_asc" <%=ViewPage.SortMode=="new_asc" || ViewPage.SortMode==""?"selected":"" %>>Mới nhất</option>
                        <option value="view_desc" <%=ViewPage.SortMode=="view_desc"?"selected":"" %>>Đọc nhiều</option>
                    </select>
                    <select id="page" class="filter" data-url="<%=ViewPage.CurrentURL %>">
                        <option value="10" <%=model.PageSize==10?"selected":"" %>>Số tin hiển thị 10</option>
                        <option value="20" <%=model.PageSize==20?"selected":"" %>>Số tin hiển thị 20</option>
                        <option value="30" <%=model.PageSize==30?"selected":"" %>>Số tin hiển thị 30</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-4">
            <ul class="cd-accordion-menu animated">
                <li class="has-children">
                    <input type="checkbox" name="group-1" id="group-1" checked>
                    <label for="group-1">Trưng bày</label>
                    <ul>
                        <%for (int i = 0; listPage != null && i < listPage.Count; i++)
                            {
                                if (listPage[i].MenuID <= 0) continue;
                                List<ModNewsGarnitureEntity> listnews = null;
                                var childPage = SysPageService.Instance.GetByParent_Cache(listPage[i].ID);
                                if (childPage.Count < 1)
                                    listnews = ModNewsGarnitureService.Instance.CreateQuery()
                                                                       .WhereIn(o => o.MenuID, WebMenuService.Instance.GetChildIDForWeb_Cache("NewsGarniture", listPage[i].MenuID, ViewPage.CurrentLang.ID))
                                                                       .Take(4).OrderByDesc(o => o.ID).ToList_Cache();
                        %>
                        <li class="has-children">
                            <input type="checkbox" name="sub-group-<%=i + 1 %>" id="sub-group-<%=i + 1 %>">
                            <label for="sub-group-<%=i + 1 %>"><%=listPage[i].Name %></label>
                            <%if (childPage.Count > 0)
                                {  %>
                            <ul>
                                <%for (int j = 0; j < childPage.Count; j++)
                                    {
                                        listnews = ModNewsGarnitureService.Instance.CreateQuery()
                                                                        .WhereIn(o => o.MenuID, WebMenuService.Instance.GetChildIDForWeb_Cache("NewsGarniture", childPage[j].MenuID, ViewPage.CurrentLang.ID))
                                                                        .Take(4).OrderByDesc(o => o.ID).ToList_Cache();
                                %>
                                <li class="has-children">
                                    <input type="checkbox" name="sub-group-child-<%=j + 1 %>" id="sub-group-child-<%=j + 1 %>">
                                    <label for="sub-group-child-<%=j + 1 %>"><%=childPage[j].Name %></label>
                                    <ul>
                                        <%for (int z = 0; listnews != null && z < listnews.Count; z++)
                                            { %>
                                        <li><a href="<%=ViewPage.GetURL(0, listnews[z].Url) %>" title="<%=listnews[z].Name%>"><%=listnews[z].Name %></a></li>
                                        <%} %>
                                        <%if (listnews != null && listnews.Count > 3)
                                            {%>
                                        <li><a style="color: #009ef3" href="<%=ViewPage.GetPageURL(childPage[j]) %>"><i class="fa fa-angle-double-right"></i>&nbsp;Xem thêm</a></li>
                                        <%}%>
                                    </ul>
                                </li>
                                <%} %>
                            </ul>
                            <%}
                                else
                                {  %>
                            <ul>
                                <%for (int j = 0; listnews != null && j < listnews.Count; j++)
                                    { %>
                                <li><a href="<%=ViewPage.GetURL(0, listnews[j].Url) %>" title="<%=listnews[j].Name%>"><%=listnews[j].Name %></a></li>
                                <%} %>
                                <%if (listnews != null && listnews.Count > 3)
                                    {%>
                                <li><a style="color: #009ef3" href="<%=ViewPage.GetPageURL(listPage[i]) %>"><i class="fa fa-angle-double-right"></i>&nbsp;Xem thêm</a></li>
                                <%} %>
                            </ul>
                            <%} %>
                        </li>
                        <%} %>
                    </ul>
                </li>
            </ul>
            <!-- cd-accordion-menu -->
        </div>
        <div class="col-12 col-lg-8">


            <div class="box_body_content">

                <ul class="list_news">
                    <%for (int i = 0; i < listItem.Count; i++)
                        {%>
                    <li class="item_news">
                        <div class="box_left_img">
                            <div class="box_img">
                                <a href="<%=ViewPage.GetURL(0, listItem[i].Url) %>" class="clickall"></a>
                                <img src="<%=Utils.OptimalImage(listItem[i].File) %>" height="92" width="77" alt="">
                                <div class="time_news">
                                    <span><%=Utils.FomartTime(listItem[i].Updated) %> </span>- <span><%=Utils.FormatDate2(listItem[i].Updated) %></span>
                                </div>
                            </div>
                        </div>
                        <div class="box_text_r">
                            <h4><a href="<%=ViewPage.GetURL(0, listItem[i].Url) %>"><%=listItem[i].Name %></a></h4>
                            <p class="desc"><%=listItem[i].Summary %></p>
                        </div>
                    </li>
                    <%} %>
                </ul>
                <ul class="pagination">
                    <%= GetPagination("",model.page, model.PageSize, model.TotalRecord)%>
                </ul>
            </div>
        </div>
    </div>
</div>


