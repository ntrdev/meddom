﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.ViewControl" %>

<% 
    var item = ViewBag.Data as ModNewsGarnitureEntity;
    var listItem = ViewBag.Other as List<ModNewsGarnitureEntity>;
    var listLQ = ViewBag.Lienquan as List<ModNewsGarnitureEntity>;
    var listPage = ViewBag.Page as List<SysPageEntity>;

%>


<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><%=item.Name %></h1>
        </div>
        <div class="col-12 col-lg-4">
            <ul class="cd-accordion-menu animated">
                <li class="has-children">
                    <input type="checkbox" name="group-1" id="group-1" checked>
                    <label for="group-1">Trưng bày</label>
                    <ul>
                        <%for (int i = 0; listPage != null && i < listPage.Count; i++)
                            {
                                if (listPage[i].MenuID <= 0) continue;
                                List<ModNewsGarnitureEntity> listnews = null;
                                var childPage = SysPageService.Instance.GetByParent_Cache(listPage[i].ID);
                                if (childPage.Count < 1)
                                    listnews = ModNewsGarnitureService.Instance.CreateQuery()
                                                                       .WhereIn(o => o.MenuID, WebMenuService.Instance.GetChildIDForWeb_Cache("NewsGarniture", listPage[i].MenuID, ViewPage.CurrentLang.ID))
                                                                       .Take(4).OrderByDesc(o => o.ID).ToList_Cache();
                        %>
                        <li class="has-children">
                            <input type="checkbox" name="sub-group-<%=i + 1 %>" id="sub-group-<%=i + 1 %>">
                            <label for="sub-group-<%=i + 1 %>"><%=listPage[i].Name %></label>
                            <%if (childPage.Count > 0)
                                {  %>
                            <ul>
                                <%for (int j = 0; j < childPage.Count; j++)
                                    {
                                        listnews = ModNewsGarnitureService.Instance.CreateQuery()
                                                                        .WhereIn(o => o.MenuID, WebMenuService.Instance.GetChildIDForWeb_Cache("NewsGarniture", childPage[j].MenuID, ViewPage.CurrentLang.ID))
                                                                        .Take(4).OrderByDesc(o => o.ID).ToList_Cache();
                                %>
                                <li class="has-children">
                                    <input type="checkbox" name="sub-group-child-<%=j + 1 %>" id="sub-group-child-<%=j + 1 %>">
                                    <label for="sub-group-child-<%=j + 1 %>"><%=childPage[j].Name %></label>
                                    <ul>
                                        <%for (int z = 0; listnews != null && z < listnews.Count; z++)
                                            { %>
                                        <li><a href="<%=ViewPage.GetURL(0, listnews[z].Url) %>" title="<%=listnews[z].Name%>"><%=listnews[z].Name %></a></li>
                                        <%} %>
                                        <%if (listnews != null && listnews.Count > 3)
                                            {%>
                                        <li><a style="color: #009ef3" href="<%=ViewPage.GetPageURL(childPage[j]) %>"><i class="fa fa-angle-double-right"></i>&nbsp;Xem thêm</a></li>
                                        <%}%>
                                    </ul>
                                </li>
                                <%} %>
                            </ul>
                            <%}
                                else
                                {  %>
                            <ul>
                                <%for (int j = 0; listnews != null && j < listnews.Count; j++)
                                    { %>
                                <li><a href="<%=ViewPage.GetURL(0, listnews[j].Url) %>" title="<%=listnews[j].Name%>"><%=listnews[j].Name %></a></li>
                                <%} %>
                                <%if (listnews != null && listnews.Count > 3)
                                    {%>
                                <li><a style="color: #009ef3" href="<%=ViewPage.GetPageURL(listPage[i]) %>"><i class="fa fa-angle-double-right"></i>&nbsp;Xem thêm</a></li>
                                <%} %>
                            </ul>
                            <%} %>
                        </li>
                        <%} %>
                    </ul>
                </li>
            </ul>
            <!-- cd-accordion-menu -->
        </div>
        <div class="col-12 col-lg-8">


            <div class="box_body_content">
                <div class="box_title_post">
                    <div class="time_news">
                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                        <span><%=Utils.FormatDateTime(item.Updated) %></span>
                    </div>
                </div>
                <%if (!string.IsNullOrEmpty(item.UrlAudio))
                    { %>
                <audio controls autoplay>
                    <source src="<%=item.UrlAudio %>" type="audio/mpeg">
                </audio>
                <%} %>
                <%if (!string.IsNullOrEmpty(item.Summary))
                    { %>
                <p class="desc"><%=!item.Cost ? "<img src=\"/interface/pc/images/favicon.png\" alt=\"tin từ MEDDOM\" style=\" width:30px;height:30px;\"/>" : "" %> <b><%=item.Summary %></b></p>
                <%} %>
                <%=Utils.GetHtmlForSeo(item.Content) %>
            </div>

            <div class="box_like_share">
                <div class="fb-like" data-href="<%=ViewPage.GetURL(item.MenuID, item.Url) %>" data-width="" data-layout="button_count" data-action="like" data-size="large" data-share="true"></div>
            </div>
            <% if (listLQ != null && listLQ.Count > 0)
                { %>
            <div class="box_list_news_same">
                <h2>Tin liên quan</h2>
                <ul class="list_news_same owl-carousel owl-theme">
                    <%for (int i = 0; listLQ != null && i < listLQ.Count; i++)
                        { %>
                    <li class="item_news_same">
                        <div class="box_img">
                            <a href="<%=ViewPage.GetURL(0,listLQ[i].Url) %>" class="clickall"></a>
                            <img src="<%=Utils.OptimalImage(listLQ[i].File) %>" height="auto" width="100%" alt="">
                            <div class="time_news">
                                <span><%=Utils.FomartTime(listLQ[i].Updated) %></span>- <span><%=Utils.FormatDate(listLQ[i].Updated) %></span>
                            </div>
                        </div>
                        <div class="box_info">
                            <h4><a href="<%=ViewPage.GetURL(0,listLQ[i].Url) %>"><%=listLQ[i].Name %></a></h4>
                        </div>
                    </li>
                    <%} %>
                </ul>
            </div>
            <%} %>
            <%if (listItem != null && listItem.Count > 0)
                { %>
            <div class="box_list_news_same">
                <h2>Tin tức khác</h2>
                <ul class="list_news_same owl-carousel owl-theme">
                    <%for (int i = 0; listItem != null && i < listItem.Count; i++)
                        { %>
                    <li class="item_news_same">
                        <div class="box_img">
                            <a href="<%=ViewPage.GetURL(0,listItem[i].Url) %>" class="clickall"></a>
                            <img src="<%=Utils.OptimalImage(listItem[i].File) %>" height="auto" width="100%" alt="">
                            <div class="time_news">
                                <span><%=Utils.FomartTime(listItem[i].Updated) %></span>- <span><%=Utils.FormatDate(listItem[i].Updated) %></span>
                            </div>
                        </div>
                        <div class="box_info">
                            <h4><a href="<%=ViewPage.GetURL(0,listItem[i].Url) %>"><%=listItem[i].Name %></a></h4>
                        </div>
                    </li>
                    <%} %>
                </ul>
            </div>
            <%} %>
        </div>
    </div>
</div>
