﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.ViewControl" %>

<%
    var listItem = ViewBag.Data as List<ModNewsGarnitureEntity>;
    var model = ViewBag.Model as MNewsGarnitureModel;
    if (listItem == null) return;

%>

<div class="box_body_content">

    <div class="itembox_news">
        <h4 class="title_content"><%=ViewPage.CurrentPage.Name %></h4>
        <ul class="list_item_o list_trungbay_online">
            <%for (int i = 0; i < listItem.Count; i++)
                {%>
            <li class="item_news_o">
                <div class="box_img">
                    <a href="<%=ViewPage.GetURL(0, listItem[i].Url) %>" class="clickall"></a>
                    <img src="<%=Utils.OptimalImage(listItem[i].File) %>" height="92" width="77" alt="">
                </div>
                <div class="box_info">
                    <h4><a href="<%=ViewPage.GetURL(0, listItem[i].Url) %>" class=""><%=listItem[i].Name %></a></h4>
                </div>
            </li>
            <%} %>
        </ul>

        <ul class="pagination">
               <%= GetPagination("",model.page, model.PageSize, model.TotalRecord)%>
        </ul>
    </div>

</div>
