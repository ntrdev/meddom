﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.ViewControl" %>

<% 
    var item = ViewBag.Data as ModPSEntity;
    var listFile = item.GetFile;
    var listItem = ViewBag.Other as List<ModPSEntity>;
%>

<link type="text/css" href="<%=Static.Tag("/interface/utils/fancybox/jquery.fancybox.css") %>" rel="stylesheet" />
<div class="box_body_content">
    <div class="box_title_post">
        <h4><%=item.Name %></h4>
        <div class="time_news">
            <i class="fa fa-clock-o" aria-hidden="true"></i>
            <span><%=Utils.FormatDateTime(item.DateCreated) %></span>
        </div>
    </div>
     <%=Utils.GetHtmlForSeo(item.Description) %>
     <div class="tab-content mt-3">
        <div role="tabpanel" class="tab-pane fade in active show" id="img_nhakhoahoc">
            <div class="itembox_news">
                <ul class="list_item_o list_3_item">
                       <li class="item_news_o">
                        <div class="box_img" style="border-radius:5px">
                            <a href="<%=Utils.OptimalImage(item.Image) %>" data-fancybox="preview" class="fancybox" data-img="<%=Utils.OptimalImage(item.Image) %>" data-bigimg="<%=Utils.OptimalImage(item.Image) %>">
                                <img src="<%=Utils.OptimalImage(item.Image) %>" alt="<%=item.Name %>"></a>
                        </div>
                    </li>
                    <%for (int i = 0; listFile != null && i < listFile.Count; i++)
                        { %>
                    <li class="item_news_o">
                        <div class="box_img" style="border-radius:5px">
                            <a href="<%=Utils.OptimalImage(listFile[i].File) %>" data-fancybox="preview" class="fancybox" data-img="<%=Utils.OptimalImage(listFile[i].File) %>" data-bigimg="<%=Utils.OptimalImage(listFile[i].File) %>">
                                <img src="<%=Utils.OptimalImage(listFile[i].File) %>" alt="<%=item.Name %>"></a>
                        </div>
                    </li>
                    <%} %>
                </ul>
            </div>

        </div>
    </div>

    <div class="box_like_share">
        <div class="fb-like" data-href="<%=ViewPage.GetURL(item.CategoryID, item.Url) %>" data-width="" data-layout="button_count" data-action="like" data-size="large" data-share="true"></div>
    </div>

    <% if (listItem != null)
        { %>
    <div class="box_list_news_same">
        <h2>Lưu bút khác</h2>
        <ul class="list_news_same owl-carousel owl-theme">
            <%for (int i = 0; listItem != null && i < listItem.Count; i++)
                { %>
            <li class="item_news_same">
                <div class="box_img">
                    <a href="<%=ViewPage.GetURL(0, listItem[i].Url) %>" class="clickall"></a>
                    <img src="<%=Utils.OptimalImage(listItem[i].Image) %>" height="auto" width="100%" alt="">
                    <div class="time_news">
                        <span><%=Utils.FomartTime(listItem[i].DateCreated) %></span>- <span><%=Utils.FormatDate(listItem[i].DateCreated) %></span>
                    </div>
                </div>
                <div class="box_info">
                    <h4><a href="<%=ViewPage.GetURL(0, listItem[i].Url) %>"><%=listItem[i].Name %></a></h4>
                </div>
            </li>
            <%} %>
        </ul>
    </div>
    <%} %>
</div>
<script type="text/javascript" src="<%=Static.Tag("/interface/utils/fancybox/jquery.fancybox.js") %>"></script>
<script type="text/javascript">
    $(".fancybox").fancybox({
        animationEffect: "zoom-in-out",
        transitionEffect: "tube"
    });

</script>
