﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.ViewControl" %>

<%
    var listItem = ViewBag.Data as List<ModPSEntity>;
    var model = ViewBag.Model as MPSModel;

%>

<link type="text/css" href="<%=Static.Tag("/interface/utils/fancybox/jquery.fancybox.css") %>" rel="stylesheet" />
<div class="box_body_content">
    <div class="sidebar-title">
        <a href="javascript:void(0)"><%=ViewPage.CurrentPage.Name %></a>
    </div>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade in active show" id="img_nhakhoahoc">
            <div class="itembox_news">
                <ul class="list_item_o list_3_item">
                    <%for (int i = 0; listItem != null && i < listItem.Count; i++)
                        { %>
                    <li class="item_news_o">
                        <div class="box_img">
                            <a href="<%=Utils.OptimalImage(listItem[i].Image) %>" data-fancybox="preview" class="fancybox" data-img="<%=Utils.OptimalImage(listItem[i].Image) %>" data-bigimg="<%=Utils.OptimalImage(listItem[i].Image) %>">
                                <img src="<%=Utils.OptimalImage(listItem[i].Image) %>" alt="<%=listItem[i].Name %>"></a>
                        </div>
                        <div class="box_info">
                            <h4><a href="<%=ViewPage.GetURL(0,listItem[i].Url) %>"><%=listItem[i].Name %></a></h4>
                        </div>
                    </li>
                    <%} %>
                </ul>

                <ul class="pagination">
                    <%= GetPagination("",model.page, model.PageSize, model.TotalRecord)%>
                </ul>
            </div>

        </div>
    </div>
</div>
<script type="text/javascript" src="<%=Static.Tag("/interface/utils/fancybox/jquery.fancybox.js") %>"></script>
<script type="text/javascript">
    $(".fancybox").fancybox({
        animationEffect: "zoom-in-out",
        transitionEffect: "tube"
    });

</script>
