﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.ViewControl" %>

<% 
    var listItem = ViewBag.Data as ModAdvEntity;
    if (listItem == null) return;
%>


<div class="hder_mid">
    <div class="container logo-container">

        <div class="logo">
            <a href="<%=CoreMr.Reddevil.Web.HttpRequest.Domain %>" title="<%=CoreMr.Reddevil.Web.HttpRequest.Host%>">
                <img src="<%= Utils.OptimalImage(listItem.File)%>" title="<%=listItem.Name %>" /></a>
        </div>
        <div class="slogan">
            <h2>Trung tâm Di sản các nhà khoa học việt nam</h2>
            <p>Heritage Center for scientists and scholars of Vietnam</p>
        </div>
        <div class="hamburger-menu">
            <div class="bar"></div>
        </div>
    </div>
</div>
