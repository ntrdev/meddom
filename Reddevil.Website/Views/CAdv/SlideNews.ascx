﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.ViewControl" %>
<% 
    var listView = ViewBag.Data as List<ModAdvEntity>;
    if (listView == null) return;
%>

<section class="box_slide_index">
    <div class="slide_index owl-carousel owl-theme slide_index-home">
        <%for (int i = 0; i < listView.Count; i++)
            { %>
        <div class="item">
            <div class="box_img">
                <a href="<%= listView[i].URL %>" title="<%=listView[i].Name %>">
                    <img src="<%=Utils.DefaultImage(listView[i].File) %>" alt="<%=listView[i].Name %>"></a>

            </div>
            <div class="mota_slide">
                <div class="noidung">
                    <img src="/interface/pc/images/phay.png"><%=listView[i].Summary %>
                </div>
                <a class="xemthem" href="<%=listView[i].URL %>" target="<%=listView[i].Target %>" tabindex="-1">Xem chi tiết</a>
            </div>
        </div>
        <%} %>
    </div>
</section>
