﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.ViewControl" %>

<section id="featured">
    <div class="camera_wrap" id="camera-slide">
       
        <div data-src="/interface/pc/images/slides/camera/slide1/img1.jpg">
            <div class="camera_caption fadeFromLeft">
                <div class="container">
                    <div class="row">
                        <div class="span6">
                            <h2 class="animated fadeInDown"><strong>Thiết Kế <span class="colored">Website</span></strong></h2>
                            <p class="animated fadeInUp">
                                    Điều đặc biệt nhất khi thiết kế website ở Webtinhte là bạn không cần phải lo lắng về trình độ kỹ thuật,
                                    thẩm mỹ thiết kế hay hậu mãi, bảo trì, bảo hành, bảo mật… 
                                    Tất cả những gì bạn cần chuẩn bị một ý tưởng rõ ràng để cùng Project Manager của chúng tôi trao đổi tư vấn cho bạn. Webtinhte sẽ phân tích ưu/khuyết, đưa ra những tính năng phù hợp nhất cho kế hoạch, phương hướng phát triển, độ phù hợp với thị trường và kinh phí dự kiến</p>
                            <a href="<%=ViewPage.OrderBuyedUrl %>" class="btn btn-success btn-large animated fadeInUp">
                                <i class="icon-link"></i>Đặt hàng ngay
                            </a>
                            <a href="" class="btn btn-theme btn-large animated fadeInUp">
                                <i class="icon-download"></i>Thiết kế website
                            </a>
                        </div>
                        <div class="span6">
                            <img src="/interface/pc/images/slides/camera/slide1/screen.png" alt="" class="animated bounceInDown delay1" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div data-src="/interface/pc/images/slides/camera/slide2/img1.jpg">
            <div class="camera_caption fadeFromLeft">
                <div class="container">
                    <div class="row">
                        <div class="span6">
                            <img src="/interface/pc/images/slides/camera/slide2/iMac.png" alt="Nhập email để nhận các tin cũng như ưu đãi tới từ IMEXsoft®" />
                        </div>
                        <div class="span6">
                            <h2 class="animated fadeInDown"><strong>Đăng ký <span class="colored">nhận tin ưu đãi.</span></strong></h2>
                            <p class="animated fadeInUp">Bạn có thể gửi nhập email và đội ngũ chăm sóc khách hàng của chúng tôi sẽ liên lạc với bạn.</p>
                            <form>
                                <div class="input-append">
                                    <input class="span3 input-large" id="email_subcribe" value="" type="text">
                                    <button class="btn btn-theme btn-large subcribe_submit"  type="button">Đăng ký</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div data-src="/interface/pc/images/slides/camera/slide2/img1.jpg">
            <div class="camera_caption fadeFromLeft">
                <div class="container">
                    <div class="row">
                        <div class="span12 aligncenter">
                            <h2 class="animated fadeInDown"><strong><span class="colored">Responsive</span> và <span class="colored">khả năng tương thích với các trình duyệt.</span></strong></h2>
                            <p class="animated fadeInUp">Với kinh nghiệm, cập nhật các công nghệ mới để tương tích và tối ưu với tất cả các trình duyệt.</p>
                            <img src="/interface/pc/images/slides/camera/slide3/browsers.png" alt="Tương thích với các trình duyệt" class="animated bounceInDown delay1" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>