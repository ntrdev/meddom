﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.ViewControl" %>

<% 
    var _page = ViewBag.Page as SysPageEntity;
    var listPage = ViewBag.ListPage as List<SysPageEntity>;
    var listItem = ViewBag.ListView as List<ModNewsResearchEntity>;
%>
<section class="sec_2">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="box_headding">
                    <h2><a href="<%=ViewPage.GetURL(0, _page.Url) %>"><%=_page.Name %></a></h2>
                    <ul class="menu_hd">
                        <%for (int i = 0; listPage != null && i < listPage.Count; i++)
                            {%>
                        <li><a href="<%=ViewPage.GetURL(listPage[i].MenuID,listPage[i].Url) %>"><%=listPage[i].Name %></a></li>
                        <%} %>
                    </ul>

                </div>
                <div class="list_nghiencuu owl-carousel owl-theme">
                    <%for (int i = 0; listItem != null && i < listItem.Count; i++)
                        {%>
                    <div class="item">
                        <div class="box_img">
                            <a href="<%=ViewPage.GetURL(listItem[i].MenuID, listItem[i].Url) %>" class="clickall"></a>
                            <img src="<%=Utils.OptimalImage(listItem[i].File) %>" alt="">
                        </div>
                        <div class="box_content">
                            <h4><a href="<%=ViewPage.GetURL(listItem[i].MenuID, listItem[i].Url) %>"><%=listItem[i].Name %></a></h4>
                            <p class="desc"><%=listItem[i].Summary %></p>
                            <p class="date_news">
                                <i class="fa fa-calendar" aria-hidden="true"></i>
                                <%=Utils.FormatDate(listItem[i].Published) %>
                            </p>
                        </div>
                    </div>
                    <%} %>
                </div>
            </div>
        </div>

    </div>
</section>
