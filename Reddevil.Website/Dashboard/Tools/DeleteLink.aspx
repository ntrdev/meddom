﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<script runat="server">

    protected void Page_Load(object sender, EventArgs e)
    {
        if (CPLogin.IsLogin() && CPLogin.CurrentUser.IsAdministrator) return;

        Response.Redirect("Login.aspx?ReturnPath=" + Server.UrlEncode(Request.RawUrl));
    }

    protected void btnRun_Click(object sender, EventArgs e)
    {
        if (txtSQL.Text == string.Empty) { Literal1.Text = "Nhập mã url để xóa "; return; }
        var code = txtSQL.Text;
        var listCleanUrl = ModCleanURLService.Instance.CreateQuery().Where(o => o.Code == code).OrderByAsc(o => o.ID).ToList();
        if (listCleanUrl == null)
        {
             Literal1.Text = "Chưa tồn tại link này "; return; 
        }
        if (listCleanUrl != null && listCleanUrl.Count < 2)
        {
            Literal1.Text = "Không bị trùng";
            return;
        };
        if (listCleanUrl != null && listCleanUrl.Count >= 2)
        {
            ModCleanURLService.Instance.Delete(listCleanUrl[0]);

            Literal1.Text = "Đã xóa " + listCleanUrl[0].ID;
        }
    }
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>SQL</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div style="text-align: center;">
                <br />
                <asp:TextBox ID="txtSQL" TextMode="MultiLine" runat="server"></asp:TextBox>
                <br />
                <asp:Button ID="btnRun" runat="server" OnClick="btnRun_Click" Text="Thực hiện" Width="111px" />
                <br />
                <br />
                <asp:GridView ID="gvSQL" runat="server"></asp:GridView>
                <asp:Literal ID="Literal1" runat="server"></asp:Literal>
            </div>
        </div>
    </form>
</body>
</html>
