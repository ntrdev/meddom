﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<script runat="server">

    protected void Page_Load(object sender, EventArgs e)
    {
        if (CPLogin.IsLogin() && CPLogin.CurrentUser.IsAdministrator) return;

        Response.Redirect("Login.aspx?ReturnPath=" + Server.UrlEncode(Request.RawUrl));
    }
    private static int GetMaxOrder()
    {
        return ModNewsResearchService.Instance.CreateQuery()
                .Max(o => o.Order)
                .ToValue().ToInt(0) + 1;
    }


    protected void btnRun_Click(object sender, EventArgs e)
    {
        int hihi = 0;
        var listVideo = ModNewsResearchService.Instance.CreateQuery().Select(o=>new { o.Name, o.MenuID, o.ID, o.Url }).ToList_Cache();
        for (int i = 0; i < listVideo.Count; i++)
        {
            listVideo[i].Url = Data.GetCode(listVideo[i].Name) + "-" + Reddevil.Lib.Global.Random.GetRandom(4).ToLower();
            ModNewsResearchService.Instance.Save(listVideo[i], o => o.Url);
            ModCleanURLService.Instance.InsertOrUpdate(listVideo[i].Url, "NewsResearch", listVideo[i].ID, listVideo[i].MenuID, 1);
            hihi++;
        }
        Literal1.Text = hihi.ToString();
    }
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>SQL</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div style="text-align: center;">
                <asp:Button ID="btnRun" runat="server" OnClick="btnRun_Click" Text="Run" Width="111px" />
                <br />
                <asp:Literal ID="Literal2" runat="server"></asp:Literal>
                <asp:Literal ID="Literal1" runat="server"></asp:Literal>
            </div>
        </div>
    </form>
</body>
</html>
