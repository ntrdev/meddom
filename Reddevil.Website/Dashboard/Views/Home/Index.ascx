﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.CPViewControl" %>

<% 
    var listModule = Reddevil.Lib.Web.Application.CPModules.Where(o => o.ShowInMenu == true).OrderBy(o => o.Order).ToList();
    //logs
    var listUserLog = CPUserLogService.Instance.CreateQuery().Take(12).OrderByDesc(o => o.ID).ToList_Cache();
    int TotalTopic = WebMenuService.Instance.CreateQuery().Where(o => o.ParentID > 0 && o.Type != "City").Count().ToValue_Cache().ToInt(0);
    int TotalNews = ModNewsService.Instance.CreateQuery().Select(o => o.ID).Count().ToValue_Cache().ToInt(0);
    int TotalNewsVideo = ModNewsVideoService.Instance.CreateQuery().Select(o => o.ID).Count().ToValue_Cache().ToInt(0);
    int TotalFeedback = ModFeedbackService.Instance.CreateQuery().Select(o => o.ID).Count().ToValue_Cache().ToInt(0);

    var listNews = ModNewsService.Instance.CreateQuery().OrderByDesc(o => o.Order).Take(10).ToList_Cache();
    var listNewsVideo = ModNewsVideoService.Instance.CreateQuery().OrderByDesc(o => o.Order).Take(10).ToList_Cache();
    var listLogs = CPUserLogService.Instance.CreateQuery().OrderByDesc(o => o.ID).Take(10).ToList_Cache();

%>

<%= ShowMessage()%>

<div class="pcoded-content">
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8 d-none d-lg-block">
                <div class="page-header-title">
                    <i class="icon-home"></i>
                    <div class="d-inline">
                        <h5>Bảng điều khiển</h5>
                        <span>Thống kê dữ liệu hệ thống</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class=" breadcrumb breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="/{CPPath}/">
                                <i class="icon-home"></i>
                            </a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="/{CPPath}/">Bảng điều khiển</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="row">
                        <%-- <div class="col-xl-12">
                            <div class="card product-progress-card">
                                <div class="card-block">
                                    <div class="row pp-main">
                                        <div class="col-xl-3 col-md-6">
                                            <div class="pp-cont">
                                                <div class="row align-items-center m-b-20">
                                                    <div class="col-auto">
                                                        <i class="fas fa-cube f-24 text-mute"></i>
                                                    </div>
                                                    <div class="col text-right">
                                                        <h2 class="m-b-0 text-c-blue">2476</h2>
                                                    </div>
                                                </div>
                                                <div class="row align-items-center m-b-15">
                                                    <div class="col-auto">
                                                        <p class="m-b-0">Total Product</p>
                                                    </div>
                                                    <div class="col text-right">
                                                        <p class="m-b-0 text-c-blue"><i class="fas fa-long-arrow-alt-up m-r-10"></i>64%</p>
                                                    </div>
                                                </div>
                                                <div class="progress">
                                                    <div class="progress-bar bg-c-blue" style="width: 45%"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-3 col-md-6">
                                            <div class="pp-cont">
                                                <div class="row align-items-center m-b-20">
                                                    <div class="col-auto">
                                                        <i class="fas fa-tag f-24 text-mute"></i>
                                                    </div>
                                                    <div class="col text-right">
                                                        <h2 class="m-b-0 text-c-red">843</h2>
                                                    </div>
                                                </div>
                                                <div class="row align-items-center m-b-15">
                                                    <div class="col-auto">
                                                        <p class="m-b-0">New Orders</p>
                                                    </div>
                                                    <div class="col text-right">
                                                        <p class="m-b-0 text-c-red"><i class="fas fa-long-arrow-alt-down m-r-10"></i>34%</p>
                                                    </div>
                                                </div>
                                                <div class="progress">
                                                    <div class="progress-bar bg-c-red" style="width: 75%"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-3 col-md-6">
                                            <div class="pp-cont">
                                                <div class="row align-items-center m-b-20">
                                                    <div class="col-auto">
                                                        <i class="fas fa-random f-24 text-mute"></i>
                                                    </div>
                                                    <div class="col text-right">
                                                        <h2 class="m-b-0 text-c-yellow">63%</h2>
                                                    </div>
                                                </div>
                                                <div class="row align-items-center m-b-15">
                                                    <div class="col-auto">
                                                        <p class="m-b-0">Conversion Rate</p>
                                                    </div>
                                                    <div class="col text-right">
                                                        <p class="m-b-0 text-c-yellow"><i class="fas fa-long-arrow-alt-up m-r-10"></i>64%</p>
                                                    </div>
                                                </div>
                                                <div class="progress">
                                                    <div class="progress-bar bg-c-yellow" style="width: 65%"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-3 col-md-6">
                                            <div class="pp-cont">
                                                <div class="row align-items-center m-b-20">
                                                    <div class="col-auto">
                                                        <i class="fas fa-dollar-sign f-24 text-mute"></i>
                                                    </div>
                                                    <div class="col text-right">
                                                        <h2 class="m-b-0 text-c-green">41M</h2>
                                                    </div>
                                                </div>
                                                <div class="row align-items-center m-b-15">
                                                    <div class="col-auto">
                                                        <p class="m-b-0">Conversion Rate</p>
                                                    </div>
                                                    <div class="col text-right">
                                                        <p class="m-b-0 text-c-green"><i class="fas fa-long-arrow-alt-up m-r-10"></i>54%</p>
                                                    </div>
                                                </div>
                                                <div class="progress">
                                                    <div class="progress-bar bg-c-green" style="width: 35%"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>--%>
                        <div class="col-xl-4 col-md-6">
                            <a href="/{CPPath}/ModNews/Index.aspx" class="card prod-p-card card-red">
                                <div class="card-body">
                                    <div class="row align-items-center m-b-30">
                                        <div class="col">
                                            <h3 class="m-b-0 f-w-700 text-white">Tin video
                                            </h3>
                                        </div>
                                        <div class="col-auto">
                                            <i class="icon-social-dropbox text-c-red f-18"></i>
                                        </div>
                                    </div>
                                    <h6 class="m-b-5 text-white">Số lượng hiện tại: [<%=TotalNewsVideo %>]</h6>
                                </div>
                            </a>
                        </div>
                        <div class="col-xl-4 col-md-6">
                            <a href="/{CPPath}/ModNewsVideo/Index.aspx" class="card prod-p-card card-green">
                                <div class="card-body">
                                    <div class="row align-items-center m-b-30">
                                        <div class="col">
                                            <h3 class="m-b-0 f-w-700 text-white">Tổng số bài viết
                                            </h3>
                                        </div>
                                        <div class="col-auto">
                                            <i class="icon-feed text-c-green f-18"></i>
                                        </div>
                                    </div>
                                    <h6 class="m-b-5 text-white">Số lượng hiện tại: [<%=TotalNews %>]</h6>
                                </div>
                            </a>
                        </div>

                        <div class="col-xl-4 col-md-6">
                            <a href="/{CPPath}/ModFeedback/Index.aspx" class="card prod-p-card card-yellow">
                                <div class="card-body">
                                    <div class="row align-items-center m-b-30">
                                        <div class="col">
                                            <h3 class="m-b-0 f-w-700 text-white">Phản hồi</h3>
                                        </div>
                                        <div class="col-auto">
                                            <i class="icon-graduation text-c-yellow f-18"></i>
                                        </div>
                                    </div>
                                    <h6 class="m-b-5 text-white">Số lượng hiện tại: [<%=TotalFeedback %>]</h6>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-6 col-md-6">
                            <div class="card table-card">
                                <div class="card-header">
                                    <h5>Bài viết gần đây</h5>
                                    <div class="card-header-right">
                                        <ul class="list-unstyled card-option" style="width: 30px;">
                                            <li class="first-opt" style=""><i
                                                class="feather open-card-option icon-chevron-left"></i>
                                            </li>
                                            <li><i class="feather icon-maximize full-card"></i></li>
                                            <li><i class="feather icon-refresh-cw reload-card"></i>
                                            </li>
                                            <li><i
                                                class="feather open-card-option icon-chevron-left"></i>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-block p-b-0">
                                    <div class="table-responsive">
                                        <table class="table table-hover m-b-0">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Tên tiêu đề</th>
                                                    <th>Ngày tạo</th>
                                                    <th>Người tạo</th>
                                                    <th>Trạng thái</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <%for (int i = 0; listNews != null && i < listNews.Count; i++)
                                                    {%>
                                                <tr>
                                                    <td><%=i+1 %></td>
                                                    <td><a href="/<%=listNews[i].Url %><%=Setting.Sys_PageExt %>"><%=listNews[i].Name %></a></td>
                                                    <td><%=Utils.FormatDate(listNews[i].Published) %></td>
                                                    <td><%=listNews[i].AdminEdit %></td>
                                                    <td>
                                                        <%= GetHideOrShow(listNews[i].ID, listNews[i].Activity)%>
                                                    </td>
                                                </tr>
                                                <%} %>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-6 col-md-6">
                            <div class="card table-card">
                                <div class="card-header">
                                    <h5>Tin video gần đây</h5>
                                    <div class="card-header-right">
                                        <ul class="list-unstyled card-option" style="width: 30px;">
                                            <li class="first-opt" style=""><i
                                                class="feather open-card-option icon-chevron-left"></i>
                                            </li>
                                            <li><i class="feather icon-maximize full-card"></i></li>
                                            <li><i class="feather icon-refresh-cw reload-card"></i>
                                            </li>
                                            <li><i
                                                class="feather open-card-option icon-chevron-left"></i>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-block p-b-0">
                                    <div class="table-responsive">
                                        <table class="table table-hover m-b-0">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Tên tiêu đề</th>
                                                    <th>Ngày tạo</th>
                                                    <th>Người tạo</th>
                                                    <th>Trạng thái</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <%for (int i = 0; listNewsVideo != null && i < listNewsVideo.Count; i++)
                                                    {%>
                                                <tr>
                                                    <td><%=i+1 %></td>
                                                    <td><a href="/<%=listNewsVideo[i].Url %><%=Setting.Sys_PageExt %>"><%=listNewsVideo[i].Name %></a></td>
                                                    <td><%=Utils.FormatDate(listNewsVideo[i].Published) %></td>
                                                    <td><%=listNewsVideo[i].AdminEdit %></td>
                                                    <td>
                                                        <%= GetHideOrShow(listNewsVideo[i].ID, listNewsVideo[i].Activity)%>
                                                    </td>
                                                </tr>
                                                <%} %>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-12 col-md-12">
                            <div class="card table-card">
                                <div class="card-header">
                                    <h5>Lịch sử hoạt động</h5>
                                    <div class="card-header-right">
                                        <ul class="list-unstyled card-option" style="width: 30px;">
                                            <li class="first-opt" style=""><i
                                                class="feather open-card-option icon-chevron-left"></i>
                                            </li>
                                            <li><i class="feather icon-maximize full-card"></i></li>
                                            <li><i class="feather icon-refresh-cw reload-card"></i>
                                            </li>
                                            <li><i
                                                class="feather open-card-option icon-chevron-left"></i>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-block p-b-0">
                                    <div class="table-responsive">
                                        <table class="table table-hover m-b-0">
                                            <thead>
                                                <tr>
                                                    <th>Stt</th>
                                                    <th>Tài khoản</th>
                                                    <th>Họ tên</th>
                                                    <th>Thời gian</th>
                                                    <th>Hành động</th>
                                                    <th>Module</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <%for (int i = 0; listLogs != null && i < listLogs.Count; i++)
                                                    {%>
                                                <tr>
                                                    <td><%=i+1 %></td>
                                                    <td><%=listLogs[i].GetUser().LoginName %></td>
                                                    <td><%=listLogs[i].GetUser().Name %></td>
                                                    <td><%=Utils.FormatDateTime(listLogs[i].Created) %></td>
                                                    <td><%=listLogs[i].TypeAction %></td>
                                                    <td><%=listLogs[i].Module %></td>
                                                </tr>
                                                <%} %>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>





