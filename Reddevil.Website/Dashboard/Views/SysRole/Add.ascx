﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.CPViewControl" %>

<script runat="server">

    List<CPAccessEntity> listUserAccess = null;
    SysRoleModel model;
    CPRoleEntity item;
    protected void Page_Load(object sender, EventArgs e)
    {
        model = ViewBag.Model as SysRoleModel;
        item = ViewBag.Data as CPRoleEntity;

        if (model.RecordID > 0)
        {
            listUserAccess = CPAccessService.Instance.CreateQuery()
                                 .Where(o => o.Type == "CP.MODULE" && o.RoleID == model.RecordID)
                                 .ToList();
        }
    }

    int GetAccess(string refCode)
    {
        if (listUserAccess == null)
            return 0;

        var obj = listUserAccess.Find(o => o.RefCode == refCode);

        return obj == null ? 0 : obj.Value;
    }
</script>

<form id="reddevilForm" name="reddevilForm" method="post">

    <input type="hidden" id="_reddevil_action" name="_reddevil_action" />

    <div class="pcoded-content">
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8 d-none d-lg-block">
                    <div class="page-header-title">
                        <i class="icon-social-dropbox"></i>
                        <div class="d-inline">
                            <h5>Quản lý Trang(Menu)</h5>
                            <span><%= model.RecordID > 0 ? "Cập nhật quyền "+item.Name : "Thêm mới Quyền"%></span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class=" breadcrumb breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="/{CPPath}/">
                                    <i class="icon-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="/{CPPath}/<%=CPViewPage.CurrentModule.Code%>/Index.aspx">Phân Quyền</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <%= ShowMessage()%>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="card content-editor">
                                    <div class="card-header header-control">
                                        <h5><%= model.RecordID > 0 ? "Cập nhật Quyền "+item.Name : "Thêm mới Quyền"%></h5>
                                        <div class="controler-header">
                                            <%=GetDefaultAddCommand() %>
                                        </div>
                                    </div>
                                    <div class="card-block pdt-0">

                                        <div class="tab-content">
                                            <div class="tab-pane active" id="thongtinchung"
                                                role="tabpanel">
                                                <h4 class="sub-title">Thông tin Quyền</h4>
                                                <div class="edit-margin-mb form-manager">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <div class="col-sm-12">
                                                                        <label class="col-sm-12 col-form-label">Tên Nhóm Quyền:</label>
                                                                        <input type="text" class="form-control" id="Name" name="Name" value="<%=item.Name %>" placeholder="nhập vào đây, tối đa 200 ký tự ...">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <label class="col-md-3 col-form-label text-right">Quyền:</label>
                                                        <div class="col-md-9">
                                                            <div class="table-scrollable">
                                                                <table class="table table-striped table-hover table-bordered dataTable">
                                                                    <thead>
                                                                        <tr>
                                                                            <th class="sorting_disabled w1p">#</th>
                                                                            <th class="sorting_disabled w10p text-center">
                                                                                <p>Duyệt</p>
                                                                                <label class="itemCheckBox itemCheckBox-sm">
                                                                                    <input type="checkbox" onclick="javascript: rdv_checkAll(document.forms[0], 'ArrApprove', this.checked)" />
                                                                                    <i class="check-box"></i>
                                                                                </label>
                                                                            </th>
                                                                            <th class="sorting_disabled w10p text-center">
                                                                                <p>Xóa</p>
                                                                                <label class="itemCheckBox itemCheckBox-sm">
                                                                                    <input type="checkbox" onclick="javascript: rdv_checkAll(document.forms[0], 'ArrDelete', this.checked)" />
                                                                                    <i class="check-box"></i>
                                                                                </label>
                                                                            </th>
                                                                            <th class="sorting_disabled w10p text-center">
                                                                                <p>Sửa</p>
                                                                                <label class="itemCheckBox itemCheckBox-sm">
                                                                                    <input type="checkbox" onclick="javascript: rdv_checkAll(document.forms[0], 'ArrEdit', this.checked)" />
                                                                                    <i class="check-box"></i>
                                                                                </label>
                                                                            </th>
                                                                            <th class="sorting_disabled w10p text-center">
                                                                                <p>Thêm</p>
                                                                                <label class="itemCheckBox itemCheckBox-sm">
                                                                                    <input type="checkbox" onclick="javascript: rdv_checkAll(document.forms[0], 'ArrAdd', this.checked)" />
                                                                                    <i class="check-box"></i>
                                                                                </label>
                                                                            </th>
                                                                            <th class="sorting_disabled w10p text-center">
                                                                                <p>Truy cập</p>
                                                                                <label class="itemCheckBox itemCheckBox-sm">
                                                                                    <input type="checkbox" onclick="javascript: rdv_checkAll(document.forms[0], 'ArrView', this.checked)" />
                                                                                    <i class="check-box"></i>
                                                                                </label>
                                                                            </th>
                                                                            <th class="sorting_disabled">Tên Chức năng</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td>1</td>
                                                                            <td class="text-center">
                                                                                <label class="itemCheckBox itemCheckBox-sm">
                                                                                    <input type="checkbox" name="ArrApprove" value="SysAdministrator" disabled="disabled" />
                                                                                    <i class="check-box"></i>
                                                                                </label>
                                                                            </td>
                                                                            <td class="text-center">
                                                                                <label class="itemCheckBox itemCheckBox-sm">
                                                                                    <input type="checkbox" name="ArrApprove" value="SysAdministrator" disabled="disabled" />
                                                                                    <i class="check-box"></i>
                                                                                </label>
                                                                            </td>
                                                                            <td class="text-center">
                                                                                <label class="itemCheckBox itemCheckBox-sm">
                                                                                    <input type="checkbox" name="ArrApprove" value="SysAdministrator" disabled="disabled" />
                                                                                    <i class="check-box"></i>
                                                                                </label>
                                                                            </td>
                                                                            <td class="text-center">
                                                                                <label class="itemCheckBox itemCheckBox-sm">
                                                                                    <input type="checkbox" name="ArrApprove" value="SysAdministrator" disabled="disabled" />
                                                                                    <i class="check-box"></i>
                                                                                </label>
                                                                            </td>
                                                                            <td class="text-center">
                                                                                <label class="itemCheckBox itemCheckBox-sm">
                                                                                    <input type="checkbox" name="ArrView" value="SysAdministrator" <%if ((GetAccess("SysAdministrator") & 1) == 1)
                                                                                        { %>checked="checked"
                                                                                        <%} %> />
                                                                                    <i class="check-box"></i>
                                                                                </label>
                                                                            </td>
                                                                            <td>Quản trị hệ thống</td>
                                                                        </tr>

                                                                        <%for (var i = 0; i < Reddevil.Lib.Web.Application.CPModules.OrderBy(o => o.Order).ToList().Count; i++)
                                                                            { %>
                                                                        <tr>
                                                                            <td><%=i + 2 %></td>
                                                                            <td class="text-center">
                                                                                <label class="itemCheckBox itemCheckBox-sm">
                                                                                    <input type="checkbox" name="ArrApprove" value="<%= Reddevil.Lib.Web.Application.CPModules[i].Code%>" <%if ((GetAccess(Reddevil.Lib.Web.Application.CPModules[i].Code) & 16) == 16)
                                                                                        { %>checked="checked"
                                                                                        <%} %> <%if ((Reddevil.Lib.Web.Application.CPModules[i].Access & 16) != 16)
                                                                                        { %>disabled="disabled"
                                                                                        <%} %> />
                                                                                    <i class="check-box"></i>
                                                                                </label>
                                                                            </td>
                                                                            <td class="text-center">
                                                                                <label class="itemCheckBox itemCheckBox-sm">
                                                                                    <input type="checkbox" name="ArrDelete" value="<%= Reddevil.Lib.Web.Application.CPModules[i].Code%>" <%if ((GetAccess(Reddevil.Lib.Web.Application.CPModules[i].Code) & 8) == 8)
                                                                                        { %>checked="checked"
                                                                                        <%} %> <%if ((Reddevil.Lib.Web.Application.CPModules[i].Access & 8) != 8)
                                                                                        { %>disabled="disabled"
                                                                                        <%} %> />
                                                                                    <i class="check-box"></i>
                                                                                </label>
                                                                            </td>
                                                                            <td class="text-center">
                                                                                <label class="itemCheckBox itemCheckBox-sm">
                                                                                    <input type="checkbox" name="ArrEdit" value="<%= Reddevil.Lib.Web.Application.CPModules[i].Code%>" <%if ((GetAccess(Reddevil.Lib.Web.Application.CPModules[i].Code) & 4) == 4)
                                                                                        { %>checked="checked"
                                                                                        <%} %> <%if ((Reddevil.Lib.Web.Application.CPModules[i].Access & 4) != 4)
                                                                                        { %>disabled="disabled"
                                                                                        <%} %> />
                                                                                    <i class="check-box"></i>
                                                                                </label>
                                                                            </td>
                                                                            <td class="text-center">
                                                                                <label class="itemCheckBox itemCheckBox-sm">
                                                                                    <input type="checkbox" name="ArrAdd" value="<%= Reddevil.Lib.Web.Application.CPModules[i].Code%>" <%if ((GetAccess(Reddevil.Lib.Web.Application.CPModules[i].Code) & 2) == 2)
                                                                                        { %>checked="checked"
                                                                                        <%} %> <%if ((Reddevil.Lib.Web.Application.CPModules[i].Access & 2) != 2)
                                                                                        { %>disabled="disabled"
                                                                                        <%} %> />
                                                                                    <i class="check-box"></i>
                                                                                </label>
                                                                            </td>
                                                                            <td class="text-center">
                                                                                <label class="itemCheckBox itemCheckBox-sm">
                                                                                    <input type="checkbox" name="ArrView" value="<%= Reddevil.Lib.Web.Application.CPModules[i].Code%>" <%if ((GetAccess(Reddevil.Lib.Web.Application.CPModules[i].Code) & 1) == 1)
                                                                                        { %>checked="checked"
                                                                                        <%} %> <%if ((Reddevil.Lib.Web.Application.CPModules[i].Access & 1) != 1)
                                                                                        { %>disabled="disabled"
                                                                                        <%} %> />
                                                                                    <i class="check-box"></i>
                                                                                </label>
                                                                            </td>
                                                                            <td><%= Reddevil.Lib.Web.Application.CPModules[i].Description%></td>
                                                                        </tr>
                                                                        <%} %>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>

</form>
