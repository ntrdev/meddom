﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.CPViewControl" %>

<%
    var model = ViewBag.Model as SysRoleModel;
    var listItem = ViewBag.Data as List<CPRoleEntity>;
%>

<form id="reddevilForm" name="reddevilForm" method="post">

    <input type="hidden" id="_reddevil_action" name="_reddevil_action" />
    <input type="hidden" id="boxchecked" name="boxchecked" value="0" />



    <div class="pcoded-content">
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8 d-none d-lg-block">
                    <div class="page-header-title">
                        <i class="icon-social-dropbox"></i>
                        <div class="d-inline">
                            <h5>Quản lý phân Quyền truy cập</h5>
                            <span>Danh sách nhóm quyền</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class=" breadcrumb breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="/{CPPath}/">
                                    <i class="icon-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="/{CPPath}/<%=CPViewPage.CurrentModule.Code%>/Index.aspx">Phân quyền truy cập</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>


        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <%= ShowMessage()%>
                                </div>
                                <div class="card">
                                    <div class="card-header header-control">
                                        <h5>Danh sách Nhóm quyền</h5>
                                        <div class="controler-header">
                                            <%=GetDefaultCommand() %>
                                        </div>
                                    </div>
                                    <div class="card-block">
                                        <div class="dt-responsive table-responsive">
                                            <table id="" class="table_basic table table-hover m-b-0 " style="width: 100%">
                                                <thead>
                                                    <tr>
                                                        <th class="sorting text-center w1p">STT</th>
                                                        <th class="sorting_disabled text-center w1p">
                                                            <div class="border-checkbox-section">
                                                                <div class="md-checkbox border-checkbox-group border-checkbox-group-info" data-toggle="tooltip" data-placement="bottom" data-original-title="Click chọn tất cả">
                                                                    <input type="checkbox" id="checks" name="toggle" onclick="checkAll('<%=model.PageSize%>')" class="md-check border-checkbox">
                                                                    <label for="checks" class="border-checkbox-label"></label>
                                                                </div>
                                                            </div>
                                                        </th>
                                                        <th class="sorting"><%= GetSortLink("Tên nhóm quyền", "Name")%></th>

                                                        <th class="text-center">Sắp xếp vị trí</th>
                                                        <th class="text-center">Tùy chọn</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <%for (var i = 0; listItem != null && i < listItem.Count; i++)
                                                        { %>
                                                    <tr>
                                                        <td class="text-center"><%= i + 1%></td>
                                                        <td class="text-center">
                                                            <%= GetCheckbox(listItem[i].ID, i)%>
                                                        </td>
                                                        <td>
                                                            <a style="color: orangered; font-weight: 700" href="javascript:REDDEVILRedirect('Add', <%= listItem[i].ID %>)"><%= listItem[i].Name%></a>
                                                        </td>
                                                        <td class="text-center"><%= GetOrder(listItem[i].ID, listItem[i].Order)%></td>
                                                        <td class="text-center">
                                                            <div class="control-button">
                                                                <button data-toggle="tooltip" data-original-title="Chỉnh sửa" onclick="REDDEVILRedirect('Add', <%= listItem[i].ID %>)" type="button" class="btn waves-effect waves-light btn-primary btn-icon"><i class="icon-pencil"></i></button>
                                                                <button class="btn waves-effect waves-light btn-danger btn-icon" data-toggle="tooltip" data-original-title="ID" type="button"><%=listItem[i].ID %></button>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <%} %>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="row center">
                                            <div class="col-md-12 col-sm-12 justify-content-center d-flex ">
                                                <%= GetPagination(model.PageIndex, model.PageSize, model.TotalRecord)%>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <script type="text/javascript">

        var REDDEVILController = "SysRole";

        var REDDEVILArrVar = [
            "limit", "PageSize"
        ];

        var REDDEVILArrQT = [
            "<%= model.PageIndex + 1 %>", "PageIndex",
            "<%= model.Sort %>", "Sort"
        ];

        var REDDEVILArrDefault = [
            "1", "PageIndex",
            "20", "PageSize"
        ];
    </script>
</form>
