﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.CPViewControl" %>

<%
    var model = ViewBag.Model as ModLibraryModel;
    var item = ViewBag.Data as ModLibraryEntity;
%>
<input type="hidden" id="ModuleCode" value="<%=CPViewPage.CurrentModule.Code %>" />
<form id="reddevilForm" name="reddevilForm" method="post">

    <input type="hidden" id="RecordID" value="<%=model.RecordID %>" />
    <input type="hidden" id="_reddevil_action" name="_reddevil_action" />


    <div class="pcoded-content">
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8 d-none d-lg-block">
                    <div class="page-header-title">
                        <i class="fa fa-file-image-o"></i>
                        <div class="d-inline">
                            <h5>Quản lý Tin tức</h5>
                            <span><%= model.RecordID > 0 ? "Cập nhật thư viện" : "Thêm mới thư viện"%></span>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="page-header-breadcrumb">
                                    <ul class=" breadcrumb breadcrumb-title">
                                        <li class="breadcrumb-item">
                                            <a href="/{CPPath}/">
                                                <i class="icon-home"></i>
                                            </a>

                                        </li>
                                        <li class="breadcrumb-item">
                                            <i class="fa fa-chevron-right"></i>
                                            <a href="/{CPPath}/<%=CPViewPage.CurrentModule.Code%>/Index.aspx">Thư viện số</a>
                                        </li>
                                        <li class="breadcrumb-item">
                                            <i class="fa fa-chevron-right"></i>
                                            <a href="/{CPPath}/<%=CPViewPage.CurrentModule.Code%>/Add.aspx"><%= model.RecordID > 0 ? "Cập nhật "+item.Name : "Thêm mới bài viết thư viện số"%></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="card">
                                    <%= ShowMessage()%>
                                    <div class="button-header controler-header">
                                        <%=GetDefaultAddCommand() %>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-9">
                                <div class="card content-editor">
                                    <div class="card-header header-control">
                                        <h5>Thêm mới bài viết</h5>
                                    </div>
                                    <div class="card-block pdt-0">
                                        <div class="tabs-scroll">
                                            <ul class="nav nav-tabs md-tabs " role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link active" data-toggle="tab"
                                                        href="#thongtinchung" role="tab"><i class="icon-info"></i>Thông tin chung</a>
                                                    <div class="slide"></div>
                                                </li>

                                            </ul>
                                        </div>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="thongtinchung"
                                                role="tabpanel">
                                                <h4 class="sub-title">Thông tin bài viết</h4>
                                                <div class="edit-margin-mb form-manager">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <label class="col-sm-12 col-form-label">Tiêu đề</label>
                                                                    <div class="col-sm-12">
                                                                        <input type="text" class="form-control " id="Name" name="Name" value="<%=item.Name %>" placeholder="nhập vào đây, tối đa 200 ký tự ...">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <label class="col-sm-12 col-form-label">Danh mục</label>
                                                                    <div class="col-sm-12">
                                                                        <select class="form-control chosen" name="CategoryID" id="CategoryID">
                                                                            <%= Utils.ShowDdlMenuByType4("Library", model.LangID, item.CategoryID)%>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input class="input-file" type="button" id="imgInp" onclick="ShowFileForm('File'); return false">
                                                            <label tabindex="0" for="my-file" class="input-file-trigger text-center">
                                                                <i class="icon-cloud-upload"></i>&nbsp;Chọn file tài liệu (pdf, .doc, .docx)</label>
                                                            <input type="text" class="form-control" name="File" id="File" placeholder="Chọn file tài liệu" value="<%=item.File %>" />
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <label class="col-sm-12 col-form-label">
                                                                        Tóm tắt
                                                                    </label>
                                                                    <div class="col-sm-12">
                                                                        <div class="plugin-ckEditor">
                                                                            <textarea class="form-control ckeditor" rows="5" name="Description" placeholder="Mô tả tóm tắt"><%=item.Description %></textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="block-controlRight">
                                    <div class="ItemControl">
                                        <a class="titleCollap" data-toggle="collapse" href="#ctr-trangthai">Trạng thái và hiển thị</a>
                                        <div id="ctr-trangthai" class="panel-collapse collapse show">
                                            <div class="panel-body">
                                                <ul class="form-block">

                                                    <%if (CPViewPage.UserPermissions.Approve)
                                                        { %>
                                                    <li>
                                                        <div>Duyệt/Hủy duyệt</div>
                                                        <div class="md-radio-inline form-radio">
                                                            <div class="radio radio-matrial radio-success radio-inline">
                                                                <label>
                                                                    <input type="radio" id="rdo_decktop" name="Activity" <%= item.Activity ? "checked" : "" %> class="md-radiobtn" value="1">
                                                                    <i class="helper"></i>Duyệt
                                                                </label>
                                                            </div>
                                                            <div class="radio radio-matrial radio-danger radio-inline">
                                                                <label>
                                                                    <input type="radio" id="rdo_mobile" name="Activity" <%= !item.Activity ? "checked" : "" %> value="0" class="md-radiobtn">
                                                                    <i class="helper"></i>Hủy Duyệt
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <%} %>
                                                    <li>
                                                        <div>Ngày viết</div>
                                                        <div class="editorBox">
                                                            <span><%=Utils.FormatDateTime(item.DateCreated) %></span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div>Tác giả</div>
                                                        <div class="editorBox">
                                                            <%=item.CPUser %>
                                                        </div>
                                                    </li>
                                                    <li class="blockform">
                                                        <div>Đường dẫn URL</div>
                                                        <div class="editorBox">
                                                            <input type="text" class="form-block" id="Url" name="Url" value="<%=item.Url %>" placeholder="Nếu không nhập sẽ tự sinh theo Tiêu đề">
                                                        </div>
                                                    </li>
                                                    <li class="">
                                                        <div class="form-group form-md-checkboxes">
                                                            <label>Nơi hiển thị tin </label>
                                                            <div class="md-checkbox-inline border-checkbox-section">
                                                                <%= Utils.ShowCheckBoxByConfigkey("Mod.NewsState", "ArrState", item.State)%>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="ItemControl">
                                        <a class="titleCollap" data-toggle="collapse" href="#ctr-advance">Ảnh</a>
                                        <div id="ctr-advance" class="panel-collapse collapse" style="height: 0px;" aria-expanded="false">
                                            <div class="panel-body">
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <input class="input-file" type="button" id="Imagebtn" onclick="ShowFileForm('Image'); return false">
                                                        <label tabindex="0" for="my-file" class="input-file-trigger text-center">
                                                            <i class="icon-cloud-upload"></i>&nbsp;Chọn ảnh đại diện</label>
                                                        <img id="show_img_upload" src="<%=Utils.DefaultImage(item.Image) %>" />
                                                        <input type="hidden" class="form-control" name="Image" id="Image" value="<%=item.Image %>" />
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>


    <link rel="stylesheet" href="/{CPPath}/interface/utils/chosen/chosen.css" type="text/css" />
    <script type="text/javascript" src="/{CPPath}/interface/utils/chosen/chosen.js"></script>
    <script type="text/javascript">
        //setTimeout(function () { reddevil_exec_cmd('[autosave][<%=model.RecordID%>]') }, 5000);
    </script>
</form>
