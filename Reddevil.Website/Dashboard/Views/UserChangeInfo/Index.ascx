﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.CPViewControl" %>

<% 
    var item = CPViewPage.CurrentUser as CPUserEntity;
%>

<form id="reddevilForm" name="reddevilForm" method="post">

    <input type="hidden" id="_reddevil_action" name="_reddevil_action" />
    <div class="pcoded-content">
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8 d-none d-lg-block">
                    <div class="page-header-title">
                        <i class="icon-people"></i>
                        <div class="d-inline">
                            <h5>Quản lý tài khoản</h5>
                            <span>Tài khoản</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class=" breadcrumb breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="/{CPPath}/">
                                    <i class="icon-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="/{CPPath}/<%=CPViewPage.CurrentModule.Code%>/Index.aspx">Tài khoản</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <%= ShowMessage()%>
                                </div>
                            </div>
                            <div class="col-sm-9">
                                <div class="card content-editor">
                                    <div class="card-header header-control">
                                        <h5>Tài khoản</h5>
                                        <div class="controler-header">
                                            <%=GetDefaultAddCommand() %>
                                        </div>
                                    </div>
                                    <div class="card-block pdt-0">
                                        <div class="tabs-scroll">
                                            <ul class="nav nav-tabs md-tabs " role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link active" data-toggle="tab"
                                                        href="#thongtinchung" role="tab"><i class="icon-info"></i>Thông tin người dùng</a>
                                                    <div class="slide"></div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="thongtinchung"
                                                role="tabpanel">
                                                <h4 class="sub-title">Thông tin người dùng</h4>
                                                <div class="edit-margin-mb form-manager">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <label class="col-sm-12 col-form-label">Họ và tên</label>
                                                                    <div class="col-sm-12">
                                                                        <input type="text" class="form-control" id="Name" name="Name" value="<%=item.Name %>" placeholder="nhập vào đây, tối đa 200 ký tự ...">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <label class="col-sm-12 col-form-label">Số điện thoại</label>
                                                                    <div class="col-sm-12">
                                                                        <input type="text" class="form-control" id="Phone" name="Phone" value="<%=item.Phone %>" placeholder="nhập vào đây ...">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <label class="col-sm-12 col-form-label">Email</label>
                                                                    <div class="col-sm-12">
                                                                        <input type="text" class="form-control" id="Email" name="Email" value="<%=item.Email %>" placeholder="Nhập email">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <label class="col-sm-12 col-form-label">Địa chỉ:</label>
                                                                    <div class="col-sm-12">
                                                                        <input type="text" class="form-control" id="Address" name="Address" value="<%=item.Address %>" placeholder="Địa chỉ">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group form-md-line-input ">
                                                                <div class="row">
                                                                    <label class="col-sm-12 col-form-label">Tên đăng nhập:</label>
                                                                    <div class="col-sm-12">
                                                                        <input type="text" class="form-control" readonly id="" name="" value="<%=item.LoginName%>" placeholder="Tên đăng nhập phải không dấu, và không có ký tự đặc biệt (&,^,*,#)">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="block-controlRight">
                                   
                                    <div class="ItemControl">
                                        <a class="titleCollap collapsed" data-toggle="collapse" href="#ctr-advance">Ảnh đại diện</a>
                                        <div id="ctr-advance" class="panel-collapse collapse show" style="" aria-expanded="false">
                                            <div class="panel-body">
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <input class="input-file" type="button" id="imgInp" onclick="ShowFileForm('File'); return false">
                                                        <label tabindex="0" for="my-file" class="input-file-trigger text-center">
                                                            <i class="icon-cloud-upload"></i>&nbsp;Chọn ảnh đại diện</label>
                                                        <img id="show_img_upload" src="<%=Utils.OptimalImage(item.File) %>" />
                                                        <input type="hidden" class="form-control" name="File" id="File" value="<%=item.File %>" />
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
