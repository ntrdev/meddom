﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.CPViewControl" %>


<form method="post" class="login-form" id="loginForm" name="loginForm">
    <div class="auth-box card">
        <div class="card-block">
            <div class="row m-b-20">
                <div class="col-md-12">
                    <div class="text-center">
                        <img src="/{CPPath}/interface/images/logo.png" alt="" />
                    </div>
                </div>
            </div>
            <p class="text-muted text-center p-b-5">{RS:Login_LoginTitle}</p>
            <%= ShowMessage()%>
            <div class="form-group">
                <i class="fa fa-user"></i>
                <input type="text" name="UserName" class="form-control" required="" autocomplete="off" placeholder="{RS:Login_UserName}">
            </div>
            <div class="form-group">
                <i class="fa fa-lock"></i>
                <input type="password" autocomplete="off" name="Password" class="form-control" required="required" placeholder=" {RS:Login_Password}">
            </div>

            <div class="row m-t-30">
                <div class="col-md-12">
                    <input type="hidden" id="_reddevil_action" name="_reddevil_action" value="Login" />
                    <button type="button" class="btn btn-info btn-md btn-block waves-effect text-center m-b-20" onclick="loginForm.submit();">{RS:Login_LoginSubmit}</button>
                </div>
            </div>
         <%--   <div class="copyright">
                Copyright © <%=DateTime.Now.Year %> <a href="https://imexsoft.net" style="color: aliceblue" target="_blank">IMEXsoft®</a> Inc. All rights reserved.
            </div>--%>
        </div>
    </div>

</form>

