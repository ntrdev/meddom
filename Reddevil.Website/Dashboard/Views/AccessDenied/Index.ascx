﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.CPViewControl" %>

<div class="pcoded-content">
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8 d-none d-lg-block">
                <div class="page-header-title">
                    <i class="icon-bell"></i>
                    <div class="d-inline">
                        <h1>Oops!</h1>
                        <h2>Access denied!</h2>
                        <div class="error-details">
                            Xin lỗi, bạn không có quyền truy cập chức năng này. Liên hệ Administrator để biết thêm chi tiết.
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
