﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.CPViewControl" %>

<%
    var listLang = SysLangService.Instance.CreateQuery().ToList();
    var model = ViewBag.Model as SysSiteModel;
    var item = ViewBag.Data as SysSiteEntity;
%>

<form id="reddevilForm" name="reddevilForm" method="post">

    <input type="hidden" id="_reddevil_action" name="_reddevil_action" />

    <div class="pcoded-content">
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8 d-none d-lg-block">
                    <div class="page-header-title">
                        <i class="icon-social-dropbox"></i>
                        <div class="d-inline">
                            <h5>Quản lý Trang mặc định</h5>
                            <span><%= model.RecordID > 0 ? "Cập nhật " : "Thêm mới "%></span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class=" breadcrumb breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="/{CPPath}/">
                                    <i class="icon-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="/{CPPath}/<%=CPViewPage.CurrentModule.Code%>/Index.aspx">Site</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <%= ShowMessage()%>
                                </div>
                            </div>
                            <div class="col-sm-9">
                                <div class="card content-editor">
                                    <div class="card-header header-control">
                                        <h5>Thêm mới bài viết</h5>
                                        <div class="controler-header">
                                            <%=GetDefaultAddCommand() %>
                                        </div>
                                    </div>
                                    <div class="card-block pdt-0">
                                    
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="thongtinchung"
                                                role="tabpanel">
                                                <h4 class="sub-title">Thông tin mặc định</h4>
                                                <div class="edit-margin-mb form-manager">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <label class="col-sm-12 col-form-label">Tiêu đề</label>
                                                                    <div class="col-sm-12">
                                                                        <input type="text" class="form-control" id="Name" name="Name" value="<%=item.Name %>" placeholder="nhập vào đây ...">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <label class="col-sm-12 col-form-label">Trang mặc định</label>
                                                                    <div class="col-sm-12" id="list_page">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <label class="col-sm-12 col-form-label">Chọn ngôn ngữ</label>
                                                                    <div class="col-sm-12">
                                                                        <select class="form-control" onchange="lang_change(this.value)" name="LangID" id="LangID">
                                                                            <%for (var i = 0; listLang != null && i < listLang.Count; i++)
                                                                                { %>
                                                                            <option <%if (item.LangID == listLang[i].ID)
                                                                                {%>selected<%} %>
                                                                                value="<%= listLang[i].ID%>">&nbsp; <%= listLang[i].Name%></option>
                                                                            <%} %>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="block-controlRight">
                                    <div class="ItemControl">
                                        <a class="titleCollap" data-toggle="collapse"
                                            href="#ctr-trangthai">Trạng thái và hiển thị</a>
                                        <div id="ctr-trangthai" class="panel-collapse collapse show">
                                            <div class="panel-body">
                                                <ul class="form-block">

                                                    <li class="blockform">
                                                        <div>Mã thiết kế:</div>
                                                        <div class="editorBox">
                                                            <textarea class="form-control" rows="3" name="Summary" placeholder="Mã thiết kế, Có thể hệ thống sẽ tự sinh"><%=item.Custom %></textarea>
                                                        </div>
                                                    </li>

                                                </ul>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="ItemControl">
                                        <a class="titleCollap" data-toggle="collapse" href="#ctr-advance">Ảnh Quốc kỳ</a>
                                        <div id="ctr-advance" class="panel-collapse collapse" style="height: 0px;" aria-expanded="false">
                                            <div class="panel-body">
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <input class="input-file" type="button" id="imgInp" onclick="ShowFileForm('File'); return false">
                                                        <label tabindex="0" for="my-file" class="input-file-trigger text-center">
                                                            <i class="icon-cloud-upload"></i>&nbsp;Chọn ảnh đại diện</label>
                                                        <img id="show_img_upload" src="<%=Utils.OptimalImage(item.File) %>" />
                                                        <input type="hidden" class="form-control" name="File" id="File" value="<%=item.File %>" />
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <link rel="stylesheet" href="/{CPPath}/interface/utils/chosen/chosen.css" type="text/css" media="all" />
    <script type="text/javascript" src="/{CPPath}/interface/utils/chosen/chosen.js"></script>
    <script type="text/javascript">
        var ddl_lang = document.getElementById("LangID");

        function lang_change(langID) {
            var ranNum = Math.floor(Math.random() * 999999);
            var dataString = "LangID=" + langID + "&PageID=<%=item.PageID %>&rnd=" + ranNum;
            $.ajax({
                url: "/{CPPath}/Ajax/SiteGetPage.aspx",
                type: "get",
                data: dataString,
                dataType: 'json',
                success: function (data) {
                    var content = data.Params;

                    content = "<select class=\"form-control chosen\" name=\"PageID\" id=\"PageID\"><option value=\"0\">Chọn trang</option>" + content + "</select><label for=\"form_control_1\">Trang mặc định:</label><span class=\"help-block\"> Cài đặt trang mặc định cho toàn bộ website</span>";
                    $("#list_page").html(content);
                    $('.chosen').chosen({
                        search_contains: true,
                        no_results_text: 'Không tìm thấy kết quả phù hợp'
                    });
                },
                error: function (status) { }
            });
        }

        if (<%=item.LangID%> > 0) lang_change(<%=item.LangID %>);
        else if ($("#LangID").val() > 0) lang_change($("#LangID").val());
    </script>
</form>
