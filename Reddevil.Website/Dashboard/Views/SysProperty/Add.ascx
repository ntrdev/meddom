﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.CPViewControl" %>

<%
    var model = ViewBag.Model as SysPropertyModel;
    var item = ViewBag.Data as WebPropertyEntity;

    var listParent = Reddevil.Lib.Global.ListItem.List.GetList(WebPropertyService.Instance, model.LangID);

    if (model.RecordID > 0)
    {
        //loai bo thuoc tinh con cua thuoc tinh hien tai
        listParent = Reddevil.Lib.Global.ListItem.List.GetListForEdit(listParent, model.RecordID);
    }
%>

<form id="reddevilForm" name="reddevilForm" method="post">

    <input type="hidden" id="_reddevil_action" name="_reddevil_action" />





    <div class="page-content">
        <div class="breadcrumbs">
            <h1><%= model.RecordID > 0 ? "Cập nhật thuộc tính" : "Thêm mới thuộc tính"%></h1>
            <ol class="breadcrumb">
                <li>
                    <a href="/{CPPath}/"><i class="fa fa-home"></i>Home</a>
                </li>
                <li class="active"><a href="/{CPPath}/<%=CPViewPage.CurrentModule.Code%>/Index.aspx">Thuộc tính</a></li>
            </ol>
        </div>
        <div class="row">
            <div class="col-md-12">
                <%= ShowMessage()%>
                <div class="control_heading">
                    <div class="btn-group btn-group-circle">
                        <%=GetDefaultAddCommand() %>
                    </div>
                </div>
            </div>
        </div>
        <div class="row pagepost">

            <div class="col-sm-9">

                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-green-sharp">
                            <i class="icon-speech font-green-sharp"></i>
                            <span class="caption-subject bold uppercase">Thông tin chung</span>
                            <span class="caption-helper">thông tin sản phẩm</span>
                        </div>
                        <div class="actions">
                            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"></a>
                        </div>
                    </div>
                    <div class="portlet-body">

                        <div class="form-body">
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input ">
                                        <input type="text" class="form-control" id="Name" name="Name" value="<%=item.Name %>" placeholder="nhập vào đây ...">
                                        <label for="form_control_1">Tên thuộc tính:</label>
                                        <span class="help-block">Ký tự tối đa 200</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input ">
                                        <input type="text" class="form-control" id="Url" value="<%=item.Code %>" placeholder="Nếu không nhập sẽ tự sinh theo Tiêu đề">
                                        <label for="form_control_1">Mã thuộc tính(url):</label>
                                        <span class="help-block">ví dụ: san-pham-tai-nghe-cao-cap</span>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <label>Kiểu hiển thị</label>
                                    <div class="md-radio-inline">
                                        <div class="md-radio">
                                            <input type="radio" id="Multiple1" name="Multiple" value="1" class="md-radiobtn" <%=item.Multiple?"checked":"" %>>
                                            <label for="Multiple1">
                                                <span></span>
                                                <span class="check"></span>
                                                <span class="box"></span>Chọn nhiều 
                                            </label>
                                        </div>
                                        <div class="md-radio has-error">
                                            <input type="radio" id="Multiple2" name="Multiple" value="0" class="md-radiobtn" <%=!item.Multiple?"checked":"" %>>
                                            <label for="Multiple2">
                                                <span></span>
                                                <span class="check"></span>
                                                <span class="box"></span>Chọn một
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-green-sharp">
                            <i class="icon-speech font-green-sharp"></i>
                            <a class="titleCollap collapsed" data-toggle="collapse" href="#ctr-advance" aria-expanded="false"><span class="caption-subject bold uppercase">Hình ảnh </span></a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="ItemControl">

                                <div id="ctr-advance" class="panel-collapse collapse" style="height: 0px;" aria-expanded="false">
                                    <div class="panel-body">
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input class="input-file" type="button" id="imgInp" onclick="ShowFileForm('File'); return false">
                                                <label tabindex="0" for="my-file" class="input-file-trigger text-center">
                                                    <i class="icon-cloud-upload"></i>&nbsp;Chọn ảnh</label>
                                                <img id="show_img_upload" src="<%=Utils.DefaultImage(item.File) %>" />
                                                <input type="hidden" class="form-control" name="File" id="File" value="<%=item.File %>" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-green-sharp">
                            <i class="icon-speech font-green-sharp"></i>
                            <span class="caption-subject bold uppercase">Thuộc tính </span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">

                            <div class="col-md-12">
                                <div class="form-group form-md-line-input ">
                                    <select class="form-control chosen" name="ParentID">
                                        <option value="0">Thuộc tính cha</option>
                                        <%for (int i = 0; listParent != null && i < listParent.Count; i++)
                                            { %>
                                        <option <%if (item.ParentID.ToString() == listParent[i].Value)
                                            {%>selected<%} %>
                                            value='<%= listParent[i].Value%>'>&nbsp; <%= listParent[i].Name%></option>
                                        <%} %>
                                    </select>
                                    <label for="multiple" class="control-label">Thuộc tính cha:</label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label>Ẩn/Hiện</label>
                                <div class="md-radio-inline">
                                    <div class="md-radio">
                                        <input type="radio" id="Hide1" name="Hide" value="1" class="md-radiobtn" <%=item.Hide?"checked":"" %>>
                                        <label for="Hide1">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span>Hiện
                                        </label>
                                    </div>
                                    <div class="md-radio has-error">
                                        <input type="radio" id="Hide2" name="Hide" value="0" class="md-radiobtn" <%=!item.Hide?"checked":"" %>>
                                        <label for="Hide2">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span>Ẩn
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <label>Trạng thái</label>
                                <div class="md-radio-inline">
                                    <div class="md-radio">
                                        <input type="radio" id="activity1" name="Activity" value="1" class="md-radiobtn" <%=item.Activity?"checked":"" %>>
                                        <label for="activity1">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span>Bật
                                        </label>
                                    </div>
                                    <div class="md-radio has-error">
                                        <input type="radio" id="activity2" name="Activity" value="0" class="md-radiobtn" <%=!item.Activity?"checked":"" %>>
                                        <label for="activity2">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span>Tắt
                                        </label>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</form>

<link rel="stylesheet" href="/{CPPath}/interface/utils/chosen/chosen.css" type="text/css" media="all" />
<script type="text/javascript" src="/{CPPath}/interface/utils/chosen/chosen.js"></script>
<script type="text/javascript">

    $('.chosen').chosen({
        search_contains: true,
        no_results_text: 'Không tìm thấy kết quả phù hợp'
    });
</script>
