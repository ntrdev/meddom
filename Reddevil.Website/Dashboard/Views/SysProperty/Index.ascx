﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.CPViewControl" %>

<script runat="server">
    private List<EntityBase> AutoGetMap(SysPropertyModel model)
    {
        List<EntityBase> list = new List<EntityBase>();

        int _id = model.ParentID;
        while (_id > 0)
        {
            var _item = WebPropertyService.Instance.GetByID(_id);

            if (_item == null)
                break;

            _id = _item.ParentID;

            list.Insert(0, _item);
        }

        return list;
    }
</script>

<%
    var model = ViewBag.Model as SysPropertyModel;
    var listItem = ViewBag.Data as List<WebPropertyEntity>;
%>

<form id="reddevilForm" name="reddevilForm" method="post">

    <input type="hidden" id="_reddevil_action" name="_reddevil_action" />
    <input type="hidden" id="boxchecked" name="boxchecked" value="0" />



    <div class="page-content">

        <div class="breadcrumbs">
            <h1>Quản lý thuộc tính lọc</h1>
            <ol class="breadcrumb">
                <%= ShowMap(AutoGetMap(model))%>
            </ol>
        </div>
        <div class="row">
            <div class="col-md-12">
                <%= ShowMessage()%>
                <div class="control_heading">
                    <div class="btn-group btn-group-circle">
                        <%=GetDefaultListCommand("upload|Thêm nhiều")%>
                    </div>
                    <div class="row">
                        <div class="col-md-8 col-sm-12"></div>
                        <div class="col-md-4 col-sm-12">
                            <div class="col-12 col-sm-12 col-md-8 col-lg-8 text-right pull-right">
                                <div class="table-group-actions d-inline-block" style="margin-top: 10px">
                                    <%= ShowDDLLang(model.LangID)%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="dt-responsive table-responsive">
                    <table id="" class="table_basic table table-hover m-b-0 " style="width: 100%">
                        <thead>
                            <tr>
                                <th class="text-center">STT</th>
                                <th>
                                    <div class="md-checkbox">
                                        <input type="checkbox" id="checks" name="toggle" onclick="checkAll('<%=model.PageSize%>')" class="md-check">
                                        <label for="checks">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span>Chọn
                                        </label>
                                    </div>
                                </th>
                                <th class="text-center"><%= GetSortLink("Tên thuộc tính", "Name")%></th>
                                <th class="sorting text-center w10p hidden-sm hidden-col"><%= GetSortLink("Ảnh", "File")%></th>
                                <%if (model.ParentID == 0) { %><th class="sorting text-center w10p"><%= GetSortLink("Chọn nhiều", "Mutiple")%></th><%} %>
                                <th class="sorting text-center w1p"><%= GetSortLink("Ẩn/Hiện", "Hide")%></th>
                                <th class="text-center">Trạng thái</th>
                                <th style="width: 110px">Sắp xếp vị trí</th>
                                <th class="text-center">Tùy chọn</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%for (var i = 0; listItem != null && i < listItem.Count; i++)
                                {

                            %>
                            <tr>

                                <td class="text-center"><%=i+1 %></td>
                                <td><%= GetCheckbox(listItem[i].ID, i)%></td>
                                <td class="text-center"><a href="javascript:REDDEVILRedirect('Index', <%= listItem[i].ID %>, 'ParentID')">&nbsp;<%= listItem[i].Name%></a></td>
                                <td class="text-center hidden-sm hidden-col"><%= Utils.GetMedia(listItem[i].File, 40, 40)%></td>
                                <%if (model.ParentID == 0){ %><td class="text-center"><%= GetMultiple(listItem[i].Multiple)%></td><%} %>
                                <td class="text-center"><%= GetHide(listItem[i].ID, listItem[i].Hide)%></td>
                                <td class="text-center"><%= GetPublish(listItem[i].ID, listItem[i].Activity)%></td>
                                <td class="text-center"><%= GetOrder(listItem[i].ID, listItem[i].Order)%></td>
                                <td class="text-center">
                                    <div class="control-button">
                                        <button data-toggle="tooltip" data-original-title="Chỉnh sửa" onclick="REDDEVILRedirect('Add', <%= listItem[i].ID %>)" type="button" class="btn waves-effect waves-light btn-primary btn-icon"><i class="icon-pencil"></i></button>
                                        <button class="btn waves-effect waves-light btn-danger btn-icon" data-toggle="tooltip" data-original-title="ID" type="button"><%=listItem[i].ID %></button>
                                    </div>
                                </td>
                            </tr>
                            <%} %>
                        </tbody>
                    </table>
                </div>
                <div class="row center">
                    <div class="col-md-12 col-sm-12 justify-content-center d-flex ">
                        <%= GetPagination(model.PageIndex, model.PageSize, model.TotalRecord)%>
                    </div>
                </div>
            </div>
        </div>
    </div>

    

    <script type="text/javascript">

        var REDDEVILController = "SysProperty";

        var REDDEVILArrVar = [
            "filter_lang", "LangID",
            "limit", "PageSize"
        ];

        var REDDEVILArrQT = [
            "<%= model.PageIndex + 1 %>", "PageIndex",
            "<%= model.ParentID %>", "ParentID",
            "<%= model.Sort %>", "Sort"
        ];

        var REDDEVILArrDefault = [
            "1", "PageIndex",
            "1", "LangID",
            "20", "PageSize"
        ];
    </script>
</form>
