﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.CPViewControl" %>

<%
    var model = ViewBag.Model as ModNewsModel;
    var listItem = ViewBag.Data as List<ModNewsEntity>;
%>

<div class="pcoded-content">
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8 d-none d-lg-block">
                <div class="page-header-title">
                    <i class="fa fa-newspaper-o"></i>
                    <div class="d-inline">
                        <h5>Chọn tin liên quan</h5>
                        <span>Danh sách tin bài</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="row">

                        <div class="col-sm-12">
                            <div class="card">
                                <%= ShowMessage()%>
                            </div>
                            <div class="card">
                                <div class="card-header header-control">
                                    <div class="tools col-md-12">
                                        <div class="dataTables_search col-md-4 col-xs-12">
                                            <input type="text" class="form-control input-inline input-sm" id="filter_search" style="float: left; width: 100%; padding-top: 1px; position: relative; background: #fff; height: 32px; text-indent: 10px; font-size: 14px; border-radius: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px;"
                                                value="<%= model.SearchText %>" placeholder="Nhập từ khóa cần tìm" onchange="REDDEVILRedirect();return false;" />
                                            <button type="submit" class="btntop" onclick="REDDEVILRedirect();return false;" style="float: right; width: 40px; height: 32px; border: 0; cursor: pointer; background: none; position: absolute; right: 15px;">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </div>
                                        <div class="table-group-actions d-inline-block col-md-4 col-xs-12">
                                            <select id="filter_menu" class="form-control input-inline input-sm " onchange="REDDEVILRedirect()" size="1">
                                                <option value="0">Lọc theo chuyên mục</option>
                                                <%= Utils.ShowDdlMenuByType("News", model.LangID, model.MenuID)%>
                                            </select>

                                        </div>
                                        <div class="table-group-actions d-inline-block col-md-4 col-xs-12">
                                            <select id="filter_status" class="form-control input-inline input-sm " onchange="REDDEVILRedirect()" size="1">
                                                <option value="">Lọc theo trạng thái</option>
                                                <option value="0" <%=model.Status==0?"selected=\"selected\"":"" %>>Đã duyệt</option>
                                                <option value="1" <%=model.Status==1?"selected=\"selected\"":"" %>>Đang soạn</option>
                                                <option value="2" <%=model.Status==2?"selected=\"selected\"":"" %>>Chờ duyệt</option>
                                                <option value="3" <%=model.Status==3?"selected=\"selected\"":"" %>>Trả về</option>
                                            </select>

                                        </div>
                                        <%--                                            <div class="table-group-actions d-inline-block col-md-2 col-xs-12">
                                                <%= ShowDDLLang(model.LangID)%>
                                            </div>--%>
                                    </div>
                                </div>
                                <div class="card-block">
                                    <div class="dt-responsive table-responsive">
                                        <table id="" class="table_basic table table-hover m-b-0 "
                                            style="width: 100%">
                                            <thead>
                                                <tr>
                                                    <th class="sorting text-center w1p">#</th>
                                                    <th class="sorting text-center w1p">Chọn</th>
                                                    <th class="sorting"><%= GetSortLink("Tiêu đề", "Name")%></th>
                                                    <th class="sorting text-center w10p hidden-sm hidden-col"><%= GetSortLink("Ảnh", "File")%></th>
                                                    <th class="sorting text-center w10p hidden-sm hidden-col"><%= GetSortLink("Chuyên mục", "MenuID")%></th>
                                                    <th class="sorting text-center w10p hidden-sm hidden-col"><%= GetSortLink("Trạng thái", "Activity")%></th>
                                                    <th class="sorting text-center w1p hidden-sm hidden-col"><%= GetSortLink("Người đăng", "AdminEdit")%></th>
                                                    <th class="sorting text-center w1p hidden-sm hidden-col"><%= GetSortLink("Người duyệt", "AdminActive")%></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <%for (var i = 0; listItem != null && i < listItem.Count; i++)
                                                    { %>
                                                <tr>
                                                    <td class="text-center"><%= i + 1%></td>
                                                    <td class="text-center"><a href="javascript:CloseNews(<%= listItem[i].ID %>,'','1')">Chọn</a></td>
                                                    <td>
                                                        <a href="javascript:CloseNews(<%= listItem[i].ID %>,'','1')"><%= listItem[i].Name%></a>
                                                        <p class="smallsub hidden-sm hidden-col">(<span>Xem trên web</span>: <%= listItem[i].Url%>)</p>
                                                    </td>
                                                    <td class="text-center hidden-sm hidden-col">
                                                        <%= Utils.GetMedia(listItem[i].File, 40, 40)%>
                                                    </td>
                                                    <td class="text-center hidden-sm hidden-col"><%= GetName(listItem[i].GetMenu()) %></td>
                                                    <td class="text-center"><%=listItem[i].Condition==0?"Đã duyệt":(listItem[i].Condition==1?"Đang soạn":listItem[i].Condition==2?"Chờ duyệt":"Trả về") %></td>
                                                    <td class="text-center"><%= GetHideOrShow2(listItem[i].ID, listItem[i].Activity)%></td>
                                                    <td class="text-center hidden-sm hidden-col"><%= listItem[i].AdminEdit %></td>
                                                </tr>
                                                <%} %>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="row center">
                                        <div class="col-md-12 col-sm-12 justify-content-center d-flex ">
                                            <%= GetPagination(model.PageIndex, model.PageSize, model.TotalRecord)%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<link rel="stylesheet" href="/{CPPath}/interface/utils/chosen/chosen.css" type="text/css" />
<script type="text/javascript" src="/{CPPath}/interface/utils/chosen/chosen.js"></script>

<script type="text/javascript">



    var REDDEVILController = "FormNews";

    var REDDEVILArrVar = [
        "filter_menu", "MenuID",
        "filter_state", "State",
        "filter_lang", "LangID",
        "limit", "PageSize"
    ];

    var REDDEVILArrVar_QS = [
        "filter_search", "SearchText"
    ];

    var REDDEVILArrQT = [
        "<%= model.PageIndex + 1 %>", "PageIndex",
        "<%= model.Sort %>", "Sort"
    ];

    var REDDEVILArrDefault = [
        "1", "PageIndex",
        "1", "LangID",
        "20", "PageSize"
    ];
</script>
