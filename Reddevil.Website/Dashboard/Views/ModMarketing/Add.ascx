﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.CPViewControl" %>

<%
    var model = ViewBag.Model as ModMarketingModel;
    var item = ViewBag.Data as ModMarketingEntity;
    var listProduct = item.GetProduct() ?? new List<ModProductEntity>();
%>

<form id="reddevilForm" name="reddevilForm" method="post">
    <input type="hidden" id="_reddevil_action" name="_reddevil_action" />
    <input type="hidden" id="RecordID" name="RecordID" value="<%=model.RecordID %>" />

    <div class="page-content-wrapper">
        <h3 class="page-title">Tin marketing <small><%= model.RecordID > 0 ? "Chỉnh sửa": "Thêm mới"%></small></h3>
        <div class="page-bar justify-content-between">
            <ul class="breadcrumb">
                <li class="breadcrumb-item">
                    <i class="fa fa-home"></i>
                    <a href="/{CPPath}/">Home</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="/{CPPath}/<%=CPViewPage.CurrentModule.Code%>/Index.aspx">Chương trình Affiliate</a>
                </li>
            </ul>
            <div class="page-toolbar">
                <div class="btn-group">
                    <a href="/" class="btn green" target="_blank"><i class="icon-screen-desktop"></i>Xem Website</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <%= ShowMessage()%>

                <div class="form-horizontal form-row-seperated">
                    <div class="portlet">
                        <div class="portlet-title">
                            <div class="caption"></div>
                            <div class="actions btn-set">
                                <%= GetDefaultAddCommand()%>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="tabbable">
                                <ul class="nav nav-tabs">
                                    <li class="nav-link active">
                                        <a class="#" href="#general" data-toggle="tab">Thông tin chung</a>
                                    </li>
                                    <li class="nav-link">
                                        <a class="#" href="#product" data-toggle="tab">Thành viên đã đăng ký</a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="general">
                                        <div class="row">
                                            <div class="col-12 col-sm-12 col-md-12 col-lg-8">
                                                <div class="portlet box blue-steel">
                                                    <div class="portlet-title">
                                                        <div class="caption">Thông tin chung</div>
                                                    </div>
                                                    <div class="portlet-body">
                                                        <div class="form-body">
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label text-right">Tiêu đề:</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control title" name="Name" id="Name" value="<%=item.Name %>" />
                                                                    <span class="help-block text-primary">Ký tự đối ta: 200</span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label text-right">URL trình duyệt:</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" name="Code" value="<%=item.Code %>" placeholder="Nếu không nhập sẽ tự sinh theo Tiêu đề" />
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label text-right">Chiết khấu cho CTV(%) các SP trong chiến dịch (%):</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" onfocus="(this.type='number')" name="SellOfferForSeller" value="<%=item.SellOfferForSeller %>" placeholder="Chiết khấu cho CTV(%) các SP trong chiến dịch" />
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label text-right">Chiết khấu cho KH(%)các SP trong chiến dịch (%):</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" onfocus="(this.type='number')" name="SellOfferForUser" value="<%=item.SellOfferForUser %>" placeholder="Chiết khấu cho KH(%)các SP trong chiến dịch" />
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label text-right">Ngày bắt đầu:</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" onfocus="(this.type='date')" class="form-control" name="StartDate" value="<%=item.StartDate> DateTime.MinValue? string.Format("{0:dd-MM-yyyy}", item.StartDate) :string.Empty%>" placeholder="Chọn ngày bắt đầu cho chiến dịch marketing" />

                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label text-right">Ngày kết thúc:</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" onfocus="(this.type='date')" class="form-control" name="EndDate" value="<%=item.EndDate> DateTime.MinValue? string.Format("{0:dd-MM-yyyy}", item.EndDate) :string.Empty %>" placeholder="Chọn ngày kêt thúc cho chiến dịch marketing" />

                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label text-right">Video hướng dẫn (youtube.com):</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" name="Video" value="<%=item.Video %>" placeholder="Link youtube hướng dẫn đăng ký chương trình" />

                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label text-right">Mô tả chiến dịch :</label>
                                                                <div class="col-md-9">
                                                                    <textarea class="form-control description" rows="5" name="Summary" placeholder="Nhập nội dung tóm tắt. Tối đa 400 ký tự"><%=item.Summary%></textarea>
                                                                    <span class="help-block text-primary">Ký tự đối ta: 400</span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-3 col-form-label text-right">Chuyên mục:</label>
                                                                <div class="col-md-9">
                                                                    <select class="form-control" name="MenuID" id="MenuID">
                                                                        <option value="0">Root</option>
                                                                        <%= Utils.ShowDdlMenuByType("Marketing", model.LangID, item.MenuID)%>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-md-12 col-form-label text-right">Nội dung</label>
                                                                <div class="col-md-12">
                                                                    <textarea class="form-control ckeditor" name="Content" id="Content" rows="" cols=""><%=item.Content %></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-12 col-md-12 col-lg-4">
                                                <div class="portlet box blue-steel">
                                                    <div class="portlet-title">
                                                        <div class="caption">Thuộc tính</div>
                                                    </div>
                                                    <div class="portlet-body">
                                                        <div class="form-body">
                                                            <div class="form-group">
                                                                <%if (!string.IsNullOrEmpty(item.File))
                                                                    { %>
                                                                <p class="preview "><%= Utils.GetMedia(item.File, 80, 80)%></p>
                                                                <%}
                                                                    else
                                                                    { %>
                                                                <p class="preview">
                                                                    <img src="" width="80" height="80" />
                                                                </p>
                                                                <%} %>

                                                                <label class="portlet-title-sub">Hình banner chương trình:</label>
                                                                <div class="form-inline">
                                                                    <input type="text" class="form-control" name="File" id="File" value="<%=item.File %>" />
                                                                    <button type="button" class="btn btn-primary" onclick="ShowFileForm('File'); return false">Chọn ảnh</button>
                                                                </div>
                                                            </div>


                                                            <%if (CPViewPage.UserPermissions.Approve)
                                                                {%>
                                                            <div class="form-group">
                                                                <label class="portlet-title-sub">Duyệt</label>
                                                                <div class="radio-list">
                                                                    <label class="radioPure radio-inline">
                                                                        <input type="radio" name="Activity" <%= item.Activity ? "checked": "" %> value="1" />
                                                                        <span class="outer"><span class="inner"></span></span><i>Có</i>
                                                                    </label>
                                                                    <label class="radioPure radio-inline">
                                                                        <input type="radio" name="Activity" <%= !item.Activity ? "checked": "" %> value="0" />
                                                                        <span class="outer"><span class="inner"></span></span><i>Không</i>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <%} %>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="portlet box blue-steel">
                                                    <div class="portlet-title">
                                                        <div class="caption">Sản phẩm</div>
                                                    </div>
                                                    <div class="portlet-body">
                                                        <div class="form-body">

                                                            <div class="form-group">
                                                                <label class="portlet-title-sub">Sản phẩm:</label>
                                                                <ul class="cmd-custom" id="list-gift">


                                                                    <%foreach (var prod in listProduct)

                                                                        {%>
                                                                    <li>
                                                                        <img src="<%=Utils.DefaultImage(prod.FileAvatar) %>" />
                                                                        <b><%=prod.Name %></b>
                                                                        <a href="javascript:void(0)" onclick="deleteProduct('<%=prod.ID %>')" data-toggle="tooltip" data-placement="bottom" data-original-title="Xóa"><i class="fa fa-ban"></i></a>

                                                                    </li>
                                                                    <%} %>
                                                                </ul>
                                                                <div class="form-inline">
                                                                    <button type="button" class="btn btn-primary" onclick="ShowProductForm('Product'); return false">Chọn sản phẩm</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>

                                        </div>
                                    </div>
                                    <div class="tab-pane" id="product">
                                        <iframe id="frame_user" name="frame_user" src="/{CPPath}/FormUserMarketing/Index.aspx/MarketingID/<%= model.RecordID %>" style='position: static; top: 240px; left: 0px; width: 100%; height: 900px; z-index: 999; overflow: auto;' frameborder='no'></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        //setTimeout(function () { reddevil_exec_cmd('[autosave][<%=model.RecordID%>]') }, 5000);
    </script>

</form>
