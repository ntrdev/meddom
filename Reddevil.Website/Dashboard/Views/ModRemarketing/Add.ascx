﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.CPViewControl" %>

<%
    var model = ViewBag.Model as ModRemarketingModel;
    var item = ViewBag.Data as ModRemarketingEntity;
%>

<form id="vswForm" name="vswForm" method="post">
    <input type="hidden" id="_vsw_action" name="_vsw_action" />

    <div class="page-content-wrapper">
        <h3 class="page-title">Showroom <small><%= model.RecordID > 0 ? "Chỉnh sửa": "Thêm mới"%></small></h3>
        <div class="page-bar justify-content-between">
            <ul class="breadcrumb">
                <li class="breadcrumb-item">
                    <i class="fa fa-home"></i>
                    <a href="/{CPPath}/">Home</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="/{CPPath}/<%=CPViewPage.CurrentModule.Code%>/Index.aspx">Remarketing</a>
                </li>
            </ul>
            <div class="page-toolbar">
                <div class="btn-group">
                    <a href="/" class="btn green" target="_blank"><i class="icon-screen-desktop"></i>Xem Website</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <%= ShowMessage()%>

                <div class="form-horizontal form-row-seperated">
                    <div class="portlet">
                        <div class="portlet-title">
                            <div class="caption"></div>
                            <div class="actions btn-set">
                                <%= GetDefaultAddCommand()%>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="portlet box blue-steel">
                                        <div class="portlet-title">
                                            <div class="caption">Thông tin chung</div>
                                        </div>
                                        <div class="portlet-body">
                                            <div class="form-body">
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-right">Tên:</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" name="Name" value="<%=item.Name %>" />
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-right">Loại:</label>
                                                    <div class="col-md-9">
                                                        <select class="form-control" name="Code" id="Code">
                                                            <option value="">Root</option>
                                                            <option value="home" <%if (item.Code == "home"){%>selected="selected"<%} %>>Trang chủ</option>
                                                            <option value="category" <%if (item.Code == "category"){%>selected="selected"<%} %>>Danh mục / Tìm kiếm</option>
                                                            <option value="detail" <%if (item.Code == "detail"){%>selected="selected"<%} %>>Chi tiết sản phẩm</option>
                                                            <option value="cart" <%if (item.Code == "cart"){%>selected="selected"<%} %>>Giỏ hàng / Thanh toán</option>
                                                            <option value="complete" <%if (item.Code == "complete"){%>selected="selected"<%} %>>Mua hàng thành công</option>
                                                            <option value="other" <%if (item.Code == "other"){%>selected="selected"<%} %>>Trang khác</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-right">Mã re-marketing:</label>
                                                    <div class="col-md-9">
                                                        <textarea class="form-control" rows="5" name="Content"><%=item.Content%></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</form>
