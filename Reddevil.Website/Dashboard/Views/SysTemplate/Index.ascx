﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.CPViewControl" %>

<%
    var model = ViewBag.Model as SysTemplateModel;
    var listItem = ViewBag.Data as List<SysTemplateEntity>;
%>

<form id="reddevilForm" name="reddevilForm" method="post">

    <input type="hidden" id="_reddevil_action" name="_reddevil_action" />
    <input type="hidden" id="boxchecked" name="boxchecked" value="0" />



    <div class="pcoded-content">
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8 d-none d-lg-block">
                    <div class="page-header-title">
                        <i class="icon-social-dropbox"></i>
                        <div class="d-inline">
                            <h5>Quản lý Giao diện</h5>
                            <span>Danh sách giao diện </span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class=" breadcrumb breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="/{CPPath}/">
                                    <i class="icon-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="/{CPPath}/<%=CPViewPage.CurrentModule.Code%>/Index.aspx">Giao diện</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>


        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <%= ShowMessage()%>
                                </div>
                                <div class="card">
                                    <div class="card-header header-control">
                                        <h5>Danh sách Giao diện</h5>
                                        <div class="controler-header">
                                            <%=GetSortListCommand()%>
                                        </div>
                                    </div>
                                    <div class="card-header header-control">
                                        <div class="tools col-md-12">
                                            <div class="dataTables_search col-md-6 col-xs-12">
                                                <div class="border-checkbox-section">
                                                    <div class="md-checkbox border-checkbox-group border-checkbox-group-info" data-toggle="tooltip" data-placement="bottom" data-original-title="Click chọn tất cả">
                                                        <input type="checkbox" id="checks" name="toggle" onclick="checkAll('<%=model.PageSize%>')" class="md-check border-checkbox">
                                                        <label for="checks" class="border-checkbox-label"></label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="table-group-actions d-inline-block col-md-4 col-xs-12">
                                            </div>

                                          <%--  <div class="table-group-actions d-inline-block col-md-2 col-xs-12">
                                                <%= ShowDDLLang(model.LangID)%>
                                            </div>--%>

                                        </div>
                                    </div>
                                    <div class="card-block">
                                        <div class=" manager_page">
                                            <div class="row">
                                                <%for (var i = 0; listItem != null && i < listItem.Count; i++)
                                                    { %>
                                                <div class="col-md-4">
                                                    <button type="button" class="btn blue-hoki btn-outline sbold uppercase btn-block btn-lg">
                                                        <%= GetCheckbox(listItem[i].ID, i)%> <%=listItem[i].Name %>
                                                        <span><b>Thiết bị hiển thị: </b><%= listItem[i].DeviceText %></span>
                                                        <span><b>File Hiển Thị: </b><%= listItem[i].File %></span>
                                                        <p data-toggle="tooltip" data-original-title="Chỉnh sửa" onclick="REDDEVILRedirect('Add', <%= listItem[i].ID %>)" class="btn waves-effect waves-light green  btn-icon"><i class="icon-pencil"></i></p>
                                                        <p class="btn waves-effect waves-light red btn-icon" data-toggle="tooltip" data-original-title="ID"><%=listItem[i].ID %></p>
                                                    </button>

                                                </div>

                                                <%} %>
                                            </div>
                                        </div>
                                        <div class="row center">
                                            <div class="col-md-12 col-sm-12 justify-content-center d-flex ">
                                                <%= GetPagination(model.PageIndex, model.PageSize, model.TotalRecord)%>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <script type="text/javascript">
        window.document.title = 'Quản lý Mẫu giao diện' + " | trình quản trị IMEXsoft® ";


        var REDDEVILController = "SysTemplate";

        var REDDEVILArrVar = [
            "filter_lang", "LangID",
            "limit", "PageSize"
        ];

        var REDDEVILArrQT = [
            "<%= model.PageIndex + 1 %>", "PageIndex",
            "<%= model.Sort %>", "Sort"
        ];

        var REDDEVILArrDefault = [
            "1", "PageIndex",
            "1", "LangID",
            "20", "PageSize"
        ];
    </script>

</form>
