﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.CPViewControl" %>

<%
    var model = ViewBag.Model as SysTemplateModel;
    var item = ViewBag.Data as SysTemplateEntity;

    var path = Server.MapPath("~/Views/Shared");

    string[] arrFiles = null;
    if (System.IO.Directory.Exists(path))
        arrFiles = System.IO.Directory.GetFiles(path);
%>

<form id="reddevilForm" name="reddevilForm" method="post">

    <input type="hidden" id="_reddevil_action" name="_reddevil_action" />

    <div class="pcoded-content">
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8 d-none d-lg-block">
                    <div class="page-header-title">
                        <i class="icon-social-dropbox"></i>
                        <div class="d-inline">
                            <h5>Quản lý Giao diện</h5>
                            <span><%= model.RecordID > 0 ? "Cập nhật "+item.Name : "Thêm mới giao diện"%></span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class=" breadcrumb breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="/{CPPath}/">
                                    <i class="icon-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="/{CPPath}/<%=CPViewPage.CurrentModule.Code%>/Index.aspx">Mẫu giao diện</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <%= ShowMessage()%>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="card content-editor">
                                    <div class="card-header header-control">
                                        <h5><%= model.RecordID > 0 ? "Cập nhật "+item.Name : "Thêm mới giao diện"%></h5>

                                    </div>
                                    <div class="card-block pdt-0">
                                        <div class="tabs-scroll">
                                            <ul class="nav nav-tabs md-tabs " role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link active" data-toggle="tab"
                                                        href="#thongtinchung" role="tab"><i class="icon-info"></i>Thông tin chung</a>
                                                    <div class="slide"></div>
                                                </li>

                                            </ul>
                                        </div>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="thongtinchung"
                                                role="tabpanel">
                                                <h4 class="sub-title">Thông tin chung</h4>
                                                <div class="edit-margin-mb form-manager">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <label class="col-sm-12 col-form-label">Tên mẫu giao diện</label>
                                                                    <div class="col-sm-12">
                                                                        <input type="text" class="form-control" id="Name" name="Name" value="<%=item.Name %>" placeholder="nhập vào đây, tối đa 200 ký tự ...">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <label class="col-sm-12 col-form-label">File Hiển Thị</label>
                                                                    <div class="col-sm-12">
                                                                        <select class="form-control" name="File">
                                                                            <%foreach (var file in arrFiles)
                                                                                {
                                                                                    var fileName = System.IO.Path.GetFileName(file);
                                                                            %>
                                                                            <option <%if (item.File == fileName)
                                                                                {%>selected<%} %>
                                                                                value="<%=fileName %>"><%=fileName %></option>
                                                                            <%}%>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <label class="col-sm-12 col-form-label">
                                                                        Thiết bị Hiển thị
                                                                    </label>
                                                                    <div class="col-sm-12">
                                                                        <div class="md-radio-inline form-radio m-b-30">
                                                                            <div class="radio radio-matrial radio-success radio-inline">
                                                                                <label>
                                                                                    <input type="radio" id="rdo_decktop" name="Device" <%= item.Device == 0 ? "checked": "" %> class="md-radiobtn" value="0">
                                                                                    <i class="helper"></i>PC / Laptop
                                                                                </label>
                                                                            </div>
                                                                            <div class="radio radio-matrial radio-success radio-inline">
                                                                                <label>
                                                                                    <input type="radio" id="rdo_mobile" name="Device" <%= item.Device == 1 ? "checked": "" %> value="1" class="md-radiobtn">
                                                                                    <i class="helper"></i>Mobile
                                                                                </label>
                                                                            </div>
                                                                            <div class="radio radio-matrial radio-success radio-inline">
                                                                                <label>
                                                                                    <input type="radio" id="rdo_tablet" name="Device" <%= item.Device == 2 ? "checked": "" %> value="2" class="md-radiobtn">
                                                                                    <i class="helper"></i>Tablet
                                                                                </label>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="">
                                                                <div class="form-group has-warning row ">
                                                                    <label class="col-form-label titleCollap collapsed " data-toggle="collapse">Mã thiết kế</label>
                                                                    <div class="col-sm-12">
                                                                        <div class="plugin-ckEditor collapse" id="ctr-trangthai">
                                                                            <textarea class="form-control custom" name="Custom" id="Custom" rows="15" cols=""><%=item.Custom %></textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-9">
                                <div class="card-header header-control">
                                    <h5>Giao diện</h5>
                                    <div class="controler-header">
                                        <%=GetDefaultAddCommand() %>
                                    </div>
                                </div>
                                <div class="block-controlRight">
                                    <div class="ItemControl">


                                        <div class="panel-collapse collapse show">

                                            <div class="panel-body">

                                                <ul class="sortMaugiaodien" id="content_design">
                                                    <div class="lds-ring">
                                                        <div></div>
                                                        <div></div>
                                                        <div></div>
                                                        <div></div>
                                                    </div>
                                                </ul>
                                                <iframe id="frame_design" name="frame_design" src="/{CPPath}/Design/EditTemplate.aspx?id=<%= model.RecordID %>" onload="get_content_design()" style="height: 0; width: 0; border-width: 0;"></iframe>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>




    <link rel="stylesheet" href="/{CPPath}/interface/utils/chosen/chosen.css" type="text/css" media="all" />
    <script type="text/javascript" src="/{CPPath}/interface/utils/chosen/chosen.js"></script>
    <script type="text/javascript">
        window.document.title = '<%=model.RecordID>0? "Xem, cấu hình mẫu giao diện " +item.Name +" | trình quản trị IMEXsoft® ":"Thêm mới mẫu giao diện | trình quản trị IMEXsoft®"%>';

        $(".titleCollap").click(function () {
            $("#ctr-trangthai").slideToggle();
        });

        var ListOnLoad = new Array();
        var has_update = false;

        function get_content_design() {
            var ReddevilContainer = window.frames["frame_design"].document.getElementById("rdv_container");
            if (ReddevilContainer)
                document.getElementById("content_design").innerHTML = ReddevilContainer.innerHTML;
            else
                document.getElementById("content_design").innerHTML = window.frames["frame_design"].document.childNodes[0].innerHTML;

            if (has_update)
                custom_update();

            for (var i = 0; i < ListOnLoad.length; i++) {
                layout_change(ListOnLoad[i].pid, ListOnLoad[i].list_param, ListOnLoad[i].layout)
            }

            $('[data-toggle="tooltip"]').tooltip();
            $('.chosen-layout').chosen({

                display_selected_options: true,
                placeholder_text_multiple: 'Gõ từ khóa để tìm kiếm',
                no_results_text: 'Không có kết quả',
                enable_split_word_search: true,
                search_contains: true,
                display_disabled_options: true,
                single_backstroke_delete: false,
                inherit_select_classes: true
            });
        }

        function custom_update() {
            var ranNum = Math.floor(Math.random() * 999999);
            var dataString = "";
            $.ajax({
                url: "/{CPPath}/Ajax/TemplateGetCustom/<%= item.ID %>/?rnd=" + ranNum,
                type: "get",
                data: dataString,
                dataType: 'json',
                success: function (data) {
                    var content = data.Html;
                    if (content !== "") $("#Custom").html(content);
                },
                error: function (status) { }
            });
        }

        function cp_update(value) {

            has_update = true;

            var input = document.createElement("input");
            input.type = "hidden";
            input.name = "rdv_submit";
            input.value = value;

            var cpForm = window.frames["frame_design"].document.forms[0];

            var arrTagInput = window.frames["frame_design"].document.getElementsByTagName("input");
            var i;
            for (i = 0; i < arrTagInput.length; i++) {
                if (document.getElementById(arrTagInput[i].id))
                    arrTagInput[i].value = document.getElementById(arrTagInput[i].id).value;
            }

            var arrTagSelect = window.frames["frame_design"].document.getElementsByTagName("select");
            for (i = 0; i < arrTagSelect.length; i++) {
                if (document.getElementById(arrTagSelect[i].id))
                    arrTagSelect[i].value = document.getElementById(arrTagSelect[i].id).value;
            }

            cpForm.appendChild(input);
            cpForm.submit();

        }

        function dragStart(ev) {
            ev.dataTransfer.effectAllowed = "move";
            ev.dataTransfer.setData("Text", ev.target.getAttribute("id"));
            ev.dataTransfer.setDragImage(ev.target, 0, 0);
            return true;
        }
        function dragEnter(ev) {
            ev.preventDefault();
            return true;
        }
        function dragOver() {
            return false;
        }
        function dragDrop(ev) {
            var src = ev.dataTransfer.getData("Text");

            var id = ev.target.id;

            if (id !== "to_rdvid_" + src)
                cp_update(id + "$" + src + "|move");

            //ev.target.appendChild(document.getElementById(src));

            ev.stopPropagation();
            return false;
        }
        function do_display(idTbl) {
            var o = document.getElementById(idTbl);
            if (o.style.display === "")
                o.style.display = "none";
            else
                o.style.display = "";
        }
    </script>
</form>
