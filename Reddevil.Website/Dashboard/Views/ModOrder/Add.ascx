﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.CPViewControl" %>

<%
    var model = ViewBag.Model as ModOrderModel;
    var item = ViewBag.Data as ModOrderEntity;

    var listItem = item.GetOrderDetailCode();

    string html = !string.IsNullOrEmpty(item.Name) ? item.Name + "\n" : "";
    html += !string.IsNullOrEmpty(item.Email) ? item.Email + "\n" : "";
    html += !string.IsNullOrEmpty(item.Phone) ? item.Phone + "\n" : "";
    html += !string.IsNullOrEmpty(item.Address) ? item.Address + "\n" : "";
    html += !string.IsNullOrEmpty(item.Content) ? item.Content + "\n\n" : "";
%>

<form id="reddevilForm" name="reddevilForm" method="post">
    <input type="hidden" id="_reddevil_action" name="_reddevil_action" />

    <div class="page-content">
        <div class="breadcrumbs">
            <h1><%= model.RecordID > 0 ? "Cập nhật đơn hàng" : "Thêm mới đơn hàng"%></h1>
            <ol class="breadcrumb">
                <li>
                    <a href="/{CPPath}/"><i class="fa fa-home"></i>Home</a>
                </li>
                <li class="active"><a href="/{CPPath}/<%=CPViewPage.CurrentModule.Code%>/Index.aspx">Đơn hàng</a></li>
            </ol>
        </div>
        <div class="row">
            <div class="col-md-12">
                <%= ShowMessage()%>
                <div class="control_heading">
                    <div class="btn-group btn-group-circle">
                        <%=GetDefaultAddCommand() %>
                    </div>
                </div>
            </div>
        </div>
        <div class="row pagepost">

            <div class="col-sm-6">

                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-green-sharp">
                            <i class="icon-speech font-green-sharp"></i>
                            <span class="caption-subject bold uppercase">Thông tin người mua</span>
                            <span class="caption-helper">thông tin đơn hàng</span>
                        </div>
                        <div class="actions">
                            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"></a>
                        </div>
                    </div>
                    <div class="portlet-body">

                        <div class="form-body">
                            <%if (item.UserID < 1)
                                { %>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input ">
                                        <input type="text" class="form-control" id="Name" disabled value="<%=item.Name %>" />
                                        <label for="form_control_1">Tên người mua:</label>
                                        <span class="help-block">Ký tự tối đa 200</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input ">
                                        <input type="text" class="form-control" disabled id="Phone" value="<%=item.Phone %>" />
                                        <label for="form_control_1">Số điện thoại người mua:</label>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input ">
                                        <input type="text" class="form-control" disabled id="Email" value="<%=item.Email %>" placeholder="nhập vào đây ...">
                                        <label for="form_control_1">Email:</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input ">
                                        <input type="text" class="form-control" id="Address" disabled value="<%=item.Address %>" />
                                        <label for="form_control_1">Địa chỉ:</label>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input ">
                                        <input type="text" class="form-control" id="PaymentID" disabled value="<%=item.GetPayment().Name%>" />
                                        <label for="form_control_1">Hình thức thanh toán:</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input">
                                        <textarea class="form-control" rows="2" name="Summary" disabled placeholder="Ghi chú của người mua"><%=item.Content %></textarea>
                                        <label for="form_control_1">Mô tả tóm tắt:</label>
                                        <span class="help-block">Mô tả ngắn về sản phẩm</span>
                                    </div>
                                </div>
                            </div>
                            <%}
                                else
                                {%>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input ">
                                        <input type="text" class="form-control" id="Name" disabled value="<%=item.GetUser().Name %>" />
                                        <label for="form_control_1">Tên người mua:</label>
                                        <span class="help-block">Ký tự tối đa 200</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input ">
                                        <input type="text" class="form-control" disabled id="Phone" value="<%=Utils.FormatPhoneNumber(item.GetUser().Phone) %>" />
                                        <label for="form_control_1">Số điện thoại người mua:</label>
                                        <span class="help-block">ví dụ: san-pham-tai-nghe-cao-cap</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input ">
                                        <input type="text" class="form-control" disabled id="Email" value="<%=item.GetUser().Email %>" placeholder="nhập vào đây ...">
                                        <label for="form_control_1">Email:</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input ">
                                        <input type="text" class="form-control" id="Address" disabled value="<%=item.GetUser().GetAddressReturn().GetAddress() %>" />
                                        <label for="form_control_1">Địa chỉ:</label>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input ">
                                        <input type="text" class="form-control" id="PaymentID" disabled value="<%=item.GetPayment().Name%>" />
                                        <label for="form_control_1">Hình thức thanh toán:</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input">
                                        <textarea class="form-control" rows="1" disabled placeholder="Ghi chú của người mua"><%=item.Content %></textarea>
                                        <label for="form_control_1">Mô tả tóm tắt:</label>
                                        <span class="help-block">Mô tả ngắn về sản phẩm</span>
                                    </div>
                                </div>
                            </div>
                            <%} %>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-green-sharp">
                            <i class="icon-speech font-green-sharp"></i>
                            <span class="caption-subject bold uppercase">Thông tin chung</span>
                            <span class="caption-helper">thông tin đơn hàng</span>
                        </div>
                        <div class="actions">
                            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"></a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="ItemControl">

                                <div id="ctr-advance" class="panel-collapse collapse in" style="height: 0px;" aria-expanded="false">
                                    <div class="panel-body">
                                        <div class="form-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group form-md-line-input ">
                                                        <select class="form-control chosen" name="StatusID" id="StatusID">
                                                            <%= Utils.ShowDdlMenuByType("Status", model.LangID, item.StatusID)%>
                                                        </select>
                                                        <label for="form_control_1">Tình trạng:</label>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group form-md-line-input ">
                                                        <input type="text" class="form-control" id="Code" disabled value="<%=item.Code %>" />
                                                        <label for="form_control_1">Mã đơn hàng:</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group form-md-line-input ">
                                                        <input type="text" class="form-control" id="Total" value="<%=Utils.FormatMoney(item.Total) %>">
                                                        <label for="form_control_1">Tổng tiền:</label>
                                                        <span class="help-block"><%=Utils.NumberToWord(item.Total.ToString()) %></span>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group form-md-line-input ">
                                                        <input type="text" class="form-control" id="TradingCode" value="<%=item.TradingCode %>" />
                                                        <label for="form_control_1">Mã giao dịch qua ví điện tử:</label>
                                                        <span class="help-block"><%=item.OrderType %></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group form-md-line-input ">
                                                        <input type="text" class="form-control" id="" disabled value="<%=item.GetPayment().Name%>" />
                                                        <label for="form_control_1">Hình thức thanh toán:</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group form-md-line-input ">
                                                        <input type="text" class="form-control" id="Created" disabled value="<%=Utils.FormatDateTime(item.Created)%>" />
                                                        <label for="form_control_1">Ngày mua:</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Trạng thái:</label>
                                                    <div class="md-radio-inline">
                                                        <div class="md-radio">
                                                            <input type="radio" id="activity1" name="Success" value="1" class="md-radiobtn" <%=item.Success?"checked":"" %>>
                                                            <label for="activity1">
                                                                <span></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Đã hoàn thành
                                                            </label>
                                                        </div>
                                                        <div class="md-radio has-error">
                                                            <input type="radio" id="activity2" name="Success" value="0" class="md-radiobtn" <%=!item.Success?"checked":"" %>>
                                                            <label for="activity2">
                                                                <span></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Chưa hoàn thành
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Trạng thái thanh toán:</label>
                                                    <div class="md-radio-inline">
                                                        <div class="md-radio">
                                                            <input type="radio" id="Paid1" name="Paid" value="1" class="md-radiobtn" <%=item.Paid?"checked":"" %>>
                                                            <label for="Paid1">
                                                                <span></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Đã thanh toán
                                                            </label>
                                                        </div>
                                                        <div class="md-radio has-error">
                                                            <input type="radio" id="Paid2" name="Paid" value="0" class="md-radiobtn" <%=!item.Paid?"checked":"" %>>
                                                            <label for="Paid2">
                                                                <span></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Chưa thanh toán
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-green-sharp">
                            <i class="icon-speech font-green-sharp"></i>
                            <span class="caption-subject bold uppercase">Thông tin sản phẩm</span>
                            <span class="caption-helper">danh sách sản phẩm đặt mua</span>
                        </div>
                        <div class="actions">
                            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"></a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="ItemControl">
                                <div id="" class="panel-collapse collapse in" style="height: 0px;" aria-expanded="false">
                                    <div class="panel-body">
                                        <div class="form-body">
                                            <div class="row">
                                                <div class="dt-responsive table-responsive col-md-12">
                                                    <table id="" class="table_basic table table-hover m-b-0 " style="width: 100%">
                                                        <thead>
                                                            <tr>
                                                                <th class="sorting text-center w1p">STT</th>

                                                                <th class="sorting"><%= GetSortUnLink("Tên sản phẩm", "Name")%></th>
                                                                <th class="sorting text-center w10p hidden-sm hidden-col"><%= GetSortUnLink("Ảnh", "File")%></th>
                                                                <th class="sorting text-center w10p hidden-sm hidden-col"><%= GetSortUnLink("Giá mua sp", "Price")%></th>
                                                                <th class="sorting text-center w10p hidden-sm hidden-col"><%= GetSortUnLink("SL", "Quantity")%></th>
                                                                <th class="sorting text-center w10p hidden-sm hidden-col"><%= GetSortUnLink("Khuyến mại", "Code")%></th>
                                                                <th class="sorting text-center w10p hidden-sm hidden-col"><%= GetSortUnLink("Thành tiền", "Total")%></th>
                                                                <th class="text-center">Tùy chọn</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <%for (var i = 0; listItem != null && i < listItem.Count; i++)
                                                                {
                                                                    var product = ModProductService.Instance.GetByID_Cache(listItem[i].ProductID);
                                                                    if (product == null) continue;

                                                                    html += (i + 1) + ") " + product.Name + " - Số lượng: " + listItem[i].Quantity + "\n";
                                                            %>
                                                            <tr>
                                                                <td class="text-center"><%= i + 1%></td>

                                                                <td>
                                                                    <a href="/{CPPath}/ModProduct/Add.aspx/RecordID/<%= product.ID %>" target="_blank"><%= product.Name%></a>
                                                                    <p class="smallsub hidden-sm hidden-col">(<a target="_blank" href="<%=CoreMr.Reddevil.Web.HttpRequest.Domain+"/"+product.Url %><%=Setting.Sys_PageExt %>"> <span>Xem trên web</span>: <%= product.Sku%> </a>)</p>
                                                                </td>
                                                                <td class="text-center hidden-sm hidden-col">
                                                                    <%= Utils.GetMedia(product.FileAvatar, 40, 40)%>
                                                                </td>
                                                                <td class="text-center hidden-sm hidden-col"><%= string.Format("{0:#,##0}", listItem[i].Price) %>đ</td>
                                                                <td class="text-center hidden-sm hidden-col"><%=listItem[i].Quantity %></td>
                                                                <td class="text-center hidden-sm hidden-col"><%=listItem[i].GiffCode%></td>
                                                                <td class="text-center hidden-sm hidden-col"><%= string.Format("{0:#,##0}", listItem[i].Price * listItem[i].Quantity) %></td>
                                                                <td class="text-center">
                                                                    <div class="control-button">
                                                                        <button class="btn waves-effect waves-light btn-danger btn-icon" data-toggle="tooltip" data-original-title="ID" type="button"><%=listItem[i].ID %></button>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <%} %>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>


            </div>
            <div class="col-sm-6">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-green-sharp">
                            <i class="icon-speech font-green-sharp"></i>
                            <span class="caption-subject bold uppercase">Tiện ích</span>
                        </div>
                        <div class="actions">
                            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"></a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="ItemControl">
                                <div id="" class="panel-collapse collapse in" style="height: 0px;" aria-expanded="false">
                                    <div class="panel-body">
                                        <div class="form-body">
                                            <div class="row">
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-right">Nội dung:</label>
                                                    <div class="col-md-9">
                                                        <textarea class="form-control" rows="10" id="html"><%=html%></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-right">&nbsp;</label>
                                                    <div class="col-md-9">
                                                        <button type="button" class="btn btn-primary" onclick="copyToClipboard('#html'); return false">Copy</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
