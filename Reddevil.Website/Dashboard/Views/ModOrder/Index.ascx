﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.CPViewControl" %>

<% 
    var model = ViewBag.Model as ModOrderModel;
    var listItem = ViewBag.Data as List<ModOrderEntity>;
    int totalRc = ViewBag.AllOrder;
    int totalss = ViewBag.Sucess;
    long total = ViewBag.Total;
    string[] _doanhthu = new string[12];
    int year = DateTime.Now.Year;

    if (model.Date > DateTime.MinValue)
    {
        long _tongdoanhthu = 0;
        foreach (var item in listItem)
        {
            _tongdoanhthu += item.Total;
        }
        _doanhthu[model.Month - 1] = _tongdoanhthu.ToString();
    }
    else if (model.Date <= DateTime.MinValue)
    {
        for (int i = 1; i < 13; i++)
        {
            var Doanhthuthang = ModOrderService.Instance.CreateQuery()
                                           .Select(o => new { o.Total, o.Year, o.Month })
                                           .Where(o => o.Success == true && o.Month == i && o.Year == year)
                                           .ToList_Cache();
            if (Doanhthuthang != null)
            {
                long _tongdoanhthu = 0;
                foreach (var item in Doanhthuthang)
                {
                    _tongdoanhthu += item.Total;
                }
                _doanhthu[i - 1] = _tongdoanhthu.ToString();
            }
        }
    }
%>

<form id="reddevilForm" name="reddevilForm" method="post">

    <input type="hidden" id="_reddevil_action" name="_reddevil_action" />
    <input type="hidden" id="boxchecked" name="boxchecked" value="0" />

    <div class="page-content">
        <div class="breadcrumbs">
            <h1>Quản lý đơn hàng</h1>
            <ol class="breadcrumb">
                <li><a href="/{CPPath}/"><i class="fa fa-home"></i>Home</a></li>
                <li class="active"><a href="/{CPPath}/<%=CPViewPage.CurrentModule.Code%>/Index.aspx">Đơn hàng</a></li>
            </ol>
        </div>
        <div class="row">
            <div class="col-md-12">
                <%= ShowMessage()%>
                <div class="control_heading">
                    <div class="btn-group btn-group-circle">
                        <%=GetDefaultListCommand() %>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject bold uppercase">Danh sách đơn hàng</span>
                        </div>
                        <div class="tools col-md-9">
                            <div class="table-group-actions d-inline-block col-md-3 col-xs-12">
                                <select id="filter_menu" class="form-control input-sm chosen_menu" onchange="REDDEVILRedirect()" size="1">
                                    <option value="0">(Tình trạng đơn hàng)</option>
                                    <%= Utils.ShowDdlMenuByType2("Status", model.LangID, model.StatusID)%>
                                </select>
                            </div>
                            <div class="dataTables_search col-md-4 col-xs-12">
                                <input type="text" onclick="this.type = 'date'" class="form-control input-inline input-sm" id="filter_date" value="<%=Utils.FormatDate(model.Date) %>" placeholder="Chọn tháng cần xem thống kê" onchange="REDDEVILRedirect();return false;" />
                                <button type="button" class="btn btn-default blue" style="height: 29.5px; padding: 5px" onclick="location.href = '/{CPPath}/<%=CPViewPage.CurrentModule.Code%>/Index.aspx'" data-toggle="tooltip" data-placement="bottom" data-original-title="Xem tất cả" aria-describedby="tooltip198890"><i class="fa fa-plus"></i><span class="hidden-xs">Xem tất cả</span></button>
                            </div>
                            <div class="table-group-actions d-inline-block col-md-2 col-xs-12" style="float: right">
                                <%= ShowDDLLang(model.LangID)%>
                            </div>
                            <div class="dataTables_search col-md-3 col-xs-12 " style="float: right">
                                <input type="text" class="form-control  input-sm" id="filter_search" style="float: left; width: 100%; position: relative; height: 32px; text-indent: 10px; font-size: 14px; border-radius: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px;"
                                    value="<%= model.SearchText %>" placeholder="Nhập từ khóa cần tìm" onchange="REDDEVILRedirect();return false;" />
                                <button type="submit" class="btntop" onclick="REDDEVILRedirect();return false;" style="float: right; width: 40px; height: 32px; border: 0; cursor: pointer; background: none; position: absolute; right: 15px;">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>


                        <div class="portlet-body">
                            <div class="row list-separated text-center">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                    <div class="font-grey-mint font-sm">Tổng doanh thu(tất cả đơn hàng)</div>
                                    <div class="uppercase font-hg font-red-flamingo">
                                        <%=Utils.FormatMoney(total) %>
                                        <span class="font-lg font-grey-mint">VNĐ</span>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                    <div class="font-grey-mint font-sm">Số lượng đơn hàng </div>
                                    <div class="uppercase font-hg theme-font">
                                        <%=Utils.FormatMoney(totalRc) %>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                    <div class="font-grey-mint font-sm">Đơn hàng đã hoàn thành </div>
                                    <div class="uppercase font-hg theme-font">
                                        <%=Utils.FormatMoney(totalss) %>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="dt-responsive table-responsive">
                            <table id="" class="table_basic table table-hover m-b-0 " style="width: 100%">
                                <thead>
                                    <tr>
                                        <th class="sorting text-center w1p">STT</th>
                                        <th class="sorting_disabled text-center w1p">
                                            <div class="md-checkbox">
                                                <input type="checkbox" id="checks" name="toggle" onclick="checkAll('<%=model.PageSize%>')" class="md-check">
                                                <label for="checks">
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span>Chọn
                                                </label>
                                            </div>
                                        </th>
                                        <th class="sorting"><%= GetSortLink("Mã đơn hàng", "Code")%></th>
                                        <th class="sorting text-center hidden-sm hidden-col"><%= GetSortLink("Họ và tên", "Name")%></th>
                                        <th class="sorting text-center w10p"><%= GetSortLink("Tổng tiền", "Total")%></th>
                                        <th class="sorting text-center w10p hidden-sm hidden-col"><%= GetSortLink("Hình thức thanh toán", "PaymentID")%></th>
                                        <th class="sorting text-center  hidden-sm hidden-col"><%= GetSortLink("Mã giao dịch qua ví ĐT", "TrandingCode")%></th>
                                        <th class="sorting text-center w10p hidden-sm hidden-col"><%= GetSortLink("Tình trạng", "StatusID")%></th>
                                        <th class="sorting text-center hidden-sm hidden-col"><%= GetSortLink("Trạng thái", "Success")%></th>
                                        <th class="sorting text-center w10p hidden-sm hidden-col"><%= GetSortLink("Ngày mua", "Created")%></th>
                                        <th class="text-center">Trạng thái</th>
                                        <th class="text-center">Tùy chọn</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <%for (var i = 0; listItem != null && i < listItem.Count; i++)
                                        { %>
                                    <tr>
                                        <td class="text-center"><%= i + 1%></td>
                                        <td class="text-center">
                                            <%= GetCheckbox(listItem[i].ID, i)%>
                                        </td>
                                        <td><a href="javascript:REDDEVILRedirect('Add', <%= listItem[i].ID %>)"><%= listItem[i].Code%></a></td>
                                        <td class="text-center hidden-sm hidden-col"><%= listItem[i].Name%></td>
                                        <td class="text-center hidden-sm hidden-col"><%= string.Format("{0:#,##0}", listItem[i].Total) %>đ</td>
                                        <td class="text-center hidden-sm hidden-col"><%= GetName(listItem[i].GetPayment()) %></td>
                                        <td class="text-center hidden-sm hidden-col"><%=listItem[i].TradingCode%></td>
                                        <td class="text-center hidden-sm hidden-col"><%= listItem[i].GetStatus().Name%></td>
                                        <td class="text-center hidden-sm hidden-col">
                                            <%if (listItem[i].Paid)
                                                { %><a href="javascript:void(0)" style="color: #559d01" data-toggle="tooltip" data-placement="bottom" title="Đã thanh toán" data-original-title="Đã thanh toán"> <span class="fa fa-check-circle publish"></span></a><%}
                                                                                                                                                                                                                                                                         else
                                                                                                                                                                                                                                                                         { %><a href="javascript:void(0)" style="color: red" data-toggle="tooltip" data-placement="bottom" title="Chưa thanh toán" data-original-title="Chưa thanh toán"> <span class="fa fa-times-circle-o unpublish"></span></a><%} %></td>
                                        <td class="text-center hidden-sm hidden-col"><%= string.Format("{0:dd-MM-yyyy HH:mm}", listItem[i].Created) %></td>
                                        <td class="text-center hidden-sm hidden-col">
                                            <%if (listItem[i].Success)
                                                { %><a href="javascript:void(0)" style="color: #559d01" data-toggle="tooltip" data-placement="bottom" title="Đã hoàn thành" data-original-title="Đã hoàn thành"> <span class="fa fa-check-circle publish"></span></a><%}
                                                                                                                                                                                                                                                                         else
                                                                                                                                                                                                                                                                         { %><a href="javascript:void(0)" style="color: red" data-toggle="tooltip" data-placement="bottom" title="Chưa hoàn thành" data-original-title="Chưa hoàn thành"> <span class="fa fa-times-circle-o unpublish"></span></a><%} %></td>
                                        <td class="text-center">
                                            <div class="control-button">
                                                <button data-toggle="tooltip" data-original-title="Chỉnh sửa" onclick="REDDEVILRedirect('Add', <%= listItem[i].ID %>)" type="button" class="btn waves-effect waves-light btn-primary btn-icon"><i class="icon-pencil"></i></button>
                                                <button class="btn waves-effect waves-light btn-danger btn-icon" data-toggle="tooltip" data-original-title="ID" type="button"><%=listItem[i].ID %></button>
                                            </div>
                                        </td>
                                    </tr>
                                    <%} %>
                                </tbody>
                            </table>
                        </div>
                        <div class="row center">
                            <div class="col-md-12 col-sm-12 justify-content-center d-flex ">
                                <%= GetPagination(model.PageIndex, model.PageSize, model.TotalRecord)%>
                            </div>
                        </div>
                        <div class="portlet box green-haze box-logs">
                            <div class="portlet-title">
                                <div class="caption caption-md">
                                    <div class="caption">
                                        <i class="icon-bar-chart font-dark hide"></i>&nbsp;Tăng trưởng doanh thu năm <%= year %>
                                    </div>
                                </div>
                                <div class="actions">
                                    <div class="btn-group btn-group-devided" data-toggle="buttons">
                                    </div>
                                </div>

                                <div class="hidden-xs col-md-12 col-sm-12" style="height: 500px">
                                    <div id="widgetChart_tangtruong" style="height: 100%"></div>
                                </div>
                                <div class="hidden-lg hidden-md text-center note-show-mb">
                                    Dữ liệu chỉ hiện thị được trên thiết bị Desktop
																	
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>





    <script type="text/javascript" src="/{CPPath}/interface/utils/echarts/echarts.min.js"></script>
    <script type="text/javascript">

        var REDDEVILController = 'ModOrder';

        var REDDEVILArrVar = [
            'filter_lang', 'LangID',
            'limit', 'PageSize',
            "filter_date", "Date",
            "filter_menu", "StatusID"
        ];

        var REDDEVILArrVar_QS = [
            'filter_search', 'SearchText'
        ];

        var REDDEVILArrQT = [
            '<%= model.PageIndex + 1 %>', 'PageIndex',
            '<%= model.Sort %>', 'Sort'
        ];

        var REDDEVILArrDefault = [
            '1', 'PageIndex',
            '1', 'LangID',
            '20', 'PageSize'
        ];

<%if (model.Date > DateTime.MinValue)
        { %>
        var dom = document.getElementById("widgetChart_tangtruong");
        var myChart = echarts.init(dom);
        var app = {};
        option = null;


        option = {
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'shadow',

                }
            },

            legend: {
                data: ['Doanh thu']
            },
            xAxis: [
                {
                    type: 'category',
                    data: ['<%="Tháng "+model.Month + " năm "+ model.Date.Year%>'],
                    axisPointer: {
                        type: 'shadow'
                    },
                    name: 'Thời gian',
                }
            ],
            yAxis: [
                {
                    type: '',
                    name: 'Doanh thu',
                },
            ],
            series: [
                {
                    name: 'Doanh thu',
                    type: 'bar',
                    stack: 'groupCharts',
                    data: [<%= _doanhthu[model.Month-1]%>]
                }
            ]
        };
        ;
        if (option && typeof option === "object") {
            myChart.setOption(option, true);
        }
            <%}
        else
        {%>
        var dom = document.getElementById("widgetChart_tangtruong");
        var myChart = echarts.init(dom);
        var app = {};
        option = null;


        option = {
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'shadow',

                }
            },

            legend: {
                data: ['Doanh thu']
            },
            xAxis: [
                {
                    type: 'category',
                    data: ['T 1', 'T 2', 'T 3', 'T 4', 'T 5', 'T 6', 'T 7', 'T 8', 'T 9', 'T 10', 'T 11', 'T 12'],
                    axisPointer: {
                        type: 'shadow'
                    },
                    name: 'Thời gian',
                }
            ],
            yAxis: [
                {
                    type: '',
                    name: 'Doanh thu',
                },
            ],
            series: [
                {
                    name: 'Doanh thu',
                    type: 'line',
                    stack: 'groupCharts',
                    data: [<%=string.Join(",", _doanhthu)%>]
                }
            ]
        };
        ;
        if (option && typeof option === "object") {
            myChart.setOption(option, true);
        }
            <%}%>
</script>
</form>
