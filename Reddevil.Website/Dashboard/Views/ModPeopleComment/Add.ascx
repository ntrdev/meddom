﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.CPViewControl" %>

<%
    var model = ViewBag.Model as ModPeopleCommentModel;
    var item = ViewBag.Data as ModPeopleCommentEntity;
%>

<form id="reddevilForm" name="reddevilForm" method="post">
    <input type="hidden" id="_reddevil_action" name="_reddevil_action" />

    <div class="page-content">
        <div class="breadcrumbs">
            <h1><%= model.RecordID > 0 ? "Cập nhật nhận xét" : "Thêm mới"%></h1>
            <ol class="breadcrumb">
                <li>
                    <a href="/{CPPath}/"><i class="fa fa-home"></i>Home</a>
                </li>
                <li class="active"><a href="/{CPPath}/<%=CPViewPage.CurrentModule.Code%>/Index.aspx">Nhận xét</a></li>
            </ol>
        </div>
        <div class="row">
            <div class="col-md-12">
                <%= ShowMessage()%>
                <div class="control_heading">
                    <div class="btn-group btn-group-circle">
                        <%=GetDefaultAddCommand() %>
                    </div>
                </div>
            </div>
        </div>
        <div class="row pagepost">

            <div class="col-sm-9">

                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-green-sharp">
                            <i class="icon-speech font-green-sharp"></i>
                            <span class="caption-subject bold uppercase">Thông tin chung</span>
                            <span class="caption-helper">thông tin khách hàng</span>
                        </div>
                        <div class="actions">
                            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"></a>
                        </div>
                    </div>
                    <div class="portlet-body">

                        <div class="form-body">
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input ">
                                        <input type="text" class="form-control" id="Name" name="Name" value="<%=item.Name %>" placeholder="nhập vào đây ...">
                                        <label for="form_control_1">Tên khách hàng:</label>
                                        <span class="help-block">Ký tự tối đa 200</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input ">
                                        <input type="text" class="form-control" id="Url" name="Url" value="<%=item.Url %>" placeholder="">
                                        <label for="form_control_1">URL khách hàng:</label>
                                        <span class="help-block">ví dụ: https://WEBTINHTE.vn</span>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group form-md-line-input">
                                        <textarea class="form-control" rows="6" name="Content" placeholder="Nhấn Enter để thêm tùy chọn khác"><%=item.Content %></textarea>
                                        <label for="form_control_1">Nhận xét của khách hàng:</label>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-green-sharp">
                            <i class="icon-speech font-green-sharp"></i>
                            <a class="titleCollap collapsed" data-toggle="collapse" href="#ctr-advance" aria-expanded="false"><span class="caption-subject bold uppercase">Hình ảnh đại diện</span></a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="ItemControl">

                                <div id="ctr-advance" class="panel-collapse collapse" style="height: 0px;" aria-expanded="false">
                                    <div class="panel-body">
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input class="input-file" type="button" id="imgInp" onclick="ShowFileForm('File'); return false">
                                                <label tabindex="0" for="my-file" class="input-file-trigger text-center">
                                                    <i class="icon-cloud-upload"></i>&nbsp;Chọn ảnh đại diện</label>
                                                <img id="show_img_upload" src="<%=Utils.DefaultImage(item.File) %>" />
                                                <input type="hidden" class="form-control" name="File" id="File" value="<%=item.File %>" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-green-sharp">
                            <i class="icon-speech font-green-sharp"></i>
                            <span class="caption-subject bold uppercase">Thuộc tính</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">

                            <%if (CPViewPage.UserPermissions.Approve)
                                {%>
                            <div class="col-md-12">
                                <label>Trạng thái</label>
                                <div class="md-radio-inline">
                                    <div class="md-radio">
                                        <input type="radio" id="activity1" name="Activity" value="1" class="md-radiobtn" <%=item.Activity?"checked":"" %>>
                                        <label for="activity1">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span>Duyệt
                                        </label>
                                    </div>
                                    <div class="md-radio has-error">
                                        <input type="radio" id="activity2" name="Activity" value="0" class="md-radiobtn" <%=!item.Activity?"checked":"" %>>
                                        <label for="activity2">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span>Không duyệt
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <%} %>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <script type="text/javascript">
        //setTimeout(function () { reddevil_exec_cmd('[autosave][<%=model.RecordID%>]') }, 5000);
    </script>

</form>
