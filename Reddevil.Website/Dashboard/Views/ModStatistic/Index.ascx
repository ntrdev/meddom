﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.CPViewControl" %>

<%
    var model = ViewBag.Model as ModStatisticModel;
    var listItem = ViewBag.Data as List<ModStatisticEntity>;
%>

<form id="vswForm" name="vswForm" method="post">

    <input type="hidden" id="_vsw_action" name="_vsw_action" />
    <input type="hidden" id="boxchecked" name="boxchecked" value="0" />

    <div class="page-content-wrapper">
        <h3 class="page-title">Thống kê truy cập</h3>
        <div class="page-bar justify-content-between">
            <ul class="breadcrumb">
                <li class="breadcrumb-item">
                    <i class="fa fa-home"></i>
                    <a href="/{CPPath}/">Home</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="/{CPPath}/<%=CPViewPage.CurrentModule.Code%>/Index.aspx">Thống kê truy cập</a>
                </li>
            </ul>
            <div class="page-toolbar">
                <div class="btn-group">
                    <a href="/" class="btn green" target="_blank"><i class="icon-screen-desktop"></i>Xem Website</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <%= ShowMessage()%>

                <div class="portlet portlet">
                    
                    <div class="portlet-body">
                        <div class="dataTables_wrapper">
                            <div class="row hidden-sm hidden-col">
                                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                    
                                </div>
                                <div class="col-12 col-sm-12 col-md-8 col-lg-8 text-right pull-right">
                                    <div class="table-group-actions d-inline-block">
                                    </div>
                                    <div class="table-group-actions d-inline-block">
                                    </div>
                                </div>
                            </div>

                            <div class="table-scrollable">
                                <table class="table table-striped table-hover table-bordered dataTable">
                                    <thead>
                                        <tr>
                                            <th class="sorting text-center w1p">#</th>
                                            <th class="sorting"><%= GetSortLink("Link", "Link")%></th>
                                            <th class="sorting text-center hidden-sm hidden-col"><%= GetSortLink("Lượt xem", "View")%></th>
                                            <th class="sorting text-center w1p"><%= GetSortLink("ID", "ID")%></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%for (var i = 0; listItem != null && i < listItem.Count; i++){
                                                var listItemDetail = ModStatisticDetailService.Instance.CreateQuery().Where(o => o.StatisticID == listItem[i].ID).OrderByDesc(o => o.View).ToList_Cache();
                                                long count = 0;
                                        %>
                                        <tr>
                                            <td class="text-center"><%= i + 1%></td>
                                            <td>
                                                <a href="#" data-toggle="modal" data-target="#statistic-<%=listItem[i].ID %>"><%=VSW.Core.Web.HttpRequest.Domain %><%= listItem[i].Link%></a>

                                                <div id="statistic-<%=listItem[i].ID %>" class="modal fade" role="dialog">
                                                    <table class="table table-striped table-hover table-bordered dataTable">
                                                        <thead>
                                                            <tr>
                                                                <th class="sorting text-center w1p">#</th>
                                                                <th class="sorting"><%= GetSortLink("Thành phố", "City")%></th>
                                                                <th class="sorting text-center hidden-sm hidden-col"><%= GetSortLink("Lượt xem", "View")%></th>
                                                                <th class="sorting text-center w1p"><%= GetSortLink("ID", "ID")%></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <%for (var j = 0; listItemDetail != null && j < listItemDetail.Count; j++){
                                                                    count += listItemDetail[j].View;
                                                            %>
                                                            <tr>
                                                                <td class="text-center"><%= j + 1%></td>
                                                                <td>
                                                                    <%= listItemDetail[j].City%><br /><%= listItemDetail[j].Country + "-" + listItemDetail[j].CountryCode%>
                                                                </td>
                                                                <td class="text-center hidden-sm hidden-col"><%= string.Format("{0:#,##0}", listItemDetail[j].View) %></td>
                                                                <td class="text-center"><%= listItem[i].ID%></td>
                                                            </tr>
                                                            <%} %>
                                                        </tbody>
                                                    </table>
                                                </div>

                                            </td>
                                            <td class="text-center hidden-sm hidden-col"><%= string.Format("{0:#,##0}", count) %></td>
                                            <td class="text-center">
                                                <%= listItem[i].ID%>
                                            </td>
                                        </tr>
                                        <%} %>
                                    </tbody>
                                </table>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 justify-content-center d-flex ">
                                    <%= GetPagination(model.PageIndex, model.PageSize, model.TotalRecord)%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">

        var VSWController = "ModStatistic";

        var VSWArrVar = [
            "limit", "PageSize"
        ];

        var VSWArrVar_QS = [
        ];

        var VSWArrQT = [
            "<%= model.PageIndex + 1 %>", "PageIndex",
            "<%= model.Sort %>", "Sort"
        ];

        var VSWArrDefault = [
            "1", "PageIndex",
            "20", "PageSize"
        ];
    </script>

</form>
