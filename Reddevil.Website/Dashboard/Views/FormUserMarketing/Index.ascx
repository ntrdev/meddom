﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.CPViewControl" %>

<%
    var model = ViewBag.Model as FormUserMarketingModel;
    var listItem = ViewBag.Data as List<ModSellerEntity>;
%>
<form id="reddevilForm" name="reddevilForm" method="post">

    <input type="hidden" id="_reddevil_action" name="_reddevil_action" />
    <input type="hidden" id="boxchecked" name="boxchecked" value="0" />
    <div class="page-content-wrapper">

        <h3 class="page-title">Thành viên đã đăng ký</h3>

        <div class="row">
            <div class="col-md-12">
                <%= ShowMessage()%>

                <div class="portlet portlet">
                    <div class="portlet-title">
                        <div class="actions btn-set">
                            <%=GetTinyListCommand()%>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="dataTables_wrapper">
                            <div class="row hidden-sm hidden-col">

                                <div class="col-12 col-sm-12 col-md-12 col-lg-12 text-right pull-right">

                                    <div class="table-group-actions d-inline-block">
                                        <%= ShowDDLLang(model.LangID)%>
                                    </div>
                                </div>
                            </div>

                            <div class="table-scrollable">
                                <table class="table table-striped table-hover table-bordered dataTable">
                                    <thead>
                                        <tr>
                                            <th class="sorting text-center w1p">#</th>
                                            <th class="sorting_disabled text-center w1p">
                                                <label class="itemCheckBox itemCheckBox-sm">
                                                    <input type="checkbox" data-toggle="tooltip" data-original-title="Xóa" name="toggle" onclick="checkAll(<%= model.PageSize %>)">
                                                    <i class="check-box"></i>
                                                </label>
                                            </th>
                                            <th class="sorting text-center w10p hidden-sm hidden-col"><%= GetSortLink("Tên thành viên", "Name")%></th>
                                            <th class="sorting text-center w10p hidden-sm hidden-col"><%= GetSortLink("Email", "Email")%></th>
                                            <th class="sorting text-center w10p hidden-sm hidden-col"><%= GetSortLink("Phone", "Phone")%></th>
                                            <th class="sorting text-center w10p hidden-sm hidden-col"><%= GetSortLink("Ảnh", "File")%></th>
                                            <th class="sorting text-center w10p hidden-sm hidden-col"><%= GetSortLink("Ngày tham gia", "Published")%></th>
                                            <th class="sorting text-center w10p hidden-sm hidden-col"><%= GetSortLink("Duyệt", "Activity")%></th>

                                            <th class="sorting text-center w10p hidden-sm hidden-col"><%= GetSortLink("Shop Or User", "Activity")%></th>
                                            <th class="sorting text-center w1p"><%= GetSortLink("ID", "ID")%></th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%for (var i = 0; listItem != null && i < listItem.Count; i++)
                                            {
                                                var _user = ModUserService.Instance.GetByID_Cache(listItem[i].SellerID);
                                                if (_user == null) continue;
                                        %>
                                        <tr>
                                            <td class="text-center"><%= i + 1%></td>
                                            <td class="text-center">
                                                <%= GetCheckbox(listItem[i].ID, i)%>
                                            </td>
                                            <td>
                                                <a target="_blank" href="/{CPPath}/ModUser/Add.aspx/RecordID/<%=listItem[i].SellerID %>"><%= _user.Name%></a>
                                                <p class="smallsub hidden-sm hidden-col">(<span>Account</span>: <%= _user.LoginName%>)</p>
                                            </td>
                                            <td class="text-center hidden-sm hidden-col"><%= _user.Email %></td>
                                            <td class="text-center hidden-sm hidden-col"><%= _user.Phone %></td>
                                            <td class="text-center hidden-sm hidden-col">
                                                <%= Utils.GetMedia(_user.Avatar, 40, 40)%>
                                            </td>
                                            <td class="text-center hidden-sm hidden-col"><%= string.Format("{0:dd-MM-yyyy HH:mm}", _user.Created) %></td>
                                            <td class="text-center">
                                                <%= GetPublish(listItem[i].ID, listItem[i].Activity)%>
                                            </td>
                                            <td class="text-center hidden-sm hidden-col">
                                                <a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" data-original-title="<%=_user.IsShop?"Đã đăng ký shop" : "Thành viên thường" %>">
                                                    <span class="fa <%=_user.IsShop?"fa-check-circle publish" : "fa-dot-circle-o unpublish" %>"></span>
                                                </a>
                                            </td>
                                            <td class="text-center"><%= listItem[i].ID%></td>
                                        </tr>
                                        <%} %>
                                    </tbody>
                                </table>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 justify-content-center d-flex ">
                                    <%= GetPagination(model.PageIndex, model.PageSize, model.TotalRecord)%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>


<script type="text/javascript">

    var REDDEVILController = "FormUserMarketing";

    var REDDEVILArrVar = [
        "filter_menu", "MenuID",
        "filter_lang", "LangID",
        "limit", "PageSize"
    ];

    var REDDEVILArrVar_QS = [
        "filter_search", "SearchText"
    ];

    var REDDEVILArrQT = [
        "<%= model.PageIndex + 1 %>", "PageIndex",
        "<%= model.Sort %>", "Sort"
    ];

    var REDDEVILArrDefault = [
        "1", "PageIndex",
        "1", "LangID",
        "20", "PageSize"
    ];
</script>
