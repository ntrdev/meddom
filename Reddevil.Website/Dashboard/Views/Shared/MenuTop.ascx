﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.CPViewControl" %>

<% 
    var listManager = Reddevil.Lib.Web.Application.CPModules.Where(o => o.Partitioning == 1).OrderBy(o => o.Order).ToList();
    var listAdv = Reddevil.Lib.Web.Application.CPModules.Where(o => o.Partitioning == 2).OrderBy(o => o.Order).ToList();

%>

<nav class="pcoded-navbar">
    <div class="nav-list">
        <div class="pcoded-inner-navbar main-menu">
            <div class="pcoded-navigation-label">Menu tùy chọn</div>
            <ul class="pcoded-item pcoded-left-item">
                <li class="pcoded-trigger">
                    <a href="/{CPPath}/" class="waves-effect waves-dark">
                        <span class="pcoded-micon">
                            <i class="icon-home"></i>
                        </span>
                        <span class="pcoded-mtext">Trang quản trị</span>
                    </a>
                </li>

                <li class="pcoded-hasmenu ">

                    <a href="javascript:void(0)" class="waves-effect waves-dark">
                        <span class="pcoded-micon">
                            <i class="icon-social-dropbox"></i>
                        </span>
                        <span class="pcoded-mtext">Quản lý</span>
                    </a>
                    <ul class="pcoded-submenu">
                        <%for (int i = 0; i < listManager.Count; i++)
                            {
                        %>
                        <li class="<%=CPViewPage.CurrentModule.Code==listManager[i].Code?"active select-parent":"" %>">
                            <a href="/{CPPath}/<%=listManager[i].Code%>/Index.aspx" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Danh sách <%=listManager[i].Name %></span>
                            </a>
                        </li>
                        <%} %>
                    </ul>
                </li>

                <li class="pcoded-hasmenu">
                    <a href="javascript:void(0)" class="waves-effect waves-dark">
                        <span class="pcoded-micon">
                            <i class="icon-globe"></i>
                        </span>
                        <span class="pcoded-mtext">Biên tập website</span>
                    </a>
                    <ul class="pcoded-submenu">
                        <li class="<%=CPViewPage.CurrentModule.Code=="SysMenu"?"active select-parent":"" %>">
                            <a href="/{CPPath}/SysMenu/Index.aspx" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Danh mục website</span>
                            </a>
                        </li>
                        <li class="<%=CPViewPage.CurrentModule.Code=="SysPage"?"active select-parent":"" %>">
                            <a href="/{CPPath}/SysPage/Index.aspx" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">{RS:MenuTop_Page}</span>
                            </a>
                        </li>
                        <%--<li class="<%=CPViewPage.CurrentModule.Code=="SysTemplate"?"active select-parent":"" %>">
                            <a href="/{CPPath}/SysTemplate/Index.aspx" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">{RS:MenuTop_Template}</span>
                            </a>
                        </li>
                        <li class="<%=CPViewPage.CurrentModule.Code=="SysSite"?"active select-parent":"" %>">
                            <a href="/{CPPath}/SysSite/Index.aspx" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Trang mặc định</span>
                            </a>
                        </li>--%>
                    </ul>
                </li>

                <li class="pcoded-hasmenu">
                    <a href="javascript:void(0)" class="waves-effect waves-dark">
                        <span class="pcoded-micon">
                            <i class="icon-rocket"></i>
                        </span>
                        <span class="pcoded-mtext">Logo & Thông tin liên hệ</span>
                    </a>
                    <ul class="pcoded-submenu">

                        <%for (int i = 0; i < listAdv.Count; i++)
                            {

                        %>
                        <li class="<%=CPViewPage.CurrentModule.Code==listAdv[i].Code?"active select-parent":"" %>">
                            <a href="/{CPPath}/<%=listAdv[i].Code%>/Index.aspx" class="waves-effect waves-dark">
                                <span class="pcoded-mtext"><%=listAdv[i].Name %></span>
                            </a>
                        </li>
                        <%} %>
                        <li class="<%=CPViewPage.CurrentModule.Code=="ModResource"?"active select-parent ":"" %>">
                            <a href="/{CPPath}/ModResource/Index.aspx" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Thông tin liên hệ</span>
                            </a>
                        </li>
                    </ul>
                </li>



                <li class="pcoded-hasmenu">
                    <a href="javascript:void(0)" class="waves-effect waves-dark">
                        <span class="pcoded-micon">
                            <i class="icon-people"></i>
                        </span>
                        <span class="pcoded-mtext">Tài khoản cá nhân</span>
                    </a>
                    <ul class="pcoded-submenu">

                        <li class="<%=CPViewPage.CurrentModule.Code=="UserChangeInfo"?"active select-parent":"" %>">
                            <a href="/{CPPath}/UserChangeInfo.aspx" class="waves-effect waves-dark">
                                <span class="pcoded-micon">
                                    <i class="icon-people"></i>
                                </span>
                                <span class="pcoded-mtext">Thông tin tài khoản</span>
                            </a>
                        </li>
                        <li class="<%=CPViewPage.CurrentModule.Code=="UserChangePass"?"active select-parent":"" %>">
                            <a href="/{CPPath}/UserChangePass.aspx" class="waves-effect waves-dark">
                                <span class="pcoded-micon">
                                    <i class="icon-people"></i>
                                </span>
                                <span class="pcoded-mtext">Đổi mật khẩu</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="pcoded-hasmenu">
                    <a href="javascript:void(0)" class="waves-effect waves-dark">
                        <span class="pcoded-micon">
                            <i class="icon-settings"></i>
                        </span>
                        <span class="pcoded-mtext">Cài đặt hệ thống</span>
                    </a>
                    <ul class="pcoded-submenu">
                        <li class="<%=CPViewPage.CurrentModule.Code=="SysRole"?"active select-parent":"" %>">
                            <a href="/{CPPath}/SysRole/Index.aspx" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Phân quyền truy cập</span>
                            </a>
                        </li>
                        <li class="<%=CPViewPage.CurrentModule.Code=="SysUser"?"active select-parent":"" %>">
                            <a href="/{CPPath}/SysUser/Index.aspx" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Quản lý tài khoản quản trị</span>
                            </a>
                        </li>
                        <li class="<%=CPViewPage.CurrentModule.Code=="SysUserLog"?"active select-parent":"" %>">
                            <a href="/{CPPath}/SysUserLog/Index.aspx" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Lịch sử đăng nhập</span>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>

</nav>
