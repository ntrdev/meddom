﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.CPViewControl" %>

<%
    var model = ViewBag.Model as ModProductModel;
    var item = ViewBag.Data as ModProductEntity;

    var listFile = item.GetFile();
    //var listProduct = item.GetComboProduct();
    //var listGift = item.GetGift();
%>

<form id="vswForm" name="vswForm" method="post">
    <input type="hidden" id="_vsw_action" name="_vsw_action" />
    <input type="hidden" id="RecordID" value="<%=model.RecordID %>" />

    <%--<input type="hidden" id="ComboID" value="<%=model.RecordID %>" />--%>

    <div class="page-content-wrapper">
        <h3 class="page-title">Sản phẩm <small><%= model.RecordID > 0 ? "Chỉnh sửa": "Thêm mới"%></small></h3>
        <div class="page-bar justify-content-between">
            <ul class="breadcrumb">
                <li class="breadcrumb-item">
                    <i class="fa fa-home"></i>
                    <a href="/{CPPath}/">Home</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="/{CPPath}/<%=CPViewPage.CurrentModule.Code%>/Index.aspx">Sản phẩm</a>
                </li>
            </ul>
            <div class="page-toolbar">
                <div class="btn-group">
                    <a href="/" class="btn green" target="_blank"><i class="icon-screen-desktop"></i>Xem Website</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <%= ShowMessage()%>

                <div class="form-horizontal form-row-seperated">
                    <div class="portlet">
                        <div class="portlet-title">
                            <div class="caption"></div>
                            <div class="actions btn-set">
                                <%= GetDefaultAddCommand()%>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-8">
                                    <div class="portlet box blue-steel">
                                        <div class="portlet-title">
                                            <div class="caption">Thông tin chung</div>
                                        </div>
                                        <div class="portlet-body">
                                            <div class="form-body">
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-right">Tên sản phẩm:</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control title" name="Name" id="Name" value="<%=item.Name %>" />
                                                        <span class="help-block text-primary">Ký tự đối ta: 200</span>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-right"><a target="_blank" href="<%=CoreMr.Reddevil.Web.HttpRequest.Domain +item.Url%><%=Setting.Sys_PageExt %>" style="color:blue">XEM TRÊN WEB:</a></label>
                                                    <div class="col-md-9">
                                                         <input type="text" class="form-control" disabled="disabled" name="Url" value="<%=item.Url %>" placeholder="Nếu không nhập sẽ tự sinh theo Tiêu đề" />
                                                       
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-right">Mã sản phẩm:</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control title" name="Sku" id="Sku" value="<%=item.Sku %>" />
                                                        <span class="help-block text-primary">Ký tự đối ta: 200</span>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-right">Giá bán:</label>
                                                    <div class="col-md-9">
                                                        <input type="number" class="form-control price" name="Price" value="<%=item.Price %>" />
                                                        <span class="help-block text-primary"><%=Utils.NumberToWord(item.Price.ToString())%></span>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-right">Giá gốc:</label>
                                                    <div class="col-md-9">
                                                        <input type="number" class="form-control price" name="PriceCost" value="<%=item.PriceCost %>" />
                                                        <span class="help-block text-primary"><%=Utils.NumberToWord(item.PriceCost.ToString())%></span>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-right">Cho phép thương lượng:</label>
                                                    <div class="col-md-9">
                                                        <div class="radio-list">
                                                            <label class="radioPure radio-inline">
                                                                <input type="radio" name="ActiveNegotiate" <%= item.ActiveNegotiate ? "checked": "" %> value="1" />
                                                                <span class="outer"><span class="inner"></span></span><i>Có</i>
                                                            </label>
                                                            <label class="radioPure radio-inline">
                                                                <input type="radio" name="ActiveNegotiate" <%= !item.ActiveNegotiate ? "checked": "" %> value="0" />
                                                                <span class="outer"><span class="inner"></span></span><i>Không</i>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                               
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-right">Giá min:</label>
                                                    <div class="col-md-9">
                                                        <input type="number" class="form-control price" name="PriceMin" value="<%=item.PriceMin %>" />
                                                        <span class="help-block text-primary"><%=Utils.NumberToWord(item.PriceMin.ToString())%></span>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-right">Giá max:</label>
                                                    <div class="col-md-9">
                                                        <input type="number" class="form-control price" name="PriceMax" value="<%=item.PriceMax %>" />
                                                        <span class="help-block text-primary"><%=Utils.NumberToWord(item.PriceMax.ToString())%></span>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-right">Chuyên mục:</label>
                                                    <div class="col-md-9">
                                                        <select class="form-control chosen" name="MenuID" id="MenuID" onchange="GetProperties(this.value)">
                                                            <%= Utils.ShowDdlMenuByType("Product", model.LangID, item.MenuID)%>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-right">Thuộc tính lọc:</label>
                                                    <div class="col-md-9" id="list-property"></div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-12 col-form-label text-right">Nội dung</label>
                                                    <div class="col-md-12">
                                                        <textarea class="form-control ckeditor" name="Content" id="Content" rows="" cols=""><%=item.Content %></textarea>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-4">

                                    <div class="portlet box blue-steel">
                                        <div class="portlet-title">
                                            <div class="caption">HÌNH ẢNH</div>
                                        </div>
                                        <div class="portlet-body">
                                            <div class="form-body">
                                                <div class="form-group">
                                                    <%if (!string.IsNullOrEmpty(item.FileAvatar))
                                                        { %>
                                                    <p class="preview "><%= Utils.GetMedia(item.FileAvatar, 80, 80)%></p>
                                                    <%}
                                                        else
                                                        { %>
                                                    <p class="preview">
                                                        <img src="" width="80" height="80" />
                                                    </p>
                                                    <%} %>

                                                    <label class="portlet-title-sub">Hình minh họa:</label>
                                                    <div class="form-inline">
                                                        <input type="text" class="form-control" name="FileAvatar" id="FileAvatar" value="<%=item.FileAvatar %>" />
                                                        <button type="button" class="btn btn-primary" onclick="ShowFileForm('FileAvatar'); return false">Chọn ảnh</button>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="portlet-title-sub">Ảnh phụ:</label>
                                                    <ul class="cmd-custom" id="pos_Files">
                                                        <% for (var i = 0; listFile != null && i < listFile.Count; i++)
                                                            {
                                                        %>
                                                        <li>
                                                            <img src="<%=Utils.DefaultImage(listFile[i].File) %>" class="small" />
                                                            <a href="javascript:void(0)" onclick="image_delete('<%=i %>')" data-toggle="tooltip" data-placement="bottom" data-original-title="Xóa"><i class="fa fa-ban"></i></a>

                                                        </li>
                                                        <%} %>
                                                    </ul>
                                                    <div class="form-inline">
                                                        <textarea class="form-control" style="display: none;" name="Files" id="Files"><%=item.Files%></textarea>
                                                        <button type="button" class="btn btn-primary" onclick="ShowFilesForm('Files'); return false">Chọn ảnh</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="portlet box blue-steel">
                                        <div class="portlet-title">
                                            <div class="caption">THUỘC TÍNH</div>
                                        </div>
                                        <div class="portlet-body">
                                            <div class="form-body">
                                                <div class="form-group">
                                                    <label class="portlet-title-sub">Vị trí</label>
                                                    <div class="checkbox-list">
                                                        <%= Utils.ShowCheckBoxByConfigkey("Mod.ProductState", "ArrState", item.State)%>
                                                    </div>
                                                </div>

                                                <%if (CPViewPage.UserPermissions.Approve)
                                                    {%>
                                                <div class="form-group">
                                                    <label class="portlet-title-sub">Duyệt</label>
                                                    <div class="radio-list">
                                                        <label class="radioPure radio-inline">
                                                            <input type="radio" name="Activity" <%= item.Activity ? "checked": "" %> value="1" />
                                                            <span class="outer"><span class="inner"></span></span><i>Có</i>
                                                        </label>
                                                        <label class="radioPure radio-inline">
                                                            <input type="radio" name="Activity" <%= !item.Activity ? "checked": "" %> value="0" />
                                                            <span class="outer"><span class="inner"></span></span><i>Không</i>
                                                        </label>
                                                    </div>
                                                </div>
                                                <%} %>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="portlet box blue-steel">
                                        <div class="portlet-title">
                                            <div class="caption">Thông tin thêm</div>
                                        </div>
                                        <div class="portlet-body">
                                            <div class="form-body">
                                                <div class="form-group ">
                                                    <label class="col-form-label">Link nguồn:</label>
                                                    <input type="text" class="form-control title" name="UrlCost" id="UrlCost" disabled="disabled" value="<%=item.UrlCost %>" />
                                                    <a href="<%=item.UrlCost %>" target="_blank"><span class="help-block text-primary">Xem link</span></a>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-form-label">Tên shop:</label>
                                                    <input type="text" class="form-control title" disabled="disabled" value="<%=item.GetShop()!=null?item.GetShop().Name:"" %>" />
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-form-label">Chiết khấu bán hàng:</label>
                                                    <input type="text" class="form-control title" disabled="disabled" id="ChietKhau" name="ChietKhau" value="<%=item.ChietKhau<0?0:item.ChietKhau %>" />
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-form-label">Ghi chú (Admin):</label>
                                                    <input type="text" class="form-control" disabled="disabled" name="Note" id="Note" value="<%=item.Note %>" />
                                                </div>
                                                <div class="form-group">
                                                    <button type="button" <%if (!CPViewPage.UserPermissions.Full)
                                                        {%>
                                                        disabled="disabled" <%} %>class="btn blue toggle-password" data-toggle="tooltip" data-placement="bottom" data-original-title="Edit">
                                                        <i class="fa fa-pencil"></i>Edit</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="portlet box blue-steel">
                                        <div class="portlet-title">
                                            <div class="caption">SEO</div>
                                        </div>
                                        <div class="portlet-body">
                                            <div class="form-body">
                                                <div class="form-group ">
                                                    <label class="col-form-label">PageTitle:</label>
                                                    <input type="text" class="form-control title" name="PageTitle" value="<%=item.PageTitle %>" />
                                                    <span class="help-block text-primary">Ký tự đối ta: 200</span>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-form-label">PageHeading (H1):</label>
                                                    <input type="text" class="form-control title" name="PageHeading" value="<%=item.PageHeading %>" />
                                                    <span class="help-block text-primary">Ký tự đối ta: 200</span>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-form-label">Description:</label>
                                                    <textarea class="form-control description" rows="5" name="PageDescription" placeholder="Nhập nội dung tóm tắt. Tối đa 400 ký tự"><%=item.PageDescription%></textarea>
                                                    <span class="help-block text-primary">Ký tự đối ta: 400</span>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-form-label">Keywords:</label>
                                                    <input type="text" class="form-control" name="PageKeywords" value="<%=item.PageKeywords %>" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <link rel="stylesheet" href="/{CPPath}/Content/utils/chosen/chosen.css" type="text/css" media="all" />
    <script type="text/javascript" src="/{CPPath}/Content/utils/chosen/chosen.js"></script>
    <script type="text/javascript">
        //setTimeout(function () { vsw_exec_cmd('[autosave][<%=model.RecordID%>]') }, 30000);
        window.document.title = '<%=model.RecordID>0? "Cập nhật sản phẩm " +item.Name +" | trình quản trị ":"Thêm mới sản phẩm"%>';

        $('.chosen').chosen({
            search_contains: true,
            no_results_text: 'Không tìm thấy kết quả phù hợp'
        });

        $(".toggle-password").click(function () {

            var input1 = $('#Note');
            var input2 = $('#ChietKhau');
            var input3 = $('#UrlCost');


            if (input1.attr("disabled") == "disabled") {
                input1.removeAttr('disabled');
            } else {
                input1.attr("disabled", "disabled");
            }
            if (input2.attr("disabled") == "disabled") {
                input2.removeAttr('disabled');
            } else {
                input2.attr("disabled", "disabled");
            }
            if (input3.attr("disabled") == "disabled") {
                input3.removeAttr('disabled');
            } else {
                input3.attr("disabled", "disabled");
            }
        });

        //hinh anh
        var _ArrFiles = new Array();

        <% for (var i = 0; listFile != null && i < listFile.Count; i++)
        {
            if (string.IsNullOrEmpty(listFile[i].File)) continue;
        %>
        _ArrFiles.push('<%=listFile[i].File%>');
        <%} %>

        function refreshPageFiles(fileUrl, file, arg) {
            if (arg.length < 1) return;

            var result = '';
            if (name_control.indexOf('Files') > -1) {
                for (var i = 0; i < arg.length; i++) {
                    if (arg[i].url == '') continue;

                    result += arg[i].url + (i < arg.length - 1 ? '\n' : '');
                    _ArrFiles.push(arg[i].url.replace('\n', '').trim());

                }
                jQuery('#' + name_control).val(result);
            }
            else {
                jQuery('#' + name_control).val(arg[0].url);
                if (jQuery('#img_view')) jQuery('#img_view').attr('src', arg[0].url);
            }

            if (name_control.indexOf('Files') > -1) {
                image_display();
            }
        }

        function image_display() {
            var s = '';
            var v = '';


            for (var i = 0; i < _ArrFiles.length; i++) {
                v += (v === '' ? "" : '\n') + _ArrFiles[i];
                s += '  <li>\
                                <img src="' + _ArrFiles[i] + '" class="small" />\
                                <a href="javascript:void(0)" onclick="image_delete(' + i + ')" data-toggle="tooltip" data-placement="bottom" data-original-title="Xóa"><i class="fa fa-ban"></i></a>\
                            </li>';
            }


            jQuery('#pos_Files').html(s);
            jQuery('#Files').val(v);
        }


        function image_delete(index) {
            if (confirm('Bạn chắc muốn xóa không ?')) {

                _ArrFiles.splice(index, 1);
                image_display();

            }
        }

        //thuoc tinh
        function GetProperties(MenuID) {
            var ranNum = Math.floor(Math.random() * 999999);
            var dataString = "MenuID=" + MenuID + "&LangID=<%=model.LangID %>&ProductID=<%=item.ID%>&rnd=" + ranNum;

            $.ajax({
                url: "/{CPPath}/Ajax/GetProperties.aspx",
                type: "get",
                data: dataString,
                dataType: 'json',
                success: function (data) {
                    var content = data.Html;
                    $("#list-property").html(content);
                },
                error: function (status) { }
            });
        }

        if (<%=item.MenuID%> > 0) GetProperties('<%=item.MenuID%>');
        else GetProperties($('#MenuID').val());

    </script>
</form>
