﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.CPViewControl" %>

<%
    var model = ViewBag.Model as ModProductModel;
    var item = ViewBag.Data as ModProductEntity;
    var listFile = item.GetFile ?? new List<ModDynamicFileEntity>();
%>

<form id="reddevilForm" name="reddevilForm" method="post">
    <input type="hidden" id="_reddevil_action" name="_reddevil_action" />
    <input type="hidden" id="RecordID" value="<%=model.RecordID %>" />


    <div class="page-content">
        <div class="breadcrumbs">
            <h1><%= model.RecordID > 0 ? "Cập nhật sản phẩm" : "Thêm mới sản phẩm"%></h1>
            <ol class="breadcrumb">
                <li>
                    <a href="/{CPPath}/"><i class="fa fa-home"></i>Home</a>
                </li>
                <li class="active"><a href="/{CPPath}/<%=CPViewPage.CurrentModule.Code%>/Index.aspx">Sản phẩm</a></li>
            </ol>
        </div>
        <div class="row">
            <div class="col-md-12">
                <%= ShowMessage()%>
                <div class="control_heading">
                    <div class="btn-group btn-group-circle">
                        <%=GetDefaultAddCommand() %>
                    </div>
                </div>
            </div>
        </div>
        <div class="row pagepost">

            <div class="col-sm-9">

                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-green-sharp">
                            <i class="icon-speech font-green-sharp"></i>
                            <span class="caption-subject bold uppercase">Thông tin chung</span>
                            <span class="caption-helper">thông tin sản phẩm</span>
                        </div>
                        <div class="actions">
                            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"></a>
                        </div>
                    </div>
                    <div class="portlet-body">

                        <div class="form-body">
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input ">
                                        <input type="text" class="form-control" id="Name" name="Name" value="<%=item.Name %>" placeholder="nhập vào đây ...">
                                        <label for="form_control_1">Tên sản phẩm:</label>
                                        <span class="help-block">Ký tự tối đa 200</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input ">
                                        <input type="text" class="form-control" id="Url" value="<%=item.Url %>" placeholder="Nếu không nhập sẽ tự sinh theo Tiêu đề">
                                        <label for="form_control_1">URL trình duyệt:</label>
                                        <span class="help-block">ví dụ: san-pham-tai-nghe-cao-cap</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-md-line-input ">
                                        <input type="text" class="form-control" id="Sku" name="Sku" value="<%=item.Sku %>" placeholder="nhập vào đây ...">
                                        <label for="form_control_1">Mã sản phẩm:</label>
                                        <span class="help-block">Ví dụ: SKU0001</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-md-line-input ">
                                        <input type="text" class="form-control" id="Price" name="Price" value="<%=item.Price %>" placeholder="nhập giá bán...">
                                        <label for="form_control_1">Giá bán:</label>
                                        <span class="help-block"><%=item.Price<1?"ví dụ: nhập 100000 = 100.000đ":Utils.NumberToWord(item.Price.ToString()) %> </span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-md-line-input ">
                                        <input type="text" class="form-control" id="PriceCost" name="PriceCost" value="<%=item.PriceCost%>" placeholder="nhập giá gốc (giá gạch chân)...">
                                        <label for="form_control_1">Giá gốc:</label>
                                        <span class="help-block"><%=item.PriceCost<1?"ví dụ: nhập 100000 = 100.000đ":Utils.NumberToWord(item.PriceCost.ToString()) %></span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input ">
                                        <input type="text" class="form-control" id="Brand" name="Brand" value="<%=item.Brand%>" placeholder="Copy dán link của dự án vào đây...">
                                        <label for="form_control_1">Link dự án:</label>
                                        <span class="help-block">Link dự án đã hoàn thành vd: http://webtinhte.vn</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input ">

                                        <select class="form-control chosen" name="MenuID" id="MenuID" onchange="GetProperties(this.value)">
                                            <%= Utils.ShowDdlMenuByType("Product", model.LangID, item.MenuID)%>
                                        </select>
                                        <label for="multiple" class="control-label">Chọn chuyên mục:</label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group form-md-line-input ">
                                        <label for="form_control_1">Thuộc tính lọc:</label>
                                        <div class="" id="list-property"></div>

                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group form-md-line-input">
                                        <textarea class="form-control" rows="3" name="Summary" placeholder="Mô tả tóm tắt sản phẩm"><%=item.Summary %></textarea>
                                        <label for="form_control_1">Mô tả tóm tắt:</label>
                                        <span class="help-block">Mô tả ngắn về sản phẩm</span>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group form-md-line-input ">
                                        <textarea class="form-control ckeditor" name="Content" placeholder=""><%=item.Content %></textarea>
                                        <label for="form_control_1">Nội dung:</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-green-sharp">
                            <i class="icon-speech font-green-sharp"></i>
                            <a class="titleCollap collapsed" data-toggle="collapse" href="#ctr-advance" aria-expanded="false"><span class="caption-subject bold uppercase">Hình ảnh sản phẩm</span></a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="ItemControl">

                                <div id="ctr-advance" class="panel-collapse collapse" style="height: 0px;" aria-expanded="false">
                                    <div class="panel-body">
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input class="input-file" type="button" id="imgInp" onclick="ShowFileForm('FileAvatar'); return false">
                                                <label tabindex="0" for="my-file" class="input-file-trigger text-center">
                                                    <i class="icon-cloud-upload"></i>&nbsp;Chọn ảnh đại diện</label>
                                                <img id="show_img_upload" src="<%=Utils.DefaultImage(item.FileAvatar) %>" />
                                                <input type="hidden" class="form-control" name="FileAvatar" id="FileAvatar" value="<%=item.FileAvatar %>" />
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input class="input-file" type="button" onclick="ShowFile(); return false">
                                                <input class="input-file" type="hidden" hidden id="dynamic_id" value="1">
                                                <label tabindex="0" for="my-file" class="input-file-trigger text-center">
                                                    <i class="icon-cloud-upload"></i>&nbsp;Chọn thêm ảnh</label>

                                                <ul class="cmd-custom" id="list-file">
                                                    <%for (int i = 0; i < listFile.Count; i++)
                                                        {%>
                                                    <li>
                                                        <img src="<%=Utils.DefaultImage(listFile[i].File) %>" />
                                                        <a href="javascript:void(0)" onclick="deleteFile('<%=listFile[i].File %>','1')" data-toggle="tooltip" data-placement="bottom" data-original-title="Xóa"><i class="fa fa-ban"></i></a>
                                                        <a href="javascript:void(0)" onclick="upFile('<%=listFile[i].File %>','1')" data-toggle="tooltip" data-placement="bottom" data-original-title="Chuyển lên trên"><i class="fa fa-arrow-up"></i></a>
                                                        <a href="javascript:void(0)" onclick="downFile('<%=listFile[i].File %>','1')" data-toggle="tooltip" data-placement="bottom" data-original-title="Chuyển xuống dưới"><i class="fa fa-arrow-down"></i></a>
                                                    </li>
                                                    <%} %>
                                                </ul>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-green-sharp">
                            <i class="icon-speech font-green-sharp"></i>
                            <span class="caption-subject bold uppercase">Thuộc tính sản phẩm</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group form-md-checkboxes">
                                    <label>Vị trí thuộc tính sản phẩm</label>
                                    <div class="md-checkbox-inline">
                                        <%= Utils.ShowCheckBoxByConfigkey("Mod.ProductState", "ArrState", item.State)%>
                                    </div>
                                </div>
                            </div>
                            <%if (CPViewPage.UserPermissions.Approve)
                                {%>
                            <div class="col-md-12">
                                <label>Trạng thái</label>
                                <div class="md-radio-inline">
                                    <div class="md-radio">
                                        <input type="radio" id="activity1" name="Activity" value="1" class="md-radiobtn" <%=item.Activity?"checked":"" %>>
                                        <label for="activity1">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span>Duyệt
                                        </label>
                                    </div>
                                    <div class="md-radio has-error">
                                        <input type="radio" id="activity2" name="Activity" value="0" class="md-radiobtn" <%=!item.Activity?"checked":"" %>>
                                        <label for="activity2">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span>Không duyệt
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <%} %>
                        </div>
                    </div>
                </div>
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-green-sharp">
                            <i class="icon-speech font-green-sharp"></i>
                            <span class="caption-subject bold uppercase">SEO Customer</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group form-md-line-input ">
                                    <input type="text" class="form-control" id="PageTitle" name="PageTitle" value="<%=item.PageTitle %>" placeholder="nhập vào đây ...">
                                    <label for="form_control_1">PageTitle:</label>
                                    <span class="help-block">Ký tự tối đa: 200</span>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group form-md-line-input">
                                    <textarea class="form-control" rows="3" name="PageDescription" placeholder="Nhập nội dung tóm tắt. Tối đa 300 ký tự"><%=item.PageDescription%></textarea>
                                    <label for="form_control_1">Description:</label>
                                    <span class="help-block">Ký tự tối đa: 300</span>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group form-md-line-input ">
                                    <input type="text" class="form-control" name="PageKeywords" value="<%=item.PageKeywords %>" />
                                    <label for="form_control_1">Keywords:</label>
                                    <span class="help-block">Ví dụ: Sản phẩm axx, giá rẻ...</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <link rel="stylesheet" href="/{CPPath}/interface/utils/chosen/chosen.css" type="text/css" media="all" />
    <script type="text/javascript" src="/{CPPath}/interface/utils/chosen/chosen.js"></script>
    <script type="text/javascript">
        //setTimeout(function () { reddevil_exec_cmd('[autosave][<%=model.RecordID%>]') }, 30000);
        window.document.title = '<%=model.RecordID>0? "Cập nhật sản phẩm " +item.Name +" | trình quản trị ":"Thêm mới sản phẩm"%>';

        $('.chosen').chosen({
            search_contains: true,
            no_results_text: 'Không tìm thấy kết quả phù hợp'
        });


        function refreshPageFiles(fileUrl, file, arg) {
            if (arg.length < 1) return;

            jQuery('#' + name_control).val(arg[0].url);
            if (jQuery('#show_img_upload')) jQuery('#show_img_upload').attr('src', arg[0].url);


        }

        //thuoc tinh
        function GetProperties(MenuID) {
            var ranNum = Math.floor(Math.random() * 999999);
            var dataString = "MenuID=" + MenuID + "&LangID=<%=model.LangID %>&ProductID=<%=item.ID%>&rnd=" + ranNum;

            $.ajax({
                url: "/{CPPath}/Ajax/GetProperties.aspx",
                type: "get",
                data: dataString,
                dataType: 'json',
                success: function (data) {
                    var content = data.Html;
                    $("#list-property").html(content);
                },
                error: function (status) { }
            });
        }

        if (<%=item.MenuID%> > 0) GetProperties('<%=item.MenuID%>');
        else GetProperties($('#MenuID').val());

    </script>
</form>
