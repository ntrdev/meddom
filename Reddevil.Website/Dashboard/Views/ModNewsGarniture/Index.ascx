﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.CPViewControl" %>

<%
    var model = ViewBag.Model as ModNewsGarnitureModel;
    var listItem = ViewBag.Data as List<ModNewsGarnitureEntity>;
%>

<form id="reddevilForm" name="reddevilForm" method="post">

    <input type="hidden" id="_reddevil_action" name="_reddevil_action" />
    <input type="hidden" id="boxchecked" name="boxchecked" value="0" />

    <div class="pcoded-content">
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8 d-none d-lg-block">
                    <div class="page-header-title">
                        <i class="fa fa-cubes"></i>
                        <div class="d-inline">
                            <h5>Biên tập Tin trưng bày</h5>
                            <span>Danh sách trưng bày</span>
                        </div>
                    </div>
                </div>

            </div>
        </div>


        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="page-header-breadcrumb">
                                    <ul class=" breadcrumb breadcrumb-title">
                                        <li class="breadcrumb-item">
                                            <a href="/{CPPath}/">
                                                <i class="icon-home"></i>
                                            </a>

                                        </li>
                                        <li class="breadcrumb-item">
                                            <i class="fa fa-chevron-right"></i>
                                            <a href="/{CPPath}/<%=CPViewPage.CurrentModule.Code%>/Index.aspx">Tin Trưng bày</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="card">
                                    <%= ShowMessage()%>
                                </div>
                                <div class="card">
                                    <div class="card-header header-control">
                                        <h5>Danh sách Tin</h5>
                                        <div class="controler-header">
                                            <%=GetDefaultListCommand() %>
                                        </div>
                                    </div>
                                    <div class="card-header header-control">
                                        <div class="tools col-md-12">
                                            <div class="dataTables_search col-md-6 col-xs-12">
                                                <input type="text" class="form-control input-inline input-sm" id="filter_search" style="float: left; width: 100%; padding-top: 1px; position: relative; background: #fff; height: 32px; text-indent: 10px; font-size: 14px; border-radius: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px;"
                                                    value="<%= model.SearchText %>" placeholder="Nhập từ khóa cần tìm" onchange="REDDEVILRedirect();return false;" />
                                                <button type="submit" class="btntop" onclick="REDDEVILRedirect();return false;" style="float: right; width: 40px; height: 32px; border: 0; cursor: pointer; background: none; position: absolute; right: 15px;">
                                                    <i class="fa fa-search"></i>
                                                </button>
                                            </div>
                                            <div class="table-group-actions d-inline-block col-md-4 col-xs-12">
                                                <select id="filter_menu" class="form-control input-inline input-sm chosen_menu" onchange="REDDEVILRedirect()" size="1">
                                                    <option value="0">Lọc theo chuyên mục</option>
                                                    <%= Utils.ShowDdlMenuByType("NewsGarniture", model.LangID, model.MenuID)%>
                                                </select>

                                            </div>
                                            <div class="table-group-actions d-inline-block col-md-4 col-xs-12">
                                                <select id="filter_status" class="form-control input-inline input-sm chosen_menu" onchange="REDDEVILRedirect()" size="1">
                                                    <option value="">Lọc theo trạng thái</option>
                                                    <option value="0" <%=model.Status==0?"selected=\"selected\"":"" %>>Đã duyệt</option>
                                                    <option value="1" <%=model.Status==1?"selected=\"selected\"":"" %>>Đang soạn</option>
                                                    <option value="2" <%=model.Status==2?"selected=\"selected\"":"" %>>Chờ duyệt</option>
                                                    <option value="3" <%=model.Status==3?"selected=\"selected\"":"" %>>Trả về</option>
                                                </select>

                                            </div>

                                            <%--                                            <div class="table-group-actions d-inline-block col-md-2 col-xs-12">
                                                <%= ShowDDLLang(model.LangID)%>
                                            </div>--%>
                                        </div>
                                    </div>
                                    <div class="card-block">
                                        <div class="dt-responsive table-responsive">
                                            <table id="" class="table_basic table table-hover m-b-0 "
                                                style="width: 100%">
                                                <thead>
                                                    <tr>
                                                        <th style="text-align: center;">
                                                            <div class="border-checkbox-section">
                                                                <div class="md-checkbox border-checkbox-group border-checkbox-group-info" data-toggle="tooltip" data-placement="bottom" data-original-title="Click chọn tất cả">
                                                                    <input type="checkbox" id="checks" name="toggle" onclick="checkAll('<%=model.PageSize%>')" class="md-check border-checkbox">
                                                                    <label for="checks" class="border-checkbox-label"></label>
                                                                </div>
                                                            </div>
                                                        </th>
                                                        <th><%= GetSortLink("Tiêu đề ", "Name")%></th>
                                                        <th>Ảnh đại diện</th>
                                                        <th>QR CODE</th>
                                                        <th><%= GetSortLink("Danh mục", "MenuID")%></th>
                                                        <th class="text-center">Trạng thái</th>
                                                        <th class="text-center">Duyệt/Chờ Duyệt</th>
                                                        <th class="text-center">Người đăng</th>
                                                        <th class="text-center">Người duyệt</th>
                                                        <th style="width: 110px">Sắp xếp vị trí</th>
                                                        <th class="text-center">Tùy chọn</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <%for (var i = 0; listItem != null && i < listItem.Count; i++)
                                                        { %>
                                                    <tr>

                                                        <td class="text-center">
                                                            <%= GetCheckbox(listItem[i].ID, i)%>
                                                        </td>

                                                        <td>
                                                            <a style="color: orangered; font-weight: 700" href="javascript:REDDEVILRedirect('Add', <%= listItem[i].ID %>)"><%= listItem[i].Name%></a>
                                                            <p class="smallsub hidden-sm hidden-col">(<a target="_blank" href="<%=CoreMr.Reddevil.Web.HttpRequest.Domain %>/<%= listItem[i].Url %>"><span>Xem trên web</span></a>)</p>
                                                        </td>
                                                        <td class="text-center hidden-sm hidden-col">
                                                            <%= Utils.GetMedia(listItem[i].File, 40, 40)%>
                                                        </td>
                                                          <td class="text-center hidden-sm hidden-col">
                                                          <img src="<%=Utils.DefaultImage(listItem[i].QrCode) %>" width="50", height="50" />
                                                        </td>
                                                        <td class="text-center hidden-sm hidden-col"><%= GetName(listItem[i].GetMenu()) %></td>
                                                        <td class="text-center"><%=listItem[i].Condition==0?"Đã duyệt":(listItem[i].Condition==1?"Đang soạn":listItem[i].Condition==2?"Chờ duyệt":"Trả về") %></td>
                                                        <td class="text-center"><%= GetHideOrShow2(listItem[i].ID, listItem[i].Activity)%></td>
                                                        <td class="text-center hidden-sm hidden-col"><%= listItem[i].AdminEdit %></td>
                                                        <td class="text-center hidden-sm hidden-col"><%= listItem[i].AdminApprove %></td>
                                                        <td class="text-center"><%= GetOrder(listItem[i].ID, listItem[i].Order)%></td>
                                                        <td class="text-center">
                                                            <div class="control-button">
                                                                <button data-toggle="tooltip" data-original-title="Chỉnh sửa" onclick="REDDEVILRedirect('Add', <%= listItem[i].ID %>)" type="button" class="btn waves-effect waves-light btn-primary btn-icon"><i class="icon-pencil"></i></button>
                                                                <button class="btn waves-effect waves-light btn-danger btn-icon" data-toggle="tooltip" data-original-title="Lượt xem" type="button"><i class="fa fa-eye"></i><%=listItem[i].View %></button>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <%} %>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="row center">
                                            <div class="col-md-12 col-sm-12 justify-content-center d-flex ">
                                                <%= GetPagination(model.PageIndex, model.PageSize, model.TotalRecord)%>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <link rel="stylesheet" href="/{CPPath}/interface/utils/chosen/chosen.css" type="text/css" media="all" />
    <script type="text/javascript" src="/{CPPath}/interface/utils/chosen/chosen.js"></script>

    <script type="text/javascript">

        var REDDEVILController = "ModNewsGarniture";

        var REDDEVILArrVar = [
            "filter_menu", "MenuID",
            "filter_state", "State",
            "filter_status", "Status",
            "filter_lang", "LangID",
            "limit", "PageSize"
        ];

        var REDDEVILArrVar_QS = [
            "filter_search", "SearchText"
        ];

        var REDDEVILArrQT = [
            "<%= model.PageIndex + 1 %>", "PageIndex",
            "<%= model.Sort %>", "Sort"
        ];

        var REDDEVILArrDefault = [
            "1", "PageIndex",
            "1", "LangID",
            "20", "PageSize"
        ];

        $('.chosen_menu').chosen({

            display_selected_options: true,
            placeholder_text_multiple: 'Gõ từ khóa cần tìm',
            no_results_text: 'Không có kết quả',
            enable_split_word_search: true,
            search_contains: true,
            display_disabled_options: true,
            single_backstroke_delete: false,
            inherit_select_classes: true
        });
    </script>

</form>
