﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.CPViewControl" %>

<%
    var model = ViewBag.Model as SysMenuModel;
    var item = ViewBag.Data as WebMenuEntity;

    var listParent = Reddevil.Lib.Global.ListItem.List.GetList(WebMenuService.Instance, model.LangID);

    if (model.RecordID > 0)
    {
        //loai bo danh muc con cua danh muc hien tai
        listParent = Reddevil.Lib.Global.ListItem.List.GetListForEdit(listParent, model.RecordID);
    }

%>

<form id="reddevilForm" name="reddevilForm" method="post">

    <input type="hidden" id="_reddevil_action" name="_reddevil_action" />





    <div class="pcoded-content">
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8 d-none d-lg-block">
                    <div class="page-header-title">
                        <i class="icon-social-dropbox"></i>
                        <div class="d-inline">
                            <h5>Quản lý Danh mục</h5>
                            <span><%= model.RecordID > 0 ? "Cập nhật Chuyên mục " +item.Name: "Thêm mới Chuyên mục"%></span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class=" breadcrumb breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="/{CPPath}/">
                                    <i class="icon-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="/{CPPath}/<%=CPViewPage.CurrentModule.Code%>/Index.aspx">Danh mục</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <%= ShowMessage()%>
                                </div>
                            </div>
                            <div class="col-sm-9">
                                <div class="card content-editor">
                                    <div class="card-header header-control">
                                        <h5><%= model.RecordID > 0 ? "Cập nhật Chuyên mục " +item.Name: "Thêm mới Chuyên mục"%></h5>
                                        <div class="controler-header">
                                            <%=GetDefaultAddCommand() %>
                                        </div>
                                    </div>
                                    <div class="card-block pdt-0">
                                        <div class="tabs-scroll">
                                            <ul class="nav nav-tabs md-tabs " role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link active" data-toggle="tab"
                                                        href="#thongtinchung" role="tab"><i class="icon-info"></i>Thông tin chung</a>
                                                    <div class="slide"></div>
                                                </li>

                                            </ul>
                                        </div>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="thongtinchung"
                                                role="tabpanel">
                                                <h4 class="sub-title">Thông tin chuyên mục</h4>
                                                <div class="edit-margin-mb form-manager">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <label class="col-sm-12 col-form-label">Tên danh mục</label>
                                                                    <div class="col-sm-12">
                                                                        <input type="text" class="form-control" id="Name" name="Name" value="<%=item.Name %>" autocomplete="off" placeholder="Nhập tên chuyên mục vào đây ...">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <%if (item.ParentID == 0)
                                                            { %>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <label class="col-sm-12 col-form-label">Loại chuyên mục</label>
                                                                    <div class="col-sm-12">
                                                                        <input type="text" class="form-control" id="Type" name="Type" value="<%=item.Type %>" autocomplete="off" placeholder="News or Adv or Video">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <%} %>
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <label class="col-sm-12 col-form-label">
                                                                        Tóm tắt
                                                                    </label>
                                                                    <div class="col-sm-12">
                                                                        <div class="plugin-ckEditor">
                                                                            <textarea class="form-control" rows="3" name="Summary" placeholder="Mô tả tóm tắt"><%=item.Summary %></textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="block-controlRight">
                                    <div class="ItemControl">
                                        <a class="titleCollap" data-toggle="collapse"
                                            href="#ctr-trangthai">Trạng thái và hiển thị</a>
                                        <div id="ctr-trangthai" class="panel-collapse collapse show">
                                            <div class="panel-body">
                                                <ul class="form-block">
                                                    <%if (CPViewPage.UserPermissions.Approve)
                                                        {%>
                                                    <li>
                                                        <div>Hiển thị</div>
                                                        <div class="editorBox">
                                                            <%=GetHideOrShow(item.ID, item.Activity) %>
                                                        </div>
                                                    </li>
                                                    <%} %>

                                                    <li class="blockform">
                                                        <div>Đường dẫn URL Chuyên mục</div>
                                                        <div class="editorBox">
                                                            <input type="text" class="form-block" id="Url" name="Url" value="<%=item.Url %>" placeholder="Nếu không nhập sẽ tự sinh theo Tên danh mục">
                                                        </div>
                                                    </li>

                                                </ul>
                                            </div>

                                        </div>
                                    </div>
                                    <%if (item.ParentID > 0)
                                        { %>
                                    <div class="ItemControl">
                                        <a class="titleCollap" data-toggle="collapse" href="#ctr-advance">Ảnh đại diện</a>
                                        <div id="ctr-advance" class="panel-collapse collapse" style="height: 0px;" aria-expanded="false">
                                            <div class="panel-body">
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <input class="input-file" type="button" id="imgInp" onclick="ShowFileForm('File'); return false">
                                                        <label tabindex="0" for="my-file" class="input-file-trigger text-center">
                                                            <i class="icon-cloud-upload"></i>&nbsp;Chọn ảnh đại diện</label>
                                                        <img id="show_img_upload" src="<%=Utils.OptimalImage(item.File) %>" />
                                                        <input type="hidden" class="form-control" name="File" id="File" value="<%=item.File %>" />
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="ItemControl">
                                        <a class="titleCollap" data-toggle="collapse"
                                            href="#ctr-seo-editor">Thuộc tính cho chuyên mục</a>
                                        <div id="ctr-seo-editor" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <div class="card-block">
                                                    <div class="form-group row">
                                                        <label class="col-sm-12 col-form-label">Chuyên mục cha:</label>
                                                        <div class="col-sm-12">
                                                            <select class="form-control chosen" name="ParentID">
                                                                <option value="0">Chọn chuyên mục cha</option>
                                                                <%for (var i = 0; listParent != null && i < listParent.Count; i++)
                                                                    { %>
                                                                <option <%if (item.ParentID.ToString() == listParent[i].Value)
                                                                    {%>selected<%} %>
                                                                    value="<%= listParent[i].Value%>"><%= listParent[i].Name%></option>
                                                                <%} %>
                                                            </select>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <%} %>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</form>

<link rel="stylesheet" href="/{CPPath}/interface/utils/chosen/chosen.css" type="text/css" media="all" />
<script type="text/javascript" src="/{CPPath}/interface/utils/chosen/chosen.js"></script>

<script type="text/javascript">  $('.chosen').chosen({
        search_contains: true,
        no_results_text: 'Không tìm thấy kết quả phù hợp'
    });</script>
