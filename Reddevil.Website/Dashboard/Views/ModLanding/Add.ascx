﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.CPViewControl" %>

<% 
    var model = ViewBag.Model as ModLandingModel;
    var item = ViewBag.Data as ModLandingEntity;

    var listProduct = item.GetProduct();
    var listGift = item.GetGift();
    var listFile = item.GetFile();
%>

<form id="reddevilForm" name="reddevilForm" method="post">
    <input type="hidden" id="_reddevil_action" name="_reddevil_action" />
    <input type="hidden" id="LandID" value="<%=model.RecordID%>" />

    <div class="page-content-wrapper">
        <h3 class="page-title">Landing Page <small><%= model.RecordID > 0 ? "Chỉnh sửa": "Thêm mới"%></small></h3>
        <div class="page-bar justify-content-between">
            <ul class="breadcrumb">
                <li class="breadcrumb-item">
                    <i class="fa fa-home"></i>
                    <a href="/{CPPath}/">Home</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="/{CPPath}/<%=CPViewPage.CurrentModule.Code%>/Index.aspx">Landing Page</a>
                </li>
            </ul>
            <div class="page-toolbar">
                <div class="btn-group">
                    <a href="/" class="btn green" target="_blank"><i class="icon-screen-desktop"></i>Xem Website</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <%= ShowMessage()%>

                <div class="form-horizontal form-row-seperated">
                    <div class="portlet">
                        <div class="portlet-title">
                            <div class="caption"></div>
                            <div class="actions btn-set">
                                <%= GetDefaultAddCommand()%>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-8">
                                    <div class="portlet box blue-steel">
                                        <div class="portlet-title">
                                            <div class="caption">Thông tin chung</div>
                                        </div>
                                        <div class="portlet-body">
                                            <div class="form-body">
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-right">Tên Landing Page:</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control title" name="Name" id="Name" value="<%=item.Name %>" />
                                                        <span class="help-block text-primary">Ký tự đối ta: 200</span>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-right">URL trình duyệt:</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" name="Code" value="<%=item.Code %>" placeholder="Nếu không nhập sẽ tự sinh theo Tiêu đề" />
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-right">Giật Tít Landing Page:</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control title" name="Title" id="Title" value="<%=item.Title %>" />
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-right">Chuyên mục:</label>
                                                    <div class="col-md-9">
                                                        <select class="form-control" name="MenuID" id="MenuID">
                                                            <option value="0">Root</option>
                                                            <%= Utils.ShowDdlMenuByType("Landing", model.LangID, item.MenuID)%>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="portlet box blue-steel">
                                        <div class="portlet-title">
                                            <div class="caption">Sản phẩm</div>
                                        </div>
                                        <div class="portlet-body">
                                            <div class="form-body">
                                                <div class="form-group">
                                                    <ul class="cmd-custom" id="list-product">
                                                        <%for (int i = 0; listProduct != null && i < listProduct.Count; i++){
                                                                var product = ModProductService.Instance.GetByID(listProduct[i].ProductID);
                                                                if (product == null) continue;
                                                        %>
                                                        <li>
                                                            <%= Utils.GetMedia(product.File, 100, 100)%>
                                                            <a href="javascript:void(0)" onclick="deleteProductLanding('<%=listProduct[i].LandingID %>', '<%=listProduct[i].ProductID %>')" data-toggle="tooltip" data-placement="bottom" data-original-title="Xóa"><i class="fa fa-ban"></i></a>
                                                            <a href="javascript:void(0)" onclick="upProductLanding('<%=listProduct[i].LandingID %>', '<%=listProduct[i].ProductID %>')" data-toggle="tooltip" data-placement="bottom" data-original-title="Chuyển lên trên"><i class="fa fa-arrow-up"></i></a>
                                                            <a href="javascript:void(0)" onclick="downProductLanding('<%=listProduct[i].LandingID %>', '<%=listProduct[i].ProductID %>')" data-toggle="tooltip" data-placement="bottom" data-original-title="Chuyển xuống dưới"><i class="fa fa-arrow-down"></i></a>
                                                            <p><%=product.Name %></p>
                                                        </li>
                                                        <%} %>
                                                    </ul>
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-right">Tìm:</label>
                                                        <div class="col-md-9">
                                                            <input type="text" class="form-control title" onkeyup="getProductLanding('<%=model.RecordID %>', this.value); jQuery('#list-search').show();" placeholder="Gõ tên, mã sản phẩm để tìm kiếm" maxlength="255" />
                                                            <ul id="list-search" class="list-search"></ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-4">
                                    <div class="portlet box blue-steel">
                                        <div class="portlet-title">
                                            <div class="caption">Slide</div>
                                        </div>
                                        <div class="portlet-body">
                                            <div class="form-body">
                                                <div class="form-group">
                                                    <label class="portlet-title-sub">Danh sách:</label>
                                                    <ul class="cmd-custom" id="list-file">
                                                        <%for (int i = 0; i < listFile.Count; i++){%>
                                                        <li>
                                                            <img src="<%=listFile[i].FileLanding %>" />
                                                            <a href="javascript:void(0)" onclick="deleteFileLanding('<%=listFile[i].LandingID %>', '<%=listFile[i].FileLanding %>')" data-toggle="tooltip" data-placement="bottom" data-original-title="Xóa"><i class="fa fa-ban"></i></a>
                                                            <a href="javascript:void(0)" onclick="upFileLanding('<%=listFile[i].LandingID %>', '<%=listFile[i].FileLanding %>')" data-toggle="tooltip" data-placement="bottom" data-original-title="Chuyển lên trên"><i class="fa fa-arrow-up"></i></a>
                                                            <a href="javascript:void(0)" onclick="downFileLanding('<%=listFile[i].LandingID %>', '<%=listFile[i].FileLanding %>')" data-toggle="tooltip" data-placement="bottom" data-original-title="Chuyển xuống dưới"><i class="fa fa-arrow-down"></i></a>
                                                        </li>
                                                        <%} %>
                                                    </ul>
                                                    <div class="form-inline">
                                                        <button type="button" class="btn btn-primary" onclick="ShowFileLanding();">Chọn ảnh</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="portlet box blue-steel">
                                        <div class="portlet-title">
                                            <div class="caption">QUÀ TẶNG</div>
                                        </div>
                                        <div class="portlet-body">
                                            <div class="form-body">

                                                <div class="form-group">
                                                    <label class="portlet-title-sub">Quà tặng:</label>
                                                    <ul class="cmd-custom" id="list-gift">
                                                        <%for (int i = 0; listGift != null && i < listGift.Count; i++){
                                                                var gift = ModProductService.Instance.GetByID(listGift[i].GiftID);
                                                                if (gift == null) continue;
                                                        %>
                                                        <li>
                                                            <%= Utils.GetMedia(gift.File, 100, 100)%>
                                                            <a href="javascript:void(0)" onclick="deleteGiftLanding('<%=item.ID %>', '<%=gift.ID %>')" data-toggle="tooltip" data-placement="bottom" data-original-title="Xóa"><i class="fa fa-ban"></i></a>
                                                            <a href="javascript:void(0)" onclick="upGiftLanding('<%=listGift[i].LandingID %>', '<%=listGift[i].GiftID %>')" data-toggle="tooltip" data-placement="bottom" data-original-title="Chuyển lên trên"><i class="fa fa-arrow-up"></i></a>
                                                            <a href="javascript:void(0)" onclick="downGiftLanding('<%=listGift[i].LandingID %>', '<%=listGift[i].GiftID %>')" data-toggle="tooltip" data-placement="bottom" data-original-title="Chuyển xuống dưới"><i class="fa fa-arrow-down"></i></a>
                                                            <p><%=gift.Name %></p>
                                                        </li>
                                                        <%} %>
                                                    </ul>
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-right">Tìm:</label>
                                                        <div class="col-md-9">
                                                            <input type="text" class="form-control title" onkeyup="getGiftLanding('<%=model.RecordID %>', this.value); jQuery('#list-search-gift').show();" placeholder="Gõ tên, mã quà tặng để tìm kiếm" maxlength="255" />
                                                            <ul id="list-search-gift" class="list-search"></ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="portlet box blue-steel">
                                        <div class="portlet-title">
                                            <div class="caption">THUỘC TÍNH</div>
                                        </div>
                                        <div class="portlet-body">
                                            <div class="form-body">
                                                <div class="form-group">
                                                    <label class="portlet-title-sub">Vị trí</label>
                                                    <div class="checkbox-list">
                                                        <%= Utils.ShowCheckBoxByConfigkey("Mod.ProductState", "ArrState", item.State)%>
                                                    </div>
                                                </div>

                                                <%if (CPViewPage.UserPermissions.Approve)
                                                    {%>
                                                <div class="form-group">
                                                    <label class="portlet-title-sub">Duyệt</label>
                                                    <div class="radio-list">
                                                        <label class="radioPure radio-inline">
                                                            <input type="radio" name="Activity" <%= item.Activity ? "checked": "" %> value="1" />
                                                            <span class="outer"><span class="inner"></span></span><i>Có</i>
                                                        </label>
                                                        <label class="radioPure radio-inline">
                                                            <input type="radio" name="Activity" <%= !item.Activity ? "checked": "" %> value="0" />
                                                            <span class="outer"><span class="inner"></span></span><i>Không</i>
                                                        </label>
                                                    </div>
                                                </div>
                                                <%} %>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="portlet box blue-steel">
                                        <div class="portlet-title">
                                            <div class="caption">SEO</div>
                                        </div>
                                        <div class="portlet-body">
                                            <div class="form-body">
                                                <div class="form-group ">
                                                    <label class="col-form-label">PageTitle:</label>
                                                    <input type="text" class="form-control title" name="PageTitle" value="<%=item.PageTitle %>" />
                                                    <span class="help-block text-primary">Ký tự đối ta: 200</span>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-form-label">Description:</label>
                                                    <textarea class="form-control description" rows="5" name="PageDescription" placeholder="Nhập nội dung tóm tắt. Tối đa 400 ký tự"><%=item.PageDescription%></textarea>
                                                    <span class="help-block text-primary">Ký tự đối ta: 400</span>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-form-label">Keywords:</label>
                                                    <input type="text" class="form-control" name="PageKeywords" value="<%=item.PageKeywords %>" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
