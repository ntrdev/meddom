﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.CPViewControl" %>

<%
    var model = ViewBag.Model as ModSkinModel;
    var listItem = ViewBag.Data as List<ModSkinEntity>;
%>

<form id="vswForm" name="vswForm" method="post">

    <input type="hidden" id="_vsw_action" name="_vsw_action" />
    <input type="hidden" id="boxchecked" name="boxchecked" value="0" />

    <div id="toolbar-box">
        <div class="t">
            <div class="t">
                <div class="t"></div>
            </div>
        </div>
        <div class="m">
            <div class="toolbar-list" id="toolbar">
                <%=GetDefaultListCommand()%>
            </div>
            <div class="pagetitle icon-48-article">
                <h2>Trang phục</h2>
            </div>
            <div class="clr"></div>
        </div>
        <div class="b">
            <div class="b">
                <div class="b"></div>
            </div>
        </div>
    </div>
    <div class="clr"></div>

    <script type="text/javascript">

        var VSWController = "ModSkin";

        var VSWArrVar = [
            "limit", "PageSize"
        ];

        var VSWArrVar_QS = [
            "filter_search", "SearchText"
        ];

        var VSWArrQT = [
            "<%= model.PageIndex + 1 %>", "PageIndex",
            "<%= model.Sort %>", "Sort"
        ];

        var VSWArrDefault = [
            "1", "PageIndex",
            "20", "PageSize"
        ];
    </script>

    <%= ShowMessage()%>

    <div id="element-box">
        <div class="t">
            <div class="t">
                <div class="t"></div>
            </div>
        </div>
        <div class="m">

            <table>
                <tr>
                    <td width="100%">Lọc:
                        <input type="text" value="<%= model.SearchText %>" id="filter_search" class="text_area" onchange="VSWRedirect();return false;" />
                        <button onclick="VSWRedirect();return false;">Tìm kiếm</button>
                    </td>
                    <td nowrap="nowrap">
                        
                    </td>
                </tr>
            </table>

            <table class="adminlist" cellspacing="1">
                <thead>
                    <tr>
                        <th width="1%">#</th>
                        <th width="1%">
                            <input type="checkbox" name="toggle" value="" onclick="checkAll(<%= model.PageSize %>);" />
                        </th>
                        <th class="title">
                            <%= GetSortLink("Tên trang phục", "Name")%>
                        </th>
                        <th style="width: 40px" nowrap="nowrap">
                            <%= GetSortLink("Ảnh", "File")%>
                        </th>
                        <th width="1%" nowrap="nowrap">
                            <%= GetSortLink("Giá", "Price")%>
                        </th>
                        <th style="width: 150px" nowrap="nowrap">
                            <%= GetSortLink("Ngày mở", "DateReleased")%>
                        </th>
                        <th style="width: 150px" nowrap="nowrap">
                            <%= GetSortLink("Ngày hết hạn", "DateEnded")%>
                        </th>
                        <th style="width: 150px" nowrap="nowrap">
                            <%= GetSortLink("Ngày đăng", "Published")%>
                        </th>
                        <th width="1%" nowrap="nowrap">
                            <%= GetSortLink("VIP", "IsVip")%>
                        </th>
                        <th width="1%" nowrap="nowrap">
                            <%= GetSortLink("Duyệt", "Activity")%>
                        </th>
                        <th width="1%" nowrap="nowrap">
                            <%= GetSortLink("Sắp xếp", "Order")%>
                            <a href="javascript:vsw_exec_cmd('saveorder')" class="saveorder" title="Lưu sắp xếp"></a>
                        </th>
                        <th width="1%" nowrap="nowrap">
                            <%= GetSortLink("ID", "ID")%>
                        </th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <td colspan="15">
                            <del class="container">
                                <%= GetPagination(model.PageIndex, model.PageSize, model.TotalRecord)%>
                            </del>
                        </td>
                    </tr>
                </tfoot>
                <tbody>
                    <%for (var i = 0; listItem != null && i < listItem.Count; i++){ %>
                    <tr class="row<%= i%2 %>">
                        <td align="center">
                            <%= i + 1%>
                        </td>
                        <td align="center">
                            <%= GetCheckbox(listItem[i].ID, i)%>
                        </td>
                        <td>
                            <a href="javascript:VSWRedirect('Add', <%= listItem[i].ID %>)"><%= listItem[i].Name%></a>
                        </td>
                        <td align="center">
                            <%= Utils.GetMedia(listItem[i].File, 40, 40)%>
                        </td>
                        <td align="center">
                            <%= listItem[i].Price %>
                        </td>
                        <td align="center">
                            <%= string.Format("{0:dd-MM-yyyy HH:mm}", listItem[i].DateReleased) %>
                        </td>
                        <td align="center">
                            <%= string.Format("{0:dd-MM-yyyy HH:mm}", listItem[i].DateEnded) %>
                        </td>
                        <td align="center">
                            <%= string.Format("{0:dd-MM-yyyy HH:mm}", listItem[i].Published) %>
                        </td>
                        <td align="center">
                            <%= GetIsVip(listItem[i].ID, listItem[i].IsVip)%>
                        </td>
                        <td align="center">
                            <%= GetPublish(listItem[i].ID, listItem[i].Activity)%>
                        </td>
                        <td class="order">
                            <%= GetOrder(listItem[i].ID, listItem[i].Order)%>
                        </td>
                        <td align="center">
                            <%= listItem[i].ID%>
                        </td>
                    </tr>
                    <%} %>
                </tbody>
            </table>

            <div class="clr"></div>
        </div>
        <div class="b">
            <div class="b">
                <div class="b"></div>
            </div>
        </div>
    </div>
</form>