﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.CPViewControl" %>

<%
    var model = ViewBag.Model as ModSkinModel;
    var item = ViewBag.Data as ModSkinEntity;
%>

<form id="vswForm" name="vswForm" method="post">
    <input type="hidden" id="_vsw_action" name="_vsw_action" />

    <div id="toolbar-box">
        <div class="t">
            <div class="t">
                <div class="t"></div>
            </div>
        </div>
        <div class="m">
            <div class="toolbar-list" id="toolbar">
                <%= GetDefaultAddCommand()%>
            </div>
            <div class="pagetitle icon-48-mediamanager">
                <h2>Trang phục: <%=  model.RecordID > 0 ? "Chỉnh sửa": "Thêm mới"%></h2>
            </div>
            <div class="clr"></div>
        </div>
        <div class="b">
            <div class="b">
                <div class="b"></div>
            </div>
        </div>
    </div>
    <div class="clr"></div>

    <%= ShowMessage()%>

    <div id="element-box">
        <div class="t">
            <div class="t">
                <div class="t"></div>
            </div>
        </div>
        <div class="m">
            <div class="col width-100">
                <table class="admintable">
                    <tr>
                        <td class="key">
                            <label>Tên trang phục:</label>
                        </td>
                        <td>
                            <input class="text_input" type="text" name="Name" value="<%=item.Name %>" maxlength="255" />
                        </td>
                    </tr>
                    <tr>
                        <td class="key">
                            <label>Ảnh:</label>
                        </td>
                        <td>
                            <%= Utils.GetMedia(item.File, 100, 80)%>
                            <br />
                            <input class="text_input" type="text" name="File" id="File" value="<%=item.File %>" style="width: 80%;" maxlength="255" />
                            <input class="text_input" style="width: 17%;" type="button" onclick="ShowFileForm('File'); return false;" value="Chọn File" />
                        </td>
                    </tr>
                    <%if (CPViewPage.UserPermissions.Approve){%>
                    <tr>
                        <td class="key">
                            <label>Duyệt:</label>
                        </td>
                        <td>
                            <input name="Activity" <%= item.Activity ? "checked": "" %> type="radio" value="1" />
                            Có
                            <input name="Activity" <%= !item.Activity ? "checked": "" %> type="radio" value="0" />
                            Không
                        </td>
                    </tr>
                    <%} %>
                </table>
            </div>
            <div class="clr"></div>
        </div>
        <div class="b">
            <div class="b">
                <div class="b"></div>
            </div>
        </div>
    </div>
</form>