﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.CPViewControl" %>

<%
    var model = ViewBag.Model as ModShopModel;
    var listItem = ViewBag.Data as List<ModShopEntity>;
    var listRole = CPUserRoleService.Instance.CreateQuery().Where(o => o.RoleID == 9).ToList_Cache();
%>
<form id="reddevilForm" name="reddevilForm" method="post">

    <input type="hidden" id="_reddevil_action" name="_reddevil_action" />
    <input type="hidden" id="boxchecked" name="boxchecked" value="0" />

    <input type="hidden" id="selectp" name="selectp" value="0" />
    <input type="hidden" id="empmanger" name="empmanger" value="0" />

    <div class="page-content-wrapper">
        <h3 class="page-title">Quản lý gian hàng</h3>
        <div class="page-bar justify-content-between">
            <ul class="breadcrumb">
                <li class="breadcrumb-item">
                    <i class="fa fa-home"></i>
                    <a href="/{CPPath}/">Home</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="/{CPPath}/<%=CPViewPage.CurrentModule.Code%>/Index.aspx">Shop</a>
                </li>
            </ul>
            <div class="page-toolbar">
                <div class="btn-group">
                    <a href="/" class="btn green" target="_blank"><i class="icon-screen-desktop"></i>Xem Website</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <%= ShowMessage()%>

                <div class="portlet portlet">
                    <div class="portlet-title">
                        <div class="actions btn-set">
                            <%if (CPViewPage.UserPermissions.Approve)
                                {%>
                            <button type="button" class="btn blue" onclick="if(document.reddevilForm.boxchecked.value>0 && document.reddevilForm.empmanger.value.length>0){reddevil_exec_cmd('setnv')}" data-toggle="tooltip" data-placement="bottom" data-original-title="Set NV quản lý Shop"><i class="fa fa-files-o"></i>Set NV quản lý</button>
                            <%} %>
                            <%=GetTinyListCommand()%>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="dataTables_wrapper">
                            <div class="row hidden-sm hidden-col">
                                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                    <div class="dataTables_search">
                                        <input type="text" class="form-control input-inline input-sm" id="filter_search" value="<%= model.SearchText %>" placeholder="Nhập từ khóa cần tìm" onchange="REDDEVILRedirect();return false;" />
                                        <button type="button" class="btn blue" onclick="REDDEVILRedirect();return false;">Tìm kiếm</button>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 col-md-4 col-lg-4 ">
                                    <div class="" style="width: 100% !important">

                                        <%if (listRole != null && listRole.Count > 0)
                                            { %>
                                        <select id="shop_id" class="form-control input-inline input-sm chosen_shop">
                                            <option value="">- Chọn quản lý cho Shop -</option>
                                            <%foreach (var item in listRole)
                                                {
                                                    var _userAd = CPUserService.Instance.GetByID(item.UserID);
                                                    if (_userAd == null) continue;
                                            %>
                                            <option value="<%=_userAd.ID %>"><%=_userAd.Name %></option>
                                            <%} %>
                                        </select>
                                        <%} %>
                                    </div>


                                </div>
                                <div class="col-12 col-sm-12 col-md-8 col-lg-4 text-right pull-right">

                                    <div class="table-group-actions d-inline-block">
                                        <%= ShowDDLLang(model.LangID)%>
                                    </div>
                                </div>
                            </div>

                            <div class="table-scrollable">
                                <table class="table table-striped table-hover table-bordered dataTable">
                                    <thead>
                                        <tr>
                                            <th class="sorting text-center w1p">#</th>
                                            <th class="sorting_disabled text-center w1p">
                                                <label class="itemCheckBox itemCheckBox-sm">
                                                    <input type="checkbox" name="toggle" onclick="checkAll(<%= model.PageSize %>)">
                                                    <i class="check-box"></i>
                                                </label>
                                            </th>

                                            <th class="sorting"><%= GetSortLink("Tên shop", "Name")%></th>
                                            <th class="sorting"><%= GetSortLink("Tên đăng nhập", "LoginName")%></th>
                                            <th class="sorting text-center w5p hidden-sm hidden-col"><%= GetSortLink("SL sản phẩm bán", "CountProduct")%></th>
                                            <th class="sorting text-center w10p hidden-sm hidden-col"><%= GetSortLink("Tên thành viên - Email", "UserID")%></th>
                                            <th class="sorting text-center w10p hidden-sm hidden-col"><%= GetSortLink("Ảnh logo", "Logo")%></th>
                                            <th class="sorting text-center w5p hidden-sm hidden-col"><%= GetSortLink("Nhân viên quản lý", "UserID")%></th>
                                            <th class="sorting text-center w10p hidden-sm hidden-col"><%= GetSortLink("Ngày đăng ký", "Published")%></th>
                                            <th class="sorting text-center w1p"><%= GetSortLink("Đối tác", "Partner")%></th>
                                            <th class="sorting text-center w1p"><%= GetSortLink("Duyệt", "Activity")%></th>
                                            <th class="sorting text-center w1p"><%= GetSortLink("Ẩn/Hiện Liên hệ", "Hide")%> </th>
                                            <th class="sorting text-center w1p"><%= GetSortLink("ID", "ID")%></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%for (var i = 0; listItem != null && i < listItem.Count; i++)
                                            { %>
                                        <tr>
                                            <td class="text-center"><%= i + 1%></td>
                                            <td class="text-center">
                                                <%= GetCheckbox(listItem[i].ID, i)%>
                                            </td>
                                            <td>
                                                <a href="javascript:REDDEVILRedirect('Add', <%= listItem[i].ID %>)"><%= listItem[i].Name%></a>
                                                <p class="smallsub hidden-sm hidden-col">(<span>Url Shop:</span> <a href="<%=CoreMr.Reddevil.Web.HttpRequest.Domain %>/shop/<%= listItem[i].Url%><%=Setting.Sys_PageExt %>" target="_blank"><%= listItem[i].Url%></a>)</p>
                                            </td>
                                            <td class="text-center hidden-sm hidden-col">
                                                <a href="/{CPPath}/ModUser/Add.aspx/RecordID/<%=listItem[i].GetUser().ID %>" target="_blank"><%=listItem[i].GetUser().LoginName%></a>
                                            </td>
                                            <td class="text-center hidden-sm hidden-col"><%=listItem[i].CountProduct%></td>
                                            <td class="text-center hidden-sm hidden-col"><%=listItem[i].GetUser().Name +" - "+ listItem[i].GetUser().Email%></td>
                                            <td class="text-center hidden-sm hidden-col">
                                                <%= Utils.GetMedia(listItem[i].Logo, 40, 40)%>
                                            </td>
                                            <td class="text-center hidden-sm hidden-col"><%=listItem[i].GetUserAd.Name +" - Phone: "+listItem[i].GetUserAd.Phone %></td>
                                            <td class="text-center hidden-sm hidden-col"><%= string.Format("{0:dd-MM-yyyy}", listItem[i].Created) %></td>
                                            <td class="text-center">
                                                <%if (listItem[i].Partner)
                                                    { %><a href="javascript:void(0)" title="Shop đã là đối tác"> <span class="fa fa-check-circle publish "></span></a><%}
                                                                                                                                                                     else
                                                                                                                                                                     {%><a href="javascript:void(0)" title="Shop chưa phải đối tác"><span class="fa fa-check-circle publish fa fa-dot-circle-o unpublish"></span></a> <%} %>
                                            </td>
                                            <td class="text-center">
                                                <%if (listItem[i].Activity)
                                                    { %><a href="javascript:void(0)" title="Đã duyệt shop"> <span class="fa fa-check-circle publish "></span></a><%}
                                                                                                                                                                     else
                                                                                                                                                                     {%><a href="javascript:void(0)" title="Shop chưa được duyệt"><span class="fa fa-check-circle publish fa fa-dot-circle-o unpublish"></span></a> <%} %>
                                            </td>
                                            <td class="text-center">
                                                <%if (listItem[i].Hide)
                                                    { %><a href="javascript:void(0)" title="Đã ẩn liên hệ của shop"> <span class="fa fa-check-circle publish "></span></a><%}
                                                                                                                                                                     else
                                                                                                                                                                     {%><a href="javascript:void(0)" title="Đã hiển thị liên hệ của shop"><span class="fa fa-check-circle publish fa fa-dot-circle-o unpublish"></span></a> <%} %>
                                            </td>
                                            <td class="text-center"><%= listItem[i].ID%></td>
                                        </tr>
                                        <%} %>
                                    </tbody>
                                </table>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 justify-content-center d-flex ">
                                    <%= GetPagination(model.PageIndex, model.PageSize, model.TotalRecord)%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <link rel="stylesheet" href="/{CPPath}/interface/utils/chosen/chosen.css" type="text/css" media="all" />
    <script type="text/javascript" src="/{CPPath}/interface/utils/chosen/chosen.js"></script>
    <script type="text/javascript">
        window.document.title = 'Quản lý gian hàng';
        var REDDEVILController = "ModShop";

        var REDDEVILArrVar = [
            "filter_lang", "LangID",
            "limit", "PageSize"
        ];

        var REDDEVILArrVar_QS = [
            "filter_search", "SearchText"
        ];

        var REDDEVILArrQT = [
            "<%= model.PageIndex + 1 %>", "PageIndex",
            "<%= model.Sort %>", "Sort"
        ];

        var REDDEVILArrDefault = [
            "1", "PageIndex",
            "1", "LangID",
            "20", "PageSize"
        ];
        $('.chosen_shop').chosen({

            display_selected_options: true,
            placeholder_text_multiple: 'Gõ từ khóa để tìm quản lý cho shop',
            no_results_text: 'Không có kết quả',
            enable_split_word_search: true,
            search_contains: true,
            display_disabled_options: true,
            single_backstroke_delete: false,
            inherit_select_classes: true
        });
        $(".chosen_shop").on('change', function () {
            $('#empmanger').val($(".chosen_shop").val())
        })
    </script>

</form>
