﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.CPViewControl" %>

<%
    var model = ViewBag.Model as ModShopModel;
    var item = ViewBag.Data as ModShopEntity;
%>

<form id="reddevilForm" name="reddevilForm" method="post">
    <input type="hidden" id="_reddevil_action" name="_reddevil_action" />

    <div class="page-content-wrapper">
        <h3 class="page-title"><small><%= model.RecordID > 0 ? "Chỉnh sửa ": "Thêm mới "%></small>gian hàng</h3>
        <div class="page-bar justify-content-between">
            <ul class="breadcrumb">
                <li class="breadcrumb-item">
                    <i class="fa fa-home"></i>
                    <a href="/{CPPath}/">Home</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="/{CPPath}/<%=CPViewPage.CurrentModule.Code%>/Index.aspx">Shop</a>
                </li>
            </ul>
            <div class="page-toolbar">
                <div class="btn-group">
                    <a href="/" class="btn green" target="_blank"><i class="icon-screen-desktop"></i>Xem Website</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <%= ShowMessage()%>

                <div class="form-horizontal form-row-seperated">
                    <div class="portlet">
                        <div class="portlet-title">
                            <div class="caption"></div>
                            <div class="actions btn-set">
                                <%= GetDefaultAddCommand()%>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="portlet box blue-steel">
                                        <div class="portlet-title">
                                            <div class="caption">Thông tin chung</div>
                                        </div>
                                        <div class="portlet-body">
                                            <div class="form-body">
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-right">Tên shop:</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" name="Name" value="<%=item.Name %>" />
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-right">Logo:</label>
                                                    <div class="col-md-9">
                                                        <%if (!string.IsNullOrEmpty(item.Logo))
                                                            { %>
                                                        <p class="preview "><%= Utils.GetMedia(item.Logo, 80, 80)%></p>
                                                        <%}
                                                            else
                                                            { %>
                                                        <p class="preview">
                                                            <img src="" width="80" height="80" />
                                                        </p>
                                                        <%} %>
                                                        <%--  <div class="form-inline">
                                                            <input type="text" class="form-control" name="File" id="File" value="<%=item.File %>" />
                                                            <button type="button" class="btn btn-primary" onclick="ShowFileForm('File'); return false">Chọn ảnh</button>
                                                        </div>--%>
                                                    </div>
                                                </div>
                                                <%if (CPViewPage.UserPermissions.Full)
                                                    {%>

                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-right">Duyệt:</label>
                                                    <div class="col-md-9">
                                                        <label class="radioPure radio-inline" style="float: left; width: 10%">
                                                            <input type="radio" name="Activity" <%= item.Activity ? "checked": "" %> value="1" />
                                                            <span class="outer"><span class="inner"></span></span><i>Có</i>
                                                        </label>
                                                        <label class="radioPure radio-inline" style="float: left; width: 10%">
                                                            <input type="radio" name="Activity" <%= !item.Activity ? "checked": "" %> value="0" />
                                                            <span class="outer"><span class="inner"></span></span><i>Không</i>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-right">Ẩn/Hiện Liên hệ:</label>
                                                    <div class="col-md-9">
                                                        <label class="radioPure radio-inline" style="float: left; width: 10%">
                                                            <input type="radio" name="Hide" <%= item.Hide ? "checked": "" %> value="1" />
                                                            <span class="outer"><span class="inner"></span></span><i>Ẩn liên hệ của shop</i>
                                                        </label>
                                                        <label class="radioPure radio-inline" style="float: left; width: 10%">
                                                            <input type="radio" name="Hide" <%= !item.Hide ? "checked": "" %> value="0" />
                                                            <span class="outer"><span class="inner"></span></span><i>Hiển thị liên hệ của shop</i>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-right">Duyệt Là Đối Tác:</label>
                                                    <div class="col-md-9">
                                                        <label class="radioPure radio-inline" style="float: left; width: 10%">
                                                            <input type="radio" name="Partner" <%= item.Partner ? "checked": "" %> value="1" />
                                                            <span class="outer"><span class="inner"></span></span><i>Có</i>
                                                        </label>
                                                        <label class="radioPure radio-inline" style="float: left; width: 10%">
                                                            <input type="radio" name="Partner" <%= !item.Partner ? "checked": "" %> value="0" />
                                                            <span class="outer"><span class="inner"></span></span><i>Không</i>
                                                        </label>
                                                    </div>
                                                </div>
                                                <%} %>
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-right">Thông tin thành viên đăng ký:</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" disabled="" value="<%=item.GetUser().Name %>" />
                                                        <a href="/{CPPath}/ModUser/Add.aspx/RecordID/<%=item.GetUser().ID %>" target="_blank">
                                                            <p>Vào cập nhật thông tin</p>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-right"><a href="<%=item.WebCost %>" target="_blank" style="color: blue">Xem website:</a></label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" name="WebCost" value="<%=item.WebCost %>" placeholder="ví dụ: http://chenlin.vn" />
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-right"><a href="mailto:<%=item.EmailPartNer %>" style="color: blue">Gửi Email đối tác:</a></label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" name="EmailPartNer" value="<%=item.EmailPartNer %>" />
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-right">Email của shop:</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" name="Email" value="<%=item.GetUser().Email %>" />
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-right">Số điện thoại:</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" name="Phone" value="<%=item.GetUser().Phone %>" />
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-right">URL:</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" disabled="disabled" value="http://webtinhte.netshop/<%=item.Url %>" />
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-right">Mô tả đối tác(Hiển thị ngoài web):</label>
                                                    <div class="col-md-9">
                                                        <textarea class="form-control description" disabled="disabled" rows="5" placeholder="Nhập nội dung tóm tắt. Tối đa 400 ký tự"><%=item.Summary%></textarea>
                                                        <span class="help-block text-primary">Ký tự đối ta: 400</span>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-right">Mô tả đối tác(Hiển thị trong admin):</label>
                                                    <div class="col-md-9">
                                                        <textarea class="form-control description ckeditor" rows="5" name="Content" placeholder="Nhập nội dung tóm tắt. Tối đa 400 ký tự"><%=item.Content%></textarea>
                                                        <span class="help-block text-primary">Ký tự đối ta: 4000</span>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</form>
