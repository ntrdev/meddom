﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.CPViewControl" %>

<%
    var model = ViewBag.Model as ModAdvModel;
    var item = ViewBag.Data as ModAdvEntity;
%>

<form id="reddevilForm" name="reddevilForm" method="post">
    <input type="hidden" id="_reddevil_action" name="_reddevil_action" />
    <div class="pcoded-content">
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8 d-none d-lg-block">
                    <div class="page-header-title">
                        <i class="icon-social-dropbox"></i>
                        <div class="d-inline">
                            <h5>Quản lý Ảnh/Banner</h5>
                            <span><%= model.RecordID > 0 ? "Cập nhật Ảnh/Banner" : "Thêm mới "%></span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class=" breadcrumb breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="/{CPPath}/">
                                    <i class="icon-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="/{CPPath}/<%=CPViewPage.CurrentModule.Code%>/Index.aspx">Ảnh/Banner QC</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <%= ShowMessage()%>
                                </div>
                            </div>
                            <div class="col-sm-9">
                                <div class="card content-editor">
                                    <div class="card-header header-control">
                                        <h5><%= model.RecordID > 0 ? "Cập nhật Ảnh/Banner" : "Thêm mới "%></h5>
                                        <div class="controler-header">
                                            <%=GetDefaultAddCommand() %>
                                        </div>
                                    </div>
                                    <div class="card-block pdt-0">
                                        <div class="tabs-scroll">
                                            <ul class="nav nav-tabs md-tabs " role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link active" data-toggle="tab"
                                                        href="#thongtinchung" role="tab"><i class="icon-info"></i>Thông tin chung</a>
                                                    <div class="slide"></div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="thongtinchung"
                                                role="tabpanel">
                                                <h4 class="sub-title">Thông tin ảnh</h4>
                                                <div class="edit-margin-mb form-manager">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <label class="col-sm-12 col-form-label">Tên ảnh</label>
                                                                    <div class="col-sm-12">
                                                                        <input type="text" class="form-control" id="Name" name="Name" value="<%=item.Name %>" placeholder="nhập vào đây, tối đa 200 ký tự ...">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <label class="col-sm-12 col-form-label">Chuyên mục</label>
                                                                    <div class="col-sm-12">
                                                                        <select class="form-control chosen" name="MenuID" id="MenuID" onchange="GetProperties(this.value)">
                                                                            <%= Utils.ShowDdlMenuByType("Adv", model.LangID, item.MenuID)%>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                             <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <label class="col-sm-12 col-form-label">Link chèn vào ảnh</label>
                                                                    <div class="col-sm-12">
                                                                        <input type="text" class="form-control" id="URL" name="URL" value="<%=item.URL %>" placeholder="Copy link paste in...">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <label class="col-sm-12 col-form-label">Mở link(Click vào chuyển tới link hoặc mở 1 tab mới)</label>
                                                                    <div class="col-sm-12">
                                                                        <select class="form-control" name="Target">
                                                                            <option value=""></option>
                                                                            <option <%if (item.Target == "_blank")
                                                                                {%>selected<%} %>
                                                                                value="_blank">Mở tab mới</option>
                                                                            <option <%if (item.Target == "_parent")
                                                                                {%>selected<%} %>
                                                                                value="_parent">Chuyển hướng ngay</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                     <%--  <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <label class="col-sm-12 col-form-label">Chiều cao ảnh(px)</label>
                                                                    <div class="col-sm-12">
                                                                        <input type="text" class="form-control" id="Height" onclick="this.type = 'number'" name="Height" value="<%=item.Height %>" placeholder="vd: 20px...">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6"> 
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <label class="col-sm-12 col-form-label">Chiều rộng ảnh(px)</label>
                                                                    <div class="col-sm-12">
                                                                        <input type="text" class="form-control" id="Width" onclick="this.type = 'number'" name="Width" value="<%=item.Width %>" placeholder="vd: 20px...">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>--%>
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <label class="col-sm-12 col-form-label">
                                                                        Tóm tắt
                                                                    </label>
                                                                    <div class="col-sm-12">
                                                                        <div class="plugin-ckEditor">
                                                                            <textarea class="form-control" rows="3" name="Summary" placeholder="Mô tả tóm tắt"><%=item.Summary %></textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="block-controlRight">
                                    <div class="ItemControl">
                                        <a class="titleCollap" data-toggle="collapse"
                                            href="#ctr-trangthai">Trạng thái và hiển thị</a>
                                        <div id="ctr-trangthai" class="panel-collapse collapse show">
                                            <div class="panel-body">
                                                <ul class="form-block">
                                                    <%if (CPViewPage.UserPermissions.Approve)
                                                        {%>
                                                    <li>
                                                        <div>Hiển thị</div>
                                                        <div class="editorBox">
                                                            <%=GetHideOrShow(item.ID, item.Activity) %>
                                                        </div>
                                                    </li>
                                                    <%} %>
                                                </ul>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="ItemControl">
                                        <a class="titleCollap collapsed" data-toggle="collapse" href="#ctr-advance">Ảnh đại diện</a>
                                        <div id="ctr-advance" class="panel-collapse collapse show" style="" aria-expanded="false">
                                            <div class="panel-body">
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <input class="input-file" type="button" id="imgInp" onclick="ShowFileForm('File'); return false">
                                                        <label tabindex="0" for="my-file" class="input-file-trigger text-center">
                                                            <i class="icon-cloud-upload"></i>&nbsp;Chọn ảnh đại diện</label>
                                                        <img id="show_img_upload" src="<%=Utils.OptimalImage(item.File) %>" />
                                                        <input type="hidden" class="form-control" name="File" id="File" value="<%=item.File %>" />
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



</form>
