﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.CPViewControl" %>

<%
    var model = ViewBag.Model as ModAdvModel;
    var listItem = ViewBag.Data as List<ModAdvEntity>;
%>

<form id="reddevilForm" name="reddevilForm" method="post">

    <input type="hidden" id="_reddevil_action" name="_reddevil_action" />
    <input type="hidden" id="boxchecked" name="boxchecked" value="0" />


    <div class="pcoded-content">
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8 d-none d-lg-block">
                    <div class="page-header-title">
                        <i class="icon-social-dropbox"></i>
                        <div class="d-inline">
                            <h5>Biên tập banner</h5>
                            <span>Danh sách banner</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class=" breadcrumb breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="/{CPPath}/">
                                    <i class="icon-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="/{CPPath}/<%=CPViewPage.CurrentModule.Code%>/Index.aspx">Ảnh - Banner</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>


        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <%= ShowMessage()%>
                                </div>
                                <div class="card">
                                    <div class="card-header header-control">
                                        <h5>Danh sách Banner</h5>
                                        <div class="controler-header">
                                            <%=GetDefaultCommand() %>
                                        </div>
                                    </div>
                                    <div class="card-header header-control">
                                        <div class="tools col-md-12">
                                            <div class="dataTables_search col-md-6 col-xs-12">
                                                <input type="text" class="form-control input-inline input-sm" id="filter_search" style="float: left; width: 100%; padding-top: 1px; position: relative; background: #fff; height: 32px; text-indent: 10px; font-size: 14px; border-radius: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px;"
                                                    value="<%= model.SearchText %>" placeholder="Nhập từ khóa cần tìm" onchange="REDDEVILRedirect();return false;" />
                                                <button type="submit" class="btntop" onclick="REDDEVILRedirect();return false;" style="float: right; width: 40px; height: 32px; border: 0; cursor: pointer; background: none; position: absolute; right: 15px;">
                                                    <i class="fa fa-search"></i>
                                                </button>
                                            </div>
                                            <div class="table-group-actions d-inline-block col-md-4 col-xs-12">
                                                <select id="filter_menu" class="form-control input-inline input-sm chosen_menu" onchange="REDDEVILRedirect()" size="1">
                                                    <option value="0">Lọc theo chuyên mục</option>
                                                    <%= Utils.ShowDdlMenuByType("Adv", model.LangID, model.MenuID)%>
                                                </select>

                                            </div>

<%--                                            <div class="table-group-actions d-inline-block col-md-2 col-xs-12">
                                                <%= ShowDDLLang(model.LangID)%>
                                            </div>--%>

                                        </div>
                                    </div>
                                    <div class="card-block">
                                        <div class="dt-responsive table-responsive">
                                            <table id="" class="table_basic table table-hover m-b-0 "
                                                style="width: 100%">
                                                <thead>
                                                    <tr>
                                                        <th style="text-align: center;">
                                                            <div class="border-checkbox-section">
                                                                <div class="md-checkbox border-checkbox-group border-checkbox-group-info" data-toggle="tooltip" data-placement="bottom" data-original-title="Click chọn tất cả">
                                                                    <input type="checkbox" id="checks" name="toggle" onclick="checkAll('<%=model.PageSize%>')" class="md-check border-checkbox">
                                                                    <label for="checks" class="border-checkbox-label"></label>
                                                                </div>
                                                            </div>
                                                        </th>
                                                        <th>Ảnh</th>
                                                        <th><%= GetSortLink("Tên ảnh (alt) ", "Name")%></th>
                                                        <th><%= GetSortLink("Link chèn(Url)", "Url")%></th>
                                                        <th><%= GetSortLink("Danh mục", "MenuID")%></th>
                                                        <%--<th class="text-center">Trạng thái</th>--%>
                                                        <th style="width: 110px">Sắp xếp vị trí</th>
                                                        <th class="text-center">Tùy chọn</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <%for (var i = 0; listItem != null && i < listItem.Count; i++)
                                                        { %>
                                                    <tr>

                                                        <td class="text-center">
                                                            <%= GetCheckbox(listItem[i].ID, i)%>
                                                        </td>
                                                        <td class="text-center hidden-sm hidden-col">
                                                            <%= Utils.GetMedia(listItem[i].File, 40, 40)%>
                                                        </td>
                                                        <td>
                                                            <a style="color: orangered; font-weight: 700" href="javascript:REDDEVILRedirect('Add', <%= listItem[i].ID %>)"><%= listItem[i].Name%></a>
                                                        </td>
                                                        <td class="text-center hidden-sm hidden-col"><%= listItem[i].URL %></td>
                                                        <td class="text-center hidden-sm hidden-col"><%= GetName(listItem[i].GetMenu()) %></td>
                                                        <%--<td class="text-center"><%= GetPublish(listItem[i].ID, listItem[i].Activity)%></td>--%>
                                                        <td class="text-center"><%= GetOrder(listItem[i].ID, listItem[i].Order)%></td>
                                                        <td class="text-center">
                                                            <div class="control-button">
                                                                <button data-toggle="tooltip" data-original-title="Chỉnh sửa" onclick="REDDEVILRedirect('Add', <%= listItem[i].ID %>)" type="button" class="btn waves-effect waves-light btn-primary btn-icon"><i class="icon-pencil"></i></button>
                                                                <button class="btn waves-effect waves-light btn-danger btn-icon" data-toggle="tooltip" data-original-title="ID" type="button"><%=listItem[i].ID %></button>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <%} %>
                                                </tbody>
                                            </table>

                                        </div>
                                        <div class="row center">
                                            <div class="col-md-12 col-sm-12 justify-content-center d-flex ">
                                                <%= GetPagination(model.PageIndex, model.PageSize, model.TotalRecord)%>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">

        var REDDEVILController = "ModAdv";

        var REDDEVILArrVar = [
            "filter_menu", "MenuID",
            "filter_lang", "LangID",
            "limit", "PageSize"
        ];

        var REDDEVILArrQT = [
            "<%= model.PageIndex + 1 %>", "PageIndex",
            "<%= model.Sort %>", "Sort"
        ];

        var REDDEVILArrDefault = [
            "1", "PageIndex",
            "1", "LangID",
            "20", "PageSize"
        ];
    </script>

</form>
