﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.CPViewControl" %>

<%
    var model = ViewBag.Model as ModScientistModel;
    var item = ViewBag.Data as ModScientistEntity;

%>

<form id="reddevilForm" name="reddevilForm" method="post">
    <input type="hidden" id="_reddevil_action" name="_reddevil_action" />

    <div class="pcoded-content">
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8 d-none d-lg-block">
                    <div class="page-header-title">
                        <i class="icon-social-dropbox"></i>
                        <div class="d-inline">
                            <h5>Quản lý Tin tức</h5>
                            <span><%= model.RecordID > 0 ? "Cập nhật bài viết" : "Thêm mới bài viết"%></span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class=" breadcrumb breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="/{CPPath}/">
                                    <i class="icon-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="/{CPPath}/<%=CPViewPage.CurrentModule.Code%>/Index.aspx">Tin tức</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <%= ShowMessage()%>
                                </div>
                            </div>
                            <div class="col-sm-9">
                                <div class="card content-editor">
                                    <div class="card-header header-control">
                                        <h5>Thêm mới bài viết</h5>
                                        <div class="controler-header">
                                            <%=GetDefaultAddCommand() %>
                                        </div>
                                    </div>
                                    <div class="card-block pdt-0">
                                        <div class="tabs-scroll">
                                            <ul class="nav nav-tabs md-tabs " role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link active" data-toggle="tab"
                                                        href="#thongtinchung" role="tab"><i class="icon-info"></i>Thông tin chung</a>
                                                    <div class="slide"></div>
                                                </li>

                                            </ul>
                                        </div>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="thongtinchung"
                                                role="tabpanel">
                                                <h4 class="sub-title">Thông tin bài viết</h4>
                                                <div class="edit-margin-mb form-manager">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <label class="col-sm-12 col-form-label">Tên nhà khoa học</label>
                                                                    <div class="col-sm-12">
                                                                        <input type="text" class="form-control" id="Name" name="Name" value="<%=item.Name %>" placeholder="nhập vào đây, tối đa 200 ký tự ...">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <label class="col-sm-12 col-form-label">Danh mục nhà khoa học</label>
                                                                    <div class="col-sm-12">
                                                                        <select class="form-control chosen" name="MenuID" id="MenuID" onchange="GetProperties(this.value)">
                                                                            <%= Utils.ShowDdlMenuByType("Scientist", model.LangID, item.MenuID)%>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <label class="col-sm-12 col-form-label">Địa chỉ</label>
                                                                    <div class="col-sm-12">
                                                                        <div class="input-group input-group-info" style="margin-bottom: 0;">
                                                                            <span class="input-group-prepend">
                                                                                <label class="input-group-text"><i class="icon-map"></i></label>
                                                                            </span>
                                                                            <input style="border-color: #cccccc;" type="text" class="form-control fill" id="Address" name="Address" value="<%=item.Address %>" placeholder="Địa chỉ chi tiết...">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <label class="col-sm-12 col-form-label">Ngày sinh</label>
                                                                    <div class="col-sm-12">
                                                                        <div class="input-group input-group-danger" style="margin-bottom: 0;">
                                                                            <span class="input-group-prepend">
                                                                                <label class="input-group-text"><i class="icon-calendar"></i></label>
                                                                            </span>
                                                                            <input style="border-color: #cccccc;" type="text" class="form-control dateOfbirth fill" id="DateOfBirth" name="DateOfBirth" value="<%=item.DateOfBirth<=DateTime.MinValue?DateTime.Now.Date:item.DateOfBirth %>">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <label class="col-sm-12 col-form-label">Tỉnh/Thành phố</label>
                                                                    <div class="col-sm-12">
                                                                        <div class="input-group input-group-info" style="margin-bottom: 0;">
                                                                            <span class="input-group-prepend">
                                                                                <label class="input-group-text"><i class="icon-map"></i></label>
                                                                            </span>
                                                                            <select name="CityID" id="CityID" onchange="getDistrict(this.value, '0','DistrictID');" required="" class="form-control">
                                                                                <option value="0">- Chọn -</option>
                                                                                <%=Utils.ShowDdlMenuByTypeCity("City",1, item.CityID) %>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <label class="col-sm-12 col-form-label">Quận/Huyện</label>
                                                                    <div class="col-sm-12">
                                                                        <div class="input-group input-group-info" style="margin-bottom: 0;">
                                                                            <span class="input-group-prepend">
                                                                                <label class="input-group-text"><i class="icon-map"></i></label>
                                                                            </span>
                                                                            <select name="DistrictID" id="DistrictID" class="form-control chosen-district" required="" onchange="getDistrict(this.value,'0','WardID');">
                                                                                <option value="0">- Chọn Quận/Huyện -</option>
                                                                                <%=Utils.ShowDdlMenuByParent(item.CityID, item.DistrictID) %>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <label class="col-sm-12 col-form-label">Phường/Xã</label>
                                                                    <div class="col-sm-12">
                                                                        <div class="input-group input-group-info" style="margin-bottom: 0;">
                                                                            <span class="input-group-prepend">
                                                                                <label class="input-group-text"><i class="icon-map"></i></label>
                                                                            </span>
                                                                            <select id="WardID" name="WardID" class="form-control chosen-phuong">
                                                                                <option value="0">- Chọn -</option>
                                                                                <%=Utils.ShowDdlMenuByParent(item.DistrictID, item.WardID) %>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <label class="col-sm-12 col-form-label">
                                                                        Tóm tắt
                                                                    </label>
                                                                    <div class="col-sm-12">
                                                                        <div class="plugin-ckEditor">
                                                                            <textarea class="form-control" rows="3" name="Summary" placeholder="Mô tả tóm tắt"><%=item.Summary %></textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <label class="col-sm-12 col-form-label">
                                                                        Nội dung chi tiết
                                                                    </label>
                                                                    <div class="col-sm-12">
                                                                        <div class="plugin-ckEditor">
                                                                            <textarea class="form-control ckeditor" name="Content" placeholder=""><%=item.Content %></textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="block-controlRight">
                                    <div class="ItemControl">
                                        <a class="titleCollap" data-toggle="collapse"
                                            href="#ctr-trangthai">Trạng thái và hiển thị</a>
                                        <div id="ctr-trangthai" class="panel-collapse collapse show">
                                            <div class="panel-body">
                                                <ul class="form-block">
                                                    <li>
                                                        <div>Hiển thị</div>
                                                        <div class="editorBox">
                                                            <%=GetHideOrShow(item.ID, item.Activity) %>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div>Ngày Đăng</div>
                                                        <div class="editorBox">
                                                            <span><%=Utils.FormatDateTime(item.Created) %></span>
                                                        </div>
                                                    </li>
                                                    <li class="">
                                                        <div class="form-group form-md-checkboxes">
                                                            <label>Giới tính</label>
                                                            <div class="md-radio-inline form-radio">
                                                                <div class="radio radio-matrial radio-success radio-inline">
                                                                    <label>
                                                                        <input type="radio" id="rdo_decktop" name="Gender" <%=item.Gender?"checked=\"checked\"":"" %> class="md-radiobtn" value="1">
                                                                        <i class="helper"></i>Nam
                                                                    </label>
                                                                </div>
                                                                <div class="radio radio-matrial radio-success radio-inline">
                                                                    <label>
                                                                        <input type="radio" id="rdo_decktop" name="Gender" <%=!item.Gender?"checked=\"checked\"":"" %> class="md-radiobtn" value="0">
                                                                        <i class="helper"></i>Nữ
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div>Tác giả</div>
                                                        <div class="editorBox">
                                                            <%=item.GetAdmin().LoginName %>
                                                        </div>
                                                    </li>
                                                    <li class="blockform">
                                                        <div>Đường dẫn URL</div>
                                                        <div class="editorBox">
                                                            <input type="text" class="form-block" id="Url" name="Url" value="<%=item.Url %>" placeholder="Nếu không nhập sẽ tự sinh theo Tiêu đề">
                                                        </div>
                                                    </li>

                                                </ul>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="ItemControl">
                                        <a class="titleCollap" data-toggle="collapse" href="#ctr-advance">Ảnh đại diện</a>
                                        <div id="ctr-advance" class="panel-collapse collapse" style="height: 0px;" aria-expanded="false">
                                            <div class="panel-body">
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <input class="input-file" type="button" id="imgInp" onclick="ShowFileForm('FileAvatar'); return false">
                                                        <label tabindex="0" for="my-file" class="input-file-trigger text-center">
                                                            <i class="icon-cloud-upload"></i>&nbsp;Chọn ảnh đại diện</label>
                                                        <img id="show_img_upload" src="<%=Utils.DefaultImage(item.FileAvatar) %>" />
                                                        <input type="hidden" class="form-control" name="FileAvatar" id="FileAvatar" value="<%=item.FileAvatar %>" />
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="ItemControl">
                                        <a class="titleCollap" data-toggle="collapse"
                                            href="#ctr-seo-editor">Thông tin thêm</a>
                                        <div id="ctr-seo-editor" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <div class="card-block">


                                                    <div class="form-group row">
                                                        <label
                                                            class="col-sm-12 col-form-label">
                                                            Học hàm:</label>
                                                        <div class="col-sm-12">
                                                            <input type="text" class="form-control" id="PageTitle" name="AcademicRank" value="<%=item.AcademicRank %>" placeholder="vd:Phó Giáo sư...">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label
                                                            class="col-sm-12 col-form-label">
                                                            Học vị :</label>
                                                        <div class="col-sm-12">
                                                            <input type="text" class="form-control" name="Degree" value="<%=item.Degree %>" placeholder="vd:Tiến sĩ..." />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">Danh hiệu, giải thưởng:</label>
                                                        <div class="tags_max_char">
                                                            <textarea class="form-control" rows="3" name="Appellation" placeholder="Nhập nội dung tóm tắt. Tối đa 300 ký tự"><%=item.Appellation%></textarea>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>


    <link rel="stylesheet" href="/{CPPath}/interface/utils/chosen/chosen.css" type="text/css" media="all" />
    <script type="text/javascript" src="/{CPPath}/interface/utils/chosen/chosen.js"></script>
    <script type="text/javascript">
        //setTimeout(function () { reddevil_exec_cmd('[autosave][<%=model.RecordID%>]') }, 5000);
    </script>
</form>
