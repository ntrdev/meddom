﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.CPViewControl" %>

<%
    var model = ViewBag.Model as SysResourceModel;
    var listLang = SysLangService.Instance.CreateQuery().ToList_Cache();
%>

<form id="reddevilForm" name="reddevilForm" method="post">

    <input type="hidden" id="_reddevil_action" name="_reddevil_action" />

    <div class="pcoded-content">
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8 d-none d-lg-block">
                    <div class="page-header-title">
                        <i class="icon-social-dropbox"></i>
                        <div class="d-inline">
                            <h5>Quản lý Tài nguyên</h5>
                            <span>Thêm nhiều Thêm nhiều</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class=" breadcrumb breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="/{CPPath}/">
                                    <i class="icon-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="/{CPPath}/<%=CPViewPage.CurrentModule.Code%>/Index.aspx">Thêm nhiều Tài nguyên</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <%= ShowMessage()%>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="card content-editor">
                                    <div class="card-header header-control">
                                        <h5>Thêm nhiều tài nguyên</h5>
                                        <div class="controler-header">
                                            <%=GetDefaultAddCommand() %>
                                        </div>
                                    </div>
                                    <div class="card-block pdt-0">

                                        <div class="tab-content">
                                            <div class="tab-pane active" id="thongtinchung"
                                                role="tabpanel">
                                                <h4 class="sub-title">Thông tin tài nguyên</h4>
                                                <div class="edit-margin-mb form-manager">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <label class="col-sm-12 col-form-label">Danh sách Tài nguyên:</label>
                                                                    <div class="col-sm-12">
                                                                        <textarea class="form-control custom" name="Resource" rows="5" cols="" placeholder="Key1=Value1&#10;Key2=Value2"><%=model.Resource %></textarea>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 col-form-label text-right">&nbsp;</label>
                                                                <div class="col-md-9">
                                                                    <p class="help-block text-primary">Nhập dạng:</p>
                                                                    <p class="help-block text-primary">Key1=Value1</p>
                                                                    <p class="help-block text-primary">Key2=Value2</p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    

</form>
