﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.CPViewControl" %>

<%
    var model = ViewBag.Model as ModNewsModel;
    var item = ViewBag.Data as ModNewsEntity;
    var listFile = item.GetFile ?? new List<ModDynamicFileEntity>();
    var listNews = item.GetNews ?? new List<ModNewsMutilEntity>();
%>
<input type="hidden" id="ModuleCode" value="<%=CPViewPage.CurrentModule.Code %>" />
<form id="reddevilForm" name="reddevilForm" method="post">

    <input type="hidden" id="RecordID" value="<%=model.RecordID %>" />
    <input type="hidden" id="_reddevil_action" name="_reddevil_action" />


    <div class="pcoded-content">
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8 d-none d-lg-block">
                    <div class="page-header-title">
                        <i class="fa fa-file-image-o"></i>
                        <div class="d-inline">
                            <h5>Quản lý Tin tức</h5>
                            <span><%= model.RecordID > 0 ? "Cập nhật bài viết" : "Thêm mới bài viết"%></span>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="page-header-breadcrumb">
                                    <ul class=" breadcrumb breadcrumb-title">
                                        <li class="breadcrumb-item">
                                            <a href="/{CPPath}/">
                                                <i class="icon-home"></i>
                                            </a>

                                        </li>
                                        <li class="breadcrumb-item">
                                            <i class="fa fa-chevron-right"></i>
                                            <a href="/{CPPath}/<%=CPViewPage.CurrentModule.Code%>/Index.aspx">Tin tức</a>
                                        </li>
                                        <li class="breadcrumb-item">
                                            <i class="fa fa-chevron-right"></i>
                                            <a href="/{CPPath}/<%=CPViewPage.CurrentModule.Code%>/Add.aspx"><%= model.RecordID > 0 ? "Cập nhật "+item.Name : "Thêm mới bài viết"%></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="card">
                                    <%= ShowMessage()%>
                                    <div class="button-header controler-header">
                                        <%=GetDefaultAddCommand() %>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-9">
                                <div class="card content-editor">
                                    <div class="card-header header-control">
                                        <h5>Thêm mới bài viết</h5>
                                    </div>
                                    <div class="card-block pdt-0">
                                        <div class="tabs-scroll">
                                            <ul class="nav nav-tabs md-tabs " role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link active" data-toggle="tab"
                                                        href="#thongtinchung" role="tab"><i class="icon-info"></i>Thông tin chung</a>
                                                    <div class="slide"></div>
                                                </li>

                                            </ul>
                                        </div>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="thongtinchung"
                                                role="tabpanel">
                                                <h4 class="sub-title">Thông tin bài viết</h4>
                                                <div class="edit-margin-mb form-manager">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <label class="col-sm-12 col-form-label">Tiêu đề</label>
                                                                    <div class="col-sm-12">
                                                                        <input type="text" class="form-control" id="Name" name="Name" value="<%=item.Name %>" placeholder="nhập vào đây, tối đa 200 ký tự ...">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <label class="col-sm-12 col-form-label">Danh mục</label>
                                                                    <div class="col-sm-12">
                                                                        <select class="form-control chosen" name="MenuID" id="MenuID">
                                                                            <%= Utils.ShowDdlMenuByType4("News", model.LangID, item.MenuID)%>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <label class="col-sm-12 col-form-label">
                                                                        Tóm tắt
                                                                    </label>
                                                                    <div class="col-sm-12">
                                                                        <div class="plugin-ckEditor">
                                                                            <textarea class="form-control" rows="3" name="Summary" placeholder="Mô tả tóm tắt"><%=item.Summary %></textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <label class="col-sm-12 col-form-label">
                                                                        Nội dung chi tiết
                                                                    </label>
                                                                    <div class="col-sm-12">
                                                                        <div class="plugin-ckEditor">
                                                                            <textarea class="form-control ckeditor" name="Content" id="Content"><%=item.Content %></textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="block-controlRight">
                                    <div class="ItemControl">
                                        <a class="titleCollap" data-toggle="collapse" href="#ctr-trangthai">Trạng thái và hiển thị</a>
                                        <div id="ctr-trangthai" class="panel-collapse collapse show">
                                            <div class="panel-body">
                                                <ul class="form-block">
                                                    <%if (CPViewPage.CurrentUser.IsAdministrator && CPViewPage.UserPermissions.Full)
                                                        { %>
                                                    <li>
                                                        <div>Trạng thái</div>
                                                        <div class="editorBox">
                                                            <select class="form-control chosen" name="Condition" id="Condition">
                                                                <option value="0" <%=item.Activity?"selected=\"selected\"":"" %>>- Đã duyệt -</option>
                                                                <option value="1" <%=item.Condition==1?"selected=\"selected\"":"" %>>- Đang soạn -</option>
                                                                <option value="2" <%=item.Condition==2?"selected=\"selected\"":"" %>>- Chờ duyệt -</option>
                                                                <option value="3" <%=item.Condition==3?"selected=\"selected\"":"" %>>- Trả về -</option>
                                                            </select>
                                                        </div>
                                                    </li>
                                                    <%}
                                                        else if (item.Condition > 0 && item.AdminEdit == CPLogin.CurrentUser.LoginName)
                                                        { %>
                                                    <li>
                                                        <div>Trạng thái</div>
                                                        <div class="editorBox">
                                                            <select class="form-control chosen" name="Condition" id="Condition">
                                                                <%--<option value="0" <%=item.Activity?"selected=\"selected\"":"" %>>- Đã duyệt -</option>--%>
                                                                <option value="1" <%=item.Condition==1?"selected=\"selected\"":"" %>>- Đang soạn -</option>
                                                                <option value="2" <%=item.Condition==2?"selected=\"selected\"":"" %>>- Chờ duyệt -</option>
                                                                <option value="3" disabled <%=item.Condition==3?"selected=\"selected\"":"" %>>- Trả về -</option>
                                                            </select>

                                                        </div>
                                                    </li>
                                                    <%} %>
                                                    <%if (CPViewPage.UserPermissions.Approve)
                                                        { %>
                                                    <li>
                                                        <div>Duyệt/Hủy duyệt</div>
                                                        <div class="md-radio-inline form-radio">
                                                            <div class="radio radio-matrial radio-success radio-inline">
                                                                <label>
                                                                    <input type="radio" id="rdo_decktop" name="Activity" <%= item.Activity ?"checked": "" %> class="md-radiobtn" value="1">
                                                                    <i class="helper"></i>Duyệt
                                                                </label>
                                                            </div>
                                                            <div class="radio radio-matrial radio-danger radio-inline">
                                                                <label>
                                                                    <input type="radio" id="rdo_mobile" name="Activity" <%= !item.Activity?"checked": "" %> value="0" class="md-radiobtn">
                                                                    <i class="helper"></i>Hủy Duyệt
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <%} %>

                                                    <li>
                                                        <div>Nguồn gốc</div>
                                                        <div class="md-radio-inline form-radio">
                                                            <div class="radio radio-matrial radio-success radio-inline">
                                                                <label>
                                                                    <input type="radio" name="Cost" <%= item.Cost ?"checked": "" %> class="md-radiobtn" value="1">
                                                                    <i class="helper"></i>Tham khảo
                                                                </label>
                                                            </div>
                                                            <div class="radio radio-matrial radio-danger radio-inline">
                                                                <label>
                                                                    <input type="radio" name="Cost" <%= !item.Cost?"checked": "" %> value="0" class="md-radiobtn">
                                                                    <i class="helper"></i>Tự viết
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div>Đăng</div>
                                                        <div class="editorBox">
                                                            <span><%=Utils.FormatDateTime(item.Published) %></span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div>Tác giả</div>
                                                        <div class="editorBox">
                                                            <%=item.AdminEdit %>
                                                        </div>
                                                    </li>
                                                    <li class="blockform">
                                                        <div>Đường dẫn URL</div>
                                                        <div class="editorBox">
                                                            <input type="text" class="form-block" id="Url" name="Url" value="<%=item.Url %>" placeholder="Nếu không nhập sẽ tự sinh theo Tiêu đề">
                                                        </div>
                                                    </li>
                                                    <li class="">
                                                        <div class="form-group form-md-checkboxes">
                                                            <label>Nơi hiển thị tin tức</label>
                                                            <div class="md-checkbox-inline border-checkbox-section">
                                                                <%= Utils.ShowCheckBoxByConfigkey("Mod.NewsState", "ArrState", item.State)%>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="">
                                                        <div class="form-group ">
                                                            <label>Chuyển chuyên mục</label>
                                                            <div class="col-sm-12">
                                                                <select class="form-control chosen" name="ChangeID" id="ChangeID">
                                                                    <option value="0">-Chọn danh mục cần chuyển-</option>
                                                                    <%= Utils.ShowDdlAllMenuNews(model.LangID, model.ChangeID)%>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="ItemControl">
                                        <a class="titleCollap" data-toggle="collapse" href="#ctr-advance">Ảnh</a>
                                        <div id="ctr-advance" class="panel-collapse collapse" style="height: 0px;" aria-expanded="false">
                                            <div class="panel-body">
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <input class="input-file" type="button" id="imgInp" onclick="ShowFileForm('File'); return false">
                                                        <label tabindex="0" for="my-file" class="input-file-trigger text-center">
                                                            <i class="icon-cloud-upload"></i>&nbsp;Chọn ảnh đại diện</label>
                                                        <img id="show_img_upload" src="<%=Utils.DefaultImage(item.File) %>" />
                                                        <input type="hidden" class="form-control" name="File" id="File" value="<%=item.File %>" />
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <input class="input-file" type="button" onclick="ShowFile(); return false">
                                                        <input class="input-file" type="hidden" hidden id="dynamic_id" value="3">
                                                        <label tabindex="0" for="my-file" class="input-file-trigger text-center">
                                                            <i class="icon-cloud-upload"></i>&nbsp;Chọn thêm ảnh
                                                        </label>

                                                        <ul class="cmd-custom" id="list-file">
                                                            <%for (int i = 0; i < listFile.Count; i++)
                                                                {%>
                                                            <li>
                                                                <img src="<%=Utils.OptimalImage(listFile[i].File) %>" />
                                                                <a href="javascript:void(0)" onclick="deleteFile('<%=listFile[i].File %>','3')" data-toggle="tooltip" data-placement="bottom" data-original-title="Xóa"><i class="fa fa-ban"></i></a>
                                                                <a href="javascript:void(0)" onclick="upFile('<%=listFile[i].File %>','3')" data-toggle="tooltip" data-placement="bottom" data-original-title="Chuyển lên trên"><i class="fa fa-arrow-up"></i></a>
                                                                <a href="javascript:void(0)" onclick="downFile('<%=listFile[i].File %>','3')" data-toggle="tooltip" data-placement="bottom" data-original-title="Chuyển xuống dưới"><i class="fa fa-arrow-down"></i></a>
                                                            </li>
                                                            <%} %>
                                                        </ul>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ItemControl">
                                        <a class="titleCollap" data-toggle="collapse" href="#ctr-news">Tin liên quan</a>
                                        <div id="ctr-news" class="panel-collapse collapse" style="height: 0px;" aria-expanded="false">
                                            <div class="panel-body">
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <input class="input-file" type="button" onclick="ShowNewsForm('FormNews', '<%=item.ID%>'); return false">
                                                        <input class="input-file" type="hidden" hidden id="Parent" value="<%=item.ID %>">
                                                        <label tabindex="0" for="my-file" class="input-file-trigger text-center">
                                                            <i class="icon-cloud-upload"></i>&nbsp;Chọn Tin Liên Quan
                                                        </label>

                                                        <ul class="cmd-custom" id="list-news">
                                                            <%for (int i = 0; i < listNews.Count; i++)
                                                                {
                                                                    var _news = ModNewsService.Instance.CreateQuery().Select(o => new { o.File, o.Url, o.Name }).Where(o => o.ID == listNews[i].NewsID).ToSingle_Cache();
                                                            %>
                                                            <li>
                                                                <a href="<%=CoreMr.Reddevil.Web.HttpRequest.Domain+"/"+_news.Url %>" target="_blank" style="width: 85%"><%=_news.Name %></a>
                                                                <a href="javascript:void(0)" onclick="deleteNews('<%=listNews[i].NewsID %>','<%=listNews[i].ParentID %>','<%=listNews[i].TypeID %>')" data-toggle="tooltip" data-placement="bottom" data-original-title="Xóa"><i class="fa fa-ban"></i></a>

                                                            </li>
                                                            <%} %>
                                                        </ul>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="ItemControl">
                                        <a class="titleCollap" data-toggle="collapse"
                                            href="#ctr-seo-editor">SEO Editor</a>
                                        <div id="ctr-seo-editor" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <div class="card-block">


                                                    <div class="form-group row">
                                                        <label
                                                            class="col-sm-12 col-form-label">
                                                            PageTitle:</label>
                                                        <div class="col-sm-12">
                                                            <input type="text" class="form-control" id="PageTitle" name="PageTitle" value="<%=item.PageTitle %>" placeholder="nhập vào đây ...">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label
                                                            class="col-sm-12 col-form-label">
                                                            Description:</label>
                                                        <div class="col-sm-12">
                                                            <textarea class="form-control" rows="3" name="PageDescription" placeholder="Nhập nội dung tóm tắt. Tối đa 300 ký tự"><%=item.PageDescription%></textarea>

                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">Page Keyword</label>
                                                        <div class="tags_max_char">
                                                            <input type="text" class="form-control" name="PageKeywords" value="<%=item.PageKeywords %>" />
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>


    <link rel="stylesheet" href="/{CPPath}/interface/utils/chosen/chosen.css" type="text/css" />
    <script type="text/javascript" src="/{CPPath}/interface/utils/chosen/chosen.js"></script>
    <script type="text/javascript">
        //setTimeout(function () { reddevil_exec_cmd('[autosave][<%=model.RecordID%>]') }, 5000);
    </script>
</form>
