﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.CPViewControl" %>

<div class="pcoded-content">
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8 d-none d-lg-block">
                <div class="page-header-title">
                    <i class="icon-social-dropbox"></i>
                    <div class="d-inline">
                        <h5>Quản lý file tải lên</h5>
                        <span>Danh sách File</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class=" breadcrumb breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="/{CPPath}/">
                                <i class="icon-home"></i>
                            </a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="/{CPPath}/<%=CPViewPage.CurrentModule.Code%>/Index.aspx">File đã tải lên</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>


    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="portlet portlet">
                                <div class="portlet-body">
                                    <div class="dataTables_wrapper">
                                        <div class="table-scrollable">

                                            <script type="text/javascript">
                                                function showFileInfo(fileUrl, data) {
                                                    var msg = "The selected URL is: <a href=\"" + fileUrl + "\">" + fileUrl + "</a><br /><br />";
                                                    if (fileUrl !== data["fileUrl"])
                                                        msg += "<b>File url:</b> " + data["fileUrl"] + "<br />";
                                                    msg += "<b>File size:</b> " + data["fileSize"] + "KB<br />";
                                                    msg += "<b>Last modified:</b> " + data["fileDate"];

                                                    this.openMsgDialog("Selected file", msg);
                                                }

                                                var finder = new CKFinder();
                                                finder.basePath = "../";
                                                finder.height = 600;
                                                finder.selectActionFunction = showFileInfo;
                                                finder.create();
                                            </script>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
