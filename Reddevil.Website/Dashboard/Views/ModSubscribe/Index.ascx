﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.CPViewControl" %>

<%
    var model = ViewBag.Model as ModSubscribeModel;
    var listItem = ViewBag.Data as List<ModSubscribeEntity>;

%>
<form id="reddevilForm" name="reddevilForm" method="post">

    <input type="hidden" id="_reddevil_action" name="_reddevil_action" />
    <input type="hidden" id="boxchecked" name="boxchecked" value="0" />

    <div class="page-content-wrapper">
        <h3 class="page-title">Sản phẩm</h3>
        <div class="page-bar justify-content-between">
            <ul class="breadcrumb">
                <li class="breadcrumb-item">
                    <i class="fa fa-home"></i>
                    <a href="/{CPPath}/">Home</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="/{CPPath}/<%=CPViewPage.CurrentModule.Code%>/Index.aspx">Đăng ký nhận báo giá</a>
                </li>
            </ul>
            <div class="page-toolbar">
                <div class="btn-group">
                    <a href="/" class="btn green" target="_blank"><i class="icon-screen-desktop"></i>Xem Website</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <%= ShowMessage()%>

                <div class="portlet portlet">
                    <div class="portlet-title">
                        <div class="actions btn-set">
                            <%=GetTinyListCommand()%>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="dataTables_wrapper">
                            <div class="row hidden-sm hidden-col">
                                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                    <div class="dataTables_search">
                                        <input type="text" class="form-control input-inline input-sm" id="filter_search" value="<%= model.SearchText %>" placeholder="Nhập từ khóa cần tìm" onchange="REDDEVILRedirect();return false;" />
                                        <button type="button" class="btn blue" onclick="REDDEVILRedirect();return false;">Tìm kiếm</button>
                                    </div>
                                </div>
                            </div>

                            <div class="table-scrollable">
                                <table class="table table-striped table-hover table-bordered dataTable">
                                    <thead>
                                        <tr>
                                            <th class="sorting text-center w1p">#</th>
                                            <th class="sorting_disabled text-center w1p">
                                                <label class="itemCheckBox itemCheckBox-sm">
                                                    <input type="checkbox" name="toggle" onclick="checkAll(<%= model.PageSize %>)">
                                                    <i class="check-box"></i>
                                                </label>
                                            </th>

                                            <th class="sorting"><%= GetSortLink("Tên người nhận báo giá", "Name")%></th>
                                            <th class="sorting"><%= GetSortLink("Email nhận báo giá", "Email")%></th>
                                            <th class="sorting"><%= GetSortLink("Phone", "Phone")%></th>
                                            <th class="sorting"><%= GetSortLink("Thành viên đăng nhập", "LoginName")%></th>
                                            <th class="sorting text-center w5p hidden-sm hidden-col"><%= GetSortLink("Sản phẩm cần báo giá", "ProductID")%></th>
                                            <th class="sorting text-center w10p hidden-sm hidden-col"><%= GetSortLink("Shop", "ShopID")%></th>
                                            <th class="sorting text-center w5p hidden-sm hidden-col"><%= GetSortLink("Nhân viên quản lý shop", "AdminID")%></th>
                                            <th class="sorting text-center w10p hidden-sm hidden-col"><%= GetSortLink("Ngày đăng ký", "Published")%></th>
                                            <th class="sorting text-center w10p"><%= GetSortLink("Trạng thái báo giá", "Activity")%></th>
                                            <th class="sorting text-center w1p"><%= GetSortLink("ID", "ID")%></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%for (var i = 0; listItem != null && i < listItem.Count; i++)
                                            { %>
                                        <tr>
                                            <td class="text-center"><%= i + 1%></td>
                                            <td class="text-center">
                                                <%= GetCheckbox(listItem[i].ID, i)%>
                                            </td>
                                            <td>
                                                <a href="javascript:REDDEVILRedirect('Add', <%= listItem[i].ID %>)"><%= listItem[i].Name%></a>
                                            </td>
                                            <td class="text-center hidden-sm hidden-col">
                                                <%=listItem[i].UserID>0?listItem[i].GetUser.Email: listItem[i].Email%>
                                            </td>
                                            <td class="text-center hidden-sm hidden-col">
                                                <%=listItem[i].UserID>0?Utils.FormatPhoneNumber(listItem[i].GetUser.Phone): Utils.FormatPhoneNumber(listItem[i].Phone)%>
                                            </td>
                                            <td class="text-center hidden-sm hidden-col">
                                                <a href="/{CPPath}/ModUser/Add.aspx/RecordID/<%=listItem[i].GetUser.ID %>" target="_blank"><%=listItem[i].GetUser.LoginName%></a>
                                            </td>
                                            <td class="text-center hidden-sm hidden-col">
                                                <a href="/{CPPath}/ModProduct/Add.aspx/RecordID/<%=listItem[i].GetProduct().ID %>" target="_blank"><%=listItem[i].GetProduct().Name%></a>
                                            </td>
                                            <td class="text-center hidden-sm hidden-col"><%=listItem[i].GetShop.Name%>
                                                <p class="smallsub hidden-sm hidden-col">(<span>Url Shop:</span> <a href="<%=CoreMr.Reddevil.Web.HttpRequest.Domain %>/shop/<%= listItem[i].GetShop.Url%><%=Setting.Sys_PageExt %>" target="_blank"><%= listItem[i].GetShop.Url%></a>)</p>
                                            </td>

                                            <td class="text-center hidden-sm hidden-col"><%=listItem[i].GetShop.GetUserAd.Name +"-"+listItem[i].GetShop.GetUserAd.LoginName%>
                                            </td>

                                            <td class="text-center hidden-sm hidden-col"><%= string.Format("{0:dd-MM-yyyy}", listItem[i].Created) %></td>
                                            <td class="text-center">
                                                <%if (listItem[i].Activity)
                                                    { %><a href="javascript:void(0)" title="Đã gửi báo giá"> <span class="fa fa-check-circle publish "></span></a><%}
                                                                                                                                                                      else
                                                                                                                                                                      {%><a href="javascript:void(0)" title="Chưa gửi báo giá"><span class="fa fa-check-circle publish fa fa-dot-circle-o unpublish"></span></a> <%} %>
                                            </td>
                                            <td class="text-center"><%= listItem[i].ID%></td>
                                        </tr>
                                        <%} %>
                                    </tbody>
                                </table>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 justify-content-center d-flex ">
                                    <%= GetPagination(model.PageIndex, model.PageSize, model.TotalRecord)%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        window.document.title = 'Quản lý đăng ký nhận báo giá';
        var REDDEVILController = "ModSubscribe";

        var REDDEVILArrVar = [
            "filter_lang", "LangID",
            "limit", "PageSize"
        ];

        var REDDEVILArrVar_QS = [
            "filter_search", "SearchText"
        ];

        var REDDEVILArrQT = [
            "<%= model.PageIndex + 1 %>", "PageIndex",
            "<%= model.Sort %>", "Sort"
        ];

        var REDDEVILArrDefault = [
            "1", "PageIndex",
            "1", "LangID",
            "20", "PageSize"
        ];

    </script>

</form>
