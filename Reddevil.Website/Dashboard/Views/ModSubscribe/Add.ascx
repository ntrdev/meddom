﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.CPViewControl" %>

<%
    var model = ViewBag.Model as ModSubscribeModel;
    var item = ViewBag.Data as ModSubscribeEntity;
%>

<form id="reddevilForm" name="reddevilForm" method="post">
    <input type="hidden" id="_reddevil_action" name="_reddevil_action" />

    <div class="page-content-wrapper">
        <h3 class="page-title">Báo giá <small><%= model.RecordID > 0 ? "Chỉnh sửa": "Thêm mới"%></small></h3>
        <div class="page-bar justify-content-between">
            <ul class="breadcrumb">
                <li class="breadcrumb-item">
                    <i class="fa fa-home"></i>
                    <a href="/{CPPath}/">Home</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="/{CPPath}/<%=CPViewPage.CurrentModule.Code%>/Index.aspx">Báo giá</a>
                </li>
            </ul>
            <div class="page-toolbar">
                <div class="btn-group">
                    <a href="/" class="btn green" target="_blank"><i class="icon-screen-desktop"></i>Xem Website</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <%= ShowMessage()%>

                <div class="form-horizontal form-row-seperated">
                    <div class="portlet">
                        <div class="portlet-title">
                            <div class="caption"></div>
                            <div class="actions btn-set">
                                <%= GetDefaultAddCommand()%>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="portlet box blue-steel">
                                        <div class="portlet-title">
                                            <div class="caption">Thông tin chung</div>
                                        </div>
                                        <div class="portlet-body">
                                            <div class="form-body">
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-right">Tên người nhận:</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" name="Name" disabled value="<%=item.Name %>" />
                                                    </div>
                                                </div>
                                                <%if (CPViewPage.UserPermissions.Full)
                                                    {%>

                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-right">Báo giá:</label>
                                                    <div class="col-md-9">
                                                        <label class="radioPure radio-inline" style="float: left; width: 10%">
                                                            <input type="radio" name="Activity" <%= item.Activity ? "checked": "" %> value="1" />
                                                            <span class="outer"><span class="inner"></span></span><i>Đã báo giá</i>
                                                        </label>
                                                        <label class="radioPure radio-inline" style="float: left; width: 10%">
                                                            <input type="radio" name="Activity" <%= !item.Activity ? "checked": "" %> value="0" />
                                                            <span class="outer"><span class="inner"></span></span><i>Chưa báo giá</i>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-right">Sản phẩm cần báo giá:</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" name="Phone" disabled="" value="<%=item.GetProduct().Name +" -> SKU: "+item.GetProduct().Sku %>" />
                                                        <a style="color:blue" href="<%=CoreMr.Reddevil.Web.HttpRequest.Domain+"/"+item.GetProduct().Url+Setting.Sys_PageExt %>" target="_blank"><%=item.GetProduct().Name %></a>
                                                    </div>
                                                </div>
                                                <%} %>
                                                <%if (item.UserID > 0)
                                                    { %>
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-right">Thông tin thành viên đăng ký:</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" disabled="" value="<%=item.GetUser.LoginName %>" />
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-right">Email nhận báo giá:</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" name="Email" value="<%=item.GetUser.Email %>" />
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-right">Số điện thoại:</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" name="Phone" value="<%=item.GetUser.Phone %>" />
                                                    </div>
                                                </div>

                                                <%}
                                                    else
                                                    {%>
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-right">Thông tin người nhận báo giá:</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" disabled="" value="<%=item.Name %>" />
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-right">Email nhận báo giá:</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" name="Email" disabled="" value="<%=item.Email %>" />
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-right">Số điện thoại:</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" name="Phone" disabled="" value="<%=item.Phone %>" />
                                                    </div>
                                                </div>

                                                <%} %>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</form>
