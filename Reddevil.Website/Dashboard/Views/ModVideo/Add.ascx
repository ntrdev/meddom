﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.CPViewControl" %>

<%
    var model = ViewBag.Model as ModVideoModel;
    var item = ViewBag.Data as ModVideoEntity;
%>

<form id="vswForm" name="vswForm" method="post">
    <input type="hidden" id="_vsw_action" name="_vsw_action" />

    <div id="toolbar-box">
        <div class="t">
            <div class="t">
                <div class="t"></div>
            </div>
        </div>
        <div class="m">
            <div class="toolbar-list" id="toolbar">
                <%= GetDefaultAddCommand()%>
            </div>
            <div class="pagetitle icon-48-mediamanager">
                <h2>Video : <%=  model.RecordID > 0 ? "Chỉnh sửa" : "Thêm mới"%></h2>
            </div>
            <div class="clr"></div>
        </div>
        <div class="b">
            <div class="b">
                <div class="b"></div>
            </div>
        </div>
    </div>
    <div class="clr"></div>

    <%= ShowMessage()%>

    <div id="element-box">
        <div class="t">
            <div class="t">
                <div class="t"></div>
            </div>
        </div>
        <div class="m">
            <div class="col width-100">
                <table class="admintable">
                    <tr>
                        <td class="key">
                            <label>Tiêu đề:</label>
                        </td>
                        <td>
                            <input class="text_input" type="text" name="Name" value="<%=item.Name %>" maxlength="255" />
                        </td>
                    </tr>
                    <tr>
                        <td class="key">
                            <label>Mã URL:</label>
                        </td>
                        <td>
                            <input class="text_input" type="text" name="Code" value="<%=item.Code %>" maxlength="255" />
                        </td>
                    </tr>
                    <tr>
                        <td class="key">
                            <label>Đường dẫn <a href="http://www.youtube.com/" target="_blank">Youtube.com</a> :</label>
                        </td>
                        <td>
                            <input class="text_input" type="text" name="File" value="<%=item.File %>" maxlength="255" />
                        </td>
                    </tr>
                    <tr>
                        <td class="key">&nbsp;
                        </td>
                        <td>
                            <b style="font-style: italic; color: #878787;">Up file video lên <a href="http://www.youtube.com/" target="_blank">Youtube.com</a>, sau đó copy đường dẫn và dán vào đây.</b>
                        </td>
                    </tr>
                    <tr>
                        <td class="key">
                            <label>Mô tả:</label>
                        </td>
                        <td>
                            <textarea class="text_input" style="height: 150px" name="Summary"><%=item.Summary%></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td class="key">
                            <label>Chuyên mục :</label>
                        </td>
                        <td>
                            <select name="MenuID" id="MenuID" class="text_input">
                                <option value="0">Root</option>
                                <%= Utils.ShowDdlMenuByType("Video", model.LangID, item.MenuID)%>
                            </select>
                        </td>
                    </tr>
                    <%if (CPViewPage.UserPermissions.Approve){%>
                    <tr>
                        <td class="key">
                            <label>Duyệt :</label>
                        </td>
                        <td>
                            <input name="Activity" <%= item.Activity ? "checked" : "" %> type="radio" value="1" />
                            Có
                            <input name="Activity" <%= !item.Activity ? "checked" : "" %> type="radio" value="0" />
                            Không
                        </td>
                    </tr>
                    <%} %>
                </table>
            </div>
            <div class="clr"></div>
        </div>
        <div class="b">
            <div class="b">
                <div class="b"></div>
            </div>
        </div>
    </div>
</form>