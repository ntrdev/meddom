﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.CPViewControl" %>

<%
    var model = ViewBag.Model as ModAdvPageModel;
    var item = ViewBag.Data as ModAdvPageEntity;
%>

<form id="reddevilForm" name="reddevilForm" method="post">
    <input type="hidden" id="_reddevil_action" name="_reddevil_action" />


    <div class="page-content">
        <div class="breadcrumbs">
            <h1><%= model.RecordID > 0 ? "Cập nhật banner quảng cáo" : "Thêm mới "%></h1>
            <ol class="breadcrumb">
                <li>
                    <a href="/{CPPath}/"><i class="fa fa-home"></i>Home</a>
                </li>
                <li class="active"><a href="/{CPPath}/<%=CPViewPage.CurrentModule.Code%>/Index.aspx">Ảnh - Banner Quảng Cáo</a></li>
            </ol>
        </div>
        <div class="row">
            <div class="col-md-12">
                <%= ShowMessage()%>
                <div class="control_heading">
                    <div class="btn-group btn-group-circle">
                        <%=GetDefaultAddCommand() %>
                    </div>
                </div>
            </div>
        </div>
        <div class="row pagepost">

            <div class="col-sm-9">

                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-green-sharp">
                            <i class="icon-speech font-green-sharp"></i>
                            <span class="caption-subject bold uppercase">Thông tin chung</span>
                            <span class="caption-helper">thông tin chèn ảnh</span>
                        </div>
                        <div class="actions">
                            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"></a>
                        </div>
                    </div>
                    <div class="portlet-body">

                        <div class="form-body">
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input ">
                                        <input type="text" class="form-control" id="Name" name="Name" value="<%=item.Name %>" placeholder="nhập vào đây ...">
                                        <label for="form_control_1">Tên ảnh:</label>
                                        <span class="help-block">Ký tự tối đa 200</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input ">
                                        <input type="text" class="form-control" id="Url" value="<%=item.URL %>" placeholder="Copy link dán vào đây...">
                                        <label for="form_control_1">URL trình duyệt chèn vào ảnh:</label>
                                        <span class="help-block">ví dụ: san-pham-tai-nghe-cao-cap</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-md-line-input ">
                                        <input type="text" onclick="this.type = 'number'" class="form-control" id="Width" name="Width" value="<%=item.Width %>" placeholder="nhập vào đây ...">
                                        <label for="form_control_1">Chiều rộng ảnh:</label>
                                        <span class="help-block">Ví dụ: 20 (px)</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-md-line-input ">
                                        <input type="text" onclick="this.type = 'number'" class="form-control" id="Height" name="Height" value="<%=item.Height %>" placeholder="nhập vào đây...">
                                        <label for="form_control_1">Chiều cao ảnh:</label>
                                        <span class="help-block">Ví dụ: 20 (px)</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-md-line-input ">
                                        <input type="text" class="form-control" id="AddInTag" name="AddInTag" value="<%=item.AddInTag%>" placeholder="nhập vào đây...">
                                        <label for="form_control_1">Chèn thêm:</label>

                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-md-line-input ">
                                        <select class="form-control" name="Target">
                                            <option value=""></option>
                                            <option <%if (item.Target == "_blank")
                                                {%>selected<%} %>
                                                value="_blank">_blank</option>
                                            <option <%if (item.Target == "_parent")
                                                {%>selected<%} %>
                                                value="_parent">_parent</option>
                                        </select>
                                        <label for="multiple" class="control-label">Target(mở link):</label>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group form-md-line-input ">
                                        <select class="form-control chosen" name="MenuID" id="MenuID" onchange="GetProperties(this.value)">
                                            <%= Utils.ShowDdlMenuByType("Product", model.LangID, item.MenuID)%>
                                        </select>
                                        <label for="multiple" class="control-label">Chọn chuyên mục:</label>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-green-sharp">
                            <i class="icon-speech font-green-sharp"></i>
                            <a class="titleCollap collapsed" data-toggle="collapse" href="#ctr-advance" aria-expanded="false"><span class="caption-subject bold uppercase">Hình ảnh Quảng cáo</span></a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="ItemControl">

                                <div id="ctr-advance" class="panel-collapse collapse" style="height: 0px;" aria-expanded="false">
                                    <div class="panel-body">
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input class="input-file" type="button" id="imgInp" onclick="ShowFileForm('File'); return false">
                                                <label tabindex="0" for="my-file" class="input-file-trigger text-center">
                                                    <i class="icon-cloud-upload"></i>&nbsp;Chọn ảnh QC</label>
                                                <img id="show_img_upload" src="<%=Utils.DefaultImage(item.File) %>" />
                                                <input type="hidden" class="form-control" name="File" id="File" value="<%=item.File %>" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-green-sharp">
                            <i class="icon-speech font-green-sharp"></i>
                            <span class="caption-subject bold uppercase">Thuộc tính</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-12">
                                <label>Hiển thị tại trang con</label>
                                <div class="md-radio-inline">
                                    <div class="md-radio">
                                        <input type="radio" id="OnDetail1" name="OnDetail" value="1" class="md-radiobtn" <%=item.OnDetail?"checked":"" %>>
                                        <label for="OnDetail1">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span>Cho phép hiển thị 
                                        </label>
                                    </div>
                                    <div class="md-radio has-error">
                                        <input type="radio" id="OnDetail2" name="OnDetail" value="0" class="md-radiobtn" <%=!item.OnDetail?"checked":"" %>>
                                        <label for="OnDetail2">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span>Không hiển thị
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <%if (CPViewPage.UserPermissions.Approve)
                                {%>
                            <div class="col-md-12">
                                <label>Trạng thái</label>
                                <div class="md-radio-inline">
                                    <div class="md-radio">
                                        <input type="radio" id="activity1" name="Activity" value="1" class="md-radiobtn" <%=item.Activity?"checked":"" %>>
                                        <label for="activity1">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span>Duyệt
                                        </label>
                                    </div>
                                    <div class="md-radio has-error">
                                        <input type="radio" id="activity2" name="Activity" value="0" class="md-radiobtn" <%=!item.Activity?"checked":"" %>>
                                        <label for="activity2">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span>Không duyệt
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <%} %>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>


    

</form>
<link rel="stylesheet" href="/{CPPath}/interface/utils/chosen/chosen.css" type="text/css" media="all" />
<script type="text/javascript" src="/{CPPath}/interface/utils/chosen/chosen.js"></script>
<script type="text/javascript">

    $('.chosen').chosen({
        search_contains: true,
        no_results_text: 'Không tìm thấy kết quả phù hợp'
    });
</script>
