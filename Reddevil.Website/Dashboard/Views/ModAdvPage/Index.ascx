﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.CPViewControl" %>

<%
    var model = ViewBag.Model as ModAdvPageModel;
    var listItem = ViewBag.Data as List<ModAdvPageEntity>;
%>

<form id="reddevilForm" name="reddevilForm" method="post">

    <input type="hidden" id="_reddevil_action" name="_reddevil_action" />
    <input type="hidden" id="boxchecked" name="boxchecked" value="0" />
    <div class="page-content">
        <div class="breadcrumbs">
            <h1>Quản lý ảnh - banner Quảng cáo</h1>
            <ol class="breadcrumb">
                <li><a href="/{CPPath}/"><i class="fa fa-home"></i>Home</a></li>
                <li class="active"><a href="/{CPPath}/<%=CPViewPage.CurrentModule.Code%>/Index.aspx">Ảnh - Banner</a></li>
            </ol>
        </div>
        <div class="row">
            <div class="col-md-12">
                <%= ShowMessage()%>
                <div class="control_heading">
                    <div class="btn-group btn-group-circle">
                        <%=GetDefaultListCommand() %>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject bold uppercase">Ảnh - banner</span>
                        </div>
                        <div class="tools col-md-9">
                            <div class="table-group-actions d-inline-block col-md-4 col-xs-12">
                                <select id="filter_menu" class="form-control input-inline input-sm chosen_menu" onchange="REDDEVILRedirect()" size="1">
                                    <option value="0">Lọc theo chuyên mục</option>
                                    <%= Utils.ShowDdlMenuByType("Product", model.LangID, model.MenuID)%>
                                </select>

                            </div>


                            <div class="table-group-actions d-inline-block col-md-2 col-xs-6">
                                <%= ShowDDLLang(model.LangID)%>
                            </div>
                            <div class="dataTables_search col-md-3 col-xs-12">
                                <input type="text" class="form-control input-inline input-sm" id="filter_search" style="float: left; width: 100%; padding-top: 1px; position: relative; background: #fff; height: 32px; text-indent: 10px; font-size: 14px; border-radius: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px;"
                                    value="<%= model.SearchText %>" placeholder="Nhập từ khóa cần tìm" onchange="REDDEVILRedirect();return false;" />
                                <button type="submit" class="btntop" onclick="REDDEVILRedirect();return false;" style="float: right; width: 40px; height: 32px; border: 0; cursor: pointer; background: none; position: absolute; right: 15px;">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="dt-responsive table-responsive">
                            <table id="" class="table_basic table table-hover m-b-0 " style="width: 100%">
                                <thead>
                                    <tr>
                                        <th class="sorting text-center w1p">STT</th>
                                        <th class="sorting_disabled text-center w1p">
                                            <div class="md-checkbox">
                                                <input type="checkbox" id="checks" name="toggle" onclick="checkAll('<%=model.PageSize%>')" class="md-check">
                                                <label for="checks">
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span>Chọn
                                                </label>
                                            </div>
                                        </th>
                                        <th class="sorting"><%= GetSortLink("Tên ", "Name")%></th>
                                        <th class="sorting text-center w10p hidden-sm hidden-col"><%= GetSortLink("Ảnh", "File")%></th>
                                        <th class="sorting text-center w1p hidden-sm hidden-col"><%= GetSortLink("Link chèn(Url)", "Url")%></th>
                                        <th class="sorting text-center w10p hidden-sm hidden-col"><%= GetSortLink("Chiều rộng", "Width")%></th>
                                        <th class="sorting text-center w10p hidden-sm hidden-col"><%= GetSortLink("Chiều cao", "Height")%></th>
                                        <th class="sorting text-center w10p hidden-sm hidden-col"><%= GetSortLink("Chuyên mục", "MenuID")%></th>
                                        <th class="sorting text-center w1p"><%= GetSortLink("On Detail", "OnDetail")%></th>
                                        <th class="text-center">Trạng thái</th>
                                        <th style="width: 110px">Sắp xếp vị trí</th>
                                        <th class="text-center">Tùy chọn</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <%for (var i = 0; listItem != null && i < listItem.Count; i++)
                                        { %>
                                    <tr>
                                        <td class="text-center"><%= i + 1%></td>
                                        <td class="text-center">
                                            <%= GetCheckbox(listItem[i].ID, i)%>
                                        </td>
                                        <td>
                                            <a href="javascript:REDDEVILRedirect('Add', <%= listItem[i].ID %>)"><%= listItem[i].Name%></a>
                                        </td>
                                        <td class="text-center hidden-sm hidden-col">
                                            <%= Utils.GetMedia(listItem[i].File, 40, 40)%>
                                        </td>
                                        <td class="text-center hidden-sm hidden-col"><%= listItem[i].URL %></td>
                                        <td class="text-center hidden-sm hidden-col"><%= string.Format("{0:#,##0}", listItem[i].Width) %> px</td>
                                        <td class="text-center hidden-sm hidden-col"><%= string.Format("{0:#,##0}", listItem[i].Height) %> px</td>
                                        <td class="text-center hidden-sm hidden-col"><%= GetName(listItem[i].GetMenu()) %></td>
                                        <td class="text-center"><%= GetOnDetail(listItem[i].ID, listItem[i].OnDetail)%></td>
                                        <td class="text-center"><%= GetPublish(listItem[i].ID, listItem[i].Activity)%></td>
                                        <td class="text-center"><%= GetOrder(listItem[i].ID, listItem[i].Order)%></td>
                                        <td class="text-center">
                                            <div class="control-button">
                                                <button data-toggle="tooltip" data-original-title="Chỉnh sửa" onclick="REDDEVILRedirect('Add', <%= listItem[i].ID %>)" type="button" class="btn waves-effect waves-light btn-primary btn-icon"><i class="icon-pencil"></i></button>
                                                <button class="btn waves-effect waves-light btn-danger btn-icon" data-toggle="tooltip" data-original-title="ID" type="button"><%=listItem[i].ID %></button>
                                            </div>
                                        </td>
                                    </tr>
                                    <%} %>
                                </tbody>
                            </table>
                        </div>
                        <div class="row center">
                            <div class="col-md-12 col-sm-12 justify-content-center d-flex ">
                                <%= GetPagination(model.PageIndex, model.PageSize, model.TotalRecord)%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
    <link rel="stylesheet" href="/{CPPath}/interface/utils/chosen/chosen.css" type="text/css" media="all" />
    <script type="text/javascript" src="/{CPPath}/interface/utils/chosen/chosen.js"></script>
    <script type="text/javascript">

        var REDDEVILController = "ModAdvPage";

        var REDDEVILArrVar = [
            "filter_menu", "MenuID",
            "filter_lang", "LangID",
            "limit", "PageSize"
        ];
        var REDDEVILArrVar_QS = [
            "filter_search", "SearchText"
        ];

        var REDDEVILArrQT = [
            "<%= model.PageIndex + 1 %>", "PageIndex",
            "<%= model.Sort %>", "Sort"
        ];

        var REDDEVILArrDefault = [
            "1", "PageIndex",
            "1", "LangID",
            "20", "PageSize"
        ];
        $('.chosen_menu').chosen({

            display_selected_options: true,
            placeholder_text_multiple: 'Gõ từ khóa cần tìm',
            no_results_text: 'Không có kết quả',
            enable_split_word_search: true,
            search_contains: true,
            display_disabled_options: true,
            single_backstroke_delete: false,
            inherit_select_classes: true
        });
    </script>

</form>
