﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.CPViewControl" %>

<%
    var model = ViewBag.Model as SysPageModel;
    var item = ViewBag.Data as SysPageEntity;

    var listTemplate = SysTemplateService.Instance.CreateQuery()
                                        .Where(o => o.LangID == model.LangID && o.Device == 0)
                                        .OrderByAsc(o => o.Order)
                                        .ToList();

    var listTemplateMobile = SysTemplateService.Instance.CreateQuery()
                                        .Where(o => o.LangID == model.LangID && o.Device == 1)
                                        .OrderByAsc(o => o.Order)
                                        .ToList();

    var listTemplateTablet = SysTemplateService.Instance.CreateQuery()
                                        .Where(o => o.LangID == model.LangID && o.Device == 2)
                                        .OrderByAsc(o => o.Order)
                                        .ToList();

    var listModule = Reddevil.Lib.Web.Application.Modules.Where(o => o.IsControl == false).OrderBy(o => o.Order).ToList();

    var listParent = Reddevil.Lib.Global.ListItem.List.GetList(SysPageService.Instance, model.LangID);

    if (model.RecordID > 0)
    {
        //loai bo danh muc con cua danh muc hien tai
        listParent = Reddevil.Lib.Global.ListItem.List.GetListForEdit(listParent, model.RecordID);
    }


%>

<form id="reddevilForm" name="reddevilForm" method="post">
    <input type="hidden" id="_reddevil_action" name="_reddevil_action" />

    <div class="pcoded-content">
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8 d-none d-lg-block">
                    <div class="page-header-title">
                        <i class="icon-social-dropbox"></i>
                        <div class="d-inline">
                            <h5>Quản lý Trang(Menu)</h5>
                            <span><%= model.RecordID > 0 ? "Cập nhật trang "+item.Name : "Thêm mới trang"%></span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class=" breadcrumb breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="/{CPPath}/">
                                    <i class="icon-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="/{CPPath}/<%=CPViewPage.CurrentModule.Code%>/Index.aspx">Trang(Menu)</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <%= ShowMessage()%>
                                </div>
                            </div>
                            <div class="col-sm-9">
                                <div class="card content-editor">
                                    <div class="card-header header-control">
                                        <h5><%= model.RecordID > 0 ? "Cập nhật trang "+item.Name : "Thêm mới trang"%></h5>
                                        <div class="controler-header">
                                            <%=GetDefaultAddCommand() %>
                                        </div>
                                    </div>
                                    <div class="card-block pdt-0">
                                        <div class="tabs-scroll">
                                            <ul class="nav nav-tabs md-tabs " role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link active" data-toggle="tab"
                                                        href="#thongtinchung" role="tab"><i class="icon-info"></i>Thông tin chung</a>
                                                    <div class="slide"></div>
                                                </li>

                                            </ul>
                                        </div>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="thongtinchung"
                                                role="tabpanel">
                                                <h4 class="sub-title">Thông tin trang</h4>
                                                <div class="edit-margin-mb form-manager">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <div class="col-sm-12">
                                                                        <label class="col-sm-12 col-form-label">Tên Menu:</label>
                                                                        <input type="text" class="form-control" id="Name" name="Name" value="<%=item.Name %>" placeholder="nhập vào đây, tối đa 200 ký tự ...">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <%if (model.ParentID > 0)
                                                            {%>

                                                        <div class="col-md-6">
                                                            <div class="form-group form-md-line-input ">
                                                                <div class="row">
                                                                    <label class="col-sm-12 col-form-label">Mẫu giao diện PC:</label>
                                                                    <select class="form-control" name="TemplateID">
                                                                        <option value="0"></option>
                                                                        <%for (var i = 0; listTemplate != null && i < listTemplate.Count; i++)
                                                                            { %>
                                                                        <option <%if (item.TemplateID == listTemplate[i].ID)
                                                                            {%>selected<%} %>
                                                                            value="<%= listTemplate[i].ID%>">&nbsp; <%= listTemplate[i].Name%></option>
                                                                        <%} %>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <%if (Setting.Sys_Mobile == true)
                                                            { %>
                                                        <div class="col-md-6">
                                                            <div class="form-group form-md-line-input ">
                                                                <div class="row">
                                                                    <label class="col-sm-12 col-form-label">Mẫu giao diện Mobile:</label>
                                                                    <select class="form-control" name="TemplateMobileID">
                                                                        <option value="0"></option>
                                                                        <%for (var i = 0; listTemplateMobile != null && i < listTemplateMobile.Count; i++)
                                                                            { %>
                                                                        <option <%if (item.TemplateMobileID == listTemplateMobile[i].ID)
                                                                            {%>selected<%} %>
                                                                            value="<%= listTemplateMobile[i].ID%>">&nbsp; <%= listTemplateMobile[i].Name%></option>
                                                                        <%} %>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <%} %>
                                                        <div class="col-md-6">
                                                            <div class="form-group form-md-line-input ">
                                                                <div class="row">
                                                                    <label class="col-sm-12 col-form-label">Chức năng:</label>
                                                                    <select class="form-control" name="ModuleCode" id="ModuleCode" onchange="page_control_change(this.value)">
                                                                        <option value="0"></option>
                                                                        <%for (var i = 0; i < listModule.Count; i++)
                                                                            { %>
                                                                        <option <%if (item.ModuleCode == listModule[i].Code)
                                                                            {%>selected<%} %>
                                                                            value="<%= listModule[i].Code%>">&nbsp; <%= listModule[i].Name%></option>
                                                                        <%} %>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group form-md-line-input ">
                                                                <div class="row" id="list_menu"></div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group form-md-line-input " id="control_param">
                                                            </div>

                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group form-md-line-input ">
                                                                <div class="row">
                                                                    <label for="multiple" class="control-label">Trang cha:</label>
                                                                    <select class="form-control" name="ParentID">
                                                                        <option value="0">Chọn trang cha</option>
                                                                        <%for (var i = 0; listParent != null && i < listParent.Count; i++)
                                                                            { %>
                                                                        <option <%if (item.ParentID.ToString() == listParent[i].Value)
                                                                            {%>selected<%} %>
                                                                            value="<%= listParent[i].Value%>">&nbsp; <%= listParent[i].Name%></option>
                                                                        <%} %>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <%} %>

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <label class="col-sm-12 col-form-label">
                                                                        Mã thiết kế
                                                                    </label>
                                                                    <div class="col-sm-12">
                                                                        <div class="plugin-ckEditor">
                                                                            <textarea class="form-control" rows="3" name="Custom" id="Custom" placeholder="Mã thiết kế"><%=item.Custom %></textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <label class="col-sm-12 col-form-label">
                                                                        Tóm tắt
                                                                    </label>
                                                                    <div class="col-sm-12">
                                                                        <div class="plugin-ckEditor">
                                                                            <textarea class="form-control" rows="3" name="Summary" placeholder="Mô tả tóm tắt"><%=item.Summary %></textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <label class="col-sm-12 col-form-label">
                                                                        Nội dung chi tiết
                                                                    </label>
                                                                    <div class="col-sm-12">
                                                                        <div class="plugin-ckEditor">
                                                                            <textarea class="form-control ckeditor" name="Content" placeholder=""><%=item.Content %></textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="block-controlRight">
                                    <div class="ItemControl">
                                        <a class="titleCollap" data-toggle="collapse"
                                            href="#ctr-trangthai">Trạng thái và hiển thị</a>
                                        <div id="ctr-trangthai" class="panel-collapse collapse show">
                                            <div class="panel-body">
                                                <ul class="form-block">
                                                    <li>
                                                        <div>Hiển thị</div>
                                                        <div class="editorBox">
                                                            <%=GetHideOrShow(item.ID, item.Activity) %>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div>Ngày tạo</div>
                                                        <div class="editorBox">
                                                            <span><%=Utils.FormatDateTime(item.Created) %></span>
                                                        </div>
                                                    </li>
                                                    <li class="blockform">
                                                        <div>Đường dẫn URL</div>
                                                        <div class="editorBox">
                                                            <input type="text" class="form-block" id="Url" name="Url" value="<%=item.Url %>" placeholder="Nếu không nhập sẽ tự sinh theo Tiêu đề">
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="ItemControl">
                                        <a class="titleCollap" data-toggle="collapse" href="#ctr-advance">Ảnh đại diện</a>
                                        <div id="ctr-advance" class="panel-collapse collapse" style="height: 0px;" aria-expanded="false">
                                            <div class="panel-body">
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <input class="input-file" type="button" id="imgInp" onclick="ShowFileForm('File'); return false">
                                                        <label tabindex="0" for="my-file" class="input-file-trigger text-center">
                                                            <i class="icon-cloud-upload"></i>&nbsp;Chọn ảnh đại diện</label>
                                                        <img id="show_img_upload" src="<%=Utils.OptimalImage(item.File) %>" />
                                                        <input type="hidden" class="form-control" name="File" id="File" value="<%=item.File %>" />
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="ItemControl">
                                        <a class="titleCollap" data-toggle="collapse"
                                            href="#ctr-seo-editor">SEO Editor</a>
                                        <div id="ctr-seo-editor" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <div class="card-block">


                                                    <div class="form-group row">
                                                        <label
                                                            class="col-sm-12 col-form-label">
                                                            PageTitle:</label>
                                                        <div class="col-sm-12">
                                                            <input type="text" class="form-control" id="PageTitle" name="PageTitle" value="<%=item.PageTitle %>" placeholder="nhập vào đây ...">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label
                                                            class="col-sm-12 col-form-label">
                                                            PageTitle thẻ H1:</label>
                                                        <div class="col-sm-12">
                                                            <input type="text" class="form-control" id="PageHeading" name="PageHeading" value="<%=item.PageHeading %>" placeholder="nhập vào đây ...">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label
                                                            class="col-sm-12 col-form-label">
                                                            Description:</label>
                                                        <div class="col-sm-12">
                                                            <textarea class="form-control" rows="3" name="PageDescription" placeholder="Nhập nội dung tóm tắt. Tối đa 300 ký tự"><%=item.PageDescription%></textarea>

                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">Page Keyword</label>
                                                        <div class="tags_max_char">
                                                            <input type="text" class="form-control" name="PageKeywords" value="<%=item.PageKeywords %>" />
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <link rel="stylesheet" href="/{CPPath}/interface/utils/chosen/chosen.css" type="text/css" media="all" />
    <script type="text/javascript" src="/{CPPath}/interface/utils/chosen/chosen.js"></script>
    <script type="text/javascript">
        function menu_change(name) {
            var txtPageName = document.getElementById('Name');
            if (txtPageName.value === '') {
                var i = name.indexOf('---- ');
                if (i > -1)
                    txtPageName.value = name.substr(i + 5);
            }
        }

        function page_control_change(controlID) {
            var ranNum = Math.floor(Math.random() * 999999);
            var dataString = 'LangID=<%= model.LangID%>&PageID=<%=item.ID %>&ModuleCode=' + controlID + '&rnd=' + ranNum;
            $.ajax({
                url: '/{CPPath}/Ajax/PageGetControl/',
                type: 'get',
                data: dataString,
                dataType: 'json',
                success: function (data) {
                    var content = data.Html;
                    var listMenu = data.Params;
                    var listBrand = data.Js;

                    listMenu = '<label class="col-sm-12 col-form-label">Liên kết Chuyên mục:</label ><select class="form-control chosen" name="MenuID" onchange="menu_change(this.options[this.selectedIndex].text)"><option value="0">Chuyên mục</option>' + listMenu + '</select>';
                    listBrand = '<select class="form-control" name="BrandID"><option value="0">Root</option>' + listBrand + '</select>';

                    $('#control_param').html(content);
                    $('#list_menu').html(listMenu);
                    $('#list_brand').html(listBrand);
                    $('.chosen').chosen({
                        search_contains: true,
                        no_results_text: 'Không tìm thấy kết quả phù hợp'
                    });

                    if (controlID != 'MProduct')
                        $('.brand-block').hide();
                    else
                        $('.brand-block').show();

                    //window.setTimeout('CKEditorInstance()', 100);
                },
                error: function () { }
            });
        }

        //if ('<%=item.ModuleCode%>' !== '') page_control_change('<%= item.ModuleCode %>');
        if ($('#ModuleCode').val() !== '') page_control_change($('#ModuleCode').val());
    </script>

</form>
