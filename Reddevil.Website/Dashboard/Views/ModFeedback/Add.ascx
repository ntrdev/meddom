﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.CPViewControl" %>

<%
    var model = ViewBag.Model as ModFeedbackModel;
    var item = ViewBag.Data as ModFeedbackEntity;
%>

<form id="reddevilForm" name="reddevilForm" method="post">

    <input type="hidden" id="_reddevil_action" name="_reddevil_action" />





    <div class="pcoded-content">
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8 d-none d-lg-block">
                    <div class="page-header-title">
                        <i class="fa fa-search"></i>
                        <div class="d-inline">
                            <h5>Quản lý Phản hồi</h5>
                            <span><%= model.RecordID > 0 ? "Xem phản hồi" : "Thêm mới bài viết"%></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="page-header-breadcrumb">
                                    <ul class=" breadcrumb breadcrumb-title">
                                        <li class="breadcrumb-item">
                                            <a href="/{CPPath}/">
                                                <i class="icon-home"></i>
                                            </a>

                                        </li>
                                        <li class="breadcrumb-item">
                                            <i class="fa fa-chevron-right"></i>
                                            <a href="/{CPPath}/<%=CPViewPage.CurrentModule.Code%>/Index.aspx">Phản hồi</a>
                                        </li>
                                        <li class="breadcrumb-item">
                                            <i class="fa fa-chevron-right"></i>
                                            <a href="/{CPPath}/<%=CPViewPage.CurrentModule.Code%>/Add.aspx"><%= model.RecordID > 0 ? "Xem phản hồi từ "+item.Name : "Thêm mới bài viết"%></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="card">
                                    <%= ShowMessage()%>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="card content-editor">
                                    <div class="card-header header-control">
                                        <h5>Thêm mới bài viết</h5>
                                        <div class="controler-header">
                                            <%=GetTinyAddCommand() %>
                                        </div>
                                    </div>
                                    <div class="card-block pdt-0">

                                        <div class="tab-content">
                                            <div class="tab-pane active" id="thongtinchung"
                                                role="tabpanel">
                                                <h4 class="sub-title">Thông tin phản hồi</h4>
                                                <div class="edit-margin-mb form-manager">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <label class="col-sm-12 col-form-label">Họ và Tên</label>
                                                                    <div class="col-sm-12">
                                                                        <input type="text" class="form-control" id="Name" name="Name" value="<%=item.Name %>" placeholder="nhập vào đây, tối đa 200 ký tự ...">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <label class="col-sm-12 col-form-label">Số điện thoại</label>
                                                                    <div class="col-sm-12">
                                                                        <input type="text" class="form-control" id="Phone" readonly value="<%=item.Phone %>" placeholder="nhập vào đây, tối đa 200 ký tự ...">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <label class="col-sm-12 col-form-label">Địa chỉ Email</label>
                                                                    <div class="col-sm-12">
                                                                        <input type="text" class="form-control" readonly value="<%=item.Email %>" placeholder="nhập vào đây, tối đa 200 ký tự ...">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <label class="col-sm-12 col-form-label">Địa chỉ</label>
                                                                    <div class="col-sm-12">
                                                                        <input type="text" class="form-control" readonly value="<%=item.Address %>" placeholder="">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <label class="col-sm-12 col-form-label">Nội dung phản hồi</label>
                                                                    <div class="col-sm-12">
                                                                        <textarea class="form-control" rows="3" name="Content" placeholder="Nội dung yêu cầu"><%=item.Content %></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

</form>
