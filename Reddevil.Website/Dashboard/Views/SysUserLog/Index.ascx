﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Reddevil.Lib.MVC.CPViewControl" %>

<%
    var model = ViewBag.Model as SysUserLogModel;
    var listItem = ViewBag.Data as List<CPUserLogEntity>;
%>

<form id="reddevilForm" name="reddevilForm" method="post">

    <input type="hidden" id="_reddevil_action" name="_reddevil_action" />
    <input type="hidden" id="boxchecked" name="boxchecked" value="0" />

    <div class="pcoded-content">
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8 d-none d-lg-block">
                    <div class="page-header-title">
                        <i class="icon-social-dropbox"></i>
                        <div class="d-inline">
                            <h5>Nhật ký quản trị</h5>
                            <span>Danh sách nhật ký</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class=" breadcrumb breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="/{CPPath}/">
                                    <i class="icon-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="/{CPPath}/<%=CPViewPage.CurrentModule.Code%>/Index.aspx">Nhật ký quản trị</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>



        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <%= ShowMessage()%>
                                </div>

                                <div class="card">
                                    <div class="card-header header-control">
                                        <h5>Danh sách</h5>
                                        <div class="controler-header">
                                            <%=GetTinyCacheListCommand()%>
                                        </div>
                                    </div>

                                    <div class="card-block">
                                        <div class="row">
                                            <div class="input-group input-group-danger col-sm-3" style="margin-bottom: 0;">
                                                <span class="input-group-prepend">
                                                    <label class="input-group-text"><i class="icon-calendar"></i></label>
                                                </span>
                                                <input style="border-color: #cccccc;" type="text" class="form-control datetimepicker_riki fill" value="<%=Utils.FormatDate2(model.StartDate) %>" placeholder="Chọn từ ngày" id="filter_sdate">
                                            </div>
                                            <div class="input-group input-group-danger col-sm-3" style="margin-bottom: 0;">
                                                <span class="input-group-prepend">
                                                    <label class="input-group-text"><i class="icon-calendar"></i></label>
                                                </span>
                                                <input style="border-color: #cccccc;" type="text" class="form-control datetimepicker_riki fill" value="<%=Utils.FormatDate2(model.EndDate) %>" placeholder="Chọn đến ngày" id="filter_edate">
                                            </div>
                                            <button type="submit" class="btntop" onclick="REDDEVILRedirect();return false;">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </div>
                                        <div class="dt-responsive table-responsive">
                                            <table id="" class="table_basic table table-hover m-b-0 "
                                                style="width: 100%">
                                                <thead>
                                                    <tr>
                                                        <th class="sorting text-center w1p">#</th>
                                                        <th class="sorting_disabled text-center w1p">
                                                            <div class="md-checkbox">
                                                                <input type="checkbox" id="checks" name="toggle" onclick="checkAll('<%=model.PageSize%>')" class="md-check">
                                                                <label for="checks">
                                                                    <span></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span>
                                                                </label>
                                                            </div>
                                                        </th>
                                                        <th class="sorting"><%= GetSortLink("Người dùng", "UserID")%></th>
                                                        <th class="sorting text-center hidden-sm hidden-col"><%= GetSortLink("Hoạt động", "Note")%></th>
                                                        <th class="sorting text-center hidden-sm hidden-col"><%= GetSortLink("Tại", "Module")%></th>
                                                        <th class="sorting text-center w10p hidden-sm hidden-col"><%= GetSortLink("IP", "IP")%></th>
                                                        <th class="sorting text-center w10p hidden-sm hidden-col"><%= GetSortLink("Ngày", "Created")%></th>
                                                        <th class="sorting text-center w1p"><%= GetSortLink("ID", "ID")%></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <%for (var i = 0; listItem != null && i < listItem.Count; i++)
                                                        { %>
                                                    <tr>
                                                        <td class="text-center"><%= i + 1%></td>
                                                        <td class="text-center">
                                                            <%= GetCheckbox(listItem[i].ID, i)%>
                                                        </td>
                                                        <td>
                                                            <%= listItem[i].GetUser() != null ? listItem[i].GetUser().LoginName : string.Empty%>
                                                        </td>
                                                        <td class="text-center hidden-sm hidden-col"><%=listItem[i].Note %></td>
                                                        <td class="text-center hidden-sm hidden-col"><%=listItem[i].Module %></td>
                                                        <td class="text-center hidden-sm hidden-col"><%=listItem[i].IP %></td>
                                                        <td class="text-center hidden-sm hidden-col"><%= string.Format("{0:dd-MM-yyyy HH:mm}", listItem[i].Created) %></td>
                                                        <td class="text-center"><%= listItem[i].ID%></td>
                                                    </tr>
                                                    <%} %>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="row center">
                                            <div class="col-md-12 col-sm-12 justify-content-center d-flex ">
                                                <%= GetPagination(model.PageIndex, model.PageSize, model.TotalRecord)%>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">

        var REDDEVILController = "SysUserLog";

        var REDDEVILArrVar = [
            "limit", "PageSize",
            "filter_sdate", "StartDate",
            "filter_edate", "EndDate"
        ];

        var REDDEVILArrQT = [
            "<%= model.PageIndex + 1 %>", "PageIndex",
            "<%= model.Sort %>", "Sort"
        ];

        var REDDEVILArrDefault = [
            "1", "PageIndex",
            "20", "PageSize"
        ];
    </script>

</form>
