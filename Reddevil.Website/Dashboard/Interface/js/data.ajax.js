﻿function ShowFile(type) {
    var finder = new CKFinder();
    finder.basePath = '../';
    finder.selectActionFunction = refreshFile;
    finder.popup();

    return false;
}
function ShowFileLanding() {
    var finder = new CKFinder();
    finder.basePath = '../';
    finder.selectActionFunction = refreshFileLanding;
    finder.popup();

    return false;
}
function refreshFileLanding(arg) {
    var langID = $("#LandID").val();
    addFileLanding(langID, arg);
}

function ShowProductForm(sValue) {
    window.open('/' + window.CPPath + '/FormNews/Index.aspx?Value=' + sValue, '', 'width=1024, height=800, top=80, left=250,scrollbars=yes');
    return false;
}

function refreshFile(arg) {
    addFile(arg);
}

function refreshGift(arg) {
    addProduct(arg);
}


function refreshPagenews(arg, agr2, type) {
    addNews(arg, agr2, type);
}


function CloseGift(arg) {
    if (window.opener)
        window.opener.refreshGift(arg);
    else
        window.parent.refreshGift(arg);

    window.close();
}

//news lq
function addNews(id, parent, type) {
    $.ajax({
        type: 'post',
        url: '/' + window.CPPath + '/Ajax/AddNews/',
        data: {
            ID: id,
            ParentID: parent,
            Name: $('#Name').val(),
            MenuID: $('#MenuID').val(),
            Type: type

        },
        dataType: 'json',
        success: function (data) {
            var content = data.Html;
            var js = data.Js;
            var params = data.Params;
            if (params != '') {
                zebra_alert('Thông báo !', params);
                return;
            }
            if (js != '') {
                location.href = '/' + window.CPPath + '/' + $('#ModuleCode').val() + '/Add/RecordID/' + js;
                return;
            }

            $('#list-news').html(content);
        },
        error: function (status) { }
    });
}
function deleteNews(id, parent, type) {
    $.ajax({
        type: 'post',
        url: '/' + window.CPPath + '/Ajax/DeleteNews/',
        data: {
            ID: id,
            ParentID: parent,
            Type: type
        },
        dataType: 'json',
        success: function (data) {
            var content = data.Html;
            $('#list-news').html(content);
        },
        error: function (status) { }
    });
}

//file
function addFile(file) {
    $.ajax({
        type: 'post',
        url: '/' + window.CPPath + '/Ajax/AddFile/',
        data: {
            Name: $('#Name').val(),
            MenuID: $('#MenuID').val(),
            ProductID: $('#RecordID').val(),
            File: file,
            TypeID: $('#dynamic_id').val(),
            Dyanamic: $('#ModuleCode').val(),
            Image: $('#File').val(),
        },
        dataType: 'json',
        success: function (data) {
            var js = data.Js;
            var params = data.Params;
            var content = data.Html;

            if (params != '') {
                zebra_alert('Thông báo !', params);
                return;
            }

            if (js != '') {
                location.href = '/' + window.CPPath + '/' + $('#ModuleCode').val() + '/Add.aspx/RecordID/' + js;
                return;
            }

            $('#list-file').html(content);
        },
        error: function (status) { }
    });
}

function deleteFile(file, type) {
    $.ajax({
        type: 'post',
        url: '/' + window.CPPath + '/Ajax/DeleteFile/',
        data: {
            ProductID: $('#RecordID').val(),
            File: file,
            TypeID: type
        },
        dataType: 'json',
        success: function (data) {
            var content = data.Html;

            $('#list-file').html(content);
        },
        error: function (status) { }
    });
}

function upFile(file, type) {
    $.ajax({
        type: 'post',
        url: '/' + window.CPPath + '/Ajax/UpFile/',
        data: {
            ProductID: $('#RecordID').val(),
            File: file,
            TypeID: type
        },
        dataType: 'json',
        success: function (data) {
            var content = data.Html;

            $('#list-file').html(content);
        },
        error: function (status) { }
    });
}

function downFile(file) {
    $.ajax({
        type: 'post',
        url: '/' + window.CPPath + '/Ajax/DownFile/',
        data: {
            ProductID: $('#RecordID').val(),
            File: file
        },
        dataType: 'json',
        success: function (data) {
            var content = data.Html;

            $('#list-file').html(content);
        },
        error: function (status) { }
    });
}

// quan huyen

function getDistrict(ParentID, Select, Type) {

    $.ajax({
        url: '/' + window.CPPath + '/Ajax/GetChild/',
        type: 'post',
        data: {
            ParentID: ParentID,
            SelectedID: Select
        },
        dataType: 'json',
        success: function (data) {
            var content = data.Html;

            $('#' + Type).html(content);
        },
        error: function (status) { }
    });
}



function deleteCache() {

    $.ajax({
        url: '/' + window.CPPath + '/Ajax/DeleteCache/',
        type: 'post',
        dataType: 'json',
        success: function (data) {
            var content = data.Message;
            if (content != '') {
                zebra_alert('Thông báo', content)
            }
        },
        error: function (status) { }
    });
}
