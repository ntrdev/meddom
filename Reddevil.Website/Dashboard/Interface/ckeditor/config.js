
CKEDITOR.editorConfig = function (config) {
	//config.skin = 'office2013';
	config.language = 'vi';
	config.height = 400;
	config.toolbarCanCollapse = true;
	config.extraPlugins = 'footnotes';
	config.removeButtons = 'Flash,About';
	config.allowedContent = true;
	config.protectedSource.push(/<i[^>]*><\/i>/g);
	config.protectedSource.push(/<span[^>]*><\/span>/g);
	config.protectedSource.push(/<a[^>]*><\/a>/g);
	config.fullPage = false;
};
