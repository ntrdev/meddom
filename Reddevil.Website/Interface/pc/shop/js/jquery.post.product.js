﻿//js creat by vantruong2412@live.com

function add_distribute() {
    var html = '<div class="box--form">';
    html += '           <div class="form-row"  id="distribute_1">';
    html += '               <div class="col_1">';
    html += '                   <label> Nhóm phân loại hàng 1</label>';
    html += '               </div>';
    html += '               <div class="col_2">';
    html += '                   <div class="formPopup">';
    html += '                           <div class="form-row formEditer">';
    html += '                               <div class="col_1"><label>Tên nhóm phân loại</label></div>';
    html += '                               <div class="col_2">';
    html += '                                    <div class="inputGroup">';
    html += '                                       <input class="g-distribute_1" placeholder="Nhập tên nhóm hàng, ví dụ: màu sắc, kích thước..."  name="GDistribute_1" /><span class="number" id="count_name_g_1">0/20</span>';
    html += '                                    </div>';
    html += '                               </div>';
    html += '                           </div>';
    html += '                       <div id="v_distribute_1">';
    html += '                           <div class="form-row formEditer" id="delete_v_distribute_1_0">';
    html += '                                <div class="col_1"><label>Phân loại hàng</label></div>';
    html += '                                <div class="col_2">';
    html += '                                     <div class="inputGroup">';
    html += '                                           <input class="v-distribute_1_0" id="v-distribute_1_0"  placeholder="Nhập tên loại hàng, ví dụ: trắng, đỏ..." name="v-distribute_1_0" /><span class="number" id="count_name_v_1_0">0/20</span>';
    html += '                                     </div>';
    html += '                                </div>';
    html += '                               <span class="btn-control"><svg aria-hidden="true" onclick="delele_v_distribute(\'1\',\'0\')" focusable="false" data-prefix="fal" data-icon="trash-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-trash-alt fa-w-14 fa-2x"><path fill="currentColor" d="M296 432h16a8 8 0 0 0 8-8V152a8 8 0 0 0-8-8h-16a8 8 0 0 0-8 8v272a8 8 0 0 0 8 8zm-160 0h16a8 8 0 0 0 8-8V152a8 8 0 0 0-8-8h-16a8 8 0 0 0-8 8v272a8 8 0 0 0 8 8zM440 64H336l-33.6-44.8A48 48 0 0 0 264 0h-80a48 48 0 0 0-38.4 19.2L112 64H8a8 8 0 0 0-8 8v16a8 8 0 0 0 8 8h24v368a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V96h24a8 8 0 0 0 8-8V72a8 8 0 0 0-8-8zM171.2 38.4A16.1 16.1 0 0 1 184 32h80a16.1 16.1 0 0 1 12.8 6.4L296 64H152zM384 464a16 16 0 0 1-16 16H80a16 16 0 0 1-16-16V96h320zm-168-32h16a8 8 0 0 0 8-8V152a8 8 0 0 0-8-8h-16a8 8 0 0 0-8 8v272a8 8 0 0 0 8 8z" class=""></path></svg></span>';
    html += '                           </div>';
    html += '                       </div>';
    html += '                           <div class="boxCenter"><input type="button" onclick="add_v_distribute(\'1\',\'true\')" class="buttonAdd" value="Thêm phân loại hàng"/></div>';
    html += '                     </div>';
    html += '                </div>';
    html += '           </div>';
    html += '           <div class="form-row" id="distribute_2">';
    html += '               <div class="col_1">';
    html += '                   <label> Nhóm phân loại hàng 2</label>';
    html += '               </div>';
    html += '               <div class="col_2">';
    html += '                        <button class="buttonAdd btn-block" onclick="add_distribute2()">Thêm mới</button>';
    html += '                </div>';
    html += '          </div>';
    html += '</div>';

    html += '<div class="box--form">';
    html += ' <div class="form-row">';
    html += '  <div class="col_1" >';
    html += '      <label>Tip thiết lập phân loại hàng</label>';
    html += '   </div>';
    html += '  <div class="col_2" style="width: 76%;">';
    html += '       <table width="100%" border="0" cellspacing="0" cellpadding="0">';
    html += '         <tr>';
    html += '            <td>';
    html += '                  <input placeholder="Giá (vnđ)" id="default_price" class="inputControl" />';
    html += '            </td>';
    html += '            <td>';
    html += '                 <input placeholder="số lượng trong kho" id="default_store" class="inputControl" />';
    html += '            </td>';
    html += '            <td>';
    html += '                 <input placeholder="Sku phân loại" id="default_sku" class="inputControl" />';
    html += '            </td>';
    html += '       </tr>';
    html += '   </table>';
    html += '  </div>';
    html += ' <div class="col_3">';
    html += '         <button class="btn-apply" id="apply">Áp dụng</button>';
    html += '   </div>';
    html += ' </div>';

    html += '<div id="table_distribute">';
    html += ' <div class="form-row">';
    html += '     <div class="col_1">';
    html += '        <label>Danh sách phân loại</label>';
    html += '     </div>';
    html += '      <div class="col_2">';
    html += '        <table class="table table-bordered tableInput" width="100%" border="0" cellspacing="0" cellpadding="0">';
    html += '            <thead>';
    html += '              <tr id="name_gdistribute">';
    html += '                  <th id="name_gdistribute_1"></th>';
    html += '                  <th id="name_gdistribute_2" style="display:block;"></th>';
    html += '                  <th>Giá</th>';
    html += '                  <th>Kho hàng</th>';
    html += '                  <th>SKU</th>';
    html += '              </tr>';
    html += '           </thead>';
    html += '           <tbody id="table_1">';
    html += '          </tbody>';
    html += '         </table>';
    html += '    </div>';
    html += '  </div>';
    html += '</div>';


    html += '</div>';
    $('#distribute_add').html(html);
    callBack(0);
}

function add_distribute2() {
    var html = '         <div class="col_1">';
    html += '                   <label> Nhóm phân loại hàng 2</label>';
    html += '               </div>';
    html += '               <div class="col_2">';
    html += '                   <div class="formPopup">';
    html += '                           <div class="form-row formEditer">';
    html += '                               <div class="col_1"><label>Tên nhóm phân loại</label></div>';
    html += '                               <div class="col_2">';
    html += '                                    <div class="inputGroup">';
    html += '                                       <input class="g-distribute_2" placeholder="Nhập tên nhóm hàng, ví dụ: màu sắc, kích thước..."  name="GDistribute_2" /><span class="number" id="count_name_g_2">0/20</span>';
    html += '                                    </div>';
    html += '                               </div>';
    html += '                           </div>';
    html += '                       <div id="v_distribute_2">';
    html += '                           <div class="form-row formEditer" id="delete_v_distribute_2_0">';
    html += '                                <div class="col_1"><label></label></div>';
    html += '                                <div class="col_2">';
    html += '                                     <div class="inputGroup">';
    html += '                                           <input class="v-distribute_2_0" id="v-distribute_2_0"  placeholder="Nhập tên loại hàng, ví dụ: trắng, đỏ..." name="v-distribute_2_0" /><span class="number" id="count_name_v_2_0">0/20</span>';
    html += '                                     </div>';
    html += '                                </div>';
    html += '                               <span class="btn-control"><svg aria-hidden="true" onclick="delele_v_distribute(\'2\',\'0\')" focusable="false" data-prefix="fal" data-icon="trash-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-trash-alt fa-w-14 fa-2x"><path fill="currentColor" d="M296 432h16a8 8 0 0 0 8-8V152a8 8 0 0 0-8-8h-16a8 8 0 0 0-8 8v272a8 8 0 0 0 8 8zm-160 0h16a8 8 0 0 0 8-8V152a8 8 0 0 0-8-8h-16a8 8 0 0 0-8 8v272a8 8 0 0 0 8 8zM440 64H336l-33.6-44.8A48 48 0 0 0 264 0h-80a48 48 0 0 0-38.4 19.2L112 64H8a8 8 0 0 0-8 8v16a8 8 0 0 0 8 8h24v368a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V96h24a8 8 0 0 0 8-8V72a8 8 0 0 0-8-8zM171.2 38.4A16.1 16.1 0 0 1 184 32h80a16.1 16.1 0 0 1 12.8 6.4L296 64H152zM384 464a16 16 0 0 1-16 16H80a16 16 0 0 1-16-16V96h320zm-168-32h16a8 8 0 0 0 8-8V152a8 8 0 0 0-8-8h-16a8 8 0 0 0-8 8v272a8 8 0 0 0 8 8z" class=""></path></svg></span>';
    html += '                           </div>';
    html += '                       </div>';
    html += '                           <div class="boxCenter"><input type="button" onclick="add_v_distribute(\'2\',\'false\')" class="buttonAdd" value="Thêm phân loại hàng"/></div>';
    html += '                     </div>';
    html += '                </div>';


    $('#distribute_2').html(html);
    var name_1 = $('.g-distribute_1').val();
    var name_2 = $('.g-distribute_2').val();

    $('#name_gdistribute_2').show();

    $("td[id^='distribute_price_for_2_']").empty();
    $("td[id^='distribute_store_for_2_']").empty();
    $("td[id^='distribute_sku_for_2_']").empty();


    callBack();
}
var index = 0; var index2 = 0;
function add_v_distribute(id, check) {

    if (id == 2 && index2 == 0) {
        index = 0; index2++;
        index++;
    }
    else {
        index++;

    }
    var html = '                           <div class="form-row formEditer" id="delete_v_distribute_' + id + '_' + index + '">';
    html += '                                <div class="col_1"><label></label></div>';
    html += '                                <div class="col_2">';
    html += '                                     <div class="inputGroup">';
    html += '                                           <input id="v-distribute_' + id + '_' + index + '" class="v-distribute_' + id + '_' + index + '" placeholder="Nhập tên loại hàng, ví dụ: trắng, đỏ..." name="v-distribute_' + id + '_' + index + '" /><span class="number" id="count_name_v_' + id + '_' + index + '">0/20</span>';
    html += '                                     </div>';
    html += '                                </div>';
    html += '                               <span class="btn-control"><svg aria-hidden="true" onclick="delele_v_distribute(\'' + id + '\',\'' + index + '\')" focusable="false" data-prefix="fal" data-icon="trash-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-trash-alt fa-w-14 fa-2x"><path fill="currentColor" d="M296 432h16a8 8 0 0 0 8-8V152a8 8 0 0 0-8-8h-16a8 8 0 0 0-8 8v272a8 8 0 0 0 8 8zm-160 0h16a8 8 0 0 0 8-8V152a8 8 0 0 0-8-8h-16a8 8 0 0 0-8 8v272a8 8 0 0 0 8 8zM440 64H336l-33.6-44.8A48 48 0 0 0 264 0h-80a48 48 0 0 0-38.4 19.2L112 64H8a8 8 0 0 0-8 8v16a8 8 0 0 0 8 8h24v368a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V96h24a8 8 0 0 0 8-8V72a8 8 0 0 0-8-8zM171.2 38.4A16.1 16.1 0 0 1 184 32h80a16.1 16.1 0 0 1 12.8 6.4L296 64H152zM384 464a16 16 0 0 1-16 16H80a16 16 0 0 1-16-16V96h320zm-168-32h16a8 8 0 0 0 8-8V152a8 8 0 0 0-8-8h-16a8 8 0 0 0-8 8v272a8 8 0 0 0 8 8z" class=""></path></svg></span>';
    html += '                           </div>';
    $('#v_distribute_' + id).append(html);
    var _value = $('.v-distribute_' + id + '_' + (index - 1) + '').val();
    var name = remove_unicode(_value);
    var name_1 = $('.g-distribute_1').val();
    if (check == "true") {

        var htmlTable = '          <tr id="delete_tr_distribute_' + id + '_' + (index - 1) + '">';
        htmlTable += '                <td>';
        htmlTable += '                     <input id="td-v-distribute_1_' + (index - 1) + '" value="' + _value + '" readonly name="td-v-distribute_1_' + (index - 1) + '" class="inputControl" placeholder="32gb, 64gb, 128gb..." />';
        htmlTable += '                </td>';
        htmlTable += '                <td style="" id="td-v-distribute_2">';
        htmlTable += '                 </td>';
        htmlTable += '                <td id="distribute_price_for_2_' + name + '" class="distribute_price_for_2_' + name + '">';
        htmlTable += '                    <input type="number"  value="" name="td-v-price_' + id + '_' + (index - 1) + '_' + name + '" class="inputControl" placeholder="Giá bán sp của ' + name_1 + ' ' + _value + '"  />';
        htmlTable += '                 </td>';
        htmlTable += '                <td id="distribute_store_for_2_' + name + '" >';
        htmlTable += '                    <input type="number" value="" name="td-v-store_' + id + '_' + (index - 1) + '_' + name + '" class="inputControl" placeholder="Số lượng sp của ' + name_1 + ' ' + _value + '"/>';
        htmlTable += '                </td>';
        htmlTable += '                <td id="distribute_sku_for_2_' + name + '">';
        htmlTable += '                    <input value="" name="td-v-sku_' + id + '_' + (index - 1) + '_' + name + '" class="inputControl" placeholder="Mã sp của ' + name_1 + ' ' + _value + '" />';
        htmlTable += '                 </td>';
        htmlTable += '            </tr>';
        $('#table_1').append(htmlTable);
        callBack(index);


        create_json(name_1, _value, 1, (index - 1));
    }
    if (check == "false") {
        var name_2 = $('.g-distribute_2').val();
        $('#td-v-distribute_2').show();

        var htmlValue = '<input value="' + _value + '" readonly name="td-v-distribute_' + id + '_' + (index - 1) + '" id="td-v-distribute_' + id + '_' + (index - 1) + '" class="inputControl" placeholder="Thuộc tính sản phẩm" />';
        $("td[id^='td-v-distribute_2']").append(htmlValue);

        var htmlPrice = '<input type="text" onclick="this.type=\'number\'"  value="" name="td-v-price_' + id + '_' + name + '_' + (index - 1) + '"  id="" placeholder="Giá bán sp của ' + name_2 + ' ' + _value + '"  class="inputControl td-v-price_' + id + '_' + name + '_' + (index - 1) + '" />';
        $("td[id^='distribute_price_for_2']").append(htmlPrice);

        var htmlTableStore = '<input type="text" onclick="this.type=\'number\'" value=""  name="td-v-store_' + id + '_' + name + '_' + (index - 1) + '" id="" placeholder="Số lượng sp của ' + name_2 + ' ' + _value + '"  class="inputControl td-v-store_' + id + '_' + name + '_' + (index - 1) + '" />';
        $("td[id^='distribute_store_for_2']").append(htmlTableStore);

        var htmlTableSku = '<input type="text" onclick="this.type=\'number\'" value="" name="td-v-sku_' + id + '_' + name + '_' + (index - 1) + '" id=""  class="inputControl td-v-sku_' + id + '_' + name + '_' + (index - 1) + '" placeholder="Mã sp của ' + name_2 + ' ' + _value + '" />';
        $("td[id^='distribute_sku_for_2']").append(htmlTableSku);


        callBack(index);


        create_json(name_2, _value, 2, (index - 1));

    }




}

function remove_unicode(str) {
    str = str.toLowerCase();
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g, "");
    str = str.replace(/ + /g, "");
    str = str.replace(" ", "");
    str = str.trim();

    return str;
}

function delele_v_distribute(id, index) {

    var getName = $('#v-distribute_' + id + '_' + index + '').val();
    if (getName != '' || getName != 'undefined') {

        getName = remove_unicode(getName);

        var getAllTag = $("td[id^='distribute_price_for_2_']");

        if (getAllTag.length > 0 && id > 1) {
            for (var i = 0; i < getAllTag.length; i++) {
                var _tach1 = getAllTag[i].getAttribute('id').split('_');
                detele_json(id, index, '', getName, _tach1[4]);
            }

        }
        else {
            detele_json(id, index, '', '', getName);
        }

        $('td').find($("input[name^='td-v-distribute_" + id + "_" + index + "']")).remove();
        $('td').find($("input[name^='td-v-price_" + id + "_" + getName + "_" + index + "']")).remove();
        $('td').find($("input[name^='td-v-store_" + id + "_" + getName + "_" + index + "']")).remove();
        $('td').find($("input[name^='td-v-sku_" + id + "_" + getName + "_" + index + "']")).remove();
        $('#delete_v_distribute_' + id + '_' + index).remove();
        $('#delete_tr_distribute_' + id + '_' + index).remove();
    }





}

function detele_json(id, index, keyname, valueName, valueParent) {

    var _checkCookies = readCookie("gshop_property_pro_" + id);
    if (_checkCookies != null) {
        var obj = JSON.parse(_checkCookies);
        keyname = obj.name;
        keyname = remove_unicode(keyname) + '_' + index;
        delete obj.value[keyname];
        if (id == 1) {
            keyname = remove_unicode(valueParent);
            delete obj.price[keyname];
            delete obj.store[keyname];
            delete obj.sku[keyname];
        }
        if (valueName != '' && valueParent != '') {
            keyname = valueName + '_' + valueParent;
            delete obj.price[keyname];
            delete obj.store[keyname];
            delete obj.sku[keyname];
        }
        else if (valueParent != '') {
            var checkCookies2 = readCookie("gshop_property_pro_2");
            if (checkCookies2 != null) {
                var obj2 = JSON.parse(checkCookies2);
                var getAllTagNameInput = $("input[id^='td-v-distribute_2_']");
                for (var i = 0; i < getAllTagNameInput.length; i++) {
                    var valueInput = getAllTagNameInput[i].value;
                    keyname = remove_unicode(valueInput) + '_' + valueParent;
                    delete obj2.price[keyname];
                    delete obj2.store[keyname];
                    delete obj2.sku[keyname];
                }
                var jsonString = JSON.stringify(obj2);
                console.log(jsonString);
                createCookie("gshop_property_pro_2", jsonString, 10);

            }
        }
        var jsonString2 = JSON.stringify(obj);
        console.log(jsonString2);
        createCookie("gshop_property_pro_" + id, jsonString2, 10);
    }

}
function update_json(name, index, id, value) {
    var _checkCookies = readCookie("gshop_property_pro_" + id);
    if (_checkCookies != null) {
        //i = i + 1;
        var obj = JSON.parse(_checkCookies);
        obj.value[name + '_' + index] = value;
        var jsonString2 = JSON.stringify(obj);
        console.log(jsonString2);
        createCookie("gshop_property_pro_" + id, jsonString2, 10);
    }
}


//create json
function create_json(name, value, id, index) {
    var _name = remove_unicode(name);
    var jsonObj = {
        name: name,
        value: {},
        price: {},
        store: {},
        sku: {}
    };
    var newUser = _name;
    var newValue = value;
    jsonObj.value[newUser + '_' + index] = newValue;

    var jsonString = JSON.stringify(jsonObj);
    console.log(jsonString);



    var _checkCookies = readCookie("gshop_property_pro_" + id);
    if (_checkCookies != null) {
        var obj = JSON.parse(_checkCookies);
        obj.value[newUser + '_' + index] = newValue;
        var jsonString2 = JSON.stringify(obj);
        console.log(jsonString2);
        createCookie("gshop_property_pro_" + id, jsonString2, 10);
    }
    else {
        createCookie("gshop_property_pro_" + id, jsonString, 10);
        console.log(jsonString);
    }
}


function callBack(index) {
    $('input.g-distribute_1').keyup(function () {
        var name = $(this).val();
        $('#name_gdistribute_1').html(name);
        var max = 20;
        var len = $(this).val().length;
        if (len >= max) {
            $(this).parent().find('#count_name_g_1').text('0/20');
            $(this).val($(this).val().substring(0, 19));
        } else {
            var char = max - len;
            $(this).parent().find('#count_name_g_1').text(char + '/20');
        }
    });
    $('input.g-distribute_2').keyup(function () {
        var name = $(this).val();
        $('#name_gdistribute_2').html(name);
    });
    $('input.v-distribute_1_1').keyup(function () {
        var name = $(this).val();
        $('#name_gdistribute_2').html(name);
    });

    $("input[name^='v-distribute_1_']").keyup(function () {
        var _hihi = $(this).attr("name").split('_');
        if (_hihi.length > 1) index = _hihi[2];
        var max = 20;
        var len = $(this).val().length;
        if (len >= max) {
            $(this).parent().find('#count_name_v_1_' + index + '').text('0/20');
            $(this).val($(this).val().substring(0, 19));
        } else {
            var char = max - len;
            $(this).parent().find('#count_name_v_1_' + index + '').text(char + '/20');
        }
        $('td').find($('#td-v-distribute_1_' + index + '')).val($(this).val());

        var nameobj = remove_unicode($('.g-distribute_1').val());
        update_json(nameobj, index, _hihi[1], $(this).val());
    });

    $("input[name^='v-distribute_2_']").keyup(function () {

        var _hihi = $(this).attr("name").split('_');
        if (_hihi.length > 1) index = _hihi[2];
        var max = 20;
        var len = $(this).val().length;
        if (len >= max) {
            $(this).parent().find('#count_name_v_2_' + index + '').text('0/20');
            $(this).val($(this).val().substring(0, 19));
        } else {
            var char = max - len;
            $(this).parent().find('#count_name_v_2_' + index + '').text(char + '/20');
        }

        var listInput = document.getElementsByName('td-v-distribute_2_' + index);
        for (var i = 0; i < listInput.length; i++) {
            listInput[i].value = $(this).val();
        }

        var nameobj = remove_unicode($('.g-distribute_2').val());
        update_json(nameobj, index, _hihi[1], $(this).val());
    });


    //price
    $("input[name^='td-v-price_2_']").keyup(function () {

        var _tach = $(this).attr("name").split('_');
        if (_tach.length > 1) index = _tach[2];

        var _checkCookies = readCookie("gshop_property_pro_" + _tach[1]);
        if (_checkCookies != null) {

            var _tachPrid = $(this).parent().attr('id').split('_');

            var obj = JSON.parse(_checkCookies);

            obj.price[_tach[2] + '_' + _tachPrid[4]] = $(this).val();
            var jsonString2 = JSON.stringify(obj);

            console.log(jsonString2);
            createCookie("gshop_property_pro_" + _tach[1], jsonString2, 10);
        }

    });

    //store 
    $("input[name^='td-v-store_2_']").keyup(function () {

        var _tach = $(this).attr("name").split('_');
        if (_tach.length > 1) index = _tach[2];

        var _checkCookies = readCookie("gshop_property_pro_" + _tach[1]);
        if (_checkCookies != null) {

            var _tachPrid = $(this).parent().attr('id').split('_');

            var obj = JSON.parse(_checkCookies);

            obj.store[_tach[2] + '_' + _tachPrid[4]] = $(this).val();
            var jsonString2 = JSON.stringify(obj);

            console.log(jsonString2);
            createCookie("gshop_property_pro_" + _tach[1], jsonString2, 10);
        }
    });

    //Sku

    $("input[name^='td-v-sku_2_']").keyup(function () {

        var _tach = $(this).attr("name").split('_');
        if (_tach.length > 1) index = _tach[2];

        var _checkCookies = readCookie("gshop_property_pro_" + _tach[1]);
        if (_checkCookies != null) {

            var _tachPrid = $(this).parent().attr('id').split('_');

            var obj = JSON.parse(_checkCookies);

            obj.sku[_tach[2] + '_' + _tachPrid[4]] = $(this).val();
            var jsonString2 = JSON.stringify(obj);

            console.log(jsonString2);
            createCookie("gshop_property_pro_" + _tach[1], jsonString2, 10);
        }
    });

    $("input[name^='td-v-price_1_']").keyup(function () {

        var _tach = $(this).attr("name").split('_');
        if (_tach.length > 1) index = _tach[2];

        var _checkCookies = readCookie("gshop_property_pro_" + _tach[1]);
        if (_checkCookies != null) {

            var _tachPrid = $(this).parent().attr('id').split('_');

            var obj = JSON.parse(_checkCookies);

            obj.price[_tachPrid[4]] = $(this).val();
            var jsonString2 = JSON.stringify(obj);

            console.log(jsonString2);
            createCookie("gshop_property_pro_" + _tach[1], jsonString2, 10);
        }

    });

    //store 
    $("input[name^='td-v-store_1_']").keyup(function () {

        var _tach = $(this).attr("name").split('_');
        if (_tach.length > 1) index = _tach[2];

        var _checkCookies = readCookie("gshop_property_pro_" + _tach[1]);
        if (_checkCookies != null) {

            var _tachPrid = $(this).parent().attr('id').split('_');

            var obj = JSON.parse(_checkCookies);

            obj.store[_tachPrid[4]] = $(this).val();
            var jsonString2 = JSON.stringify(obj);

            console.log(jsonString2);
            createCookie("gshop_property_pro_" + _tach[1], jsonString2, 10);
        }
    });

    //Sku

    $("input[name^='td-v-sku_1_']").keyup(function () {

        var _tach = $(this).attr("name").split('_');
        if (_tach.length > 1) index = _tach[2];

        var _checkCookies = readCookie("gshop_property_pro_" + _tach[1]);
        if (_checkCookies != null) {

            var _tachPrid = $(this).parent().attr('id').split('_');

            var obj = JSON.parse(_checkCookies);

            obj.sku[_tachPrid[4]] = $(this).val();
            var jsonString2 = JSON.stringify(obj);

            console.log(jsonString2);
            createCookie("gshop_property_pro_" + _tach[1], jsonString2, 10);
        }
    });
}







$("#Quotes").click(function () {
    if (document.getElementById('Quotes').checked) {
        document.getElementById('Quotes').value = "1";

        $(".price2").slideToggle();
        $(".priceCost").slideToggle();

    }
    else {
        document.getElementById('Quotes').value = "0"; $(".price2").slideToggle();
        $(".priceCost").slideToggle();
        document.getElementById('Quotes').removeAttribute('checked');
    }

});


$("#check_confirm_up").click(function () {
    var _priceMin = $('#priceMin').val();
    var _priceMax = $('#priceMax').val();
    if (document.getElementById('check_confirm_up').checked) {
        document.getElementById('check_confirm_up').value = "1";

        if (_priceMin <= 0) {
            $('#priceMin').val($('#price').val());
        }
        if (_priceMax <= 0) {
            $('#priceMax').val($('#priceCost').val());
        }
        $(".priceMin").slideToggle();
        $(".priceMax").slideToggle();

    }
    else {
        document.getElementById('check_confirm_up').value = "0"; $(".priceMin").slideToggle();
        $(".priceMax").slideToggle();
        if (_priceMin >= 0) {
            $('#priceMin').val(0);
        }
        if (_priceMax >= 0) {
            $('#priceMax').val(0);
        }
        document.getElementById('check_confirm_up').removeAttribute('checked');
    }

});


$('.districtID_ghn').on("click", function () {
    var _id = $(this).data('districtid');
    var _text = $(this).text();
    $('#FromCityID').val(_id);

    document.getElementById("name_cty").textContent = _text;
    // onclick="document.getElementById(\'FromCityID\').value=\''+$(list_district[i]).data('districtid')+'\';document.getElementById(\'name_cty\').textContent=\''+$(list_district[i]).text()+'\';"
    showCity();
})

function search_city(keyword) {
    debugger;
    if (keyword != '') {
        var list_district = $('.districtIDghn');
        var hshs = "";
        for (var i = 0; list_district != null && i < list_district.length; i++) {
            var haah = $(list_district[i]).text().toUpperCase();

            if (haah.includes(keyword.toUpperCase())) {
                hshs += '<li class="shopee-address-picker__current-level-list-item districtID_ghn" data-districtid="' + $(list_district[i]).data('districtid') + '">' + $(list_district[i]).text() + '</li>';

            }
        }

        $('#list_city').html(hshs);

        $('.districtID_ghn').on("click", function () {
            var _id = $(this).data('districtid');
            var _text = $(this).text();
            $('#FromCityID').val(_id);

            document.getElementById("name_cty").textContent = _text;
            // onclick="document.getElementById(\'FromCityID\').value=\''+$(list_district[i]).data('districtid')+'\';document.getElementById(\'name_cty\').textContent=\''+$(list_district[i]).text()+'\';"
            showCity();
        });
    }
    else {
        getDistrict_ghn();
    }
}
function showCity() {

    $('.shopee-address-picker').slideToggle();
}


$('input[type="file"]').change(function () {
    var id = $(this).data('id');
    var arrID = id.split('_');
    if (arrID.length === 2) {
        var index = id.replace(arrID[0] + '_', '');
        readURL(this, arrID[0] + '_Preview_' + index);
    }
    else {
        readURL(this, id + '_Preview');
    }

    return;
});

function readURL(input, preview) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('.' + preview).attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

function previewFile(type) {
    if (type === 'logo') {
        var img = $('.fileinput-new img');
        var img2 = $('.profile-userpic img');
        var file = document.querySelector('input[name=upload-logo]').files[0];
        var reader = new FileReader();

        if (file) {
            reader.readAsDataURL(file);
        } else {
            img.attr('src', '');
        }

        reader.onloadend = function () {
            img.attr('src', reader.result);
            img2.attr('src', reader.result);
        }
    }

}

function deleteFile(id) {
    var img = $('#' + id);
    img.attr('src', '');
}
//thuoc tinh
function GetProperties(MenuID, ProductID) {
    var ranNum = Math.floor(Math.random() * 999999);
    var dataString = "MenuID=" + MenuID + "&LangID=1&ProductID=" + ProductID + "&rnd=" + ranNum;

    $.ajax({
        url: "/Ajax/GetProperties2.html",
        type: "get",
        data: dataString,
        dataType: 'json',
        success: function (data) {
            var content = data.Html;

            $("#list_property").html(content);
        },
        error: function (status) { }
    });
}