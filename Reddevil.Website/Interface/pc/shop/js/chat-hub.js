﻿
$(function () {
    var notifications = $.connection.signalR;
    registerClientMethods(notifications);


    $.connection.hub.start().done(function () {
        registerEvents(notifications);

    }).fail(function (e) {
        alert(e);
    });
    window.onbeforeunload = function (e) {
        $.connection.hub.stop();
    };
});





function registerEvents(chatHub) {

    $("#btnStartChat").click(function () {

        var name = $("#shopName").val();
        var toUserID = $('#bsUserIdShop').val();

        var fromUserID = $('#bsUserId').val();

        if (fromUserID > 0) {
            toUserID = fromUserID;
        }
        if (toUserID <= 0) {
            toUserID = $('#bsUserId2').val();
        }
        if (name.length > 0) {
            chatHub.server.connect(name, toUserID);
        }
        else {
            alert("Chọn người cần nhắn tin");
        }

    });



    $('#btnSendMsg').click(function () {

        var today = new Date(); var dd = today.getDate(); var mm = today.getMonth() + 1; var hh = today.getHours(); var mn = today.getMinutes(); var yyyy = today.getFullYear();
        if (dd < 10) { dd = '0' + dd; }
        if (mm < 10) { mm = '0' + mm; }
        if (mn < 10) { mn = '0' + mn; }
        var todaytext = dd + '/' + mm + '/' + yyyy + ' ' + hh + ':' + mn;

        var msg = $("#txtMessage").val();
        var fromID = $("#fromID").val();

        var toUser = $('#bsUserIdShop').val();

        if (toUser <= 0) {
            toUser = $('#bsUserId2').val();
        }

        var logiNameToUser = $('#loginNameToUser').val();

        var repFromUser = $('#setToIDUser').val();

        var fileFrom = $('#fileUser').val();
        var htmlMsg = "";
        var classx = "." + fromID;

        if (msg.length > 0 && repFromUser <= 0) {

            htmlMsg = '<li class="' + fromID + '"><div class="media"><div class="media-left"><img src="' + fileFrom + '" class="media-object" ></div><div class="media-body"><p>' + msg + '</p></div><div class="timestamp">' + todaytext + '</div></div></li>';

            chatHub.server.sendPrivateMessage(logiNameToUser, toUser, msg, htmlMsg);



            $("#txtMessage").val('');

            $('#divChatWindow_' + toUser + '').append(htmlMsg);


            $(classx).addClass("me");
            var height = $('#chatbox_' + toUser + '')[0].scrollHeight;

            $('#chatbox_' + toUser + '').scrollTop(height);
        }

        if (msg.length > 0 && repFromUser > 0) {
            if (toUser !== repFromUser) {

                htmlMsg = '<li class="' + fromID + '"><div class="media"><div class="media-left"><img src="' + fileFrom + '" class="media-object" ></div><div class="media-body"><p>' + msg + '</p></div><div class="timestamp">' + todaytext + '</div></div></li>';

            }
            if (toUser === repFromUser) {

                htmlMsg = '<li class="' + fromID + '"><div class="media"><div class="media-left"><img src="' + fileFrom + '" class="media-object" ></div><div class="media-body"><p>' + msg + '</p></div><div class="timestamp">' + todaytext + '</div></div></li>';
            }
            chatHub.server.sendPrivateMessage(logiNameToUser, repFromUser, msg, htmlMsg);

            $("#txtMessage").val('');

            $('#divChatWindow_' + repFromUser + '').append(htmlMsg);

            $(classx).addClass("me");

            var height2 = $('#chatbox_' + repFromUser + '')[0].scrollHeight;
            $('#chatbox_' + repFromUser + '').scrollTop(height2);
        }

    });


    $("#txtMessage").keypress(function (e) {
        if (e.which === 13) {
            $('#btnSendMsg').click();
        }
    });

    $("#close").click(function () {

        var interest = $('ul#list_userChat').find('li.active').data('id');

        var _fromUser = $("#ConnectionID").val();
        var _toUser = $('#setToIDUser').val();
        var toUser = $('#bsUserId').val();

        createCookie('Web_CloseChat', 1, 120);
    });


    $("#logout").click(function () {
        //var _fromUser = $("#ConnectionID").val();
        // chatHub.server.removeMess(_fromUser);

        localStorage.removeItem('tab_user');
        location.href = $(this).data('url');

    });




    var _Online = readCookie('cityplaza.vn.v2_Web.Login');
    if (_Online === null) {
        localStorage.removeItem('tab_isme');
        localStorage.removeItem('tab_user');
    } else {
        var fromID = $("#fromID").val();

        localStoreIsMe(fromID);
    }

}



function getAllTabConnect() {
    var _listConect = [];
    var oldata = JSON.parse(localStorage.getItem('tab_user'));
    for (var i = 0; oldata !== null && i < oldata.length; i++) {
        _listConect.push(oldata[i].connectionId);
    }

    return _listConect;
}



function localStore(UserToId, connectionIDtoUser) {


    var _Online = readCookie('cityplaza.vn.v2_Web.Login');
    if (_Online === null) {
        localStorage.removeItem('tab_user');
    }



    if (connectionIDtoUser !== '') {
        var user_connect_tab = [];
        var _data = '';

        var store = localStorage.getItem('tab_user');
        if (store === null) {
            _data = { userid: UserToId, connectionId: connectionIDtoUser, tab: 1 };
            user_connect_tab.push(_data);
            localStorage.setItem('tab_user', JSON.stringify(user_connect_tab));
        }
        else {

            var oldata = JSON.parse(localStorage.getItem('tab_user'));


            if (oldata !== null) {
                var obj = findObjectByKey(oldata, 'connectionId', connectionIDtoUser);
                if (obj === null) {
                    _data = { userid: UserToId, connectionId: connectionIDtoUser, tab: oldata.length + 1 };
                    oldata.push(_data);
                    localStorage.setItem('tab_user', JSON.stringify(oldata));
                }
            }

        }
    }
}

function findObjectByKey(array, key, value) {
    for (var i = 0; i < array.length; i++) {
        if (array[i][key] === value) {
            return array[i];
        }
    }
    return null;
}

function localStoreIsMe(UserfromId) {




    var user_connect_tab = [];
    var _data = '';

    var store = localStorage.getItem('tab_isme');
    if (store === null) {
        _data = { userid: UserfromId, connectionId: $.connection.hub.id, tab: 1 };
        user_connect_tab.push(_data);
        localStorage.setItem('tab_isme', JSON.stringify(user_connect_tab));
    }
    else {

        var oldata = JSON.parse(localStorage.getItem('tab_isme'));


        if (oldata !== null) {
            var obj = findObjectByKey(oldata, 'connectionId', $.connection.hub.id);
            if (obj === null) {
                _data = { userid: UserfromId, connectionId: $.connection.hub.id, tab: oldata.length + 1 };
                oldata.push(_data);
                localStorage.setItem('tab_isme', JSON.stringify(oldata));
            }
        }
    }

}


function getAllTabIsMe() {
    var _listConect2 = [];
    var oldata2 = JSON.parse(localStorage.getItem('tab_isme'));
    for (var i = 0; oldata2 !== null && i < oldata2.length; i++) {
        _listConect2.push(oldata2[i].connectionId);
    }

    return _listConect2;
}


function registerClientMethods(chatHub) {

    // gọi đến người dùng khi đăng nhập
    chatHub.client.onConnected = function (userFromId, toUserName, toUserID, fileTouser, msg, active) {

        var ctrId = 'chatbox_' + toUserID;//id tab mới dành cho chát nhiều người 
        $("#name_shop").html(toUserName);
        $("#loginNameToUser").val(toUserName);
        $('#btn-chat').show();

        open_global();
        $('#txtMessage').val('');
        $('#txtMessage').focus();
        var _checkCtrID = "userBox_" + toUserID;

        if ($('#' + _checkCtrID).length < 1 && active !== '') {
            addListUser(userFromId, toUserID, fileTouser, toUserName, 1, active);
            createPrivateChatWindow(toUserID, toUserName, fileTouser, 1, active);


        }

        if ($('#' + _checkCtrID).length < 1 && active === '' && msg === '') {
            addListUser(userFromId, toUserID, fileTouser, toUserName, 1, active);
            createPrivateChatWindow(toUserID, toUserName, fileTouser, 1, active);


        }
        if (msg !== '' && $('#' + _checkCtrID).length < 1) {
            AddMessage(msg, fileTouser, toUserID, ctrId, 1);
            var classx = "." + userFromId;
            $(classx).addClass("me");
        }


        //localStore(toUserID, connectionToId);
        //localStoreIsMe(userFromId);
    };


    //nhận id của người dùng rồi xóa tab
    // chatHub.client.onDisconnected = function (disconn) { };

    chatHub.client.addNotification = function (data, msg, type) {


        if (data !== null) {
            var Code = "http://gshop.vn/" + data.Code + ".html";
            pushNotification(msg, Code);

            $('#count_notifi').html('1');
        }
    };


    //chat trực tiếp
    chatHub.client.sendPrivateMessagenhanve = function (fromUserUsername, Name, message, IDfrom, fileUser, toUserID, seen) {

        open_global();

        var _checkCtrID = 'userBox_' + IDfrom;
        var ctrId = 'chatbox_' + IDfrom;//id tab mới dành cho chát nhiều người 
        var findID = $('#list_chatContent').find('.chat-window-scrollable').length;

        if ($('#' + _checkCtrID).length < 1 && findID < 1) {
            addListUser(toUserID, IDfrom, fileUser, fromUserUsername, 1, seen);
            $('#setToIDUser').val(IDfrom);
        }

        if ($('#' + _checkCtrID).length < 1 && findID > 0) {
            addListUser(toUserID, IDfrom, fileUser, fromUserUsername, 0, seen);
        }

        if ($('#' + ctrId).length < 1 && findID < 1) {
            createPrivateChatWindow(IDfrom, fromUserUsername, fileUser, 1);
            $('#setToIDUser').val(IDfrom);
            $("#name_shop").html(fromUserUsername);
            $("#loginNameToUser").val(fromUserUsername);

        }
        if ($('#' + ctrId).length < 1 && findID > 0) {
            createPrivateChatWindow(IDfrom, fromUserUsername, fileUser, 0);
        }

        AddMessage(message, fileUser, IDfrom, ctrId);

        if (!$('#' + _checkCtrID).is('.active')) {
            $('#count_mess_' + IDfrom + '').removeClass('seen');
            $('#count_mess_' + IDfrom + '').html('1+');
        }

        $('#bsUserId').val(toUserID);
        var _checkSetID = $('#setToIDUser').val();
        if (_checkSetID <= 0) {
            $('#setToIDUser').val(IDfrom);
            $("#loginNameToUser").val(fromUserUsername);
        }
        // localStore(toUserID, connectionIDto);
        //localStoreIsMe(IDfrom);

    };

    chatHub.client.sendPrivateMessageAllTabs = function (message, fileUser, toUserID) {

        var today = new Date(); var dd = today.getDate(); var mm = today.getMonth() + 1; var hh = today.getHours(); var mn = today.getMinutes(); var yyyy = today.getFullYear();
        if (dd < 10) { dd = '0' + dd; }
        if (mm < 10) { mm = '0' + mm; }
        if (mn < 10) { mn = '0' + mn; }
        var todaytext = dd + '/' + mm + '/' + yyyy + ' ' + hh + ':' + mn;
        var hihi = $('#fromID').val();

        var htmlMsg = '<li class="' + hihi + '"><div class="media"><div class="media-left"><img src="' + fileUser + '" class="media-object" ></div><div class="media-body"><p>' + message + '</p></div><div class="timestamp">' + todaytext + '</div></div></li>';

        $('#divChatWindow_' + toUserID + '').append(htmlMsg);

        var classx = "." + hihi;
        $(classx).addClass("me");

        var height = $('#chatbox_' + toUserID + '')[0].scrollHeight;

        $('#chatbox_' + toUserID + '').scrollTop(height);



    };

    //nhận lại tn đã chat cho shop, nhưng shop Off
    chatHub.client.sendPrivateMessageShopOffline = function (toUserUsername, fromUserUsername, message, IDfrom, fileUser, toUserID, seen) {

        $('#btn-chat').show();

        var _checkCtrID = 'userBox_' + toUserID;
        var ctrId = 'chatbox_' + toUserID;//id tab mới dành cho chát nhiều người 
        var findID = $('#list_chatContent').find('.chat-window-scrollable').length;

        if ($('#' + _checkCtrID).length < 1 && findID < 1) {
            addListUser(IDfrom, toUserID, fileUser, toUserUsername, 1, seen);
            $('#setToIDUser').val(toUserID);
            $("#loginNameToUser").val(toUserUsername);
        }

        if ($('#' + _checkCtrID).length < 1 && findID > 0) {
            addListUser(IDfrom, toUserID, fileUser, toUserUsername, 0, seen);
        }

        if ($('#' + ctrId).length < 1 && findID < 1) {
            createPrivateChatWindow(toUserID, fromUserUsername, fileUser, 1);

            $('#setToIDUser').val(toUserID);
            $("#name_shop").html(toUserUsername);
            $("#loginNameToUser").val(toUserUsername);
        }
        if ($('#' + ctrId).length < 1 && findID > 0) {
            createPrivateChatWindow(toUserID, toUserUsername, fileUser, 0);
        }
        AddMessage(message, fileUser, toUserID, ctrId, 1);

        var fromID = $('#fromID').val();
        var classx = "." + fromID;
        if (fromID > 0) {

            $(classx).addClass("me");
        } else {

            $(classx).addClass("me");
        }

        $('#bsUserId').val(toUserID);
        var _checkSetID = $('#setToIDUser').val();
        if (_checkSetID <= 0) {
            $('#setToIDUser').val(IDfrom);
            $("#loginNameToUser").val(fromUserUsername);
        }

        var _checkOffChat = readCookie('Web_CloseChat');
        if (_checkOffChat < 1) {
            open_global();
        }


        // localStore(toUserID, connectionIDto);

    };

    chatHub.client.sendConnectionID = function (toUserID, connectionIDto) {
        localStore(toUserID, connectionIDto);

    };


    //chatHub.client.sendPrivateMessageKiotOffYou = function (toUserUsername, fromUserUsername, message, IDfrom, fileUser, toUserID) {
    //    $('#btn-chat').show();

    //    var _checkCtrID = 'userBox_' + IDfrom;
    //    var ctrId = 'chatbox_' + IDfrom;//id tab mới dành cho chát nhiều người 
    //    var findID = $('#list_chatContent').find('.chat-window-scrollable').length;

    //    if ($('#' + _checkCtrID).length == 0 && findID == 0) {
    //        addListUser(toUserID, IDfrom, fileUser, toUserUsername, 1);
    //        $('#setIDUser').val(IDfrom);
    //    }

    //    if ($('#' + _checkCtrID).length == 0 && findID > 0) {
    //        addListUser(toUserID, IDfrom, fileUser, toUserUsername, 0);
    //    }

    //    if ($('#' + ctrId).length == 0 && findID == 0) {
    //        createPrivateChatWindow(IDfrom, toUserUsername, fileUser, 1);

    //        $('#setIDUser').val(toUserID);
    //        $("#name_shop").html(fromUserUsername);
    //    }
    //    if ($('#' + ctrId).length == 0 && findID > 0) {
    //        createPrivateChatWindow(IDfrom, toUserUsername, fileUser, 0);
    //    }

    //    AddMessage(message, fileUser, IDfrom, ctrId, 1);

    //    var classx = "." + IDfrom;
    //    $(classx).addClass("me");

    //    $('#bsUserId').val(IDfrom);
    //    var _checkSetID = $('#setIDUser').val();
    //    if (_checkSetID <= 0) {
    //        $('#setIDUser').val(IDfrom);
    //    }

    //    var _checkOffChat = readCookie('Web_CloseChat');
    //    if (_checkOffChat < 1) {
    //        open_global();
    //    }
    //}
}

function addListUser(userFromId, ToUserID, fileUserTo, toUserName, checkActive, seen) {
    var _listUser = '';
    //  $('#list_userChat li').removeClass('active');
    if (checkActive === 1) {

        _listUser = '<li id="userBox_' + ToUserID + '" class="itemzz item active " data-bsID="' + userFromId + '" data-id="' + ToUserID + '" data-name="' + toUserName + '">' +
            '<input id="setToUserID_' + ToUserID + '" value="' + ToUserID + '" type="hidden" />' +
            '<input id="setFromUserID_' + userFromId + '" value="' + userFromId + '" type="hidden" />' +
            '<div class="media shop-chat_avatar"><div class="media-left media-top">' +
            '<img src="' + fileUserTo + '" class="media-object" style="border-radius: 50% !important">' + '</div>' +
            '<div class="media-body">' + ' <h4 class="media-heading">' + toUserName + '</h4>' +

            '<div class="flex">' +
            '</div>' +
            '<span class="badge show ' + seen + '" id="count_mess_' + ToUserID + '"><i class="fa fa-check"></i></span>' +
            '</div>' +
            ' <a href="javascript:;" class="moveto selected btn btn-move btn-add btn-icon-only green delete-messenger" onclick="delete_mess(\'' + userFromId + '\',\'' + ToUserID + '\')" data-fromuser=' + userFromId + ' data-touser=' + ToUserID + '><i class="fa fa-minus"></i></a>' +
            '</li>';
    }
    else {
        _listUser = '<li id="userBox_' + ToUserID + '" class="item "  data-bsID="' + userFromId + '" data-id="' + ToUserID + '" data-name="' + toUserName + '">' +
            '<input id="setToUserID_' + ToUserID + '" value="' + ToUserID + '" type="hidden" />' +
            '<input id="setFromUserID_' + userFromId + '" value="' + userFromId + '" type="hidden" />' +
            '<div class="media shop-chat_avatar"><div class="media-left media-top">' +
            '<img src="' + fileUserTo + '" class="media-object" style="border-radius: 50% !important">' + '</div>' +
            '<div class="media-body">' + ' <h4 class="media-heading">' + toUserName + '</h4>' +

            '<div class="flex">' +
            '</div>' +
            '<span class="badge show ' + seen + '" id="count_mess_' + ToUserID + '">1+</span>' +
            '</div>' +
            ' <a href="javascript:;" class="moveto selected btn btn-move btn-add btn-icon-only green delete-messenger" onclick="delete_mess(\'' + userFromId + '\',\'' + ToUserID + '\')" data-fromuser=' + userFromId + ' data-touser=' + ToUserID + '><i class="fa fa-minus"></i></a>' +
            '</li>';
    }
    $('#list_userChat').append(_listUser);
    callBack();

}
//create tabcontent box 1vsN
function createPrivateChatWindow(userId, userName, fileUser, checkActive) {
    var divTabchat = '';
    // $('div.chat-window-scrollable').removeClass('active');
    if (checkActive === 1) {
        divTabchat = '<div class="chat-window-content chat-window-scrollable active" data-id="' + userId + '" id="chatbox_' + userId + '">' +
            '<div class="chat-window-content-inner" ><ul id="divChatWindow_' + userId + '" class="list_comment"></ul></div>' + '</div>';


    }
    else {
        divTabchat = '<div class="chat-window-content chat-window-scrollable" data-id="' + userId + '" id="chatbox_' + userId + '" >' +
            '<div class="chat-window-content-inner" ><ul id="divChatWindow_' + userId + '" class="list_comment"></ul></div>' + '</div>';
    }
    $('#list_chatContent').append(divTabchat);
    callBack();

}


function AddMessage(message, fileUser, userIDFrom, ctrId, offLine) {

    if (offLine === 1) {

        $('#' + ctrId).find($('#divChatWindow_' + userIDFrom + '').append(message));
    }
    else {
        var today = new Date(); var dd = today.getDate(); var mm = today.getMonth() + 1; var hh = today.getHours(); var mn = today.getMinutes(); var yyyy = today.getFullYear();
        if (dd < 10) { dd = '0' + dd; }
        if (mm < 10) { mm = '0' + mm; }
        if (mn < 10) { mn = '0' + mn; }
        var todaytext = dd + '/' + mm + '/' + yyyy + ' ' + hh + ':' + mn;
        var htmlMsg = '<li class="' + userIDFrom + '"><div class="media"><div class="media-left"><img src="' + fileUser + '" class="media-object" ></div><div class="media-body"><p>' + message + '</p></div><div class="timestamp">' + todaytext + '</div></div></li>';
        $('#' + ctrId).find($('#divChatWindow_' + userIDFrom + '').append(htmlMsg));

        var height = $('#chatbox_' + userIDFrom + '')[0].scrollHeight;
        $('#chatbox_' + userIDFrom + '').scrollTop(height);

        playSound();
    }

    callBack();
}


function callBack() {


    $('#list_userChat li').click(function () {
        $('#list_userChat li').removeClass('active');
        $(this).addClass('active');

        var id = $(this).data('id');
        var _setName = $(this).data('name');
        var fromId = $(this).data('bsid');

        $("#name_shop").html(_setName);
        $('#setToIDUser').val(id);
        $('#bsUserId').val(fromId);

        var check = $('#chatbox_' + id + '');

        //if ($('div.chat-window-scrollable').hasClass('active')) {
        $('div.chat-window-scrollable').removeClass('active');

        $(check).addClass('active');
        $('#count_mess_' + id + '').addClass('seen');
        $('#count_mess_' + id + '').html('<i class="fa fa-check"></i>');

        var height = $('#chatbox_' + id + '')[0].scrollHeight;
        $('#chatbox_' + id + '').scrollTop(height);
        //}

    });

    //$('.delete-messenger').on('click', function () {
    //    var toUser = $(this).data('touser');
    //    var fromUser = $(this).data('fromuser');
    //    delete_mess(fromUser, toUser);
    //});
}

function playSound() {
    var sound = document.getElementById("audio");
    sound.play();
}

function open_global() {
    $('#btn-chat').fadeOut(500);
    $('#shop-chat-bg').fadeIn(1000);
}
function closeglobal() {
    $('#shop-chat-bg').fadeOut(500);
    $('#btn-chat').fadeIn(1000);


}
function mouse_point() {
    $('#btnStartChat').addCss('cursor', 'pointer');
}


function delete_mess(toUser, fromUser) {

    $.ajax({
        url: '/ajax/DeleteMessenger.html',
        type: 'post',
        data: 'touserid=' + toUser + '&fromuser=' + fromUser,
        dataType: 'json',
        success: function (data) {
            var content = data.Html;
            var notifi = data.Params;
            if (content != '') {
                var _checkCtrID = 'userBox_' + toUser;
                var ctrId = 'chatbox_' + toUser;
                location.reload();
                //document.getElementById(_checkCtrID).remove();
                //document.getElementById(ctrId).remove();

            }
            if (notifi != '') {
                alert(notifi);
            }

        },
        error: function () { }
    });
}