$(function () {
    setTimeout(function () {
        BackgroundLazyLoadMore();
    }, 800);
});

function BackgroundLoadNode({ node, loadedClassName }) {
    let func = node.getAttribute('data-func-load');
    let value = node.getAttribute('data-value-load');
    let page = node.getAttribute('data-page-load');
    let pagedex = node.getAttribute('data-pagedex-load');
    let show = (onComplete) => {
        requestAnimationFrame(() => {
            $.ajax({
                url: '/ajax/Anonymous/',
                type: 'post',
                data: {
                    device: 1,
                    typefunc: func,
                    cate: value,
                    pageid: page,
                    pagedex: pagedex
                },
                dataType: 'json',
                success: function (data) {
                    var content = data.Html;

                    if (content != '') {
                        var _class = '.' + func + '_' + value;
                        $(_class).fadeOut(500, function () {
                            $(this).html(content).fadeIn(500);
                        });
                    }
                },
                error: function (data) { }
            });
            onComplete();
        });
    };

    return {
        node,
        load: (onComplete) => {
            show(onComplete);
        }
    };
}

let defaultOptionsLoadMore = {
    selector: '[data-func-load]',
    loadedClassName: 'loaded'
};

function BackgroundLazyLoadMore({ selector, loadedClassName } = defaultOptionsLoadMore) {
    let nodes = [].slice.apply(document.querySelectorAll(selector))
        .map(node => new BackgroundLoadNode({ node, loadedClassName }));

    let callback = (entries, observer) => {
        entries.forEach(({ target, isIntersecting }) => {
            if (!isIntersecting) {
                return;
            }

            let obj = nodes.find(it => it.node.isSameNode(target));

            if (obj) {
                obj.load(() => {
                    observer.unobserve(target);
                    nodes = nodes.filter(n => !n.node.isSameNode(target));

                    if (!nodes.length) {
                        observer.disconnect();
                    }
                });
            }
        });
    };

    let observer = new IntersectionObserver(callback);
    nodes.forEach(node => observer.observe(node.node));
}


(function ($) {
    var size;

    //fixed HEADER WHEN SCROLL PAGE
    function fixedMenu() {
        var sc = $(window).scrollTop();

    }

    // VERIFY WINDOW SIZE
    function windowSize() {
        size = $(document).width();
        if (size >= 991) {
            $('body').removeClass('open-menu');
            $('.hamburger-menu .bar').removeClass('animate');
        }
    };

    // ESC BUTTON ACTION
    $(document).keyup(function (e) {
        if (e.keyCode == 27) {
            $('.bar').removeClass('animate');
            $('body').removeClass('open-menu');
            $('header .desk-menu .menu-container .menu .menu-item-has-children a ul').each(function (index) {
                $(this).removeClass('open-sub');
            });
        }
    });

    // ANIMATE HAMBURGER MENU
    $('.hamburger-menu').on('click', function () {
        $('.hamburger-menu .bar').toggleClass('animate');
        $(".box_s_h_mb").removeClass('show_h');
        if ($('body').hasClass('open-menu')) {
            $('body').removeClass('open-menu');
        } else {
            $('body').toggleClass('open-menu');
        }
    });

    // btn-icon-menu-mobie-USER
    $(".box-btn-mb").on('click', function () {
        if ($(window).width() < 992) {
            $(".show-list").toggle(200);
            $(".box-btn-mb .fa").toggleClass("fa-times");
            $(".box-btn-mb .fa").toggleClass("fa-caret-down");
        }
    });

    // back
    $('header .desk-menu .menu-container .menu .menu-item-has-children ul').each(function (index) {
        $(this).append('<li class="back"><a href="#">Back</a></li>');
    });

    // RESPONSIVE MENU NAVIGATION
    $('header .desk-menu .menu-container .menu .menu-item-has-children > a').on('click', function (e) {
        if (size <= 991) {
            e.preventDefault();
            $(this).next('ul').addClass('open-sub');
        }
    });

    // CLICK FUNCTION BACK MENU RESPONSIVE
    $('header .desk-menu .menu-container .menu .menu-item-has-children ul .back').on('click', function (e) {
        e.preventDefault();
        $(this).parent('ul').removeClass('open-sub');
    });

    $('body .over-menu').on('click', function () {
        $('body').removeClass('open-menu');
        $('.bar').removeClass('animate');
        $(".box_s_h_mb").removeClass('show_h');
        $('body').removeClass('bgr_x');

    });

    $('.phone').on('input', function (evt) {
        $(this).val(function (_, val) {
            val = val.replace(/(\d{4})(\d{3})(\d{3})/, "$1.$2.$3");
            val = val.replace(/\s/g, '');
            return val;
        });
    });

    $(document).ready(function () {
        windowSize();
    });

    $(window).scroll(function () {
        fixedMenu();
    });

    $(window).resize(function () {
        windowSize();
    });




    $('.translation-links a').click(function () {
        var lang = $(this).data('lang');

        var $frame = $('.goog-te-menu-frame:first');
        if ($frame.length < 1) {
            alert("Error: Could not find Google translate frame.");
            return false;
        }

        $frame.contents().find('.goog-te-menu2-item span.text:contains(' + lang + ')').get(0).click();

        return false;

    });




    // Back to top
    $(".back-to-top a").click(function (n) {
        n.preventDefault();
        $("html, body").animate({
            scrollTop: 0
        }, 500)
    });
    $(window).scroll(function () {
        $(document).scrollTop() > 1e3 ? $(".back-to-top").addClass("display") : $(".back-to-top").removeClass("display")
    });

})(jQuery);


// slide_index
$('.slide_index-home').owlCarousel({
    loop: true,
    autoplay: true,
    autoplayTimeout: 10000,
    items: 1,
    smartSpeed: 450,
    paginationSpeed: 400,
    nav: true,
    dots: false,
    responsive: {
        0: {

            nav: true,
        },
        992: {

            nav: true,
        },
    }


});

// slide_hoatdong
$('.box_3img').owlCarousel({
    loop: true,
    items: 1,
    smartSpeed: 450,
    paginationSpeed: 400,
    nav: true,
    responsive: {
        0: {

            nav: true,
        },
        992: {

            nav: true,
        },
    },
    pagination: false


});




// list_news
$('.owl_news_id').owlCarousel({
    loop: true,
    autoplay: true,
    autoplayTimeout: 6000,
    items: 1,
    responsiveClass: true,
    dots: true,
    nav: true
})
// list video news id
$('.list_videos_id').owlCarousel({
    loop: true,
    items: 3,
    margin: 10,
    dots: false,
    nav: true,
    responsiveClass: true,
    responsive: {
        768: {
            items: 3,
        },
        0: {
            items: 2,
        }
    }

})

// list news same same
$('.list_news_same').owlCarousel({
    loop: true,
    items: 3,
    margin: 10,
    dots: true,
    nav: true,
    responsiveClass: true,
    responsive: {
        768: {
            items: 3,
        },
        0: {
            items: 2,
        }
    }

})

//list_nghien cứu
$('.list_nghiencuu').owlCarousel({
    center: true,
    items: 3,
    loop: true,
    margin: 10,
    nav: true,
    dots: false,
    responsive: {
        767: {
            items: 3,
            nav: true,
        },
        0: {
            center: false,
            items: 2,
            nav: true,
        }
    }
});


$('textarea.description').on("keypress", function () {
    var max = 4000;
    var len = $(this).val().length;
    if (len >= max) {
        $(this).parent().find('.validation').text('Ký tự tối đa: 0');
        $(this).val($(this).val().substring(0, 3999));
    } else {
        var char = max - len;
        $(this).parent().find('.validation').text('Ký tự tối đa: ' + char);
    }
});
function change_captcha() {
    var e = Math.floor(Math.random() * 999999); document.getElementById("imgValidCode").src = "/ajax/Security.html?Code=" + e;
}


$('.get-data').on('click', function () {
    getDetail($(this).data('name'), $(this).data('value'), $(this).data('id'));
    $('.itembox_links').find('ul').children().removeClass('active');
    $(this).parent().addClass('active');
});







function viewacticle(name) {
    $('.loading').show();

    setTimeout(function () {
        $.ajax({
            url: '/ajax/ViewsActicle/',
            type: 'POST',
            data: {
                NKHID: name,
            },
            success: function (data) {
                var content = data.Html;


                if (content != '') {
                    $('#load_noidung').html(content);
                }

                $('.loading').hide();
            },
            error: function () { }
        });
    }, 250);

}


function getDetail(name, type, nkh) {
    $('.loading').show();
    setTimeout(function () {
        $.ajax({
            url: '/ajax/GetData/',
            type: 'POST',
            data: {
                NKHID: nkh,
                name: name,
                Type: type
            },
            success: function (data) {
                var content = data.Html;


                if (content != '') {
                    $('#load_noidung').html(content);
                }

                $('.loading').hide();
            },
            error: function () { }
        });
    }, 250);

}
$('.list_links_l').on("change", function () {
    var selected = $(':selected', this);
    var name = selected.closest('optgroup').attr('id');
    var type = this.value; var nkhid = $('#nkhid').val();
    getDetail(name, type, nkhid);
})
$('#sort').on('change', function () {
    var _url = $(this).data('url');
    var selected = $('#page').val();
    var sURL = '';
    sURL += (sURL == '' ? '?' : '&') + 'sort=' + this.value;
    sURL += (sURL == '' ? '?' : '&') + 'PageSize=' + selected;
    filter_product(_url + sURL, '.list_news','');
   
})
$('#page').on('change', function () {
    var _url = $(this).data('url');
    var selected = $('#sort').val();
    var sURL = '';
    sURL += (sURL == '' ? '?' : '&') + 'sort=' + selected;
    sURL += (sURL == '' ? '?' : '&') + 'PageSize=' + this.value;
    filter_product(_url + sURL,'.list_news','');
})


function filter_product(url, classz, keysearch) {
    $('.loading').show();
   
    url += (keysearch == '' ? '' : (url.indexOf('?') > 0 ? '&news=' + keysearch : '?news=' + keysearch));
    $.ajax({
        url: url,
        type: 'get',
        dataType: 'html',
        success: function (data) {
            var html = data;
            data = $(data).find(classz).html();
            data = $.trim(data);
            window.history.pushState({}, null, url);
            $(classz).html(data);
            data = $(html).find('.pagination').html();
            data = $.trim(data);
            $('.pagination').html(data);
            $('.loading').hide();
        },
        error: function () { }
    });
}
function search() {
    $.ajax({
        url: '/ajax/search',
        type: 'get',
        dataType: 'json',
        success: function (resultOut) {
            if (resultOut != undefined) {
                $('.ui.search').search({
                    type: 'category',
                    source: resultOut.DataInfo
                });
            }
        },
        error: function (status) { }
    });
}

var imageArray = [];
function readURL(input, preview) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('.' + preview).attr('src', e.target.result);
            imageArray.push(e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}


$('input[type="file"]').change(function () {
    var id = $(this).data('id');
    var arrID = id.split('_');
    if (arrID.length === 2) {
        var index = id.replace(arrID[0] + '_', '');
        readURL(this, arrID[0] + '_Preview_' + index);
    }
    else {
        readURL(this, id + '_Preview');
    }

    return;
});


function previewFile(type) {
    if (type === 'logo') {
        var img = $('.fileinput-new img');
        var img2 = $('.profile-userpic img');
        var file = document.querySelector('input[name=upload-logo]').files[0];
        var reader = new FileReader();

        if (file) {
            reader.readAsDataURL(file);
        } else {
            img.attr('src', '');
        }

        reader.onloadend = function () {
            img.attr('src', reader.result);
            img2.attr('src', reader.result);
        }
    }

}

function deleteFile(id) {
    var img = $('#' + id);
    img.attr('src', '');
}


function check_pass(pass, pass2) {

    if (pass !== pass2) {
        $('#Password2').html('Mật khẩu nhập lại không đồng nhất.');
    }
    else {
        $('#Password2').html('');
    }
}


function check_acc(loginName, type, type2) {

    var dataString = 'LoginName=' + loginName + '&Type=' + type;

    $.ajax({
        url: '/api/getaccount/',
        type: 'GET',
        data: dataString,
        dataType: 'json',
        success: function (data) {
            var content = data.Html;

            if (type === 'LoginName') {
                $('#messenger').html(content);
            }
            if (type === 'email' && type2 == '') {
                $('#email').html(content);
            }
            if (type === 'phone' && type2 == '') {
                $('#phone').html(content);
            }
            if (type === 'Pass') {
                $('#Pass').html(content);
            }
            if (type2 != '') {
                $('#' + type2).html(content);
            }
        },
        error: function (status) { }
    });
}


$('.loginname_changer').on('input', function (evt) {
    $(this).val(function (_, val) {

        str = val.toLowerCase();
        str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
        str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
        str = str.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
        str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
        str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
        str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
        str = str.replace(/đ/g, 'd');
        str = str.replace(/\W+/g, ' ');
        str = str.replace(/\s/g, '');
        return str;

    });
});


$('.name_full').on('input', function (evt) {
    $(this).val(function (_, val) {
        if (val.length > 50) { alert('Họ tên không được vượt quá 50 ký tự'); return; }

        return val;
    });
});

$('.phone').on('change', function (evt) {
    $(this).val(function (_, val) {
        val = val.replace(/(\d{4})(\d{3})(\d{3})/, "$1.$2.$3");
        val = val.replace(/\s/g, '');
        if (val.length != 12) { alert('Nhập đúng định dạng của số điện thoại'); return; }
        if ($(this).data('id') == 1) {
            check_acc(val, 'phone', '');
        }
        else { check_acc(val, 'phone', 'phone2'); }
        return val;
    });
});


function down_load(url, link, check) {
    if (check === 'True') {
        $.ajax({
            url: '/ajax/plusdownload/',
            type: 'post',
            dataType: 'json',
            data: { url: url },
            success: function () {

            },
            error: function () { }
        });
        const a = document.createElement('a');
        a.href = link;
        a.download = link.split('/').pop();
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
    }
    else {
        zebra_alert('Thông báo !', 'Quý vị cần đăng nhập để tải tệp');
    }
}