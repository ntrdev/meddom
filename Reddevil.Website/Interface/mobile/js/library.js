﻿$(function () {
    var $menu = $("#mainMenu").clone();
    $menu.attr("id", "mobile-menu");
    $menu.mmenu({
        extensions: ["theme-white"]
    });

    $('.liststore li.item-order').click(function () {
        var e = $(this).data("id");
        $('div.showroom').eq(e).modal('show');
    })

    $('#btnOrder').click(function () {
        checkout();
    })

    $('.buy_now').click(function () {
        add_cart($(this).data('id'), $(this).data('returnpath'));
    })

    //$('.phone_pd_btn').click(function () {
    //    location.href = 'tel:' + $(this).text().replace(/\D+/g, '');
    //})

    $('#ATM').click(function () {
        $('.bankBox').show();
    });
    $('#COD').click(function () {
        $('.bankBox').hide();
    });
    $('#Amortization').click(function () {
        $('.bankBox').hide();
    });

    $('.pagination a').click(function () {
        page++;
        paging_product($(this).data('url'));
    })

    $('.main-property li a').click(function () {
        var $this = $(this);
        $('.main-property li a').each(function () {
            if ($(this).attr('name') == $this.attr('name')) {
                $(this).removeClass('active');
            }
        })

        $this.addClass('active');
        filter_product($this.data('url'));

        $('.property-selected').html('');
        $('.main-property li a').each(function () {
            if ($(this).hasClass('active')) {
                $('.property-selected').append('<p>' + $(this).text() + '</p>');
            }
        })
    })

    $('.category li,.filterBrand li,.product-fs li').click(function () {
        location.href = $(this).data('url');
    })

    $('.postInfo table').addClass('table mb0');

    $('.ctgrLeft,.fix_header,.infoRightDetail').stick_in_parent();

    var slide = new Swiper('.slidemain', {
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });

    var brand = new Swiper('.brand-home', {
        slidesPerView: 3,
        slidesPerColumn: 3,
        spaceBetween: 0,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });

    var detail = new Swiper('.owl-detail-prd', {
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });

    //hidden content
    $('#js-show-more').click(function () {
        if ($('.js-content').hasClass('expand')) {
            $('.js-content').removeClass("expand");
            document.getElementById('js-show-more').innerHTML = " Thu Gọn Nội Dung";
        } else {
            $('.js-content').addClass("expand");
            document.getElementById('js-show-more').innerHTML = " Xem Thêm Nội Dung";
        }
    });
})

function add_cart(id, returnpath) {
    location.href = '/gio-hang/Add.html?ProductID=' + id + '&Quantity=1&returnpath=' + returnpath;
}

function update_cart(index, quantity, returnpath) {
    location.href = '/gio-hang/Update.html?Index=' + index + '&Quantity=' + quantity + '&returnpath=' + returnpath;
}

function delete_cart(index) {
    location.href = '/gio-hang/Delete.html?Index=' + index;
}

function change_captcha() {
    var e = Math.floor(Math.random() * 999999); document.getElementById("imgValidCode").src = "/ajax/Security.html?Code=" + e
}