﻿$(function () {

    $('#btnOrder').click(function () {
        checkout();
    })

    $('.buy_now').click(function () {
        add_cart($(this).data('id'), $(this).data('returnpath'));
    })

    $('.pagination a').click(function () {
        page++;
        paging_product($(this).data('url'));
    })

    $('.filterTopCtgr a').click(function () {
        $('.filterTopCtgr a').removeClass('active');
        $(this).addClass('active');

        filter_product($(this).data('url'));
    })

    $('.right-property li input').on('change', function () {
        $('input[type="checkbox"][name="' + $(this).attr('name') + '"]').not(this).prop('checked', false);
        filter_product($(this).data('url'));
    })

    $('.category li,.filterBrand li,.product-fs li').click(function () {
        location.href = $(this).data('url');
    })

    $('.postInfo table').addClass('table mb0');

    $('.ctgrLeft,.fix_header,.infoRightDetail').stick_in_parent();


    //var top = $('.ctgrLeft').length ? $('.ctgrLeft').offset().top + $('.ctgrLeft').height() : $('#footer').offset().top;
    //var bottom = $('#footer').offset().top - $(window).height();
    //var right = $('.ctInfoRight').height();
    //$(window).scroll(function () {

    //    if ($('.ctgrLeft').length) {
    //        if ($(window).scrollTop() > top) {
    //            $('.ctgrLeft').addClass('left-fixed', { duration: 500 });
    //        } else {
    //            $('.ctgrLeft').removeClass('left-fixed', { duration: 500 });
    //        }

    //        if ($(window).scrollTop() > bottom) {
    //            $('.ctgrLeft').removeClass('left-fixed', { duration: 500 });
    //        }
    //    }

    //    if ($(window).scrollTop() > $('#headerTop').height()) {
    //        $('#headerTop').addClass('top-fixed', { duration: 500 });
    //    }
    //    else {
    //        $('#headerTop').removeClass('top-fixed', { duration: 500 });
    //    }

    //    //if ($('.infoRightDetail').length) {
    //    //    if ($(window).scrollTop() > ($('.ctInfoRight').offset().top + $('.ctInfoRight').height())) {
    //    //        $('.infoRightDetail').addClass('right-fixed', { duration: 500 });
    //    //    } else {
    //    //        $('.infoRightDetail').removeClass('right-fixed', { duration: 500 });
    //    //    }

    //    //    //if ($(window).scrollTop() > (bottom - right)) {
    //    //    //    $('.infoRightDetail').removeClass('right-fixed', { duration: 500 });
    //    //    //}
    //    //}
    //});

    var slideTop = $('#slide_home');
    slideTop.owlCarousel({
        autoPlay: 3000,
        navigation: false,
        pagination: false,
        slideSpeed: 500,
        paginationSpeed: 500,
        singleItem: true,
        pagination: true,
        autoHeight: true,
    });
    $('#slide_home_prev').click(function () {
        slideTop.trigger('owl.next');
    })
    $('#slide_home_next').click(function () {
        slideTop.trigger('owl.prev');
    })

    'use strict';
    $(".owl-sp-nbat").owlCarousel({
        slideSpeed: 300,
        items: 5,
        itemsCustom: false,
        itemsDesktop: [1199, 5],
        itemsDesktopSmall: [979, 4],
        itemsTablet: [768, 3],
        itemsTabletSmall: false,
        itemsMobile: [479, 2],
        autoPlay: true,
        stopOnHover: true,
        addClassActive: true,
        autoHeight: false,
        responsive: true,
        navigation: true,
        pagination: false,
        navigationText: ["\<i class=\"fa fa-chevron-left\"></i>", "\<i class=\"fa fa-chevron-right\"></i>"],
    });
})

var currentURL = $('#CurrentURL').length ? $('#CurrentURL').val() : '';

function GetParam() {
    var vars = {};
    var parts = currentURL.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
        vars[key] = value;
    });

    return vars;
}

function GetURL(type, value) {
    var sort = '', atr = '', page = '1';
    var param = GetParam();

    if (currentURL.indexOf('sort=') > -1)
        sort = param['sort'];

    if (currentURL.indexOf('atr=') > -1)
        atr = param['atr'];

    if (currentURL.indexOf('page=') > -1)
        page = param['page'];

    if (type == 'sort') {
        return window.location.href + '?sort=' + value + '&atr=' + atr + '&page=' + page;
    }

    if (type == 'atr') {
        value = atr + '-' + value;
        return window.location.href + '?sort=' + sort + '&atr=' + value + '&page=' + page;
    }

    if (type == 'page') {
        return window.location.href + '?sort=' + sort + '&atr=' + atr + '&page=' + value;
    }
}

function add_cart(id, returnpath) {
    location.href = '/gio-hang/Add.html?ProductID=' + id + '&Quantity=1&returnpath=' + returnpath;
}

function update_cart(index, quantity, returnpath) {
    location.href = '/gio-hang/Update.html?Index=' + index + '&Quantity=' + quantity + '&returnpath=' + returnpath;
}

function delete_cart(index) {
    location.href = '/gio-hang/Delete.html?Index=' + index;
}

function change_captcha() {
    var e = Math.floor(Math.random() * 999999); document.getElementById("imgValidCode").src = "/ajax/Security.html?Code=" + e
}