﻿function readURL(e) {
    if (e.files && e.files[0]) {
        var t = new FileReader;
        t.onload = function (t) {
            $(e).parent().find("img").attr("src", t.target.result)
        }, t.readAsDataURL(e.files[0])
    }
}

function get_code(e) {
    return remove_unicode(e).replace(/[^A-Z0-9]/gi, "")
}

function remove_unicode(e) {
    return (e = (e = (e = (e = (e = (e = (e = (e = (e = (e = e.toLowerCase()).replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a")).replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e")).replace(/ì|í|ị|ỉ|ĩ/g, "i")).replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o")).replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u")).replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y")).replace(/đ/g, "d")).replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'| |\"|\&|\#|\[|\]|~|$|_/g, "-")).replace(/-+-/g, "-")).replace(/^\-+|\-+$/g, "")
}

function formatDollar(e) {
    return e.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.")
}

function createCookie(e, t, o) {
    var i = "";
    if (o) {
        var n = new Date;
        n.setTime(n.getTime() + 60 * o * 1e3), i = "; expires=" + n.toGMTString()
    }
    document.cookie = e + "=" + t + i + "; path=/"
}

function readCookie(e) {
    for (var t = e + "=", o = document.cookie.split(";"), i = 0; i < o.length; i++) {
        for (var n = o[i];
            " " == n.charAt(0);) n = n.substring(1, n.length);
        if (0 == n.indexOf(t)) return n.substring(t.length, n.length)
    }
    return null
}

function eraseCookie(e) {
    createCookie(e, "", -1)
}

function add_checkout(e, t) {
    location.href = "/gio-hang/AddCheckout.html?ProductID=" + e + "&Quantity=" + t
}

function add_cart(e, t, o) {
    var g = $('#GiftID').val();
    if (g == 'undefined')
        g = '';

    e < 1 ? zebra_alert("Thông báo !", "Sản phẩm không tồn tại.") : t < 1 ? zebra_alert("Thông báo !", "Số lượng mua phải từ 1 sản phẩm.") : location.href = "/gio-hang/Add.html?ProductID=" + e + "&Quantity=" + t + "&GiftID=" + g + "&returnpath=" + o
}

function update_cart(e, t, o) {
    location.href = "/gio-hang/Update.html?Index=" + e + "&Quantity=" + t + "&returnpath=" + o
}

function delete_cart(e, t) {
    zebra_confirm("Thông báo !", "Bạn chắc chắn muốn xóa ?", "/gio-hang/Delete.html?Index=" + e + "&returnpath=" + t)
}

function add_favorite(e, t) {
    location.href = "/yeu-thich/Add.html?ProductID=" + e + "&returnpath=" + t
}

function delete_favorite(e) {
    zebra_confirm("Thông báo !", "Bạn chắc chắn muốn xóa ?", "/yeu-thich/Delete.html?Index=" + e)
}

function playVideo(e, t, o, i, n, a) {
    jwplayer(e).setup({
        file: t,
        image: o,
        abouttext: "ANGKORICH - jwplayer build 01032015",
        width: i,
        height: n,
        stretching: "exactfit",
        autostart: a,
        logo: {
            file: "",
            link: ""
        }
    })
}

function change_captcha() {
    var e = Math.floor(999999 * Math.random());
    document.getElementById("imgValidCode").src = "/Ajax/Security.html?Code=" + e
}

function popup(e, t, o, i) {
    location.href = e
}
$(function () {
    $('.view-more').click(function () {
        paging_product($(this).data('url'));
    })

    $(".register-promotion").click(function () {
        register()
    }), $("#register_form").on("submit", function () {
        register()
    }), $(".item-order").click(function () {
        var e = $(this).data("id");
        $("div.showroom").eq(e).modal("show")
    }), $("#ATM").click(function () {
        $("#bank-box").modal("show")
        }), $(".listform_filter li").click(function () {
        location.href = $(this).data("url")
    }), $(".btn-cart").click(function () {
        add_cart($(this).data("id"), 1, $(this).data("returnpath"))
    }), $(".back-to-top a").click(function (e) {
        e.preventDefault(), $("html, body").animate({
            scrollTop: 0
        }, 500)
    }), $(window).scroll(function () {
        $(document).scrollTop() > 1e3 ? $(".back-to-top").addClass("display") : $(".back-to-top").removeClass("display")
    });
    var e = $("#slide_home");
    e.owlCarousel({
        autoPlay: 3e3,
        navigation: !1,
        pagination: !1,
        slideSpeed: 500,
        paginationSpeed: 500,
        singleItem: !0,
        pagination: !0,
        autoHeight: !0
    }), $("#slide_home_prev").click(function () {
        e.trigger("owl.next")
    }), $("#slide_home_next").click(function () {
        e.trigger("owl.prev")
    }), $(".owl-branTop").owlCarousel({
        slideSpeed: 300,
        items: 7,
        itemsCustom: !1,
        itemsDesktop: [1199, 7],
        itemsDesktopSmall: [979, 7],
        itemsTablet: !1,
        itemsTabletSmall: !1,
        itemsMobile: !1,
        autoPlay: 1e4,
        stopOnHover: !0,
        addClassActive: !0,
        autoHeight: !1,
        responsive: !0,
        navigation: !1,
        pagination: !1,
        navigationText: ["", ""]
    });
    var t = $("#fix_header"),
        o = $(t).offset().top;
    $.event.add(window, "scroll", function () {
        var e = $(".logo"),
            i = $(window).scrollTop();
        $(t).css("position", i > o ? "fixed" : "relative"), $(t).css("top", i > o ? "0px" : ""), $(t).css("width", i > o ? "100%" : ""), $(t).css("box-shadow", i > o ? "0 2px 2px rgba(0, 0, 0, 0.17)" : ""), $(e).css("width", ""), $(e).css("height", ""), $(e).css("margin-left", i > o ? "0px" : ""), $(e).css("margin-top", i > o ? "0px" : ""), i <= 30 ? $(t).removeClass("scrollHeader") : $(t).addClass("scrollHeader")
    });
    var i = $("#slide-img"),
        n = $("#slide-text");
    i.owlCarousel({
        autoPlay: 1e4,
        singleItem: !0,
        slideSpeed: 600,
        navigation: !1,
        pagination: !1,
        afterAction: function (e) {
            var t = this.currentItem;
            $("#slide-text").find(".owl-item").removeClass("synced").eq(t).addClass("synced"), void 0 !== $("#slide-text").data("owlCarousel") && function (e) {
                var t = n.data("owlCarousel").owl.visibleItems,
                    o = e,
                    i = !1;
                for (var a in t)
                    if (o === t[a]) i = !0;
                !1 === i ? o > t[t.length - 1] ? n.trigger("owl.goTo", o - t.length + 2) : (o - 1 == -1 && (o = 0), n.trigger("owl.goTo", o)) : o === t[t.length - 1] ? n.trigger("owl.goTo", t[1]) : o === t[0] && n.trigger("owl.goTo", o - 1)
            }(t)
        },
        responsiveRefreshRate: 200,
        navigationText: ["<i class='glyphicon glyphicon-menu-left'></i>", "<i class='glyphicon glyphicon-menu-right'></i>"]
    }), n.owlCarousel({
        autoPlay: 1e4,
        items: 5,
        itemsDesktop: [1199, 5],
        itemsDesktopSmall: [979, 5],
        itemsTablet: [768, 5],
        itemsMobile: [479, 5],
        pagination: !1,
        responsiveRefreshRate: 100,
        afterInit: function (e) {
            e.find(".owl-item").eq(0).addClass("synced")
        }
    }), $("#slide-text").on("click", ".owl-item", function (e) {
        e.preventDefault();
        var t = $(this).data("owlItem");
        i.trigger("owl.goTo", t)
    }), $("#js-show-more").click(function () {
        $(".js-content").hasClass("expand") ? ($(".js-content").removeClass("expand"), document.getElementById("js-show-more").innerHTML = " Thu Gọn Nội Dung") : ($(".js-content").addClass("expand"), document.getElementById("js-show-more").innerHTML = " Xem Thêm Nội Dung")
    }), $('input[name="Formality"]').click(function () {
        "0" == $(this).attr("value") && ($(".Shipping").show(), $(".Showroom").hide()), "1" == $(this).attr("value") && ($(".Showroom").show(), $(".Shipping").hide())
    }), $("input#Shipping").trigger("click")
}),
    function (e, t, o) {
        var i, n = e.getElementsByTagName("script")[0];
        e.getElementById(o) || ((i = e.createElement("script")).id = o, i.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.6&appId=565526433591379", n.parentNode.insertBefore(i, n))
    }(document, 0, "facebook-jssdk");
$(document).ready(function () {
    $(".fb-customerchat iframe").removeClass("fb_customer_chat_bounce_in_v2");
    $(".fb-customerchat iframe").addClass("fb_customer_chat_bounce_out_v2");
});